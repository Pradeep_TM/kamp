﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Windows.Input;
    using KAMP.KYC.BLL;
    using Core.Workflow;
    using KAMP.KYC.UserControls;
    public class CaseManagementPageVM : INotifyPropertyChanged
    {
        public CaseManagementPageVM()
        {
            PropertyChanged += CaseManagementPageVM_PropertyChanged;
            InitializeData();
        }

        private void InitializeData()
        {
            CaseFiles = new ObservableCollection<CaseFileVM>(new KYCBLL().GetAssignedCases());
            BkpCaseFiles = CaseFiles;
            IsViewingAllCases = true;
            IsCaseFileCardOpen = false;
            SetDefaultCaseSelection();
        }

        private void SetDefaultCaseSelection()
        {
            if (CaseFiles.IsCollectionValid())
                SelectedCaseFile = CaseFiles.First();
        }

        #region Private Fields
        private CaseFileVM _selectedCaseFile;
        private ObservableCollection<CaseFileVM> _caseFiles;
        private ObservableCollection<CaseFileVM> _bkpcaseFiles;
        private ICommand _viewMyCases;
        private ICommand _viewCaseCard;
        private ICommand _viewAllCases;
        private bool _isViewingAllCases;
        private bool _isCaseFileCardOpen;
        private CaseFileCardPageVM _caseFileCardVM;

        #endregion

        #region Properties

        private PaginationVM _paginationVM;
        public PaginationVM PaginationVM
        {
            get { return _paginationVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _paginationVM, (x) => x.PaginationVM); }
        }

        public ObservableCollection<CaseFileVM> CaseFiles
        {
            get { return _caseFiles; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseFiles, (x) => x.CaseFiles); }
        }

        public ObservableCollection<CaseFileVM> BkpCaseFiles
        {
            get { return _bkpcaseFiles; }
            set { PropertyChanged.HandleValueChange(this, value, ref _bkpcaseFiles, (x) => x.BkpCaseFiles); }
        }

        public CaseFileVM SelectedCaseFile
        {
            get { return _selectedCaseFile; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedCaseFile, (x) => x.SelectedCaseFile); }
        }

        public bool IsViewingAllCases
        {
            get { return _isViewingAllCases; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isViewingAllCases, (x) => x.IsViewingAllCases); }
        }

        public CaseFileCardPageVM CaseFileCardVM
        {
            get { return _caseFileCardVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseFileCardVM, (x) => x.CaseFileCardVM); }
        }

        public ICommand ViewMyCases
        {
            get
            {
                if (_viewMyCases.IsNull())
                {
                    _viewMyCases = new DelegateCommand(
                        () =>
                        {
                            //CaseFiles = new ObservableCollection<CaseFileVM>();
                            //CaseFiles.AddRange(new KYCBLL().GetMyCases());
                            BkpCaseFiles = new ObservableCollection<CaseFileVM>();
                            BkpCaseFiles.AddRange(new KYCBLL().GetMyCases());
                            CaseFiles = new ObservableCollection<CaseFileVM>(BkpCaseFiles.Take(PaginationVM.DisplayitemCount));
                            PaginationVM.TotalItem = BkpCaseFiles.Count;
                            PaginationVM.StartIndex = CaseFiles.IsCollectionValid() ? 1 : 0;
                            PaginationVM.CurrentIndex = CaseFiles.IsCollectionValid() ? 1 : 0;
                            SetDefaultCaseSelection();
                            SelectedCaseFile = null;
                            IsViewingAllCases = false;
                        }
                        );

                }

                return _viewMyCases;
            }
        }



        public ICommand ViewAllCases
        {
            get
            {
                if (_viewAllCases.IsNull())
                {
                    _viewAllCases = new DelegateCommand(
                        () =>
                        {

                            ShowAllCases();
                        }
                        );

                }

                return _viewAllCases;
            }
        }

        private void ShowAllCases()
        {
            BkpCaseFiles = new ObservableCollection<CaseFileVM>();
            BkpCaseFiles.AddRange(new KYCBLL().GetAssignedCases());
            PaginationVM.TotalItem = BkpCaseFiles.Count;
            CaseFiles = new ObservableCollection<CaseFileVM>(BkpCaseFiles.Take(PaginationVM.DisplayitemCount));
            PaginationVM.StartIndex = CaseFiles.IsCollectionValid() ? 1 : 0;
            PaginationVM.CurrentIndex = CaseFiles.IsCollectionValid() ? 1 : 0;
            SetDefaultCaseSelection();
            SelectedCaseFile = null;
            IsViewingAllCases = true;
        }

        public bool IsCaseFileCardOpen
        {
            get { return _isCaseFileCardOpen; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isCaseFileCardOpen, (x) => x.IsCaseFileCardOpen); }
        }

        public ICommand ViewCaseCard
        {
            get
            {
                if (_viewCaseCard.IsNull())
                {
                    _viewCaseCard = new DelegateCommand(
                        () =>
                        {
                            AppVariables.KYCViewModel.IsPopupOpen = true;
                            IsCaseFileCardOpen = true;
                            CaseFileCardVM = new CaseFileCardPageVM() { CaseFileDetails = SelectedCaseFile };
                            CaseFileCardVM.OnCaseCardClosed = CloseCaseFileCard;

                            if (OnOpenCaseFileCard.IsNotNull())
                            {
                                OnOpenCaseFileCard();
                            }
                        }, () => SelectedCaseFile.IsNotNull()
                        );
                }

                return _viewCaseCard;
            }
        }

        private void CloseCaseFileCard()
        {
            AppVariables.KYCViewModel.IsPopupOpen = false;
            IsCaseFileCardOpen = false;

            if (OnCloseCaseFileCard.IsNotNull())
                OnCloseCaseFileCard();

            ShowAllCases();
        }

        #endregion

        #region Methods
        void CaseManagementPageVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }
        #endregion

        #region Event Handler
        public event PropertyChangedEventHandler PropertyChanged;
        public Action OnOpenCaseFileCard;
        public Action OnCloseCaseFileCard;
        #endregion
    }
}
