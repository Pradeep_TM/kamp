﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Windows.Input;
    using System.Windows.Controls;

    public class RequirementTabVM : INotifyPropertyChanged
    {
        public Action<RequirementVM> OnShowResponseScreen;
        public Action OnReqDetialsResponseSave;
        public Action OnReqDetialsResponseClose;

        public RequirementTabVM()
        {
            PropertyChanged += RequirementTabVM_PropertyChanged;
            //Requirements.CollectionChanged += Requirements_CollectionChanged;
        }

        void Requirements_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                //(sender as RequirementVM).OnReqDetialsResponseSave = () => OnReqDetialsResponseSave();
            }
        }

        void RequirementTabVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        private ObservableCollection<RequirementVM> _requirements;

        public ObservableCollection<RequirementVM> Requirements
        {
            get { return _requirements; }
            set { PropertyChanged.HandleValueChange(this, value, ref _requirements, (x) => x.Requirements); }
        }

        //private RequirementVM _selectedReq;

        //public RequirementVM SelectedReq
        //{
        //    get { return _selectedReq; }
        //    set { PropertyChanged.HandleValueChange(this, value, ref _selectedReq, (x) => x.SelectedReq); }
        //}
        

        private ICommand _showResponseDetails;

        public ICommand ShowResponseDetails
        {
            get
            {
                if (_showResponseDetails.IsNull())
                {
                    _showResponseDetails = new DelegateCommand<Button>((btn) =>
                    {
                        var requirement = (btn.Tag as RequirementVM);
                        if (OnShowResponseScreen.IsNotNull())
                            OnShowResponseScreen(requirement);
                    });
                }
                return _showResponseDetails;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsEditable
        {
            get { return AppVariables.CaseFileCardPageVM.IsEditable; }

        }
    }
}
