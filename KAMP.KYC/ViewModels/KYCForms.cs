﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    public enum KYCForms
    {
        ScreenConfiguration = 1,
        Accounts,
        BankData,
        KPMGEditable,
        AssignCases,
    }
}
