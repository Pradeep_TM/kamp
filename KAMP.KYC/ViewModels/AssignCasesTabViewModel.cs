﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.Core.Repository;
    using KAMP.Core.Repository.UM;
    using KAMP.Core.Repository.WF;
    using KAMP.KYC.UserControls;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Core.Workflow;
    using AutoMapper;
    using KAMP.Core.Workflow.Helpers;
    public class AssignCasesTabVM : INotifyPropertyChanged
    {
        #region  Private Properties
        private ObservableCollection<CaseDetails> _caseList;
        private ObservableCollection<CaseDetails> _assignCaseList;
        private ObservableCollection<CaseDetails> _unassignCaseList;
        private List<MasterEntity> _caseCategoryList;
        private List<string> _cases;
        private List<User> _user;
        private long _selecteduser;
        private int _selectedStatus;
        private string _selectedCaseNo;
        private MasterEntity _selectedUnassignedCategory;
        private MasterEntity _selectedAssignedCategory;
        private ICommand _assign;
        private ICommand _reAssign;
        #endregion

        #region Constructor
        public AssignCasesTabVM()
        {
        }

        #endregion

        #region Public Properties
        public long SelectedUser
        {
            get { return _selecteduser; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selecteduser, (x) => x.SelectedUser);
                if (FilterSearch.IsNotNull() && !IsInitialLoad)
                    FilterSearch();
            }
        }

        public int SelectedStatus
        {
            get { return _selectedStatus; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedStatus, (x) => x.SelectedStatus);
                if (FilterSearch.IsNotNull() && !IsInitialLoad)
                    FilterSearch();
            }
        }

        public string SelectedCaseNo
        {
            get { return _selectedCaseNo; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedCaseNo, (x) => x.SelectedCaseNo);
                if (FilterSearch.IsNotNull()  && !IsInitialLoad)
                    FilterSearch();
            }
        }

        public bool IsInitialLoad { get; set; }
        public Action FilterSearchUnassigendCases;
        public Action FilterSearch;



        public ObservableCollection<CaseDetails> CaseList
        {
            get { return _caseList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseList, x => x.CaseList);
            }
        }

        public ObservableCollection<CaseDetails> AssignCaseList
        {
            get { return _assignCaseList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assignCaseList, x => x.AssignCaseList);
            }
        }
        public ObservableCollection<CaseDetails> UnAssignCaseList
        {
            get { return _unassignCaseList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _unassignCaseList, x => x.UnAssignCaseList);
            }
        }

        public ICommand Assign
        {
            get
            {
                if (_assign.IsNull())
                    _assign = new DelegateCommand(() =>
                {

                    AssignReAssignCase(true);
                });
                return _assign;
            }

        }

        public ICommand ReAssign
        {
            get
            {
                if (_reAssign.IsNull())
                    _reAssign = new DelegateCommand(() =>
                    {
                        AssignReAssignCase(false);

                    });
                return _reAssign;
            }

        }

        private CaseDetails _selectedCaseDetail;
        public CaseDetails SelectedCaseDetail
        {
            get { return _selectedCaseDetail; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedCaseDetail, (x) => x.SelectedCaseDetail); }
        }


        public MasterEntity SelectedUnassignedCategory
        {
            get { return _selectedUnassignedCategory; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedUnassignedCategory, (x) => x.SelectedUnassignedCategory);
                if (FilterSearchUnassigendCases.IsNotNull())
                    FilterSearchUnassigendCases();
            }
        }
        public MasterEntity SelectedAssignedCategory
        {
            get { return _selectedAssignedCategory; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedAssignedCategory, (x) => x.SelectedAssignedCategory);
                if (FilterSearch.IsNotNull() && !IsInitialLoad)
                    FilterSearch();
            }
        }


        public List<User> UserList
        {
            get { return _user; }
            set { PropertyChanged.HandleValueChange(this, value, ref _user, (x) => x.UserList); }
        }

        private List<State> _stateList;
        public List<State> StateList
        {
            get { return _stateList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stateList, (x) => x.StateList); }
        }
        public List<MasterEntity> CaseCategoryList
        {
            get { return _caseCategoryList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseCategoryList, (x) => x.CaseCategoryList); }
        }
        public List<string> Cases
        {
            get { return _cases; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cases, (x) => x.Cases); }
        }

        private PaginationVM _assignPaginationVM;
        public PaginationVM AssignPaginationVM
        {
            get { return _assignPaginationVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assignPaginationVM, (x) => x.AssignPaginationVM); }
        }
        private PaginationVM _UnAssignPaginationVM;
        public PaginationVM UNAssignPaginationVM
        {
            get { return _UnAssignPaginationVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _UnAssignPaginationVM, (x) => x.UNAssignPaginationVM); }
        }
        #endregion

        #region Property Change Interface Implemantation
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region private method
        private void AssignReAssignCase(bool isAssign)
        {

            if (SelectedCaseDetail == null)
            {
                MessageBoxControl.Show("Please select a Case.", MessageBoxButton.OK, MessageType.Error);
                return;
            }

            if(SelectedCaseDetail.Status.Trim().ToLower().Equals(Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower()))
            {
                MessageBoxControl.Show("The case has been closed.", MessageBoxButton.OK, MessageType.Information);
                return;
            }
       
            Grid gd = new Grid();
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .5;
            gd.Children.Add(lbl);
            var ucAssignReAssign = new UCAssignReassign(isAssign, SelectedCaseDetail, RefreshAssignCollection, RefreshUNAssignCollection, gd);
            gd.Children.Add(ucAssignReAssign);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(gd);
            }
        }
        private void RefreshAssignCollection(int cfid,string CaseNo,long userId)
        {
            var cases = CaseList.FirstOrDefault(x => x.CfId == cfid && x.CaseNumber.Trim().ToLower().Equals(CaseNo.Trim().ToLower()));
            var caseDetail = WorkflowAPI.GetCaseDetails(cases.CaseNumber);
            cases = Mapper.Map<CaseDetail, CaseDetails>(caseDetail, cases);
            FilterSearch();
        }
        private void RefreshUNAssignCollection(int cfid, string CaseNo, long userId)
        {
            var cases = CaseList.FirstOrDefault(x => x.CfId==cfid && x.CaseNumber.Trim().ToLower().Equals(CaseNo.Trim().ToLower()));
            var caseDetail =WorkflowAPI.GetCaseDetails(cases.CaseNumber);
            cases = Mapper.Map<CaseDetail, CaseDetails>(caseDetail, cases);

            #region Updating cases dropdownlist
            IsInitialLoad = true;
            Cases = CaseList.Where(x => x.UserId != 0 || x.Status.Equals(Helper.Status.End)).Select(x => x.CaseNumber).ToList();
            Cases.Insert(0, KYCConstants.DefaultDropDownValue);
            SelectedCaseNo = Cases.FirstOrDefault();
            IsInitialLoad = false;
            #endregion

            FilterSearchUnassigendCases();
            FilterSearch();
        }
        #endregion
    }
}
