﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    public class ConfigurableScreen : INotifyPropertyChanged
    {
        public ConfigurableScreen()
        {
            PropertyChanged += ConfigurableScreen_PropertyChanged;
        }

        void ConfigurableScreen_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        public int FormId { get; set; }
        public string ScreenName { get; set; }
        public string TableName { get; set; }
        public bool NeedGrouping { get; set; }
        public bool ShowDetailsIcon { get; set; }
        public bool AllowAdding { get; set; }
        public bool NeedMandatoryField { get; set; }
        public bool NeedToolTipCol { get; set; }
        public Func<List<FormElementVM>> FieldDataProvider { get; set; }
        
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
