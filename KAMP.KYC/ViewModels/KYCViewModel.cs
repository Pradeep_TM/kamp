﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using Core.FrameworkComponents.ViewModels;
    using System.Collections.ObjectModel;
    using System.ComponentModel;

    public class KYCViewModel : INotifyPropertyChanged
    {
        public KYCViewModel()
        {
            FillNavigationViewModel();
            PropertyChanged += KYCViewModel_PropertyChanged;
        }

        void KYCViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        private string _currentPageName;

        public string CurrentPageName
        {
            get { return _currentPageName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _currentPageName, (x) => x.CurrentPageName); }
        }

        private HeaderViewModel _headerViewModel;

        public HeaderViewModel HeaderViewModel
        {
            get { return _headerViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _headerViewModel, (x) => x.HeaderViewModel); }
        }

        private bool _isPopupOpen;

        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isPopupOpen, (x) => x.IsPopupOpen); }
        }

        private NavigationControlViewModel _navigationViewModel;

        public NavigationControlViewModel NavigationViewModel
        {
            get { return _navigationViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _navigationViewModel, (x) => x.NavigationViewModel); }
        }

        private void FillNavigationViewModel()
        {
            NavigationViewModel = new NavigationControlViewModel();
            NavigationViewModel.PinImage = @"../Assets/Images/pin.png";
            NavigationViewModel.UnPinImage = @"../Assets/Images/unpin.png";
            NavigationViewModel.ArrowLeftNavigation = @"../Assets/Images/arrowleftnavigation.png";
            NavigationViewModel.DarkThemeColor = "#FF005B5B";
            NavigationViewModel.ContentBackgroundColor = "#FFDBE4E4";
            NavigationViewModel.LightThemeColor = "#FF079595";

            //NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            //{
            //    TabName = "Dashboard",
            //    Items = new List<NavigationItemModel> 
            //    {
            //        new NavigationItemModel { UserControlName = KYCPageControls.Home, ImageUrl = @"../Assets/Images/kyc.png" }
            //    }
            //});

            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "Configuration",
                Items = new List<NavigationItemModel> 
                {
                    new NavigationItemModel { UserControlName = KYCPageControls.ScreenConfiguration, ImageUrl = @"../Assets/Images/config.png", EntityType = "FormElement" }
                }
            });

            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "Case Management",
                Items = new List<NavigationItemModel> 
                {
                    new NavigationItemModel { UserControlName = KYCPageControls.AssignCases, ImageUrl = @"../Assets/Images/assigncases.png", EntityType = "KYCCaseFile" },
                    new NavigationItemModel { UserControlName = KYCPageControls.CaseManagement, ImageUrl = @"../Assets/Images/casemanagement.png", EntityType = "KYCCaseFile" } 
                }
            });

            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "Requirement",
                Items = new List<NavigationItemModel> 
                {
                    new NavigationItemModel { UserControlName = KYCPageControls.RequirementClassification, ImageUrl = @"../Assets/Images/classification.png", EntityType = "ReqClassification" },
                    new NavigationItemModel { UserControlName = KYCPageControls.CreateRequirement, ImageUrl = @"../Assets/Images/create-req.png", EntityType = "Requirement" },
                    new NavigationItemModel { UserControlName = KYCPageControls.RequirementManagement, ImageUrl = @"../Assets/Images/reqmanagement.png", EntityType = "Requirement" }
                }
            });

            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "Workflow",
                Items = new List<NavigationItemModel> 
                {
                    new NavigationItemModel { UserControlName = KYCPageControls.ManageWorkflow, ImageUrl =  @"../Assets/Images/CreateWorkflow.png", EntityType = "WFDefinition" },
                    new NavigationItemModel { UserControlName = KYCPageControls.ManageState, ImageUrl =  @"../Assets/Images/State.png" , EntityType = "State"},
                     new NavigationItemModel { UserControlName = KYCPageControls.WorkflowMonitoring, ImageUrl =  @"../Assets/Images/monitoring.png" , EntityType = "Assignment"}
                }
            });

            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "System Management",
                Items = new List<NavigationItemModel> 
                {
                   new NavigationItemModel { UserControlName = KYCPageControls.History, ImageUrl = @"../Assets/Images/history.png" , EntityType="LogDetail"},
                   new NavigationItemModel { UserControlName = KYCPageControls.LookUps, ImageUrl = @"../Assets/Images/lookups.png", EntityType = "KYCLookUp" },
                   new NavigationItemModel { UserControlName = KYCPageControls.AuditConfiguration, ImageUrl = @"../Assets/Images/wrench.png", EntityType = "ConfigurationTable" }
                  
                }

            });

            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "Reporting Service",
                Items = new List<NavigationItemModel> 
                {
                   new NavigationItemModel { UserControlName = KYCPageControls.Report, ImageUrl = @"../Assets/Images/analyst.png" , EntityType = "CaseAgingReportDetails"}
                }

            });

            HeaderViewModel = new HeaderViewModel()
            {
                DarkThemeColor = "#FF005B5B",
                ModuleName = "Know Your Customer",
                ImgModule = @"../Assets/Images/kyc_logo.png",
                LightThemeColor="#FF079595"
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
