﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;


namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;

    public class SingleSelectReqDetailsVM : INotifyPropertyChanged, IReqDetails
    {
        public SingleSelectReqDetailsVM()
        {
            PropertyChanged += MultiSelectQnVM_PropertyChanged;
            Options = new ObservableCollection<ReqOptionVM>();
            Options.CollectionChanged += Options_CollectionChanged;
            Options.Add(new ReqOptionVM() {});            
        }

        void Options_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                var newItems = e.NewItems as List<ReqOptionVM>;
                foreach (var item in e.NewItems)
                    (item as ReqOptionVM).OnDelete += DeleteOptions;
            }
        }

        public void DeleteOptions(ReqOptionVM obj)
        {
            Options.Remove(obj);
        }

        void MultiSelectQnVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        private ObservableCollection<ReqOptionVM> _options;
        private ICommand _addOption;

        public ObservableCollection<ReqOptionVM> Options
        {
            get { return _options; }
            set { PropertyChanged.HandleValueChange(this, value, ref _options, x => x.Options); }
        }

        public ICommand AddOption
        {
            get
            {
                if (_addOption.IsNull())
                {
                    _addOption = new DelegateCommand(
                        () => Options.Add(new ReqOptionVM() { OptionName="", OnDelete = DeleteOptions })
                    );
                }

                return _addOption;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public RequirementType RequirementType
        {
            get
            {
                return RequirementType.SingleSelectOption;
            }
        }
    }
}
