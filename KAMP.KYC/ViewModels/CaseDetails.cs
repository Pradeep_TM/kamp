﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.Workflow;
    using Core.FrameworkComponents;
    public class CaseDetails : INotifyPropertyChanged
    {
        #region Private Properties
        private int _cfId;
        private DateTime? _createDate;
        private bool? _isOpen;
        private DateTime? _levelOpenDate;
        private DateTime? _caseOpenDate;
        private DateTime? _closeDate;
        private DateTime? _parkDate;
        private DateTime? _assignDate;
        private int? _averageScore;
        private int? _daysInQueue;
        private string _highestStatus;
        private string _caseNumber;
        private string _caseName;
        private int? _caseScore;
        private string _anlyst;
        private int? _transactionCount;
        private int? _transactionScore;
        private int? _accountCount;
        private int? _accountScore;
        private string _stateName;
        private int _stateId;
        private string _userName;
        private long _userId;
        private string _categoryName;
        private int _categoryId;
        private string _requestStatus;
        private string _remediationPapulation;
        private string _customerNumber;
        private string _customerName;
        private string _comments;
        private string _parkReason;

        private bool _isPark;
        #endregion
        #region Public Properties

        public bool? IsOpen
        {
            get { return _isOpen; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isOpen, x => x.IsOpen);
            }
        }

        public DateTime? CloseDate
        {
            get { return _closeDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _closeDate, x => x.CloseDate);
            }
        }

        public DateTime? LevelOpenDate
        {
            get { return _levelOpenDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _levelOpenDate, x => x.LevelOpenDate);
            }
        }
        public DateTime? CaseOpenDate
        {
            get { return _caseOpenDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _levelOpenDate, x => x.CaseOpenDate);
            }
        }

        public DateTime? CreateDate
        {
            get { return _createDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createDate, x => x.CreateDate);
            }
        }

        public DateTime? ParkDate
        {
            get { return _parkDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _parkDate, x => x.ParkDate);
            }
        }

        public DateTime? AssignDate
        {
            get { return _assignDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assignDate, x => x.AssignDate);
            }
        }

        public int? DaysInQueue
        {
            get { return _daysInQueue; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _daysInQueue, x => x.DaysInQueue);
            }
        }

        public string HighestStatus
        {
            get { return _highestStatus; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _highestStatus, x => x.HighestStatus);
            }
        }

        public int? AverageScore
        {
            get { return _averageScore; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _averageScore, x => x.AverageScore);
            }
        }

        public int? CaseScore
        {
            get { return _caseScore; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseScore, x => x.CaseScore);
            }
        }

        public int CfId
        {
            get { return _cfId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _cfId, x => x.CfId);
            }
        }

        public string CaseName
        {
            get { return _caseName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseName, x => x.CaseName);
            }
        }

        public string Analyst
        {
            get { return _anlyst; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _anlyst, x => x.Analyst);
            }
        }

        public int CategoryId
        {
            get { return _categoryId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _categoryId, x => x.CategoryId);
            }
        }
        public string CategoryName
        {
            get { return _categoryName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _categoryName, x => x.CategoryName);
            }
        }

        public int? TransactionCount
        {
            get { return _transactionCount; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _transactionCount, x => x.TransactionCount);
            }
        }
        public int? TransactionScore
        {
            get { return _transactionScore; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _transactionScore, x => x.TransactionScore);
            }
        }

        public int? AccountCount
        {
            get { return _accountCount; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _accountCount, x => x.AccountCount);
            }
        }
        public int? AccountScore
        {
            get { return _accountScore; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _accountScore, x => x.AccountScore);
            }
        }

        public string Statename
        {
            get { return _stateName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _stateName, x => x.Statename);
            }
        }


        public int StateId
        {
            get { return _stateId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _stateId, x => x.StateId);
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _status, x => x.Status);
            }
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userName, x => x.UserName);
            }
        }

        public long UserId
        {
            get { return _userId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userId, x => x.UserId);
            }
        }

        public string CaseNumber
        {
            get { return _caseNumber; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseNumber, x => x.CaseNumber);
            }
        }
        public string RequestStatus
        {
            get { return _requestStatus; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _requestStatus, x => x.RequestStatus);
            }
        }
        public string RemediationPapulation
        {
            get { return _remediationPapulation; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _remediationPapulation, x => x.RemediationPapulation);
            }
        }
        public string CustomerNumber
        {
            get { return _customerNumber; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _customerNumber, x => x.CustomerNumber);
            }
        }
        public string CustomerName
        {
            get { return _customerName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _customerName, x => x.CustomerName);
            }
        }
        public bool IsPark
        {
            get { return _isPark; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isPark, x => x.IsPark);
            }
        }

        public string Comments
        {
            get { return _comments; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _comments, x => x.Comments);
            }
        }

        public string ParkReason
        {
            get { return _parkReason; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _parkReason, x => x.ParkReason);
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;



        public string _status;
    }
}
