﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using AutoMapper;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.KYC.BLL;
    using KAMP.KYC.AppCode;
    using KAMP.Core.Repository.KYC;
    using KAMP.KYC.UserControls;
    using KAMP.Core.Repository;

    public class ScreenConfigurationPageVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Action ScreenSelectionChanged;

        public ScreenConfigurationPageVM()
        {
            PropertyChanged += ScreenConfigurationVM_PropertyChanged;
            Initialize();
        }

        private void Initialize()
        {
            new BootStrapper().Configure();
            ConfigurableScreens = new ReadOnlyCollection<ConfigurableScreen>(new List<ConfigurableScreen>() 
            {
                new ConfigurableScreen(){ ScreenName = KYCConstants.DefaultDropDownValue },
                new ConfigurableScreen(){ ScreenName = KYCScreenConfigConstants.Accounts,
                    FieldDataProvider = () => { return new KYCBLL().GetTableSchema(Convert.ToInt32(KYCForms.Accounts), "KYC_Accounts"); }, FormId = Convert.ToInt32(KYCForms.Accounts) },
                new ConfigurableScreen(){ ScreenName = KYCScreenConfigConstants.BankData, NeedToolTipCol = true,
                    FieldDataProvider = () => { return new KYCBLL().GetTableSchema(Convert.ToInt32(KYCForms.BankData), "KYC_BankData"); }, FormId  = Convert.ToInt32(KYCForms.BankData), NeedGrouping = true },                
                new ConfigurableScreen(){ ScreenName = KYCScreenConfigConstants.KPMGEditable, FormId = Convert.ToInt32(KYCForms.KPMGEditable), NeedToolTipCol = true,
                FieldDataProvider = () => { return new KYCBLL().GetTableSchema(Convert.ToInt32(KYCForms.KPMGEditable), string.Empty, true); }, AllowAdding = true, NeedGrouping = true, NeedMandatoryField = true, ShowDetailsIcon = true}                
            });

            SelectedScreen = ConfigurableScreens[0];

            FormElements = new ObservableCollection<FormElementVM>();
            FormElements.CollectionChanged += FormElements_CollectionChanged;
            GroupNames = new ObservableCollection<string>(FormElements.Select(x => x.GroupName).Distinct());
            GroupNames.Insert(0, KYCConstants.DefaultDropDownValue);
            GroupNames.CollectionChanged += GroupNames_CollectionChanged;

            UnGroupedFormElements = new ObservableCollection<FormElementVM>();
            GroupedFormElements = new ObservableCollection<FormElementVM>();
            UnGroupedFormElements.CollectionChanged += FormElements_CollectionChanged;
            GroupedFormElements.CollectionChanged += FormElements_CollectionChanged;

            ValueTypes = new ObservableCollection<MasterEntity>() 
            {
                new MasterEntity() { Code = (int)RequirementType.FreeText, Name = "Free Text" },
                new MasterEntity() { Code = (int)RequirementType.SingleSelectOption, Name = "Single select option" },
                new MasterEntity() { Code = (int)RequirementType.MultiSelectOption, Name = "Multi select option" },
            };

            FormElement = new FormElementVM();
        }

        void UnGroupedFormElements_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
        }

        void GroupNames_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
        }

        void FormElements_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (var item in e.NewItems)
                {
                    var formEle = (item as FormElementVM);
                    formEle.OnSelectionChanged = (fe) => CommandManager.InvalidateRequerySuggested();
                }
            }

            PropertyChanged.Raise(this, (x) => x.FECollectionView);
        }

        private bool _isFormElementsVisible;

        public bool IsFormElementsVisible
        {
            get { _isFormElementsVisible = FormElements.IsCollectionValid(); return _isFormElementsVisible; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isFormElementsVisible, (x) => x.IsFormElementsVisible); }
        }

        private ICollectionView _feCollectionView;

        public ICollectionView FECollectionView
        {
            get { return _feCollectionView; }
            set { PropertyChanged.HandleValueChange(this, value, ref _feCollectionView, (x) => x.FECollectionView); }
        }

        private ConfigurableScreen _selectedScreen;
        private ObservableCollection<FormElementVM> _formElements;

        public ConfigurableScreen SelectedScreen
        {
            get { return _selectedScreen; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _selectedScreen, (x) => x.SelectedScreen))
                {
                    if (GroupNames.IsCollectionValid())
                        GroupNames.Clear();
                    if (GroupedFormElements.IsCollectionValid())
                        GroupedFormElements.Clear();
                    if (FormElements.IsCollectionValid())
                        FormElements.Clear();
                    RefreshUngroupedElements();


                    if (SelectedScreen.ScreenName != KYCConstants.DefaultDropDownValue)
                    {
                        var formElements = SelectedScreen.FieldDataProvider();
                        FormElements.AddRange(formElements);
                        RefreshUngroupedElements();

                        GroupNames.Insert(0, KYCConstants.DefaultDropDownValue);
                        GroupNames.AddRange(_formElements.Where(g => g.GroupName.IsNotEmpty()).Select(x => x.GroupName).OrderBy(x => x).Distinct());
                    }

                    if (ScreenSelectionChanged.IsNotNull())
                    {
                        ScreenSelectionChanged();
                    }

                    PropertyChanged.Raise(this, x => x.IsFormElementsVisible);
                }
            }
        }

        private void RefreshUngroupedElements()
        {
            if (UnGroupedFormElements.IsCollectionValid())
                UnGroupedFormElements.Clear();

            if (_formElements.IsCollectionValid())
            {
                var unGrpdEle = _formElements.Where(x => x.GroupName.IsEmpty()).ToList();
                if (unGrpdEle.IsCollectionValid())
                    UnGroupedFormElements.AddRange(unGrpdEle);
            }

        }

        public ObservableCollection<FormElementVM> FormElements
        {
            get { return _formElements; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _formElements, (x) => x.FormElements))
                {
                    PropertyChanged.Raise(this, (x) => x.FECollectionView);
                }
            }
        }

        private ReadOnlyCollection<ConfigurableScreen> _configurableScreens;

        public ReadOnlyCollection<ConfigurableScreen> ConfigurableScreens
        {
            get { return _configurableScreens; }
            set { PropertyChanged.HandleValueChange(this, value, ref _configurableScreens, (x) => x.ConfigurableScreens); }
        }

        private ObservableCollection<MasterEntity> _valueTypes;

        public ObservableCollection<MasterEntity> ValueTypes
        {
            get { return _valueTypes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _valueTypes, (x) => x.ValueTypes); }
        }

        private ICommand _save;

        public ICommand Save
        {
            get
            {
                if (_save.IsNull())
                {
                    new BootStrapper().Configure();
                    _save = new DelegateCommand(() =>
                    {
                        new KYCBLL().SaveConfiguration(FormElements.ToList());
                        MessageBoxControl.Show("Screen Configuration for {0} saved successfully".ToFormat(SelectedScreen.ScreenName), type: MessageType.Information);
                    },
                    () => { return SelectedScreen.ScreenName != KYCConstants.DefaultDropDownValue && FormElements.IsCollectionValid(); });
                }

                return _save;
            }
        }

        private ICommand _initiateAddNewColumn;

        public ICommand InitiateAddNewColumn
        {
            get
            {
                if (_initiateAddNewColumn.IsNull())
                {
                    _initiateAddNewColumn = new DelegateCommand(() => OpenAddNewColumn = true);
                    FormElement.IsValid = true;
                }

                return _initiateAddNewColumn;
            }
        }

        private ICommand _addNewColumn;

        public ICommand AddNewColumn
        {
            get
            {
                if (_addNewColumn.IsNull())
                {
                    _addNewColumn = new DelegateCommand(() =>
                    {

                        try
                        {
                            FormElement.FormId = SelectedScreen.FormId;

                            if (FormElement.ValueType == (int)RequirementType.FreeText)
                                FormElement.Options = new ObservableCollection<FormElementOptionVM>();

                            var savedFormEle = new KYCBLL().AddNewFormElement(FormElement);
                            FormElements.Add(savedFormEle);
                            FormElement = new FormElementVM() { IsValid = true };
                            OpenAddNewColumn = AppVariables.KYCViewModel.IsPopupOpen = false;
                            RefreshUngroupedElements();
                            MessageBoxControl.Show("Column added successfully.", type: MessageType.Information);
                        }
                        catch(BusinessException ex)
                        {
                            OpenAddNewColumn = AppVariables.KYCViewModel.IsPopupOpen = false;
                            MessageBoxControl.Show(ex.Message, type: MessageType.Error);
                            OpenAddNewColumn = AppVariables.KYCViewModel.IsPopupOpen = true;
                        }
                    },
                    () => { return FormElement.IsNotNull() && FormElement.Key.IsNotEmpty() && FormElement.DisplayName.IsNotEmpty() && FormElement.ValueType != 0 && (!FormElement.NeedOptions || FormElement.Options.IsCollectionValid()); });
                }

                return _addNewColumn;
            }
        }

        //private List<FormElementVM> _ungroupedElements;

        //public List<FormElementVM> UngroupedElements
        //{
        //    get { return _ungroupedElements; }
        //    set { PropertyChanged.HandleValueChange(this, value, ref _ungroupedElements, (x) => x.UngroupedElements); }
        //}

        private ObservableCollection<FormElementVM> _groupedFormElements;

        public ObservableCollection<FormElementVM> GroupedFormElements
        {
            get
            {
                //_groupedFormElements.Clear();
                //_groupedFormElements.AddRange(FormElements.Where(x => x.GroupName == GroupName).ToList());
                return _groupedFormElements;
            }
            set { PropertyChanged.HandleValueChange(this, value, ref _groupedFormElements, (x) => x.GroupedFormElements); }
        }

        private ObservableCollection<FormElementVM> _unGroupedFormElements;

        public ObservableCollection<FormElementVM> UnGroupedFormElements
        {
            get
            {
                //_unGroupedFormElements.Clear();
                //_unGroupedFormElements.AddRange(FormElements.Where(x => x.GroupName.IsEmpty()).ToList());
                return _unGroupedFormElements;
            }
            set { PropertyChanged.HandleValueChange(this, value, ref _unGroupedFormElements, (x) => x.GroupedFormElements); }
        }

        private ObservableCollection<string> _groupNames;

        public ObservableCollection<string> GroupNames
        {
            get { return _groupNames; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _groupNames, (x) => x.GroupNames);
            }
        }

        private string _groupName;

        public string GroupName
        {
            get { return _groupName; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _groupName, (x) => x.GroupName))
                {

                    GroupedFormElements.Clear();

                    if (GroupName != KYCConstants.DefaultDropDownValue)
                    {
                        var grpdElements = _formElements.Where(x => x.GroupName == GroupName).ToList();
                        if (grpdElements.IsCollectionValid())
                            GroupedFormElements.AddRange(grpdElements);
                    }
                }
            }
        }

        private string _newGroupName;

        public string NewGroupName
        {
            get { return _newGroupName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _newGroupName, (x) => x.NewGroupName);
            }
        }

        private bool _openAddNewColumn;
        public bool OpenAddNewColumn
        {
            get { return _openAddNewColumn; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _openAddNewColumn, (x) => x.OpenAddNewColumn))
                {
                    AppVariables.KYCViewModel.IsPopupOpen = value;
                }
            }
        }

        private FormElementVM _formElement;

        public FormElementVM FormElement
        {
            get { return _formElement; }
            set { PropertyChanged.HandleValueChange(this, value, ref _formElement, (x) => x.FormElement); }
        }

        private ICommand _addGroup;

        public ICommand AddGroup
        {
            get
            {
                if (_addGroup.IsNull())
                {
                    _addGroup = new DelegateCommand(() =>
                    {
                        GroupNames.Add(NewGroupName);
                        GroupNames.RemoveAt(0);
                        GroupNames = new ObservableCollection<string>(GroupNames.OrderBy(x => x));
                        GroupNames.Insert(0, KYCConstants.DefaultDropDownValue);
                        PropertyChanged.Raise(this, (x) => x.GroupNames);
                        MessageBoxControl.Show("Group name - {0} added successfully".ToFormat(NewGroupName), type: MessageType.Information);
                        NewGroupName = string.Empty;
                    },
                    () => { return NewGroupName.IsNotEmpty() && !(GroupNames.IsCollectionValid() && GroupNames.Contains(NewGroupName)); });
                }

                return _addGroup;
            }
        }

        private ICommand _addElement;

        public ICommand AddElement
        {
            get
            {
                if (_addElement.IsNull())
                {
                    _addElement = new DelegateCommand(
                        () =>
                        {
                            var selectedEle = UnGroupedFormElements.Where(x => x.IsSelected).ToList();
                            selectedEle.ForEach(x =>
                            {
                                UnGroupedFormElements.Remove(x);
                                GroupedFormElements.Add(x);
                                x.GroupName = GroupName;
                            });
                        },
                        () => UnGroupedFormElements.IsCollectionValid() && GroupName.IsNotEmpty() && UnGroupedFormElements.Any(x => x.IsSelected)
                    );
                }
                return _addElement;
            }
        }

        private ICommand _closePopup;

        public ICommand ClosePopup
        {
            get
            {
                if (_closePopup.IsNull())
                {
                    _closePopup = new DelegateCommand(
                        () =>
                        {
                            OpenAddNewColumn = AppVariables.KYCViewModel.IsPopupOpen = false;
                            FormElement = new FormElementVM() { IsValid = true };
                        }
                        );
                }

                return _closePopup;
            }
        }

        private ICommand _removeElement;

        public ICommand RemoveElement
        {
            get
            {
                if (_removeElement.IsNull())
                {
                    _removeElement = new DelegateCommand(
                        () =>
                        {
                            var selectedEle = GroupedFormElements.Where(x => x.IsSelected).ToList();
                            selectedEle.ForEach(x =>
                            {
                                GroupedFormElements.Remove(x);
                                UnGroupedFormElements.Add(x);
                                x.GroupName = null;
                            });
                        },
                        () => GroupedFormElements.IsCollectionValid() && GroupName.IsNotEmpty() && GroupedFormElements.Any(x => x.IsSelected)
                    );
                }

                return _removeElement;
            }
        }

        void ScreenConfigurationVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }
    }
}
