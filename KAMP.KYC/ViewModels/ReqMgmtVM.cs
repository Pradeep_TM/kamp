﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.KYC.UserControls;
    using KAMP.KYC.Views;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    public class ReqMgmtVM : INotifyPropertyChanged
    {
        public Action LoadCreateReq;

        private ObservableCollection<ReqClassificationVM> _reqClassification;

        public ObservableCollection<ReqClassificationVM> ReqClassification
        {
            get { return _reqClassification; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqClassification, (x) => x.ReqClassification); }
        }

        private ICommand _viewDetail;

        public ICommand ViewDetail
        {
            get
            {
                if (_viewDetail.IsNull())
                    _viewDetail = new DelegateCommand<long>((id) =>
                        {
                            _gd = new Grid();
                            Label lbl = new Label();
                            lbl.Background = Brushes.Black;
                            lbl.Opacity = .5;
                            _gd.Children.Add(lbl);
                            AppVariables.KYCViewModel.IsPopupOpen = false;
                            var ucReqDetail = new UCReqDetails(id, Close);
                            _gd.Children.Add(ucReqDetail);
                            AppVariables.MainGrid.Children.Add(_gd);
                        });
                return _viewDetail;
            }
        }

        private ICommand _editDetail;

        public ICommand EditDetail
        {
            get
            {
                if (_editDetail.IsNull())
                    _editDetail = new DelegateCommand<RequirementVM>((req) =>
                    {
                        AppVariables.RequirementToEdit = req.Id;

                        if (LoadCreateReq.IsNotNull())
                            LoadCreateReq();
                        //Show Req screen.
                    });
                return _editDetail;
            }
        }

        private ICommand _createReq;

        public ICommand CreateReq
        {
            get
            {
                if (_createReq.IsNull())
                    _createReq = new DelegateCommand<RequirementVM>((req) =>
                    {
                        if (LoadCreateReq.IsNotNull())
                            LoadCreateReq();
                    });
                return _createReq;
            }
        }

        private Grid _gd;
        public void Close()
        {
            if (_gd.IsNotNull())
                AppVariables.MainGrid.Children.Remove(_gd);
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
