﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;

    public class BankDataPageVM : INotifyPropertyChanged
    {
        public BankDataPageVM()
        {
            _formEleGrps = new ObservableCollection<FormElementGroupVM>();
            PropertyChanged += BankDataVM_PropertyChanged;
        }

        void BankDataVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        private ObservableCollection<FormElementGroupVM> _formEleGrps;

        public ObservableCollection<FormElementGroupVM> FormEleGrps
        {
            get { return _formEleGrps; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _formEleGrps, (x) => x.FormEleGrps))
                {
                   // FormEleGrpsLeft = new ObservableCollection<FormElementGroupVM>(FormEleGrps.Where((x, i) => i % 2 == 0));
                   // FormEleGrpsRight = new ObservableCollection<FormElementGroupVM>(FormEleGrps.Where((x, i) => i % 2 == 1));
                }
            }
        }

        public ObservableCollection<FormElementGroupVM> FormEleGrpsLeft
        {
            get { return new ObservableCollection<FormElementGroupVM>(FormEleGrps.Where((x, i) => i % 2 == 0)); ; }
        }

        public ObservableCollection<FormElementGroupVM> FormEleGrpsRight
        {
            get { return new ObservableCollection<FormElementGroupVM>(FormEleGrps.Where((x, i) => i % 2 == 1)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
