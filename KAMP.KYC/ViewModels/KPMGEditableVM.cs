﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;

    public partial class KPMGEditableVM : INotifyPropertyChanged
    {
        public KPMGEditableVM()
        {
            PropertyChanged += KPMGEditable_PropertyChanged;
        }

        void KPMGEditable_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        public long Id { get; set; }
        public int cf_ID { get; set; }

        private string _value;

        public string Value
        {
            get { return _value; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _value, (x) => x.Value))
                {
                    IsUpdated = true;
                }
            }
        }

        private FormElementVM _formElement;
        public FormElementVM FormElement
        {
            get { return _formElement; }
            set { PropertyChanged.HandleValueChange(this, value, ref _formElement, (x) => x.FormElement); }
        }

        public bool IsTextColumn
        {
            get { return FormElement.ValueType == (int)RequirementType.FreeText; }
        }

        public bool IsSingleSelectColumn
        {
            get
            {
                return FormElement.ValueType == (int)RequirementType.SingleSelectOption;
            }
        }

        public bool IsMultiSelectColumn
        {
            get
            {
                return FormElement.ValueType == (int)RequirementType.MultiSelectOption;
            }
        }

        private bool _isUpdated;

        public bool IsUpdated
        {
            get { return _isUpdated; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isUpdated, (x) => x.IsUpdated); }
        }

        private FormElementOptionVM _formElementOption;
        public FormElementOptionVM FormElementOption
        {
            get { return _formElementOption; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _formElementOption, (x) => x.FormElementOption))
                {
                    IsUpdated = true;
                }
            }
        }

        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int GroupId { get; set; }
        public Nullable<int> FormElementId { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class KPMGEditableGrpVM : INotifyPropertyChanged
    {
        public KPMGEditableGrpVM()
        {
            PropertyChanged += KPMGEditableGrpVM_PropertyChanged;
        }

        private ObservableCollection<KPMGEditableVM> _kpmgEditables;

        public ObservableCollection<KPMGEditableVM> KPMGEditables
        {
            get { return _kpmgEditables; }
            set { PropertyChanged.HandleValueChange(this, value, ref _kpmgEditables, (x) => x.KPMGEditables); }
        }

        private string _groupName;

        public string GroupName
        {
            get { return _groupName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _groupName, (x) => x.GroupName); }
        }

        void KPMGEditableGrpVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

