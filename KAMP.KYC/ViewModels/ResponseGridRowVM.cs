﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Windows.Input;
    using KAMP.KYC.UserControls;

    public class ResponseGridRowVM : INotifyPropertyChanged
    {
        public void ResponseGridCellVM()
        {
            PropertyChanged += ResponseGridRowVM_PropertyChanged;
        }

        public Action<ResponseGridRowVM> OnKPMGEditableRowDelete;

        private int _rowId;

        public int RowId
        {
            get { return _rowId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rowId, (x) => x.RowId); }
        }

        private ObservableCollection<ResponseGridCellVM> _responseGridCells;

        public ObservableCollection<ResponseGridCellVM> ResponseGridCells
        {
            get { return _responseGridCells; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responseGridCells, (x) => x.ResponseGridCells); }
        }

        private ICommand _deleteRow;

        public ICommand DeleteRow
        {
            get
            {
                if (_deleteRow.IsNull())
                {
                    _deleteRow = new DelegateCommand
                        (
                        () =>
                        {
                            if (MessageBoxControl.Show("Are you sure, you want to delete this row?", messageBoxButton: System.Windows.MessageBoxButton.OKCancel, type: MessageType.Alert) == System.Windows.MessageBoxResult.OK)
                            {
                                if (OnKPMGEditableRowDelete.IsNotNull())
                                {
                                    OnKPMGEditableRowDelete(this);
                                } 
                            }
                            
                        }
                        );
                }
                return _deleteRow;
            }
        }

        void ResponseGridRowVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
