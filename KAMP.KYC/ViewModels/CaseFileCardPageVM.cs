﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using AutoMapper;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.Core.Common.ViewModels;
    using System.Windows.Input;
    using Core.Workflow.ViewModels;
    using Core.Workflow.UserControls;
    using System.Windows.Controls;
    using System.Windows;
    using System.Windows.Media;
    using KAMP.Core.Common.UserControls;
    using System.Collections.ObjectModel;

    using KAMP.Core.Workflow;
    using KAMP.KYC.BLL;
    using KAMP.KYC.UserControls;
    public class CaseFileCardPageVM : INotifyPropertyChanged
    {
        public Action OnCaseCardClosed;

        public CaseFileCardPageVM()
        {
            PropertyChanged += CaseFileCardPageVM_PropertyChanged;
        }

        #region Private Fields
        private Grid wfProcessGrid;
        private CaseFileVM _caseFileDetails;
        private KPMGEditablePageVM _kpmgEditableVM;
        private AccountVM _accountVM;
        private BankDataPageVM _bankDataVM;
        private RequirementTabVM _requirementTabVM;
        private ICommand _closeCaseFileCard;
        private bool _isSingleSelectResponseOpen;
        private bool _isMultipleSelectResponseOpen;
        private bool _isGridResponseOpen;
        private ObservableCollection<ReqClassificationVM> _classifications;
        private bool? _isOpen;
        private ReqClassificationVM _selectedClassification;
        private ICommand _openCategorySelection;
        private ICommand _closeCategorySelection;
        private ObservableCollection<ReqCategoryVM> _selectedCategories;
        private ObservableCollection<ReqCategoryVM> _selectedCategoriesByClsfcn;
        private bool _isCategorySelectionOpen;
        private ICommand _addCategories;
        private ICommand _removeCategories;
        private ICommand _openWfProcess;
        private ICommand _openCase;
        private ICommand _openRFICommand;
        private bool _isChildPopupOpen;
        private ICommand _catSelectionDone;
        private Grid _gd;
        private CaseHistoryVM _caseHistoryViewModel;
        private HistoryVM _historyViewModel;
        private ObservableCollection<ReqClassificationVM> _selectedClassifications;
        private ReqClassificationVM _selClassification;
        #endregion

        #region Properties

        public bool? IsOpen
        {
            get
            {

                return CaseFileDetails.WfDetails.IsOpen;
            }
        }

        #region KYC Remediation
        public ObservableCollection<ReqClassificationVM> Classifications
        {
            get { return _classifications; }
            set { PropertyChanged.HandleValueChange(this, value, ref _classifications, (x) => x.Classifications); }
        }

        /// <summary>
        /// Classification selected to set the Category selection
        /// </summary>
        public ReqClassificationVM SelectedClassification
        {
            get { return _selectedClassification; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _selectedClassification, (x) => x.SelectedClassification))
                {
                    RefreshCategoriesByClsfcn();

                    if (SelectedClassification.IsNotNull() && SelectedClassification.ReqCategories.IsCollectionValid())
                    {
                        var items = SelectedClassification.ReqCategories.Where(x => SelectedCategoriesByClsfcn.Any(c => c.Id == x.Id))
                             .ToList();

                        items.ForEach(x => SelectedClassification.ReqCategories.Remove(x));
                    }

                }
            }
        }

        /// <summary>
        /// List of classifications for which one or more categories are selected.
        /// Used for perviewing the Selected categories.
        /// </summary>
        public ObservableCollection<ReqClassificationVM> SelectedClassifications
        {
            get { return _selectedClassifications; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedClassifications, (x) => x.SelectedClassifications); }
        }

        /// <summary>
        /// Classification Selected in Selected Classifications combobox
        /// Used for previewing the selected categories.
        /// </summary>
        public ReqClassificationVM SelClassification
        {
            get { return _selClassification; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selClassification, (x) => x.SelClassification); }
        }        

        public ObservableCollection<ReqCategoryVM> SelectedCategories
        {
            get { return _selectedCategories; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedCategories, (x) => x.SelectedCategories); }
        }

        public ObservableCollection<ReqCategoryVM> SelectedCategoriesByClsfcn
        {
            get { return _selectedCategoriesByClsfcn; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedCategoriesByClsfcn, (x) => x.SelectedCategoriesByClsfcn); }
        }

        public bool IsCategorySelectionOpen
        {
            get { return _isCategorySelectionOpen; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _isCategorySelectionOpen, (x) => x.IsCategorySelectionOpen))
                {
                    //IsCaseFileCardEnabled = !value;
                }
            }
        }

        public ICommand OpenCategorySelection
        {
            get
            {
                if (_openCategorySelection.IsNull())
                {
                    _openCategorySelection = new DelegateCommand(
                        () =>
                        {
                            IsCategorySelectionOpen = true;

                            ShowPopup(new UCCaseCategory(), this);
                        },
                        () => SelectedClassification.IsNotNull()
                    );
                }
                return _openCategorySelection;
            }
        }

        public ICommand CloseCategorySelection
        {
            get
            {
                if (_closeCategorySelection.IsNull())
                {
                    _closeCategorySelection = new DelegateCommand(
                        () =>
                        {
                            IsCategorySelectionOpen = false;

                            ClosePopup();
                        }
                    );
                }
                return _closeCategorySelection;
            }
        }

        public ICommand AddCategories
        {
            get
            {
                if (_addCategories.IsNull())
                    _addCategories = new DelegateCommand(
                        () =>
                        {
                            if (SelectedClassification.ReqCategories.IsCollectionValid())
                            {
                                var selCategories = SelectedClassification.ReqCategories.Where(x => x.IsSelected).ToList();

                                if (selCategories.IsCollectionValid())
                                {
                                    selCategories.ForEach(x =>
                                    {
                                        x.Classification = SelectedClassification;
                                        SelectedClassification.ReqCategories.Remove(x);
                                    });

                                    if (SelectedCategories.IsNull())
                                    {
                                        SelectedCategories = new ObservableCollection<ReqCategoryVM>();
                                    }

                                    SelectedCategories.AddRange(selCategories);

                                    //Add categories for preview
                                    var classification = SelectedClassifications.FirstOrDefault(x => x.Id == SelectedClassification.Id);
                                    if (classification.IsNull()) 
                                    {
                                        classification = new ReqClassificationVM()
                                        {
                                            ClassificationName = SelectedClassification.ClassificationName,
                                            Id = SelectedClassification.Id
                                        };
                                        classification.ReqCategories = new ObservableCollection<ReqCategoryVM>();
                                        SelectedClassifications.Add(classification);
                                    }
                                    classification.ReqCategories.AddRange(selCategories);
                                    PropertyChanged.Raise(this, x => x.SelectedClassifications);

                                    RefreshCategoriesByClsfcn();
                                }
                            }

                        },
                        () => SelectedClassification.IsNotNull() && SelectedClassification.ReqCategories.IsCollectionValid() && SelectedClassification.ReqCategories.Any(x => x.IsSelected)
                );
                return _addCategories;
            }
        }

        public ICommand RemoveCategories
        {
            get
            {
                if (_removeCategories.IsNull())
                    _removeCategories = new DelegateCommand(
                        () =>
                        {
                            if (SelectedCategories.IsCollectionValid())
                            {
                                var messageBoxResult = KAMP.KYC.UserControls.MessageBoxControl.Show("Removing this category removes all the requirements associated. Do you want to continue?"
                                 , MessageBoxButton.OKCancel, KAMP.KYC.UserControls.MessageType.Alert);

                                if (messageBoxResult == MessageBoxResult.OK)
                                {
                                    var categoriesToRem = SelectedCategoriesByClsfcn.Where(x => x.IsSelected).ToList();

                                    if (categoriesToRem.IsCollectionValid())
                                    {
                                        categoriesToRem.ForEach(x =>
                                        {
                                            SelectedCategories.Remove(x);
                                        });

                                        if (SelectedClassification.ReqCategories.IsNull())
                                            SelectedClassification.ReqCategories = new ObservableCollection<ReqCategoryVM>();

                                        SelectedClassification.ReqCategories.AddRange(categoriesToRem);

                                        var selClassification = SelectedClassifications.FirstOrDefault(x => x.Id == SelectedClassification.Id);

                                        categoriesToRem.ForEach(x =>
                                        {
                                            var catToRemove = SelectedClassification.ReqCategories.FirstOrDefault(cat => cat.Id == x.Id);
                                            selClassification.ReqCategories.Remove(catToRemove);

                                            if (!selClassification.ReqCategories.IsCollectionValid())
                                                SelectedClassifications.Remove(selClassification);
                                        });

                                        PropertyChanged.Raise(this, x => x.SelectedClassifications);

                                        RefreshCategoriesByClsfcn();
                                    }
                                }
                            }
                        },
                        () => SelectedCategories.IsCollectionValid() && SelectedCategories.Any(x => x.IsSelected)
                        );

                return _removeCategories;
            }
        }

        public ICommand CatSelectionDone
        {
            get
            {
                if (_catSelectionDone.IsNull())
                {
                    _catSelectionDone = new DelegateCommand(
                        () =>
                        {
                            //TODO: Validate for the categories
                            var kycBLL = new KYCBLL();
                            kycBLL.SaveCaseCategories(this);
                            RequirementTabVM.Requirements = kycBLL.GetRequirements(CaseFileDetails.cf_ID);

                            if (RequirementTabVM.Requirements.IsCollectionValid())
                            {
                                foreach (var req in RequirementTabVM.Requirements)
                                {
                                    req.OnReqDetialsResponseSave = RequirementTabVM.OnReqDetialsResponseSave;
                                    req.OnReqDetailsResponseClose = RequirementTabVM.OnReqDetialsResponseClose;
                                }
                            }                                                     

                            IsCategorySelectionOpen = false;
                            ClosePopup();
                        }
                        );
                }
                return _catSelectionDone;
            }
        }

        #endregion

        public CaseFileVM CaseFileDetails
        {
            get { return _caseFileDetails; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseFileDetails, (x) => x.CaseFileDetails); }
        }

        public BankDataPageVM BankDataVM
        {
            get { return _bankDataVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _bankDataVM, (x) => x.BankDataVM); }
        }

        public KPMGEditablePageVM KPMGEditableVM
        {
            get { return _kpmgEditableVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _kpmgEditableVM, (x) => x.KPMGEditableVM); }
        }

        public AccountVM AccountVM
        {
            get { return _accountVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _accountVM, (x) => x.AccountVM); }
        }

        private FileLogsViewModel _fileLogViewModel;
        public FileLogsViewModel FileLogViewModel
        {
            get { return _fileLogViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fileLogViewModel, (x) => x.FileLogViewModel); }
        }

        public RequirementTabVM RequirementTabVM
        {
            get { return _requirementTabVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _requirementTabVM, (x) => x.RequirementTabVM); }
        }

        public CaseHistoryVM CaseHistoryViewModel
        {
            get { return _caseHistoryViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseHistoryViewModel, x => x.CaseHistoryViewModel);
            }
        }

        public HistoryVM HistoryViewModel
        {
            get { return _historyViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _historyViewModel, x => x.HistoryViewModel);
            }

        }

        public bool IsSingleSelectResponseOpen
        {
            get { return _isSingleSelectResponseOpen; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isSingleSelectResponseOpen, (x) => x.IsSingleSelectResponseOpen);
                //IsCaseFileCardEnabled = !value;
            }
        }

        public bool IsMultipleSelectResponseOpen
        {
            get { return _isMultipleSelectResponseOpen; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isMultipleSelectResponseOpen, (x) => x.IsMultipleSelectResponseOpen);
                //IsCaseFileCardEnabled = !value;
            }
        }

        public bool IsGridResponseOpen
        {
            get { return _isGridResponseOpen; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isGridResponseOpen, (x) => x.IsGridResponseOpen);
                //IsCaseFileCardEnabled = !value;
            }
        }

        public bool IsCaseFileCardEnabled
        {
            get { return _isChildPopupOpen; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isChildPopupOpen, (x) => x.IsCaseFileCardEnabled); }
        }

        public ICommand CloseCaseFileCard
        {
            get
            {
                if (_closeCaseFileCard.IsNull())
                {
                    _closeCaseFileCard = new DelegateCommand(
                        () =>
                        {
                            AppVariables.KYCViewModel.IsPopupOpen = false;

                            if (OnCaseCardClosed.IsNotNull())
                                OnCaseCardClosed();

                        });
                }

                return _closeCaseFileCard;
            }
        }

        private RequirementVM _selectedReqVM;

        public RequirementVM SelectedReqVM
        {
            get { return _selectedReqVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedReqVM, (x) => x.SelectedReqVM); }
        }
        private void AssignSingleSelectOption()
        {
            if (SelectedReqVM.IsNotNull())
            {
                var reqOpt = SelectedReqVM.ResponseVM.ResponseOptions.FirstOrDefault();
                if (reqOpt.IsNull())
                    return;
                var reqOptId = reqOpt.ReqOptionVM.Id;
                SelectedReqVM.SelectedOption = (SelectedReqVM.RequirementDetails as SingleSelectReqDetailsVM).Options.FirstOrDefault(x => x.Id == reqOptId);
            }
        }

        private void AssignMultiSelectOption()
        {
            SelectedReqVM.MultiSelectedOptions = new ObservableCollection<ReqOptionVM>();
            SelectedReqVM.ResponseVM.ResponseOptions.Each(x => SelectedReqVM.MultiSelectedOptions.Add(new ReqOptionVM()
            {
                Id = x.ReqOptionVM.Id,
                OptionName = x.ReqOptionVM.OptionName
            }));

            SelectedReqVM.AvailableOptions = new ObservableCollection<ReqOptionVM>((SelectedReqVM.RequirementDetails as MultiSelectReqDetailsVM).Options);
        }
        public void ShowResponseScreen(RequirementVM requirement)
        {
            SelectedReqVM = requirement;

            switch (requirement.ReqType)
            {
                case RequirementType.FreeText:
                    ShowPopup(new UCFreeTextResponse(), this.SelectedReqVM);
                    break;
                case RequirementType.SingleSelectOption:
                    //AppVariables.KYCViewModel.IsPopupOpen = true;

                    ShowPopup(new UCSingleSelectResponse(), this.SelectedReqVM);
                    IsSingleSelectResponseOpen = true;
                    AssignSingleSelectOption();
                    break;
                case RequirementType.MultiSelectOption:
                    //AppVariables.KYCViewModel.IsPopupOpen = true;
                    AssignMultiSelectOption();
                    ShowPopup(new UCMultiSelectResponse(), this.SelectedReqVM);
                    IsMultipleSelectResponseOpen = true;
                    break;
                case RequirementType.Grid:
                    //AppVariables.KYCViewModel.IsPopupOpen = true;
                    //TODO: to be implemented
                    var ucGridResponse = new UCGridResponse();
                    ShowPopup(ucGridResponse, this.SelectedReqVM);
                    ucGridResponse.BindGridDetials(SelectedReqVM);
                    //IsGridResponseOpen = true;
                    break;
                default:
                    break;
            }
        }

        public void ReqDetialsResponseSave()
        {
            CloseResponseDialogs();
        }

        public void CloseResponseDialogs()
        {
            //AppVariables.KYCViewModel.IsPopupOpen = false;
            IsSingleSelectResponseOpen = false;
            IsMultipleSelectResponseOpen = false;
            IsGridResponseOpen = false;

            ClosePopup();
        }

        public ICommand OpenWFProcess
        {
            get
            {
                if (_openWfProcess.IsNull())
                {
                    _openWfProcess = new DelegateCommand(
                        () =>
                        {
                            //Validate promote readyness
                            bool isValid = ValidatePromoteReadyness();

                            if (CaseFileDetails.IsNotNull() && CaseFileDetails.WfDetails.IsNotNull()
                                      && CaseFileDetails.WfDetails.CaseNo.IsNotEmpty() && isValid)
                                OpenWorkflow(CaseFileDetails.WfDetails.CaseNo);
                        }, IsAssignedUser);
                }

                return _openWfProcess;
            }
        }

        private bool ValidatePromoteReadyness()
        {
            var isValid = true;
            if (RequirementTabVM.Requirements.IsCollectionValid())
            {
                if (!RequirementTabVM.Requirements.All(x => x.ResponseVM.IsNotNull() && x.ResponseVM.ResponseState == (int)ResponseState.Answered))
                {
                    KAMP.KYC.UserControls.MessageBoxControl.Show("Please answer the requirements to promote", type: UserControls.MessageType.Error);
                    return false;
                }
            }

            if (RequirementTabVM.Requirements.IsCollectionValid())
            {
                if (!RequirementTabVM.Requirements.All(x => x.ResponseVM.IsNotNull() && x.ResponseVM.ResponseState == (int)ResponseState.Answered))
                {
                    KAMP.KYC.UserControls.MessageBoxControl.Show("Please answer all the requirements to promote.", type: UserControls.MessageType.Error);
                    return false;
                }
            }

            var kpmgEditables = KPMGEditableVM.KPMGEditableGrps.SelectMany(x => x.KPMGEditables).ToList();
            if (kpmgEditables.IsCollectionValid())
            {
                if (!KPMGEditableVM.ValidateKpmgEditables(kpmgEditables))
                    return false;
            }

            if (kpmgEditables.Any(x => x.IsUpdated))
            {
                KAMP.KYC.UserControls.MessageBoxControl.Show("Please save KPMG Editable.", type: UserControls.MessageType.Error);
                return false;
            }

            return isValid;
        }

        public ICommand OpenCase
        {
            get
            {
                if (_openCase.IsNull())
                {
                    _openCase = new DelegateCommand<string>(
                        (caseNo) =>
                        {

                            if (CaseFileDetails.WfDetails.UserId != AppVariables.KAMPPrincipal.UserId)
                            {
                                KAMP.KYC.UserControls.MessageBoxControl.Show("You can not open this case.This case is not assigned to you.", type: KAMP.KYC.UserControls.MessageType.Error);
                                return;
                            }
                            if (caseNo.IsNotEmpty())
                            {
                                try
                                {
                                    WorkflowAPI.OpenCase(caseNo);
                                    KAMP.KYC.UserControls.MessageBoxControl.Show("Case opened Successfully.", type: KAMP.KYC.UserControls.MessageType.Information);
                                    CaseFileDetails.WfDetails.IsOpen = true;
                                    PropertyChanged.Raise(this, (x) => x.IsOpen);
                                    PropertyChanged.Raise(this, (x) => x.AssignedUser);
                                    PropertyChanged.Raise(this, (x) => x.IsEditable);
                                    PropertyChanged.Raise(this.KPMGEditableVM, (x) => x.IsEditable);
                                    PropertyChanged.Raise(this.RequirementTabVM, (x) => x.IsEditable);

                                }
                                catch (BusinessException ex)
                                {
                                    KAMP.KYC.UserControls.MessageBoxControl.Show(ex.Message, type: KAMP.KYC.UserControls.MessageType.Error);
                                }
                            }
                        }
                        );
                }

                return _openCase;
            }
        }

        public ICommand OpenRFICommand
        {
            get
            {
                if (_openRFICommand.IsNull())
                {
                    _openRFICommand = new DelegateCommand(
                        () =>
                        {
                            OpenRFI();
                        }, IsAssignedUser);
                }

                return _openRFICommand;
            }
        }

        public bool IsAssignedUser()
        {

            if (CaseFileDetails.IsNotNull() && CaseFileDetails.WfDetails.IsNotNull() &&
                CaseFileDetails.WfDetails.UserId == AppVariables.KAMPPrincipal.UserId && CaseFileDetails.WfDetails.IsOpen.HasValue && CaseFileDetails.WfDetails.IsOpen.Value)
                return true;
            return false;
        }
        public bool IsEditable
        {
            get
            {
                return IsAssignedUser();
            }
        }
        public bool AssignedUser
        {
            get
            {
                return CaseFileDetails.IsNotNull() && CaseFileDetails.WfDetails.IsNotNull() &&
                CaseFileDetails.WfDetails.UserId == AppVariables.KAMPPrincipal.UserId;
            }
        }
        #endregion

        #region Methods
        void CaseFileCardPageVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }
        private void OpenWorkflow(string caseNo)
        {
            if (wfProcessGrid.IsNotNull())
                return;
            var uc = new UCWorkflowAction();
            var viewModel = new WorkflowActionVm
            {
                CaseNo = caseNo,
            };
            uc.DataContext = viewModel;
            uc.InitializedData();
            viewModel.ClosePopUp += Close;
            viewModel.PostAction += PostAction;
            AppVariables.KYCViewModel.IsPopupOpen = false;
            wfProcessGrid = new Grid();
            wfProcessGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            wfProcessGrid.Children.Add(lbl);
            wfProcessGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(wfProcessGrid);
            }
        }

        private void PostAction(string caseNo)
        {

            if (OnCaseCardClosed.IsNotNull())
                OnCaseCardClosed();

        }

        private void Close()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(wfProcessGrid);
                wfProcessGrid = null;
            }

        }

        private void OpenRFI()
        {
            var context = Application.Current.Properties["Context"] as Context;
            var rfiViewModel = new RFIViewModel { CaseNumber = CaseFileDetails.CaseNo, ModuleId = context.Module.Id, SenderName = context.User.UserName.Split('\\').LastOrDefault(), RFIColorModel = new RFIColorModel { DarkColor = "#FF005B5B", BackGroundColor = "#FFDBE4E4", LightestColor = "#FFD0E8E8", LabelColor = "#FF005B5B", AttachButtonColor = "#FF005B5B" } };
            var uc = new RFIMain(rfiViewModel);
            uc.CloseControl = CloseRFI;
            rFIGrid = new Grid();
            rFIGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            rFIGrid.Children.Add(lbl);
            rFIGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(rFIGrid);
            }
        }

        private void CloseRFI()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(rFIGrid);
            }
        }

        private void ClosePopup()
        {
            if (_gd.IsNotNull())
                AppVariables.MainGrid.Children.Remove(_gd);
            _gd = null;
        }

        private void ShowPopup(UserControl uc, object dataContext)
        {
            if (_gd.IsNotNull())
                return;
            _gd = new Grid();
            Label lbl = new Label();
            Panel.SetZIndex(uc, 101);
            lbl.Background = Brushes.Black;
            lbl.Opacity = .5;
            _gd.Children.Add(lbl);
            uc.DataContext = dataContext;
            _gd.Children.Add(uc);
            AppVariables.MainGrid.Children.Add(_gd);
        }

        public void RefreshCategoriesByClsfcn()
        {
            SelectedCategoriesByClsfcn = new ObservableCollection<ReqCategoryVM>();

            if (SelectedClassification.IsNotNull())
            {
                var categoriesByClsfication = SelectedCategories.Where(x => x.Classification.Id == SelectedClassification.Id).ToList();
                SelectedCategoriesByClsfcn = new ObservableCollection<ReqCategoryVM>(categoriesByClsfication);
            }
        }

        public Grid rFIGrid { get; set; }
        #endregion

        #region Events and delegates
        //public Action<string> TabSelectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
