﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.KYC;

namespace KAMP.KYC.ViewModels
{
 
  public class CaseHistoryVM : INotifyPropertyChanged
    {

        private ObservableCollection<HistoryModel> _caseHistoryList;
        public ObservableCollection<HistoryModel> CaseHistoryList
        {
            get { return _caseHistoryList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseHistoryList, (x) => x.CaseHistoryList); }
        }

        private ObservableCollection<KYCUser> _userList;
        public ObservableCollection<KYCUser> UserList
        {
            get { return _userList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userList, (x) => x.UserList); }
        }

        private int _cfId;
        public int CfId
        {
            get { return _cfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfId, (x) => x.CfId); }
        }


        private string _selectedUser;
        public string SelectedUser
        {
            get { return _selectedUser; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedUser, (x) => x.SelectedUser); }
        }

        private List<string> _entities;
        public List<string> Entities
        {
            get { return _entities; }
            set { PropertyChanged.HandleValueChange(this, value, ref _entities, (x) => x.Entities); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

