﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Collections.ObjectModel;
    using BLL;
    using KAMP.Core.Repository;
    using System.Windows.Input;
    using KAMP.KYC.UserControls;

    public class CreateReqPageVM : INotifyPropertyChanged
    {
        public CreateReqPageVM()
        {
            PropertyChanged += CreateReqPageVM_PropertyChanged;

            InitializeData();
        }

        public Action OnRedirectToReqMgmt;

        #region Private Fields
        private KYCBLL _kycBLL;
        private RequirementVM _reqVM;
        private ObservableCollection<ReqCategoryVM> _reqCategories;
        private ObservableCollection<MasterEntity> _requirementTypes;
        private ICommand _saveRequirement;
        private MasterEntity _requirementType;
        private ObservableCollection<ReqClassificationVM> _reqClassifications;
        private ReqClassificationVM _reqClassificationVm;
        private ObservableCollection<ReqCategoryVM> _reqCategoriesByClsfcn;
        #endregion

        #region Properties
        public RequirementVM ReqVM
        {
            get { return _reqVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqVM, (x) => x.ReqVM); }
        }

        public ReqClassificationVM ReqClassificationVm
        {
            get { return _reqClassificationVm; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _reqClassificationVm, (x) => x.ReqClassificationVm))
                {
                    ReqCategoriesByClsfcn = ReqClassificationVm == null ? ReqCategories : new ObservableCollection<ReqCategoryVM>(ReqCategories.Where(x => x.Classification.Id == _reqClassificationVm.Id && x.IsValid).OrderBy(x => x.CategoryName));
                }
            }
        }

        public ObservableCollection<ReqCategoryVM> ReqCategoriesByClsfcn
        {
            get { return _reqCategoriesByClsfcn; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqCategoriesByClsfcn, (x) => x.ReqCategoriesByClsfcn); }
        }

        public ObservableCollection<ReqCategoryVM> ReqCategories
        {
            get { return _reqCategories; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqCategories, (x) => x.ReqCategories); }
        }

        public ObservableCollection<ReqClassificationVM> ReqClassifications
        {
            get { return _reqClassifications; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqClassifications, (x) => x.ReqClassifications); }
        }

        public ObservableCollection<MasterEntity> RequirementTypes
        {
            get { return _requirementTypes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _requirementTypes, (x) => x.RequirementTypes); }
        }

        public MasterEntity RequirementType
        {
            get { return _requirementType; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _requirementType, (x) => x.RequirementType))
                {
                    //RequirementDetails = typeof(IReqDetails).GetImplementors()
                    //                        .Where(x => x.BaseType != typeof(UserControl))
                    //                        .Select(x => Activator.CreateInstance(x) as IReqDetails)
                    //                        .FirstOrDefault(x => x.RequirementType == ReqType);

                    //PropertyChanged.Raise(this, (x) => x.RequirementDetails);
                    //MessageBoxControl.Show("Finally Im called.");
                }
            }
        }

        public ICommand SaveRequirement
        {
            get
            {
                if (_saveRequirement.IsNull())
                {
                    _saveRequirement = new DelegateCommand(() =>
                    {
                        if (ReqVM.Id != 0)
                        {
                            if (new KYCBLL().CheckIfResponseIsSaved(ReqVM.Id))
                            {
                                MessageBoxControl.Show("Update failed! Response already saved.", type: MessageType.Error);
                                return;
                            }
                        }

                        new KYCBLL().SaveRequirement(ReqVM);
                        MessageBoxControl.Show("Requirement saved successfully.", type: MessageType.Information);

                        if (ReqVM.Id != 0)
                        {
                            if(OnRedirectToReqMgmt.IsNotNull())
                                OnRedirectToReqMgmt();
                        }
                        else
                        {
                            ReqVM.RequirementText = string.Empty;
                            ReqVM.RequirementType = null;
                            ReqVM.Category = null;
                            ReqClassificationVm = null;
                        }
                    }, () =>
                    {
                        if (ReqVM.IsNotNull())
                        {
                            var reqTextValid = ReqVM.RequirementText.IsNotEmpty();
                            bool columnDetailsValid = ValidateRequirementDetails(ReqVM.RequirementDetails);
                            bool requirementTypeValid = ReqVM.RequirementType != null && ReqVM.RequirementType.Code > 0;

                            return reqTextValid && columnDetailsValid && requirementTypeValid;
                        }

                        return false;
                    });
                }

                return _saveRequirement;
            }
        }

        private bool ValidateRequirementDetails(IReqDetails reqDetails)
        {
            bool columnDetailsValid = true;

            if (reqDetails is SingleSelectReqDetailsVM)
            {
                var singleSelectDetails = reqDetails as SingleSelectReqDetailsVM;
                columnDetailsValid = singleSelectDetails.Options.IsCollectionValid() && singleSelectDetails.Options.All(x => x.OptionName.IsNotEmpty());
            }
            else if (reqDetails is MultiSelectReqDetailsVM)
            {
                var multiSelectDetails = reqDetails as MultiSelectReqDetailsVM;
                columnDetailsValid = multiSelectDetails.Options.IsCollectionValid() && multiSelectDetails.Options.All(x => x.OptionName.IsNotEmpty());
            }

            else if (reqDetails is GridReqDetailsVM)
            {
                var gridDetails = reqDetails as GridReqDetailsVM;
                columnDetailsValid = gridDetails.Columns.IsCollectionValid() && gridDetails.Columns.All(x => x.ColumnText.IsNotEmpty() && (x.ColumnContent.IsNull() || ValidateRequirementDetails(x.ColumnContent)));
            }

            return columnDetailsValid;
        }

        //public ICommand SaveRequirement
        //{
        //    get 
        //    {
        //        return new DelegateCommand(() => {}, () => );
        //    }
        //}
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods
        void CreateReqPageVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        private void InitializeData()
        {
            _kycBLL = new KYCBLL();
            var categories = _kycBLL.GetAllCategory();
            ReqCategories = new ObservableCollection<ReqCategoryVM>();

            if (categories.IsCollectionValid())
                ReqCategories.AddRange(categories.Where(x => x.IsValid).OrderBy(x => x.CategoryName));

            var classifications = _kycBLL.GetAllClassification();
            ReqClassifications = new ObservableCollection<ReqClassificationVM>();

            if (classifications.IsCollectionValid())
                ReqClassifications.AddRange(classifications.Where(x => x.IsValid).OrderBy(x => x.ClassificationName));

            RequirementTypes = new ObservableCollection<MasterEntity>();
            RequirementTypes.AddRange(Enum.GetValues(typeof(RequirementType))
                                            .Cast<RequirementType>()
                                            .OrderBy(x => x.GetDisplayName())
                                            .Select(x => new MasterEntity() { Code = (int)x, Name = x.GetDisplayName() }));
            ReqCategoriesByClsfcn = new ObservableCollection<ReqCategoryVM>(ReqCategories);

            ReqVM = new RequirementVM();
        }
        #endregion
    }
}
