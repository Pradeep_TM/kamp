﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    public enum RequirementType
    {
        [Display(Name = "Free Text")]
        FreeText = 1,
        [Display(Name = "Single Select Option")]
        SingleSelectOption,
        [Display(Name = "Multi Select Option")]
        MultiSelectOption,
        [Display(Name = "Grid")]
        Grid
    }
}