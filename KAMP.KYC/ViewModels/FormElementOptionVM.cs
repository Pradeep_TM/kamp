﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.KYC.UserControls;
    using System.Windows;
    using System.Windows.Input;

    public partial class FormElementOptionVM : INotifyPropertyChanged
    {
        public FormElementOptionVM()
        {
            PropertyChanged += FormElementOptionVM_PropertyChanged;
        }

        void FormElementOptionVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        public Action<FormElementOptionVM> OnDelete;
        public Action SelectionChanged;

        private string _optionText;

        public string OptionText
        {
            get { return _optionText; }
            set { PropertyChanged.HandleValueChange(this, value, ref _optionText, (x) => x.OptionText); }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected))
                {
                    if (SelectionChanged.IsNotNull())
                    {
                        SelectionChanged();
                    }
                }
            }
        }

        private ICommand _deleteOption;

        public ICommand DeleteOption
        {
            get
            {
                if (_deleteOption.IsNull())
                {
                    _deleteOption = new DelegateCommand(
                        () =>
                        {
                            if (OnDelete.IsNotNull() && MessageBoxControl.Show("Are you sure you want to delete this option?", messageBoxButton: MessageBoxButton.OKCancel, type: MessageType.Alert) == MessageBoxResult.OK) 
                                OnDelete(this);
                        });
                }

                return _deleteOption;
            }
        }
        

        public int Id { get; set; }
        public int FormElementId { get; set; }         
        public bool Valid { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
