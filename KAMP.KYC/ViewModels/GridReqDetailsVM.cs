﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;


namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;

    public class GridReqDetailsVM : INotifyPropertyChanged, IReqDetails
    {
        public GridReqDetailsVM()
        {
            PropertyChanged += GridQnVM_PropertyChanged;
            Columns = new ObservableCollection<ReqGridColumnVM>();
            ColumnType = ViewModels.RequirementType.FreeText;
            Columns.CollectionChanged += Columns_CollectionChanged;
            //Columns.Add(new ColumnVM());
        }

        void Columns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (var item in e.NewItems)
                    (item as ReqGridColumnVM).OnDelete += DeleteColumns;
            }
        }

        public void DeleteColumns(ReqGridColumnVM obj)
        {
            Columns.Remove(obj);
        }

        void GridQnVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        public List<RequirementType> TypesOfQuestion
        {
            get
            {
                var types = Enum.GetValues(typeof(RequirementType)).Cast<RequirementType>().ToList();
                types.Remove(RequirementType.Grid);
                return types;
            }
        }

        private ObservableCollection<ReqGridColumnVM> _columns;

        public ObservableCollection<ReqGridColumnVM> Columns
        {
            get { return _columns; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columns, x => x.Columns); }
        }

        private ICommand _addColumn;

        public ICommand AddColumn
        {
            get
            {
                if (_addColumn.IsNull())
                {
                    _addColumn = new DelegateCommand(
                        () =>
                        {
                            var qnContent = typeof(IReqDetails).GetImplementors()
                                                    .Select(x => ((IReqDetails)Activator.CreateInstance(x)))
                                                    .FirstOrDefault(x => x.RequirementType == ColumnType && x.GetType().BaseType != typeof(UserControl));
                            var gridColVm = new ReqGridColumnVM() { ColumnType = ColumnType, ColumnContent = qnContent, OnDelete = DeleteColumns  };
                            Columns.Add(gridColVm);
                        }
                    );
                }

                return _addColumn;
            }
        }

        private RequirementType _columnType;

        public RequirementType ColumnType
        {
            get { return _columnType; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnType, x => x.ColumnType); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public RequirementType RequirementType
        {
            get
            {
                return RequirementType.Grid;
            }
        }
    }
}
