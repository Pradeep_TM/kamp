﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.Repository.UM;
    using Core.FrameworkComponents;
    using System.Windows.Input;
    using System.Windows;
    using KAMP.Core.Workflow;
    using KAMP.KYC.UserControls;
    public class AssignReAssignVM : INotifyPropertyChanged
    {
        public Action Close;
        public Action<int, string, long> OnAssignRefreshGrid;
        public Action<int, string, long> OnUNAssignRefreshGrid;

        private List<User> _users;

        public List<User> Users
        {
            get { return _users; }
            set { PropertyChanged.HandleValueChange(this, value, ref _users, x => x.Users); }
        }

        private long _selectedUserId;

        public long SelectedUserId
        {
            get { return _selectedUserId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedUserId, x => x.SelectedUserId); }
        }


        private bool _isAssign;

        public bool IsAssign
        {
            get { return _isAssign; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isAssign, x => x.IsAssign); }
        }
        private ICommand _assignCommand;

        public ICommand AssignCommand
        {
            get
            {
                if (_assignCommand.IsNull())
                    _assignCommand = new DelegateCommand(() =>
                    {
                        var caseNo = CurrentCase.CaseNumber;
                        var cfId = CurrentCase.CfId;
                        var context = Application.Current.Properties["Context"] as Context;
                        try
                        {
                            if(SelectedUserId==0)
                            {
                                MessageBoxControl.Show("Please select a user.", MessageBoxButton.OK, MessageType.Error);
                                return;
                            }

                            WorkflowAPI.Assign(caseNo, SelectedUserId, cfId, context.Module.Id);
                            MessageBoxControl.Show("Case Successfully Assigned.", MessageBoxButton.OK, MessageType.Information);
                            if (OnUNAssignRefreshGrid.IsNotNull())
                                OnUNAssignRefreshGrid(CurrentCase.CfId,CurrentCase.CaseNumber, SelectedUserId);
                            if (Close.IsNotNull())
                                Close();
                        }
                        catch (BusinessException ex)
                        {
                            MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, MessageType.Error);
                        }
                    });

                return _assignCommand;
            }

        }
        private ICommand _reAssignCommand;

        public ICommand ReAssignCommand
        {
            get
            {
                if (_reAssignCommand.IsNull())
                    _reAssignCommand = new DelegateCommand(() =>
                    {
                        var context = Application.Current.Properties["Context"] as Context;
                        var caseNo = CurrentCase.CaseNumber;
                        var cfId = CurrentCase.CfId;
                        try
                        {
                            if (SelectedUserId == 0)
                            {
                                MessageBoxControl.Show("Please select a user.", MessageBoxButton.OK, MessageType.Error);
                                return;
                            }

                            WorkflowAPI.ReAssign(caseNo, SelectedUserId, context.Module.Id);
                            MessageBoxControl.Show("Case Successfully Re-Assigned.", MessageBoxButton.OK, MessageType.Information);

                            if (OnAssignRefreshGrid.IsNotNull())
                                OnAssignRefreshGrid(CurrentCase.CfId, CurrentCase.CaseNumber, SelectedUserId);

                            if (Close.IsNotNull())
                                Close();
                        }
                        catch (BusinessException ex)
                        {
                            MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, MessageType.Error);
                        }
                    });

                return _reAssignCommand;
            }

        }
        private ICommand _closeCommand;

        public ICommand CloseCommand
        {
            get
            {
                if (_closeCommand.IsNull())
                    _closeCommand = new DelegateCommand(() =>
                    {
                        if (Close.IsNotNull())
                            Close();
                    });

                return _closeCommand;
            }

        }
        public CaseDetails CurrentCase { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
