﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.KYC.BLL;
    using KAMP.KYC.UserControls;

    public class KPMGEditablePageVM : INotifyPropertyChanged
    {
        public KPMGEditablePageVM()
        {
            PropertyChanged += KPMGEditablePageVM_PropertyChanged;
        }

        void KPMGEditablePageVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<KPMGEditableGrpVM> _kpmgEditableGrps;

        public ObservableCollection<KPMGEditableGrpVM> KPMGEditableGrps
        {
            get { return _kpmgEditableGrps; }
            set { PropertyChanged.HandleValueChange(this, value, ref _kpmgEditableGrps, (x) => x.KPMGEditableGrps); }
        }

        public ObservableCollection<KPMGEditableGrpVM> KPMGEditablesGrpsLeft
        {
            get { return new ObservableCollection<KPMGEditableGrpVM>(KPMGEditableGrps.Where((x, i) => i % 2 == 0)); ; }
        }

        

        public bool IsEditable
        {
            get { return AppVariables.CaseFileCardPageVM.IsEditable; }
            
        }
        

        public ObservableCollection<KPMGEditableGrpVM> KPMGEditablesGrpsRight
        {
            get { return new ObservableCollection<KPMGEditableGrpVM>(KPMGEditableGrps.Where((x, i) => i % 2 == 1)); }
        }

        private ICommand _saveKPMGEditable;

        public ICommand SaveKPMGEditable
        {
            get
            {
                if (_saveKPMGEditable.IsNull())
                {
                    _saveKPMGEditable = new DelegateCommand(() =>
                    {
                        var kpmgEditables = KPMGEditableGrps.SelectMany(x => x.KPMGEditables).ToList();
                        //validate whether all mandatory fields are filled.
                        bool isValid = ValidateKpmgEditables(kpmgEditables);

                        if (!isValid)
                        {
                            return;
                        }

                        var _kycBLL = new KYCBLL();
                        _kycBLL.SaveKPMGEditables(kpmgEditables);
                        KPMGEditableGrps = _kycBLL.GetKPMGEditableData(AppVariables.CaseFileCardPageVM.CaseFileDetails.cf_ID);
                        PropertyChanged.Raise(this, (x) => x.KPMGEditablesGrpsLeft);
                        PropertyChanged.Raise(this, (x) => x.KPMGEditablesGrpsRight);
                        MessageBoxControl.Show("KPMG Editable saved successfully", type: MessageType.Information);
                     }, () => { return KPMGEditableGrps.IsCollectionValid(); });
                }

                return _saveKPMGEditable;
            }
        }

        public bool ValidateKpmgEditables(List<KPMGEditableVM> kpmgEditables)
        {
            bool isValid = true;
            var mandatoryFieldsWithErrors = new List<string>();

            if (kpmgEditables.IsCollectionValid())
            {
                
                kpmgEditables.ForEach(x =>
                {
                    bool mandatoryNotFilled = false;
                    
                    if (x.FormElement.IsMandatory)
                    {
                        var fieldType = (RequirementType)x.FormElement.ValueType;

                        switch (fieldType)
                        {
                            case RequirementType.FreeText:
                                mandatoryNotFilled = x.Value.IsEmpty();
                                break;
                            case RequirementType.SingleSelectOption:
                                mandatoryNotFilled = x.FormElementOption.IsNull();
                                break;
                            case RequirementType.MultiSelectOption:
                                mandatoryNotFilled = !x.FormElement.Options.Any(o => o.IsSelected);
                                break;
                        }

                        if (mandatoryNotFilled)
                        {
                            mandatoryFieldsWithErrors.Add(x.FormElement.DisplayName);
                        }
                    }
                    
                });
                
            }            

            if (mandatoryFieldsWithErrors.IsCollectionValid())
            {
                isValid = false;
                MessageBoxControl.Show("Please fill mandatory field(s) in KPMG Editable.\r\n- " + string.Join(", ", mandatoryFieldsWithErrors), type: MessageType.Error);
            }

            return isValid;
        }

    }
}
