﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace KAMP.KYC.ViewModels
{
   using KAMP.Core.FrameworkComponents;
   using KAMP.Core.Repository.KYC;
   
   public class LookUpsVM : INotifyPropertyChanged
    {
        ObservableCollection<KYCLookUpGroups> _lookUpGroup;
        ObservableCollection<KYCLookUp> _lookUps;
        KYCLookUpGroups _selectedGroup;

        public ObservableCollection<KYCLookUpGroups> LookUpGroupList
        {
            get { return _lookUpGroup; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lookUpGroup, (x) => x.LookUpGroupList); }
        }

        public KYCLookUpGroups SelectedGroup
        {
            get { return _selectedGroup; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedGroup, (x) => x.SelectedGroup); }
        }

        public ObservableCollection<KYCLookUp> LookUpsList
        {
            get { return _lookUps; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lookUps, (x) => x.LookUpsList); }
        }

        private string _selectedCategory;
        public string SelectedCategory
        {
            get { return _selectedCategory; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedCategory, (x) => x.SelectedCategory); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
