using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Windows.Input;
    using KAMP.KYC.UserControls;
    using KAMP.KYC.BLL;

    public partial class ResponseVM : INotifyPropertyChanged
    {
        public ResponseVM()
        {
            //RequirementOptionVM = new ReqOptionVM() { OptionName = "New Option" };
        }

        #region private fields
        private string _responseText;
        private short _responseState;
        private ResponseState _responseStatus;
        private ObservableCollection<ResponseOptionVM> _responseOptions;
        private ObservableCollection<ResponseGridRowVM> _responseGridRows;
        private ReqOptionVM _requirementOptionVM;
        #endregion

        
        public long Id { get; set; }
        public int cf_ID { get; set; }

        public string ResponseText
        {
            get { return _responseText; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responseText, (x) => x.ResponseText); }
        }

        public long RequirementId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ResponseJson { get; set; }
        public short ResponseState
        {
            get { return _responseState; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _responseState, (x) => x.ResponseState))
                    ResponseStatus = (ResponseState)ResponseState;
            }
        }

        private string _comment;

        public string Comment
        {
            get { return _comment; }
            set { PropertyChanged.HandleValueChange(this, value, ref _comment, (x) => x.Comment); }
        }

        private string _responsePreview;

        public string ResponsePreview
        {
            get { return _responsePreview; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responsePreview, (x) => x.ResponsePreview); }
        }

        private List<string> _responsePreviewDetials;

        public List<string> ResponsePreviewDetials
        {
            get { return _responsePreviewDetials; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responsePreviewDetials, (x) => x.ResponsePreviewDetials); }
        }

        public ResponseState ResponseStatus
        {
            get { return _responseStatus; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _responseStatus, (x) => x.ResponseStatus))
                {
                    ResponseState = (short)ResponseStatus;                    
                }
            }
        }

        public ObservableCollection<ResponseOptionVM> ResponseOptions
        {
            get { return _responseOptions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responseOptions, (x) => x.ResponseOptions); }
        }

        public ReqOptionVM RequirementOptionVM
        {
            get { return _requirementOptionVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _requirementOptionVM, (x) => x.RequirementOptionVM); }
        }

        private bool? _isResponsePreviewDetailsVisible;

        public bool? IsResponsePreviewDetailsVisible
        {
            get { return _isResponsePreviewDetailsVisible; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isResponsePreviewDetailsVisible, (x) => x.IsResponsePreviewDetailsVisible); }
        }

        public ObservableCollection<ResponseGridRowVM> ResponseGridRows
        {
            get { return _responseGridRows; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responseGridRows, (x) => x.ResponseGridRows); }
        }

        public void DeleteGridRow(ResponseGridRowVM rowVM)
        {
            ResponseGridRows.Remove(rowVM);            
            new KYCBLL().DeleteResponseGridRow(rowVM);
            MessageBoxControl.Show("Row deleted successfully.");
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
