﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Collections.ObjectModel;

    [Serializable]
    public partial class ReqCategoryVM : INotifyPropertyChanged
    {
        public ReqCategoryVM()
        {
            PropertyChanged += ReqCategoryVM_PropertyChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region Private fields
        private int _id;
        private string _categoryName;
        private bool _isModified;
        private bool _isValid;
        private ReqClassificationVM _classification;
        private ObservableCollection<RequirementVM> _requirementVM;
        private bool _isSelected;
        #endregion

        #region Properties
        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        public string CategoryName
        {
            get { return _categoryName; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _categoryName, (x) => x.CategoryName))
                    IsModified = true;
            }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _isValid, (x) => x.IsValid))
                    IsModified = true;
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected);

            }
        }

        public bool IsModified
        {
            get { return _isModified; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isModified, (x) => x.IsModified); }
        }


        public ObservableCollection<RequirementVM> Requirement
        {
            get { return _requirementVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _requirementVM, (x) => x.Requirement); }

        }

        private bool _rowDetailVis;
        public bool RowDetailVis
        {
            get { return _rowDetailVis; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rowDetailVis, (x) => x.RowDetailVis); }
        }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public ReqClassificationVM Classification
        {
            get { return _classification; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _classification, (x) => x.Classification))
                    IsModified = true;
            }
        }        
        #endregion

        #region Methods
        void ReqCategoryVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }
        #endregion
    }
}
