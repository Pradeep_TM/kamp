﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.KYC.BLL;
    using KAMP.KYC.UserControls;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;

    public class ReqClassificationPageVM : INotifyPropertyChanged
    {
        public ReqClassificationPageVM()
        {
            PropertyChanged += ReqManagementVM_PropertyChanged;
            _kycBLL = new KYCBLL();
            InitializeData();
        }

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Private fields
        private ReqClassificationVM _classification;
        private ObservableCollection<ReqClassificationVM> _classifications;
        private ICommand _addClassification;
        public ICommand _initiateAddClassification;
        private ICommand _saveClassifications;
        private bool _isAddClassifincationOpen;

        private ObservableCollection<ReqCategoryVM> _categories;
        private ReqCategoryVM _category;
        private ICommand _initiateAddCategory;
        private ICommand _addCategory;
        private ICommand _saveCategories;
        private bool _isAddCategoryOpen;
        private ObservableCollection<ReqClassificationVM> _validClassifications;

        private KYCBLL _kycBLL;
        private ICommand _closePopup;
        #endregion

        #region Properties

        

        public ICommand ClosePopup
        {
            get
            {
                if (_closePopup.IsNull())
                {
                    _closePopup = new DelegateCommand(
                        () =>
                        {
                            IsAddClassificationOpen = IsAddCategoryOpen = AppVariables.KYCViewModel.IsPopupOpen = false;
                        }
                        );
                }

                return _closePopup;
            }
        }

        #region Classification
        public ReqClassificationVM Classification
        {
            get { return _classification; }
            set { PropertyChanged.HandleValueChange(this, value, ref _classification, (x) => x.Classification); }
        }

        public ObservableCollection<ReqClassificationVM> Classifications
        {
            get { return _classifications; }
            set { PropertyChanged.HandleValueChange(this, value, ref _classifications, (x) => x.Classifications); }
        }

        public bool IsAddClassificationOpen
        {
            get { return _isAddClassifincationOpen; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isAddClassifincationOpen, (x) => x.IsAddClassificationOpen); }
        }

        public ICommand InitiateAddClassification
        {
            get
            {
                if (_initiateAddClassification.IsNull())
                {
                    _initiateAddClassification = new DelegateCommand(
                        () =>
                        {
                            Classification = new ReqClassificationVM() { IsValid = true };
                            IsAddClassificationOpen = AppVariables.KYCViewModel.IsPopupOpen = true;
                        });
                }
                return _initiateAddClassification;
            }
        }

        public ICommand AddClassification
        {
            get
            {
                if (_addClassification.IsNull())
                {
                    _addClassification = new DelegateCommand(
                    () =>
                    {
                        var classification = _kycBLL.AddClassification(Classification);
                        IsAddClassificationOpen = AppVariables.KYCViewModel.IsPopupOpen = false;
                        MessageBoxControl.Show("Classification - '{0}' added successfully.".ToFormat(Classification.ClassificationName), type: MessageType.Information);
                        Classifications.Add(classification);
                        PropertyChanged.Raise(this, x => x.Classifications);
                        UpdateValidClassifications();
                    }
                    , () => Classification.IsNotNull() && Classification.ClassificationName.IsNotEmpty()
                    );
                }

                return _addClassification;
            }
        }

        public ICommand SaveClassifications
        {
            get
            {
                if (_saveClassifications.IsNull())
                {
                    _saveClassifications = new DelegateCommand(
                    () =>
                    {
                        _kycBLL.SaveClassifications(Classifications.Where(c => c.IsModified).ToList());
                        MessageBoxControl.Show("Classification(s) saved successfully.", type: MessageType.Information);
                        UpdateValidClassifications();
                    }
                    , () => Classifications.IsCollectionValid() && Classifications.Any(x => x.IsModified)
                    );
                }

                return _saveClassifications;
            }
        }
        #endregion

        #region Category
        public ReqCategoryVM Category
        {
            get { return _category; }
            set { PropertyChanged.HandleValueChange(this, value, ref _category, (x) => x.Category); }
        }

        public ObservableCollection<ReqCategoryVM> Categories
        {
            get { return _categories; }
            set { PropertyChanged.HandleValueChange(this, value, ref _categories, (x) => x.Categories); }
        }

        public bool IsAddCategoryOpen
        {
            get { return _isAddCategoryOpen; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isAddCategoryOpen, (x) => x.IsAddCategoryOpen); }
        }

        public ObservableCollection<ReqClassificationVM> ValidClassifications
        {
            get { return _validClassifications; }
            set { PropertyChanged.HandleValueChange(this, value, ref _validClassifications, (x) => x.ValidClassifications); }
        }

        public ICommand InitiateAddCategory
        {
            get
            {
                if (_initiateAddCategory.IsNull())
                {
                    _initiateAddCategory = new DelegateCommand(
                        () =>
                        {
                            Category = new ReqCategoryVM() { IsValid = true };
                            IsAddCategoryOpen = AppVariables.KYCViewModel.IsPopupOpen = true;
                        });
                }
                return _initiateAddCategory;
            }
        }

        public ICommand AddCategory
        {
            get
            {
                if (_addCategory.IsNull())
                {
                    _addCategory = new DelegateCommand(
                    () =>
                    {
                        var category = _kycBLL.AddCategory(Category);
                        IsAddCategoryOpen = AppVariables.KYCViewModel.IsPopupOpen = false;
                        MessageBoxControl.Show("Category - '{0}' added successfully.".ToFormat(Category.CategoryName), type: MessageType.Information);
                        Categories.Add(category);
                        PropertyChanged.Raise(this, x => x.Categories);
                        UpdateValidClassifications();
                    }
                    , () => Category.IsNotNull() && Category.CategoryName.IsNotEmpty() && Category.Classification.IsNotNull()
                    );
                }

                return _addCategory;
            }
        }
        
        public ICommand SaveCategories
        {
            get
            {
                if (_saveCategories.IsNull())
                {
                    _saveCategories = new DelegateCommand(
                    () =>
                    {
                        _kycBLL.SaveCategories(Categories.Where(c => c.IsModified).ToList());
                        MessageBoxControl.Show("Category(ies) saved successfully", type: MessageType.Information);
                    }
                    , () => Categories.IsCollectionValid() && Categories.Any(x => x.IsModified)
                    );
                }

                return _saveCategories;
            }
        }
        #endregion

        #endregion

        #region Methods
        private void InitializeData()
        {
            Classifications = new ObservableCollection<ReqClassificationVM>();
            Categories = new ObservableCollection<ReqCategoryVM>();

            var classificaionsDb = _kycBLL.GetAllClassification();
            if (classificaionsDb.IsCollectionValid())
            {
                classificaionsDb.ForEach(c => c.IsModified = false);
                Classifications.AddRange(classificaionsDb);
            }

            var categoryDb = _kycBLL.GetAllCategory();
            if (categoryDb.IsCollectionValid())
                Categories.AddRange(categoryDb);

            UpdateValidClassifications();

            if (categoryDb.IsCollectionValid())
                categoryDb.ForEach(c => c.IsModified = false);
        }

        private void UpdateValidClassifications()
        {
            ValidClassifications = Classifications.IsCollectionValid() ?
                          new ObservableCollection<ReqClassificationVM>(Classifications.Where(x => x.IsValid))
                          : null;

            if (ValidClassifications.IsCollectionValid() && Categories.IsCollectionValid())
            {
                for (int i = 0; i < Categories.Count; i++)
                {
                    Categories[i].Classification = ValidClassifications.FirstOrDefault(x => x.Id == Categories[i].Classification.Id);
                }
            }

            _classifications = new ObservableCollection<ReqClassificationVM>(Classifications.OrderBy(x => x.ClassificationName));
            _categories = new ObservableCollection<ReqCategoryVM>(Categories.OrderBy(x => x.CategoryName));
        }

        void ReqManagementVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }
        #endregion
    }
}
