﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using AutoMapper;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.Core.Repository;
    using KAMP.KYC.BLL;
    using KAMP.KYC.UserControls;
    using System.Collections.ObjectModel;
    using System.Windows.Controls;
    using System.Windows.Media;

    public class FormElementVM : INotifyPropertyChanged
    {
        public FormElementVM()
        {
            PropertyChanged += FormElementVM_PropertyChanged;
            Options = new ObservableCollection<FormElementOptionVM>();
        }

        public Action<FormElementVM> OnGroupNameChanged;
        public Action<FormElementVM> OnSelectionChanged;
        public Action<FormElementVM> OnIsUpdateChanged;
        public Action OptionSelectionChanged;

        public Boolean IsUpdated { get; set; }

        private Grid _gd;

        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        private string _key;

        public string Key
        {
            get { return _key; }
            set { PropertyChanged.HandleValueChange(this, value, ref _key, (x) => x.Key); }
        }

        private string _displayName;

        public string DisplayName
        {
            get { return _displayName; }
            set { IsUpdated = PropertyChanged.HandleValueChange(this, value, ref _displayName, (x) => x.DisplayName) || IsUpdated; }
        }

        private int _valueType;

        public int ValueType
        {
            get { return _valueType; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _valueType, (x) => x.ValueType))
                {
                    NeedOptions = ValueType != (int)RequirementType.FreeText;
                }
            }
        }

        private bool _needOptions;

        public bool NeedOptions
        {
            get { return _needOptions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _needOptions, (x) => x.NeedOptions); }
        }

        private bool _multiSelectColumn;

        public bool MultiSelectColumn
        {
            get { return _multiSelectColumn; }
            set { PropertyChanged.HandleValueChange(this, value, ref _multiSelectColumn, (x) => x.MultiSelectColumn); }
        }

        private string _optionText;

        public string OptionText
        {
            get { return _optionText; }
            set { PropertyChanged.HandleValueChange(this, value, ref _optionText, (x) => x.OptionText); }
        }

        private ICommand _openOptions;

        public ICommand OpenOptions
        {
            get
            {
                if (_openOptions.IsNull())
                {
                    _openOptions = new DelegateCommand(
                        () =>
                        {
                            //Attach event handlers to Options
                            if (Options.IsCollectionValid())
                            {
                                Options.Each(x =>
                                {
                                    x.OnDelete = DeleteOption;
                                });
                            }

                            //Open Options Popup  
                            ShowPopup(new UCKPMGEditableOptions(), this);
                        }
                    );

                }

                return _openOptions;
            }
        }

        private ICommand _closeOptions;

        public ICommand CloseOptions
        {
            get
            {
                if (_closeOptions.IsNull())
                {
                    _closeOptions = new DelegateCommand(
                        () =>
                        {
                            var optionsToRem = Options.Where(x => x.OptionText.IsEmpty()).ToList();
                            optionsToRem.Each(x => Options.Remove(x));
                            ClosePopup();
                        }
                    );
                }

                return _closeOptions;
            }
        }

        private ICommand _saveOptions;

        public ICommand SaveOptions
        {
            get
            {
                if (_saveOptions.IsNull())
                {
                    _saveOptions = new DelegateCommand(
                        () =>
                        {
                            new KYCBLL().SaveFormElementOptions(this);
                            MessageBoxControl.Show("Options saved successfully.");
                            ClosePopup();
                        },
                        () =>
                        {
                            return Options.IsCollectionValid() && Options.All(x => x.OptionText.IsNotEmpty());
                        }
                    );
                }

                return _saveOptions;
            }
        }

        private ICommand _addOption;

        public ICommand AddOption
        {
            get
            {
                if (_addOption.IsNull())
                {
                    _addOption = new DelegateCommand(() =>
                        {
                            var optionToAdd = new FormElementOptionVM()
                            {
                                CreatedBy = 1,
                                CreatedDate = DateTime.Now,
                                ModifiedBy = 1,
                                ModifiedDate = DateTime.Now,
                                OptionText = OptionText
                            };
                            optionToAdd.OnDelete += DeleteOption;
                            Options.Add(optionToAdd);

                            OptionText = string.Empty;
                        },
                        () => OptionText.IsNotEmpty()
                    );
                }
                return _addOption;
            }
        }

        private ICommand _addOptionInOptionPopup;

        public ICommand AddOptionInOptionPopup
        {
            get
            {
                if (_addOptionInOptionPopup.IsNull())
                {
                    _addOptionInOptionPopup = new DelegateCommand(() =>
                    {
                        var optionToAdd = new FormElementOptionVM()
                        {
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ModifiedBy = 1,
                            ModifiedDate = DateTime.Now,
                            OptionText = OptionText
                        };
                        optionToAdd.OnDelete += DeleteOption;
                        Options.Add(optionToAdd);

                        OptionText = string.Empty;
                    }
                    );
                }
                return _addOptionInOptionPopup;
            }
        }

        private void ShowPopup(UserControl uc, object dataContext)
        {
            if (_gd.IsNotNull())
                return;

            _gd = new Grid();
            Label lbl = new Label();
            Panel.SetZIndex(uc, 101);
            lbl.Background = Brushes.Black;
            lbl.Opacity = .5;
            _gd.Children.Add(lbl);
            uc.DataContext = dataContext;
            _gd.Children.Add(uc);
            AppVariables.MainGrid.Children.Add(_gd);
        }

        private void ClosePopup()
        {
            if (_gd.IsNotNull())
            {
                AppVariables.MainGrid.Children.Remove(_gd);
                _gd = null;
            }
        }

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set { IsUpdated = PropertyChanged.HandleValueChange(this, value, ref _isValid, (x) => x.IsValid) || IsUpdated; }
        }

        private bool _isMandatory;

        public bool IsMandatory
        {
            get { return _isMandatory; }
            set { IsUpdated = PropertyChanged.HandleValueChange(this, value, ref _isMandatory, (x) => x.IsMandatory) || IsUpdated; }
        }

        private string _optionsHeading;

        public string OptionsHeading
        {
            get { return _optionsHeading; }
            set { PropertyChanged.HandleValueChange(this, value, ref _optionsHeading, (x) => x.OptionsHeading); }
        }

        private int _formId;

        public int FormId
        {
            get { return _formId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _formId, (x) => x.FormId); }
        }

        private int _groupId;

        public int GroupId
        {
            get { return _groupId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _groupId, (x) => x.GroupId); }
        }

        private string _groupName;

        public string GroupName
        {
            get { return _groupName; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _groupName, (x) => x.GroupName))
                {
                    if (OnGroupNameChanged.IsNotNull())
                        OnGroupNameChanged(this);
                }
            }
        }

        private string _toolTip;

        public string ToolTip
        {
            get { return _toolTip; }
            set { PropertyChanged.HandleValueChange(this, value, ref _toolTip, (x) => x.ToolTip); }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected))
                {
                    if (OnSelectionChanged.IsNotNull())
                        OnSelectionChanged(this);
                }
            }
        }

        private ObservableCollection<FormElementOptionVM> _options;

        public ObservableCollection<FormElementOptionVM> Options
        {
            get { return _options; }
            set { PropertyChanged.HandleValueChange(this, value, ref _options, (x) => x.Options); }
        }

        private string _selectedOptions;

        public string SelectedOptions
        {
            get { return _selectedOptions = Options.IsCollectionValid() ? string.Join(", ", Options) : string.Empty; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedOptions, (x) => x.SelectedOptions); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void FormElementVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        internal void DeleteOption(FormElementOptionVM option)
        {
            if (Options.IsCollectionValid())
                Options.Remove(option);
        }
    }

    public class FormElementNodeVM
    {
        public FormElementVM FormElement { get; set; }
        public string Value { get; set; }
    }

    public class FormElementGroupVM
    {
        public FormElementGroupVM()
        {
            FormElements = new ObservableCollection<FormElementNodeVM>();
        }

        public ObservableCollection<FormElementNodeVM> FormElements { get; set; }
        public string GroupName { get; set; }
    }
}
