using System;
using System.Windows.Input;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.ComponentModel;
    using System.Windows;
    using UserControls;

    public partial class ReqOptionVM : INotifyPropertyChanged
    {
        public ReqOptionVM()
        {
            PropertyChanged += ReqOptionVM_PropertyChanged;
        }        

        #region Private Fields
        private string _optionName;
        private bool _isValid;
        #endregion

        #region Properties
        public long Id { get; set; }
        public Nullable<long> RequirementId { get; set; }
        public Nullable<long> GridColumnId { get; set; }
        public string OptionName
        {
            get { return _optionName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _optionName, (x) => x.OptionName); }
        }
        public bool IsValid
        {
            get { return _isValid; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isValid, (x) => x.IsValid); }
        }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; } 
        #endregion

        private ICommand _deleteOption;

        public ICommand DeleteOption
        {
            get
            {
                if (_deleteOption.IsNull())
                {
                    _deleteOption = new DelegateCommand(
                        () =>
                        {
                            if (OnDelete.IsNotNull() && MessageBoxControl.Show("Are you sure you want to delete this option?", messageBoxButton: MessageBoxButton.OKCancel, type: MessageType.Alert) == MessageBoxResult.OK) OnDelete(this);
                        });
                }

                return _deleteOption;
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected);

            }
        }

        #region Events
        public Action<ReqOptionVM> OnDelete;
        public event PropertyChangedEventHandler PropertyChanged; 
        #endregion

        #region Methods
        void ReqOptionVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        } 
        #endregion
    }
}
