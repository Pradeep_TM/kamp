using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows;
using System.Linq;


namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using UserControls;

    public class ReqGridColumnVM : INotifyPropertyChanged
    {
        public ReqGridColumnVM()
        {
            PropertyChanged += ColumnVM_PropertyChanged;
        }

        public Action<ReqGridColumnVM> OnDelete;

        void ColumnVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        private string _columnText;

        public string ColumnText
        {
            get { return _columnText; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnText, x => x.ColumnText); }
        }
                
        private long _id;

        public long Id
        {                   
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }
        
        
        //public object ColumnVal { get; set; }

        private RequirementType _columnType;

        public RequirementType ColumnType
        {
            get { return _columnType; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnType, x => x.ColumnType); }
        }

        private IReqDetails _columnContent;
        public IReqDetails ColumnContent
        {
            get { return _columnContent; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnContent, x => x.ColumnContent); }
        }

        private ICommand _deleteColumn;

        public ICommand DeleteColumn
        {
            get
            {
                if (_deleteColumn.IsNull())
                {
                    _deleteColumn = new DelegateCommand(
                        () =>
                        {
                            if (OnDelete.IsNotNull() && MessageBoxControl.Show("Are you sure you want to delete this Column?", messageBoxButton: MessageBoxButton.OKCancel, type: MessageType.Alert) == MessageBoxResult.OK)
                                OnDelete(this);
                        });
                }

                return _deleteColumn;
            }
        }

        private ICommand _addColumnDetails;

        public ICommand AddColumnDetails
        {
            get
            {
                if (_addColumnDetails.IsNull())
                {
                    _addColumnDetails = new DelegateCommand(
                        () =>
                        {

                            var questionContents = typeof(IReqDetails).GetImplementors()
                                                    .Select(x => ((IReqDetails)Activator.CreateInstance(x)))
                                                    .Where(x => x.RequirementType == ColumnType);

                        }, () => { return ColumnType != RequirementType.FreeText; });
                }

                return _addColumnDetails;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

