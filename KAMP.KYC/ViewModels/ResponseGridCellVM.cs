using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Windows.Controls;
    using System.Windows.Input;

    public partial class ResponseGridCellVM : INotifyPropertyChanged
    {
        public ResponseGridCellVM()
        {
            PropertyChanged += ResponseGridCell_PropertyChanged;
        }

        public Action OnResponseDetailsOpen;

        void ResponseGridCell_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        private long _responseId;
        private long _columnId;
        private int _rowId;
        private string _responseText;

        private string _columnName;

        public string ColumnName
        {
            get { return _columnName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnName, (x) => x.ColumnName); }
        }


        public long Id { get; set; }

        public long ResponseId
        {
            get { return _responseId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responseId, (x) => x.ResponseId); }
        }

        public long ColumnId
        {
            get { return _columnId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnId, (x) => x.ColumnId); }
        }

        public int RowId
        {
            get { return _rowId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rowId, (x) => x.RowId); }
        }

        public string ResponseText
        {
            get { return _responseText; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responseText, (x) => x.ResponseText); }
        }

        private string _selectedOptions;

        public string SelectedOptions
        {
            get { return _selectedOptions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedOptions, (x) => x.SelectedOptions); }
        }


        //private ICommand _addGridDetails;

        //public ICommand AddGridDetails
        //{
        //    get
        //    {
        //        if (_addGridDetails.IsNull())
        //        {
        //            _addGridDetails = new DelegateCommand<Button>((btn) =>
        //            {
        //                //Display grid details
        //                var responseGridVM = (btn.Tag as ResponseGridCellVM);

        //                //Display grid details
        //                if(OnResponseDetailsOpen.IsNotNull())
        //                    OnResponseDetailsOpen();                                     
        //            });
        //        }
        //        return _addGridDetails;
        //    }
        //}

        public ReqGridColumnVM ReqGridColumn { get; set; }

        public List<ResponseOptionVM> ResponseOptions { get; set; }

        public ResponseOptionVM ResponseOption { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void ResponseOptionSelectionChanged()
        {
            SelectedOptions = string.Empty;

            if (ResponseOptions.IsCollectionValid())
            {
                SelectedOptions = ResponseOptions.Where(x => x.IsSelected).Aggregate("", (x, y) => x + (x.IsEmpty() ? "" : ", ") + y.ReqOptionVM.OptionName);
            }
        }
    }
}
