﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.KYC.ViewModels
{
    public enum ResponseState : short
    {
        [Display(Name = "Not Answered")]
        UnAnswered = 1,
        [Display(Name = "Answered")]
        Answered,
        [Display(Name = "Partially Answered")]
        PartiallyAnswered
    }
}
