﻿using KAMP.Core.UserManagement.AppCode;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace KAMP.KYC.ViewModels
{
    using KAMP.Core.Common.Models;
    using ViewModels;

    public static class KYCPageControls
    {
        public const string ScreenConfiguration = "Screen Configuration";
        public const string Home = "Know Your Customer";
        public const string BankData = "Bank Data";
        public const string RequirementClassification = "Requirement Classification";
        public const string CreateRequirement = "Create Requirement";
        public const string Accounts = "Accounts";
        public const string KPMGEditable = "KPMG Editable";
        public const string AssignCases = "Assign Cases";
        public const string ManageWorkflow = "Manage Workflow";
        public const string ManageState = "Manage State";
        public const string WorkflowMonitoring = "Workflow Monitoring";
        public const string CaseManagement = "Case Management";
        public const string Report = "Report";
        public const string LookUps = "Lookups";
        public const string AuditConfiguration = "Audit Configuration";
        public const string History = "History";
        public const string RequirementManagement = "Requirement Management";
        
        
    }

    public static class KYCScreenConfigConstants
    {
        public const string BankData = "Bank Data";
        public const string Accounts = "Accounts";
        public static string KPMGEditable = "KPMG Editable";
    }

    public static class KYCConstants
    {
        public const string DefaultDropDownValue = "Select";
        public const string CloseApplicationText = "Are you sure you want to close the application?";
        //public const int CFID = 4;
        public const string CaseCategory = "CaseCategory";
        public const string DefaultDDLValue = "-Select-";
        public const int DisplayItemCount = 10;
        public const string NotApplicableText = "N.A";
        public const string KpmgEditable = "KPMGEditable";
        public const string FormElement = "FormElement";
        public const string ReqCategory = "ReqCategory";
        public const string ReqClassification = "ReqClassification";
        public const string Requirement = "Requirement";
        public const string Response = "Response";
        public const string FormElementOption = "FormElementOption";
        public const string AssignedTo = "AssignedTo";
        public const string CategoryFilter = "Category Filter";

        public const string MultipleValues = "Multiple Value(s)";
        public const string GridOfAnswers = "Grid of Answer(s)";

        public const string DefRequirements = "Default/Mandatory Requirements";
    } 

    public static class AppVariables
    {
        public static KAMPPrincipal KAMPPrincipal { get { return ((KAMPPrincipal)Thread.CurrentPrincipal); } }
        public static KYCViewModel KYCViewModel { get { return Application.Current.MainWindow.DataContext as KYCViewModel; } }
        public static Grid MainGrid { get { return Application.Current.MainWindow.FindName("GdContainer") as Grid; } }        
        public static CaseFileCardPageVM CaseFileCardPageVM { get; set; }
        public static long? RequirementToEdit { get; set; }
    }
}
