﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;

    public class AccountVM : INotifyPropertyChanged
    {
        public AccountVM()
        {
            PropertyChanged += AccountVM_PropertyChanged;
        }

        private DataTable _accountsData;

        public DataTable AccountsData
        {
            get { return _accountsData; }
            set { PropertyChanged.HandleValueChange(this, value, ref _accountsData, (x) => x.AccountsData); }
        }


        void AccountVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
