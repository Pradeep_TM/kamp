using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using System.Collections.ObjectModel;

    [Serializable]
    public partial class ReqClassificationVM :INotifyPropertyChanged
    {
        public ReqClassificationVM()
        {
            PropertyChanged += ReqClassificationVM_PropertyChanged;            
        }


        [field:NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public Action<ObservableCollection<ReqCategoryVM>> OnReqCategorySelected;

        #region Private Fields
        private int _id;
        private string _classificationName;
        private bool _isValid;
        private bool _isModified;
        private ObservableCollection<ReqCategoryVM> _reqCategories;
        public ObservableCollection<ReqCategoryVM> _selectedCategory;
        #endregion       
                
        #region Properties
        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        public string ClassificationName
        {
            get { return _classificationName; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _classificationName, (x) => x.ClassificationName))
                {
                    IsModified = true;
                }
            }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _isValid, (x) => x.IsValid))
                {
                    IsModified = true;
                }
            }
        }

        public bool IsModified
        {
            get { return _isModified; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isModified, (x) => x.IsModified); }
        }

        public ObservableCollection<ReqCategoryVM> ReqCategories
        {
            get { return _reqCategories; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqCategories, (x) => x.ReqCategories); }
        }

        public ObservableCollection<ReqCategoryVM> SelectedCategories
        {
            get { return _selectedCategory; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _selectedCategory, (x) => x.SelectedCategories))
                {
                    if (OnReqCategorySelected.IsNotNull())
                        OnReqCategorySelected(SelectedCategories);
                };
            }
        }
        private bool _rowDetailVis;
        public bool RowDetailVis
        {
            get { return _rowDetailVis; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rowDetailVis, (x) => x.RowDetailVis); }
        }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; } 
        #endregion       
                
        #region Methods
        void ReqClassificationVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        } 
        #endregion        
    }
}
