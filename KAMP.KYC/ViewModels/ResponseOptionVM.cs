using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;

    public partial class ResponseOptionVM : INotifyPropertyChanged
    {
        public ResponseOptionVM()
        {
            PropertyChanged += ResponseOption_PropertyChanged;
        }

        public Action OptionSelectionChanged;

        void ResponseOption_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        private Nullable<long> _responseId;
        private long _reqOptionId;
        private Nullable<long> _gridCellId;
        private ReqOptionVM _reqOptionVM;

        public long Id { get; set; }
        public Nullable<long> ResponseId
        {
            get { return _responseId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _responseId, (x) => x.ResponseId); }
        }

        public long ReqOptionId
        {
            get { return _reqOptionId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqOptionId, (x) => x.ReqOptionId); }
        }

        public ReqOptionVM ReqOptionVM
        {
            get { return _reqOptionVM; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqOptionVM, (x) => x.ReqOptionVM); }
        }

        public Nullable<long> GridCellId
        {
            get { return _gridCellId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _gridCellId, (x) => x.GridCellId); }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _isSelected, x => x.IsSelected) && OptionSelectionChanged.IsNotNull())
                {
                    OptionSelectionChanged();
                }
            }
        }
        
        //public virtual Response Response { get; set; }
        //public virtual ResponseGridCell ResponseGridCell { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
