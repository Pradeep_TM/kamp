﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Controls;


namespace KAMP.KYC.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.Core.Repository;
    using KAMP.KYC.UserControls;
    using ViewModels;
    using KAMP.KYC.BLL;
    using System.Windows;

    public class RequirementVM : INotifyPropertyChanged
    {
        public RequirementVM()
        {
            PropertyChanged += RequirementVM_PropertyChanged;

            InitializeData();
        }

        private void InitializeData()
        {

        }

        public Action OnReqDetialsResponseSave;
        public Action OnReqDetailsResponseClose;

        void RequirementVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        #region Private Fields
        public long _id;
        public string _requirementText;
        public bool _isMandatory;
        public bool _isDefault;
        public ReqCategoryVM _category;
        private RequirementType _reqType;
        private MasterEntity _requirementType;
        private ResponseVM _responseVM;
        //private ObservableCollection<ReqOptionVM> _reqOptions;
        //private ObservableCollection<ReqGridColumnVM> _reqGridColumn;
        private IReqDetails _requirementDetails;
        private ObservableCollection<RequirementVM> _childRequirements;
        #endregion

        #region Properties
        public long Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        public string RequirementText
        {
            get { return _requirementText; }
            set { PropertyChanged.HandleValueChange(this, value, ref _requirementText, (x) => x.RequirementText); }
        }

        public ReqCategoryVM Category
        {
            get { return _category; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _category, (x) => x.Category);
            }
        }

        public RequirementType ReqType
        {
            get { return RequirementType != null ? (RequirementType)RequirementType.Code : _reqType; }
            set { PropertyChanged.HandleValueChange(this, value, ref _reqType, (x) => ReqType); }
        }

        public MasterEntity RequirementType
        {
            get { return _requirementType; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _requirementType, (x) => x.RequirementType))
                {
                    RequirementDetails = typeof(IReqDetails).GetImplementors()
                                            .Where(x => x.BaseType != typeof(UserControl))
                                            .Select(x => Activator.CreateInstance(x) as IReqDetails)
                                            .FirstOrDefault(x => x.RequirementType == ReqType);
                }
            }
        }
        //public ObservableCollection<ReqOptionVM> ReqOptions
        //{
        //    get { return _reqOptions; }
        //    set { PropertyChanged.HandleValueChange(this, value, ref _reqOptions, (x) => x.ReqOptions); }
        //}

        //public ObservableCollection<ReqGridColumnVM> ReqGridColumns
        //{
        //    get { return _reqGridColumn; } 
        //    set { PropertyChanged.HandleValueChange(this, value, ref _reqGridColumn, (x) => x.ReqGridColumns); }
        //}

        private bool _isAnsPreviewVisible;

        public bool IsAnsPreviewVisible
        {
            get { _isAnsPreviewVisible = true; return _isAnsPreviewVisible; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isAnsPreviewVisible, (x) => x.IsAnsPreviewVisible); }
        }

        public bool IsGridReq
        { get { return ReqType == ViewModels.RequirementType.Grid; } }

        public bool IsMultiSelectReq
        { get { return ReqType == ViewModels.RequirementType.MultiSelectOption; } }

        public IReqDetails RequirementDetails
        {
            get { return _requirementDetails; }
            set { PropertyChanged.HandleValueChange(this, value, ref _requirementDetails, (x) => x.RequirementDetails); }
        }

        public ObservableCollection<RequirementVM> ChildRequirements
        {
            get { return _childRequirements; }
            set { PropertyChanged.HandleValueChange(this, value, ref _childRequirements, (x) => x.ChildRequirements); }
        }

        public ResponseVM ResponseVM
        {
            get { return _responseVM; }
            set { _responseVM = value; }
        }

        private bool _isRespPreviewDetailsAvlbl;

        /// <summary>
        /// Show or hide the lens icon to display preview details.
        /// </summary>
        public bool IsRespPreviewDetailsAvlbl
        {
            get
            {
                _isRespPreviewDetailsAvlbl = (ReqType == KYC.ViewModels.RequirementType.MultiSelectOption || ReqType == ViewModels.RequirementType.Grid) && ResponseVM.ResponseStatus != ResponseState.UnAnswered;
                return _isRespPreviewDetailsAvlbl;
            }            
        }


        private ICommand _saveResponse;

        public ICommand SaveResponse
        {
            get
            {
                if (_saveResponse.IsNull())
                {
                    _saveResponse = new DelegateCommand(() =>
                    {
                        ResponseVM.cf_ID = AppVariables.CaseFileCardPageVM.CaseFileDetails.cf_ID;
                        ResponseVM.RequirementId = this.Id;

                        switch (ReqType)
                        {
                            case ViewModels.RequirementType.FreeText:
                                //Adding selected option
                                ResponseVM.ResponseState = (short)ResponseState.Answered;
                                new KYCBLL().SaveResponses(this);
                                MessageBoxControl.Show("Response saved successfully.", System.Windows.MessageBoxButton.OK, MessageType.Information);
                                ResponseVM.ResponsePreview = ResponseVM.ResponseText;
                                break;
                            case ViewModels.RequirementType.SingleSelectOption:
                                //var options = (RequirementDetails as SingleSelectReqDetailsVM).Options;
                                //var selectedOption = options.FirstOrDefault(x => x.IsValid);
                                SaveSingleSelectRequirement();
                                ResponseVM.ResponsePreview = SelectedOption.OptionName;
                                break;

                            case ViewModels.RequirementType.MultiSelectOption:
                                SaveMultiSelectResponse();
                                ResponseVM.ResponsePreview = KYCConstants.MultipleValues;
                                break;

                            case ViewModels.RequirementType.Grid:
                                SaveGridResponse();
                                ResponseVM.ResponsePreview = KYCConstants.GridOfAnswers;
                                break;
                            default:
                                break;
                        }

                        PropertyChanged.Raise(this, x => x.IsRespPreviewDetailsAvlbl);

                        if (OnReqDetialsResponseSave.IsNotNull())
                        {
                            OnReqDetialsResponseSave();
                        }
                    }, () =>
                    {
                        return CheckResponseSaveAvailability();
                    });
                }

                return _saveResponse;
            }
        }



        private bool CheckResponseSaveAvailability()
        {
            switch (ReqType)
            {
                case ViewModels.RequirementType.FreeText:
                    return ResponseVM.IsNotNull() && ResponseVM.ResponseText.IsNotEmpty();
                case ViewModels.RequirementType.SingleSelectOption:
                    //var options = (RequirementDetails as SingleSelectReqDetailsVM).Options;
                    //if (options.IsCollectionValid())
                    //{
                    //    var selectedOption = options.Where(x => x.IsValid).ToList();
                    //    return selectedOption.Exists(x => x.IsValid);
                    //}

                    return _selectedOption.IsNotNull();


                case ViewModels.RequirementType.MultiSelectOption:

                    //Create a new list for assigning the selected multiselect options
                    //Check whether that list IsCollectionValid
                    //var multiOptions = (RequirementDetails as MultiSelectReqDetailsVM).Options;
                    //if (multiOptions.IsCollectionValid())
                    //{
                    //    var selectedMultiOptions = multiOptions.Where(x => x.IsValid).ToList();
                    //    return selectedMultiOptions.Exists(x => x.IsValid);
                    //}
                    return AddedOptions.IsCollectionValid();

                case ViewModels.RequirementType.Grid:
                    var gridColumns = (RequirementDetails as GridReqDetailsVM).Columns;
                    //Validate grid details.
                    return true;
                default:
                    return false;
            }

            return false;
        }

        private void SaveGridResponse()
        {
            /*Update responseId in all the responsegridcells
             *Update GridId and ResponseId in all the options
             */
            bool isAllGridCellsFilled = ResponseVM.ResponseGridRows.IsCollectionValid();
            bool isAnyCellFilled = false;

            if (ResponseVM.ResponseGridRows.IsNull())
                ResponseVM.ResponseGridRows = new ObservableCollection<ResponseGridRowVM>();

            ResponseVM.ResponseGridRows.SelectMany(x => x.ResponseGridCells)
                .ToList()
                .ForEach(x =>
                {
                    x.ResponseId = ResponseVM.Id;

                    if (x.ReqGridColumn.ColumnType == ViewModels.RequirementType.SingleSelectOption && x.ResponseOption.IsNotNull())
                    {
                        x.ResponseOption.GridCellId = x.Id;
                    }

                    if (x.ReqGridColumn.ColumnType == ViewModels.RequirementType.MultiSelectOption && x.ResponseOptions.IsCollectionValid())
                    {
                        foreach (var respOption in x.ResponseOptions)
                        {
                            //respOption.ResponseId = ResponseVM.Id;
                            respOption.GridCellId = x.Id;
                        }
                    }

                    switch (x.ReqGridColumn.ColumnType)
                    {
                        case ViewModels.RequirementType.FreeText:
                            isAllGridCellsFilled = isAllGridCellsFilled && x.ResponseText.IsNotEmpty();
                            isAnyCellFilled = isAnyCellFilled || x.ResponseText.IsNotEmpty();
                            break;
                        case ViewModels.RequirementType.SingleSelectOption:
                            isAllGridCellsFilled = isAllGridCellsFilled && x.ResponseOption.IsNotNull();
                            isAnyCellFilled = isAnyCellFilled || x.ResponseOption.IsNotNull();
                            break;
                        case ViewModels.RequirementType.MultiSelectOption:
                            isAllGridCellsFilled = isAllGridCellsFilled && x.ResponseOptions.IsCollectionValid() && x.ResponseOptions.Any(o => o.IsSelected);
                            isAnyCellFilled = isAnyCellFilled || x.ResponseOptions.IsCollectionValid() && x.ResponseOptions.Any(o => o.IsSelected);
                            break;
                    }

                });            

            if (isAnyCellFilled)
                ResponseVM.ResponseState = (short)ResponseState.PartiallyAnswered;

            if (isAllGridCellsFilled)
                ResponseVM.ResponseState = (short)ResponseState.Answered;

            if (!isAnyCellFilled && !isAllGridCellsFilled)
                ResponseVM.ResponseState = (short)ResponseState.UnAnswered;

            new KYCBLL().SaveResponses(this);
            MessageBoxControl.Show("Response saved successfully.", System.Windows.MessageBoxButton.OK, MessageType.Information);

            PropertyChanged.Raise(this, x => x.IsRespPreviewDetailsAvlbl);
        }

        private void SaveMultiSelectResponse()
        {
            // var multiOptions = (RequirementDetails as MultiSelectReqDetailsVM).Options;
            if (AddedOptions.IsNotNull() && AddedOptions.IsCollectionValid())
            {
                var savedOptionsLkp = new Dictionary<long, long>();
                if (ResponseVM.ResponseOptions.IsCollectionValid())
                {
                    savedOptionsLkp = ResponseVM.ResponseOptions.ToDictionary(x => x.ReqOptionId, x => x.Id);
                }

                ResponseVM.ResponseOptions = new ObservableCollection<ResponseOptionVM>();

                //selectedMultiOptions - Bind it to selected options for multiple
                var selectedMultiOptions = AddedOptions.ToList();

                selectedMultiOptions.ForEach(x => ResponseVM.ResponseOptions.Add(new ResponseOptionVM()
                {
                    ReqOptionId = x.Id,
                    ResponseId = ResponseVM.Id,
                    ReqOptionVM = x,
                    Id = savedOptionsLkp.SafeGet(x.Id)
                }));
                ResponseVM.ResponseState = (short)ResponseState.Answered;
                new KYCBLL().SaveResponses(this);

                //Adding requirement option to the selected response option
                foreach (var respOption in ResponseVM.ResponseOptions)
                {
                    respOption.ReqOptionVM = selectedMultiOptions.FirstOrDefault(x => x.Id == respOption.ReqOptionId);
                }
                MessageBoxControl.Show("Response saved successfully.", System.Windows.MessageBoxButton.OK, MessageType.Information);
            }
        }

        private void SaveSingleSelectRequirement()
        {
            if (_selectedOption.IsNotNull())
            {
                var savedOptionLkp = new Dictionary<long, long>();
                if (ResponseVM.ResponseOptions.IsCollectionValid())
                {
                    savedOptionLkp = ResponseVM.ResponseOptions.ToDictionary(x => x.ReqOptionId, x => x.Id);
                }

                ResponseVM.ResponseOptions = new ObservableCollection<ResponseOptionVM>();
                ResponseVM.ResponseOptions.Add(new ResponseOptionVM()
                {
                    ReqOptionId = _selectedOption.Id,
                    ResponseId = ResponseVM.Id,
                    ReqOptionVM = _selectedOption,
                    Id = savedOptionLkp.SafeGet(_selectedOption.Id)
                });
                ResponseVM.ResponseState = (short)ResponseState.Answered;
                new KYCBLL().SaveResponses(this);

                //Adding selected option
                ResponseVM.ResponseOptions.First().ReqOptionVM = _selectedOption;
                MessageBoxControl.Show("Response saved successfully.", System.Windows.MessageBoxButton.OK, MessageType.Information);
            }
        }

        private ICommand _closeResponse;

        public ICommand CloseResponse
        {
            get
            {
                if (_closeResponse.IsNull())
                {
                    _closeResponse = new DelegateCommand(() => { if (OnReqDetailsResponseClose.IsNotNull()) { OnReqDetailsResponseClose(); } });
                }

                return _closeResponse;
            }
        }

        private ICommand _addRow;

        public ICommand AddRow
        {
            get
            {
                if (_addRow.IsNull())
                {
                    _addRow = new DelegateCommand(
                        () =>
                        {
                            if (ResponseVM.ResponseGridRows.IsNull())
                            {
                                ResponseVM.ResponseGridRows = new ObservableCollection<ResponseGridRowVM>();
                            }

                            var reqDetails = RequirementDetails as GridReqDetailsVM;
                            var gridRow = new ResponseGridRowVM()
                            {
                                RowId = ResponseVM.ResponseGridRows.Count,
                                ResponseGridCells = new ObservableCollection<ResponseGridCellVM>(),
                                OnKPMGEditableRowDelete = ResponseVM.DeleteGridRow
                            };
                            ResponseVM.ResponseGridRows.Add(gridRow);

                            foreach (var grdCol in reqDetails.Columns)
                            {
                                var respCol = new ResponseGridCellVM();
                                respCol.ColumnName = grdCol.ColumnText;
                                respCol.ColumnId = grdCol.Id;
                                respCol.ReqGridColumn = grdCol;
                                respCol.RowId = gridRow.RowId;
                                respCol.ResponseId = ResponseVM.Id;

                                if (grdCol.ColumnType == ViewModels.RequirementType.MultiSelectOption)
                                    respCol.ResponseOptions = (grdCol.ColumnContent as MultiSelectReqDetailsVM).Options.Select(x => new ResponseOptionVM()
                                    {
                                        ReqOptionId = x.Id,
                                        ReqOptionVM = x,
                                        ResponseId = ResponseVM.Id,
                                        OptionSelectionChanged = respCol.ResponseOptionSelectionChanged
                                    }).ToList();

                                else if (grdCol.ColumnType == ViewModels.RequirementType.SingleSelectOption)
                                    respCol.ResponseOptions = (grdCol.ColumnContent as SingleSelectReqDetailsVM).Options.Select(x => new ResponseOptionVM()
                                    {
                                        ReqOptionId = x.Id,
                                        ReqOptionVM = x,
                                        ResponseId = ResponseVM.Id,
                                        OptionSelectionChanged = respCol.ResponseOptionSelectionChanged
                                    }).ToList();

                                gridRow.ResponseGridCells.Add(respCol);
                            }
                        }
                        );
                }
                return _addRow;
            }
        }

        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ModifiedBy { get; set; }

        #region Added for single/MultiSelect Functionality

        private ReqOptionVM _selectedOption;
        public ReqOptionVM SelectedOption
        {
            get { return this._selectedOption; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedOption, (x) => x.SelectedOption);
            }
        }

        private ObservableCollection<ReqOptionVM> _multiselectedOptions;
        public ObservableCollection<ReqOptionVM> MultiSelectedOptions
        {
            get { return this._multiselectedOptions; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _multiselectedOptions, (x) => x.MultiSelectedOptions);
            }
        }

        private ObservableCollection<ReqOptionVM> _availableOptions;
        public ObservableCollection<ReqOptionVM> AvailableOptions
        {
            get { return _availableOptions; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _availableOptions, (x) => x.AvailableOptions))
                {
                    RefreshOptionsByClsfcn();

                    if (AvailableOptions.IsNotNull() && AvailableOptions.IsCollectionValid())
                        (RequirementDetails as MultiSelectReqDetailsVM).Options.Where(x => AddedOptions.Any(c => c.Id == x.Id))
                            .ToList()
                            .ForEach(x => AvailableOptions.Remove(x));


                }
            }
        }

        private ObservableCollection<ReqOptionVM> _addedOptions;
        public ObservableCollection<ReqOptionVM> AddedOptions
        {
            get { return _addedOptions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _addedOptions, (x) => x.AddedOptions); }
        }

        public void RefreshOptionsByClsfcn()
        {
            if ((RequirementDetails as MultiSelectReqDetailsVM).Options.IsCollectionValid())
            {
                var val = (RequirementDetails as MultiSelectReqDetailsVM).Options.Select(x => x.Id).ToList();
                if (MultiSelectedOptions.IsNull())
                    MultiSelectedOptions = new ObservableCollection<ReqOptionVM>();
                var categoriesByClsfication = MultiSelectedOptions.Where(x => val.Contains(x.Id)).ToList();
                AddedOptions = new ObservableCollection<ReqOptionVM>(categoriesByClsfication);
            }

        }

        private ICommand _addOptions;
        public ICommand AddOptions
        {
            get
            {
                if (_addOptions.IsNull())
                    _addOptions = new DelegateCommand(
                        () =>
                        {
                            if (_availableOptions.IsCollectionValid())
                            {
                                var selCategories = _availableOptions.Where(x => x.IsSelected).ToList();

                                selCategories.ForEach(x =>
                                {
                                    _availableOptions.Remove(x);
                                });

                                if (selCategories.IsCollectionValid())
                                {
                                    if (MultiSelectedOptions.IsNull())
                                    {
                                        MultiSelectedOptions = new ObservableCollection<ReqOptionVM>();
                                    }

                                    MultiSelectedOptions.AddRange(selCategories);
                                    RefreshOptionsByClsfcn();

                                }
                            }

                        },
                        () => _availableOptions.IsCollectionValid() && _availableOptions.Any(x => x.IsSelected)
                );
                return _addOptions;
            }
        }

        private ICommand _removeOptions;
        public ICommand RemoveOptions
        {
            get
            {
                if (_removeOptions.IsNull())
                    _removeOptions = new DelegateCommand(
                        () =>
                        {
                            if (MultiSelectedOptions.IsCollectionValid())
                            {
                                //var messageBoxResult = KAMP.KYC.UserControls.MessageBoxControl.Show("Removing this category removes all the requirements associated. Do you want to continue?"
                                // , MessageBoxButton.OKCancel, KAMP.KYC.UserControls.MessageType.Alert);

                                var categoriesToRem = AddedOptions.Where(x => x.IsSelected).ToList();

                                if (categoriesToRem.IsCollectionValid())
                                {
                                    categoriesToRem.ForEach(x =>
                                    {
                                        MultiSelectedOptions.Remove(x);
                                    });

                                    _availableOptions.AddRange(categoriesToRem);

                                    RefreshOptionsByClsfcn();
                                }

                            }
                        },
                        () => MultiSelectedOptions.IsCollectionValid() && MultiSelectedOptions.Any(x => x.IsSelected)
                        );

                return _removeOptions;
            }
        }

        #endregion

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
