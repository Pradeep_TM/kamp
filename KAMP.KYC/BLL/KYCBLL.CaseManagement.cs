﻿using KAMP.Core.Repository.KYC;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace KAMP.KYC.BLL
{
    using Core.FrameworkComponents;
    using Core.Workflow;
    using Core.Repository;
    using System.Collections.ObjectModel;

    public partial class KYCBLL
    {
        public List<CaseFileVM> GetAssignedCases()
        {
            return GetCases(onlyAssignedCases: true);
        }

        public List<CaseFileVM> GetMyCases()
        {
            return GetCases(userId: AppVariables.KAMPPrincipal.UserId);
        }

        private List<CaseFileVM> GetCases(long? userId = null, bool onlyAssignedCases = false)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var caseFileRep = uow.GetRepository<KYCCaseFile>();

                //Get Cases from workflow.
                var caseFileQry = caseFileRep.Items;

                //if (onlyAssignedCases)
                //    caseFileQry = caseFileQry.Where(x => x.AnalystId != null && x.AnalystId != 0);

                var caseFiles = (from x in caseFileQry
                                 join category in uow.GetRepository<KYCLookUp>().Items on x.CategoryId equals category.Id
                                 where category.LookGroupId == 4
                                 select new CaseFileVM()
                                 {
                                     Priority = x.Priority,
                                     CaseNo = x.CaseNo,
                                     PrimAMLRisk = x.PrimAMLRisk,
                                     PrimSUITRisk = x.PrimSUITRisk,
                                     cf_ID = x.cf_ID,
                                     CaseName = x.CaseName,
                                     RFITypeId = x.RFITypeId,
                                     IsDuplicate = (x.Duplicate.HasValue && x.Duplicate.Value) ? "Yes" : "No",
                                     CategoryName = category.Value,
                                     CategoryId = category.Id,
                                     CreateDate = x.CreatedDate ?? DateTime.Now

                                 }).ToList();

                if (caseFiles.IsCollectionValid())
                {
                    var caseNos = caseFiles.Select(x => x.CaseNo).ToList();
                    var wfDetailsLkp = WorkflowAPI.GetCaseDetails(caseNos).ToDictionary(x => x.CaseNo, y => y);

                    caseFiles.ForEach(x => x.WfDetails = wfDetailsLkp.ContainsKey(x.CaseNo) ? wfDetailsLkp[x.CaseNo] : null);

                    if (userId.HasValue)
                    {
                        caseFiles = caseFiles.Where(x => x.WfDetails.UserId == userId).ToList();
                    }

                    if (onlyAssignedCases)
                    {
                        caseFiles = caseFiles.Where(x => x.WfDetails.UserId != 0).ToList();
                    }
                }

                return caseFiles;
            }
        }

        public void SaveCaseCategories(CaseFileCardPageVM caseFileCardPageVM)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var caseFileRep = uow.GetRepository<KYCCaseFile>();
                var caseFileDb = caseFileRep.Items.Include(x => x.Categories)
                                                  .FirstOrDefault(x => x.cf_ID == caseFileCardPageVM.CaseFileDetails.cf_ID);

                var categoryIds = new List<int>();

                if (caseFileCardPageVM.SelectedCategories.IsCollectionValid())
                    categoryIds = caseFileCardPageVM.SelectedCategories.Select(x => x.Id).ToList();

                var newCategories = uow.GetRepository<ReqCategory>().Items.Where(x => categoryIds.Contains(x.Id));

                //var categoriesClone = new List<ReqCategory>(caseFileDb.Categories);
                EntityFrameworkUtilities.SyncLists(newCategories, caseFileDb.Categories, x => x.Id,
                    (x) => caseFileDb.Categories.Add(x),
                    null,
                    (x) => caseFileDb.Categories.Remove(x)
                    );

                uow.SaveChanges();
            }
        }

        public ObservableCollection<ReqCategoryVM> GetCategories(int cf_id)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var categoryRep = uow.GetRepository<ReqCategory>();

                var categoriesDB = categoryRep.Items.Include(x => x.ReqClassification)
                                                    .Where(x => x.CaseFiles.Any(y => y.cf_ID == cf_id)).ToList();

                if (categoriesDB.IsCollectionValid())
                    return Mapper.Map<ObservableCollection<ReqCategoryVM>>(categoriesDB);
            }

            return new ObservableCollection<ReqCategoryVM>();
        }

    }
}
