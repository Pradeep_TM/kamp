﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Collections.ObjectModel;

namespace KAMP.KYC.BLL
{
    using AutoMapper;
    using Core.Repository.KYC;
    using KAMP.Core.UserManagement.AppCode;
    using ViewModels;
    using Core.FrameworkComponents;

    public partial class KYCBLL
    {
        public ReqClassificationVM AddClassification(ReqClassificationVM classification)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqClassification>();
                classification.ModifiedDate = classification.CreatedDate = DateTime.Now;
                classification.ModifiedBy = classification.CreatedBy = (int)AppVariables.KAMPPrincipal.UserId;
                var classificationDb = Mapper.Map<ReqClassification>(classification);
                rep.Insert(classificationDb);
                uow.SaveChanges();

                return Mapper.Map<ReqClassificationVM>(classificationDb);
            }
        }

        public ReqCategoryVM AddCategory(ReqCategoryVM category)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqCategory>();
                category.CreatedDate = category.ModifiedDate = DateTime.Now;
                category.ModifiedBy = category.CreatedBy = (int)AppVariables.KAMPPrincipal.UserId;
                var categoryDb = Mapper.Map<ReqCategory>(category);
                categoryDb.ReqClassification = null;
                rep.Insert(categoryDb);
                uow.SaveChanges();

                var savedCategory = Mapper.Map<ReqCategoryVM>(categoryDb);
                savedCategory.Classification = category.Classification;
                return savedCategory;
            }
        }

        public void SaveClassifications(List<ReqClassificationVM> classifications)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqClassification>();
                var classificationsLkp = classifications.ToDictionary(x => x.Id, x => x);
                var ids = classificationsLkp.Keys;
                var classificationsDb = rep.Items.Where(x => ids.Contains(x.Id)).ToList();

                classificationsDb.ForEach(c =>
                {
                    var classification = classificationsLkp[c.Id];
                    c.ClassificationName = classification.ClassificationName;
                    c.IsValid = classification.IsValid;
                    c.ModifiedBy = (int)AppVariables.KAMPPrincipal.UserId;
                    c.ModifiedDate = DateTime.Now;

                    rep.Update(c);
                });

                uow.SaveChanges();
            }
        }

        public void SaveCategories(List<ReqCategoryVM> categories)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqCategory>();
                var categoryLkp = categories.ToDictionary(x => x.Id, x => x);
                var ids = categoryLkp.Keys;
                var classificationsDb = rep.Items.Where(x => ids.Contains(x.Id)).ToList();

                classificationsDb.ForEach(c =>
                {
                    var category = categoryLkp[c.Id];
                    c.CategoryName = category.CategoryName;
                    c.IsValid = category.IsValid;
                    c.ReqClassificationId = category.Classification.Id;
                    c.ModifiedBy = (int)AppVariables.KAMPPrincipal.UserId;
                    c.ModifiedDate = DateTime.Now;

                    rep.Update(c);
                });

                uow.SaveChanges();
            }
        }

        public List<ReqClassificationVM> GetAllClassification()
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqClassification>();
                var classifications = rep.Items.Include(x => x.ReqCategory).ToList();
                return Mapper.Map<List<ReqClassificationVM>>(classifications);
            }
        }

        public List<ReqClassificationVM> GetValidClassifications()
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqClassification>();
                var classifications = rep.Items.Include(x => x.ReqCategory)
                                               .Where(x => x.IsValid).ToList();

                if (classifications.IsCollectionValid())
                    classifications.ForEach(x => x.ReqCategory = x.ReqCategory.Where(y => y.IsValid)
                                   .ToList());

                return Mapper.Map<List<ReqClassificationVM>>(classifications);
            }
        }

        public List<ReqCategoryVM> GetAllCategory()
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqCategory>();
                var categories = rep.Items.Include(x => x.ReqClassification).ToList();
                return Mapper.Map<List<ReqCategoryVM>>(categories);
            }
        }

        public void SaveRequirement(RequirementVM reqVM)
        {
            var requirement = Mapper.Map<Requirement>(reqVM);

            using (var kycUow = new KYCUnitOfWork())
            {
                var repo = kycUow.GetRepository<Requirement>();
                var optionsRepo = kycUow.GetRepository<ReqOption>();
                var gridColREpo = kycUow.GetRepository<ReqGridColumn>();

                if (requirement.Id == 0)
                {
                    repo.Insert(requirement);

                    if (requirement.ReqOption.IsCollectionValid())
                        requirement.ReqOption.Each(x => optionsRepo.Insert(x));

                    if (requirement.ReqGridColumn.IsCollectionValid())
                        requirement.ReqGridColumn.Each(x => gridColREpo.Insert(x));

                    kycUow.SaveChanges();

                    ////Insert Options
                    //if (requirement.ReqGridColumn.IsCollectionValid())
                    //    requirement.ReqGridColumn.Each(grdCol =>
                    //    {
                    //        if (grdCol.ReqOptions.IsCollectionValid())
                    //        {
                    //            grdCol.ReqOptions.Each(y =>
                    //            {
                    //                y.GridColumnId = grdCol.Id;
                    //                optionsRepo.Insert(y);
                    //            });
                    //        }
                    //    });

                    //kycUow.SaveChanges();
                }
                else
                {
                    UpdateRequirement(requirement, kycUow, repo, optionsRepo, gridColREpo);
                }
            }
        }

        private static void UpdateRequirement(Requirement requirement, KYCUnitOfWork kycUow, Core.Repository.Repository<Requirement> repo, Core.Repository.Repository<ReqOption> optionsRepo, Core.Repository.Repository<ReqGridColumn> gridColREpo)
        {
            var reqDb = repo.Items
                .Include(x => x.ReqOption)
                .Include(x => x.ReqGridColumn.Select(col => col.ReqOptions))
                .FirstOrDefault(x => x.Id == requirement.Id);

            //if (reqDb.ReqGridColumn.IsCollectionValid())
            //{
            //    var gridCOlIds = reqDb.ReqGridColumn.Select(y => y.Id)
            //    .ToList();

            //    var grdColOptions = optionsRepo.Items.Where(x => x.GridColumnId != null && gridCOlIds.Contains(x.GridColumnId.Value))
            //        .ToList()
            //        .GroupBy(x => x.GridColumnId)
            //        .ToDictionary(x => x.Key, x => x.ToList());

            //    reqDb.ReqGridColumn.Each(col =>
            //    {
            //        if (col.ReqTypeId == (int)RequirementType.SingleSelectOption || col.ReqTypeId == (int)RequirementType.MultiSelectOption)
            //        {
            //            col.ReqOptions = grdColOptions.SafeGet(col.Id);
            //        }
            //    });
            //}

            reqDb.ReqCategoryId = requirement.ReqCategoryId;
            reqDb.RequirementText = requirement.RequirementText;
            reqDb.ReqType = requirement.ReqType;

            EntityFrameworkUtilities.SyncLists(requirement.ReqOption, reqDb.ReqOption, (x) => x.Id,
                (x) => { x.RequirementId = requirement.Id; optionsRepo.Insert(x); },
                null,
                (x) => { optionsRepo.Delete(x); }
                );

            //var newGridColumns = new List<ReqGridColumn>();

            EntityFrameworkUtilities.SyncLists(requirement.ReqGridColumn, reqDb.ReqGridColumn, (x) => x.Id,
                (x) =>
                {
                    x.RequirementId = reqDb.Id;
                    gridColREpo.Insert(x);

                    if (x.ReqOptions.IsCollectionValid())
                    {
                        x.ReqOptions.Each(o => optionsRepo.Insert(o));
                    }
                },
                (s, d) =>
                {
                    d.ColumnName = s.ColumnName;
                    gridColREpo.Update(d);
                    EntityFrameworkUtilities.SyncLists(s.ReqOptions, d.ReqOptions, (x) => x.Id,
                        (x) => { x.GridColumnId = s.Id; optionsRepo.Insert(x); },
                        null,
                        (x) => { optionsRepo.Delete(x); });
                },
                (x) =>
                {
                    gridColREpo.Delete(x);
                }
                );

            repo.Update(reqDb);

            kycUow.SaveChanges();

            //if (newGridColumns.IsCollectionValid())
            //    newGridColumns.Each(grdCol =>
            //    {
            //        if (grdCol.ReqOptions.IsCollectionValid())
            //        {
            //            grdCol.ReqOptions.Each(y =>
            //            {
            //                y.GridColumnId = grdCol.Id;
            //                optionsRepo.Insert(y);
            //            });
            //        }
            //    });

            //kycUow.SaveChanges();
        }

        public ObservableCollection<RequirementVM> GetRequirements(int cf_ID)
        {
            /* If already saved get all the requirements from the response table directly
             * Get Category
             * GetRequirements base on category
             * Include Responses to the requirement
             */

            using (var uow = new KYCUnitOfWork())
            {
                var categoryIds = uow.GetRepository<KYCCaseFile>().Items
                                                                  .Where(x => x.cf_ID == cf_ID)
                                                                  .SelectMany(x => x.Categories.Select(y => y.Id))
                                                                  .ToList();

                var requirements = uow.GetRepository<Requirement>().Items
                                                                   .Include(x => x.ReqCategory)
                                                                   .Include(x => x.ReqCategory.ReqClassification)
                                                                   .Include(x => x.ReqOption)
                                                                   .Include(x => x.ReqGridColumn.Select(col => col.ReqOptions))
                                                                   .Include(x => x.ChildRequirements)
                                                                   .Where(x => x.ReqCategoryId == null || categoryIds.Contains(x.ReqCategoryId.Value))
                                                                   .ToList();


                if (requirements.IsCollectionValid())
                {
                    //Fill response for already saved requirements.
                    FillResponses(cf_ID, uow, requirements);

                    //Fill response option selection to requirement option selection (to display already save data)
                    SetOptionSelection(requirements);
                }

                var requirementsVM = Mapper.Map<ObservableCollection<RequirementVM>>(requirements);

                if (requirementsVM.IsCollectionValid())
                {
                    UpdateResponseGridCells(requirementsVM);
                }

                return requirementsVM;
            }
        }

        public void SaveResponses(RequirementVM requirement)
        {
            var responseModel = Mapper.Map<Response>(requirement.ResponseVM);

            using (var uow = new KYCUnitOfWork())
            {
                var responseRep = uow.GetRepository<Response>();

                if (requirement.ReqType == RequirementType.MultiSelectOption || requirement.ReqType == RequirementType.SingleSelectOption)
                {
                    SaveResponseOptions(responseModel, uow);
                }

                if (responseModel.ResponseGridCell.IsCollectionValid())
                {
                    SaveResponseGridCells(responseModel, uow);
                }

                if (responseModel.Id == 0)
                {
                    responseModel.Requirement = null;
                    responseRep.Insert(responseModel);
                }
                else
                {
                    var savedResponse = responseRep.Items.FirstOrDefault(x => x.Id == responseModel.Id);
                    savedResponse.ResponseText = responseModel.ResponseText;
                    savedResponse.ResponseState = responseModel.ResponseState;
                    savedResponse.Comment = responseModel.Comment;
                    responseRep.Update(savedResponse);
                }

                uow.SaveChanges();
            }
        }

        internal void DeleteResponseGridRow(ResponseGridRowVM rowVM)
        {
            var dataGridCells = Mapper.Map<List<ResponseGridCell>>(rowVM.ResponseGridCells);

            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ResponseGridCell>();
                var optionsRep = uow.GetRepository<ResponseOption>();

                var respGridCellIds = dataGridCells.Select(x => x.Id);
                var gridCellsFromDb = rep.Items
                                    .Include(x => x.ResponseOptions)
                                    .Where(x => respGridCellIds.Contains(x.Id))
                                    .ToList();

                gridCellsFromDb.ForEach(x =>
                {
                    rep.Delete(x);

                    if (x.ResponseOptions.IsCollectionValid())
                    {
                        x.ResponseOptions.Each(op =>
                        {
                            optionsRep.Delete(op);
                        });
                    }
                });

                uow.SaveChanges();
            }
        }

        private static void UpdateResponseGridCells(ObservableCollection<RequirementVM> requirementsVM)
        {
            requirementsVM.Each(x =>
                {
                    if (x.ResponseVM.IsNotNull() && x.ResponseVM.ResponseGridRows.IsCollectionValid())
                    {
                        var responseGridCells = x.ResponseVM.ResponseGridRows.SelectMany(r => r.ResponseGridCells);

                        responseGridCells.Each(respCol =>
                        {
                            var grdCol = respCol.ReqGridColumn;

                            var selectedRespOptionsLkp = new Dictionary<long, long>();
                            if (grdCol.ColumnType == ViewModels.RequirementType.MultiSelectOption)
                            {
                                if (respCol.ResponseOptions.IsCollectionValid())
                                    selectedRespOptionsLkp = respCol.ResponseOptions.ToDictionary(c => c.ReqOptionVM.Id, c => c.Id);

                                respCol.ResponseOptions = (grdCol.ColumnContent as MultiSelectReqDetailsVM).Options.Select(o => new ResponseOptionVM()
                                {
                                    ReqOptionId = o.Id,
                                    ReqOptionVM = o,
                                    ResponseId = x.ResponseVM.Id,
                                    OptionSelectionChanged = respCol.ResponseOptionSelectionChanged,
                                    IsSelected = selectedRespOptionsLkp.ContainsKey(o.Id),
                                    Id = selectedRespOptionsLkp.SafeGet(o.Id)
                                }).ToList();

                                if (respCol.ResponseOptions.IsCollectionValid())
                                {
                                    respCol.ResponseOptions.ForEach(ro => { if (ro.OptionSelectionChanged.IsNotNull()) ro.OptionSelectionChanged(); });
                                }
                            }

                            else if (grdCol.ColumnType == ViewModels.RequirementType.SingleSelectOption)
                            {
                                if (respCol.ResponseOption.IsNotNull())
                                    selectedRespOptionsLkp.Add(respCol.ResponseOption.ReqOptionVM.Id, respCol.ResponseOption.Id);

                                respCol.ResponseOptions = (grdCol.ColumnContent as SingleSelectReqDetailsVM).Options.Select(o => new ResponseOptionVM()
                                {
                                    ReqOptionId = o.Id,
                                    ReqOptionVM = o,
                                    ResponseId = x.ResponseVM.Id,
                                    OptionSelectionChanged = respCol.ResponseOptionSelectionChanged,
                                    Id = selectedRespOptionsLkp.SafeGet(o.Id)
                                }).ToList();
                            }
                        });
                    }
                });
        }

        private static void SetOptionSelection(List<Requirement> requirements)
        {
            requirements.ForEach(req =>
            {
                if (req.ReqOption.IsCollectionValid() && req.Response.IsCollectionValid())
                {
                    var responseOptions = req.Response.First().ResponseOption;

                    req.ReqOption.Each(x =>
                    {
                        x.IsValid = responseOptions.Any(y => y.ReqOptionId == x.Id);
                    });
                }
            });
        }

        private static void FillResponses(int cf_ID, KYCUnitOfWork uow, List<Requirement> requirements)
        {
            var reqIds = requirements.Select(x => x.Id).ToList();
            var responsesLkp = uow.GetRepository<Response>().Items
                                                           .Include(x => x.ResponseOption.Select(ro => ro.RequirementOption))
                                                           .Include(x => x.ResponseGridCell)
                                                           .Include(x => x.ResponseGridCell.Select(z => z.ResponseOptions.Select(ro => ro.RequirementOption)))
                                                           .Where(x => reqIds.Contains(x.RequirementId) && x.cf_ID == cf_ID)
                                                           .ToList()
                                                           .GroupBy(x => x.RequirementId)
                                                           .ToDictionary(x => x.Key, x => x.FirstOrDefault());

            //var gridCOlIds = requirements.SelectMany(x => x.ReqGridColumn.Select(y => y.Id))
            //    .ToList();

            //var grdColOptions = uow.GetRepository<ReqOption>().Items.Where(x => x.GridColumnId != null && gridCOlIds.Contains(x.GridColumnId.Value))
            //    .ToList()
            //    .GroupBy(x => x.GridColumnId)
            //    .ToDictionary(x => x.Key, x => x.ToList());

            requirements.ForEach(x =>
            {
                if (x.ReqGridColumn.IsCollectionValid())
                    x.ReqGridColumn.Each(col =>
                    {
                        var response = responsesLkp.SafeGet(x.Id);
                        if (response.IsNotNull())
                        {
                            //Order response Grid cells in the order of reqGridColumns
                            if (response.ResponseGridCell.IsCollectionValid())
                            {
                                response.ResponseGridCell = (from grdCol in x.ReqGridColumn
                                                             join respCol in response.ResponseGridCell on grdCol.Id equals respCol.ColumnId
                                                             orderby respCol.RowId, grdCol.Id
                                                             select respCol).ToList();

                            }

                            x.Response = new List<Response>() { response };
                        }

                        //if (col.ReqTypeId == (int)RequirementType.SingleSelectOption || col.ReqTypeId == (int)RequirementType.MultiSelectOption)
                        //{
                        //    col.ReqOptions = grdColOptions.SafeGet(col.Id);
                        //}
                    });
            });
        }

        private static void SaveResponseGridCells(Response responseModel, KYCUnitOfWork uow)
        {
            var responseOptionsRep = uow.GetRepository<ResponseOption>();
            var responseGridCellRep = uow.GetRepository<ResponseGridCell>();

            var savedGridCells = responseGridCellRep.Items.Where(x => x.ResponseId == responseModel.Id).Include(x => x.ResponseOptions).ToList();

            //Save response grid cells
            EntityFrameworkUtilities.SyncLists(responseModel.ResponseGridCell, savedGridCells, (x) => x.Id,
                                                    (x) =>
                                                    {
                                                        x.ReqGridColumn = null;

                                                        x.ResponseOptions.Each(o =>
                                                            {
                                                                o.RequirementOption = null;
                                                                responseOptionsRep.Insert(o);
                                                            });
                                                        responseGridCellRep.Insert(x);
                                                    },
                                                    (s, d) =>
                                                    {
                                                        d.ResponseText = s.ResponseText;
                                                        s.ReqGridColumn = null;
                                                        EntityFrameworkUtilities.SyncLists(s.ResponseOptions, d.ResponseOptions, x => x.Id
                                                            , x => { x.RequirementOption = null; x.GridCellId = s.Id; responseOptionsRep.Insert(x); },
                                                            null, x => responseOptionsRep.Delete(x));
                                                        responseGridCellRep.Update(d);
                                                    },
                                                    (x) =>
                                                    {
                                                        responseGridCellRep.Delete(x);
                                                    }
                                                    );

        }

        private static void SaveResponseOptions(Response responseModel, KYCUnitOfWork uow)
        {
            var responseOptionsRep = uow.GetRepository<ResponseOption>();
            var savedOptions = responseOptionsRep.Items.Where(x => x.ResponseId == responseModel.Id && x.GridCellId == null).ToList();

            EntityFrameworkUtilities.SyncLists(responseModel.ResponseOption, savedOptions, (x) => x.Id,
                (x) => { x.RequirementOption = null; responseOptionsRep.Insert(x); },
                null,
                (x) => responseOptionsRep.Delete(x)
                );
        }

        public List<ReqClassificationVM> GetAllClassificationWithRequirement()
        {
            using (var uow = new KYCUnitOfWork())
            {
                var rep = uow.GetRepository<ReqClassification>();
                var classifications = rep.Items.Include(x => x.ReqCategory.Select(y => y.Requirement))
                                                .OrderBy(x => x.ClassificationName)
                                                .ToList();

                var reqRep = uow.GetRepository<Requirement>();
                var requirements = reqRep.Items.Where(x => !x.ReqCategoryId.HasValue).ToList();

                if (requirements.IsCollectionValid())
                {
                    var defClassification = new ReqClassification()
                    {
                        ClassificationName = KYCConstants.DefRequirements,
                        ReqCategory = new List<ReqCategory>()
                            { 
                                new ReqCategory() 
                                { 
                                    CategoryName = KYCConstants.DefRequirements,
                                    Requirement = requirements
                                }
                            }
                    };

                    if (!classifications.IsCollectionValid())
                        classifications = new List<ReqClassification>();

                    classifications.Add(defClassification);
                }

                return Mapper.Map<List<ReqClassificationVM>>(classifications);
            }
        }

        public RequirementVM GetRequirements(long reqId)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var requirements = uow.GetRepository<Requirement>().Items
                                                                      .Include(x => x.ReqOption)
                                                                      .Include(x => x.ReqGridColumn)
                                                                      .Include(x => x.ReqCategory.ReqClassification)
                                                                      .Include(x => x.ReqGridColumn.Select(y => y.ReqOptions))
                                                                      .Include(x => x.ChildRequirements)
                                                                      .FirstOrDefault(x => x.Id == reqId);

                //var gridCOlIds = requirements.ReqGridColumn.Select(y => y.Id)
                //        .ToList();

                //var grdColOptions = uow.GetRepository<ReqOption>().Items.Where(x => x.GridColumnId != null && gridCOlIds.Contains(x.GridColumnId.Value))
                //    .ToList()
                //    .GroupBy(x => x.GridColumnId)
                //    .ToDictionary(x => x.Key, x => x.ToList());

                //if (requirements.ReqType == (int)RequirementType.Grid)
                //{
                //    requirements.ReqGridColumn.Each(col =>
                //    {
                //        if (col.ReqTypeId == (int)RequirementType.SingleSelectOption || col.ReqTypeId == (int)RequirementType.MultiSelectOption)
                //        {
                //            col.ReqOptions = grdColOptions.SafeGet(col.Id);
                //        }
                //    });
                //}

                var requirementsVM = Mapper.Map<RequirementVM>(requirements);
                return requirementsVM;
            }
        }

        internal bool CheckIfResponseIsSaved(long reqId)
        {
            using (var kycUow = new KYCUnitOfWork())
            {
                return kycUow.GetRepository<Response>().Items.Any(x => x.RequirementId == reqId);
            }
        }
    }
}

