﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace KAMP.KYC.BLL
{
    using AppCode;
    using Core.FrameworkComponents;
    using Core.Repository;
    using KAMP.Core.UserManagement.AppCode;
    using System;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Threading;
    using ViewModels;
    using Core.Workflow;
    using KAMP.Core.Repository.UM;
    using System.Windows;
    using KAMP.Core.Repository.WF;
    using KAMP.Core.Repository.KYC;
    using KAMP.Core.Repository.AuditLog;
    using KAMP.Core.Repository.UnitOfWork;

    using System.Xml;
    using KAMP.Core.Repository.Models.UserManagement;
    using System.Reflection;
    /// <summary>
    /// Wrapper to access Buisiness logic, Convert Model to ViewModel
    /// </summary>
    public partial class KYCBLL
    {
        KYCDataAccess _kycDataAccess;

        public KYCBLL()
        {
            new BootStrapper().Configure();
            _kycDataAccess = new KYCDataAccess();
        }

        public List<FormElementVM> GetTableSchema(int formId, string tableName, bool returnEmptyIfnotSaved = false)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var formEleRepo = uow.GetRepository<FormElement>();

                var formElements = formEleRepo.Items.Include(x => x.FormElementOption).Where(x => x.FormId == formId).ToList();
                bool isSaved = formElements.IsCollectionValid();

                if (!isSaved)
                {
                    formElements = returnEmptyIfnotSaved ? new List<FormElement>() : _kycDataAccess.GetTableSchema(tableName);
                }

                var formElementsVM = formElements.IsCollectionValid() ? Mapper.Map<List<FormElementVM>>(formElements) : new List<FormElementVM>();

                if (!isSaved)
                    formElementsVM.ForEach(fe =>
                    {
                        fe.FormId = formId;
                        fe.DisplayName = fe.Key;
                        fe.IsValid = true;

                        //Attach event handlers to Options
                        if (fe.Options.IsCollectionValid())
                        {
                            fe.Options.Each(x =>
                            {
                                x.OnDelete = fe.DeleteOption;
                            });
                        }
                    });




                return formElementsVM;
            }
        }

        public void SaveConfiguration(List<FormElementVM> formElementsVM)
        {
            //var formElements = Mapper.Map<List<FormElement>>(formElementsVM);

            using (var uow = new KYCUnitOfWork())
            {
                var formEleRepo = uow.GetRepository<FormElement>();
                var reqTypeRepo = uow.GetRepository<ReqType>();

                var freeTextId = (int)RequirementType.FreeText;
                var freeTextReq = reqTypeRepo.Items.FirstOrDefault(rt => rt.Id == freeTextId);

                formElementsVM.ForEach(fe =>
                {
                    if (fe.Id > 0 && fe.IsUpdated)
                    {
                        var feInDb = formEleRepo.Items.FirstOrDefault(x => x.Id == fe.Id);
                        feInDb.DisplayName = fe.DisplayName;
                        feInDb.IsValid = fe.IsValid;
                        feInDb.Tooltip = fe.ToolTip;
                        feInDb.IsMandatory = fe.IsMandatory;
                        feInDb.GroupName = fe.GroupName;
                        // formEleRepo.Update(feInDb);
                    }
                    else
                    {
                        var mappedFe = Mapper.Map<FormElement>(fe);
                        mappedFe.ReqType = freeTextReq;
                        formEleRepo.Insert(mappedFe);
                    }
                });

                uow.SaveChanges();
            }
        }

        public void SaveFormElementOptions(FormElementVM formElement)
        {
            using (var uow = new KYCUnitOfWork())
            {
                var optionRep = uow.GetRepository<FormElementOption>();
                var options = Mapper.Map<List<FormElementOption>>(formElement.Options);

                var optionsFromDB = optionRep.Items.Where(x => x.FormElementId == formElement.Id).ToList();

                EntityFrameworkUtilities.SyncLists(options, optionsFromDB, (x) => x.Id,
                    (op) =>
                    {
                        if (op.OptionText.IsNotEmpty())
                        {
                            op.FormElementId = formElement.Id;
                            optionRep.Insert(op);
                        }

                    },
                    (s, d) =>
                    {
                        d.OptionText = s.OptionText;
                        d.ModifiedBy = (int)AppVariables.KAMPPrincipal.UserId;

                        optionRep.Update(d);
                    },
                    (op) => { optionRep.Delete(op); });

                uow.SaveChanges();
            }
        }

        public BankDataPageVM GetBankData(int cfid)
        {
            var bankData = new BankDataPageVM();

            using (var uow = new KYCUnitOfWork())
            {
                var frmEleRep = uow.GetRepository<FormElement>();
                var bankDataDt = _kycDataAccess.GetReadOnlyData("Select * from KYC_BankData");
                var bankDataRow = bankDataDt.Rows.Count > 0 ? bankDataDt.Rows[0] : null;
                var formElements = frmEleRep.Items.Where(fe => fe.FormId == (int)KYCForms.BankData).ToList();
                var formEelmentsVM = Mapper.Map<IList<FormElementVM>>(formElements);

                if (bankDataRow == null || !formEelmentsVM.IsCollectionValid())
                {
                    return bankData;
                }

                var formEleGroups = bankDataDt.Columns.Cast<DataColumn>()
                    .Select(col => new FormElementNodeVM()
                    {
                        FormElement = formEelmentsVM.FirstOrDefault(fe => fe.Key == col.ColumnName),
                        Value = Convert.ToString(bankDataRow[col])
                    })
                    .Where(fe => fe.FormElement.GroupName != String.Empty && fe.FormElement.IsValid)
                    .GroupBy(fe => fe.FormElement.GroupName)
                    .Select(feg => new FormElementGroupVM()
                    {
                        GroupName = feg.Key,
                        FormElements = new System.Collections.ObjectModel.ObservableCollection<FormElementNodeVM>(feg.ToList())
                    }).ToList();

                bankData.FormEleGrps.AddRange(formEleGroups);
                return bankData;
            }
        }

        public AccountVM GetAccountData(int cfid)
        {
            var accountsVm = new AccountVM();

            using (var uow = new KYCUnitOfWork())
            {
                var frmEleRep = uow.GetRepository<FormElement>();
                var accountsDt = _kycDataAccess.GetReadOnlyData("select * from KYC_Accounts where cf_ID={0}".ToFormat(cfid));
                var accountsRow = accountsDt.Rows.Count > 0 ? accountsDt.Rows[0] : null;
                if (accountsRow == null)
                {
                    return accountsVm;
                }

                var formElements = frmEleRep.Items.Where(fe => fe.FormId == (int)KYCForms.Accounts).ToList();
                var formEelmentsVM = Mapper.Map<IList<FormElementVM>>(formElements);

                var columnsToRemove = accountsDt.Columns.Cast<DataColumn>()
                    .Where(x => !formElements.Exists(y => y.Key == x.ColumnName && y.IsValid)).ToList();
                columnsToRemove.ForEach(x =>
                {
                    accountsDt.Columns.Remove(x);
                });

                accountsDt.Columns.Cast<DataColumn>().Each(x => x.ColumnName = formEelmentsVM.FirstOrDefault(y => y.Key == x.ColumnName).DisplayName);

                accountsVm.AccountsData = accountsDt;
                return accountsVm;
            }
        }

        public FormElementVM AddNewFormElement(FormElementVM formElementVm)
        {
            var formElement = Mapper.Map<FormElement>(formElementVm);

            using (var uow = new KYCUnitOfWork())
            {
                var formEleRepo = uow.GetRepository<FormElement>();

                //Extra logic are added here
                //
                if (formEleRepo.Items.Any(x => x.Key.ToLower().Equals(formElement.Key.ToLower())))
                    throw new BusinessException("Column with the same name is already exists.");
                ////

                formEleRepo.Insert(formElement);
                var formEleOptionsRepo = uow.GetRepository<FormElementOption>();

                //SaveFormElementOptions(formElementVm);                
                formElement.FormElementOption.Each(x => formEleOptionsRepo.Insert(x));
                uow.SaveChanges();
                return Mapper.Map<FormElementVM>(formElement);
            }
        }

        public ObservableCollection<KPMGEditableGrpVM> GetKPMGEditableData(int cfid)
        {
            var kpmgEditableGrps = new ObservableCollection<KPMGEditableGrpVM>();

            using (var uow = new KYCUnitOfWork())
            {
                var frmEleRep = uow.GetRepository<FormElement>();
                var kpmgEditableRep = uow.GetRepository<KPMGEditable>();
                var kpmgEditableData = kpmgEditableRep.Items.Include(x => x.FormElementOptions)
                                                            .Include(x => x.FormElement.FormElementOption)
                                                            .Where(x => x.cf_ID == cfid).ToList();

                //Kpmg editable data is saved already
                if (kpmgEditableData.IsCollectionValid())
                {

                    //Get unsaved form elements
                    var kpmgEditableFormEleIds = kpmgEditableData.Select(x => x.FormElement.Id).ToList();
                    var formElements = frmEleRep.Items.Include(x => x.FormElementOption)
                                                      .Where(x => x.FormId == (int)KYCForms.KPMGEditable && x.IsValid && !kpmgEditableFormEleIds.Contains(x.Id))
                                                      .ToList();

                    if (formElements.IsCollectionValid())
                    {
                        kpmgEditableData.AddRange(formElements.Select(x => new KPMGEditable()
                        {
                            cf_ID = cfid,
                            CreatedBy = (int)(AppVariables.KAMPPrincipal).UserId,
                            ModifiedBy = (int)(AppVariables.KAMPPrincipal).UserId,
                            CreatedDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            FormElement = x
                        }).ToList());
                    }

                    var kpmgEditableGrpsVM = kpmgEditableData.GroupBy(x => x.FormElement.GroupName)
                    .Select(x =>
                        {
                            var kpmgEditablesVM = Mapper.Map<ObservableCollection<KPMGEditableVM>>(x.ToList());

                            //Fill form elementOptionVM from Options list to set the selected option in UI
                            kpmgEditablesVM.Each(e =>
                            {
                                if (e.FormElement.ValueType == (int)RequirementType.SingleSelectOption && e.FormElementOption.IsNotNull())
                                {
                                    e.FormElementOption = e.FormElement.Options.FirstOrDefault(o => o.Id == e.FormElementOption.Id);
                                    e.IsUpdated = false;
                                }
                            });

                            var kpmgEditablesGrp = new KPMGEditableGrpVM()
                                {
                                    KPMGEditables = kpmgEditablesVM,
                                    GroupName = x.Key
                                };


                            return kpmgEditablesGrp;
                        }).ToList();

                    kpmgEditableGrps.AddRange(kpmgEditableGrpsVM);

                }
                else
                {
                    var formElements = frmEleRep.Items.Include(x => x.FormElementOption)
                                                      .Where(x => x.FormId == (int)KYCForms.KPMGEditable && x.IsValid).ToList();

                    if (!formElements.IsCollectionValid())
                        return kpmgEditableGrps;

                    var kpmgEditableGrpsVM = formElements.Select(x => new KPMGEditableVM()
                    {
                        cf_ID = cfid,
                        CreatedBy = (int)(AppVariables.KAMPPrincipal).UserId,
                        ModifiedBy = (int)(AppVariables.KAMPPrincipal).UserId,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        FormElement = Mapper.Map<FormElementVM>(x)
                    })
                    .GroupBy(x => x.FormElement.GroupName)
                    .Select(x => new KPMGEditableGrpVM()
                    {
                        KPMGEditables = new ObservableCollection<KPMGEditableVM>(x.ToList()),
                        GroupName = x.Key
                    }).ToList();

                    kpmgEditableGrps.AddRange(kpmgEditableGrpsVM);
                }
            }

            return kpmgEditableGrps;
        }

        public void SaveKPMGEditables(List<KPMGEditableVM> kpmgEditables)
        {
            var kpmgEditablesModel = Mapper.Map<List<KPMGEditable>>(kpmgEditables);
            using (var uow = new KYCUnitOfWork())
            {
                var kpmgEditableRep = uow.GetRepository<KPMGEditable>();
                var formElementOptionRep = uow.GetRepository<FormElementOption>();

                var formElementOptionIdsLkp = new Dictionary<string, List<int>>();
                var kpmgEditableIds = new List<long>();

                kpmgEditablesModel.ForEach(x =>
                {
                    if (x.FormElementOptions.IsCollectionValid())
                    {
                        formElementOptionIdsLkp.Add(x.FormElement.Key, x.FormElementOptions.Select(fe => fe.Id).ToList());
                    }

                    if (x.Id != 0 && x.IsUpdated)
                    {
                        kpmgEditableIds.Add(x.Id);
                    }
                });

                var formEleIds = formElementOptionIdsLkp.Values.SelectMany(x => x).ToList();
                var formElementOptionsLkp = formElementOptionRep.Items.Where(x => formEleIds.Contains(x.Id))
                                            .ToList()
                                            .ToDictionary(x => x.Id, x => x);
                var kpmgEditablesLkp = kpmgEditableRep.Items.Include(x => x.FormElementOptions)
                                            .Where(x => kpmgEditableIds.Contains(x.Id))
                                            .ToList()
                                            .ToDictionary(x => x.Id, x => x);

                kpmgEditablesModel.ForEach(x =>
                {
                    //Fill FormElementOptions
                    var formEleOptionIds = formElementOptionIdsLkp.SafeGet(x.FormElement.Key);
                    var formEleOptions = formEleOptionIds.IsCollectionValid() ? formEleOptionIds.Select(feo => formElementOptionsLkp.SafeGet(feo)).ToList() : null;
                    x.FormElementOptions = new List<FormElementOption>();

                    if (formEleOptions.IsCollectionValid())
                    {
                        formEleOptions.ForEach(feo => x.FormElementOptions.Add(feo));
                    }

                    if (x.Id == 0)
                    {
                        var kpmgEditableModel = Mapper.Map<KPMGEditable>(x);
                        kpmgEditableModel.cf_ID = AppVariables.CaseFileCardPageVM.CaseFileDetails.cf_ID;
                        kpmgEditableModel.FormElementId = x.FormElement.Id;
                        kpmgEditableModel.FormElement = null;

                        kpmgEditableRep.Insert(kpmgEditableModel);
                    }
                    else
                    {
                        if (x.IsUpdated)
                        {
                            var kpmgEditableDb = kpmgEditablesLkp.SafeGet(x.Id);
                            kpmgEditableDb.Value = x.Value;
                            kpmgEditableDb.FormElementId = x.FormElement.Id;

                            EntityFrameworkUtilities.SyncLists(x.FormElementOptions, kpmgEditableDb.FormElementOptions, f => f.Id,
                                                (f) => kpmgEditableDb.FormElementOptions.Add(f),
                                                null,
                                                (f) => kpmgEditableDb.FormElementOptions.Remove(f)
                                                );
                        }
                    }
                });

                uow.SaveChanges();
            }
        }

        /// <summary>
        /// This method is used to get all cases from  KYC CASE FILE
        /// </summary>
        /// <returns></returns>
        internal ObservableCollection<CaseDetails> GetAllCases()
        {
            var caseDetails = new ObservableCollection<CaseDetails>();
            using (var kycUnitOfWork = new KYCUnitOfWork())
            {
                var casesRepo = kycUnitOfWork.GetRepository<KYCCaseFile>();
                var formElementRepo = kycUnitOfWork.GetRepository<KYCLookUp>();
                var query = from cases in casesRepo.Items
                            join frmEleOption in formElementRepo.Items on cases.CategoryId equals frmEleOption.Id
                            where frmEleOption.LookGroupId == 4
                            select new { Cases = cases, Category = frmEleOption };

                var caseList = query.Select(x => x.Cases).ToList();
                var catagories = query.Select(x => x.Category).ToList().Distinct();
                var wFCaseDetails = WorkflowAPI.GetCaseDetails(caseList.Select(x => x.CaseNo).ToList());
                foreach (var item in caseList)
                {
                    var caseDetail = Mapper.Map<CaseDetails>(item);
                    var category = catagories.FirstOrDefault(x => x.Id == caseDetail.CategoryId);
                    caseDetail.CategoryName = category.IsNotNull() ? category.Value : string.Empty;
                    var caseRelatedData = wFCaseDetails.FirstOrDefault(x => x.CaseNo.ToLower().Equals(item.CaseNo.ToLower()));
                    caseDetail = Mapper.Map<CaseDetail, CaseDetails>(caseRelatedData, caseDetail);
                    caseDetails.Add(caseDetail);
                }

            }
            return caseDetails;
        }

        /// <summary>
        /// Get User by StateId
        /// </summary>
        /// <param name="stateId">stateId</param>
        /// <returns>List of users</returns>
        internal List<User> GetUsersByStateId(int stateId)
        {
            using (var kycUOW = new KYCUnitOfWork())
            {
                var userRepo = kycUOW.GetRepository<User>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var users = userRepo.Items.Include(user => user.UserInGroup.Select(x => x.Group)).Where(x => x.IsActive && x.UserInGroup.Any(gm => gm.IsActive && gm.ModuleTypeId == appcontext.Module.Id && gm.Group.States.Any(s => s.ModuleTypeId == appcontext.Module.Id && s.Id == stateId))).ToList();
                //var users = userRepo.Items.Include(user => user.UserInGroup.Select(x => x.Group)).Where(x => x.IsActive && x.UserInGroup.Any(gm => gm.Group.States.Any(s => s.Id == stateId))).ToList();
                return users;
            }
        }

        internal List<User> GetUsersFromAnalystGroup()
        {
            using (var kycUOW = new KYCUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var userRepo = kycUOW.GetRepository<User>();
                return userRepo.Items.Include(x => x.UserInGroup).Where(x => x.IsActive && x.UserInGroup.
                    Any(gp => gp.IsActive && gp.ModuleTypeId == appcontext.Module.Id && gp.Group.GroupName.ToLower() == Core.Workflow.Helpers.Helper.Group.Analyst.ToLower())).ToList();
            }
        }

        internal List<MasterEntity> GetCaseCategory(string categoryName)
        {
            using (var kycUOW = new KYCUnitOfWork())
            {
                var formElementRepo = kycUOW.GetRepository<FormElement>();
                var formElement = formElementRepo.Items.Include(x => x.FormElementOption).FirstOrDefault(x => x.Key.Trim().Equals(categoryName.Trim()));
                return formElement.IsNotNull() ? formElement.FormElementOption.Select(x => new MasterEntity { Code = x.Value.Value, Name = x.OptionText }).ToList() : null;
            }
        }


        internal List<User> GetUsers()
        {
            using (var kycUOW = new KYCUnitOfWork())
            {
                var userRepo = kycUOW.GetRepository<User>();
                return userRepo.Items.Where(x => x.IsActive).ToList();
            }
        }

        internal List<Core.Repository.WF.State> GetStates()
        {
            using (var kycUOW = new KYCUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var stateRepo = kycUOW.GetRepository<State>();
                return stateRepo.Items.Where(x => x.ModuleTypeId == appcontext.Module.Id).OrderBy(x => x.StateName).ToList();
            }
        }


        #region LookUp

        internal List<MasterEntity> GetCategoryFilters(string categoryFilter)
        {
            using (var kycUOW = new KYCUnitOfWork())
            {
                var lookUpRepo = kycUOW.GetRepository<KYCLookUp>();
                var val = lookUpRepo.Items.Include(x => x.KYCLookUpGroups);
                var grpValues = lookUpRepo.Items.Include(x => x.KYCLookUpGroups).Where(x => x.KYCLookUpGroups.Name.Trim().Equals(KYCConstants.CategoryFilter.Trim())).ToList();
                return grpValues.IsNotNull() ? grpValues.Select(x => new MasterEntity { Code = x.Id, Name = x.Value }).ToList() : null;

            }

        }

        internal ObservableCollection<KYCLookUpGroups> GetLookGroups()
        {
            var collection = new ObservableCollection<KYCLookUpGroups>();
            using (var kycUnitOfWork = new KYCUnitOfWork())
            {
                var data = kycUnitOfWork.GetRepository<KYCLookUpGroups>().Items.ToList();
                data.Insert(0, new KYCLookUpGroups { Id = 0, Name = KYCConstants.DefaultDDLValue });
                data.ForEach(collection.Add);
            }
            return collection;
        }

        internal ObservableCollection<KYCLookUp> GetLookUp(int lookUpGroupId)
        {
            var collection = new ObservableCollection<KYCLookUp>();
            using (var tlbUnitOfWork = new KYCUnitOfWork())
            {
                var data = tlbUnitOfWork.GetRepository<KYCLookUp>().Items.Where(x => x.LookGroupId == lookUpGroupId).ToList();
                data.ForEach(collection.Add);
            }
            return collection;
        }

        internal void AddLookUp(KYCLookUp lookUpItem)
        {
            using (var tlbUnitOfWork = new KYCUnitOfWork())
            {
                var repo = tlbUnitOfWork.GetRepository<KYCLookUp>();
                repo.Insert(lookUpItem);
                tlbUnitOfWork.SaveChanges();
            }
        }

        internal void EditLookUp(KYCLookUp lookUpItem)
        {
            using (var tlbUnitOfWork = new KYCUnitOfWork())
            {
                var repo = tlbUnitOfWork.GetRepository<KYCLookUp>();
                var itemToUpdate = repo.Items.FirstOrDefault(x => x.Id == lookUpItem.Id && x.LookGroupId == lookUpItem.LookGroupId);
                itemToUpdate.Value = lookUpItem.Value;
                itemToUpdate.Reference = lookUpItem.Reference;
                tlbUnitOfWork.SaveChanges();
            }
        }

        internal void DeleteLookUp(KYCLookUp item)
        {
            using (var tlbUnitOfWork = new KYCUnitOfWork())
            {
                var repo = tlbUnitOfWork.GetRepository<KYCLookUp>();
                var itemToDelete = repo.Items.FirstOrDefault(x => x.Id == item.Id && x.LookGroupId == item.LookGroupId);
                repo.Delete(itemToDelete);
                tlbUnitOfWork.SaveChanges();

            }
        }
        #endregion

        #region Audit Logic
        public List<ConfigurationTable> GetAuditConfiguration()
        {
            using (var auditUOW = new AuditUnitOfWork())
            {
                var appcontext = System.Windows.Application.Current.Properties["Context"] as Context;
                var auditRepo = auditUOW.GetRepository<ConfigurationTable>();
                return auditRepo.Items.Include(x => x.EntityModelXmls).Where(x => x.ModuleId == appcontext.Module.Id).ToList();
            }
        }

        public void AddConfiguration(ConfigurationTable config)
        {
            using (var auditUOW = new AuditUnitOfWork())
            {
                var auditRepo = auditUOW.GetRepository<ConfigurationTable>();
                auditRepo.Insert(config);
                auditUOW.SaveChanges();
            }
        }

        public void UpdateConfiguration(ConfigurationTable config)
        {
            using (var auditUOW = new AuditUnitOfWork())
            {
                var auditRepo = auditUOW.GetRepository<ConfigurationTable>();
                var auditData = auditRepo.Items.FirstOrDefault(x => x.Id == config.Id);
                auditData.ConfigurationXml = config.ConfigurationXml;

                foreach (var item in config.EntityModelXmls)
                {
                    var auditRepoXml = auditUOW.GetRepository<EntityModelXml>();
                    var auditDataXml = auditRepoXml.Items.FirstOrDefault(x => x.ConfigurationId == item.ConfigurationId && x.Action == item.Action);
                    if (auditDataXml.IsNull())
                    {
                        auditRepoXml.Insert(item);
                    }
                    else
                        auditDataXml.EntityXml = item.EntityXml;
                }
                auditUOW.SaveChanges();
            }
        }
        #endregion

        #region History
        internal ObservableCollection<KYCUser> GetExistingUsers()
        {
            var userList = new ObservableCollection<KYCUser>();
            using (var kycUnitOfWork = new KYCUnitOfWork())
            {
                var userRepo = kycUnitOfWork.GetRepository<User>();
                var context = Application.Current.Properties["Context"] as Context;
                if (context.IsNull()) return null;
                var users = userRepo.Items.Include(x => x.UserInGroup.Select(y => y.Group.States))
                                .Where(u => u.UserInGroup.Any(gp => gp.ModuleTypeId == context.Module.Id) && u.IsActive).OrderBy(x => x.UserName).ToList();

                new BootStrapper().Configure();
                foreach (var user in users)
                {
                    var usr = Mapper.Map<KYCUser>(user);
                    userList.Add(usr);
                }
                userList.OrderBy(x => x.UserName);
                userList.Insert(0, new KYCUser { UserId = 0, UserName = KYCConstants.DefaultDDLValue });
                return userList;
            }

        }

        internal ObservableCollection<HistoryModel> GetApplicationHistory()
        {
            var collection = new ObservableCollection<HistoryModel>();
            using (var auditUOW = new AuditUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var logDetailsRepo = auditUOW.GetRepository<LogDetail>();
                var getEntities = GetEntities();
                var logDetails = logDetailsRepo.Items.Include(x => x.Logs)
                    .Where(x => (!x.EntityName.ToLower().Equals(KYCConstants.KpmgEditable.ToLower())) && getEntities.Contains(x.EntityName) && x.ModuleId == appcontext.Module.Id).ToList();

                foreach (var item in logDetails)
                {
                    var list = GetDataFromXlm(item);
                    if (list.IsNotNull())
                        collection.AddRange(list);
                }
                FillFormElementFields(collection);
            }
            return collection;
        }

        private static void FillFormElementFields(ObservableCollection<HistoryModel> collection)
        {
            if (collection.IsCollectionValid())
            {
                var dataByEntityName = collection.GroupBy(x => x.Entity);

                foreach (var entityHistory in dataByEntityName)
                {
                    if (entityHistory.Key.Equals(KYCConstants.FormElement))
                    {
                        using (var uow = new KYCUnitOfWork())
                        {
                            var uIdsLkp = entityHistory.Select(x => new KeyValuePair<int, HistoryModel>(x.Uid, x)).ToList();
                            var ids = uIdsLkp.Select(kv => kv.Key).Distinct().ToList();
                            var EditableRep = uow.GetRepository<FormElement>();
                            var formElementNames = EditableRep.Items.Where(x => ids.Contains((int)x.Id))
                                                                     .Select(x => new { Id = x.Id, Name = x.DisplayName }).Distinct().ToList();
                            var elementNames = formElementNames.ToDictionary(x => x.Id, x => x.Name);
                            entityHistory.Each(x =>
                            {
                                x.FieldName = elementNames.SafeGet(x.Uid);
                            });
                        }
                    }
                }
            }
        }

        internal ObservableCollection<HistoryModel> GetCaseHistory(List<string> EntityName, int cfID, string userName = null)
        {
            var collection = new ObservableCollection<HistoryModel>();
            using (var auditUOW = new AuditUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var logDetailsRepo = auditUOW.GetRepository<LogDetail>();

                var query = logDetailsRepo.Items.Include(x => x.Logs)
                            .Where(x => EntityName.Contains(x.EntityName) && x.ModuleId == appcontext.Module.Id);


                if (userName.IsNotEmpty())
                    query = query.Where(x => x.CreatedBy.ToLower().Equals(userName.ToLower()) && x.ModuleId == appcontext.Module.Id);

                if (cfID != 0)
                {
                    query = query.Where(x => x.cf_ID == cfID);
                }

                var logDetails = query.ToList();

                foreach (var item in logDetails)
                {
                    var list = GetDataFromXlm(item);
                    if (list.IsNotNull())
                        collection.AddRange(list);
                }
                FillEntityDetails(collection);
            }
            return collection;
        }

        internal ObservableCollection<HistoryModel> GetLoginHistory()
        {
            var collection = new ObservableCollection<HistoryModel>();
            using (var uMUOW = new UMUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var loginDetailsRepo = uMUOW.GetRepository<UserLoginHistory>();
                var loginDetails = loginDetailsRepo.Items.Where(x => x.ModuleTypeId == appcontext.Module.Id).ToList();
                foreach (var item in loginDetails)
                {
                    HistoryModel model = new HistoryModel
                    {
                        User = item.UserName,
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        LoginTime = item.LoginTime
                    };
                    collection.Add(model);
                }
            }
            return collection;
        }

        private ObservableCollection<HistoryModel> GetDataFromXlm(LogDetail item)
        {
            var data = new ObservableCollection<HistoryModel>();

            if (item.Logs.IsNull() || item.Logs.Count == 0) return null;
            XmlDocument xDoc = new XmlDocument();

            xDoc.InnerXml = item.Logs.FirstOrDefault().AuditXml;
            var node = xDoc.ChildNodes;
            if (node.Item(0).ChildNodes.Count == 0) return null;

            var nodes = node.Item(0).ChildNodes;
            if (nodes.Count == 0) return null;

            foreach (var itemNode in nodes)
            {
                var caseHistory = new HistoryModel();
                caseHistory.Entity = item.EntityName;
                caseHistory.Action = item.Action;
                caseHistory.TimeStamp = item.DateTime;
                caseHistory.User = item.CreatedBy;
                caseHistory.FieldName = ((System.Xml.XmlElement)(itemNode)).Name;
                caseHistory.Uid = item.EntityUId;
                var valuesArray = ((System.Xml.XmlElement)(itemNode)).InnerText.Split("#$#".ToCharArray());
                if (item.Action.EqualsIgnoreCase("Added"))
                {
                    caseHistory.ModifiedValue = KYCConstants.NotApplicableText;
                    caseHistory.PreviousValue = valuesArray[0];
                }

                else
                {
                    caseHistory.ModifiedValue = valuesArray.LastOrDefault();
                    caseHistory.PreviousValue = valuesArray[0];
                }
                data.Add(caseHistory);
            }

            return data;
        }

        private void FillEntityDetails(ObservableCollection<HistoryModel> data)
        {
            if (data.IsCollectionValid())
            {
                var dataByEntityName = data.GroupBy(x => x.Entity);

                foreach (var entityHistory in dataByEntityName)
                {
                    switch (entityHistory.Key)
                    {
                        case KYCConstants.KpmgEditable:
                            using (var uow = new KYCUnitOfWork())
                            {
                                var uIdsLkp = entityHistory.Select(x => new KeyValuePair<int, HistoryModel>(x.Uid, x)).ToList();
                                var kpmgEditableRep = uow.GetRepository<KPMGEditable>();
                                var ids = uIdsLkp.Select(kv => kv.Key).Distinct().ToList();
                                var formElementNames = kpmgEditableRep.Items.Where(x => ids.Contains((int)x.Id))
                                                                         .Select(x => new { Id = x.Id, Name = x.FormElement.DisplayName }).Distinct().ToList();
                                var elementNames = formElementNames.ToDictionary(x => x.Id, x => x.Name);
                                entityHistory.Each(x =>
                                {
                                    x.FieldName = "KPMG Editable - " + elementNames.SafeGet(x.Uid);
                                });
                            }
                            break;
                        case KYCConstants.FormElement:
                            using (var uow = new KYCUnitOfWork())
                            {
                                var uIdsLkp = entityHistory.Select(x => new KeyValuePair<int, HistoryModel>(x.Uid, x)).ToList();
                                var ids = uIdsLkp.Select(kv => kv.Key).Distinct().ToList();
                                var EditableRep = uow.GetRepository<FormElement>();
                                var formElementNames = EditableRep.Items.Where(x => ids.Contains((int)x.Id))
                                                                         .Select(x => new { Id = x.Id, Name = x.DisplayName }).Distinct().ToList();
                                var elementNames = formElementNames.ToDictionary(x => x.Id, x => x.Name);
                                entityHistory.Each(x =>
                                {
                                    x.FieldName = "Screen Configuration - " + elementNames.SafeGet(x.Uid);
                                });
                            }
                            break;
                        case KYCConstants.ReqCategory:
                        case KYCConstants.ReqClassification:
                        case KYCConstants.Requirement:
                        case KYCConstants.Response:
                        case KYCConstants.FormElementOption:
                            AssignPriorEntityForFields(entityHistory, entityHistory.Key);
                            break;
                        default:
                            break;

                    }

                }
            }
        }

        private void AssignPriorEntityForFields(IGrouping<string, HistoryModel> entityHistory, string entityName)
        {
            string resolvedName = null;

            var lookup = EntityLookup();
            if (!string.IsNullOrEmpty(entityName))
                resolvedName = lookup.FirstOrDefault(x => x.Key.Equals(entityName)).Value;
            if (!entityName.Equals(KYCConstants.KpmgEditable) && !entityName.Equals(KYCConstants.FormElement))
            {
                if (entityHistory.IsCollectionValid())
                {
                    entityHistory.Each(x =>
                    {
                        x.FieldName = resolvedName + " - " + x.FieldName;
                    });
                }
            }

        }

        public List<string> GetEntities()
        {
            var context = Application.Current.Properties["Context"] as Context;
            if (context.IsNull()) return new List<string>();
            List<string> Types = new List<string>();
            var classes = Assembly.GetAssembly(typeof(ConfigurationTable)).ExportedTypes;
            foreach (var type in classes)
            {
                var attributes = type.CustomAttributes;

                foreach (var attribute in attributes)
                {
                    if (attribute.AttributeType.Name.Equals(typeof(LogEntityAttribute).Name))
                    {
                        if (attribute.ConstructorArguments.FirstOrDefault().Value.ToString().ToLower().Equals(context.Module.Name.ToLower()))
                        {
                            Types.Add(type.Name);
                        }
                    }
                }
            }
            return Types;
        }

        public dynamic ResolveEntities(List<string> availableEntities, string selectedEntity)
        {
            string selectedValue = null;

            var lookup = EntityLookup();
            if (selectedEntity == null && availableEntities.IsCollectionValid())
            {
                var resolvedNames = lookup.Where(x => availableEntities.Contains(x.Key)).Select(s => s.Value).ToList<string>();
                return resolvedNames.IsCollectionValid() ? resolvedNames : null;
            }
            else
            {
                return selectedValue = lookup.FirstOrDefault(x => x.Value.Equals(selectedEntity)).Key;
            }

        }

        public Dictionary<string, string> EntityLookup()
        {
            Dictionary<string, string> entityLookup = new Dictionary<string, string>();

            entityLookup.Add(KYCConstants.KpmgEditable, "KPMG Editable");
            entityLookup.Add(KYCConstants.FormElement, "Screen Configuration");
            entityLookup.Add(KYCConstants.ReqCategory, "Requirement Category");
            entityLookup.Add(KYCConstants.ReqClassification, "Requirement Classification");
            entityLookup.Add(KYCConstants.Requirement, "Requirement");
            entityLookup.Add(KYCConstants.Response, "Response");
            entityLookup.Add(KYCConstants.FormElementOption, "Form Element Options");

            return entityLookup;
        }
        #endregion

    }
}
