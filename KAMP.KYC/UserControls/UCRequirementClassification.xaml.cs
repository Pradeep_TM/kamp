﻿using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCRequirementClassification.xaml
    /// </summary>
    using Core.FrameworkComponents;
    public partial class UCRequirementClassification : UserControl, IPageControl
    {
        ReqClassificationPageVM _viewModel;
        public UCRequirementClassification()
        {
            InitializeComponent();
        }

        public dynamic InitializeViewModel()
        {
            _viewModel = new ReqClassificationPageVM();            
            return _viewModel;
        }

        public string PageName
        {
            get { return KYCPageControls.RequirementClassification; }
        }
    }
}
