﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.KYC;
    using KAMP.KYC.BLL;
    /// <summary>
    /// Interaction logic for AddEditLookUp.xaml
    /// </summary>
    public partial class AddEditLookUp : UserControl
    {
        KYCLookUp lookUpItem;
        public Action Close;

        public AddEditLookUp(Core.Repository.KYC.KYCLookUp item, KYCLookUpGroups group)
        {
            InitializeComponent();
            if (item.IsNull())
            {
                lookUpItem = new Core.Repository.KYC.KYCLookUp();
                isAdd = true;
                BtnAddUpdate.Content = "Add";
            }
            else
            {
                lookUpItem = item;
                BtnAddUpdate.Content = "Update";
            }
            lookUpItem.LookGroupId = group.Id;
            DataContext = lookUpItem;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (Close.IsNotNull())
            {
                Close();
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (lookUpItem.IsNull() || lookUpItem.Value.IsNull() || string.IsNullOrEmpty(lookUpItem.Value.Trim()))
            {
                MessageBoxControl.Show("Look Up value can not be empty.", MessageBoxButton.OK, MessageType.Error);
                return;
            }
            var bLL = new KYCBLL();
            if (isAdd)//Add
            {
                bLL.AddLookUp(lookUpItem);
                if (Close.IsNotNull())
                {
                    Close();
                }
                MessageBoxControl.Show("Look up added successfully");
               
            }
            else
            {
                bLL.EditLookUp(lookUpItem);
                if (Close.IsNotNull())
                {
                    Close();
                }
                MessageBoxControl.Show("Look up updated successfully");
               
            }
        }

        public bool isAdd { get; set; }
    }
}
