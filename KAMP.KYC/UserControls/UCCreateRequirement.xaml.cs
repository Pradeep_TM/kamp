﻿using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AutoMapper;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCCreateRequirement.xaml
    /// </summary>
    using Core.FrameworkComponents;
    using KAMP.KYC.BLL;
    using KAMP.KYC.Views;
    public partial class UCCreateRequirement : UserControl, IPageControl
    {
        public UCCreateRequirement()
        {
            InitializeComponent();
        }

        public dynamic InitializeViewModel()
        {
            var pageVM = new CreateReqPageVM() { OnRedirectToReqMgmt = RedirectToReqMgmt };

            if (AppVariables.RequirementToEdit.HasValue)
            {
                var req = new KYCBLL().GetRequirements(AppVariables.RequirementToEdit.Value);
                pageVM.ReqVM = new RequirementVM();

                if (req.Category.IsNotNull() && req.Category.Classification.IsNotNull())
                {
                    var classification = pageVM.ReqClassifications.FirstOrDefault(x => x.Id == req.Category.Classification.Id);
                    var category = pageVM.ReqCategoriesByClsfcn.FirstOrDefault(x => x.Id == req.Category.Id);
                    pageVM.ReqClassificationVm = classification;
                    pageVM.ReqVM.Category = category;
                }

                pageVM.ReqVM.RequirementType = pageVM.RequirementTypes.FirstOrDefault(x => x.Code == (int)req.ReqType);
                pageVM.ReqVM.RequirementText = req.RequirementText;
                pageVM.ReqVM.RequirementDetails = req.RequirementDetails;
                pageVM.ReqVM.ReqType = req.ReqType;
                pageVM.ReqVM.Id = req.Id;

                //AttachEventHandler
                switch (pageVM.ReqVM.ReqType)
                {
                    case RequirementType.SingleSelectOption:
                        SingleSelectAttachHandler(pageVM.ReqVM.RequirementDetails);
                        break;

                    case RequirementType.MultiSelectOption:
                        MultiSelectAttachHandler(pageVM.ReqVM.RequirementDetails);
                        break;

                    case RequirementType.Grid:
                        GridAttachHandler(pageVM.ReqVM.RequirementDetails);
                        break;
                }               

                AppVariables.RequirementToEdit = null;
            }
            return pageVM;
        }

        private void RedirectToReqMgmt()
        {
            var entryWindow = Window.GetWindow(this) as EntryWindow;
            entryWindow.LoadPageControl(KYCPageControls.RequirementManagement);
        }

        private static void GridAttachHandler(IReqDetails reqDetails)
        {
            var grdDet = reqDetails as GridReqDetailsVM;
            if (grdDet.Columns.IsCollectionValid())
            {
                grdDet.Columns.Each(x =>
                {
                    x.OnDelete += grdDet.DeleteColumns;

                    if (x.ColumnType == RequirementType.SingleSelectOption)
                    {
                        SingleSelectAttachHandler(x.ColumnContent);
                    }
                    else if (x.ColumnType == RequirementType.MultiSelectOption)
                    {
                        MultiSelectAttachHandler(x.ColumnContent);
                    }
                });
            }
        }

        public string PageName
        {
            get { return KYCPageControls.CreateRequirement; }
        }

        private static void MultiSelectAttachHandler(IReqDetails reqDetails)
        {
            var multiSelectDet = reqDetails as MultiSelectReqDetailsVM;
            if (multiSelectDet.Options.IsCollectionValid())
            {
                multiSelectDet.Options.Each(x => x.OnDelete += multiSelectDet.DeleteOptions);
            }
        }

        private static void SingleSelectAttachHandler(IReqDetails reqDetails)
        {
            var singleSelectDet = reqDetails as SingleSelectReqDetailsVM;
            if (singleSelectDet.Options.IsCollectionValid())
            {
                singleSelectDet.Options.Each(x => x.OnDelete += singleSelectDet.DeleteOptions);
            }
        }
    }
}
