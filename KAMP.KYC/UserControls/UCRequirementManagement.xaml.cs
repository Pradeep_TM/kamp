﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    using Core.FrameworkComponents;
    using KAMP.KYC.BLL;
    using KAMP.KYC.ViewModels;
    using KAMP.KYC.Views;
    /// <summary>
    /// Interaction logic for UCRequirementManagement.xaml
    /// </summary>
    public partial class UCRequirementManagement : UserControl, IPageControl
    {
        private ReqMgmtVM _viewModel;
        public UCRequirementManagement()
        {
            InitializeComponent();
        }

        public dynamic InitializeViewModel()
        {
            if (_viewModel.IsNull())
                _viewModel = new ReqMgmtVM() { LoadCreateReq = LoadCreateReq };
            var bl = new KYCBLL();
            var reqClassification = bl.GetAllClassificationWithRequirement();
            if (reqClassification.IsCollectionValid())
                _viewModel.ReqClassification = new System.Collections.ObjectModel.ObservableCollection<ReqClassificationVM>(reqClassification);
            return _viewModel;
        }

        public string PageName
        {
            get { return "Requirement Management"; }
        }

        public void LoadCreateReq()
        {
            var window = Window.GetWindow(this);
            var entryWin = window as EntryWindow;
            entryWin.LoadPageControl(KYCPageControls.CreateRequirement);
        }

        private void CreateRequirement(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this) as EntryWindow).LoadPageControl(KYCPageControls.CreateRequirement);
        }
    }
}
