﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    using KAMP.KYC.AppCode;
    using ViewModels;

    /// <summary>
    /// Interaction logic for UCGridResponse.xaml
    /// </summary>
    public partial class UCGridResponse : UserControl
    {
        public UCGridResponse()
        {
            InitializeComponent();
        }

        internal void BindGridDetials(RequirementVM selectedReqVM)
        {
            var grdReqDetails = selectedReqVM.RequirementDetails as GridReqDetailsVM;
                        
            var deleteCellBinding = new Binding();
            grdGridResponse.Columns.Add(new CustomBoundColumn() 
            {
                Binding = deleteCellBinding,
                Header = "Delete",
                TemplateName = "deleteRowTemplate"
            });

            foreach (var column in grdReqDetails.Columns)
            {
                switch (column.ColumnType)
                {
                    case RequirementType.FreeText:
                        var cellBinding = new Binding(string.Format("ResponseGridCells[{0}]", grdReqDetails.Columns.IndexOf(column)));
                        
                        grdGridResponse.Columns.Add(new CustomBoundColumn()
                        {
                            Binding = cellBinding,
                            Header = column.ColumnText,
                            TemplateName = "textColumnTemplate",
                            EditingTemplateName = "textColumnEditTemplate"
                        });
                        break;
                    case RequirementType.SingleSelectOption:
                        var index = grdReqDetails.Columns.IndexOf(column);
                        //var responseGridCells = selectedReqVM.ResponseVM.ResponseGridRows[index];
                        var gridCellBinding = new Binding(string.Format("ResponseGridCells[{0}]", grdReqDetails.Columns.IndexOf(column)));

                        grdGridResponse.Columns.Add(new CustomBoundColumn()
                        {
                            Binding = gridCellBinding,
                            Header = column.ColumnText,
                            TemplateName = "singleSelectDisplayTemplate",
                            EditingTemplateName = "singleSelectComboBoxTemplate"
                        });                        
                        break;

                    case RequirementType.MultiSelectOption:
                        //Bind Template
                        var itemsBinding = new Binding(string.Format("ResponseGridCells[{0}]", grdReqDetails.Columns.IndexOf(column)));
                        grdGridResponse.Columns.Add(
                            new CustomBoundColumn()
                                {
                                    Header = column.ColumnText,
                                    Binding = itemsBinding,
                                    TemplateName = "multiSelectDisplayTemplate",
                                    EditingTemplateName = "multiSelectComboBoxTemplate"
                                });
                        break;

                    default:
                        //grdGridResponse.Columns.Add(new DataGridTextColumn() { Binding = binding, Header = column.ColumnText });
                        break;
                }
            }
        }

        private void DataGridCell_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell != null && !cell.IsEditing && !cell.IsReadOnly)
            {
                if (!cell.IsFocused)
                {
                    cell.Focus();
                }
                DataGrid dataGrid = FindVisualParent<DataGrid>(cell);
                if (dataGrid != null)
                {
                    if (dataGrid.SelectionUnit != DataGridSelectionUnit.FullRow)
                    {
                        if (!cell.IsSelected)
                            cell.IsSelected = true;
                    }
                    else
                    {
                        DataGridRow row = FindVisualParent<DataGridRow>(cell);
                        if (row != null && !row.IsSelected)
                        {
                            row.IsSelected = true;
                        }
                    }
                }
            }
        }

        static T FindVisualParent<T>(UIElement element) where T : UIElement
        {
            UIElement parent = element;
            while (parent != null)
            {
                T correctlyTyped = parent as T;
                if (correctlyTyped != null)
                {
                    return correctlyTyped;
                }

                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            return null;
        } 
    }
}
