﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    using KAMP.KYC.AppCode;
    using ViewModels;

    /// <summary>
    /// Interaction logic for UCGridResponsePreview.xaml
    /// </summary>
    public partial class UCGridResponsePreview : UserControl
    {
        public UCGridResponsePreview()
        {
            InitializeComponent();
        }

        internal void BindGridDetials(RequirementVM selectedReqVM)
        {
            var grdReqDetails = selectedReqVM.RequirementDetails as GridReqDetailsVM;
            grdGridResponse.Columns.Clear();

            foreach (var column in grdReqDetails.Columns)
            {
                switch (column.ColumnType)
                {
                    case RequirementType.FreeText:
                        var cellBinding = new Binding(string.Format("ResponseGridCells[{0}]", grdReqDetails.Columns.IndexOf(column)));

                        grdGridResponse.Columns.Add(new CustomBoundColumn()
                        {
                            Binding = cellBinding,
                            Header = column.ColumnText,
                            TemplateName = "textColumnTemplate",                            
                        });
                        break;
                    case RequirementType.SingleSelectOption:
                        var index = grdReqDetails.Columns.IndexOf(column);
                        //var responseGridCells = selectedReqVM.ResponseVM.ResponseGridRows[index];
                        var gridCellBinding = new Binding(string.Format("ResponseGridCells[{0}]", grdReqDetails.Columns.IndexOf(column)));

                        grdGridResponse.Columns.Add(new CustomBoundColumn()
                        {
                            Binding = gridCellBinding,
                            Header = column.ColumnText,
                            TemplateName = "singleSelectDisplayTemplate",                            
                        });
                        break;

                    case RequirementType.MultiSelectOption:
                        //Bind Template
                        var itemsBinding = new Binding(string.Format("ResponseGridCells[{0}]", grdReqDetails.Columns.IndexOf(column)));
                        grdGridResponse.Columns.Add(
                            new CustomBoundColumn()
                            {
                                Header = column.ColumnText,
                                Binding = itemsBinding,
                                TemplateName = "multiSelectDisplayTemplate"                                
                            });
                        break;

                    default:
                        //grdGridResponse.Columns.Add(new DataGridTextColumn() { Binding = binding, Header = column.ColumnText });
                        break;
                }
            }
        }
    }
}
