﻿using KAMP.Core.Repository.KYC;
using KAMP.KYC.BLL;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCHistory.xaml
    /// </summary>
    public partial class UCCaseHistory : UserControl
    {
        public Grid caseHistoryGrid { get; set; }
        private CaseHistoryVM _viewModel { get; set; }

        public UCCaseHistory()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(OnLoaded);
        }

        private void OnLoaded(object sender,RoutedEventArgs e)
        {
            string userName = null;

            _viewModel = DataContext as CaseHistoryVM;
            if (_viewModel != null && !string.IsNullOrEmpty(_viewModel.SelectedUser))
            {
                if (_viewModel.SelectedUser.Equals(KYCConstants.DefaultDDLValue))
                    userName = null;
                else
                    userName = _viewModel.SelectedUser;
                var bLL = new KYCBLL();
                _viewModel.CaseHistoryList = bLL.GetCaseHistory(_viewModel.Entities, _viewModel.CfId, userName);
            }
        }

        private void ViewDetailsClicked(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = DgCaseHistory.SelectedItem as HistoryModel;

            if (selectedItemfrmGrid==null) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var uc = new UCHistoryDetails();
            uc.DataContext = selectedItemfrmGrid;
            uc.CloseControl = CloseControl;
            caseHistoryGrid = new Grid();
            caseHistoryGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            caseHistoryGrid.Children.Add(lbl);
            caseHistoryGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container!=null)
            {
                container.Children.Add(caseHistoryGrid);
            }
            
        }

        private void CloseControl()
        {
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container !=null)
            {
                container.Children.Remove(caseHistoryGrid);
            }
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void UserSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string userName=null;
            var ddl = sender as ComboBox;
            if (ddl.SelectedIndex <=0) return;
            _viewModel = DataContext as CaseHistoryVM;
            var bLL = new KYCBLL();
            if(_viewModel.SelectedUser.Equals(KYCConstants.DefaultDDLValue))
              userName = null;
            else
               userName=_viewModel.SelectedUser;
            _viewModel.CaseHistoryList = bLL.GetCaseHistory(_viewModel.Entities, _viewModel.CfId, userName);
        }
         
    }
    }

