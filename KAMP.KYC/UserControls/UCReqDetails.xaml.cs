﻿using KAMP.KYC.BLL;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    using System.ComponentModel;
    /// <summary>
    /// Interaction logic for UCReqDetails.xaml
    /// </summary>
    using ViewModels;
    using KAMP.Core.FrameworkComponents;
    public partial class UCReqDetails : UserControl, INotifyPropertyChanged
    {
        private RequirementVM _viewNodel;
        public UCReqDetails(long reqId, Action close)
        {
            InitializeComponent();
            _viewNodel = new KYCBLL().GetRequirements(reqId);
            _viewNodel.OnReqDetailsResponseClose += close;
            DataContext = _viewNodel;
            RaisePropertyChanged();
        }

        private void RaisePropertyChanged()
        {
            PropertyChanged.Raise(this, (x) => x.ReqType);
            PropertyChanged.Raise(this, (x) => x.IsOptions);
            PropertyChanged.Raise(this, (x) => x.IsGridType);
        }

        public string ReqType
        {
            get
            {
                if (_viewNodel == null)
                    return null;
                var requirementType = (RequirementType)_viewNodel.ReqType;
                switch (requirementType)
                {
                    case RequirementType.SingleSelectOption:
                        return RequirementType.SingleSelectOption.ToString();
                    case RequirementType.MultiSelectOption:
                        return RequirementType.MultiSelectOption.ToString();
                    case RequirementType.Grid:
                        return RequirementType.Grid.ToString();
                    default:
                        return RequirementType.FreeText.ToString();
                }
            }
        }

        public bool IsOptions
        {
            get
            {
                if (_viewNodel == null)
                    return false;

                var requirementType = (RequirementType)_viewNodel.ReqType;
                switch (requirementType)
                {
                    case RequirementType.SingleSelectOption:
                        return (_viewNodel.RequirementDetails as SingleSelectReqDetailsVM).Options.IsCollectionValid();
                    case RequirementType.MultiSelectOption:
                        return (_viewNodel.RequirementDetails as MultiSelectReqDetailsVM).Options.IsCollectionValid();
                    default:
                        return false;
                }
            }
        }

        public bool IsGridType
        {
            get
            {
                if (_viewNodel == null)
                    return false;

                var requirementType = (RequirementType)_viewNodel.ReqType;

                if (RequirementType.Grid == requirementType)
                    return true;

                return false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }


}
