﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    using KAMP.Core.Common.UserControls;
    using KAMP.Core.Common.ViewModels;
    using KAMP.Core.FrameworkComponents;
    using ViewModels;

    /// <summary>
    /// Interaction logic for UCCaseFileCard.xaml
    /// </summary>
    public partial class UCCaseFileCard : UserControl
    {
        CaseFileCardPageVM caseDetails { get { return this.DataContext as CaseFileCardPageVM; } }

        public UCCaseFileCard()
        {
            InitializeComponent();
        }

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    //Load tab data on demand
        //    if (e.Source is TabItem && this.IsLoaded)
        //    {
        //        var tabName = (e.Source as TabItem);

        //    }
        //}
    }
}
