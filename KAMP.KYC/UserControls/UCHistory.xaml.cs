﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.KYC;
using KAMP.KYC.BLL;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCHistory.xaml
    /// </summary>
    public partial class UCHistory : UserControl, IPageControl
    {
        public Grid tLBHistoryGrid { get; set; }

        public CaseFileCardPageVM _viewModel { get; set; }

        bool _historySelectionChanged;

        public string PageName
        {
            get { return KYCPageControls.History; }
        }
      
        public UCHistory()
        {
            InitializeComponent();
        }

        public dynamic InitializeViewModel()
        {
            _viewModel = new CaseFileCardPageVM();
            _viewModel.HistoryViewModel = new HistoryVM();
            _viewModel.HistoryViewModel.HistoryCategoryList = new ObservableCollection<string> { KYCConstants.DefaultDDLValue, "Application", "Case", "Login" };
            _viewModel.HistoryViewModel.SelectedCategory = _viewModel.HistoryViewModel.HistoryCategoryList.FirstOrDefault();
           // this.DataContext = _viewModel;
            return _viewModel;
        }

        private void HistorySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //TODO Get Category List fron some other place
            HideControls();

            var ddl = sender as ComboBox;
            var bll = new KYCBLL();
            _historySelectionChanged = true;
            if (ddl.SelectedIndex == -1 || ddl.SelectedIndex == 0)
            {
                ResetViewModel();
            }
            else if (ddl.SelectedItem.ToString().Equals("Application"))
            {
                _viewModel.HistoryViewModel.HistoryList = new ObservableCollection<HistoryModel>();
                var applEntities = bll.GetApplicationHistory().Select(x => x.Entity).Distinct().ToList();
                if (_viewModel.HistoryViewModel.EntityList.IsNull())
                    _viewModel.HistoryViewModel.EntityList = new List<string>();
                var availableEntities = bll.ResolveEntities(applEntities, null);
                if (availableEntities != null)
                    _viewModel.HistoryViewModel.EntityList = availableEntities;
                _viewModel.HistoryViewModel.EntityList.Insert(0, KYCConstants.DefaultDDLValue);
                
                if (_viewModel.HistoryViewModel.EntityList.IsCollectionValid())
                _viewModel.HistoryViewModel.SelectedEntity = _viewModel.HistoryViewModel.EntityList.FirstOrDefault();

                StkSubCategory.Visibility = Visibility.Visible;
                DgKYCHistory.Visibility = Visibility.Visible;
            }

            else if (ddl.SelectedItem.ToString().Equals("Case"))
            {   var entities = bll.GetEntities();
                var historyList = bll.GetCaseHistory(entities, 0);
                _viewModel.HistoryViewModel.HistoryList = historyList;
                DgKYCHistory.Visibility = Visibility.Visible;
            }

            else if (ddl.SelectedItem.ToString().Equals("Login"))
            {
                _viewModel.HistoryViewModel.LoginHistoryList = new ObservableCollection<HistoryModel>();
                _viewModel.HistoryViewModel.UserList = bll.GetLoginHistory().Select(x => x.User).Distinct().OrderBy(x => x).ToList();
                _viewModel.HistoryViewModel.UserList.Insert(0, KYCConstants.DefaultDDLValue);
                _viewModel.HistoryViewModel.SelectedUserName = _viewModel.HistoryViewModel.UserList.FirstOrDefault();     
                DgKYCLoginHistory.Visibility = Visibility.Visible;
                StkUser.Visibility = Visibility.Visible;
              
            }
        
        }

        private void HideControls()
        {
            DgKYCHistory.Visibility = Visibility.Hidden;
            DgKYCLoginHistory.Visibility = Visibility.Hidden;
            StkUser.Visibility = Visibility.Hidden;
            StkSubCategory.Visibility = Visibility.Hidden;
        }

        private void ResetViewModel()
        {
            _viewModel.HistoryViewModel.HistoryList = null;
            _viewModel.HistoryViewModel.EntityList = null;
            _viewModel.HistoryViewModel.LoginHistoryList = null;
            _viewModel.HistoryViewModel.UserList = null;
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void ViewDetailsClicked(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = DgKYCHistory.SelectedItem as HistoryModel;

            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var uc = new UCHistoryDetails();
            uc.DataContext = selectedItemfrmGrid;
            uc.CloseControl = CloseControl;
            tLBHistoryGrid = new Grid();
            tLBHistoryGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            tLBHistoryGrid.Children.Add(lbl);
            tLBHistoryGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(tLBHistoryGrid);
            }
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(tLBHistoryGrid);
            }
        }

        private void ddlHistorySubCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(_viewModel.HistoryViewModel.SelectedEntity))
            {
                //StkSubCategory.Visibility = Visibility.Collapsed; 
                return;
            }
            if (_viewModel.HistoryViewModel.SelectedEntity != null && _viewModel.HistoryViewModel.SelectedEntity.Equals(KYCConstants.DefaultDDLValue))
                return;
            var bll = new KYCBLL();
            _viewModel.HistoryViewModel.SelectedEntity = bll.ResolveEntities(null, _viewModel.HistoryViewModel.SelectedEntity);
            var list = bll.GetApplicationHistory().Where(x => x.Entity.Equals(_viewModel.HistoryViewModel.SelectedEntity)).OrderBy(x => x.Entity).ToList();
            var collection = new ObservableCollection<HistoryModel>();
            foreach (var item in list)
            {
                collection.Add(item);
            }
            _viewModel.HistoryViewModel.HistoryList = collection;
            _viewModel.HistoryViewModel.IsLoginGridVisible = false;
        }

        private void ddlUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(_viewModel.HistoryViewModel.SelectedUserName))
            {
                StkUser.Visibility = Visibility.Collapsed;
                return;
            }
               
            if (_viewModel.HistoryViewModel.SelectedUserName.Equals(KYCConstants.DefaultDDLValue)) return;
            var bll = new KYCBLL();
            var list = bll.GetLoginHistory().Where(x => x.User.Equals(_viewModel.HistoryViewModel.SelectedUserName)).ToList();
            var collection = new ObservableCollection<HistoryModel>();
            foreach (var item in list)
            {
                collection.Add(item);
            }
            _viewModel.HistoryViewModel.LoginHistoryList = collection;
        }

    }
}
