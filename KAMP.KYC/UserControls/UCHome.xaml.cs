﻿using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCHome.xaml
    /// </summary>
    using Core.FrameworkComponents;
    public partial class UCHome : UserControl, IPageControl
    {
        public UCHome()
        {
            InitializeComponent();
        }

        public dynamic InitializeViewModel()
        {
            return new HomeVM();   
        }

        public string PageName
        {
            get { return KYCPageControls.Home; }
        }
    }
}
