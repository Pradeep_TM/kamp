﻿using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCAssignCases.xaml
    /// </summary>
    using Core.FrameworkComponents;
    using Core.Repository;
    using Core.Repository.KYC;
    using BLL;
    using KAMP.Core.Workflow.Helpers;
    using System.Collections.ObjectModel;
    using KAMP.Core.Repository.WF;
    using KAMP.Core.Repository.UM;
    public partial class UCAssignCases : UserControl, IPageControl
    {
        private double _height;
        private int _displayItemCount = 10;
        private AssignCasesTabVM _viewModel;
        public UCAssignCases()
        {
            InitializeComponent();
        }

        void UCAssignCases_Loaded(object sender, RoutedEventArgs e)
        {
            BorderAssigned_PreviewMouseDown(null, null);
            IsUnassignedTabOpened = true;
        }

        #region Logic for hide unhide tabs
        private void Border_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_height <= 0)
                _height = 420;

            if (UnassignedContainer.Height != 0)
            {
                HideUnassignedTab();
            }

            if (UnassignedContainer.Height == 0)
            {
                ShowUnassignedTab();
            }
        }

        private void ShowUnassignedTab()
        {
            if (IsAssignedTabOpened)
            {
                DoubleAnimation animation1 = new DoubleAnimation();
                animation1.To = 0;
                animation1.From = 0;
                animation1.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation1);
            }
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = _height;
            animation.From = 0;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
            IsUnassignedTabOpened = true;
        }

        private void HideUnassignedTab()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 0;
            animation.From = _height;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
        }

        private void BorderAssigned_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_heightAssigned <= 0)
                _heightAssigned = 470;

            if (AssignedContainer.Height != 0)
            {
                HideAssignedTab();
            }

            if (AssignedContainer.Height == 0)
            {
                ShowAssignedTab();
            }
        }

        private void ShowAssignedTab()
        {
            if (IsUnassignedTabOpened)
            {
                DoubleAnimation animation1 = new DoubleAnimation();
                animation1.To = 0;
                animation1.From = 0;
                animation1.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation1);
            }
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = _heightAssigned;
            animation.From = 0;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
            IsAssignedTabOpened = true;
        }

        private void HideAssignedTab()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 0;
            animation.From = _heightAssigned;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
        }

        private double _heightAssigned;
        private bool IsAssignedTabOpened;
        private bool IsUnassignedTabOpened;
        #endregion

        #region IPageControl Implementation
        public dynamic InitializeViewModel()
        {
            IsAssignedTabOpened = true;
            IsUnassignedTabOpened = false;
            _height = 420;
            ShowUnassignedTab();

            if (_viewModel.IsNull())
                _viewModel = new AssignCasesTabVM();

            if (!_viewModel.CaseList.IsCollectionValid())
                _viewModel.CaseList = new ObservableCollection<CaseDetails>();

            if (_viewModel.FilterSearchUnassigendCases.IsNull())
                _viewModel.FilterSearchUnassigendCases += InitializedUnAssignCasePagination;

            if (_viewModel.FilterSearch.IsNull())
                _viewModel.FilterSearch += InitializedAssignasePagination;

            _viewModel.IsInitialLoad = true;

            InitializedData();
            InitializedPagination();
            return _viewModel;
        }

        private void InitializedPagination()
        {
            InitializedAssignasePagination();
            InitializedUnAssignCasePagination();
        }
        /// <summary>
        /// Initializing the assign grid pagination.
        /// </summary>
        private void InitializedAssignasePagination()
        {
            if (_viewModel.AssignCaseList.IsNull())
                _viewModel.AssignCaseList = new ObservableCollection<CaseDetails>();

            var query = _viewModel.CaseList.Where(x => x.UserId != 0 || x.Status.Equals(Helper.Status.End));

            if (_viewModel.SelectedAssignedCategory.Code > 0)
                query = query.Where(x => x.CategoryName.Equals(_viewModel.SelectedAssignedCategory.Name));

            if (_viewModel.SelectedCaseNo.IsNotNull() && !_viewModel.SelectedCaseNo.Equals(KYCConstants.DefaultDropDownValue))
                query = query.Where(x => x.CaseNumber.Trim().ToLower().Equals(_viewModel.SelectedCaseNo.Trim().ToLower()));

            if (_viewModel.SelectedStatus > 0)
                query = query.Where(x => x.StateId == _viewModel.SelectedStatus);

            if (_viewModel.SelectedStatus == -1)
                query = query.Where(x => x.Statename.Trim().ToLower().Equals(Helper.Status.Assigned.Trim().ToLower()));

            if (_viewModel.SelectedStatus == -2)
                query = query.Where(x => x.Statename.Trim().ToLower().Equals(Helper.Status.End.Trim().ToLower()));

            if (_viewModel.SelectedUser > 0)
                query = query.Where(x => x.UserId == _viewModel.SelectedUser);

            var AssignCases = query.ToList();

            var totalcount = AssignCases.Count;

            AssignCases = AssignCases.Take(_displayItemCount).ToList();

            _viewModel.AssignCaseList.Clear();

            AssignCases.ForEach(_viewModel.AssignCaseList.Add);

            _viewModel.AssignPaginationVM = new PaginationVM
            {
                TotalItem = totalcount,
                StartIndex = AssignCases.IsCollectionValid() ? 1 : 0,
                CurrentIndex = AssignCases.IsCollectionValid() ? 1 : 0,
                DisplayitemCount = _displayItemCount,
                PageClick = AssignPageClick,
            };

        }

        /// <summary>
        /// Initializing the unassign grid pagination.
        /// </summary>
        private void InitializedUnAssignCasePagination()
        {
            if (_viewModel.UnAssignCaseList.IsNull())
                _viewModel.UnAssignCaseList = new ObservableCollection<CaseDetails>();

            var query = _viewModel.CaseList.Where(x => x.UserId == 0 && x.Status.Equals(Helper.Status.UnAssigned));

            if (_viewModel.SelectedUnassignedCategory.Code > 0)
                query = query.Where(x => x.CategoryName.Equals(_viewModel.SelectedUnassignedCategory.Name));

            var unAssignCases = query.ToList();

            var totalcount = unAssignCases.Count;

            unAssignCases = unAssignCases.Take(_displayItemCount).ToList();

            _viewModel.UnAssignCaseList.Clear();

            unAssignCases.ForEach(_viewModel.UnAssignCaseList.Add);

            _viewModel.UNAssignPaginationVM = new PaginationVM
            {
                TotalItem = totalcount,
                StartIndex = unAssignCases.IsCollectionValid() ? 1 : 0,
                CurrentIndex = unAssignCases.IsCollectionValid() ? 1 : 0,
                DisplayitemCount = _displayItemCount,
                PageClick = UNAssignPageClick,
            };


        }

        /// <summary>
        /// Un Assign Pagination next page or previous page or Last Page or First Page click.
        /// </summary>
        private void UNAssignPageClick()
        {
            int skipItem = _viewModel.UNAssignPaginationVM.StartIndex - 1;
            var query = _viewModel.CaseList.Where(x => x.UserId == 0 && x.Status.Equals(Helper.Status.UnAssigned)).
                Skip(skipItem).Take(_viewModel.UNAssignPaginationVM.DisplayitemCount);

            if (_viewModel.SelectedUnassignedCategory.Code > 0)
                query = query.Where(x => x.CategoryId == _viewModel.SelectedUnassignedCategory.Code);

            if (_viewModel.UnAssignCaseList.IsCollectionValid())
                _viewModel.UnAssignCaseList.Clear();

            var cases = query.ToList();

            cases.ForEach(_viewModel.UnAssignCaseList.Add);

        }

        /// <summary>
        ///  Un Assign Pagination next page or previous page or Last Page or First Page Click.
        /// </summary>
        private void AssignPageClick()
        {
            int skipItem = _viewModel.AssignPaginationVM.StartIndex - 1;
            var query = _viewModel.CaseList.Where(x => x.UserId != 0 || x.Status.Equals(Helper.Status.End)).
                Skip(skipItem).Take(_viewModel.AssignPaginationVM.DisplayitemCount);

            if (_viewModel.SelectedAssignedCategory.Code > 0)
                query = query.Where(x => x.CategoryId == _viewModel.SelectedAssignedCategory.Code);

            if (_viewModel.SelectedCaseNo.IsNotNull() && !_viewModel.SelectedCaseNo.Equals(KYCConstants.DefaultDropDownValue))
                query = query.Where(x => x.CaseNumber.Trim().ToLower().Equals(_viewModel.SelectedCaseNo.Trim().ToLower()));

            if (_viewModel.SelectedStatus > 0)
                query = query.Where(x => x.StateId == _viewModel.SelectedStatus);

            if (_viewModel.SelectedStatus == -1)
                query = query.Where(x => x.Statename.Trim().ToLower().Equals(Helper.Status.Assigned.Trim().ToLower()));

            if (_viewModel.SelectedStatus == -2)
                query = query.Where(x => x.Statename.Trim().ToLower().Equals(Helper.Status.End.Trim().ToLower()));

            if (_viewModel.SelectedUser > 0)
                query = query.Where(x => x.UserId == _viewModel.SelectedUser);

            if (_viewModel.AssignCaseList.IsCollectionValid())
                _viewModel.AssignCaseList.Clear();

            var cases = query.ToList();

            cases.ForEach(_viewModel.AssignCaseList.Add);

        }
        private void InitializedData()
        {
            var bl = new KYCBLL();
            var cases = bl.GetAllCases();
            _viewModel.CaseList.Clear();
            _viewModel.CaseList.AddRange(cases);
        
            if (!_viewModel.CaseCategoryList.IsCollectionValid())
            {
                 //var caseCategories = bl.GetCaseCategory(KYCConstants.CaseCategory);
                _viewModel.CaseCategoryList = bl.GetCategoryFilters(KYCConstants.CategoryFilter);
                _viewModel.CaseCategoryList.Insert(0, new MasterEntity { Code = 0, Name = KYCConstants.DefaultDropDownValue });
            }

            if (!_viewModel.UserList.IsCollectionValid())
            {
                _viewModel.UserList = bl.GetUsers();
                _viewModel.UserList.Insert(0, new User { UserId = 0, UserName = KYCConstants.DefaultDropDownValue });
            }

            if (!_viewModel.StateList.IsCollectionValid())
            {
                _viewModel.StateList = bl.GetStates();
                _viewModel.StateList.Add(new State { Id = -1, StateName = Helper.Status.Assigned });
                _viewModel.StateList.Add(new State { Id = -2, StateName = Helper.Status.End });
                _viewModel.StateList = _viewModel.StateList.OrderBy(x => x.StateName).ToList();
                _viewModel.StateList.Insert(0, new State { Id = 0, StateName = KYCConstants.DefaultDropDownValue });
            }

            if (!_viewModel.Cases.IsCollectionValid())
            {
                _viewModel.Cases = _viewModel.CaseList.Where(x => x.UserId != 0 || x.Status.Equals(Helper.Status.End)).Select(x=>x.CaseNumber).ToList();
                _viewModel.Cases.Insert(0, KYCConstants.DefaultDropDownValue);
            }

            _viewModel.SelectedAssignedCategory = _viewModel.CaseCategoryList.FirstOrDefault();
            _viewModel.SelectedUnassignedCategory = _viewModel.CaseCategoryList.FirstOrDefault();
            _viewModel.SelectedCaseNo = _viewModel.Cases.FirstOrDefault();
            _viewModel.SelectedStatus = 0;
            _viewModel.SelectedUser = 0;
            _viewModel.IsInitialLoad = false;
        }


        public string PageName
        {
            get { return KYCPageControls.AssignCases; }
        }
        #endregion
    }
}
