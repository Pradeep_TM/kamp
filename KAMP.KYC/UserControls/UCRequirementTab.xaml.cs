﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;
using System.Windows.Controls.Primitives;
using KAMP.KYC.ViewModels;
using System.Threading;
using System.Windows.Threading;


namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCRequirementTab.xaml
    /// </summary>
    public partial class UCRequirementTab : UserControl
    {
        public UCRequirementTab()
        {
            InitializeComponent();
            wpfDataGrid.Loaded += wpfDataGrid_Loaded;
        }

        void wpfDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            //wpfDataGrid.Columns.AsParallel().ForAll(column =>
            //{
            //    column.MinWidth = column.ActualWidth;
            //    column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            //});
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            var respVMBtn = (sender as ToggleButton);
            var reqVm = (respVMBtn.Tag as RequirementVM);
            var respVM = reqVm.ResponseVM;
            respVM.IsResponsePreviewDetailsVisible = respVMBtn.IsChecked;

            if (reqVm.ReqType == RequirementType.Grid)
            {
                var dataGridCell = FindVisualParent<DataGridCell>(e.OriginalSource as ToggleButton);                
                var ucPopup = GetVisualChild<Popup>(dataGridCell);
                var ucPreview = ucPopup.FindName("ucGridRespPreview") as UCGridResponsePreview;
                ucPreview.BindGridDetials(reqVm); 
            }            

            Thread threadToClosePopup = new Thread(new ThreadStart((Action)delegate()
            {
                Thread.Sleep(5000);
                Dispatcher.Invoke(
                    () =>
                    {
                        respVM.IsResponsePreviewDetailsVisible = false;
                        respVMBtn.IsChecked = false;
                    },
                  DispatcherPriority.Normal
                  );
            }));

            threadToClosePopup.Start();
        }

        public static T FindVisualParent<T>(UIElement element) where T : UIElement
        {
            UIElement parent = element;
            while (parent != null)
            {
                T correctlyTyped = parent as T;
                if (correctlyTyped != null)
                {
                    return correctlyTyped;
                }

                parent = VisualTreeHelper.GetParent(parent) as UIElement;

            }
            return null;
        }

        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }


    }
}
