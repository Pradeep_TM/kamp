﻿using KAMP.Core.Common.ViewModels;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.AuditLog;
using KAMP.KYC.BLL;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCAuditConfiguration.xaml
    /// </summary>
    public partial class UCAuditConfiguration : UserControl, IPageControl
    {
        public List<Type> TypeList;
        public List<ConfigurationTable> PreviousConfigurtion;
        public string PageName
        {
            get { return KYCPageControls.AuditConfiguration; }
        }

        public dynamic InitializeViewModel()
        {
            return null;
        }

        public UCAuditConfiguration()
        {
            InitializeComponent();
            CreateTreeView();
        }

        private void CreateTreeView()
        {
            var NodesCollection = new ObservableCollection<TreeViewModel>();
            List<Type> modelTypes = GetLoggableEntities();
            var bLL = new KYCBLL();
            PreviousConfigurtion = bLL.GetAuditConfiguration();

            foreach (var type in modelTypes)
            {
                TreeViewModel model = new TreeViewModel();
                TypeList = new List<Type>();
                if (!CheckForModelInConfiguration(ref model, type))
                    model = GetPropertiesName(type);
                NodesCollection.Add(model);
            }
            TreeViewEmployee.DataContext = NodesCollection;
        }

        private TreeViewModel GetPropertiesName(Type typeOfObj)
        {
            if (TypeList.Any(x => x.Name == typeOfObj.Name)) return null;
            TypeList.Add(typeOfObj);
            TreeViewModel treeViewModel = new TreeViewModel();
            treeViewModel.Name = typeOfObj.Name;
            var attributes = typeOfObj.CustomAttributes;
            foreach (var attribute in attributes)
            {
                if (attribute.AttributeType.Name.Equals(typeof(LogEntityAttribute).Name))
                {
                    treeViewModel.Description = string.Format("({0})", attribute.ConstructorArguments[1].Value.ToString());
                }
            }

            PropertyInfo[] properties = typeOfObj.GetProperties();
            foreach (var prop in properties)
            {
                bool isNotComplexType = GetPropertyType(prop);
                if (isNotComplexType)
                {
                    if (!CheckForIdColumns(prop.Name))
                    {
                        treeViewModel.SimpleProperty.Add(new TreeNodeValues { IsSelected = false, Name = prop.Name });
                    }
                }
                else
                {//Uncomment to get nested entities
                    //var result = GetPropertiesName(GetPropertyBaseTpe(prop.PropertyType));
                    //if (result != null)
                    //    treeViewModel.ComplexProperty.Add(result);
                }
            }
            return treeViewModel;
        }

        private List<Type> GetLoggableEntities()
        {
            var context = Application.Current.Properties["Context"] as Context;
            if (context.IsNull()) return new List<Type>();
            List<Type> Types = new List<Type>();
            var classes = Assembly.GetAssembly(typeof(ConfigurationTable)).ExportedTypes;
            foreach (var type in classes)
            {
                var attributes = type.CustomAttributes;

                foreach (var attribute in attributes)
                {
                    if (attribute.AttributeType.Name.Equals(typeof(LogEntityAttribute).Name))
                    {
                        if (attribute.ConstructorArguments.FirstOrDefault().Value.ToString().ToLower().Equals(context.Module.Name.ToLower()))
                        {
                            // string className = type.Name;
                            Types.Add(type);
                        }
                    }
                }
            }
            return Types;
        }

        private bool CheckForModelInConfiguration(ref TreeViewModel model, Type typeofModel)
        {
          
            var comboboxItem = (ComboBoxItem)DdlAction.SelectedItem;
            if (PreviousConfigurtion == null || PreviousConfigurtion.Count == 0) return false;
            var configuration = PreviousConfigurtion.Where(x => x.EntityName == typeofModel.Name).FirstOrDefault();
            if (configuration == null) return false;
            var modelConfiguration = configuration.EntityModelXmls.Where(x => x.Action == comboboxItem.Content.ToString()).FirstOrDefault();
            if (modelConfiguration == null) return false;
            model = (TreeViewModel)CreateObject(modelConfiguration.EntityXml, new TreeViewModel());
            return true;
        }

        public static object CreateObject(string XMLString, object YourClassObject)
        {
            XmlSerializer oXmlSerializer = new XmlSerializer(YourClassObject.GetType());
            YourClassObject = oXmlSerializer.Deserialize(new StringReader(XMLString));
            return YourClassObject;
        }

        private bool CheckForIdColumns(string p)
        {
            var name = p.Substring(p.Length - 2).ToLower();
            if (name.ToLower().Equals("id")) return true;
            return false;
        }

        private Type GetPropertyBaseTpe(Type type)
        {
            while (type.GenericTypeArguments.Length > 0)
            {
                type = type.GenericTypeArguments[0];
            }
            return type;
        }

        private static bool GetPropertyType(PropertyInfo prop)
        {
            string type = GetLastgenericTypeArgument(prop);
            if (type.ToLower().Equals("system")) return true;
            return false;
        }

        private static string GetLastgenericTypeArgument(PropertyInfo prop)
        {
            if (prop.PropertyType.GenericTypeArguments.Length == 0) return prop.PropertyType.Namespace;
            Type type = prop.PropertyType.GenericTypeArguments[0];
            while (type.GenericTypeArguments.Length > 0)
            {
                type = type.GenericTypeArguments[0];
            }
            return type.Namespace;
        }

        private void SaveConfiguration(object sender, RoutedEventArgs e)
        {
            var collection = TreeViewEmployee.DataContext;
            ObservableCollection<TreeViewModel> dataCollection = (ObservableCollection<TreeViewModel>)collection;

            foreach (var entity in dataCollection)
            {
                SaveConfigForEntity(entity);
            }
            MessageBoxControl.Show("Configuration Stored Successfully", MessageBoxButton.OK, MessageType.Information);
        }

        private void SaveConfigForEntity(TreeViewModel dataCollection)
        {
            var bLL = new KYCBLL();
            var xmltoStore = CreateXML(dataCollection);
            string xml = GetXml(dataCollection);
            if (string.IsNullOrEmpty(xml)) return;
            string innerEntityXml = AddAttributesToXml(xml).InnerXml;

            var entityName = dataCollection.Name;
            if (PreviousConfigurtion == null || PreviousConfigurtion.Count == 0)
            {
                PreviousConfigurtion = bLL.GetAuditConfiguration();
            }
            var prevConfiguration = PreviousConfigurtion.Where(x => x.EntityName == entityName).FirstOrDefault();
            StringBuilder builder = new StringBuilder();
            if (prevConfiguration == null || PreviousConfigurtion.Count == 0)
            {
                //Add Configuration
                builder.AppendLine("<" + "Entities" + ">");
                builder.AppendLine(innerEntityXml);
                builder.AppendLine("</" + "Entities" + ">");
                var appcontext = Application.Current.Properties["Context"] as Context;
                ConfigurationTable config = new ConfigurationTable { EntityName = entityName, ConfigurationXml = builder.ToString(), ModuleId = appcontext.Module.Id };
                config.EntityModelXmls.Add(new EntityModelXml { Action = DdlAction.Text, EntityXml = xmltoStore });
                bLL.AddConfiguration(config);
            }
            else
            {
                //Update Configuration
                XmlDocument xDoc = new XmlDocument();
                xDoc.InnerXml = prevConfiguration.ConfigurationXml;
                var childNodes = xDoc.DocumentElement.GetElementsByTagName(entityName);
                for (int i = 0; i < childNodes.Count; i++)
                {
                    var action = childNodes[i].Attributes.GetNamedItem("Action").Value;
                    if (action.Equals(DdlAction.Text))
                    {
                        xDoc.DocumentElement.RemoveChild(childNodes[i]);
                    }
                }
                xDoc.DocumentElement.InnerXml = xDoc.DocumentElement.InnerXml + innerEntityXml;
                var appcontext = Application.Current.Properties["Context"] as Context;
                ConfigurationTable config = new ConfigurationTable { ModuleId = appcontext.Module.Id, EntityModelXmls = prevConfiguration.EntityModelXmls, Id = prevConfiguration.Id, EntityName = entityName, ConfigurationXml = xDoc.InnerXml };
                GetEntityXmlAcctoAction(ref config, xmltoStore);
                bLL.UpdateConfiguration(config);
                //DataBaseContext.Entry(prevConfiguration).CurrentValues.SetValues(config);
            }
        }

        private void GetEntityXmlAcctoAction(ref ConfigurationTable config, string xmltoStore)
        {
            int id = 0;
            var prevEntry = config.EntityModelXmls.Where(x => x.Action == DdlAction.Text).FirstOrDefault();
            if (prevEntry != null)
            {
                //Prev Model Exists change XML
                prevEntry.EntityXml = xmltoStore;
            }
            else
            {
                var entry = config.EntityModelXmls.FirstOrDefault();

                if (entry != null)
                {
                    id = entry.ConfigurationId;
                    config.EntityModelXmls.Add(new EntityModelXml { ConfigurationId = id, Action = DdlAction.Text, EntityXml = xmltoStore });
                }
            }
        }

        public static string CreateXML(Object YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }

        private XmlDocument AddAttributesToXml(string xml)
        {
            var context = Application.Current.Properties["Context"] as Context;

            XmlDocument configXml = new XmlDocument();
            configXml.InnerXml = xml;
            configXml.DocumentElement.SetAttribute("DateTime", DateTime.Now.ToString());
            configXml.DocumentElement.SetAttribute("Action", DdlAction.Text);
            configXml.DocumentElement.SetAttribute("Createdby", context.User.UserName);
            configXml.DocumentElement.SetAttribute("Source", "Database");
            configXml.DocumentElement.SetAttribute("Destination", "TableName");
            return configXml;
        }

        private string GetXml(TreeViewModel treeViewModel)
        {
            StringBuilder builder = new StringBuilder();
            //To clear the configuration
            // if (treeViewModel.IsSelected == false) return string.Empty;
            builder.AppendLine("<" + treeViewModel.Name + ">");
            var simpleProps = treeViewModel.SimpleProperty;
            foreach (var prop in simpleProps)
            {
                if (!prop.IsSelected == false)
                {
                    builder.AppendLine("<" + prop.Name + ">" + "</" + prop.Name + ">");
                }
            }
            var complexProps = treeViewModel.ComplexProperty;
            foreach (var prop in complexProps)
            {
                if (!prop.IsSelected.Value) continue;
                builder.AppendLine(GetXml(prop));
            }
            builder.AppendLine("</" + treeViewModel.Name + ">");
            return builder.ToString();
        }

        private void DdlActionSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((e.AddedItems.Count > 0) && (e.RemovedItems.Count > 0))
                CreateTreeView();
        }
    }
}
