﻿using KAMP.Core.FrameworkComponents;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCAssignReassign.xaml
    /// </summary>
    using Core.Workflow;
    using KAMP.KYC.BLL;
    using Core.Workflow.Helpers;
    public partial class UCAssignReassign : UserControl
    {

        public Action<int, long> OnRefreshAssignedCollection;
        public Action<int, long> OnRefreshUnAssignedCollection;
        public Action CloseWindow;
        private Grid window;
        private AssignReAssignVM _viewModel;
        public UCAssignReassign(bool isAssign, CaseDetails crntCase, Action<int, string, long> onAssignRefreshGrid, Action<int, string, long> onUnAssignRefreshGrid, Grid win)
        {
            InitializeComponent();
            window = win;
            _viewModel = new AssignReAssignVM()
            {
                CurrentCase = crntCase,
                IsAssign = isAssign,
                OnAssignRefreshGrid = onAssignRefreshGrid,
                OnUNAssignRefreshGrid = onUnAssignRefreshGrid,
                Close = Close,
            };
            IntializedData();
            DataContext = _viewModel;
        }

        private void IntializedData()
        {
            var kycBL = new KYCBLL();

            if (_viewModel.CurrentCase.Statename.Equals(Helper.Status.Assigned) || _viewModel.CurrentCase.Status.Equals(Helper.Status.UnAssigned) || _viewModel.CurrentCase.Statename.Equals(Helper.Status.UnAssigned))
                _viewModel.Users = kycBL.GetUsersFromAnalystGroup();

            else if (_viewModel.CurrentCase.StateId > 0)
                _viewModel.Users = kycBL.GetUsersByStateId(_viewModel.CurrentCase.StateId);

            _viewModel.Users.Insert(0, new Core.Repository.UM.User { UserId = 0, UserName = KYCConstants.DefaultDropDownValue });

        }


        private void Close()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(window);
            }
        }
    }
}
