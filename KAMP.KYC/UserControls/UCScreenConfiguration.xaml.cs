﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    using BLL;
    using ViewModels;

    /// <summary>
    /// Interaction logic for UCScreenConfiguration.xaml
    /// </summary>
    using Core.FrameworkComponents;
    public partial class UCScreenConfiguration : UserControl, IPageControl
    {
        public UCScreenConfiguration()
        {
            InitializeComponent();
        }

        public dynamic InitializeViewModel()
        {
            var vm = new ScreenConfigurationPageVM() { };
            vm.ScreenSelectionChanged = OnSelectedScreenChanged;
            return vm;
        }

        private void OnSelectedScreenChanged()
        {
            var viewModel = (this.DataContext as ScreenConfigurationPageVM);
            colIsMandatory.Visibility = viewModel.SelectedScreen.NeedMandatoryField ? Visibility.Visible : Visibility.Hidden;
            colOptionDetails.Visibility = viewModel.SelectedScreen.ShowDetailsIcon ? Visibility.Visible : Visibility.Hidden;
            colToolTip.Visibility = viewModel.SelectedScreen.NeedToolTipCol ? Visibility.Visible : Visibility.Hidden;
        }

        public string PageName
        {
            get { return KYCPageControls.ScreenConfiguration; }
        }
    }
}
