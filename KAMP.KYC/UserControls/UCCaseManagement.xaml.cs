﻿using KAMP.KYC.BLL;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.FrameworkComponents.Helpers;

namespace KAMP.KYC.UserControls
{
    using Core.FrameworkComponents;
    using System.Collections.ObjectModel;
    using KAMP.KYC.AppCode;

    /// <summary>
    /// Interaction logic for UCCaseManagement.xaml
    /// </summary>
    public partial class UCCaseManagement : UserControl, IPageControl
    {
        private Grid gd;
        private int _displayItemCount = KYCConstants.DisplayItemCount;
        public UCCaseManagement()
        {
            InitializeComponent();
        }

        public dynamic InitializeViewModel()
        {
            var caseFileCard = new CaseManagementPageVM();
            IntitializePagination(caseFileCard);
            caseFileCard.OnOpenCaseFileCard = OpneCaseFileCard;
            caseFileCard.OnCloseCaseFileCard = CloseCaseFileCard;
            return caseFileCard;
        }

        private void IntitializePagination(CaseManagementPageVM caseFileCard)
        {
            caseFileCard.CaseFiles = new ObservableCollection<CaseFileVM>(caseFileCard.BkpCaseFiles.Take(_displayItemCount));

            var totalcount = caseFileCard.BkpCaseFiles.Count();

            caseFileCard.PaginationVM = new PaginationVM
            {
                TotalItem = totalcount,
                StartIndex = caseFileCard.CaseFiles.IsCollectionValid() ? 1 : 0,
                CurrentIndex = caseFileCard.CaseFiles.IsCollectionValid() ? 1 : 0,
                DisplayitemCount = _displayItemCount,
                PageClick = PageClick,
            };
        }

        private void PageClick()
        {
            var _viewModel = (this.DataContext as CaseManagementPageVM);
            int skipItem = _viewModel.PaginationVM.StartIndex - 1;
            _viewModel.CaseFiles = new ObservableCollection<CaseFileVM>(_viewModel.BkpCaseFiles.Skip(skipItem).Take(_displayItemCount));
        }

        private void OpneCaseFileCard()
        {
            if (gd.IsNotNull())
            {
                AppVariables.KYCViewModel.IsPopupOpen = false;
                return;
            }

            var caseFileCard = new UCCaseFileCard() { Name = "CaseFileCard", Margin = new Thickness(0) };
            var caseFileCardVM = (this.DataContext as CaseManagementPageVM).CaseFileCardVM;
            AppVariables.CaseFileCardPageVM = caseFileCardVM;
            var kycBll = new KYCBLL();

            caseFileCardVM.BankDataVM = kycBll.GetBankData(caseFileCardVM.CaseFileDetails.cf_ID);
            caseFileCardVM.AccountVM = kycBll.GetAccountData(caseFileCardVM.CaseFileDetails.cf_ID);
            caseFileCardVM.KPMGEditableVM = new KPMGEditablePageVM() { KPMGEditableGrps = kycBll.GetKPMGEditableData(caseFileCardVM.CaseFileDetails.cf_ID) };
            caseFileCardVM.Classifications = new ObservableCollection<ReqClassificationVM>();
            caseFileCardVM.Classifications.AddRange(kycBll.GetValidClassifications());
            caseFileCardVM.FileLogViewModel = new FileLogsViewModel()
            {
                CfId = caseFileCardVM.CaseFileDetails.cf_ID,
                AssignedUser = caseFileCardVM.IsAssignedUser
            };


            caseFileCardVM.CaseHistoryViewModel = new CaseHistoryVM();
            caseFileCardVM.CaseHistoryViewModel.Entities = kycBll.GetEntities();
            caseFileCardVM.CaseHistoryViewModel.CfId = caseFileCardVM.CaseFileDetails.cf_ID;
            caseFileCardVM.CaseHistoryViewModel.CaseHistoryList = kycBll.GetCaseHistory(caseFileCardVM.CaseHistoryViewModel.Entities, 0);
            caseFileCardVM.CaseHistoryViewModel.UserList = kycBll.GetExistingUsers();
            caseFileCardVM.CaseHistoryViewModel.SelectedUser = caseFileCardVM.CaseHistoryViewModel.UserList.FirstOrDefault().UserName;

            caseFileCardVM.SelectedCategories = kycBll.GetCategories(caseFileCardVM.CaseFileDetails.cf_ID);

            caseFileCardVM.SelectedClassifications = new ObservableCollection<ReqClassificationVM>();

            if (caseFileCardVM.SelectedCategories.IsCollectionValid())
            {
                var selectedClassifications = caseFileCardVM.SelectedCategories.GroupBy(x => x.Classification.Id)
                                                            .Select(x =>
                                                            {
                                                                var classification = x.First().Classification;
                                                                classification.ReqCategories = new ObservableCollection<ReqCategoryVM>(x.ToList());
                                                                return classification;
                                                            }
                                                            ).ToList();

                caseFileCardVM.SelectedClassifications.AddRange(selectedClassifications);
            }

            caseFileCardVM.RefreshCategoriesByClsfcn();

            caseFileCardVM.RequirementTabVM = new RequirementTabVM() { Requirements = kycBll.GetRequirements(caseFileCardVM.CaseFileDetails.cf_ID) };

            //Bubbling the save and close events on response screen to case file card VM
            caseFileCardVM.RequirementTabVM.OnShowResponseScreen = caseFileCardVM.ShowResponseScreen;
            caseFileCardVM.RequirementTabVM.OnReqDetialsResponseSave = caseFileCardVM.ReqDetialsResponseSave;
            caseFileCardVM.RequirementTabVM.OnReqDetialsResponseClose = caseFileCardVM.CloseResponseDialogs;
            caseFileCardVM.IsCaseFileCardEnabled = true;

            if (caseFileCardVM.RequirementTabVM.Requirements.IsCollectionValid())
            {
                foreach (var req in caseFileCardVM.RequirementTabVM.Requirements)
                {
                    req.OnReqDetialsResponseSave = caseFileCardVM.RequirementTabVM.OnReqDetialsResponseSave;
                    req.OnReqDetailsResponseClose = caseFileCardVM.RequirementTabVM.OnReqDetialsResponseClose;
                }
            }

            caseFileCard.DataContext = caseFileCardVM;
            gd = new Grid();
            Label lbl = new Label();
            Panel.SetZIndex(caseFileCard, 100);
            lbl.Background = Brushes.Black;
            lbl.Opacity = .5;
            gd.Children.Add(lbl);
            AppVariables.KYCViewModel.IsPopupOpen = false;
            caseFileCard.DataContext = caseFileCardVM;
            gd.Children.Add(caseFileCard);
            AppVariables.MainGrid.Children.Add(gd);
            // AppVariables.MainGrid.Children.Add(caseFileCard);            
        }

        private void CloseCaseFileCard()
        {
            //var caseFileCard = AppVariables.MainGrid.Children.Cast<FrameworkElement>().FirstOrDefault(x => x.Name == "CaseFileCard");
            if (gd.IsNotNull())
                AppVariables.MainGrid.Children.Remove(gd);
            gd = null;
        }

        public string PageName
        {
            get { return KYCPageControls.CaseManagement; }
        }

        private void Export(object sender, MouseButtonEventArgs e)
        {
            var caseFileCardVM = this.DataContext as CaseManagementPageVM;
            var caseFiles = caseFileCardVM.CaseFiles.ToList();
            var mappedCaseDetails = AutoMapper.Mapper.Map<List<CaseFileVM>, List<CaseViewsModel>>(caseFiles);
            try
            {
                KAMP.Core.FrameworkComponents.Helpers.Export.CreateExcelDocument<CaseViewsModel>(mappedCaseDetails);
            }
            catch (Exception ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }
    }
}
