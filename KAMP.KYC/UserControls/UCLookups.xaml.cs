﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.KYC;
using KAMP.KYC.BLL;
using KAMP.KYC.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.UserControls
{
    /// <summary>
    /// Interaction logic for UCLookups.xaml
    /// </summary>
    public partial class UCLookups : UserControl, IPageControl
    {
       private LookUpsVM _viewModel = null;
       public Grid lookUpGrid { get; set; }

        public UCLookups()
        {
            InitializeComponent();

        }

        public string PageName
        {
            get { return KYCPageControls.LookUps; }
        }

        public dynamic InitializeViewModel()
        {
            _viewModel = new LookUpsVM();
            var bLL = new KYCBLL();
            _viewModel.LookUpGroupList = bLL.GetLookGroups();
            _viewModel.SelectedGroup = _viewModel.LookUpGroupList.FirstOrDefault();
            this.DataContext = _viewModel;
            return _viewModel;
        }


        private void LookUpSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ddl = sender as ComboBox;
            if (ddl.SelectedIndex == -1 || ddl.SelectedIndex == 0)
            {
                _viewModel.LookUpsList = null;
            }
            else
            {
                var item = ddl.SelectedItem as KYCLookUpGroups;
                _viewModel.SelectedCategory = item.Name;
                var bLL = new KYCBLL();
                _viewModel.LookUpsList = bLL.GetLookUp(item.Id);
            }
        }

        private void EditLookUp(object sender, MouseButtonEventArgs e)
        {
            if (lookUpGrid.IsNotNull())
                return;
            var item = DgLookUps.SelectedItem as KYCLookUp;
            var uc = new AddEditLookUp(item, _viewModel.SelectedGroup);
            uc.Close = CloseLookUp;
            lookUpGrid = new Grid();
            lookUpGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            lookUpGrid.Children.Add(lbl);
            lookUpGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(lookUpGrid);
            }

        }

        private void CloseLookUp()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(lookUpGrid);
                lookUpGrid = null;
            }
            var bLL = new KYCBLL();
            _viewModel.LookUpsList = bLL.GetLookUp(_viewModel.SelectedGroup.Id);
        }

        private void DeleteLookUp(object sender, MouseButtonEventArgs e)
        {
            var result = MessageBoxControl.Show("Are You sure you want to delete this look up?", MessageBoxButton.YesNo, MessageType.Alert);
            if (result == MessageBoxResult.No) return;
            var bLL = new KYCBLL();
            var item = DgLookUps.SelectedItem as KYCLookUp;
            bLL.DeleteLookUp(item);
            _viewModel.LookUpsList.Remove(item);
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void AddLookUp(object sender, RoutedEventArgs e)
        {
            if (_viewModel.SelectedGroup.Name.Equals(KYCConstants.DefaultDDLValue))
            {
                MessageBoxControl.Show("Please select a look up category.", MessageBoxButton.OK, MessageType.Error);
                return;
            }

            if (lookUpGrid.IsNotNull())
                return;

            var item = DgLookUps.SelectedItem as KYCLookUp;
            var uc = new AddEditLookUp(null, _viewModel.SelectedGroup);
            uc.Close = CloseLookUp;
            lookUpGrid = new Grid();
            lookUpGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            lookUpGrid.Children.Add(lbl);
            lookUpGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(lookUpGrid);
            }
        }



    }
}
