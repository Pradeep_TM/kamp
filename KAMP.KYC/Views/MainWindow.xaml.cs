﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.KYC.Views
{
    using ViewModels;
    using UserControls;
    using Core.FrameworkComponents;
    using KAMP.KYC.AppCode;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        KYCViewModel _viewModel;
        Dictionary<string, IPageControl> _pageCtrlsLkp;

        public MainWindow()
        {
            InitializeComponent();
            AssignDataContext();            
            InitializeUserControls();
            Loaded += KYCLoaded;

        }

        private void KYCLoaded(object sender, RoutedEventArgs e)
        {
            AssignDataContext();
            InitializeUserControls();
        }

        private void InitializeUserControls()
        {
            _pageCtrlsLkp = new Dictionary<string, IPageControl>();

            var configCntrl = new UCScreenConfiguration();
            var configPageCntrl = configCntrl as IPageControl;
            _pageCtrlsLkp.Add(KYCPageControls.ScreenConfiguration, configPageCntrl);
            configCntrl.DataContext = configPageCntrl.InitializeViewModel();
            gdPageContainer.Children.Add(configCntrl);
        }

        private void AssignDataContext()
        {
            _viewModel = new KYCViewModel { CurrentPageName = KYCPageControls.ScreenConfiguration };
            _viewModel.NavigationViewModel.LoadUserControl += LoadPageControl;
            
            this.DataContext = _viewModel;
        }

        private void LoadPageControl(string ucName)
        {
            new BootStrapper().Configure();

            //var userControl = (KYCPageControls)Enum.Parse(typeof(KYCPageControls), ucName);
            if (!_viewModel.CurrentPageName.Equals(ucName))
            {
                var removeChild = _pageCtrlsLkp[_viewModel.CurrentPageName];
                var prevControl = removeChild as UserControl;
                gdPageContainer.Children.Remove(prevControl);
                _viewModel.CurrentPageName = ucName;

                var selPageCntrl = _pageCtrlsLkp[ucName];
                var currentControl = selPageCntrl as UserControl;
                currentControl.DataContext = selPageCntrl.InitializeViewModel();
                gdPageContainer.Children.Add(currentControl);                
            }
        }

        #region EVENT HANDLERS
        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //this.Close();
            Application.Current.Shutdown();
        }

        public override void OnApplyTemplate()
        {
            DependencyObject minimizeBtn = GetTemplateChild("PART_MINIMIZE");
            if (minimizeBtn != null)
            {
                (minimizeBtn as Button).Click += Minimize_Click;
            }

            DependencyObject titleBar = GetTemplateChild("PART_TITLEBAR");
            if (titleBar != null)
            {
                (titleBar as Border).MouseLeftButtonDown += Title_MouseLeftButtonDown; ;
            }

            base.OnApplyTemplate();
        }

        void Title_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        void Minimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
        #endregion
    }
}
