﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KAMP.KYC.Views
{
    using AppCode;
    using UserControls;
    using ViewModels;
    using Core.FrameworkComponents;
    using KAMP.REPORT.Views;
    using KAMP.Core.Common.BLL;
    //using KAMP.REPORT.Views;

    /// <summary>
    /// Interaction logic for EntryWindow.xaml
    /// </summary>
    public partial class EntryWindow : Window
    {
        KYCViewModel _viewModel;
        Dictionary<string, IPageControl> _pageCtrlsLkp;
        UIElement CurrentLoadedUserControl = null;

        public EntryWindow()
        {
            InitializeComponent();
            Loaded += EntryWindow_Loaded;
        }

        void EntryWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadKYCModule();
        }

        private void LoadKYCModule()
        {

            AdduserLoginHistory();
            AssignDataContext();
            AssignEventHandlers();
            InitializeUserControls();
        }

        private void AdduserLoginHistory()
        {
            var commonBl = new CommonBL();
            var context = Application.Current.Properties["Context"] as Context;
            commonBl.AddUserLoginHistory(context, DateTime.Now);
        }

        private void InitializeUserControls()
        {
            _pageCtrlsLkp = new Dictionary<string, IPageControl>();

            var items = typeof(IPageControl).GetImplementors();

            foreach (var item in items.AsParallel())
            {
                var pageCntrl = Activator.CreateInstance(item) as IPageControl;
                _pageCtrlsLkp.Add(pageCntrl.PageName, pageCntrl);
            }
            LoadPageControl(KYCPageControls.Home);

        }

        private void AssignDataContext()
        {
            _viewModel = new KYCViewModel { };
            this.DataContext = _viewModel;
        }

        private void AssignEventHandlers()
        {
            UCHeader.CloseApp += CloseApplication;
            _viewModel.NavigationViewModel.LoadUserControl += LoadPageControl;
        }

        private void CloseApplication()
        {
            if (CurrentLoadedUserControl.GetType().ToString().Equals(typeof(ReportMainControl).ToString()))
                CurrentLoadedUserControl.Visibility = Visibility.Hidden;
            var result = MessageBoxControl.Show(KYCConstants.CloseApplicationText, MessageBoxButton.YesNo, type: MessageType.Alert);
            if (result == MessageBoxResult.Yes)
                Application.Current.Shutdown();
            else
                CurrentLoadedUserControl.Visibility = Visibility.Visible;

        }

        public void LoadPageControl(string ucName)
        {
            new BootStrapper().Configure();

            //var userControl = (KYCPageControls)Enum.Parse(typeof(KYCPageControls), ucName);
            if (_viewModel.CurrentPageName.IsNull() || !_viewModel.CurrentPageName.Equals(ucName))
            {
                if (_viewModel.CurrentPageName.IsNotEmpty())
                {
                    var removeChild = _pageCtrlsLkp[_viewModel.CurrentPageName];
                    var prevControl = removeChild as UserControl;
                    gdPageContainer.Children.Remove(prevControl);
                }

                _viewModel.CurrentPageName = ucName;
                var selPageCntrl = _pageCtrlsLkp[ucName];
                var currentControl = selPageCntrl as UserControl;
                currentControl.DataContext = selPageCntrl.InitializeViewModel();
                gdPageContainer.Children.Add(currentControl);
                CurrentLoadedUserControl = currentControl;

            }
        }
    }
}
