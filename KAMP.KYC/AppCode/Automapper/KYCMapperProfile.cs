﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace KAMP.KYC.AppCode
{
    using Core.Repository.KYC;
    using ViewModels;
    using Core.FrameworkComponents;
    using KAMP.Core.UserManagement.AppCode;
    using System.Threading;
    using Core.Workflow;
    using KAMP.Core.Repository.UM;

    public class KYCMapperProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();

            Mapper.CreateMap<KPMGEditable, KPMGEditableVM>()
                .ForMember(d => d.FormElementOption,
                            s => s.MapFrom(p => p.FormElement.ValueType == (int)RequirementType.SingleSelectOption && p.FormElementOptions.IsCollectionValid() ?
                                                        Mapper.Map<FormElementOptionVM>(p.FormElementOptions.FirstOrDefault()) : null))
                .AfterMap((s, d) =>
                {
                    d.FormElement.OptionSelectionChanged = () => d.IsUpdated = true;
                    if (s.FormElement.ValueType == (int)RequirementType.MultiSelectOption)
                    {
                        d.FormElement.Options.Each(op =>
                        {
                            if (s.FormElementOptions.IsCollectionValid())
                                op.IsSelected = s.FormElementOptions.Any(fe => fe.Id == op.Id);

                            op.SelectionChanged += d.FormElement.OptionSelectionChanged;
                        });
                    }
                    d.IsUpdated = false;
                })
                .IgnoreAllNonExisting();

            Mapper.CreateMap<KPMGEditableVM, KPMGEditable>()
                .ForMember(d => d.FormElementOptions,
                            s => s.MapFrom(p => GetFromElementOptions(p)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<FormElement, FormElementVM>()
                .ForMember(d => d.Options,
                            s => s.MapFrom(p => p.FormElementOption))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<FormElementVM, FormElement>()
                .ForMember(d => d.FormElementOption,
                            s => s.MapFrom(p => p.Options.ToList()))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<FormElementOptionVM, FormElementOption>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<FormElementOption, FormElementOptionVM>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqClassificationVM, ReqClassification>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqClassification, ReqClassificationVM>()
                .ForMember(x => x.ReqCategories, s => s.MapFrom(p => p.ReqCategory))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqCategoryVM, ReqCategory>()
                .ForMember(d => d.ReqClassification,
                            s => s.MapFrom(p => p.Classification))
                .ForMember(d => d.ReqClassificationId,
                            s => s.MapFrom(p => p.Classification.Id))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqCategory, ReqCategoryVM>()
                .ForMember(d => d.Classification,
                            s => s.MapFrom(p => p.ReqClassification))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqOptionVM, ReqOption>()
                .ForMember(d => d.CreatedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.ModifiedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.CreatedBy,
                            s => s.MapFrom(p => (int)AppVariables.KAMPPrincipal.UserId))
                .ForMember(d => d.ModifiedBy,
                            s => s.MapFrom(p => (int)AppVariables.KAMPPrincipal.UserId))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqOption, ReqOptionVM>()
                .ForMember(d => d.CreatedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.ModifiedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.CreatedBy,
                            s => s.MapFrom(p => (int)AppVariables.KAMPPrincipal.UserId))
                .ForMember(d => d.ModifiedBy,
                            s => s.MapFrom(p => (int)AppVariables.KAMPPrincipal.UserId))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqGridColumnVM, ReqGridColumn>()
                .ForMember(d => d.ColumnName,
                            s => s.MapFrom(p => p.ColumnText))
                .ForMember(d => d.ReqTypeId,
                            s => s.MapFrom(p => (int)p.ColumnType))
                .ForMember(d => d.ReqOptions,
                            s => s.MapFrom(p => GetRequirementDetails<ICollection<ReqOption>>(p.ColumnContent, RequirementType.SingleSelectOption)))
                .ForMember(d => d.CreatedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.ModifiedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.CreatedBy,
                            s => s.MapFrom(p => (int)(AppVariables.KAMPPrincipal.UserId)))
                .ForMember(d => d.ModifiedBy,
                            s => s.MapFrom(p => (int)(AppVariables.KAMPPrincipal.UserId)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ReqGridColumn, ReqGridColumnVM>()
                .ForMember(d => d.ColumnText,
                            s => s.MapFrom(p => p.ColumnName))
                .ForMember(d => d.ColumnType,
                            s => s.MapFrom(p => (RequirementType)p.ReqTypeId))
                .ForMember(d => d.ColumnContent, s => s.MapFrom(p => GetRequirementDetails(p)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<RequirementVM, Requirement>()
                .ForMember(d => d.ChildRequirements,
                            s => s.MapFrom(p => p.ChildRequirements))
                .ForMember(d => d.ReqCategoryId,
                            s => s.MapFrom(p => p.Category.Id))
                .ForMember(d => d.ReqType,
                            s => s.MapFrom(p => (int)p.ReqType))
                .ForMember(d => d.ReqOption,
                            s => s.MapFrom(p => GetRequirementDetails<ICollection<ReqOption>>(p.RequirementDetails, RequirementType.SingleSelectOption)))
                .ForMember(d => d.ReqGridColumn,
                            s => s.MapFrom(p => GetRequirementDetails<ICollection<ReqGridColumn>>(p.RequirementDetails, RequirementType.Grid)))
                .ForMember(d => d.CreatedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.ModifiedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.CreatedBy,
                            s => s.MapFrom(p => (int)(AppVariables.KAMPPrincipal.UserId)))
                .ForMember(d => d.ModifiedBy,
                            s => s.MapFrom(p => (int)(AppVariables.KAMPPrincipal.UserId)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<KYCCaseFile, CaseDetails>()
                .ForMember(d => d.CaseNumber, s => s.MapFrom(p => p.CaseNo))
                .ForMember(d => d.CategoryId, s => s.MapFrom(p => p.CategoryId))
                .ForMember(d => d.CfId, s => s.MapFrom(p => p.cf_ID))
                .ForMember(d => d.TransactionCount, s => s.MapFrom(p => p.TransactionCount))
                .ForMember(d => d.TransactionScore, s => s.MapFrom(p => p.TransactionScore))
                .ForMember(d => d.AccountCount, s => s.MapFrom(p => p.AccountCount))
                .ForMember(d => d.AccountScore, s => s.MapFrom(p => p.AccountScore))
                .ForMember(d => d.CustomerName, s => s.MapFrom(p => p.CustomerName))
                .ForMember(d => d.CustomerNumber, s => s.MapFrom(p => p.CustomerNo))
                .ForMember(d => d.CreateDate, s => s.MapFrom(p => p.CreatedDate))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<CaseDetails, CaseDetails>()
             .IgnoreAllNonExisting();

            Mapper.CreateMap<CaseDetail, CaseDetails>()
                .ForMember(d => d.UserId, s => s.MapFrom(p => p.UserId))
                .ForMember(d => d.UserName, s => s.MapFrom(p => p.UserName))
                .ForMember(d => d.StateId, s => s.MapFrom(p => p.StateId))
                .ForMember(d => d.Statename, s => s.MapFrom(p => p.State))
                .ForMember(d => d.Status, s => s.MapFrom(p => p.Status))
                .ForMember(d => d.IsOpen, s => s.MapFrom(p => p.IsOpen))
                .ForMember(d => d.LevelOpenDate, s => s.MapFrom(p => p.LevelOpenDate))
                .ForMember(d => d.HighestStatus, s => s.MapFrom(p => p.HighestStatus))
                .ForMember(d => d.CloseDate, s => s.MapFrom(p => p.CaseClosedDate))
                .ForMember(d => d.Analyst, s => s.MapFrom(p => p.LastAnalyst))
                .ForMember(d => d.AssignDate, s => s.MapFrom(p => p.AssignDate))
                .ForMember(d => d.ParkDate, s => s.MapFrom(p => p.ParkedDate))
                .ForMember(d => d.ParkReason, s => s.MapFrom(p => p.ParkReason))
                .ForMember(d => d.CaseOpenDate, s => s.MapFrom(p => p.CaseOpenDate))
                .ForMember(d => d.DaysInQueue, s => s.MapFrom(p => p.DaysInQueue))
                .IgnoreAllNonExisting();

            #region Requirement
            Mapper.CreateMap<Response, ResponseVM>()
                .ForMember(d => d.ResponseOptions, s => s.MapFrom(p => p.ResponseOption))
                .ForMember(d => d.ResponseStatus, s => s.MapFrom(p => (ResponseState)p.ResponseState))
                .AfterMap((s, d) =>
                {
                    d.ResponseGridRows = GetResponseGridRows(s.ResponseGridCell, d);
                })
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ResponseVM, Response>()
                .ForMember(d => d.ResponseOption, s => s.MapFrom(p => p.ResponseOptions))
                .ForMember(d => d.ResponseGridCell, s => s.MapFrom(p => GetResponseGridCells(p.ResponseGridRows)))
                .ForMember(d => d.CreatedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.ModifiedDate,
                            s => s.MapFrom(p => DateTime.Now))
                .ForMember(d => d.CreatedBy,
                            s => s.MapFrom(p => (int)(AppVariables.KAMPPrincipal.UserId)))
                .ForMember(d => d.ModifiedBy,
                            s => s.MapFrom(p => (int)(AppVariables.KAMPPrincipal.UserId)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ResponseGridCell, ResponseGridCellVM>()
                .ForMember(d => d.ResponseOption, s => s.MapFrom(p => (RequirementType)p.ReqGridColumn.ReqTypeId == RequirementType.SingleSelectOption ? p.ResponseOptions.FirstOrDefault() : null))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ResponseGridCellVM, ResponseGridCell>()
                .ForMember(d => d.ResponseOptions, s => s.MapFrom(p => p.ReqGridColumn.ColumnType == RequirementType.SingleSelectOption ? (p.ResponseOption.IsNotNull() ? new List<ResponseOptionVM>() { p.ResponseOption } : null)
                                                                                                                            : (p.ResponseOptions.IsCollectionValid() ? p.ResponseOptions.Where(x => x.IsSelected).ToList() : new List<ResponseOptionVM>())))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ResponseOption, ResponseOptionVM>()
                .ForMember(d => d.ReqOptionVM, s => s.MapFrom(p => p.RequirementOption))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ResponseOptionVM, ResponseOption>()  
                .ForMember(d => d.RequirementOption, s => s.MapFrom(p => p.ReqOptionVM))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Requirement, RequirementVM>()
                .ForMember(d => d.Category, s => s.MapFrom(p => p.ReqCategory))
                .ForMember(d => d.ResponseVM, s => s.MapFrom(p =>
                    p.Response.IsCollectionValid() ? p.Response.First() : new Response() { ResponseState = (int)ResponseState.UnAnswered }))
                .ForMember(d => d.RequirementDetails, s => s.MapFrom(p => GetRequirementDetails(p)))
                .AfterMap((s, d) =>
                    {
                        if (d.ResponseVM.IsNotNull())
                        {
                            if (d.ReqType == RequirementType.FreeText)
                            {
                                d.ResponseVM.ResponsePreview = d.ResponseVM.ResponseText;
                            }
                            else if (d.ReqType == RequirementType.SingleSelectOption && d.ResponseVM.ResponseOptions.IsCollectionValid())
                            {
                                d.ResponseVM.ResponsePreview = d.ResponseVM.ResponseOptions.First().ReqOptionVM.OptionName;
                            }
                            else if (d.ReqType == RequirementType.MultiSelectOption && d.ResponseVM.ResponseOptions.IsCollectionValid())
                            {
                                d.ResponseVM.ResponsePreview = KYCConstants.MultipleValues;
                            }
                            else if (d.ReqType == RequirementType.Grid && d.ResponseVM.ResponseState != (short)ResponseState.UnAnswered)
                            {
                                d.ResponseVM.ResponsePreview = KYCConstants.GridOfAnswers;
                            }
                        }
                    })
                .IgnoreAllNonExisting();
            #endregion

            Mapper.CreateMap<CaseDetail, CaseViewsModel>()
               .ForMember(d => d.LastAnalyst, s => s.MapFrom(p => p.LastAnalyst))
               .ForMember(d => d.UserName, s => s.MapFrom(p => p.UserName))
               .ForMember(d => d.HighestStatus, s => s.MapFrom(p => p.HighestStatus))
               .ForMember(d => d.State, s => s.MapFrom(p => p.State))
               .ForMember(d => d.DaysInQueue, s => s.MapFrom(p => p.DaysInQueue))
               .ForMember(d => d.Status, s => s.MapFrom(p => p.Status)).IgnoreAllNonExisting();

            Mapper.CreateMap<CaseFileVM, CaseViewsModel>()
                .ForMember(d => d.CaseNumber, s => s.MapFrom(p => p.CaseNo))
                .AfterMap((s, d) =>
                {
                    var mappedCaseDetail = Mapper.Map<CaseDetail, CaseViewsModel>(s.WfDetails);
                    d.LastAnalyst = mappedCaseDetail.LastAnalyst;
                    d.UserName = mappedCaseDetail.UserName;
                    d.HighestStatus = mappedCaseDetail.HighestStatus;
                    d.State = mappedCaseDetail.State;
                    d.DaysInQueue = mappedCaseDetail.DaysInQueue;
                    d.Status = mappedCaseDetail.Status;
                })
                .IgnoreAllNonExisting();

            Mapper.CreateMap<User, KYCUser>()
                .ForMember(d => d.UserId, s => s.MapFrom(x => x.UserId))
                .ForMember(d => d.UserName, s => s.MapFrom(x => x.UserName))
                .ForMember(d => d.StateIdList, s => s.MapFrom(x => GetAllStateIds(x.UserInGroup)));

        }

        private List<int> GetAllStateIds(ICollection<UserInGroup> collection)
        {
            List<int> stateIds = new List<int>();
            collection.Each((x) =>
            {
                var ids = x.Group.States.Select(y => y.Id).ToList();
                stateIds.AddRange(ids);
            });
            return stateIds;
        }

        private List<FormElementOption> GetFromElementOptions(KPMGEditableVM p)
        {
            var formEleOptions = new List<FormElementOptionVM>();

            if (p.FormElement.ValueType == (int)RequirementType.SingleSelectOption && p.FormElementOption.IsNotNull())
            {
                formEleOptions.Add(p.FormElementOption);
            }

            if (p.FormElement.ValueType == (int)RequirementType.MultiSelectOption && p.FormElement.Options.IsCollectionValid())
            {
                var options = p.FormElement.Options.Where(x => x.IsSelected).ToList();
                formEleOptions.AddRange(options);
            }

            return Mapper.Map<List<FormElementOption>>(formEleOptions);
        }

        //private TMember GetFromElementOptionsVM(KPMGEditableVM p)
        //{
        //    throw new NotImplementedException();
        //}

        private ICollection<ResponseGridCell> GetResponseGridCells(ObservableCollection<ResponseGridRowVM> responseRows)
        {
            if (!responseRows.IsCollectionValid())
                return null;

            return responseRows.SelectMany(x =>
               {
                   return Mapper.Map<ICollection<ResponseGridCell>>(x.ResponseGridCells);
               }).ToList();
        }

        private ObservableCollection<ResponseGridRowVM> GetResponseGridRows(ICollection<ResponseGridCell> responseGridCells, ResponseVM resp)
        {
            if (!responseGridCells.IsCollectionValid())
                return null;

            return new ObservableCollection<ResponseGridRowVM>(responseGridCells.GroupBy(x => x.RowId)
                    .Select((x, index) => new ResponseGridRowVM()
                    {
                        RowId = index,
                        ResponseGridCells = Mapper.Map<ObservableCollection<ResponseGridCellVM>>(x.ToList()),
                        OnKPMGEditableRowDelete = resp.DeleteGridRow
                    })
                    .ToList());
        }

        private IReqDetails GetRequirementDetails(Requirement req)
        {
            var requirementType = (RequirementType)req.ReqType;

            switch (requirementType)
            {
                case RequirementType.SingleSelectOption:
                    return new SingleSelectReqDetailsVM() { Options = Mapper.Map<ObservableCollection<ReqOptionVM>>(req.ReqOption) };
                case RequirementType.MultiSelectOption:
                    return new MultiSelectReqDetailsVM() { Options = Mapper.Map<ObservableCollection<ReqOptionVM>>(req.ReqOption) };
                case RequirementType.Grid:
                    return new GridReqDetailsVM() { Columns = Mapper.Map<ObservableCollection<ReqGridColumnVM>>(req.ReqGridColumn) };
                default:
                    return null;
            }
        }

        private IReqDetails GetRequirementDetails(ReqGridColumn col)
        {
            var requirementType = (RequirementType)col.ReqTypeId;

            switch (requirementType)
            {
                case RequirementType.SingleSelectOption:
                    return new SingleSelectReqDetailsVM() { Options = Mapper.Map<ObservableCollection<ReqOptionVM>>(col.ReqOptions) };
                case RequirementType.MultiSelectOption:
                    return new MultiSelectReqDetailsVM() { Options = Mapper.Map<ObservableCollection<ReqOptionVM>>(col.ReqOptions) };
                default:
                    return null;
            }
        }

        private T GetRequirementDetails<T>(IReqDetails reqDetails, RequirementType destType) where T : class
        {
            switch (destType)
            {
                case RequirementType.Grid:
                    if (reqDetails is GridReqDetailsVM)
                    {
                        return Mapper.Map<ICollection<ReqGridColumn>>((reqDetails as GridReqDetailsVM).Columns) as T;
                    }
                    return null;
                case RequirementType.SingleSelectOption:
                case RequirementType.MultiSelectOption:
                    T options = null;
                    if (reqDetails is SingleSelectReqDetailsVM)
                    {
                        options = Mapper.Map<ICollection<ReqOption>>((reqDetails as SingleSelectReqDetailsVM).Options) as T;
                    }
                    else if (reqDetails is MultiSelectReqDetailsVM)
                    {
                        options = Mapper.Map<ICollection<ReqOption>>((reqDetails as MultiSelectReqDetailsVM).Options) as T;
                    }

                    return options;
            }

            return null;
        }
    }
}
