﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace KAMP.KYC.AppCode
{
    public class ItemChangeObservableCollection<T> : ObservableCollection<T> where T : INotifyPropertyChanged
    {
        public ItemChangeObservableCollection()
            : base()
        {
            CollectionChanged += new NotifyCollectionChangedEventHandler(ContentCollectionChanged);
        }

        public event PropertyChangedEventHandler CollectionItemPropertyChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public void ContentCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (T item in e.OldItems)
                {
                    //Removed items
                    item.PropertyChanged -= EntityViewModelPropertyChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (T item in e.NewItems)
                {
                    //Added items
                    item.PropertyChanged += EntityViewModelPropertyChanged;
                }
            }
        }

        public void EntityViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //This will get called when the property of an object inside the collection changes
            if (CollectionItemPropertyChanged != null)
                CollectionItemPropertyChanged(sender, e);
        }

    }
}
