﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace KAMP.KYC.AppCode
{
    using KYC.Views;
    using Core.Interface;
    using Core.Models;
    using KAMP.Core.FrameworkComponents.Enums;
    using KAMP.Core.UserManagement.AppCode;
    using System.Threading;
    using KAMP.Core.FrameworkComponents;
    using System.Windows;
    using KAMP.KYC.BLL;

    [Export(typeof(IModule))]
    public class Entry : IModule
    {

        ModuleMetadata _metadata = null;
        public void Initialize()
        {
            var metadata = new ModuleMetadata();
            metadata.ModuleName = ModuleNames.KYC.ToString();
            metadata.HomeWindow = new EntryWindow();
            new BootStrapper().Configure();
            
            Metadata = metadata;
        }       

        public ModuleMetadata Metadata
        {
            get
            {
                return _metadata;
            }
            set
            {
                this._metadata = value;
            }
        }
    }
}
