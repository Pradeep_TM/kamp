﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace KAMP.KYC.AppCode
{
    using KAMP.Core.FrameworkComponents;

    public class CustomBoundColumn : DataGridBoundColumn
    {
        public string TemplateName { get; set; }
        public string EditingTemplateName { get; set; }

        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            return GenetateTemplateElement(cell, dataItem, TemplateName);
        }

        protected override FrameworkElement GenerateEditingElement(DataGridCell cell, object dataItem)
        {
            return GenetateTemplateElement(cell, dataItem, EditingTemplateName);
        }

        private FrameworkElement GenetateTemplateElement(DataGridCell cell, object dataItem, string templateName)
        {
            if (templateName.IsNotEmpty())
            {
                Binding binding = null;

                if (((Binding)Binding).Path.IsNull())
                {
                    binding = new Binding();
                }
                else
                {
                    binding = new Binding(((Binding)Binding).Path.Path);
                }

                binding.Source = dataItem;

                var content = new ContentControl();
                content.ContentTemplate = (DataTemplate)cell.FindResource(templateName);
                content.SetBinding(ContentControl.ContentProperty, binding);
                return content;
            }

            return null;
        }
    }
}
