﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Workflow;
using System.ComponentModel.DataAnnotations;

namespace KAMP.KYC.AppCode
{
    public class CaseViewsModel
    {
        [Display(Name = "Case No")]
        public string CaseNumber { get; set; }
        [Display(Name = "Name")]
        public string CaseName { get; set; }
        [Display(Name = "Last Analyst")]
        public string LastAnalyst { get; set; }
        [Display(Name = "Assigned To")]
        public string UserName { get; set; }
        [Display(Name = "Is this a Duplicate Case")]
        public string IsDuplicate { get; set; }
        [Display(Name = "Preliminary AML Risk")]
        public string PrimAMLRisk { get; set; }
        [Display(Name = "Us AML Risk Rating")]
        public string PrimSUITRisk { get; set; }
        [Display(Name = "Highest Status Attained")]
        public string HighestStatus { get; set; }
        [Display(Name = "Workflow State")]
        public string State { get; set; }
        [Display(Name = "Days In Queue")]
        public int DaysInQueue { get; set; }
        [Display(Name = "Priority")]
        public Nullable<int> Priority { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
       
    }
}
