﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KAMP.Core.Models;

namespace KAMP.Core.Interface
{
    public interface IModule
    {
        void Initialize();
        ModuleMetadata Metadata
        {
            get;
            set;
        }

    }
}
