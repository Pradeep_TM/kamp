﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Interface
{
    public interface IBootStrapper
    {
        void Configure();
    }
}
