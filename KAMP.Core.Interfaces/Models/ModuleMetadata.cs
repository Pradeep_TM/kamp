﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KAMP.Core.Models
{
    public class ModuleMetadata
    {
        private string _ModuleName;

        public string ModuleName
        {
            get { return _ModuleName; }
            set { _ModuleName = value; }
        }

        private object _window;

        public object HomeWindow
        {
            get { return _window; }
            set { _window = value; }
        }

        public Action SetModuleContext;
        
    }
}
