﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Interfaces
{
   public interface IReports<T>
    {
       Object Execute(T ReportType, Func<T, Object> ReportKPI);
    }
}
