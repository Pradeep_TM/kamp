﻿using KAMP.Core.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace KAMP.Core.Workflow.Testing
{
    using Core.FrameworkComponents;
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            new Workflow.AppCode.BootStrapper().Configure();
            //var bootStrappers = typeof(IBootStrapper).GetImplementors();

            //if (bootStrappers.IsCollectionValid())
            //{
            //    bootStrappers.ForEach(x => ((IBootStrapper)Activator.CreateInstance(x)).Configure());
            //}

            base.OnStartup(e);

           
        }
    }
}
