﻿using KAMP.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.Core.Repository.WF;
using KAMP.Core.Repository.UM;
using KAMP.Core.Workflow.BLL;
namespace KAMP.Core.Workflow.Testing
{
    /// <summary>
    /// Interaction logic for WFProcess.xaml
    /// </summary>
    public partial class WFProcess : Window
    {
        private WorkflowTestVm _viewModel = null;
        public WFProcess()
        {
            _viewModel = new WorkflowTestVm();
            InitiliazeData();
            DataContext = _viewModel;
            InitializeComponent();

        }

        private void InitiliazeData()
        {
            using (var wfUOF = new WFUnitOfWork())
            {
                var userRepo=wfUOF.GetRepository<User>();
                var assignmentRepo = wfUOF.GetRepository<KAMP.Core.Repository.WF.Assignment>();
                var obUsers = new ObservableCollection<User>();
                var users = userRepo.Items.ToList();
                users.ForEach(x => obUsers.Add(x));
                obUsers.Insert(0, new User { UserId = 0, UserName = "Select" });
                _viewModel.Users = obUsers;

                if (_viewModel.WFAction == null)
                    _viewModel.WFAction = new ViewModels.WorkflowActionVm()
                    {
                        BdrBrushColor = "#FF82BCD6",
                        LblForegroundColor = "#0339b1",
                        WindowBackGroundColor = "#FFCBE4E6",
                        CheckBoxFillColor = "#FF0074B9"
                    };
                
                var assignment= assignmentRepo.Items.FirstOrDefault();
                if (assignment.IsNull())
                    return;
                var assignmentId = assignment.Id;
                _viewModel.WFAction.AssignmentId = (int)assignmentId;
                _viewModel.WFAction.CaseNo = "MV00001";
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            _viewModel.WFAction.UserId = int.Parse(lstUser.SelectedValue.ToString());
            if (_viewModel.WFAction.UserId > 0)
                ucWFAction.InitializedData();

        }
    }
}
