﻿using KAMP.Core.Workflow.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Workflow.Testing
{
    using System.Windows.Media.Effects;
    using UserControls;
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Action _backToMain;
        private Action _closeWindow;
        private Window _win;
        public MainWindow()
        {
            _backToMain += OpenThisWindow;
            _closeWindow += CloseWindow;
            InitializeComponent();
        }

        private void OpenThisWindow()
        {
            new MainWindow().Show();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            var win=new WorkflowMain(_backToMain);
            Application.Current.MainWindow = win;
            win.Show();
            this.Close();
        }

        private void Assignment_Click(object sender, RoutedEventArgs e)
        {
            var win=new Assignment(_backToMain);
            Application.Current.MainWindow = win;
            win.Show();
            this.Close();
        }

        private void WorkflowProcess_Click(object sender, RoutedEventArgs e)
        {
            var win = new WFProcess();
            Application.Current.MainWindow = win;
            win.Show();
            this.Close();
        }
        private void CreateState_Click(object sender, RoutedEventArgs e)
        {

            var win=new WorkflowState(_backToMain);
            Application.Current.MainWindow = win;
            win.Show();
            this.Close();
        }

        private void OpenWorkflowUserControl(object sender, RoutedEventArgs e)
        {
            _win = new Window();
            _win.WindowState = WindowState.Maximized;
            _win.AllowsTransparency = true;
            _win.WindowStyle = WindowStyle.None;
            _win.Background = Brushes.Transparent;
            _win.VerticalAlignment = VerticalAlignment.Center;
            var uc = new UCWorkflowMain();
            Grid gd = new Grid();
            gd.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .5;
            gd.Children.Add(lbl);
            gd.Children.Add(uc);
            _win.Content = gd;
            _win.ShowDialog();
        }
        private void CloseWindow()
        {
            if (_win != null)
                _win.Close();
        }
    }
}
