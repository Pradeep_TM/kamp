﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.Workflow.ViewModels;
using System.ComponentModel;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.Core.Repository.WF;
using KAMP.Core.Repository.UM;
namespace KAMP.Core.Workflow.Testing
{
    public class WorkflowTestVm : INotifyPropertyChanged
    {
        public WorkflowActionVm _WFAction;


        public WorkflowActionVm WFAction
        {
            get { return _WFAction; }
            set { PropertyChanged.HandleValueChange(this, value, ref _WFAction, (x) => x.WFAction); }
        }

        private ObservableCollection<User> _users;

        public  ObservableCollection<User> Users
        {
            get { return _users; }
            set { PropertyChanged.HandleValueChange(this,value,ref _users,(x)=>x.Users); }
        }

        private int _selectedUserId;

        public int SelectedUserId
        {
            get { return _selectedUserId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedUserId, (x) => x.SelectedUserId); }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
