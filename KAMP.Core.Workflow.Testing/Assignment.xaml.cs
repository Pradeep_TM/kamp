﻿using Rep = KAMP.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Workflow.UserControls;
using KAMP.Core.Workflow.Helpers;
using KAMP.Core.Repository.WF;
using KAMP.Core.Repository.UM;
using KAMP.Core.Workflow.BLL;
namespace KAMP.Core.Workflow.Testing
{
    /// <summary>
    /// Interaction logic for Assignment.xaml
    /// </summary>
    public partial class Assignment : Window
    {
        private Action _backToMain;
        public Assignment(Action backtoMainWindow)
        {
            _backToMain += backtoMainWindow;
            InitializeComponent();
            InitializedData();
        }


        private void InitializedData()
        {
            using (var wfUOW = new WFUnitOfWork())
            {

                var groupRepo = wfUOW.GetRepository<Group>();
                var userRepo = wfUOW.GetRepository<User>();
                var group = groupRepo.Items.Include(x => x.UserInGroup).FirstOrDefault(x => x.GroupName == "Analyst");
                if (group.IsNull())
                    return;

                var userIds = group.UserInGroup.Where(x => x.ModuleTypeId == 1).Select(x => x.UserId).ToList();
                if (!userIds.IsCollectionValid())
                    return;
                var users = userRepo.Items.Where(x => userIds.Contains(x.UserId)).ToList();

                if (!users.IsCollectionValid())
                    return;

                ddlUsers.ItemsSource = users;
                ddlUsers.DisplayMemberPath = "UserName";
                ddlUsers.SelectedValuePath = "UserId";
            }


        }

        private void Assign_Click(object sender, RoutedEventArgs e)
        {
            using (var wFUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wFUOW.GetRepository<Rep.WF.Assignment>();
                var wfDefinition = wFUOW.GetRepository<WFDefinition>();

             
                var assignment = new Rep.WF.Assignment()
                {
                    CaseNo = TxtCaseNo.Text,
                    Status = Helper.Status.Assigned,
                    AssignedTo = new List<AssignedTo>() { new AssignedTo { UserId = int.Parse(ddlUsers.SelectedValue.ToString()), WorkflowStatus = Helper.Status.InProgess, State = Helper.Status.Assigned } },
                    WFTracking = AssignTask(),

                };

                //To do object type id is hardcoded here we need to get it from the UI
                try
                {
                    assignment.NextState = AssignNextState(wfDefinition.Items.Include(x => x.Steps).FirstOrDefault(x => x.ModuleTypeId == 1));
                    assignmentRepo.Insert(assignment);
                    wFUOW.SaveChanges();
                    OpenMessageBox("Assignment completed successfully.");
                }
                catch(BusinessException ex)
                {
                    OpenMessageBox(ex.Message);
                }
            }

        }

        private static ICollection<WFTracking> AssignTask()
        {
            var assignRules = new List<int> { 0 };
            var assignmentList = new List<WFTracking>();
            foreach (var ruleId in assignRules)
            {
                assignmentList.Add(new WFTracking { CFId = 0, ModuleTypeId = 1, ObjectId = ruleId });
            }
            return assignmentList;
        }


        private string AssignNextState(WFDefinition wF_Definition)
        {
            if (wF_Definition.IsNull())
                throw new BusinessException("Please create a workflow then assign the cases.");

            var step= wF_Definition.Steps.FirstOrDefault(x => x.SequenceNo == 1);
            if (step.IsNull())

                throw new BusinessException("Please create a workflow then assign the cases");
            return step.State;
        }


        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (_backToMain.IsNotNull())
            { _backToMain(); this.Close(); }

        }

        /// <summary>
        /// method to open the popup
        /// </summary>
        /// <param name="msg">meesage to print in the popup</param>
        private void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg);
        }
    }
}
