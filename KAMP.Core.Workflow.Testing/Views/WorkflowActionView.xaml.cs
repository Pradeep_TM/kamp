﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KAMP.Core.Workflow.Testing;

namespace KAMP.Core.Workflow.Testing
{
    /// <summary>
    /// Interaction logic for WorkflowActionView.xaml
    /// </summary>
    public partial class WorkflowActionView : Window
    {
        public WorkflowActionView()
        {
            DataContext = new { BdrBrushColor = "#FF82BCD6", LblForegroundColor = "#0339b1", WindowBackGroundColor = "#FFCBE4E6", CheckBoxFillColor = "#FF0074B9",States=GetStates() };

            InitializeComponent();
        }

        private List<WorkflowStates> GetStates()
        {
            var statesList = new List<WorkflowStates>();
            for (int i = 1; i < 10; i++)
                statesList.Add(new WorkflowStates { Id = i, Name = string.Format("Name{0}", i) });
            return statesList;
        }
    }

    public class WorkflowStates
    {
        public int Id{ get; set; }
        public string Name { get; set; }
    }
}
