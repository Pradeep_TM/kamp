SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KYC_Accounts](
	[cf_ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerNo] [nvarchar](50) NOT NULL,
	[CaseNo] [nvarchar](50) NOT NULL,
	[CustomerName] [nvarchar](150) NULL,
	[CategoryId] [int] NOT NULL,
	[RiskProfile] [nvarchar](max) NULL,
	[AnalystId] [int] NULL,
	[Due_Diligence] [nvarchar](max) NULL,
	[MemorandumOfFact] [nvarchar](max) NULL,
	[TransactionCount] [int] NULL,
	[TransactionScore] [int] NULL,
	[AccountCount] [int] NULL,
	[AccountScore] [int] NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[DemoteCount] [int] NOT NULL,
	[ExecutiveSummary] [nvarchar](max) NULL,
	[SARStatus] [int] NOT NULL,
	[HighStatusId] [int] NULL,
	[PrimAMLRisk] [nvarchar](50) NULL,
	[PrimSUITRisk] [nvarchar](50) NULL,
	[HasRelatedCases] [bit] NULL,
	[RFITypeId] [int] NULL,
	[Priority] [int] NULL,
	[RiskNumber] [int] NULL,
	[HasNoDeriv] [int] NULL,
	[PreliminaryRiskNumber] [int] NULL,
	[PotentiallyCloseable] [nvarchar](10) NULL,
	[WholeOPPotentiallyCloseable] [nvarchar](10) NULL,
	[SPVHold] [nvarchar](10) NULL,
	[CMTFlag] [nvarchar](50) NULL,
	[Valid] [bit] NULL,
	[Duplicate] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL) 

GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KYC_BankData](
	[KPMGCustomerNumber] [nvarchar](50) NOT NULL,
	[CustomerNumber] [nvarchar](50) NOT NULL,
	[CustomerName] [nvarchar](255) NULL,
	[RelationshipManagerName] [nvarchar](50) NULL,
	[PrimaryPartyName] [nvarchar](max) NULL,
	[ClientType] [nvarchar](100) NULL,
	[NucleusId] [nvarchar](100) NULL,
	[OPAccounts] [nvarchar](100) NULL,
	[PRAccounts] [nvarchar](100) NULL,
	[AddressDesc] [nvarchar](max) NULL,
	[StreetAddress] [nvarchar](max) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[PostalCode] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[CountryCode] [nvarchar](100) NULL,
	[TaxID] [nvarchar](100) NULL,
	[GovtIdType] [nvarchar](100) NULL,
	[GovtIdNo] [nvarchar](100) NULL,
	[ConfPartyId] [nvarchar](100) NULL,
	[ConfPartyName] [nvarchar](100) NULL,
	[IsDuplicated?] [nvarchar](100) NULL,
	[RequestType] [nvarchar](100) NULL,
 CONSTRAINT [pk_KPMGCustomerNumber] PRIMARY KEY CLUSTERED 
(
	[KPMGCustomerNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


--Dummy Cases

SET IDENTITY_INSERT [dbo].[KYC_CaseFile] ON
GO
INSERT [dbo].[KYC_CaseFile] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CaseName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (4, N'CITI00004', N'1', N'CITI', N'Name1', 14, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), CAST(0x0000A38000000000 AS DateTime))
GO
INSERT [dbo].[KYC_CaseFile] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CaseName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (15, N'CITI00007', N'KYCCITI00007', N'CITI', N'Name2', 15, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), CAST(0x0000A38000000000 AS DateTime))
GO
INSERT [dbo].[KYC_CaseFile] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CaseName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (16, N'CITI00005', N'KYCCITI00005', N'CITI', N'Name3', 14, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), CAST(0x0000A38000000000 AS DateTime))
GO
INSERT [dbo].[KYC_CaseFile] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CaseName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (17, N'CITI00006', N'KYCCITI00006', N'CITI', N'Name4', 15, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), CAST(0x0000A38000000000 AS DateTime))
GO
INSERT [dbo].[KYC_CaseFile] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CaseName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (18, N'CITI00008', N'KYCCITI00008', N'CITI', N'Name5', 14, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A38000000000 AS DateTime), CAST(0x0000A38000000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[KYC_CaseFile] OFF
GO

SET IDENTITY_INSERT [dbo].[KYC_Accounts] ON 

GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (4, N'1', N'CITI00004', N'CITI', 1, N'High', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (15, N'2', N'RBS00004', N'RBS', 1, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (15, N'1', N'CITI00004', N'CITI', 1, N'High', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (4, N'2', N'RBS00004', N'RBS', 1, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (17, N'3', N'JPMC00004', N'JPMC', 2, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (17, N'2', N'RBS00004', N'RBS', 1, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (18, N'2', N'RBS00004', N'RBS', 1, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (18, N'3', N'JPMC00004', N'JPMC', 2, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (16, N'2', N'RBS00004', N'RBS', 1, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO																																																																																																																																																			  
INSERT [dbo].[KYC_Accounts] ([cf_ID], [CustomerNo], [CaseNo], [CustomerName], [CategoryId], [RiskProfile], [AnalystId], [Due_Diligence], [MemorandumOfFact], [TransactionCount], [TransactionScore], [AccountCount], [AccountScore], [CreateDate], [CreatedBy], [DemoteCount], [ExecutiveSummary], [SARStatus], [HighStatusId], [PrimAMLRisk], [PrimSUITRisk], [HasRelatedCases], [RFITypeId], [Priority], [RiskNumber], [HasNoDeriv], [PreliminaryRiskNumber], [PotentiallyCloseable], [WholeOPPotentiallyCloseable], [SPVHold], [CMTFlag], [Valid], [Duplicate], [CreatedDate], [LastModifiedDate]) VALUES (16, N'3', N'JPMC00004', N'JPMC', 2, N'Medium', 1, N'', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A2A600000000 AS DateTime))
GO

SET IDENTITY_INSERT [dbo].[KYC_Accounts] OFF
GO
INSERT [dbo].[KYC_BankData] ([KPMGCustomerNumber], [CustomerNumber], [CustomerName], [RelationshipManagerName], [PrimaryPartyName], [ClientType], [NucleusId], [OPAccounts], [PRAccounts], [AddressDesc], [StreetAddress], [City], [State], [PostalCode], [Country], [CountryCode], [TaxID], [GovtIdType], [GovtIdNo], [ConfPartyId], [ConfPartyName], [IsDuplicated?], [RequestType]) VALUES (N'CITI00004', N'CITI00004', N'Test Customer', N'Risk Consulting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[KYC_BankData] ([KPMGCustomerNumber], [CustomerNumber], [CustomerName], [RelationshipManagerName], [PrimaryPartyName], [ClientType], [NucleusId], [OPAccounts], [PRAccounts], [AddressDesc], [StreetAddress], [City], [State], [PostalCode], [Country], [CountryCode], [TaxID], [GovtIdType], [GovtIdNo], [ConfPartyId], [ConfPartyName], [IsDuplicated?], [RequestType]) VALUES (N'CITI00005', N'CITI00005', N'Test Customer', N'Risk Consulting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[KYC_BankData] ([KPMGCustomerNumber], [CustomerNumber], [CustomerName], [RelationshipManagerName], [PrimaryPartyName], [ClientType], [NucleusId], [OPAccounts], [PRAccounts], [AddressDesc], [StreetAddress], [City], [State], [PostalCode], [Country], [CountryCode], [TaxID], [GovtIdType], [GovtIdNo], [ConfPartyId], [ConfPartyName], [IsDuplicated?], [RequestType]) VALUES (N'CITI00006', N'CITI00006', N'Test Customer', N'Risk Consulting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[KYC_BankData] ([KPMGCustomerNumber], [CustomerNumber], [CustomerName], [RelationshipManagerName], [PrimaryPartyName], [ClientType], [NucleusId], [OPAccounts], [PRAccounts], [AddressDesc], [StreetAddress], [City], [State], [PostalCode], [Country], [CountryCode], [TaxID], [GovtIdType], [GovtIdNo], [ConfPartyId], [ConfPartyName], [IsDuplicated?], [RequestType]) VALUES (N'CITI00007', N'CITI00007', N'Test Customer', N'Risk Consulting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[KYC_BankData] ([KPMGCustomerNumber], [CustomerNumber], [CustomerName], [RelationshipManagerName], [PrimaryPartyName], [ClientType], [NucleusId], [OPAccounts], [PRAccounts], [AddressDesc], [StreetAddress], [City], [State], [PostalCode], [Country], [CountryCode], [TaxID], [GovtIdType], [GovtIdNo], [ConfPartyId], [ConfPartyName], [IsDuplicated?], [RequestType]) VALUES (N'CITI00008', N'CITI00008', N'Test Customer', N'Risk Consulting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO