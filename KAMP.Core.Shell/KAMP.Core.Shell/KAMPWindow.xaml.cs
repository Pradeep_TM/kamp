﻿using KAMP.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KAMP.Core.Repository.Enum;
using KAMP.Core.Interfaces;
using KAMP.Core.FrameworkComponents.Enums;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository;
using System.Diagnostics;
using KAMP.Core.Common.UserControls;
using System.Configuration;

namespace KAMP.Shell
{
    /// <summary>
    /// Interaction logic for KAMP.xaml
    /// </summary>
    public partial class KAMPWindow : Window
    {
        public KAMPWindow()
        {
            InitializeComponent();
            UCHeader.CloseApp += CloseApplication;
            Loaded += KAMPWindow_Loaded;
            Application.Current.MainWindow = this;
        }

        void KAMPWindow_Loaded(object sender, RoutedEventArgs evnt)
        {
            AddModulesToDb(ModuleList.ModuleContainer);
            foreach (IModule module in ModuleList.ModuleContainer)
            {
                ModuleNames moduleNames;
                bool isParsed = Enum.TryParse(module.Metadata.ModuleName, true, out moduleNames);
                if (!isParsed) continue;
                switch (moduleNames)
                {
                    //case ModuleNames.UM:
                    //    GridUM.Visibility = Visibility.Visible;
                    //    GridUM.PreviewMouseDown += (s, e) =>
                    //        {
                    //            ShowModuleButton(s, e, module);
                    //        };
                    //    break;
                    case ModuleNames.KYC:
                        // GridKYC.Visibility = Visibility.Visible;
                        GridKYC.PreviewMouseDown += (s, e) =>
                        {
                            ShowModuleButton(s, e, module);
                        };
                        break;
                    case ModuleNames.MV:
                        // GridMV.Visibility = Visibility.Visible;
                        GridMV.PreviewMouseDown += (s, e) =>
                        {
                            ShowModuleButton(s, e, module);
                        };
                        break;
                    case ModuleNames.TLB:
                        //GridTLB.Visibility = Visibility.Visible;
                        GridTLB.PreviewMouseDown += (s, e) =>
                        {
                            ShowModuleButton(s, e, module);
                        };
                        break;

                }
                //if (Convert.ToBoolean(ConfigurationManager.AppSettings["TurnOnConfiguration"]))
                //{
                //    LoadConfigurationScreen(Window.GetWindow(this));
                //}
            }
        }

        private void CloseApplication()
        {
            App.Current.Shutdown();
        }

        private void ShowModuleButton(object sender, MouseButtonEventArgs e, IModule module)
        {
            FillModuleContext(module.Metadata.ModuleName);
            var moduleWindow = module.Metadata.HomeWindow as Window;
            Application.Current.MainWindow = moduleWindow;
            if (moduleWindow != null) moduleWindow.Show();
            this.Hide();
        }

        private void FillModuleContext(string modulename)
        {
            var context = Application.Current.Properties["Context"] as Context;
            context.Module = context.GetModuleDetails(modulename);
        }

        private void AddModulesToDb(List<IModule> moduleList)
        {
            var dbContext = new KAMPContext();
            var modulesInDb = dbContext.mstr_Module.Where(x => x.IsActive).ToList();
            if (!modulesInDb.IsCollectionValid())
            {
                int count = 0;
                foreach (var module in moduleList)
                {
                    var mstrModule = new Module()
                    {
                        ModuleTypeId = count + 1,
                        IsActive = true,
                        ModuleName = module.Metadata.ModuleName
                    };
                    count = count + 1;
                    dbContext.mstr_Module.Add(mstrModule);
                }
            }
            else
            {
                if (moduleList.Count == modulesInDb.Count)
                    return;
                if (moduleList.Count > modulesInDb.Count)
                {
                    moduleList.ForEach(x =>
                    {
                        var moduleMatch = modulesInDb.FirstOrDefault(y => y.ModuleName == x.Metadata.ModuleName);
                        if (moduleMatch.IsNull())
                        {
                            var moduleFrmDb = dbContext.mstr_Module.FirstOrDefault(z => z.ModuleName == x.Metadata.ModuleName);
                            if (moduleFrmDb.IsNull())
                            {
                                var mstrModule = new Module()
                                {
                                    ModuleTypeId = modulesInDb.Count + 1,
                                    IsActive = true,
                                    ModuleName = x.Metadata.ModuleName
                                };
                                dbContext.mstr_Module.Add(mstrModule);

                            }
                            else
                            {
                                moduleFrmDb.IsActive = true;
                                dbContext.mstr_Module.Attach(moduleFrmDb);
                                dbContext.Entry(moduleFrmDb).State = System.Data.Entity.EntityState.Modified;
                            }

                        }
                    });
                }
                else
                {
                    foreach (var moduleInDb in modulesInDb)
                    {
                        var moduleExists = moduleList.FirstOrDefault(x => x.Metadata.ModuleName == moduleInDb.ModuleName);
                        if (moduleExists.IsNull())
                        {
                            moduleInDb.IsActive = false;
                            dbContext.mstr_Module.Attach(moduleInDb);
                            dbContext.Entry(moduleInDb).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                }
            }
            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                var res = MessageBox.Show("Please contact your administrator.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                App.Current.Shutdown();

            }
        }

        //public static void LoadConfigurationScreen(Window wnd)
        //{
        //    Grid _container = null;
        //    ApplicationConfiguration _control = null;

        //    _container = wnd.FindName("GdContainer") as Grid;
        //    if (_container != null)
        //    {
        //        var childControl = FindChildControl<ApplicationConfiguration>(_container);
        //        if (childControl == null)
        //        {
        //            _control = new ApplicationConfiguration();
        //            _container.Children.Add(_control);
        //            Application.Current.Dispatcher.Invoke(() =>
        //            {
        //                _control.UpdateLayout();
        //            });
        //        }
        //    }
        //}

        private static DependencyObject FindChildControl<T>(DependencyObject control)
        {
            int childNumber = VisualTreeHelper.GetChildrenCount(control);
            for (int i = 0; i < childNumber; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(control, i);
                if (child != null && child is T)
                    return child;
                else
                    FindChildControl<T>(child);
            }
            return null;
        }
    }
}

