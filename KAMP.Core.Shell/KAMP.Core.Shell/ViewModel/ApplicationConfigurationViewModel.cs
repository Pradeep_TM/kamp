﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using KAMP.Core.FrameworkComponents;
using System.Windows.Media;
using System.Windows.Input;
using KAMP.Core.Common.Common;
using System.Windows;
using System.Windows.Threading;
using System.Data.SqlClient;
using KAMP.Core.Common.BLL;
using System.Windows.Controls;
using KAMP.Core.Common.UserControls;
using System.Threading;
using Microsoft.Win32;
using System.Drawing.Imaging;
using AutoMapper;
using System.Configuration;
using System.IO;

namespace KAMP.Shell.ViewModel
{
    public class ApplicationConfigurationViewModel : INotifyPropertyChanged
    {
        public byte[] Logo { get; set; }
        public BitmapImage bitmapImage { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangedEventHandler LostFocus;

        public ApplicationConfigurationViewModel()
        {
            PropertyChanged += OnConfigurationDetailsChanged;
            LblContent = null;
            IsErrorOccured = false;
            IsNewClient = false;
        }

        public bool IsClientNameNotNull;
        public bool IsClientCodeNotNull;
        public bool IsEngagementNameNotNull;
        public bool IsEngagementNoNotNull;
        public bool IsZipCodeNotNull;
        public bool IsImagePathNotNull;
        public Action<string> GetDatabaseName;
        private string _lblcontent;
        public string LblContent
        {
            get { return _lblcontent; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lblcontent, x => x.LblContent);
            }
        }

        private bool _errorOccured;
        public bool IsErrorOccured
        {
            get { return _errorOccured; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _errorOccured, x => x.IsErrorOccured);

            }
        }

        private string _clientCode;
        public string ClientCode
        {
            get { return _clientCode; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _clientCode, x => x.ClientCode);
            }
        }

        private bool _isNewClient;
        public bool IsNewClient
        {
            get { return _isNewClient; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isNewClient, x => x.IsNewClient);

            }
        }

        private bool _isValid;
        public bool IsValid
        {
            get
            {
                _isValid = IsClientNameNotNull && IsClientCodeNotNull && IsEngagementNameNotNull && IsEngagementNoNotNull && IsImagePathNotNull;

                return _isValid;

            }
            set { _isValid = value; }
        }


        private string _clientName;
        public string ClientName
        {
            get { return _clientName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _clientName, x => x.ClientName);
                OnClientSelection(ClientName);
            }
        }

        private List<string> _availableClientNames;
        public List<string> AvailableClientNames
        {
            get { return _availableClientNames; }
            set { PropertyChanged.HandleValueChange(this, value, ref _availableClientNames, x => x.AvailableClientNames); }
        }

        private List<string> _availableEngagementNames;
        public List<string> AvailableEngagementNames
        {
            get { return _availableEngagementNames; }
            set { PropertyChanged.HandleValueChange(this, value, ref _availableEngagementNames, x => x.AvailableEngagementNames); }
        }

        private List<string> _availableEngagementNo;
        public List<string> AvailableEngagementNo
        {
            get { return _availableEngagementNo; }
            set { PropertyChanged.HandleValueChange(this, value, ref _availableEngagementNo, x => x.AvailableEngagementNo); }
        }

        private List<string> _availableDbNames;
        public List<string> AvailableDbNames
        {
            get { return _availableDbNames; }
            set { PropertyChanged.HandleValueChange(this, value, ref _availableDbNames, x => x.AvailableDbNames); }
        }
        private string _serverName;
        public string ServerName
        {
            get { return _serverName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _serverName, x => x.ServerName);
                
                if (GetDatabaseName.IsNotNull())
                    GetDatabaseName(_serverName);
            }
        }

        private string _dbName;
        public string DBName
        {
            get { return _dbName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _dbName, x => x.DBName);
            }
        }


        private string _engagNementame;
        public string EngagementName
        {
            get { return _engagNementame; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _engagNementame, x => x.EngagementName);

            }
        }

        private string _engagementNo;
        public string EngagementNumber
        {
            get { return _engagementNo; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _engagementNo, x => x.EngagementNumber);

            }
        }

        private string _country;
        public string Country
        {
            get { return _country; }
            set { PropertyChanged.HandleValueChange(this, value, ref _country, x => x.Country); }
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isActive, x => x.IsActive); }
        }

        private string _imagePath;
        public string ImagePath
        {
            get { return _imagePath; }
            set { PropertyChanged.HandleValueChange(this, value, ref _imagePath, x => x.ImagePath); }
        }

        private ImageSource _logosrc;
        public ImageSource Logosrc
        {
            get { return _logosrc; }
            set { PropertyChanged.HandleValueChange(this, value, ref _logosrc, x => x.Logosrc); }
        }

        private ICommand _save;
        public ICommand Save
        {
            get
            {
                if (_save == null)
                {
                    _save = new DelegateCommand(() =>
                    {
                        try
                        {
                            if (!_isValid)
                            {
                                LblContent = "Please fill all required values.";
                                IsErrorOccured = true;
                                return;
                            }

                            //RegExUtility validation = new RegExUtility();
                            //if (!validation.IsUSAZipCode(_zipCode))
                            //{
                            //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
                            //    {
                            //        LblContent = "The entered zipcode is not valid.";
                            //        IsErrorOccured = true;
                            //        ZipCode = null;
                            //    }));
                            //}

                            var bl = new CommonBL();
                            SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
                            Properties.Settings.Default.ServerName = ServerName.Trim().ToString();
                            Properties.Settings.Default.DBName = DBName.Trim().ToString();
                            myBuilder.DataSource = ServerName.Trim();
                            myBuilder.InitialCatalog = DBName.Trim();
                            myBuilder.IntegratedSecurity = true;
                            string connectionString = myBuilder.ConnectionString;
                            //ConfigurationManager.ConnectionStrings["KAMPContext"].ConnectionString = connectionString;

                            Mapper.CreateMap<ApplicationConfigurationViewModel, Core.Common.ViewModels.ApplicationConfiguration>();
                            var applicationConfiguration = Mapper.Map<Core.Common.ViewModels.ApplicationConfiguration>(this);
                            var result = bl.SaveClientDetails(applicationConfiguration, connectionString);
                            if (result)
                            {
                                RunDbScript(connectionString);
                                var appcontext = Application.Current.Properties["Context"] as Context;
                                appcontext.ConnectionString = connectionString;
                                var window = new KAMPWindow();
                                window.Show();
                                Application.Current.MainWindow.Close();
                                Application.Current.MainWindow = window;
                                //KAMP.Shell.App.OnStartup();
                            }
                        }
                        catch (BusinessException ex)
                        {
                            LblContent = ex.Message.ToString();
                            IsErrorOccured = true;
                            ClearAll();

                        }
                        catch (Exception ex)
                        {
                            LblContent = "A problem occurred during serializing data to the database.";
                            IsErrorOccured = true;
                            ClearAll();
                        }

                    });
                }
                return _save;
            }
        }

        private void RunDbScript(string connectionString)
        {
            string script = File.ReadAllText(@"DbScript/SeedData.sql");
            SqlConnection conn = new SqlConnection(connectionString);
            Server server = new Server(new ServerConnection(conn));
            server.ConnectionContext.ExecuteNonQuery(script);
        }

        //private static void RemoveCurrentWindow()
        //{
        //    Grid _container = null;
        //    foreach (Window window in System.Windows.Application.Current.Windows)
        //    {
        //        if (window.Title.Equals("KAMP"))
        //        {
        //            _container = window.FindName("GdContainer") as Grid;
        //            if (_container != null)
        //            {
        //                var childControl = FindChildControl<ApplicationConfiguration>(_container);
        //                if (childControl != null)
        //                {
        //                    _container.Children.RemoveRange(1, 1);
        //                }
        //            }
        //        }
        //    }
        //}

        private ICommand _cancel;
        public ICommand Cancel
        {
            get
            {
                if (_cancel == null)
                {
                    _cancel = new DelegateCommand(() =>
                    {
                        ClearAll();
                        Application.Current.Shutdown(0);

                    });
                }
                return _cancel;
            }

        }

        private ICommand _upload;
        public ICommand Upload
        {
            get
            {
                if (_upload == null)
                {
                    _upload = new DelegateCommand(() =>
                    {
                        var tempPath = System.IO.Path.GetTempPath().Replace(@"\", "/");
                        Logosrc = bitmapImage = null;
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send, new ThreadStart(() =>
                        {
                            ImageExtensions.DeleteImage(System.IO.Path.Combine(tempPath, Constants.LOGO_NAME));
                        }));

                        OpenDialog();

                    });
                }
                return _upload;
            }

        }

        private void OnConfigurationDetailsChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == this.GetPropertyName(model => model.ClientName))
            {
                IsClientNameNotNull = !string.IsNullOrEmpty(ClientName);
                if (string.IsNullOrEmpty(ClientName))
                {
                    EngagementNumber = string.Empty;
                    EngagementName = string.Empty;
                    Logosrc = null;
                    //ZipCode = string.Empty;
                    var bl = new CommonBL();
                    AvailableEngagementNames = null;
                    AvailableEngagementNo = null;
                    if (!string.IsNullOrEmpty(ClientCode))
                        ClientCode = string.Empty;
                }
            }
            if (e.PropertyName == this.GetPropertyName(model => model.ClientCode))
            {
                IsClientCodeNotNull = !string.IsNullOrEmpty(ClientCode);
                if (string.IsNullOrEmpty(ClientCode))
                {
                    if (!string.IsNullOrEmpty(ClientName))
                        ClientName = string.Empty;
                }
            }
            if (e.PropertyName == this.GetPropertyName(model => model.EngagementName))
            {
                IsEngagementNameNotNull = !string.IsNullOrEmpty(EngagementName);
                if (string.IsNullOrEmpty(EngagementName))
                {
                    EngagementNumber = string.Empty;
                }
            }
            if (e.PropertyName == this.GetPropertyName(model => model.EngagementNumber))
            {
                IsEngagementNoNotNull = !string.IsNullOrEmpty(EngagementNumber);
                if (string.IsNullOrEmpty(EngagementNumber))
                {
                    EngagementName = string.Empty;
                }
            }
            //if (e.PropertyName == this.GetPropertyName(model => model.ZipCode))
            //{
            //    IsZipCodeNotNull = !string.IsNullOrEmpty(ZipCode);
            //    if (!string.IsNullOrEmpty(ZipCode))
            //    {
            //        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
            //        {
            //            LblContent = string.Empty;
            //            IsErrorOccured = false;
            //        }));

            //    }
            //}
            if (e.PropertyName == this.GetPropertyName(model => model.Logosrc))
            {
                IsImagePathNotNull = Logosrc != null;
            }

            IsValid = IsClientNameNotNull && IsClientCodeNotNull && IsEngagementNameNotNull && IsEngagementNoNotNull && IsImagePathNotNull;
        }

        private void OpenDialog()
        {
            OpenFileDialog openFileDialog = null;

            openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open Image";
            openFileDialog.Filter = "Image Files (*.bmp, *.jpg,*.png,*.jpeg)|*.bmp;*.jpg;*.png;*.jpeg";

            if (openFileDialog.ShowDialog() == true)
            {
                var tempImagePath = ImageExtensions.SaveImage(openFileDialog.FileName, System.IO.Path.GetTempPath());
                var trasnformedImage = ImageExtensions.ScaleImage(90, 90, tempImagePath);
                Logo = ImageExtensions.ToByteArray(trasnformedImage, ImageFormat.Jpeg);
                if (Logo.IsNotNull())
                    Logosrc = bitmapImage = ImageExtensions.ToImage(Logo);

            }

        }

        private void OnClientSelection(string clientName)
        {
            if (!string.IsNullOrEmpty(clientName))
            {
                var bl = new CommonBL();
                var val = bl.GetDetailsUponClient(clientName);

                if (val.IsNull())
                {
                    IsNewClient = true;
                    AvailableEngagementNames = null;
                    AvailableEngagementNo = null;
                }
                else
                {
                    IsNewClient = false;
                    ClientCode = val.ClientCode;
                    AvailableEngagementNames = val.ClientEngagement.Select(e => e.EngagementName).ToList();
                    AvailableEngagementNo = val.ClientEngagement.Select(e => e.EngagementNumber).ToList();
                    Logosrc = ImageExtensions.ToImage(val.Logo);
                    Logo = val.Logo;
                    //ZipCode = val.ZipCode;
                }
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
                {
                    LblContent = string.Empty;
                    IsErrorOccured = false;
                }));

            }
        }

        private static DependencyObject FindChildControl<T>(DependencyObject control)
        {
            int childNumber = VisualTreeHelper.GetChildrenCount(control);
            for (int i = 0; i < childNumber; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(control, i);
                if (child != null && child is T)
                    return child;
                else
                    FindChildControl<T>(child);
            }
            return null;
        }

        public void ClearAll()
        {
            ClientName = null;
            ClientCode = null;
            EngagementName = null;
            EngagementNumber = null;
            //ZipCode = null;
            Logosrc = null;
            Logo = null;
        }
    }
}
