﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;

namespace KAMP.Shell
{
    using Core.Interface;
    using Core.FrameworkComponents;
    using System.Security.Principal;
    using KAMP.Core.UserManagement.AppCode;
    using KAMP.Core.Repository.UnitOfWork;
    using System.Threading;
    using KAMP.Core.Interfaces;
    using KAMP.Core.Repository.UM;
    using KAMP.Core.Common.BLL;
    using System.Windows;
    using KAMP.Core.Common.Models;
    using KAMP.Core.Repository;
    using System.Data.SqlClient;
    using System.Configuration;

    public class ShellEntry
    {
        [ImportMany]
        public IEnumerable<IModule> Modules { get; set; }


        //[STAThreadAttribute]
        //public static void Main()
        //{
        //    var currentUser = new ShellEntry().InitializeAuthorization();
        //    var userExist = new ShellEntry().CheckUserExist(currentUser);
        //    if (userExist)
        //    {
        //    new ShellEntry().Run();
        //    App.Main();
        //    }
        //    else
        //    {
        //        App.Current.Shutdown();
        //    }

        //}

        public void Run()
        {            
            var directoryCatalog = new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory);
            var assemblyCataloy = new AssemblyCatalog(this.GetType().Assembly);
            var catalog = new AggregateCatalog(assemblyCataloy, directoryCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);

            //var bootStrappers = typeof(IBootStrapper).GetImplementors();

            //if (bootStrappers.IsCollectionValid())
            //{
            //    bootStrappers.ForEach(x => ((IBootStrapper)Activator.CreateInstance(x)).Configure());
            //}

            var mappings = container.GetExportedValues<IBootStrapper>();

            foreach (IBootStrapper mc in mappings)
            {
                mc.Configure();
            }

            //ModuleList.ModuleContainer.AddRange(this.Modules);

            foreach (var module in this.Modules)
            {
                module.Initialize();
                ModuleList.ModuleContainer.Add(module);
            }

            AddModulesToDb(ModuleList.ModuleContainer);
        }

        public string InitializeAuthorization()
        {
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            var KampIdentity = WindowsIdentity.GetCurrent();

            var genericPrinciple = new KAMPPrincipal(KampIdentity);

            Thread.CurrentPrincipal = genericPrinciple;
            AppDomain.CurrentDomain.SetThreadPrincipal(genericPrinciple);

            //Set Context
            var context = new Context();
            context.User.UserName = genericPrinciple.Identity.Name;
            context.User.Id = genericPrinciple.UserId;
            if (genericPrinciple.UserId != 0)
            {
                var commonBll = new CommonBL();
                var user = commonBll.GetUser(genericPrinciple.UserId);
                context.User.FirstName = user.FirstName;
                context.User.LastName = user.LastName;
                context.GetModuleDetails = (moduleName) => { return new CommonBL().GetModuleDetails(moduleName); };
                Application.Current.Properties["Context"] = context;
                //TODO:Insert into login history table.
                return KampIdentity.Name;
            }
            return null;
        }

        public bool CheckUserExist(string currentUser)
        {
            string[] domainUser = currentUser.Split('\\');
            var domain = domainUser[0];
            var user = domainUser[1];
            using (var dbContext = new UMUnitOfWork())
            {
                var userDtl = dbContext.GetRepository<User>().Items.FirstOrDefault(x => x.UserName == user && x.IsActive);
                if (userDtl != null)
                    return true;
                return false;
            }


        }

        private void AddModulesToDb(List<IModule> moduleList)
        {
            var dbContext = new KAMPContext();
            var modulesInDb = dbContext.mstr_Module.Where(x => x.IsActive).ToList();
            if (!modulesInDb.IsCollectionValid())
            {
                int count = 0;
                foreach (var module in moduleList)
                {
                    var mstrModule = new Module()
                    {
                        ModuleTypeId = count + 1,
                        IsActive = true,
                        ModuleName = module.Metadata.ModuleName
                    };
                    count = count + 1;
                    dbContext.mstr_Module.Add(mstrModule);
                }
            }
            else
            {
                if (moduleList.Count == modulesInDb.Count)
                    return;
                if (moduleList.Count > modulesInDb.Count)
                {
                    moduleList.ForEach(x =>
                    {
                        var moduleMatch = modulesInDb.FirstOrDefault(y => y.ModuleName == x.Metadata.ModuleName);
                        if (moduleMatch.IsNull())
                        {
                            var moduleFrmDb = dbContext.mstr_Module.FirstOrDefault(z => z.ModuleName == x.Metadata.ModuleName);
                            if (moduleFrmDb.IsNull())
                            {
                                var mstrModule = new Module()
                                {
                                    ModuleTypeId = modulesInDb.Count + 1,
                                    IsActive = true,
                                    ModuleName = x.Metadata.ModuleName
                                };
                                dbContext.mstr_Module.Add(mstrModule);

                            }
                            else
                            {
                                moduleFrmDb.IsActive = true;
                                dbContext.mstr_Module.Attach(moduleFrmDb);
                                dbContext.Entry(moduleFrmDb).State = System.Data.Entity.EntityState.Modified;
                            }

                        }
                    });
                }
                else
                {
                    foreach (var moduleInDb in modulesInDb)
                    {
                        var moduleExists = moduleList.FirstOrDefault(x => x.Metadata.ModuleName == moduleInDb.ModuleName);
                        if (moduleExists.IsNull())
                        {
                            moduleInDb.IsActive = false;
                            dbContext.mstr_Module.Attach(moduleInDb);
                            dbContext.Entry(moduleInDb).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                }
            }
            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                var res = MessageBox.Show("Please contact your administrator.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                App.Current.Shutdown();

            }
        }

        /*
        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var executingAssembly = Assembly.GetExecutingAssembly();
            var assemblyName = new AssemblyName(args.Name);
            var ModuleName = assemblyName.Name + ".dll";
            var resources = executingAssembly.GetManifestResourceNames().Where(a => a.EndsWith(ModuleName));
            if(resources.Any())
            {
                var resourceName = resources.First();
                using(var stream = executingAssembly.GetManifestResourceStream(resourceName))
                {
                    if (stream == null) return null;
                    var block = new byte[stream.Length];
                    try
                    {
                        stream.Read(block, 0, block.Length);
                        return Assembly.Load(block);

                    }
                    catch(IOException)
                    {
                        return null;
                    }
                    catch(BadImageFormatException)
                    {
                        return null;
                    }

                }
            }
            return null;
        }*/
    }
}
