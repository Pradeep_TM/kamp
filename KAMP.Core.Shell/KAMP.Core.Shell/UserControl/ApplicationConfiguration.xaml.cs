﻿using KAMP.Core.Common.BLL;
using KAMP.Core.Common.Common;
using KAMP.Shell.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Shell.UserControl
{
    using KAMP.Core.Common.Common;
    /// <summary>
    /// Interaction logic for ApplicationConfiguration.xaml
    /// </summary>
    public partial class ApplicationConfiguration 
    {
        public ApplicationConfigurationViewModel ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = value; }
        }
        private ApplicationConfigurationViewModel _viewModel =null;

        public ApplicationConfiguration()
        {
            IntializedViewModel();
            InitializeComponent();
            this.DataContext = ViewModel;
        }

        private void IntializedViewModel()
        {
            if (_viewModel == null)
                _viewModel = new ApplicationConfigurationViewModel();


            var bl = new CommonBL();
            _viewModel.Country = Constants.COUNTRY;
            _viewModel.IsActive = true;
            _viewModel.AvailableClientNames = bl.GetAllClientNames();
            _viewModel.AvailableEngagementNames = bl.GetAllEngagementNames();
            _viewModel.AvailableEngagementNo = bl.GetAllEngagementNo();

        }

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            _viewModel.ClearAll();
            Application.Current.Shutdown(0);
        }
    }    
}
