﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace KAMP.Shell
{
    using Core.Interface;
    using Core.FrameworkComponents;
    using System.Security.Principal;
    using KAMP.Core.UserManagement.AppCode;
    using System.Threading;
    using KAMP.Core.UM.UserControls;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        [ImportMany]
        public IEnumerable<IModule> Modules { get; set; }
        static App app;
        static Mutex mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");
        public static void OnStartup()
        {
            var shellEntry = new ShellEntry();
            var currentUser = shellEntry.InitializeAuthorization();
            if (currentUser.IsNull())
            {
                MessageBoxControl.Show("You do not have permission to access this app. Please contact your system administrator.", MessageBoxButton.OK, MessageType.Error);
                app.Shutdown();
            }
            else
            {
                var userExist = shellEntry.CheckUserExist(currentUser);
                if (userExist)
                {
                    shellEntry.Run();
                }
                else
                {
                    app.Shutdown();
                }
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            MessageBox.Show(ex.Message, "Uncaught Thread Exception",
                            MessageBoxButton.OK, MessageBoxImage.Error);
        }


        [STAThread]
        static void Main()
        {
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                app = new App();
                var shellEntry = new ShellEntry();
                var currentUser = shellEntry.InitializeAuthorization();
                //  app.Startup += OnStartup;
                app.DispatcherUnhandledException += Dispatcher_UnhandledException;
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                shellEntry.Run();
                app.Run(new KAMPWindow());
                mutex.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("Another instance of the application is already running.");
            }
        }

        static void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Unhandled Dispatcher Exception",
                                   MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
