﻿using KAMP.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;

namespace KAMP.Core.Common.ViewModels
{
   public class RFIViewScreenViewModel: INotifyPropertyChanged
    {
       public RFIViewScreenViewModel()
        {
            _rFIViewList = new ObservableCollection<RFIUploadModel>();
        }
        private ObservableCollection<RFIUploadModel> _rFIViewList;
        public ObservableCollection<RFIUploadModel> RFIVeiwList
        {
            get { return _rFIViewList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rFIViewList, (x) => x.RFIVeiwList); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
