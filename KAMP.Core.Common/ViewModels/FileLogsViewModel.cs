﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.KYC;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace KAMP.Core.Common.ViewModels
{
    public class FileLogsViewModel : INotifyPropertyChanged
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        private string _fileName;

        public string FileName
        {
            get { return _fileName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fileName, (x) => x.FileName); }
        }
        private string _documentName;

        public string DocumentName
        {
            get { return _documentName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _documentName, (x) => x.DocumentName); }
        }

        private string _partyTypeName;

        public string PartyTypeName
        {
            get { return _partyTypeName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _partyTypeName, (x) => x.PartyTypeName); }
        }

        private DateTime _dateModified;

        public DateTime DateModified
        {
            get { return _dateModified; }
            set { PropertyChanged.HandleValueChange(this, value, ref _dateModified, (x) => x.DateModified); }
        }

        private int _moduleId;

        public int ModuleId
        {
            get { return _moduleId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _moduleId, (x) => x.ModuleId); }
        }

        private int _cfid;

        public int CfId
        {
            get { return _cfid; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfid, (x) => x.CfId); }
        }

        private ObservableCollection<PartyType> _partyTypes;

        public ObservableCollection<PartyType> PartyTypes
        {
            get { return _partyTypes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _partyTypes, (x) => x.PartyTypes); }
        }

        private PartyType _selectedPartyTypes;

        public PartyType SelectedPartyType
        {
            get { return _selectedPartyTypes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedPartyTypes, (x) => x.SelectedPartyType); }
        }

        private DocumentCode _selectedDocumentCode;

        public DocumentCode SelectedDocumentCode
        {
            get { return _selectedDocumentCode; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedDocumentCode, (x) => x.SelectedDocumentCode); }
        }

        private int _partyTypeId;

        public int PartyTypeId
        {
            get { return _partyTypeId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _partyTypeId, (x) => x.PartyTypeId);
            }
        }

        private byte[] _content;

        public byte[] Content
        {
            get { return _content; }
            set { PropertyChanged.HandleValueChange(this, value, ref _content, (x) => x.Content); }
        }

        private string _fileType;

        public string FileType
        {
            get { return _fileType; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fileType, (x) => x.FileType); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { PropertyChanged.HandleValueChange(this, value, ref _name, (x) => x.Name); }
        }

        private int _contentLength;

        public int ContentLength
        {
            get { return _contentLength; }
            set { PropertyChanged.HandleValueChange(this, value, ref _contentLength, (x) => x.ContentLength); }
        }

        public string FullName { get { return Name + FileType; } }

        private string _fileExtension;

        public string FileExtension
        {
            get { return _fileExtension; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fileExtension, (x) => x.FileExtension); }
        }
        private ObservableCollection<DocumentCode> _documentCode;

        public ObservableCollection<DocumentCode> DocumentCode
        {
            get { return _documentCode; }
            set { PropertyChanged.HandleValueChange(this, value, ref _documentCode, (x) => x.DocumentCode); }
        }

        private int _documentCodeId;

        public int DocumentCodeId
        {
            get { return _documentCodeId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _documentCodeId, (x) => x.DocumentCodeId);
            }
        }

        //public bool AssignedUser
        //{ get; set; }

        public Func<bool> AssignedUser;

        private DateTime _createdDate;

        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _createdDate, (x) => x.CreatedDate); }
        }

        private int _createdBy;

        public int CreatedBy
        {
            get { return _createdBy; }
            set { PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Action OnSubmitClick;

        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }

    }
}
