﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
using System.ComponentModel;

namespace KAMP.Core.Common.ViewModels
{
    public class RFISendViewModel : INotifyPropertyChanged
    {
        private string _to;
        public string To
        {
            get { return _to; }
            set { PropertyChanged.HandleValueChange(this, value, ref _to, (x) => x.To); }
        }

        private string _cC;
        public string CC
        {
            get { return _cC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cC, (x) => x.CC); }
        }

        private string _subject;
        public string Subject
        {
            get { return _subject; }
            set { PropertyChanged.HandleValueChange(this, value, ref _subject, (x) => x.Subject); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
