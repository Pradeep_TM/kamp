﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using Microsoft.Office.Interop.Outlook;

namespace KAMP.Core.Common.ViewModels
{
    class RFIDescriptionViewModel : INotifyPropertyChanged
    {
        private RFIColorModel _rFIColorModel;
        public RFIColorModel RFIColorModel
        {
            get { return _rFIColorModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rFIColorModel, (x) => x.RFIColorModel); }
        }

        private MailItem _mailItem;
        public MailItem MailItem
        {
            get { return _mailItem; }
            set { PropertyChanged.HandleValueChange(this, value, ref _mailItem, (x) => x.MailItem); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
