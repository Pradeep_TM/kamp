﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using Microsoft.Office.Interop.Outlook;

namespace KAMP.Core.Common.ViewModels
{
    public class RFIViewModel : INotifyPropertyChanged
    {

        public RFIViewModel()
        {
            RFISendViewModel = new RFISendViewModel();
            RFIUploadViewModel = new RFIUploadViewModel();
            RFIViewScreenViewModel = new RFIViewScreenViewModel();
        }
        private int _moduleId;
        public int ModuleId
        {
            get { return _moduleId; }

            set { PropertyChanged.HandleValueChange(this, value, ref _moduleId, (x) => x.ModuleId); }
        }

        private string _senderName;
        public string SenderName
        {
            get { return _senderName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _senderName, (x) => x.SenderName); }
        }

        private string _caseNumber;
        public string CaseNumber
        {
            get { return _caseNumber; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseNumber, (x) => x.CaseNumber); }
        }

        private RFIColorModel _rFIColorModel;
        public RFIColorModel RFIColorModel
        {
            get { return _rFIColorModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rFIColorModel, (x) => x.RFIColorModel); }
        }

        private RFISendViewModel _rFISendViewModel;
        public RFISendViewModel RFISendViewModel
        {
            get { return _rFISendViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rFISendViewModel, (x) => x.RFISendViewModel); }
        }

        private RFIUploadViewModel _rFIUploadViewModel;
        public RFIUploadViewModel RFIUploadViewModel
        {
            get { return _rFIUploadViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rFIUploadViewModel, (x) => x.RFIUploadViewModel); }
        }

        private RFIViewScreenViewModel _rFIViewScreenViewModel;
        public RFIViewScreenViewModel RFIViewScreenViewModel
        {
            get { return _rFIViewScreenViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rFIViewScreenViewModel, (x) => x.RFIViewScreenViewModel); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
