﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
using KAMP.Core.Common.Models;
using System.Collections.ObjectModel;

namespace KAMP.Core.Common.ViewModels
{
    public class RFIUploadViewModel : INotifyPropertyChanged
    {
        public RFIUploadViewModel()
        {
            RFIUploadList = new ObservableCollection<RFIUploadModel>();
        }
        private ObservableCollection<RFIUploadModel> _rFIUploadList;
        public ObservableCollection<RFIUploadModel> RFIUploadList
        {
            get { return _rFIUploadList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rFIUploadList, (x) => x.RFIUploadList); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
