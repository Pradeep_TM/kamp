﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.Core.Common.ViewModels
{
    public class RFIColorModel
        : INotifyPropertyChanged
    {
        private string _darkColor;
        public string DarkColor
        {
            get { return _darkColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _darkColor, (x) => x.DarkColor); }
        }

        private string _backGroundColor;
        public string BackGroundColor
        {
            get { return _backGroundColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _backGroundColor, (x) => x.BackGroundColor); }
        }

        private string _lightestColor;
        public string LightestColor
        {
            get { return _lightestColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lightestColor, (x) => x.LightestColor); }
        }

        private string _innerBackgroundColor;
        public string InnerBackgroundColor
        {
            get { return _innerBackgroundColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _innerBackgroundColor, (x) => x.InnerBackgroundColor); }
        }

        
        private string _labelColor;
        public string LabelColor
        {
            get { return _labelColor; }
           set { PropertyChanged.HandleValueChange(this, value, ref _labelColor, (x) => x.LabelColor); }
        }

        private string _attachButtonColor;
        public string AttachButtonColor
        {
            get { return _attachButtonColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _attachButtonColor, (x) => x.AttachButtonColor); }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
