﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Windows.Input;
using System.Data;
using KAMP.Core.Common.Models;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Microsoft.Win32;
using System.IO;
using System.Data.OleDb;
using System.Globalization;
using System.Text.RegularExpressions;
using KAMP.Core.Common.UserControls;

namespace KAMP.Core.Common.ViewModels
{
    public class DataManagementViewModel : INotifyPropertyChanged, IDataErrorInfo
    {

        public DataManagementViewModel()
        {
            PropertyChanged += new PropertyChangedEventHandler(ImportDataViewModel_PropertyChanged);
            var importModes = EnumExtensions.ToList<MasterEntity>(typeof(KAMP.Core.Repository.Enum.Enums.DataImportMode), (x, code) => x.Code = code, (x, value) => x.Name = value);
            importModes.Insert(0, new MasterEntity { Code = 0, Name = "Select" });
            _dataImportModes = new ReadOnlyCollection<MasterEntity>(importModes);
            _dataImportMode = 0;
            _columnDefinitions = new ObservableCollection<ColumnDefinition>();
            ColumnDefinitions.CollectionChanged += new NotifyCollectionChangedEventHandler(ColumnDefinitions_CollectionChanged);

            this.validProperties = new Dictionary<string, bool>();
            this.validProperties.Add("TableName", true);
            this.validProperties.Add("ServerName", true);
        }
        void ColumnDefinitions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems.IsNotNull())
            {
                foreach (ColumnDefinition def in e.NewItems)
                {
                    def.ColumnId = "Col" + (ColumnDefinitions.IndexOf(def) + 1);
                    def.DataType = "Text";
                }
            }
        }

        public Action OnFixedLengthFileSelected;
        public Action OnServerNameChanged;
        public Action OnImportData;

        void ImportDataViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        public event PropertyChangedEventHandler LostFocus;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }

            if (LostFocus != null)
            {
                LostFocus(this, new PropertyChangedEventArgs(info));
            }
        }





        #endregion

        #region Constants
        private const string OleDbConnectionStr = "Provider=Microsoft.Jet.OleDb.4.0;Data Source={0};Extended Properties=\"text;HDR=Yes;FMT={1}\"";
        #endregion

        #region Private Members
        private int _engagementID;
        private string _importFilePath;
        private DataTable _importData;
        private FlatFileType _fileType;
        private ICommand _browseFileCommand;
        private ICommand _previewDataCommand;
        private ICommand _importCommand;
        private char _delimiter = ',';
        private string _serverName;
        private string _dataBaseName;
        private string _tableName;
        private ReadOnlyCollection<MasterEntity> _dataImportModes;
        private int _dataImportMode;
        private ObservableCollection<ColumnDefinition> _columnDefinitions;
        private List<string> _dataBaseNames;
        private int _uploadTypeID;
        private bool _canEditDataBaseDetails;
        private Dictionary<string, bool> validProperties;
        private bool _allPropertiesValid;
        #endregion

        #region Public Members

        public char Delimiter
        {
            get { return _delimiter; }
            set { PropertyChanged.HandleValueChange(this, value, ref _delimiter, x => x.Delimiter); }
        }

        //public int EngagementID
        //{
        //    get
        //    {
        //        return _engagementID;
        //    }
        //    set
        //    {
        //        if (PropertyChanged.HandleValueChange(this, value, ref _engagementID, x => x.EngagementID))
        //        {
        //            if (EngagementID == default(int))
        //            {
        //                CanEditDataBaseDetails = true;
        //                DataBaseName = string.Empty;
        //                ServerName = string.Empty;
        //            }
        //            else
        //            {
        //                string serverName;
        //                string dataBaseName;
        //                new BALLayer().GetDatabaseDetails(EngagementID, out serverName, out dataBaseName);

        //                CanEditDataBaseDetails = (serverName.IsEmpty() || dataBaseName.IsEmpty());
        //                _serverName = serverName;
        //                DataBaseNames = dataBaseName.IsNotEmpty() ? new List<string> { dataBaseName } : new List<string>();
        //                DataBaseNames.Insert(0, "Select");
        //                DataBaseName = dataBaseName.IsNotEmpty() ? dataBaseName : "Select";

        //                PropertyChanged.Raise(this, x => x.ServerName);
        //            }
        //        }
        //    }

        //}

        public string ImportFilePath
        {
            get { return _importFilePath; }
            set { PropertyChanged.HandleValueChange(this, value, ref _importFilePath, x => x.ImportFilePath); }

        }

        public ICommand BrowseFileCommand
        {
            get
            {
                if (_browseFileCommand.IsNull())
                {
                    _browseFileCommand = new DelegateCommand(() =>
                    {
                        OpenFileDialog fileDlg = new OpenFileDialog();
                        fileDlg.Filter = "Delimited Files (*.csv, *.txt)|*.csv; *.txt";
                        fileDlg.ValidateNames = true;
                        var res = fileDlg.ShowDialog();

                        if (res.HasValue && res.Value)
                        {
                            ImportFilePath = fileDlg.FileName;

                        }
                    },
                    () => true);
                }
                return _browseFileCommand;

            }
        }

        public ICommand PreviewDataCommand
        {
            get
            {
                if (_previewDataCommand.IsNull())
                {
                    _previewDataCommand = new DelegateCommand(
                    PreviewImportFile
                        //CanPreviewOrImport
                    );
                }

                return _previewDataCommand;
            }
        }

        public ICommand ImportCommand
        {
            get
            {
                if (_importCommand.IsNull())
                {
                    _importCommand = new DelegateCommand(
                    ImportFileData
                    );
                }

                return _importCommand;
            }
        }

        public DataTable ImportData
        {
            get { return _importData; }
            set { PropertyChanged.HandleValueChange(this, value, ref _importData, x => x.ImportData); }
        }

        public FlatFileType FileType
        {
            get { return _fileType; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _fileType, x => x.FileType) && _fileType == FlatFileType.FixedLength && OnFixedLengthFileSelected.IsNotNull())
                {
                    OnFixedLengthFileSelected();
                }
            }
        }

        public string ServerName
        {
            get { return _serverName; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _serverName, x => x.ServerName) && OnServerNameChanged.IsNotNull())
                {
                    OnServerNameChanged();
                }

                if (_serverName != value)
                {
                    _serverName = value;
                    LostFocus(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.ServerName)));
                }

            }
        }

        public string DataBaseName
        {
            get { return _dataBaseName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _dataBaseName, x => x.DataBaseName); }
        }

        public List<string> DataBaseNames
        {
            get { return _dataBaseNames; }
            set { PropertyChanged.HandleValueChange(this, value, ref _dataBaseNames, x => x.DataBaseNames); }
        }

        public string TableName
        {
            get { return _tableName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _tableName, x => x.TableName); }

        }

        public ReadOnlyCollection<MasterEntity> DataImportModes
        {
            get { return _dataImportModes; }
        }

        public int DataImportMode
        {
            get { return _dataImportMode; }
            set { PropertyChanged.HandleValueChange(this, value, ref _dataImportMode, x => x.DataImportMode); }
        }

        public int UploadTypeID
        {
            get { return _uploadTypeID; }
            set { PropertyChanged.HandleValueChange(this, value, ref _uploadTypeID, x => x.UploadTypeID); }
        }

        public ObservableCollection<ColumnDefinition> ColumnDefinitions
        {
            get { return _columnDefinitions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnDefinitions, x => x.ColumnDefinitions); }
        }

        public int DataManagementID { get; set; }

        public bool CanEditDataBaseDetails
        {
            get { return _canEditDataBaseDetails; }
            set { PropertyChanged.HandleValueChange(this, value, ref _canEditDataBaseDetails, x => x.CanEditDataBaseDetails); }
        }

        public bool AllPropertiesValid
        {
            get
            {
                return _allPropertiesValid;
            }
            set
            {
                if (_allPropertiesValid != value)
                {
                    _allPropertiesValid = value;
                    PropertyChanged(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.AllPropertiesValid)));
                }
            }
        }

        #endregion

        private void PreviewImportFile()
        {
            //var fileFormat = (FileType == FlatFileType.Other) ? FileType.GetDisplayName().ToFormat(Delimiter) : FileType.GetDisplayName();
            //var constring = string.Format(OleDbConnectionStr, Path.GetDirectoryName(ImportFilePath));
            // using (OleDbConnection oleConnection = new OleDbConnection(constring))
            //{
            //    oleConnection.Open();

            //    using (OleDbDataAdapter dsCmd = new OleDbDataAdapter(@"SELECT * FROM [{0}]".ToFormat(Path.GetFileName(ImportFilePath)), oleConnection))
            //    {
            //        ImportData = new DataTable("ImportedData");
            //        dsCmd.Fill(ImportData);
            //    }

            //    oleConnection.Close();
            //}

            //DeleteSchemaIniFile();
            //var dt = Import(ImportFilePath);
            //ImportData = dt;
            var fileFormat = (FileType == FlatFileType.Other) ? FileType.GetDisplayName().ToFormat(Delimiter) : FileType.GetDisplayName();
            if (ImportFilePath.IsNotNull())
            {
                var constring = OleDbConnectionStr.ToFormat(Path.GetDirectoryName(ImportFilePath), fileFormat);

                WriteSchemaIniFile();

                //Copy data from csv to mdb
                using (OleDbConnection oleConnection = new OleDbConnection(constring))
                {
                    oleConnection.Open();

                    using (OleDbDataAdapter dsCmd = new OleDbDataAdapter(@"SELECT * FROM [{0}]".ToFormat(Path.GetFileName(ImportFilePath)), oleConnection))
                    {
                        var dt = new DataTable("ImportedData");
                        dsCmd.Fill(dt);
                        // PropertyChanged.Raise(this, x => x.ImportData);
                        ImportData = dt;
                    }

                    oleConnection.Close();
                }

                DeleteSchemaIniFile();
            }
            else
                MessageBoxControl.Show("Please upload a file.", System.Windows.MessageBoxButton.OK, MessageType.Error);
        }

        /// <summary>
        /// Importer the specified fileName.
        /// </summary>
        /// <param name="fileName">The fileName.</param>
        public DataTable Import(string fileName)
        {
            int lineNumber = 0;
            if (fileName.IsNull())
                return null;
            FileInfo file = new FileInfo(fileName);
            var dt = new DataTable();
            WriteSchemaIniFile(file);

            using (OleDbConnection con = JetConnection(file))
            {
                using (OleDbCommand cmd = JetCommand(file, con))
                {
                    con.Open();

                    OleDbDataAdapter ada = new OleDbDataAdapter(cmd);
                    try { ada.Fill(dt); }
                    catch (Exception e)
                    {
                        MessageBoxControl.Show("Please close the file.", System.Windows.MessageBoxButton.OK, MessageType.Error);
                        return null;
                    }

                }
            }

            return dt;
        }

        /// <summary>
        /// Build a Jet Engine connection string.
        /// </summary>
        /// <param name="file">The file object of the delimited file.</param>
        /// <returns>A valid connection string.</returns>
        private OleDbConnection JetConnection(FileInfo file)
        {
            StringBuilder connection = new StringBuilder("Provider=Microsoft.Jet.OLEDB.4.0");

            connection.AppendFormat(";Data Source=\"{0}\"", file.DirectoryName);
            connection.Append(";Extended Properties='text;HDR=Yes");
            connection.Append("';");

            return new OleDbConnection(connection.ToString());
        }

        /// <summary>
        /// Jets the command.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="con">The con.</param>
        /// <returns></returns>
        private OleDbCommand JetCommand(FileInfo file, OleDbConnection con)
        {
            StringBuilder commandText = new StringBuilder("SELECT * FROM ");

            commandText.AppendFormat("[{0}]", file.Name);


            OleDbCommand cmd = new OleDbCommand(commandText.ToString(), con);

            // Set the timeout to an extremely large value. Delimited files can be large, and 
            // executing the command might take up a lot of time
            cmd.CommandTimeout = 60000;

            return cmd;
        }

        /// <summary>
        /// Writes the schema ini file if it doesn't yet exist.
        /// </summary>
        /// <param name="file">The file.</param>
        private void WriteSchemaIniFile(FileInfo file)
        {
            string schema = Path.Combine(file.DirectoryName, "Schema.ini");

            if (!File.Exists(schema))
            {
                File.Delete(schema);
            }
            using (StreamWriter writer = new StreamWriter(schema))
            {
                //writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "[{0}]", file.Name));
                //writer.WriteLine("Format=CSVDelimited");

                //switch (_delimiter)
                //{
                //    case DelimiterType.CustomDelimited:
                //        writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format=Delimited({0})", _customDelimiter));
                //        break;
                //    case DelimiterType.CsvDelimited:
                //    case DelimiterType.TabDelimited:
                //    default:
                //        writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format={0}", _delimiter));
                //        break;
                //}
                switch (FileType)
                {
                    case FlatFileType.Other:
                        writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format=Delimited({0})", Delimiter));
                        break;
                    case FlatFileType.FixedLength:
                        writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format={0}", FileType.GetDisplayName()));

                        if (ColumnDefinitions.IsCollectionValid())
                        {
                            var validColdefns = ColumnDefinitions.Where(x => x.Error.IsEmpty()).ToList();
                            if (validColdefns.IsCollectionValid())
                            {
                                validColdefns.ForEach(c =>
                                {
                                    writer.WriteLine("{0}={1} {2} Width {3}".ToFormat(c.ColumnId, c.ColumnName, c.DataType, c.Width));
                                });
                            }
                        }
                        break;
                    case FlatFileType.SemicolonDelimited:
                    case FlatFileType.SpaceDelimited:
                    case FlatFileType.CommaDelimited:
                    case FlatFileType.TabDelimited:
                    default:
                        writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format={0}", FileType.GetDisplayName()));
                        break;
                }



            }
        }

        private bool CanPreviewOrImport()
        {
            return ImportFilePath.IsNotEmpty() && File.Exists(ImportFilePath)
                && TableName.IsNotEmpty()
                && ServerName.IsNotEmpty()
                && (DataBaseName.IsNotEmpty() && !DataBaseName.Equals("Select"))
                && (UploadTypeID != 0)
                && !((short)FileType == default(short) || (FileType == FlatFileType.Other && Delimiter == default(char)))
                && (FileType != FlatFileType.FixedLength || ColumnDefinitions.Any(cd => cd.Error.IsEmpty()))
                && DataImportMode != 0;
        }

        private void ImportFileData()
        {
            PreviewImportFile();

            if (OnImportData.IsNotNull())
            {
                OnImportData();
            }
        }

        /// <summary>
        /// Writes the schema ini file if it doesn't yet exist.
        /// </summary>
        /// <param name="file">The file.</param>
        private void WriteSchemaIniFile()
        {
            if (ImportFilePath.IsNotNull())
            {
                string schema = Path.Combine(Path.GetDirectoryName(ImportFilePath), "Schema.ini");

                if (File.Exists(schema))
                {
                    File.Delete(schema);
                }

                using (StreamWriter writer = new StreamWriter(schema))
                {
                    writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "[{0}]", Path.GetFileName(ImportFilePath)));

                    switch (FileType)
                    {
                        case FlatFileType.Other:
                            writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format=Delimited({0})", Delimiter));
                            break;
                        case FlatFileType.FixedLength:
                            writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format={0}", FileType.GetDisplayName()));

                            if (ColumnDefinitions.IsCollectionValid())
                            {
                                var validColdefns = ColumnDefinitions.Where(x => x.Error.IsEmpty()).ToList();
                                if (validColdefns.IsCollectionValid())
                                {
                                    validColdefns.ForEach(c =>
                                    {
                                        writer.WriteLine("{0}={1} {2} Width {3}".ToFormat(c.ColumnId, c.ColumnName, c.DataType, c.Width));
                                    });
                                }
                            }
                            break;
                        case FlatFileType.SemicolonDelimited:
                        case FlatFileType.SpaceDelimited:
                        case FlatFileType.CommaDelimited:
                        case FlatFileType.TabDelimited:
                        default:
                            writer.WriteLine(string.Format(CultureInfo.InvariantCulture, "Format={0}", FileType.GetDisplayName()));
                            break;
                    }
                }
            }
        }

        private void DeleteSchemaIniFile()
        {
            string schema = Path.Combine(Path.GetDirectoryName(ImportFilePath), "Schema.ini");

            if (File.Exists(schema))
            {
                File.Delete(schema);
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string propertyName]
        {
            get
            {
                string validationResult = null;
                switch (propertyName)
                {
                    case "TableName":
                        validationResult = ValidateTableName();
                        validProperties[propertyName] = String.IsNullOrEmpty(validationResult) ? true : false;
                        ValidateProperties();
                        break;
                    case "ServerName":
                        validationResult = ValidateServerName();
                        validProperties[propertyName] = String.IsNullOrEmpty(validationResult) ? true : false;
                        ValidateProperties();
                        break;

                    default:
                        throw new ApplicationException("Unknown Property being validated on User.");
                }
                return validationResult;
            }
        }

        private string ValidateServerName()
        {
            if (String.IsNullOrEmpty(this.ServerName))
            {
                return "Please enter Server Name.";
            }

            else if (!String.IsNullOrEmpty(this.ServerName))
            {
                var r = new Regex("^[A-Za-z0-9][A-Za-z0-9,_ ]*$");

                if (!r.IsMatch(this.ServerName))
                {
                    return "Only AlphaNumeric.";
                }
                else
                {
                    return String.Empty;
                }
            }
            else
            {
                return String.Empty;
            }
        }

        private string ValidateTableName()
        {
            if (String.IsNullOrEmpty(this.TableName))
            {
                return "Please enter Table Name.";
            }

            else if (!String.IsNullOrEmpty(this.TableName))
            {
                var r = new Regex("^[A-Za-z0-9][A-Za-z0-9,_ ]*$");

                if (!r.IsMatch(this.TableName))
                {
                    return "Only AlphaNumeric.";
                }
                else
                {
                    return String.Empty;
                }
            }
            else
            {
                return String.Empty;
            }
        }

        private void ValidateProperties()
        {
            AllPropertiesValid = validProperties.Values.All(x => x);
            //foreach (bool isValid in validProperties.Values)
        }
    }
}
