﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KAMP.Core.Common.ViewModels
{
    public class TreeNodeValues : INotifyPropertyChanged
    {
        [XmlIgnore]
        public Action<bool?> OnSimplePropertySelectionChanged { get; set; }
        private bool _iSelected = false;

        public bool IsSelected
        {
            get { return _iSelected; }
            set
            {
                _iSelected = value;
                NotifiyPropertyChanged("IsSelected");
                if (OnSimplePropertySelectionChanged != null)
                {
                    OnSimplePropertySelectionChanged(value);
                }
            }
        }

        public string Name { get; set; }
        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
