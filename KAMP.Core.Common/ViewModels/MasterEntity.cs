﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Common.ViewModels
{
    public class MasterEntity
    {
        public int Code { get; set; }

        public string Name { get; set; }

    }
}
