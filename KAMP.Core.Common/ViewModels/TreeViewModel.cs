﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using System.Windows.Data;
using System.Xml.Serialization;

namespace KAMP.Core.Common.ViewModels
{
   public class TreeViewModel : INotifyPropertyChanged
    {
        [XmlIgnore]
        public Action<bool?> OnComplexChildPropertySelectionChanged { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        private bool? _isSeleted = false;

        public bool? IsSelected
        {
            get { return _isSeleted; }
            set
            {
                SetIsSelected(value, true, true);
            }
        }

        private void SetIsSelected(bool? value, bool isSetParent, bool isSetChild)
        {
            _isSeleted = value;
            NotifiyPropertyChanged("IsSelected");

            if (isSetParent && OnComplexChildPropertySelectionChanged != null)
            {
                OnComplexChildPropertySelectionChanged(value);
            }

            if (isSetChild)
                SelectUnSelectChildNodes(value, SimpleProperty, ComplexProperty);
        }

        public TreeViewModel()
        {
            _simpleProperty = new ObservableCollection<TreeNodeValues>();
            SimpleProperty.CollectionChanged += SimpleProperty_CollectionChanged;
            _complexProperty = new ObservableCollection<TreeViewModel>();
            ComplexProperty.CollectionChanged += ComplexProperty_CollectionChanged;
        }

        void ComplexProperty_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                e.NewItems.Cast<TreeViewModel>()
                                    .Each(x => x.OnComplexChildPropertySelectionChanged += HandleChildSelectionChanged);
            }
        }


        void SimpleProperty_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                e.NewItems.Cast<TreeNodeValues>()
                                    .Each(x => x.OnSimplePropertySelectionChanged += HandleChildSelectionChanged);

            }
        }

        private void HandleChildSelectionChanged(bool? obj)
        {
            var compSelectedCount = ComplexProperty.Count(x => x.IsSelected.HasValue && x.IsSelected.Value);
            var compPartiallySelectCount = ComplexProperty.Count(x => !x.IsSelected.HasValue);
            var simpSelectedCount = SimpleProperty.Count(x => x.IsSelected);

            if ((compSelectedCount == ComplexProperty.Count) && (simpSelectedCount == SimpleProperty.Count))
            {
                SetIsSelected(true, true, false);
            }

            else if ((compSelectedCount == 0) && simpSelectedCount == 0 && compPartiallySelectCount == 0)
            {
                SetIsSelected(false, true, false);
            }
            else { SetIsSelected(null, true, false); }
        }

        private void SelectUnSelectChildNodes(bool? value, ObservableCollection<TreeNodeValues> SimpleProperty, ObservableCollection<TreeViewModel> ComplexProperty)
        {
            foreach (var child in SimpleProperty)
            {
                child.IsSelected = value ?? false;
            }
            foreach (var child in ComplexProperty)
            {
                child.IsSelected = value;
                SelectUnSelectChildNodes(value, child.SimpleProperty, child.ComplexProperty);
            }
        }


        [XmlIgnore]
        public IList Children
        {
            get
            {
                return new CompositeCollection
                {
                    new CollectionContainer()
                    {
                        Collection=SimpleProperty
                    },
                    new CollectionContainer()
                    {
                        Collection=ComplexProperty
                    } 
                };
            }
        }
        [XmlIgnore]
        private ObservableCollection<TreeNodeValues> _simpleProperty { get; set; }
        [XmlIgnore]
        public ObservableCollection<TreeViewModel> _complexProperty { get; set; }



        public ObservableCollection<TreeNodeValues> SimpleProperty
        {
            get
            {
                return _simpleProperty;
            }
            set
            {
                _simpleProperty = value;
                NotifiyPropertyChanged("SimpleProperty");

            }
        }


        public ObservableCollection<TreeViewModel> ComplexProperty
        {
            get
            {
                return _complexProperty;
            }
            set
            {
                _complexProperty = value;
                NotifiyPropertyChanged("ComplexProperty");
            }
        }


        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
