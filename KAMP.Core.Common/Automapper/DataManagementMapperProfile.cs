﻿using AutoMapper;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.Repository.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.Core.Common.Automapper
{
    public class DataManagementMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DataManagementViewModel, DataManagement>()
             .ForMember(d => d.SourceFileName, s => s.MapFrom(p => Path.GetFileNameWithoutExtension(p.ImportFilePath)))
                .ForMember(d => d.SourceFilePath, s => s.MapFrom(p => p.ImportFilePath))
                .ForMember(d => d.DBName, s => s.MapFrom(p => p.DataBaseName))
                .ForMember(d => d.DBServerName, s => s.MapFrom(p => p.ServerName))
                .ForMember(d => d.DBTableName, s => s.MapFrom(p => p.TableName))
                .ForMember(d => d.UploadTypeID, s => s.MapFrom(p => p.UploadTypeID)).IgnoreAllNonExisting();
            base.Configure();
        }
    }
}
