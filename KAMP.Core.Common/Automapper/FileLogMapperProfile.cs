﻿using AutoMapper;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.Repository.KYC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Common.BLL;

namespace KAMP.Core.Common.Automapper
{
    public class FileLogMapperProfile : Profile
    {
        protected override void Configure()
        {
            MapEntityToViewModel();
            MapViewModelToEntity();
            base.Configure();
        }

        private void MapViewModelToEntity()
        {
            Mapper.CreateMap<FileLogsViewModel, FileLog>()
               .ForMember(d => d.cf_ID, s => s.MapFrom(p => p.CfId))
               .ForMember(d => d.Content, s => s.MapFrom(p => p.Content))
               .ForMember(d => d.ContentLength, s => s.MapFrom(p => p.ContentLength))
               .ForMember(d => d.CreatedBy, s => s.MapFrom(p => p.CreatedBy))
               .ForMember(d => d.CreatedDate, s => s.MapFrom(p => p.CreatedDate))
               .ForMember(d => d.DocumentCodeId, s => s.MapFrom(p => p.DocumentCodeId))
               .ForMember(d => d.DocumentType, s => s.MapFrom(p => p.FileType))
               .ForMember(d => d.ModuleId, s => s.MapFrom(p => p.ModuleId))
               .ForMember(d => d.Name, s => s.MapFrom(p => p.Name))
               .ForMember(d => d.Extension, s => s.MapFrom(p => p.FileExtension))
               .ForMember(d => d.PartyTypeId, s => s.MapFrom(p => p.PartyTypeId))

               .IgnoreAllNonExisting();
        }

        private void MapEntityToViewModel()
        {
            Mapper.CreateMap<FileLog, FileLogsViewModel>()
                .ForMember(d => d.CfId, s => s.MapFrom(p => p.cf_ID))
                .ForMember(d => d.Content, s => s.MapFrom(p => p.Content))
                .ForMember(d => d.ContentLength, s => s.MapFrom(p => p.ContentLength))
                .ForMember(d => d.CreatedBy, s => s.MapFrom(p => p.CreatedBy))
                .ForMember(d => d.CreatedDate, s => s.MapFrom(p => p.CreatedDate))
                .ForMember(d => d.DateModified, s => s.MapFrom(p => p.CreatedDate))
                .ForMember(d => d.DocumentCodeId, s => s.MapFrom(p => p.DocumentCodeId))
                .ForMember(d => d.FileExtension, s => s.MapFrom(p => p.Extension))
                .ForMember(d => d.FileType, s => s.MapFrom(p => p.DocumentType))
                .ForMember(d => d.ModuleId, s => s.MapFrom(p => p.ModuleId))
                .ForMember(d => d.Name, s => s.MapFrom(p => p.Name))
                .ForMember(d => d.PartyTypeId, s => s.MapFrom(p => p.PartyTypeId))
                .ForMember(d => d.Id, s => s.MapFrom(p => p.Id))
                .ForMember(d => d.FileName, s => s.MapFrom(p => p.Name))
                .ForMember(d => d.PartyTypeName, s => s.MapFrom(p => GetPartyTypeName(p.PartyTypeId)))
                .ForMember(d => d.DocumentName, s => s.MapFrom(p => GetDocumentName(p.DocumentCodeId)))
                .IgnoreAllNonExisting();

        }

        private string GetDocumentName(int documentCodeId)
        {
            var commonBl = new CommonBL();
            var documentCodes = commonBl.GetDocumentCodes().FirstOrDefault(x => x.Id == documentCodeId).DocumentCodes;
            return documentCodes;
        }

        private string GetPartyTypeName(int partyTypeId)
        {
            var commonBl = new CommonBL();
            var partyName = commonBl.GetPartyTypes().FirstOrDefault(x => x.Id == partyTypeId).Name;
            return partyName;
        }
    }
}
