﻿using KAMP.Core.Common.Automapper;
using KAMP.Core.Common.BLL;
using KAMP.Core.Common.Helper;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using KAMP.Core.FrameworkComponents.ViewModels;
using KAMP.Core.Repository;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for FileLogs.xaml
    /// </summary>
    public partial class FileLogs : UserControl
    {
        private FileLogsViewModel fileLogViewModel;
        public int CfId;
        private int DocumentId;
        private int PartyTypeId;
        Context ctx = Application.Current.Properties["Context"] as Context;
        public FileLogs()
        {
            FileLog_Loaded();
            InitializeComponent();
            Loaded += FileLog_Loaded;

        }

        void FileLog_Loaded(object sender, RoutedEventArgs e)
        {
            fileLogViewModel = this.DataContext as FileLogsViewModel;

            if (fileLogViewModel.IsNotNull())
            {
                CfId = fileLogViewModel.CfId;
                if (!fileLogViewModel.DocumentCode.IsCollectionValid())
                    fileLogViewModel.DocumentCode = GetDocumentCode();
                if (!fileLogViewModel.PartyTypes.IsCollectionValid())
                    fileLogViewModel.PartyTypes = GetPartyTypes();

                fileLogViewModel.ModuleId = ctx.Module.Id;
                fileLogViewModel.DocumentCodeId = 0;
                DocumentId = fileLogViewModel.DocumentCodeId;

                fileLogViewModel.PartyTypeId = 0;
                PartyTypeId = fileLogViewModel.PartyTypeId;

                LoadDataGrid();
                if (fileLogViewModel.OnSubmitClick.IsNull())
                    fileLogViewModel.OnSubmitClick += OnSubmitClick;
            }
        }

        private void OnSubmitClick()
        {
            if (fileLogViewModel.AssignedUser.IsNotNull() && fileLogViewModel.AssignedUser())
            {
                var res = ValidateFileLogViewModel();
                if (res.IsCollectionValid())
                {
                    var sb = new StringBuilder();
                    res.ForEach(x => { sb.AppendLine(x); });
                    MessageBoxControl.Show(sb.ToString(), MessageBoxButton.OK, MessageType.Error);
                }
                else
                {
                    LoadDataGrid();
                }
            }
        }

        private List<string> ValidateFileLogViewModel()
        {
            List<string> errorMessages = new List<string>();
            string errorMessage = null;
            if (fileLogViewModel.PartyTypeId == 0)
            {
                errorMessage = "Please Select a Party Type";
                errorMessages.Add(errorMessage);
            }
            if (fileLogViewModel.DocumentCodeId == 0)
            {
                errorMessage = "Please Select a Document Code";
                errorMessages.Add(errorMessage);
            }
            return errorMessages;
        }



        private System.Collections.ObjectModel.ObservableCollection<Repository.KYC.PartyType> GetPartyTypes()
        {
            var commonBl = new CommonBL();
            var partyTypes = commonBl.GetPartyTypes();
            return partyTypes;
        }

        private System.Collections.ObjectModel.ObservableCollection<Repository.KYC.DocumentCode> GetDocumentCode()
        {
            var commonBl = new CommonBL();
            var documentCodes = commonBl.GetDocumentCodes();
            return documentCodes;
        }

        private void LoadDataGrid()
        {
            var commonBl = new CommonBL();
            new BootStrapper().Configure();
            var fileLogs = commonBl.GetFileLogs(fileLogViewModel);
            if (!fileLogs.IsCollectionValid())
                fileLogs = new List<FileLogsViewModel>();
            GdFileLog.ItemsSource = fileLogs;
        }

        void FileLog_Loaded()
        {
            AssignResources(ctx);
        }

        private void AssignResources(Context appcontext)
        {
            List<ResourceDictionary> resourceDictionaryList = new List<ResourceDictionary>();
            var resource = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    break;
                case ModuleNames.MV:
                    break;
                case ModuleNames.KYC:
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    break;
                default:
                    break;
            }

            foreach (var resources in resourceDictionaryList)
            {
                this.Resources.MergedDictionaries.Add(resources);
            }
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (fileLogViewModel.AssignedUser.IsNotNull() && fileLogViewModel.AssignedUser())
            {

                new BootStrapper().Configure();
                var res = ValidateFileLogViewModel();
                if (res.IsCollectionValid())
                {
                    var sb = new StringBuilder();
                    res.ForEach(x => { sb.AppendLine(x); });
                    MessageBoxControl.Show(sb.ToString(), MessageBoxButton.OK, MessageType.Error);
                }
                else
                {
                    var dlg = new OpenFileDialog();
                    dlg.Multiselect = false;
                    if ((dlg.ShowDialog() ?? false))
                    {
                        var fName = System.IO.Path.GetFileName(dlg.FileName);
                        var fileStream = dlg.OpenFile();
                        var bytes = File.ReadAllBytes(dlg.FileName);
                        if (true)
                        {
                            var currentContext = TaskScheduler.FromCurrentSynchronizationContext();
                            DocumentId = fileLogViewModel.DocumentCodeId;
                            PartyTypeId = fileLogViewModel.PartyTypeId;
                            Task.Factory.StartNew(() => SaveToDisk(fName, bytes))
                                .ContinueWith((t) =>
                                {
                                    // LoadDataGrid();
                                    GdFileLog.InvalidateVisual();
                                }, System.Threading.CancellationToken.None, TaskContinuationOptions.None, currentContext);
                            OpenMessageBox("File '{0}' saved successfully.".ToFormat(fName));
                        }
                    }
                }
            }

        }

        public void SaveToDisk(string fName, byte[] bytes)
        {
            var comprssdFile = CommonHelper.Compress(bytes);
            var commonBl = new CommonBL();
            var fileLogViewModel = new FileLogsViewModel()
            {
                ModuleId = ctx.Module.Id,
                Name = System.IO.Path.GetFileNameWithoutExtension(fName),
                Content = comprssdFile,
                ContentLength = comprssdFile.Length,
                FileType = System.IO.Path.GetExtension(fName),
                CfId = CfId,
                CreatedBy = Convert.ToInt32(ctx.User.Id),
                CreatedDate = DateTime.Now,
                DateModified = DateTime.Now,
                FileExtension = System.IO.Path.GetExtension(fName),
                DocumentCodeId = DocumentId,
                PartyTypeId = PartyTypeId
            };
            commonBl.InsertFileLog(fileLogViewModel);

        }



        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg);
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void Download_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = GdFileLog.SelectedItem as FileLogsViewModel;
            var currentContext = TaskScheduler.FromCurrentSynchronizationContext();
            Task.Factory.StartNew(() =>
            {
                var file = FetchFile(selectedItem.Id);

                if (file != null)
                {
                    var tempPath = System.IO.Path.GetTempFileName();

                    var filePath = tempPath.Replace(System.IO.Path.GetExtension(tempPath), file.FileType);//System.IO.Path.Combine(System.IO.Path.GetTempPath(), file.FullName);
                    File.WriteAllBytes(filePath, file.Content);
                    Process.Start(filePath);
                }
            })
            .ContinueWith((t) =>
            {

            }, System.Threading.CancellationToken.None, TaskContinuationOptions.None, currentContext);
        }

        private FileLogsViewModel FetchFile(int id)
        {
            var bll = new CommonBL();
            var fileLog = bll.GetFileLog(id);
            if (fileLog.IsNotNull())
            {
                fileLog.Content = CommonHelper.Decompress(fileLog.Content);
            }
            return fileLog;

        }


        private void Delete_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (fileLogViewModel.AssignedUser.IsNotNull() && fileLogViewModel.AssignedUser())
            {
                var selectedItem = GdFileLog.SelectedItem as FileLogsViewModel;
                var currentContext = TaskScheduler.FromCurrentSynchronizationContext();
                var res = MessageBoxControl.Show("Are you sure you want to delete the file ? ", MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    Task.Factory.StartNew(() =>
                    {
                        DeleteFile(selectedItem.Id);
                    })
               .ContinueWith((t) =>
                               {
                                   LoadDataGrid();
                                   GdFileLog.InvalidateVisual();
                               }, System.Threading.CancellationToken.None, TaskContinuationOptions.None, currentContext);
                }
            }
        }
        private bool DeleteFile(int id)
        {
            var bll = new CommonBL();
            var fileLog = bll.DeleteFileLog(id);
            return fileLog;
        }


    }
}
