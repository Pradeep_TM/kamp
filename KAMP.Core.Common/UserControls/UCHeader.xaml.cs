﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using KAMP.Core.Interface;
using KAMP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for UCHeader.xaml
    /// </summary>
    public partial class UCHeader : UserControl
    {
        public Action CloseApp;
        public Action ConfigureAudit;
        public IModule module;
        public UCHeader()
        {
            InitializeComponent();
            Loaded += UCHeader_Loaded;
        }

        void UCHeader_Loaded(object sender, RoutedEventArgs e)
        {
            var context = Application.Current.Properties["Context"] as Context;
            if (context.IsNotNull())
                LblUserName.Content = string.Format("{0}" + " " + "{1}", context.User.FirstName, context.User.LastName);
            LblClientName.Content = System.Configuration.ConfigurationManager.AppSettings["ClientName"];
        }

        private void MinimizeApplication(object sender, MouseButtonEventArgs e)
        {
            var currentWindow = Window.GetWindow(this);
            currentWindow.WindowState = WindowState.Minimized;
        }

        private void CloseApplication(object sender, MouseButtonEventArgs e)
        {
            if (CloseApp.IsNotNull())
                CloseApp();
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            foreach (IModule module in ModuleList.ModuleContainer)
            {
                ModuleNames moduleNames;
                bool isParsed = Enum.TryParse(module.Metadata.ModuleName, true, out moduleNames);
                switch (moduleNames)
                {
                    case ModuleNames.UM:
                        FillModuleContext(module.Metadata.ModuleName);
                        var moduleWindow = module.Metadata.HomeWindow as Window;
                        if (moduleWindow.IsNotNull())
                        {
                            moduleWindow.Show();
                            if (!moduleWindow.Equals(Application.Current.MainWindow))
                            {
                                Application.Current.MainWindow.Close();
                                Application.Current.MainWindow = moduleWindow;
                            }
                        }
                        break;
                }
            }
        }

        private void FillModuleContext(string modulename)
        {
            var context = Application.Current.Properties["Context"] as Context;
            context.Module = context.GetModuleDetails(modulename);
        }

        private void LblClientName_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (popup == null)
            {
                var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                popup = new Popup();
                UCClientDetails uCClientDetails = new UCClientDetails();
                uCClientDetails.OnClosePopup += ClosePopup;
                uCClientDetails.TxtClientName.Text =System.Configuration.ConfigurationManager.AppSettings["ClientName"];
                uCClientDetails.TxtServerName.Text = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
                uCClientDetails.TxtdatabaseName.Text = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
                popup.Child = uCClientDetails;
                popup.AllowsTransparency = true;
                popup.AllowDrop = true;
                popup.IsOpen = true;
                var point = e.GetPosition(container);
                popup.HorizontalOffset = point.X - uCClientDetails.ActualWidth;
                popup.VerticalOffset = point.Y;
            }
            else
            {
                popup.IsOpen = true;
            }
        }

        private void ClosePopup()
        {
            popup.IsOpen = false;
        }
        public Popup popup { get; set; }
    }
}
