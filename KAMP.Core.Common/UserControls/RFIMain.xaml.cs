﻿using KAMP.Core.Common.BLL;
using KAMP.Core.Common.Common;
using KAMP.Core.Common.Models;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for RFIMain.xaml
    /// </summary>
    public partial class RFIMain : UserControl
    {
        public Action CloseControl;
        private string cFId;
        private RFIViewModel viewModel;
        public RFIMain(RFIViewModel rFIViewModel)
        {
            viewModel = rFIViewModel;
            DataContext = viewModel;
            InitializeComponent();
            UCSendScreen.DataContext = viewModel;
            UCUploadScreen.DataContext = viewModel;
            AssignResources();
        }

        private void CloseScreenClicked(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl != null)
            {
                CloseControl();
            }
        }

        private void AssignResources()
        {
            var appContext = Application.Current.Properties["Context"] as Context;
            var resource = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appContext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    this.UCSendScreen.Resources.MergedDictionaries.Add(resource);
                    this.UCUploadScreen.Resources.MergedDictionaries.Add(resource);
                    this.Resources.MergedDictionaries.Add(resource);

                    resource = new ResourceDictionary();
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    this.UCUploadScreen.Resources.MergedDictionaries.Add(resource);
                    this.Resources.MergedDictionaries.Add(resource);
                    this.UCViewScreen.Resources.MergedDictionaries.Add(resource);

                    resource = new ResourceDictionary();
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/TabTemplate.xaml", UriKind.RelativeOrAbsolute);
                    this.Resources.MergedDictionaries.Add(resource);
                    break;
                case ModuleNames.MV:
                    break;
                case ModuleNames.KYC:
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    this.UCSendScreen.Resources.MergedDictionaries.Add(resource);
                    this.UCUploadScreen.Resources.MergedDictionaries.Add(resource);
                    this.Resources.MergedDictionaries.Add(resource);

                    resource = new ResourceDictionary();
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    this.UCUploadScreen.Resources.MergedDictionaries.Add(resource);
                    this.Resources.MergedDictionaries.Add(resource);
                    this.UCViewScreen.Resources.MergedDictionaries.Add(resource);

                    resource = new ResourceDictionary();
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/TabTemplate.xaml", UriKind.RelativeOrAbsolute);
                    this.Resources.MergedDictionaries.Add(resource);
                    break;
                default:
                    break;
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tabControl = sender as TabControl;
            if (tabControl.SelectedIndex < 0) return;

            var control = TypeCastToContentControl(e);
            if (control.IsNull() || control.Content.IsNull())
                return;
            if (control.Content.GetType() == typeof(RFIUploadScreen))
            {
                BusyIndicatorHelper.Instance.ShowBusyIndicator("Loading");
                viewModel.RFIUploadViewModel = FillRFIUploadModel();
                control.DataContext = viewModel;
                BusyIndicatorHelper.Instance.HideBusyIndicator();
            }

            else if (control.Content.GetType() == typeof(RFISendScreen))
            {
                control.DataContext = viewModel;
            }

            else if (control.Content.GetType() == typeof(RFIViewScreen))
            {
                viewModel.RFIViewScreenViewModel = FillRFIViewScreenViewModel();
                control.DataContext = viewModel;
            }

        }

        private RFIViewScreenViewModel FillRFIViewScreenViewModel()
        {
            RFIViewScreenViewModel model = new RFIViewScreenViewModel();
            var bLL = new CommonBL();
            model.RFIVeiwList = bLL.GetUploadedMails(viewModel.CaseNumber, viewModel.ModuleId);
            return model;
        }

        private RFIUploadViewModel FillRFIUploadModel()
        {
            RFIUploadViewModel model = new RFIUploadViewModel();
            //Get different Subject Lines for current Module and this case Number
            var bLL = new CommonBL();
            var listOfSubjects = bLL.GetMailSubjectLines(viewModel.ModuleId, viewModel.CaseNumber);
            //Get mails from the mailbox
            Microsoft.Office.Interop.Outlook.Application outlookApp = new Microsoft.Office.Interop.Outlook.Application();
            Microsoft.Office.Interop.Outlook.NameSpace oNS = outlookApp.GetNamespace("mapi");

            var inboxFolder = oNS.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox);
            var sentFolder = oNS.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderSentMail);
            var inboxItems = inboxFolder.Items;
            GetMailsFromInbox(model, listOfSubjects, inboxItems);
            GetMailsFromSentFolder(model, listOfSubjects, sentFolder);

            for (int j = 0; j < model.RFIUploadList.Count - 1; j++)
            {
                var item = model.RFIUploadList[j];
                var data = model.RFIUploadList.Where(x => x.Subject.Equals(item.Subject) && x.SentDateTime.ToString().Equals(item.SentDateTime.ToString())).ToList();
                var count = data.Count;
                if (count >= 2)
                {
                    for (int i = 0; i <= count - 2; i++)
                    {
                        var removeItem = data[i];
                        var removeIndex = model.RFIUploadList.IndexOf(removeItem);
                        model.RFIUploadList.RemoveAt(removeIndex);
                    }
                }
            }
            var uploadedMails = bLL.GetUploadedMails(viewModel.CaseNumber, viewModel.ModuleId);


            foreach (var item in uploadedMails)
            {
                var itm = model.RFIUploadList.FirstOrDefault(x => x.Subject.Trim().ToLower().Equals(item.Subject.Trim().ToLower()) && x.SentDateTime.ToString().Equals(item.SentDateTime.ToString()));
                if (itm.IsNotNull())
                {
                    model.RFIUploadList.Remove(itm);
                }
            }
            return model;

        }

        private void GetMailsFromSentFolder(RFIUploadViewModel model, List<string> listOfSubjects, Microsoft.Office.Interop.Outlook.MAPIFolder sentFolder)
        {
            //Adding Mails from SentBox
            foreach (var item in listOfSubjects)
            {
                foreach (var sentItem in sentFolder.Items)
                {
                    var mailItem = GetMailFromItem(sentItem);
                    if (mailItem != null && mailItem.Subject != null && mailItem.Subject.Contains(item))
                    {
                        

                        var rfiUploadModel = new RFIUploadModel();
                        rfiUploadModel.SourceFolder = "Sent Items";
                        rfiUploadModel.CC = mailItem.CC;
                        rfiUploadModel.MailItem = mailItem;
                        rfiUploadModel.To = mailItem.To;
                        rfiUploadModel.SentDateTime = mailItem.SentOn;
                        rfiUploadModel.Subject = mailItem.Subject;
                        rfiUploadModel.From = mailItem.SenderName;

                        model.RFIUploadList.Add(rfiUploadModel);
                    }
                }
            }
        }

        private void GetMailsFromInbox(RFIUploadViewModel model, List<string> listOfSubjects, Microsoft.Office.Interop.Outlook.Items inboxItems)
        {
            //Adding Mails from inbox
            foreach (var item in listOfSubjects)
            {
                foreach (var inboxItem in inboxItems)
                {
                    var mailItem = GetMailFromItem(inboxItem);
                    if (mailItem != null && mailItem.Subject != null && mailItem.Subject.Contains(item))
                    {
                       
                        var rfiUploadModel = new RFIUploadModel();
                        rfiUploadModel.SourceFolder = "Inbox";
                        rfiUploadModel.CC = mailItem.CC;
                        rfiUploadModel.MailItem = mailItem;
                        rfiUploadModel.To = mailItem.To;
                        rfiUploadModel.SentDateTime = mailItem.SentOn;
                        rfiUploadModel.Subject = mailItem.Subject;
                        rfiUploadModel.From = mailItem.SenderName;
                        model.RFIUploadList.Add(rfiUploadModel);
                    }
                }
            }
        }

        private Microsoft.Office.Interop.Outlook.MailItem GetMailFromItem(object item)
        {
            try
            {
                Microsoft.Office.Interop.Outlook.MailItem mail = (Microsoft.Office.Interop.Outlook.MailItem)item;
                return mail;
            }
            catch
            {
                return null;
            }
        }

        private static ContentControl TypeCastToContentControl(SelectionChangedEventArgs e)
        {
            try
            {
                ContentControl cc = ((System.Windows.Controls.ContentControl)(((object[])(e.AddedItems))[0]));
                return cc;
            }
            catch
            {
                return null;
            }
        }
    }
}
