﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using KAMP.Core.FrameworkComponents;
using System.Windows.Shapes;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.Common.Models;
using System.IO;
using KAMP.Core.Common.BLL;
using KAMP.Core.FrameworkComponents.Enums;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for RFIUploadScreen.xaml
    /// </summary>
    public partial class RFIUploadScreen : UserControl
    {
        public RFIUploadScreen()
        {
            InitializeComponent();
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            var context = DataContext as RFIViewModel;
            if (context.IsNull()) return;
            var currentItem = DgUploadMails.SelectedItem as RFIUploadModel;
            if (currentItem.IsNull()) return;
            var rfiDescriptionViewModel = new RFIDescriptionViewModel { RFIColorModel = context.RFIColorModel, MailItem = currentItem.MailItem };
            var uc = new UCMailDescription();
            uc.Resources = this.Resources;
            uc.DataContext = rfiDescriptionViewModel;
            uc.Close = CloseDescription;
            rFIGrid = new Grid();
            rFIGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            rFIGrid.Children.Add(lbl);
            rFIGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(rFIGrid);
            }
        }

        private void CloseDescription()
        {
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(rFIGrid);
            }
        }



        public Grid rFIGrid { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BtnUpload.IsEnabled = false;
            var context = DataContext as RFIViewModel;
            var appContext = Application.Current.Properties["Context"] as Context;
            var selectedItem = DgUploadMails.SelectedItem as RFIUploadModel;
            if (selectedItem.IsNull() || !selectedItem.IsSelected)
            {
                if (appContext.Module.Name.Equals(ModuleNames.TLB.ToString()))
                {
                    MessageBoxControlTLB.Show("No item selected to upload.");
                }
                if (appContext.Module.Name.Equals(ModuleNames.KYC.ToString()))
                {
                    MessageBoxControlKYC.Show("No item selected to upload.");
                }
                BtnUpload.IsEnabled = true;
                return;
            }

            var path = System.IO.Path.GetTempPath() + @"\RFITemp";
            foreach (var item in context.RFIUploadViewModel.RFIUploadList)
            {
                if (item.IsSelected)
                {
                    var index = context.RFIUploadViewModel.RFIUploadList.IndexOf(item);
                    //Save the mail
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);
                    var pathToSave = path + @"\" + index.ToString() + ".msg";
                    item.MailItem.SaveAs(pathToSave);
                    item.RawMailItem = File.ReadAllBytes(pathToSave);
                    File.Delete(pathToSave);
                    
                }
            }

           

            var bLL = new CommonBL();
            bLL.SaveUploadedMails(context);

            //After Uploading delete selected Mails
            if (appContext.Module.Name.Equals(ModuleNames.TLB.ToString()))
            {
                MessageBoxControlTLB.Show("Messages uploaded successfully");
            }
            if (appContext.Module.Name.Equals(ModuleNames.KYC.ToString()))
            {
                MessageBoxControlKYC.Show("Messages uploaded successfully");
            }

            var items = context.RFIUploadViewModel.RFIUploadList.Where(x => x.IsSelected == true).ToList();

            for (int i = 0; i < items.Count; i++)
            {
                context.RFIUploadViewModel.RFIUploadList.Remove(items[i]);
            }
            BtnUpload.IsEnabled = true;
        }
    }
}
