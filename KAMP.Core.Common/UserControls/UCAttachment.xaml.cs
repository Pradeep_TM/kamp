﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for UCAttachment.xaml
    /// </summary>
    public partial class UCAttachment : UserControl
    {
        public Action<UserControl> OnRemoveAttachmentClickled;
        public UCAttachment(string attachmentName)
        {
            InitializeComponent();
            LblName.Content = attachmentName;
        }

        private void RemoveAttachments(object sender, MouseButtonEventArgs e)
        {
            OnRemoveAttachmentClickled(this);
        }
    }
}
