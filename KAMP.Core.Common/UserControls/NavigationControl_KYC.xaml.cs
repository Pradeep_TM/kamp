﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using KAMP.Core.FrameworkComponents.ViewModels;
using KAMP.Core.Interface;
using KAMP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for NavigationControl_KYC.xaml
    /// </summary>
    public partial class NavigationControl_KYC : UserControl
    {
        private bool isPanelPinned;
        private DispatcherTimer timer;

        public NavigationControl_KYC()
        {
            InitializeComponent();
            Loaded += NavigationControl_Loaded;
        }


        void NavigationControl_Loaded(object sender, RoutedEventArgs e)
        {
            GridNavigation.Width = 0;

        }

        #region RegionNavigationAnimation

        private void BdrNavigationSelected_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ShowPanel(GridNavigation);
            if (timer != null)
                timer.Stop();
        }

        private void HidePanel(Grid gridToHide, bool hideExplicitly = false)
        {
            if (isPanelPinned && !hideExplicitly) return;
            if (gridToHide.Width >= 225)
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 0;
                animation.From = 230;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                gridToHide.BeginAnimation(ItemsControl.WidthProperty, animation);

                DoubleAnimation UcAnimation = new DoubleAnimation();
                UcAnimation.To = 40;
                UcAnimation.From = 270;
                UcAnimation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                this.BeginAnimation(ItemsControl.WidthProperty, UcAnimation);
            }
        }

        private void ShowPanel(Grid gridToHide)
        {
            if (timer != null)
            {
                timer.Stop();
            }
            if (gridToHide.Width == 0)
            {
                DoubleAnimation UcAnimation = new DoubleAnimation();
                UcAnimation.To = 270;
                UcAnimation.From = 40;
                UcAnimation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                this.BeginAnimation(ItemsControl.WidthProperty, UcAnimation);

                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 230;
                animation.From = 0;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                gridToHide.BeginAnimation(ItemsControl.WidthProperty, animation);
            }


            if (isPanelPinned)
                return;
            else
            {
                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 1);
                if (gridToHide == GridNavigation)
                {
                    timer.Tick += HideNavigationPanelIfNotPinned;
                }
                else
                {
                    timer.Tick += HideModulesPanelIfNotPinned;
                }
                timer.Start();
            }
        }

        private void HideModulesPanelIfNotPinned(object sender, EventArgs e)
        {
            HidePanel(GridModules);
            (sender as DispatcherTimer).Stop();
        }

        private void HideNavigationPanelIfNotPinned(object sender, EventArgs e)
        {
            HidePanel(GridNavigation);
            (sender as DispatcherTimer).Stop();
        }



        private void BdrNavigationUnSelected_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            BdrNavigationUnSelected.Visibility = Visibility.Collapsed;
            BdrNavigationSelected.Visibility = Visibility.Visible;
            BdrModulesUnSelected.Visibility = Visibility.Visible;
            BdrModulesSelected.Visibility = Visibility.Collapsed;
            HidePanel(GridModules, true);
            GridNavigation.Visibility = Visibility.Visible;
            GridNavigation.Width = 0;
            ShowPanel(GridNavigation);
        }

        private void BdrModulesSelected_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ShowPanel(GridModules);
            if (timer != null)
                timer.Stop();
        }


        private void BdrModulesUnSelected_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            BdrNavigationUnSelected.Visibility = Visibility.Visible;
            BdrNavigationSelected.Visibility = Visibility.Collapsed;
            BdrModulesUnSelected.Visibility = Visibility.Collapsed;
            BdrModulesSelected.Visibility = Visibility.Visible;
            HidePanel(GridNavigation, true);
            GridModules.Visibility = Visibility.Visible;
            GridModules.Width = 0;
            ShowPanel(GridModules);
        }

        private void HideNavigationPalel_Clicked(object sender, RoutedEventArgs e)
        {
            HidePanel(GridNavigation);
        }

        private void HideModulesPanel_Clicked(object sender, RoutedEventArgs e)
        {
            HidePanel(GridModules);
        }
        #endregion

        private void SelectCurrentIcon(object sender, RoutedEventArgs e)
        {
            var context = DataContext as NavigationControlViewModel;
            var currentItem = (e.OriginalSource as Button).CommandParameter.ToString();
            foreach (NavigationControlModel item in context.ItemsCollection)
            {
                foreach (NavigationItemModel item1 in item.Items)
                {
                    if (!(item1.UserControlName == currentItem))
                    {
                        item1.IsSelected = false;
                    }
                    else
                    {
                        item1.IsSelected = true;
                    }
                }
            }

            HidePanel(GridNavigation, true);
        }

        private void BtnNavigationPin_Click(object sender, RoutedEventArgs e)
        {
            BtnNavigationUnPin.Visibility = Visibility.Visible;
            BtnModulesUnPin.Visibility = Visibility.Visible;
            BtnNavigationPin.Visibility = Visibility.Collapsed;
            BtnModulesPin.Visibility = Visibility.Collapsed;
            isPanelPinned = true;
            if (timer != null)
            {
                timer.Stop();
            }

        }

        private void BtnModulesPin_Click(object sender, RoutedEventArgs e)
        {
            BtnNavigationUnPin.Visibility = Visibility.Visible;
            BtnModulesUnPin.Visibility = Visibility.Visible;
            BtnNavigationPin.Visibility = Visibility.Collapsed;
            BtnModulesPin.Visibility = Visibility.Collapsed;
            isPanelPinned = true;
            if (timer != null)
            {
                timer.Stop();
            }
        }

        private void BtnNavigationUnPin_Click(object sender, RoutedEventArgs e)
        {
            isPanelPinned = false;
            BtnNavigationUnPin.Visibility = Visibility.Collapsed;
            BtnModulesUnPin.Visibility = Visibility.Collapsed;
            BtnNavigationPin.Visibility = Visibility.Visible;
            BtnModulesPin.Visibility = Visibility.Visible;

            ShowPanel(GridNavigation);
        }

        private void BtnModulesUnPin_Click(object sender, RoutedEventArgs e)
        {
            isPanelPinned = false;
            BtnNavigationUnPin.Visibility = Visibility.Collapsed;
            BtnModulesUnPin.Visibility = Visibility.Collapsed;
            BtnNavigationPin.Visibility = Visibility.Visible;
            BtnModulesPin.Visibility = Visibility.Visible;
            ShowPanel(GridModules);
        }


        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            if (timer != null)
                timer.Start();
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (timer != null)
                timer.Stop();
        }

        private void BtnCollapse_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            if (btn.Content.Equals("-"))
            {
                btn.Content = "+";
            }
            else
            {
                btn.Content = "-";
            }
        }

        private void StackPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (timer != null)
                timer.Stop();
        }

        private void GridTLB_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var modulename = ModuleNames.TLB.ToString();
            LoadWindow(modulename);
        }

        private void GridMV_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var modulename = ModuleNames.MV.ToString();
            LoadWindow(modulename);
        }



        private void GridKYC_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var modulename = ModuleNames.KYC.ToString();
            LoadWindow(modulename);
        }

        //private void GridUM_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    var modulename = ModuleNames.UM.ToString();
        //    LoadWindow(modulename);
        //}

        private static void LoadWindow(string modulename)
        {

            var module = ModuleList.ModuleContainer.FirstOrDefault(x => x.Metadata.ModuleName == modulename) as IModule;
            var window = module.Metadata.HomeWindow as Window;
            module.Initialize();
            //Fill selected module details in context
            FillModuleContext(modulename);

            if (window.ToString().Equals(Application.Current.MainWindow.ToString())) return;
            RemoveChildControl(Application.Current.MainWindow);
            //var context = Application.Current.Properties["Context"] as Context;
            //context.Module = context.GetModuleDetails(modulename);

            //if (window == Application.Current.MainWindow) return;
            window.Show();
            Application.Current.MainWindow.Hide();
            Application.Current.MainWindow = window;
        }

        private static void RemoveChildControl(Window wnd)
        {
            Grid container = null;
            UIElement control = null;

            container = wnd.FindName("GridUCContainer") as Grid;
            if (container.IsNotNull())
            {
                control = (UIElement)FindChildControl<dynamic>(container);
                if (control.IsNotNull() && control.Uid.Equals("Report"))
                    container.Children.Remove(control);
            }
        }
        private static DependencyObject FindChildControl<T>(DependencyObject control)
        {
            int childNumber = VisualTreeHelper.GetChildrenCount(control);
            for (int i = 0; i < childNumber; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(control, i);
                if (child != null && child is T)
                    return child;
                else
                    FindChildControl<T>(child);
            }
            return null;
        }

        private static void FillModuleContext(string modulename)
        {
            var context = Application.Current.Properties["Context"] as Context;
            context.Module = context.GetModuleDetails(modulename);
        }
    }
}
