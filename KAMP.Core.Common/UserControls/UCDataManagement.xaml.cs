﻿using AutoMapper;
using KAMP.Core.Common.BLL;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using KAMP.Core.Repository.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for UCDataManagement.xaml
    /// </summary>
    public partial class UCDataManagement : UserControl
    {
        Context ctx = Application.Current.Properties["Context"] as Context;
        public DataManagementViewModel _viewModel;
        public UCDataManagement()
        {
            AssignResources(ctx);
            InitializeComponent();
            _viewModel = new DataManagementViewModel() { TableName = "MV_Transnorm", CanEditDataBaseDetails = true, FileType = Models.FlatFileType.CommaDelimited };
            BindData();
            RegisterEventHandlers();
            this.DataContext = _viewModel;
        }

        private void BindData()
        {
            var commonBl = new CommonBL();
            var uploadTypes = commonBl.GetAllUploadType();
            uploadTypes.Insert(0, new MasterEntity() { Code = 0, Name = "Select" });
            ddlUploadType.ItemsSource = uploadTypes;
            ddlUploadType.SelectedIndex = 0;
        }

        private void RegisterEventHandlers()
        {
            _viewModel.OnFixedLengthFileSelected += ShowFixedLengthSpecDialog;
            _viewModel.OnServerNameChanged += ServerNameChanged;
            _viewModel.OnImportData += ImportData;
        }

        private void ShowFixedLengthSpecDialog()
        {
            //Show Dialog
            ucFixedLenghtSpecDlg.Show(this);
        }
        public void ServerNameChanged()
        {
            string exception = null;
            List<string> dataBaseNames = null;
            try
            {
                var commonBl = new CommonBL();
                if (_viewModel.ServerName.IsNotEmpty())
                {
                    dataBaseNames = commonBl.GetDataBaseNames(_viewModel.ServerName);
                    if (dataBaseNames.IsNull())
                    {
                        dataBaseNames = new List<string>();
                    }
                    dataBaseNames.Insert(0, "Select");
                    _viewModel.DataBaseNames = dataBaseNames;
                    _viewModel.DataBaseName = "Select";

                    if (exception.IsNotEmpty())
                    {
                        MessageBox.Show(exception, "Server unavailable", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex.Message;
            }
        }

        public void ImportData()
        {
            string exception = null;
            List<string> dataBaseNames = null;
            if (_viewModel.ImportData.IsNotNull())
            {
                var errorMsg = ValidateDataManagement(_viewModel);
                if (errorMsg.IsNull() || errorMsg.IsEmpty())
                {
                    var dataManagement = new DataManagement()
                    {
                        UploadTypeID = _viewModel.UploadTypeID,
                        DBName = _viewModel.DataBaseName,
                        DBServerName = _viewModel.ServerName,
                        DBTableName = _viewModel.TableName,
                        SourceFilePath = _viewModel.ImportFilePath,
                        SourceFileName = _viewModel.ImportFilePath
                    };
                    try
                    {
                        var commonBl = new CommonBL();
                        commonBl.ImportData(dataManagement);
                        var res = commonBl.InsertToTable(_viewModel).Result;
                        if (res.IsNotNull())
                        {
                            MessageBoxControl.Show("Please upload a correct file", MessageBoxButton.OK, MessageType.Error);
                        }
                        else
                        {
                            MessageBoxControl.Show("Data imported successfully", MessageBoxButton.OK, MessageType.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        exception = ex.InnerException.InnerException.Message;
                        MessageBoxControl.Show(exception, MessageBoxButton.OK, MessageType.Error);
                    }
                }
                else
                {
                    MessageBoxControl.Show(errorMsg, MessageBoxButton.OK, MessageType.Error);
                }
            }
        }

        private string ValidateDataManagement(DataManagementViewModel _viewModel)
        {
            var stringBldr = new StringBuilder();
            if (_viewModel.IsNotNull())
            {
                if (_viewModel.ImportData.IsNull())
                    stringBldr = stringBldr.AppendLine("Please upload a file.");
                if (_viewModel.ServerName.IsNull())
                    stringBldr = stringBldr.AppendLine("Server Name is mandatory.");
                if (_viewModel.UploadTypeID == 0)
                    stringBldr = stringBldr.AppendLine("Please select a Upload Type.");
                if (_viewModel.DataBaseName.IsNull())
                    stringBldr = stringBldr.AppendLine("Please select a Database.");
                if (_viewModel.DataImportMode == 0)
                    stringBldr = stringBldr.AppendLine("Please select a Mode.");
                if (_viewModel.Delimiter == 0)
                    stringBldr = stringBldr.AppendLine("Please select a Delimter.");

            }
            return stringBldr.ToString();
        }
        private void AssignResources(Context appcontext)
        {
            List<ResourceDictionary> resourceDictionaryList = new List<ResourceDictionary>();
            var resource = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    break;
                case ModuleNames.MV:
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.MV;component/Assets/Styles/ScrollViewerDDL.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.MV;component/Assets/Styles/Style.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    break;
                case ModuleNames.KYC:
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    resource.Source = new Uri(@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resourceDictionaryList.Add(resource);
                    break;
                default:
                    break;
            }

            foreach (var resources in resourceDictionaryList)
            {
                this.Resources.MergedDictionaries.Add(resources);
            }
        }


    }
}
