﻿using KAMP.Core.Common.Helper;
using KAMP.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for RFIViewScreen.xaml
    /// </summary>
    public partial class RFIViewScreen : UserControl
    {
        public RFIViewScreen()
        {
            InitializeComponent();
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = DgViewUploadedMails.SelectedItem as RFIUploadModel;
            if (selectedItem == null) return;
            //var originalBytes = CommonHelper.Decompress(selectedItem.RawMailItem);

            var currentContext = TaskScheduler.FromCurrentSynchronizationContext();
            Task.Factory.StartNew(() =>
            {
                var path = System.IO.Path.GetTempPath() + @"\RFITemp";
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                var filePath = path + @"\tempMessage.msg";

                if (File.Exists(filePath))
                    File.Delete(filePath);
                File.WriteAllBytes(filePath, selectedItem.RawMailItem);

                Process.Start(filePath);
            })
            .ContinueWith((t) =>
            {

            }, System.Threading.CancellationToken.None, TaskContinuationOptions.None, currentContext);
        }
    }
}
