﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for UCFixedLengthSpecDialog.xaml
    /// </summary>
    public partial class UCFixedLengthSpecDialog : UserControl
    {
        private UIElement _window;
        public UCFixedLengthSpecDialog()
        {
            InitializeComponent();
        }
        public void Show(UIElement window)
        {
            _window = window;
            _window.IsEnabled = false;
            popupFixedLengthFiles.IsOpen = true;
        }

        private void Close()
        {
            _window.IsEnabled = true;
            popupFixedLengthFiles.IsOpen = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
