﻿using KAMP.Core.Common.BLL;
using KAMP.Core.Common.Common;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for RFISendScreen.xaml
    /// </summary>
    public partial class RFISendScreen : UserControl
    {
        public Outlook.Application outlookApp;
        public Outlook.NameSpace oNS;
        Outlook.MailItem OutlookMsg;
        Dictionary<string, string> attachmentlist;
        private Context appContext;
        private RFIViewModel _viewModel;
        public RFISendScreen()
        {
            InitializeComponent();
            outlookApp = new Outlook.Application();
            oNS = outlookApp.GetNamespace("mapi");
            attachmentlist = new Dictionary<string, string>();
            appContext = Application.Current.Properties["Context"] as Context;
            // Create a new mail item.
            OutlookMsg = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
        }

        private void SendMail(object sender, RoutedEventArgs e)
        {
            _viewModel = DataContext as RFIViewModel;
            bool areFeildsValid = ValidateFeilds();
            if (!areFeildsValid) return;

            // Create the Outlook application.
            Outlook.Application outlookApp = new Outlook.Application();

            // Get the NameSpace and Logon information.
            oNS = outlookApp.GetNamespace("mapi");

            oNS.Logon();
            if (OutlookMsg.IsNull())
                // Create a new mail item.
                OutlookMsg = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);

            OutlookMsg.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
            OutlookMsg.HTMLBody = GetHtml(new TextRange(TxtBody.Document.ContentStart, TxtBody.Document.ContentEnd).Text);
            OutlookMsg.Subject = _viewModel.RFISendViewModel.Subject.Trim();
            //Adding recipients.
            Outlook.Recipients MailRecipients = (Outlook.Recipients)OutlookMsg.Recipients;
            var recipientList = _viewModel.RFISendViewModel.To.Trim().Split(';');
            foreach (var item in recipientList)
            {
                MailRecipients.Add(item);
            }
            //MailRecipients.Add(_viewModel.RFISendViewModel.To.Trim());
            if (!string.IsNullOrEmpty(_viewModel.RFISendViewModel.CC))
                OutlookMsg.CC = _viewModel.RFISendViewModel.CC.Trim();
            foreach (var account in oNS.Session.Accounts)
            {
                Outlook.Account acc = (Outlook.Account)account;
                var username = appContext.User.UserName.Split('\\').LastOrDefault();
                if (acc.UserName != null && acc.UserName.ToLower() == username.ToLower())
                {
                    //Using a specified account to send the mail
                    OutlookMsg.SendUsingAccount = acc;
                }
            }




            // outlookApp = null;
            var bLL = new CommonBL();
            //Save Entry
            bLL.saveSentMessage(_viewModel, OutlookMsg, attachmentlist);
            if (OutlookMsg.SendUsingAccount != null)
            {
                OutlookMsg.Send();
            }

            if (appContext.Module.Name.Equals(ModuleNames.TLB.ToString()))
            {
                MessageBoxControlTLB.Show("Message has been sent successfully");
            }
            if (appContext.Module.Name.Equals(ModuleNames.KYC.ToString()))
            {
                MessageBoxControlKYC.Show("Message has been sent successfully");
            }

            MailRecipients = null;
            OutlookMsg = null;
            ClearData();
        }

        private bool ValidateFeilds()
        {
            var result = true;
            RegExUtility util = new RegExUtility();
            //Validating to field
            if (_viewModel.RFISendViewModel.To.IsNull() || string.IsNullOrEmpty(_viewModel.RFISendViewModel.To.Trim()))
            {
                LblErrorTo.Visibility = Visibility.Visible;
                result = false;
            }

            if (_viewModel.RFISendViewModel.To.IsNotNull())
            {
                var recipientList = _viewModel.RFISendViewModel.To.Trim().Split(';');
               
                foreach (var recipient in recipientList)
                {
                    if (!util.IsValidEmail(recipient))
                    {
                        LblErrorTo.Visibility = Visibility.Visible;
                        result = false;
                    }
                }
            }

            if (result)
            {
                LblErrorTo.Visibility = Visibility.Collapsed;
            }
            //Validating cc field

            if (_viewModel.RFISendViewModel.CC.IsNotNull() && !string.IsNullOrEmpty(_viewModel.RFISendViewModel.CC.Trim()))
            {
                var cCList = _viewModel.RFISendViewModel.CC.Trim().Split(';');
                foreach (var recipient in cCList)
                {
                    if (!util.IsValidEmail(recipient))
                    {
                        LblErrorCC.Visibility = Visibility.Visible;
                        result = false;
                    }
                }
            }
            
            if (result)
            {
                LblErrorCC.Visibility = Visibility.Collapsed;
            }
            //Validating Subject
            if (_viewModel.RFISendViewModel.Subject.IsNull() || string.IsNullOrEmpty(_viewModel.RFISendViewModel.Subject.Trim()))
            {
                LblErrorSubject.Visibility = Visibility.Visible;
                result = false;
            }
            if (result)
            {
                LblErrorSubject.Visibility = Visibility.Collapsed;
            }
            return result;

        }

        private void ClearData()
        {
            _viewModel.RFISendViewModel = new RFISendViewModel();
            TxtBody.Document = new FlowDocument();
            AttachmentContainer.Children.Clear();
            attachmentlist = new Dictionary<string, string>();
        }

        private string GetHtml(string message)
        {
            StringBuilder builder = new StringBuilder();
            List<string> messageLines = message.Replace("\r\n", "$").Split('$').ToList();
            var items = messageLines.Where(x => x.Length > 0).ToList();
            items.ForEach(x => builder.AppendLine(string.Format("<p>{0}</p>", x)));
            return builder.ToString();
        }

        private void Attach_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog attachment = new OpenFileDialog();
            attachment.Title = "Select a file to Attach";
            attachment.ShowDialog();
            if (string.IsNullOrEmpty(attachment.FileName)) return;
            if (attachmentlist.ContainsKey(attachment.SafeFileName))
            {
                if (appContext.Module.Name.Equals(ModuleNames.TLB.ToString()))
                {
                    MessageBoxControlTLB.Show("This file is already attached.");
                }
                if (appContext.Module.Name.Equals(ModuleNames.KYC.ToString()))
                {
                    MessageBoxControlKYC.Show("This file is already attached.");
                }
                return;
            }
            if (OutlookMsg.IsNull())
                // Create a new mail item.
                OutlookMsg = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
            if (attachment.FileName.Length > 0)
            {
                OutlookMsg.Attachments.Add(
                attachment.FileName,
                Outlook.OlAttachmentType.olByValue, 1
                ,
                attachment.SafeFileName);
                attachmentlist.Add(attachment.SafeFileName, attachment.FileName);
            }

            UCAttachment ucAttachment = new UCAttachment(attachment.SafeFileName);
            AttachmentContainer.Children.Add(ucAttachment);
            ucAttachment.OnRemoveAttachmentClickled += RemoveFromAttachments;
        }

        private void RemoveFromAttachments(UserControl userControl)
        {
            var ucAttachment = userControl as UCAttachment;
            AttachmentContainer.Children.Remove(ucAttachment);
            var index = attachmentlist.IndexOf(ucAttachment.LblName.Content.ToString());
            if (index >= 0)
            {
                OutlookMsg.Attachments.Remove(index.Value + 1);
                attachmentlist.Remove(ucAttachment.LblName.Content.ToString().Trim());
            }
        }
    }
}
