﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for UCClientDetails.xaml
    /// </summary>
    public partial class UCClientDetails : UserControl
    {
        public Action OnClosePopup;
        public UCClientDetails()
        {
            InitializeComponent();
        }

        private void ClosePopUp(object sender, MouseButtonEventArgs e)
        {
            if (OnClosePopup != null)
            {
                OnClosePopup();
            }
        }
    }
}
