﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Common.UserControls
{
    /// <summary>
    /// Interaction logic for ProgressWindow.xaml
    /// </summary>
    public partial class ProgressWindow : Window
    {
        System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
        public ProgressWindow()
        {
            InitializeComponent();
            Loaded += ChangeTextUsingTimer;
            Unloaded += StopTimer;
        }

        private void StopTimer(object sender, RoutedEventArgs e)
        {
            timer.Stop();
        }

        private void ChangeTextUsingTimer(object sender, RoutedEventArgs e)
        {
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ChangeText;
            timer.Start();
        }

        private void ChangeText(object sender, EventArgs e)
        {
            if (lbldots.Content == null)
                lbldots.Content = lbldots.Content + ".";

            else if (lbldots.Content.ToString().Equals(". . ."))
            {
                lbldots.Content = null;
            }
            else lbldots.Content = lbldots.Content + " .";
        }
    }
}
