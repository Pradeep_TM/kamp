﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace KAMP.Core.Common.Common
{
   public static class ImageExtensions
    {
       public static string SaveImage(string sourcepath, string destinationPath)
       {
           if (!string.IsNullOrEmpty(sourcepath) && !string.IsNullOrEmpty(destinationPath))
           {
               destinationPath = destinationPath.Replace(@"\", "/");

               File.Copy(sourcepath, System.IO.Path.Combine(destinationPath, Constants.LOGO_NAME), true);
           }
           return System.IO.Path.Combine(destinationPath, Constants.LOGO_NAME);
       }

       public static Bitmap ScaleImage(int height, int width,string path)
       {
          using (var tmpBitMap = new Bitmap(path))
         {
          
          Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format64bppArgb);
          bitmap.SetResolution(tmpBitMap.HorizontalResolution, tmpBitMap.VerticalResolution);
          using (Graphics g = Graphics.FromImage(bitmap))
          {
              g.InterpolationMode = InterpolationMode.HighQualityBicubic;
              g.CompositingQuality = CompositingQuality.HighQuality;
              g.InterpolationMode = InterpolationMode.HighQualityBicubic;
              g.SmoothingMode = SmoothingMode.HighQuality;
              g.DrawImage(tmpBitMap, 0, 0, width, height);
          }
              return bitmap;
         }
       }

       public static byte[] ToByteArray(Bitmap image, ImageFormat imgFormat)
       {
           byte[] data = null;
           if (image != null)
           {
               Bitmap cloneImage = DeepCopyBitmap(image);
               using (MemoryStream ms = new MemoryStream())
               {
                   cloneImage.Save(ms, imgFormat);
                   data = ms.ToArray();
               }
               cloneImage.Dispose();
               image.Dispose();
           }
           return data != null ? data : null;
       }

       public static Bitmap DeepCopyBitmap(Bitmap oldImage)
       {
           BitmapData oldBitmapData =
               oldImage.LockBits(
                   new Rectangle(new System.Drawing.Point(), oldImage.Size),
                   ImageLockMode.ReadOnly,
                   PixelFormat.Format24bppRgb);

           int byteCount = oldBitmapData.Stride * oldImage.Height;
           byte[] bitmapBytes = new byte[byteCount];
           Marshal.Copy(oldBitmapData.Scan0, bitmapBytes, 0, byteCount);
           oldImage.UnlockBits(oldBitmapData);

           Bitmap newImage = new Bitmap(oldImage.Width, oldImage.Height);

           BitmapData newBitmapData =
               newImage.LockBits(
                   new Rectangle(new System.Drawing.Point(), newImage.Size),
                   ImageLockMode.WriteOnly,
                   PixelFormat.Format24bppRgb);
           Marshal.Copy(bitmapBytes, 0, newBitmapData.Scan0, bitmapBytes.Length);
           newImage.UnlockBits(newBitmapData);

           return newImage;
       }

       public static BitmapImage ToImage(byte[] array)
       {
           using (var ms = new System.IO.MemoryStream(array))
           {
               var image = new BitmapImage();
               image.BeginInit();
               image.CacheOption = BitmapCacheOption.OnLoad;
               image.StreamSource = ms;
               image.EndInit();
               return image != null ? image:null;
           }
       }

       public static void DeleteImage(string imgPath)
       {
           if (File.Exists(imgPath))
               File.Delete(imgPath);
       }

      
    }
}
