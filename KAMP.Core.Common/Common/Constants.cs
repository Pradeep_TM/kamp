﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Common.Common
{
  public class Constants
    {
      public const string COUNTRY = "United States of America";
      public const string LOGO_NAME = "KAMP_Logo.jpeg";
    }
}
