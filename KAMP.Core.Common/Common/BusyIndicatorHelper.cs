﻿using KAMP.Core.Common.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace KAMP.Core.Common.Common
{
    public sealed class BusyIndicatorHelper
    {
        private Window progWindow;
        string _message = null;
        internal static EventWaitHandle _progressWindowWaitHandle;
        bool _isActive;
        private static BusyIndicatorHelper instance = null;
        private static readonly object lockVariable = new object();
        public static BusyIndicatorHelper Instance
        {
            get
            {
                lock (lockVariable)
                {
                    if (instance == null)
                    {
                        instance = new BusyIndicatorHelper();
                    }
                    return instance;
                }
            }
        }

        private BusyIndicatorHelper()
        {

        }

        public void ShowBusyIndicator(string message)
        {
            Application.Current.MainWindow.IsEnabled = false;
            _message = message;
            using (_progressWindowWaitHandle = new AutoResetEvent(false))
            {
                //Starts the progress window thread
                newprogWindowThread = new Thread(new ThreadStart(ShowProgWindow));
                newprogWindowThread.SetApartmentState(ApartmentState.STA);
                newprogWindowThread.IsBackground = true;
                newprogWindowThread.Start();

                //Wait for thread to notify that it has created the window
                _progressWindowWaitHandle.WaitOne();
                _isActive = true;
            }

        }
        public void HideBusyIndicator()
        {
            //closes the Progress window
            if (_isActive)
                progWindow.Dispatcher.Invoke(new Action(progWindow.Close));
            _isActive = false;
            newprogWindowThread.Abort();
            Application.Current.MainWindow.IsEnabled = true;
        }
        private void ShowProgWindow()
        {
            //creates and shows the progress window
            progWindow = new ProgressWindow();
            progWindow.HorizontalAlignment = HorizontalAlignment.Center;
            progWindow.VerticalAlignment = VerticalAlignment.Center;
            progWindow.Show();
            //makes sure dispatcher is shut down when the window is closed
            progWindow.Closed += (s, e) => Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);
            //Notifies command thread the window has been created
            _progressWindowWaitHandle.Set();
            //Starts window dispatcher
            System.Windows.Threading.Dispatcher.Run();
        }

        public Thread newprogWindowThread { get; set; }
    }
}
