﻿using KAMP.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Common.ViewModels;
using AutoMapper;
using KAMP.Core.Repository.KYC;
using KAMP.Core.Common.Automapper;
using System.Collections.ObjectModel;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.UnitOfWork;
using KAMP.Core.Repository.AuditLog;
using System.Windows;
using KAMP.Core.Repository.RFI;
using Microsoft.Office.Interop.Outlook;
using System.IO;
using KAMP.Core.Common.Models;
using KAMP.Core.Common.Helper;
using KAMP.Core.Repository.Models.UserManagement;
using KAMP.Core.Repository.Models;
using System.Data.SqlClient;
using KAMP.Core.Repository.MV;
using System.Reflection;
using KAMP.Core.Common.UserControls;

namespace KAMP.Core.Common.BLL
{
    public class CommonBL
    {
        private const double RecordCount = 5000.0;
        internal List<FileLogsViewModel> GetFileLogs(ViewModels.FileLogsViewModel fileLogViewModel)
        {
            var dbContext = new KAMPContext();
            var fileLogsFrmDb = dbContext.FileLog
                .Where(x => x.cf_ID == fileLogViewModel.CfId && x.ModuleId == fileLogViewModel.ModuleId
                    && x.DocumentCodeId == fileLogViewModel.DocumentCodeId
                    && x.PartyTypeId == fileLogViewModel.PartyTypeId).ToList();
            if (fileLogsFrmDb.IsCollectionValid())
            {
                var mappedFileLogVM = Mapper.Map<List<FileLog>, List<FileLogsViewModel>>(fileLogsFrmDb);
                return mappedFileLogVM;
            }
            return new List<FileLogsViewModel>();
        }

        internal void InsertFileLog(FileLogsViewModel fileLogViewModel)
        {
            var dbContext = new KAMPContext();
            var mappedFileLogModel = Mapper.Map<FileLogsViewModel, FileLog>(fileLogViewModel);
            dbContext.FileLog.Add(mappedFileLogModel);
            dbContext.SaveChanges();
        }

        internal FileLogsViewModel GetFileLog(int id)
        {
            var dbContext = new KAMPContext();
            var fileLog = dbContext.FileLog.FirstOrDefault(x => x.Id == id);
            var mappedFileLogVm = Mapper.Map<FileLog, FileLogsViewModel>(fileLog);
            return mappedFileLogVm;
        }

        internal bool DeleteFileLog(int id)
        {
            var dbContext = new KAMPContext();
            var fileLog = dbContext.FileLog.FirstOrDefault(x => x.Id == id);
            if (fileLog.IsNotNull())
            {
                dbContext.FileLog.Attach(fileLog);
                dbContext.FileLog.Remove(fileLog);
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public string GetConfigurationValue(string key)
        {
            var dbContext = new KAMPContext();
            return dbContext.CommonConfigs.Where(x => x.ConfigName.Trim().ToLower().Equals(key.Trim().ToLower())).
                Select(x => x.ConfigValue).FirstOrDefault();

        }

        public User GetUser(long userId)
        {
            using (var tlbUnitOfWork = new KYCUnitOfWork())
            {
                return tlbUnitOfWork.GetRepository<User>().Items.FirstOrDefault(x => x.UserId == userId);
            }
        }

        public ModuleDetails GetModuleDetails(string moduleName)
        {
            using (var tlbUnitOfWork = new KYCUnitOfWork())
            {
                var abc = tlbUnitOfWork.GetRepository<KAMP.Core.Repository.Module>();
                var module = tlbUnitOfWork.GetRepository<KAMP.Core.Repository.Module>().Items.FirstOrDefault(x => x.ModuleName.ToLower() == moduleName.ToLower());

                return new ModuleDetails()
                {
                    Id = module.ModuleTypeId,
                    Name = moduleName
                };
            }
        }

        internal ObservableCollection<DocumentCode> GetDocumentCodes()
        {
            var dbContext = new KAMPContext();
            ObservableCollection<DocumentCode> docCodes = new ObservableCollection<DocumentCode>();
            var documentCodes = dbContext.DocumentCode.OrderBy(x => x.DocumentCodes).ToList();
            documentCodes.ForEach(docCodes.Add);
            var documentCodesInitial = new DocumentCode()
            {
                Id = 0,
                DocumentCodes = "-Select-"
            };
            docCodes.Insert(0, documentCodesInitial);
            return docCodes;
        }

        internal ObservableCollection<PartyType> GetPartyTypes()
        {
            var dbContext = new KAMPContext();
            ObservableCollection<PartyType> partyTypes = new ObservableCollection<PartyType>();

            var partyType = dbContext.PartyType.OrderBy(x => x.Name).ToList();
            partyType.ForEach(partyTypes.Add);
            var partyTypeInitial = new PartyType()
            {
                Id = 0,
                Name = "-Select-"
            };
            partyTypes.Insert(0, partyTypeInitial);
            return partyTypes;
        }


        #region Audit Logic
        public List<ConfigurationTable> GetAuditConfiguration()
        {
            using (var auditUOW = new AuditUnitOfWork())
            {
                var appcontext = System.Windows.Application.Current.Properties["Context"] as Context;
                var auditRepo = auditUOW.GetRepository<ConfigurationTable>();
                return auditRepo.Items.Include(x => x.EntityModelXmls).Where(x => x.ModuleId == appcontext.Module.Id).ToList();
            }
        }

        public void AddConfiguration(ConfigurationTable config)
        {
            using (var auditUOW = new AuditUnitOfWork())
            {
                var auditRepo = auditUOW.GetRepository<ConfigurationTable>();
                auditRepo.Insert(config);
                auditUOW.SaveChanges();
            }
        }

        public void UpdateConfiguration(ConfigurationTable config)
        {
            using (var auditUOW = new AuditUnitOfWork())
            {
                var auditRepo = auditUOW.GetRepository<ConfigurationTable>();
                var auditData = auditRepo.Items.FirstOrDefault(x => x.Id == config.Id);
                auditData.ConfigurationXml = config.ConfigurationXml;

                foreach (var item in config.EntityModelXmls)
                {
                    var auditRepoXml = auditUOW.GetRepository<EntityModelXml>();
                    var auditDataXml = auditRepoXml.Items.FirstOrDefault(x => x.ConfigurationId == item.ConfigurationId && x.Action == item.Action);
                    if (auditDataXml.IsNull())
                    {
                        auditRepoXml.Insert(item);
                    }
                    else
                        auditDataXml.EntityXml = item.EntityXml;
                }
                auditUOW.SaveChanges();
            }
        }
        #endregion

        internal void saveSentMessage(RFIViewModel rFIViewModel, Microsoft.Office.Interop.Outlook.MailItem outlookMsg, Dictionary<string, string> attachmentList)
        {
            using (var rFInitOfWork = new RFIUnitOfWork())
            {
                var repo = rFInitOfWork.GetRepository<RequestForInformation>();
                var modelToSave = new RequestForInformation();
                if (!string.IsNullOrEmpty(modelToSave.CC))
                    modelToSave.CC = rFIViewModel.RFISendViewModel.CC.Trim();
                modelToSave.Body = outlookMsg.HTMLBody;
                modelToSave.CaseNumber = rFIViewModel.CaseNumber;
                //fill user name
                modelToSave.CreatedBy = rFIViewModel.SenderName;
                if (outlookMsg.Attachments.Count > 0)
                {
                    modelToSave.IsAttachmentMail = true;
                    //Save Attachments

                    for (int i = 0; i < attachmentList.Count; i++)
                    {
                        byte[] bytes = File.ReadAllBytes(attachmentList.ElementAt(i).Value);
                        //var compressedBytes = CommonHelper.Compress(bytes);
                        modelToSave.RFIAttachments.Add(new RFIAttachments { Attachment = bytes, AttachmentName = attachmentList.ElementAt(i).Key });
                    }
                }
                modelToSave.ModuleId = rFIViewModel.ModuleId;
                modelToSave.SentDate = outlookMsg.SentOn;
                modelToSave.SentFrom = rFIViewModel.SenderName;
                modelToSave.SentTo = rFIViewModel.RFISendViewModel.To.Trim();
                modelToSave.Subject = rFIViewModel.RFISendViewModel.Subject.Trim();
                repo.Insert(modelToSave);
                rFInitOfWork.SaveChanges();

                //if (outlookMsg.Attachments.Count > 0)
                //{
                //    //Save Attachments
                //    var attachmentRepo = rFInitOfWork.GetRepository<RFIAttachments>();
                //    var currentRecord = repo.Items.OrderByDescending(x=>x.Id).FirstOrDefault(x => x.CfId.Equals(rFIViewModel.CaseNumber) && x.Subject.Equals(rFIViewModel.RFISendViewModel.Subject) && x.ModuleId == rFIViewModel.ModuleId && x.Body.Equals(outlookMsg.HTMLBody));
                //    if (currentRecord.IsNull()) return;
                //    for (int i = 0; i < attachmentList.Count; i++)
                //    {
                //        byte[] bytes = File.ReadAllBytes(attachmentList.ElementAt(i).Value);
                //        attachmentRepo.Insert(new RFIAttachments { RFIId = currentRecord.Id, Attachment = bytes, AttachmentName = attachmentList.ElementAt(i).Key });
                //    }
                //}
                //rFInitOfWork.SaveChanges();
            }
        }

        internal List<string> GetMailSubjectLines(int moduleId, string caseNumber)
        {
            var data = new List<string>();
            using (var rfiUnitOfWork = new RFIUnitOfWork())
            {
                var repo = rfiUnitOfWork.GetRepository<RequestForInformation>();
                var records = repo.Items.Where(x => x.ModuleId == moduleId && x.CaseNumber.Equals(caseNumber)).ToList();
                if (records.Count > 0)
                {
                    data = records.Select(x => x.Subject).Distinct().ToList();
                }
            }
            return data;
        }

        internal void SaveUploadedMails(RFIViewModel context)
        {
            var data = new List<string>();
            using (var rfiUnitOfWork = new RFIUnitOfWork())
            {
                var repoUpload = rfiUnitOfWork.GetRepository<RFIResponses>();
                foreach (var item in context.RFIUploadViewModel.RFIUploadList)
                {
                    if (item.IsSelected)
                    {
                        repoUpload.Insert(
                            new RFIResponses
                            {
                                ResponseUploaded = item.RawMailItem,
                                CreatedBy = context.SenderName,
                                CreatedDate = System.DateTime.Now,
                                CC = item.CC,
                                CaseNumber = context.CaseNumber,
                                ModuleId = context.ModuleId,
                                SentDate = item.SentDateTime.Value,
                                SentFrom = item.From,
                                SentTo = item.To,
                                Subject = item.Subject
                            });
                    }
                }
                rfiUnitOfWork.SaveChanges();
            }
        }

        internal ObservableCollection<RFIUploadModel> GetUploadedMails(string caseNumber, int ModuleId)
        {
            var uploadedMailList = new ObservableCollection<RFIUploadModel>();
            using (var rfiUnitOfWork = new RFIUnitOfWork())
            {
                var repo = rfiUnitOfWork.GetRepository<RFIResponses>();
                var items = repo.Items.Where(x => x.CaseNumber.Equals(caseNumber) && x.ModuleId == ModuleId).ToList();
                foreach (var item in items)
                {
                    var data = new RFIUploadModel
                    {
                        CC = item.CC,
                        From = item.SentFrom,
                        CreatedBy = item.CreatedBy,
                        RawMailItem = item.ResponseUploaded,
                        Subject = item.Subject,
                        SentDateTime = item.SentDate,
                        To = item.SentTo
                    };
                    uploadedMailList.Add(data);
                }
            }
            return uploadedMailList;
        }

        public void AddUserLoginHistory(FrameworkComponents.Context context, DateTime dateTime)
        {
            var dbContext = new KAMPContext();
            var user = context.User;
            var module = context.Module;
            if (context.IsNotNull())
            {
                var userLoginHistory = new UserLoginHistory()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.UserName,
                    UserId = user.Id,
                    LoginTime = dateTime,
                    ModuleTypeId = module.Id,
                    ModuleName = module.Name
                };
                dbContext.UserLoginHistory.Add(userLoginHistory);
                dbContext.SaveChanges();
            }
        }

        #region DataManagement

        public List<KAMP.Core.Common.ViewModels.MasterEntity> GetAllUploadType()
        {
            var dbContext = new KAMPContext();
            var uploadTypes = dbContext.UploadType.ToList();
            var masterTypes = new List<KAMP.Core.Common.ViewModels.MasterEntity>();
            uploadTypes.ForEach(x =>
            {
                var masterEntity = new KAMP.Core.Common.ViewModels.MasterEntity()
                {
                    Code = x.UploadTypeID,
                    Name = x.UploadTypeName
                };
                masterTypes.Add(masterEntity);
            });
            return masterTypes;
        }

        public List<string> GetDataBaseNames(string serverName)
        {
            var lstStrng = new List<string>();
            var conString = new SqlConnectionStringBuilder();
            conString.DataSource = serverName;
            conString.IntegratedSecurity = true;
            using (var uow = new DataImportUnitOfWork(conString.ConnectionString))
            {
                var connStatus = CheckConnectionState(conString.ConnectionString);
                if (connStatus)
                {
                    var res = uow.ExecuteSqlQuery("select Name from sys.databases where owner_sid != 0x01").AsEnumerable().ToList();
                    foreach (DataRow row in res)
                    {
                        var items = row.ItemArray.FirstOrDefault().ToString();
                        lstStrng.Add(items);
                    }
                }
                else
                {
                    MessageBoxControl.Show("Please select a correct server name.", MessageBoxButton.OK, MessageType.Error);
                }
            }
            return lstStrng;
        }

        private bool CheckConnectionState(string connString)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                try
                {
                    con.Open();
                    return (con.State == ConnectionState.Open);
                }
                catch
                {
                    return false;
                }
            }
            //return false;
        }
        #endregion

        public DataTable GetData(DataManagementViewModel dataManagement)
        {
            return new DataTable();
        }

        internal void ImportData(DataManagement data)
        {
            var dbContext = new KAMPContext();
            dbContext.DataManagement.Add(data);
            dbContext.SaveChanges();
        }



        internal async Task<string> InsertToTable(DataManagementViewModel _viewModel)
        {
            var conString = new SqlConnectionStringBuilder();
            conString.DataSource = _viewModel.ServerName;
            conString.InitialCatalog = _viewModel.DataBaseName;
            conString.IntegratedSecurity = true;
            var importDataFromDb = new List<TransNorm>();
            using (var mvUow = new DataImportUnitOfWork(conString.ConnectionString))
            {
                if (_viewModel.ImportData.IsNotNull())
                {
                    var lstData = _viewModel.ImportData.ToList<TransNorm>().Select(x => x.TranNo).ToList();
                    var lstDataRange = Math.Ceiling(((lstData.Count) / RecordCount));
                    var repo = mvUow.GetRepository<TransNorm>();
                    for (int i = 0; i < lstDataRange; i++)
                    {
                        var selectedLstData = lstData.Skip(Convert.ToInt32(RecordCount) * i).Take(Convert.ToInt32(RecordCount)).ToList();
                        var transNormLst = repo.Items.Where(x => selectedLstData.Contains(x.TranNo)).ToList();
                        importDataFromDb.AddRange(transNormLst);
                        if (transNormLst.IsCollectionValid())
                        {

                            repo.DeleteAll(transNormLst);

                            mvUow.SaveChanges();
                        }
                    }
                }

                //Task.Factory.StartNew(() =>
                //{
                //    for()
                //});

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conString.ConnectionString, SqlBulkCopyOptions.Default))
                {
                    bulkCopy.DestinationTableName = "[{0}]".ToFormat(_viewModel.TableName);
                    var idColumn = new DataColumn("Id", typeof(long));
                    _viewModel.ImportData.Columns.Add(idColumn);
                    idColumn.SetOrdinal(1);
                    _viewModel.ImportData.Rows.Cast<DataRow>().Each(x => { x[14] = _viewModel.ImportFilePath; });
                    bulkCopy.BatchSize = 10000;
                    try
                    {
                        //throw new System.Exception();
                        bulkCopy.WriteToServer(_viewModel.ImportData);
                        bulkCopy.Close();
                        return null;
                    }
                    catch (System.Exception e)
                    {

                        InsertExistingRecords(_viewModel, importDataFromDb, conString.ConnectionString);
                        return e.Message;
                    }
                }

                //mvUow.SaveChanges();
                // }
            }

        }

        private void InsertExistingRecords(DataManagementViewModel _viewModel, List<TransNorm> importDataFromDb, string connectionString)
        {
            if (importDataFromDb.IsCollectionValid())
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.Default))
                {
                    bulkCopy.DestinationTableName = "[{0}]".ToFormat(_viewModel.TableName);

                    bulkCopy.BatchSize = 10000;
                    try
                    {
                        bulkCopy.WriteToServer(_viewModel.ImportData);
                        bulkCopy.Close();
                    }
                    catch (System.Exception e)
                    {
                    }
                }
        }



        internal List<Client> GetApplicationConfigurations()
        {
            using (var appConfigUOW = new AppConfigUnitOfWork())
            {
                var appRepo = appConfigUOW.GetRepository<Client>();
                var getConfigs = appRepo.Items.Include(x => x.ClientEngagement).ToList();
                return getConfigs;
            }

        }



        public List<string> GetAllClientNames()
        {
            using (var appConfigUOW = new AppConfigUnitOfWork())
            {
                var appRepo = appConfigUOW.GetRepository<Client>();
                return appRepo.Items.Select(x => x.ClientName).ToList();
            }
        }


        public List<string> GetAllEngagementNames()
        {
            using (var appConfigUOW = new AppConfigUnitOfWork())
            {
                var appRepo = appConfigUOW.GetRepository<ClientEngagement>();
                return appRepo.Items.Select(x => x.EngagementName).ToList();
            }
        }


        public List<string> GetAllEngagementNo()
        {
            using (var appConfigUOW = new AppConfigUnitOfWork())
            {
                var appRepo = appConfigUOW.GetRepository<ClientEngagement>();
                return appRepo.Items.Select(x => x.EngagementNumber).ToList();
            }
        }

        public Client GetDetailsUponClient(string clientName)
        {
            using (var appConfigUOW = new AppConfigUnitOfWork())
            {
                var appRepo = appConfigUOW.GetRepository<Client>();
                var config = appRepo.Items.Include(x => x.ClientEngagement).ToList();
                if (config.IsCollectionValid())
                    return config.FirstOrDefault(x => x.ClientName.Trim().ToLower().Equals(clientName.Trim().ToLower()));
                else
                    return null;
            }
        }

        internal Client GetDetailsUponEngagement(string engagementName, string engagementNumber)
        {
            using (var appConfigUOW = new AppConfigUnitOfWork())
            {
                var appRepo = appConfigUOW.GetRepository<Client>();
                var config = appRepo.Items.Include(x => x.ClientEngagement).ToList();
                if (config.IsCollectionValid())
                {
                    if (!string.IsNullOrEmpty(engagementName))
                    {
                        return config.FirstOrDefault(x => x.ClientEngagement.Any(e => e.EngagementName.Trim().ToLower()
                            .Equals(engagementName.Trim().ToLower())));
                    }
                    else if (!string.IsNullOrEmpty(engagementNumber))
                    {
                        return config.FirstOrDefault(x => x.ClientEngagement.Any(e => e.EngagementNumber.Trim().ToLower()
                            .Equals(engagementNumber.Trim().ToLower())));
                    }
                }
                return null;
            }
        }

        //public bool SaveClientDetails(ApplicationConfiguration viewModel, string connectionString)
        //{
        //    using (var appConfigUOW = new AppConfigUnitOfWork(connectionString))
        //    {
        //        new BootStrapper().Configure();
        //        var clientRepo = appConfigUOW.GetRepository<Client>();
        //        var clientEngagementRepo = appConfigUOW.GetRepository<ClientEngagement>();
        //        var client = clientRepo.Items.Include(x => x.ClientEngagement).FirstOrDefault(x => x.ClientName.Trim().ToLower().Equals(viewModel.ClientName.Trim().ToLower()));
        //        if (client.IsNull())
        //        {
        //            var newClient = Mapper.Map<Client>(viewModel);
        //            var newClientEnagement = Mapper.Map<ClientEngagement>(viewModel);
        //            newClient.ClientEngagement.Add(newClientEnagement);
        //            clientRepo.Insert(newClient);
        //            appConfigUOW.SaveChanges();
        //            return true;
        //        }
        //        else if (client.IsNotNull())
        //        {
        //            var clientEnagement = client.ClientEngagement.FirstOrDefault(x => x.EngagementName.Trim().ToLower().Equals(viewModel.EngagementName.Trim().ToLower()));
        //            if (clientEnagement.IsNotNull())
        //            {
        //                if (!clientEnagement.EngagementNumber.Equals(viewModel.EngagementNumber))
        //                    throw new BusinessException("Please select appropriate values");
        //                return false;
        //            }

        //            if (client.ClientEngagement.Any(x => x.EngagementNumber.Trim().ToLower().Equals(viewModel.EngagementNumber.Trim().ToLower())))
        //                throw new BusinessException("Please select appropriate values");

        //            var newClientEnagement = Mapper.Map<ClientEngagement>(viewModel);
        //            client.ClientEngagement.Add(newClientEnagement);
        //            client.ClientCode = viewModel.ClientCode;
        //            client.Logo = viewModel.Logo;
        //            client.ZipCode = viewModel.ZipCode;
        //            appConfigUOW.SaveChanges();
        //            return true;
        //        }
        //    }
        //    return false;
        //}
    }
}
