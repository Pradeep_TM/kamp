﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Text.RegularExpressions;

namespace KAMP.Core.Common.Models
{
    public class ColumnDefinition : INotifyPropertyChanged, IDataErrorInfo
    {
        public ColumnDefinition()
        {
            PropertyChanged += new PropertyChangedEventHandler(ColumDefinition_PropertyChanged);
        }

        void ColumDefinition_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        private string _columnName;

        public string ColumnName
        {
            get { return _columnName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _columnName, x => x.ColumnName); }
        }

        public string ColumnId { get; set; }
        public string DataType { get; set; }
        public short Width { get; set; }

        public string Error
        {
            get { return this[this.GetPropertyName(x => x.ColumnName)] + this[this.GetPropertyName(x => x.Width)]; }
        }

        public string this[string propName]
        {
            get
            {
                if (propName == this.GetPropertyName(x => x.ColumnName))
                {
                    if (ColumnName.IsEmpty())
                        return "Enter the Column Name";

                    if (!new Regex("^[a-zA-Z][a-zA-Z0-9 _-]{0,24}$").IsMatch(ColumnName))
                        return "Column Name should contain alphanumeric characters only";
                }

                if (propName == this.GetPropertyName(x => x.Width))
                {
                    if (Width <= 0 || Width >= 9999)
                        return "Width should be between 1 to 9999";
                }

                return string.Empty;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
