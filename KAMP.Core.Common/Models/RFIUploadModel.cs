﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using KAMP.Core.FrameworkComponents;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace KAMP.Core.Common.Models
{
   public class RFIUploadModel: INotifyPropertyChanged
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected); }
        }

        private string _to;
        public string To
        {
            get { return _to; }
            set { PropertyChanged.HandleValueChange(this, value, ref _to, (x) => x.To); }
        }

        private string _from;
        public string From
        {
            get { return _from; }
            set { PropertyChanged.HandleValueChange(this, value, ref _from, (x) => x.From); }
        }

        private string _sourceFolder;
        public string SourceFolder
        {
            get { return _sourceFolder; }
            set { PropertyChanged.HandleValueChange(this, value, ref _sourceFolder, (x) => x.SourceFolder); }
        }

        private string _cC;
        public string CC
        {
            get { return _cC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cC, (x) => x.CC); }
        }

        private string _subject;
        public string Subject
        {
            get { return _subject; }
            set { PropertyChanged.HandleValueChange(this, value, ref _subject, (x) => x.Subject); }
        }

        private string _createdBy;
        public string CreatedBy
        {
            get { return _createdBy; }
            set { PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy); }
        }


        private MailItem _mailItem;
        public MailItem MailItem
        {
            get { return _mailItem; }
            set { PropertyChanged.HandleValueChange(this, value, ref _mailItem, (x) => x.MailItem); }
        }

        private DateTime? _sentDateTime;
        public DateTime? SentDateTime
        {
            get { return _sentDateTime; }
            set { PropertyChanged.HandleValueChange(this, value, ref _sentDateTime, (x) => x.SentDateTime); }
        }

        private byte[] _rawMailItem;
        public byte[] RawMailItem
        {
            get { return _rawMailItem; }
            set { PropertyChanged.HandleValueChange(this, value, ref _rawMailItem, (x) => x.RawMailItem); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
