﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Common.Models
{
    public enum FlatFileType : short
    {
        [Display(Name = "TabDelimited")]
        TabDelimited = 1,
        [Display(Name = "Delimited(,)")]
        CommaDelimited,
        [Display(Name = "Delimited(;)")]
        SemicolonDelimited,
        [Display(Name = "Delimited( )")]
        SpaceDelimited,
        [Display(Name = "FixedLength")]
        FixedLength,
        [Display(Name = "Delimited({0})")]
        Other,
    }
}
