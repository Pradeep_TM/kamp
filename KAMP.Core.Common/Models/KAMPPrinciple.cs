﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.UnitOfWork;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository;
using System.Windows;
using KAMP.Core.Common.UserControls;

namespace KAMP.Core.Common.Models
{
    public class KAMPPrincipal : IPrincipal
    {

        List<string> roles;
        List<Dictionary<string, List<string>>> permissionSetDict;

        public KAMPPrincipal(IIdentity identity)
        {
            this.Identity = identity;
            SetUserRoles();
        }
        public IIdentity Identity
        {
            get;
            private set;
        }

        public bool IsInRole(string role)
        {
            if (roles.IsCollectionValid())
                return roles.Contains(role);
            return false;
        }

        public bool IsInModule(int module)
        {
            return modules.ContainsKey(module);
        }

        private void SetUserRoles()
        {
            var name = Identity.Name;
            string[] domainUser = name.Split('\\');
            var domain = domainUser[0];
            var user = domainUser[1];
            using (var uow = new UMUnitOfWork())
            {
                IQueryable<User> userQry = uow.GetRepository<User>().Items.Include(x => x.UserInRole
                                                            .Select(y => y.Role.RoleInPermission
                                                                .Select(z => z.ModuleType)))
                                                .Include(x => x.UserInRole
                                                            .Select(y => y.Role.RoleInPermission
                                                                .Select(z => z.PermissionSet.PermissionInSet.Select(s => s.Permission))))
                                                .Where(x => x.UserName == user && x.IsActive);

                var userEnt = userQry.FirstOrDefault();
                if (userEnt.IsNull())
                {
                    return;
                }

                UserId = userEnt.UserId;
                modules = new Dictionary<int, string>();
                if (userEnt.IsSuperAdmin.HasValue && userEnt.IsSuperAdmin.Value == false)
                {
                    if (userEnt.UserInRole.IsCollectionValid())
                    {
                        try
                        {
                            roles = userEnt.UserInRole.Select(x => x.Role.RoleName).ToList();
                            //var res = userEnt.UserInRole.SelectMany(x => x.Role.RoleInPermission.Select(y => y.PermissionSet).Where(c => c.IsActive)).Where(r => r.IsActive);
                            var roleInPermissionList = userEnt.UserInRole.SelectMany(x => x.Role.RoleInPermission.Where(z => z.IsActive.Value == true).Select(r => r.PermissionSet)).ToList();
                            List<PermissionSet> permissionLst = new List<PermissionSet>();
                            foreach (var roleInPermItem in roleInPermissionList)
                            {
                                if (!permissionLst.Contains(roleInPermItem))
                                    permissionLst.Add(roleInPermItem);
                            }

                            var permissions = permissionLst.ToDictionary(x => x.ModuleObjectName, y => y.PermissionInSet.Select(pis => pis.Permission.PermissionName).ToList());

                            var mods = userEnt.UserInRole.SelectMany(x => x.Role.RoleInPermission.Select(y => y.ModuleType));

                            foreach (var modId in mods)
                            {
                                if (!modules.ContainsKey(modId.ModuleTypeId))
                                    modules.Add(modId.ModuleTypeId, modId.ModuleName);
                            }
                            List<Dictionary<string, List<string>>> permissionSets = new List<Dictionary<string, List<string>>>();
                            permissionSets.Add(permissions);
                            permissionSetDict = permissionSets;
                        }
                        catch (Exception ex)
                        {
                            //MessageBoxControl.Show("The role and permission mapping is wrong.Please correct it and try again.");
                        }
                    }
                }
                else if (userEnt.IsSuperAdmin.HasValue && userEnt.IsSuperAdmin.Value == true)
                {
                    var modulesFromDb = uow.GetRepository<Module>().Items.ToList();
                    if (modulesFromDb.IsCollectionValid())
                    {
                        foreach (var module in modulesFromDb)
                        {
                            modules.Add(module.ModuleTypeId, module.ModuleName);
                        }
                    }
                }
            }
        }

        public List<string> GetUserRoles()
        {
            return roles;
        }

        public bool ValidatePermissions(string Entity, string Action, string moduleId = null)
        {
            res = false;
            var modId = 0;
            if (permissionSetDict != null)
            {
                var permissionSetFromDb = new Dictionary<long, string>();
                using (var uow = new UMUnitOfWork())
                {
                    if (moduleId.IsNotNull())
                    {
                        modId = moduleId.AsInt();
                        permissionSetFromDb = uow.GetRepository<PermissionSet>().Items.Where(x => x.ModuleId == modId).ToDictionary(x => x.PermissionSetId, x => x.ModuleObjectName);
                    }
                }
                foreach (var permRes in permissionSetDict)
                {
                    if (permRes != null && permRes.Count > 0)
                    {
                        var result = false;
                        if (modId != 0)
                        {
                            result = permissionSetFromDb.ContainsValue(Entity);
                        }
                        else
                        {
                            result = permRes.ContainsKey(Entity);
                        }
                        if (!result)
                            return result;
                        else
                        {
                            var entityPresent = permRes.ContainsKey(Entity);
                            if (entityPresent)
                            {
                                var actions = permRes[Entity];
                                var valueResult = actions.Contains(Action);
                                return res = result = valueResult;
                            }
                        }
                    }
                }
            }
            else if (modules.IsCollectionValid())
            {
                return true;
            }
            return res;
        }

        public Dictionary<int, string> GetModule()
        {
            return modules;
        }

        private bool res { get; set; }

        public Dictionary<int, string> modules { get; set; }
        public long UserId { get; set; }
    }
}
