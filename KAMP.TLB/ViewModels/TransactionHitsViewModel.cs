﻿using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using KAMP.Core.FrameworkComponents;
using System.Text;
using System.Threading.Tasks;
using KAMP.TLB.BLL;

namespace KAMP.TLB.ViewModels
{
    public class TransactionHitsViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<TransactionHitsModel> _transactionHitsList;
        public ObservableCollection<TransactionHitsModel> TransactionHitsList
        {
            get { return _transactionHitsList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionHitsList, (x) => x.TransactionHitsList); }
        }

        private int _transactionTotalScore;
        public int TransactionTotalScore
        {
            get { return _transactionTotalScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionTotalScore, (x) => x.TransactionTotalScore); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public TransactionHitsViewModel(TransactionDetail selectedItemfrmGrid)
        {
            var bLL = new TLBBLL();
            TransactionHitsList = bLL.GetTransaction(selectedItemfrmGrid.Id); 
            TransactionTotalScore = (TransactionHitsList.Select(x => x.TransactionScore)).Sum(x => x);
        }
    }
}
