﻿using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    public class RelatedCasesViewModel : INotifyPropertyChanged
    {
        private int _cfId;
        public int CfId
        {
            get { return _cfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfId, (x) => x.CfId); }
        }

        private ObservableCollection<CaseDetails> _relatedCasesList; 
        public ObservableCollection<CaseDetails> RelatedCasesList
        {
            get { return _relatedCasesList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _relatedCasesList, (x) => x.RelatedCasesList); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
