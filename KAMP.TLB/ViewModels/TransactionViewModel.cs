﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using KAMP.TLB.Models;

namespace KAMP.TLB.ViewModels
{
    public class TransactionViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<TransactionDetail> _transactionList;
        public ObservableCollection<TransactionDetail> TransactionList
        {
            get { return _transactionList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionList, (x) => x.TransactionList); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
