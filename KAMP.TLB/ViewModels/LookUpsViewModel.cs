﻿using KAMP.Core.Repository.TLB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    public class LookUpsViewModel : INotifyPropertyChanged
    {
        ObservableCollection<LookUpGroups> _lookUpGroup;
        ObservableCollection<LookUp> _lookUps;
        LookUpGroups _selectedGroup;
         
        public ObservableCollection<LookUpGroups> LookUpGroupList
        {
            get { return _lookUpGroup; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lookUpGroup, (x) => x.LookUpGroupList); }
        }

        public LookUpGroups SelectedGroup
        {
            get { return _selectedGroup; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedGroup, (x) => x.SelectedGroup); }
        }

        public ObservableCollection<LookUp> LookUpsList
        {
            get { return _lookUps; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lookUps, (x) => x.LookUpsList); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
