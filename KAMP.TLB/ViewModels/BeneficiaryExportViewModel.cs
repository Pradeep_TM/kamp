﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.ComponentModel.DataAnnotations;

namespace KAMP.TLB.ViewModels
{
    public class BeneficiaryExportViewModel : INotifyPropertyChanged
    {
        private string _beneficiaries;
        [Display(Name = "Beneficiaries")]
        public string Beneficiaries
        {
            get { return _beneficiaries; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaries, (x) => x.Beneficiaries); }
        }

        private int _transactions;
        public int Transactions
        {
            get { return _transactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactions, (x) => x.Transactions); }
        }

        private decimal _totalAmount;
        [Display(Name = "Total Amount")]
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalAmount, (x) => x.TotalAmount); }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { PropertyChanged.HandleValueChange(this, value, ref _score, (x) => x.Score); }
        }

        private double _avgScore;
        [Display(Name = "Avg Score")]
        public double AvgScore
        {
            get { return _avgScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _avgScore, (x) => x.AvgScore); }
        }

        private string _lastTransaction;
        [Display(Name = "Last Transaction")]
        public string LastTransaction
        {
            get { return _lastTransaction; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lastTransaction, (x) => x.LastTransaction); }
        }

        private string _firstTransaction;
        [Display(Name = "First Transaction")]
        public string FirstTransaction
        {
            get { return _firstTransaction; }
            set { PropertyChanged.HandleValueChange(this, value, ref _firstTransaction, (x) => x.FirstTransaction); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
