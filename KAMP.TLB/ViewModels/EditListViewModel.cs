﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.TLB.Models;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;

namespace KAMP.TLB.ViewModels
{
    public class EditListViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Parameters> _parameters;

        public ObservableCollection<Parameters> Parameters
        {
            get { return _parameters; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameters, (x) => x.Parameters); }
        }   

        public event PropertyChangedEventHandler PropertyChanged;   
    }
}
