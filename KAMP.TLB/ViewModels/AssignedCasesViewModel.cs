﻿using KAMP.Core.FrameworkComponents.UserControls;
using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
using KAMP.TLB.BLL;
using KAMP.Core.Workflow.Helpers;
using KAMP.TLB.AppCode;

namespace KAMP.TLB.ViewModels
{
    public class AssignedCasesViewModel : INotifyPropertyChanged, IPageControlContract
    {
        private uint _assignedCasesCount;
        private ObservableCollection<CaseDetails> _listAssignedCasesCollection;
        private ObservableCollection<CaseDetails> _listAssignedFilteredCasesCollection;

        public AssignedCasesViewModel()
        {
            ListAssignCasesCollection = new ObservableCollection<CaseDetails>();
            ListAssignFilteredCasesCollection = new ObservableCollection<CaseDetails>();
        }

        public ObservableCollection<CaseDetails> ListAssignCasesCollection
        {
            get { return _listAssignedCasesCollection; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _listAssignedCasesCollection, x => x.ListAssignCasesCollection);
            }
        }
        public ObservableCollection<CaseDetails> ListAssignFilteredCasesCollection
        {
            get { return _listAssignedFilteredCasesCollection; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _listAssignedFilteredCasesCollection, x => x.ListAssignFilteredCasesCollection);
            }
        }

        #region IPageControlContract Members

        public uint GetTotalCount(object filterTag)
        {
            ObservableCollection<object> result = new ObservableCollection<object>();
            var filters = new CaseFilters();
            if (filterTag.IsNotNull())
                filters = filterTag as CaseFilters;
            if (filters.IsNull())
                filters = new CaseFilters();
            var bLL = new TLBBLL();
            var cases = bLL.GetPageSizeAfterFilters(true,filters.CategoryId,filters.userId,filters.stateId,filters.caseNumber);
            return (uint)cases;
        }

        public ObservableCollection<object> GetRecordsBy(uint startingIndex, uint numberOfRecords, object filterTag)
        {
            ObservableCollection<object> result = new ObservableCollection<object>();

            var filters = new CaseFilters();
            if (filterTag.IsNotNull())
               filters = filterTag as CaseFilters;
            var bLL= new TLBBLL();
            var cases = bLL.GetCasesByIndexandFilters(startingIndex, numberOfRecords, true, filters.CategoryId, filters.userId, filters.stateId, filters.caseNumber);

            foreach (var item in cases)
            {
                result.Add(item);
            }
            return result;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
