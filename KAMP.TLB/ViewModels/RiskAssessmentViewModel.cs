﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.Repository.Models;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.Models;
using System.Collections.ObjectModel;
using KAMP.Core.Repository.TLB;

namespace KAMP.TLB.ViewModels
{
    public class RiskAssessmentViewModel : INotifyPropertyChanged
    {
        private List<AssesmentType> _assessmentTypes;

        public List<AssesmentType> AssessmentTypes
        {  
            get { return _assessmentTypes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentTypes, (x) => x.AssessmentTypes); }
        }

        private int _assessmentTypeId;

        public int AssessmentTypeId
        {  
            get { return _assessmentTypeId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assessmentTypeId, (x) => x.AssessmentTypeId);
                if (IsCategorySelected.IsNotNull())
                {
                    IsCategorySelected();
                }
            }
        }

        private ObservableCollection<Assesments> _assessments;

        public ObservableCollection<Assesments> Assessments
        {
            get { return _assessments; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessments, (x) => x.Assessments); }
        }

        public Action IsCategorySelected;


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
