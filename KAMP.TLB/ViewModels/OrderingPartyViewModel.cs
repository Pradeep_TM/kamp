﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.ViewModels
{
    using Core.FrameworkComponents;
    using System.Collections.ObjectModel;
    using Models;
    public class OrderingPartyViewModel:INotifyPropertyChanged
    {
        private decimal _overallTotalAmount;

        public decimal OverallTotalAmount
        {
            get { return _overallTotalAmount; }
            set { PropertyChanged.HandleValueChange(this, value, ref _overallTotalAmount, (x) => x.OverallTotalAmount); }
        }

        private decimal _overallTotalTransactions;

        public decimal OverallTotalTransactions
        {
            get { return _overallTotalTransactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _overallTotalTransactions, (x) => x.OverallTotalTransactions); }
        }

        private decimal _overallTotalScore;

        public decimal OverallTotalScore
        {
            get { return _overallTotalScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _overallTotalScore, (x) => x.OverallTotalScore); }
        }

        private ObservableCollection<OrderingParty> _orderingParty;

        public ObservableCollection<OrderingParty> OrderingParty
        {
            get { return _orderingParty; } 
            set { PropertyChanged.HandleValueChange(this, value, ref _orderingParty, (x) => x.OrderingParty); }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
