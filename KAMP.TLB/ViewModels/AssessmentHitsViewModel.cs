﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.ViewModels
{
    public class AssessmentHitsViewModel : INotifyPropertyChanged
    {
        private int _assessmentId;

        public int AssessmentId
        {
            get { return _assessmentId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentId, (x) => x.AssessmentId); }
        }

        private string _parameter;

        public string Parameter
        {
            get { return _parameter; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameter, (x) => x.Parameter); }
        }

        private string _searchTerm;

        public string SearchTerm
        {
            get { return _searchTerm; }
            set { PropertyChanged.HandleValueChange(this, value, ref _searchTerm, (x) => x.SearchTerm); }
        }

        private int _occurrences;

        public int Occurrences
        {
            get { return _occurrences; }
            set { PropertyChanged.HandleValueChange(this, value, ref _occurrences, (x) => x.Occurrences); }
        }

        private int _totalOccurrences;

        public int TotalOccurrences
        {
            get { return _totalOccurrences; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalOccurrences, (x) => x.TotalOccurrences); }
        }

        private List<AssessmentHitByCategoryViewModel> _assessmentHitByCategoryViewModel;

        public List<AssessmentHitByCategoryViewModel> AssessmentHitByCategoryViewModel
        {
            get { return _assessmentHitByCategoryViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentHitByCategoryViewModel, (x) => x.AssessmentHitByCategoryViewModel); }
        }

        private int _selectedGroupId;

        public int SelectedGroupId
        {
            get { return _selectedGroupId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedGroupId, (x) => x.SelectedGroupId); }
        }
        
        

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
