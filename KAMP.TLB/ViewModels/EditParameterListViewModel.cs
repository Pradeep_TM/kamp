﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.TLB;
using System.Collections.ObjectModel;
using KAMP.TLB.Models;
using System.Windows.Input;

namespace KAMP.TLB.ViewModels
{
    public class EditParameterListViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _parameterGroupId;

        public int ParameterGroupId
        {
            get { return _parameterGroupId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _parameterGroupId, (x) => x.ParameterGroupId);
                if (IsCategorySelected.IsNotNull())
                {
                    IsCategorySelected();
                }
            }
        }

        private ObservableCollection<TLBParameterGroups> _parameterGroups;

        public ObservableCollection<TLBParameterGroups> ParameterGroups
        {
            get { return _parameterGroups; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterGroups, (x) => x.ParameterGroups); }
        }

        private ObservableCollection<RiskParameter> _riskParameters;

        public ObservableCollection<RiskParameter> RiskParamters
        {
            get { return _riskParameters; }
            set { PropertyChanged.HandleValueChange(this, value, ref _riskParameters, (x) => x.RiskParamters); }
        }

        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }
        private string _parameterValue;

        public string ParameterValue
        {
            get { return _parameterValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterValue, (x) => x.ParameterValue); }
        }


        public Action OnNewClick { get; set; }


        private ICommand _new;

        public ICommand New
        {
            get
            {

                if (_new == null)
                    _new = new DelegateCommand(() =>
                    {
                        if (OnNewClick.IsNotNull())
                            OnNewClick();
                    });
                return _new;
            }

        }
        public Action<int> OnSaveClick;
        private ICommand _save;
        public ICommand Save
        {
            get
            {

                if (_save == null)
                    _save = new DelegateCommand<int>((s) =>
                    {
                        if (OnSaveClick.IsNotNull())
                            OnSaveClick(s);
                    });
                return _save;
            }
        }

        public bool IsParameters { get; set; }
        public bool IsVisible { get; set; }

        public Action IsCategorySelected;

    }
}
