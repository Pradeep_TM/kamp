﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;

namespace KAMP.TLB.ViewModels
{ 
    public class TabControlViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private long _cfId;
        public long CFId
        {
            get { return _cfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfId, (x) => x.CFId); }
        }

        private ObservableCollection<ClientAccountViewModel> clientAccountViewModels;    
        public ObservableCollection<ClientAccountViewModel> ClientAccountViewModels
        {
            get { return clientAccountViewModels; }
            set { PropertyChanged.HandleValueChange(this, value, ref clientAccountViewModels, (x) => x.ClientAccountViewModels); }
        }

    }
}
