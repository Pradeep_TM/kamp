﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.Models;
using System.Collections.ObjectModel;
using KAMP.Core.FrameworkComponents.UserControls;
using KAMP.Core.Repository.TLB;
using KAMP.TLB.BLL;
using KAMP.Core.Repository;
using KAMP.Core.Repository.WF;
using KAMP.TLB.AppCode;

namespace KAMP.TLB.ViewModels
{
    public class AssignCasesTabViewModel : INotifyPropertyChanged
    {

        #region  Private Properties
        private UnassignedCasesViewModel _unassignedCasesViewModel;
        private AssignedCasesViewModel _assignedCasesViewModel;
       // private ObservableCollection<string> _caseNumberList;
        private ObservableCollection<string> _assignedCasesList;
        //private ObservableCollection<MasterEntity> _statusList;
        //private ObservableCollection<TLBUser> _userList;
       // private List<TLBUser> _userListforAnalystState;
        private List<TLBUser> _userStatusMappedList;
        //private ObservableCollection<LookUp> _caseCategory;
        private long _selecteduser;
        private int _selectedStatus;
        private string _selectedCaseNo;
        private LookUp _selectedCategory;
        private LookUp _selectedUnassignedCategory;
        #endregion

        #region Constructor
        public AssignCasesTabViewModel()
        {
            _unassignedCasesViewModel = new UnassignedCasesViewModel();
            _assignedCasesViewModel = new AssignedCasesViewModel();
            _assignedCasesList = new ObservableCollection<string>();
        }

        //private List<TLBUser> GetAllUsersForAnalystState(ObservableCollection<TLBUser> userList)
        //{
        //    var users = UserList.Where(x => x.StateIdList != null && x.StateIdList.Any(y => y == StatusList.Where(z => z.Name == "Analyst").
        //        FirstOrDefault().Code)).ToList();
        //    return users;
        #endregion

        #region Private Members
        private ObservableCollection<TLBUser> GetAllUsers()
        {
            var tLBBL = new TLBBLL();
            return tLBBL.GetUsers();
        }

        private ObservableCollection<LookUp> GetCaseCategory()
        {
            var tLBBL = new TLBBLL();
            return tLBBL.GetCaseCategory();
        }
        #endregion

        #region Public Properties
        public long SelectedUser
        {
            get { return _selecteduser; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selecteduser, (x) => x.SelectedUser);
                if (FilterSearch.IsNotNull() && !IsInitialLoad)
                    FilterSearch();
            }
        }

        public int SelectedStatus
        {
            get { return _selectedStatus; }
            set
            {
               PropertyChanged.HandleValueChange(this, value, ref _selectedStatus, (x) => x.SelectedStatus);
               if (FilterSearch.IsNotNull() && !IsInitialLoad)
                    FilterSearch();
            }
        }

        public string SelectedCaseNo
        {
            get { return _selectedCaseNo; }
            set
            {
               PropertyChanged.HandleValueChange(this, value, ref _selectedCaseNo, (x) => x.SelectedCaseNo);
               if (FilterSearch.IsNotNull() && !IsInitialLoad)
                    FilterSearch();
            }
        }

        public bool IsInitialLoad { get; set; }

        public LookUp SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                 PropertyChanged.HandleValueChange(this, value, ref _selectedCategory, (x) => x.SelectedCategory);
                 if (FilterSearch.IsNotNull() && !IsInitialLoad)
                    FilterSearch();
            }
        }

        public LookUp SelectedUnassignedCategory
        {
            get { return _selectedUnassignedCategory; }
            set
            {
               PropertyChanged.HandleValueChange(this, value, ref _selectedUnassignedCategory, (x) => x.SelectedUnassignedCategory);
                if (FilterSearchUnassigendCases.IsNotNull() && !IsInitialLoad)
                    FilterSearchUnassigendCases();
            }
        }

        public Action FilterSearchUnassigendCases;

        public Action FilterSearch;

        public List<TLBUser> UserStatusMappedList
        {
            get { return _userStatusMappedList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userStatusMappedList, x => x.UserStatusMappedList);
            }
        }

        public AssignedCasesViewModel AssignedCasesViewModel
        {
            get { return _assignedCasesViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assignedCasesViewModel, x => x.AssignedCasesViewModel);
            }
        }

        public UnassignedCasesViewModel UnassignedCasesViewModel
        {
            get { return _unassignedCasesViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _unassignedCasesViewModel, x => x.UnassignedCasesViewModel);
            }
        }

        //public ObservableCollection<LookUp> CaseCategory
        //{
        //    get { return _caseCategory; }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _caseCategory, x => x.CaseCategory);
        //    }
        //}

        public ObservableCollection<string> AssignedCasesList
        {
            get { return _assignedCasesList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assignedCasesList, x => x.AssignedCasesList);
            }
        }

        //public ObservableCollection<string> CaseNumbersList
        //{
        //    get { return _caseNumberList; }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _caseNumberList, x => x.CaseNumbersList);
        //    }
        //}

        //public ObservableCollection<MasterEntity> StatusList
        //{
        //    get { return _statusList; }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _statusList, x => x.StatusList);
        //    }
        //}

        //public ObservableCollection<TLBUser> UserList
        //{
        //    get { return _userList; }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _userList, x => x.UserList);
        //    }
        //}
        //public List<TLBUser> UserListforAnalystState
        //{
        //    get { return _userListforAnalystState; }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _userListforAnalystState, x => x.UserListforAnalystState);
        //    }
        //}

        
        #endregion

        #region Property Change Interface Implemantation
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

    }
}
