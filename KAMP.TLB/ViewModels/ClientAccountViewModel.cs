﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.ViewModels
{
    public class ClientAccountViewModel : INotifyPropertyChanged
    {
        private int _transactionId;
        public int TransactionId
        {
            get { return _transactionId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionId, (x) => x.TransactionId); }
        }

        private int _cfId;

        public int CfId
        {
            get { return _cfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfId, (x) => x.CfId); }
        }


        private string _account;

        public string Account
        {
            get { return _account; }
            set { PropertyChanged.HandleValueChange(this, value, ref _account, (x) => x.Account); }
        }

        private string _accountName;

        public string AccountName
        {
            get
            {
                return _accountName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _accountName, (x) => x.AccountName);
            }
        }

        private string _accountStatus;

        public string AccountStatus
        {
            get { return _accountStatus; }
            set { PropertyChanged.HandleValueChange(this, value, ref _accountStatus, (x) => x.AccountStatus); }
        }

        private string _address;
        public string Address
        {
            get { return _address; }
            set { PropertyChanged.HandleValueChange(this, value, ref _address, (x) => x.Address); }
        }

        private DateTime _closedDate;
        public DateTime ClosedDate
        {
            get { return _closedDate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _closedDate, (x) => x.ClosedDate); }
        }

        private long _transactions;
        public long Transactions
        {
            get { return _transactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactions, (x) => x.Transactions); }
        }

        private decimal _incomingUsd;
        public decimal IncomingUsd
        {
            get { return _incomingUsd; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _incomingUsd, (x) => x.IncomingUsd);
            }
        }

        private decimal _outgoingUsd;
        public decimal OutgoingUsd
        {
            get { return _outgoingUsd; }
            set { PropertyChanged.HandleValueChange(this, value, ref _outgoingUsd, (x) => x.OutgoingUsd); }
        }

        private decimal _bankTransfersUsd;
        public decimal BankTransfersUsd
        {
            get { return _bankTransfersUsd; }
            set { PropertyChanged.HandleValueChange(this, value, ref _bankTransfersUsd, (x) => x.BankTransfersUsd); }
        }

        private decimal _bankTransfersUsdActual;
        public decimal BankTransfersUsdActual
        {
            get { return _bankTransfersUsdActual; }
            set { PropertyChanged.HandleValueChange(this, value, ref _bankTransfersUsdActual, (x) => x.BankTransfersUsdActual); }
        }

        private long _transActual;
        public long TransActual
        {
            get { return _transActual; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transActual, (x) => x.TransActual); }
        }

        private decimal _totalIncomingUsd;
        public decimal TotalIncomingUsd
        {
            get { return _totalIncomingUsd; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _totalIncomingUsd, (x) => x.TotalIncomingUsd);
            }
        }

        private decimal _totalOutgoingUsd;
        public decimal TotalOutgoingUsd
        {
            get { return _totalOutgoingUsd; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _totalOutgoingUsd, (x) => x.TotalOutgoingUsd);
            }
        }

        private decimal _totalBankTransfersUsd;
        public decimal TotalBankTransfersUsd
        {
            get { return _totalOutgoingUsd; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _totalBankTransfersUsd, (x) => x.TotalBankTransfersUsd);
            }
        }

        private decimal _totalTransactions;
        public decimal TotalTransactions
        {
            get { return _totalTransactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalTransactions, (x) => x.TotalTransactions); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
