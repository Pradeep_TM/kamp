﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;

namespace KAMP.TLB.ViewModels
{
    public class DispositionExportViewModel : INotifyPropertyChanged
    {
        private bool _isRiskProfileChecked;
        public bool IsRiskProfileChecked
        {
            get { return _isRiskProfileChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isRiskProfileChecked, (x) => x.IsRiskProfileChecked); }
        }

        private bool _isMOFChecked;
        public bool IsMOFChecked
        {
            get { return _isMOFChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isMOFChecked, (x) => x.IsMOFChecked); }
        }
       
        private bool _isDueDillligenceChecked;
        public bool IsDueDillligenceChecked
        {
            get { return _isDueDillligenceChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isDueDillligenceChecked, (x) => x.IsDueDillligenceChecked); }
        }
      
        private bool _isESChecked;
        public bool IsESChecked
        {
            get { return _isESChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isESChecked, (x) => x.IsESChecked); }
        }

        public event PropertyChangedEventHandler PropertyChanged; 
    }
}
