﻿using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
using System.Windows;

namespace KAMP.TLB.ViewModels
{
    public class TLBHistoryViewModel : INotifyPropertyChanged
    {

        public TLBHistoryViewModel()
        {
            _historyCategoryList = new ObservableCollection<string>();
            _historyList = new ObservableCollection<HistoryModel>();
        } 

        private ObservableCollection<HistoryModel> _historyList;
        public ObservableCollection<HistoryModel> HistoryList
        {
            get { return _historyList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _historyList, (x) => x.HistoryList); }
        }

        private ObservableCollection<HistoryModel> _loginHistoryList;
        public ObservableCollection<HistoryModel> LoginHistoryList
        {
            get { return _loginHistoryList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _loginHistoryList, (x) => x.LoginHistoryList); }
        }

        private ObservableCollection<string> _historyCategoryList;
        public ObservableCollection<string> HistoryCategoryList
        {
            get { return _historyCategoryList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _historyCategoryList, (x) => x.HistoryCategoryList); }
        }

        private string _selectedCategory;
        public string SelectedCategory
        {
            get { return _selectedCategory; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedCategory, (x) => x.SelectedCategory); }
        }

        private bool _isLoginGridVisible;
        public bool IsLoginGridVisible
        {
            get { return _isLoginGridVisible; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isLoginGridVisible, (x) => x.IsLoginGridVisible); }
        }

        private List<string> _entityList;
        public List<string> EntityList
        {
            get { return _entityList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _entityList, (x) => x.EntityList); }
        }

        private List<string> _userList;
        public List<string> UserList
        {
            get { return _userList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userList, (x) => x.UserList); }
        }

        private string _selectedEntity;
        public string SelectedEntity
        {
            get { return _selectedEntity; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedEntity, (x) => x.SelectedEntity); }
        }

        private string _selectedUserName;
        public string SelectedUserName
        {
            get { return _selectedUserName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedUserName, (x) => x.SelectedUserName); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
