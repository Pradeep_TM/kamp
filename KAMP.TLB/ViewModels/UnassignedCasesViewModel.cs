﻿using KAMP.Core.FrameworkComponents.UserControls;
using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.BLL;
using KAMP.Core.Workflow.Helpers;
using KAMP.TLB.AppCode;

namespace KAMP.TLB.ViewModels
{
   public class UnassignedCasesViewModel : INotifyPropertyChanged, IPageControlContract
    {
       private uint _assignedCasesCount;
       private ObservableCollection<CaseDetails> _listUnAssignedCasesCollection;
       private ObservableCollection<CaseDetails> _listUnAssignedFilteredCasesCollection;

       public UnassignedCasesViewModel()
       {
           _listUnAssignedCasesCollection = new ObservableCollection<CaseDetails>();
           _listUnAssignedFilteredCasesCollection = new ObservableCollection<CaseDetails>();
       }

       public ObservableCollection<CaseDetails> ListUnAssignCasesCollection
       {
           get 
           {
               return _listUnAssignedCasesCollection; 
           }
           set
           {
               PropertyChanged.HandleValueChange(this, value, ref _listUnAssignedCasesCollection, x => x.ListUnAssignCasesCollection);
           }
       }
       public ObservableCollection<CaseDetails> ListUnAssignFilteredCasesCollection
       {
           get
           {
               return _listUnAssignedFilteredCasesCollection; 
           }
           set
           {
               PropertyChanged.HandleValueChange(this, value, ref _listUnAssignedFilteredCasesCollection, x => x.ListUnAssignFilteredCasesCollection);
           }
       }

       #region IPageControlContract Members

        public uint GetTotalCount(object filterTag)
        {
            ObservableCollection<object> result = new ObservableCollection<object>();
            var filters = new CaseFilters();
            if (filterTag.IsNotNull())
                filters = filterTag as CaseFilters;
            if (filters.IsNull())
                filters = new CaseFilters();
            var bLL = new TLBBLL();
            var cases = bLL.GetPageSizeAfterFilters(false, filters.CategoryId, filters.userId, filters.stateId, filters.caseNumber);
            return (uint)cases;
        }

        public ObservableCollection<object> GetRecordsBy(uint startingIndex, uint numberOfRecords, object filterTag)
        {
            ObservableCollection<object> result = new ObservableCollection<object>();
            var filters = new CaseFilters();
            if (filterTag.IsNotNull())
                filters = filterTag as CaseFilters;
            var bLL = new TLBBLL();
            var cases = bLL.GetCasesByIndexandFilters(startingIndex, numberOfRecords,false, filters.CategoryId, filters.userId, filters.stateId, filters.caseNumber);

            foreach (var item in cases)
            {
                result.Add(item);
            }
            return result;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

       
    }
}
