﻿using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.ViewModels
{
    public class AssessmentListViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Parameters> _parameters;

        public ObservableCollection<Parameters> Parameters
        {
            get { return _parameters; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameters, (x) => x.Parameters); }
        }

        private ObservableCollection<Parameters> _selectedParameters;

        public ObservableCollection<Parameters> SelectedParameters
        {
            get { return _selectedParameters; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedParameters, (x) => x.SelectedParameters); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
