﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.ViewModels
{
    public class AssessmentHitByCategoryViewModel : INotifyPropertyChanged
    {

        private int _parameterGroupId;

        public int ParameterGroupId
        {
            get { return _parameterGroupId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterGroupId, (x) => x.ParameterGroupId); }
        }

        private string _groupName;

        public string GroupName
        {
            get { return _groupName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _groupName, (x) => x.GroupName); }
        }



        public event PropertyChangedEventHandler PropertyChanged;
    }
}
