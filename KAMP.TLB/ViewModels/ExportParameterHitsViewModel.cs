﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.ViewModels
{
    public class ExportParameterHitsViewModel : INotifyPropertyChanged
    {
        private int _transactionId;

        public int TransactionId
        {
            get { return _transactionId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionId, (x) => x.TransactionId); }
        }

        private string _actualValue;

        public string ActualValue
        {
            get { return _actualValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _actualValue, (x) => x.ActualValue); }  
        }

        private string _paramValue;  

        public string ParamValue
        {
            get { return _paramValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _paramValue, (x) => x.ParamValue); }
        }

        private string _field;

        public string Field
        {
            get { return _field; }
            set { PropertyChanged.HandleValueChange(this, value, ref _field, (x) => x.Field); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
