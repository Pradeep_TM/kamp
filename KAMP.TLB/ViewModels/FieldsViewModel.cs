﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.ViewModels
{
    public class FieldsViewModel : INotifyPropertyChanged
    {

        private string _fieldName;

        public string FieldName
        {
            get { return _fieldName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fieldName, (x) => x.FieldName); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }  
}
