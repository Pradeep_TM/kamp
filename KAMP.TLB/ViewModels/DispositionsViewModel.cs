﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace KAMP.TLB.ViewModels
{
    public class DispositionsViewModel : INotifyPropertyChanged
    {
        public int CfId { get; set; }

        private bool _isTabEnabled = true;
        public bool IsTabEnabled
        {
            get { return _isTabEnabled; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isTabEnabled, (x) => x.IsTabEnabled); }
        }

        private string _execSummary;
        public string ExecSummary
        {
            get { return _execSummary; }
            set { PropertyChanged.HandleValueChange(this, value, ref _execSummary, (x) => x.ExecSummary); }
        }
        //private FlowDocument _execSummary;
        //public FlowDocument ExecSummary
        //{
        //    get { return _execSummary; }
        //    set { PropertyChanged.HandleValueChange(this, value, ref _execSummary, (x) => x.ExecSummary); }
        //}

        private string _duediligence;
        public string Duediligence
        {
            get { return _duediligence; }
            set { PropertyChanged.HandleValueChange(this, value, ref _duediligence, (x) => x.Duediligence); }
        }

        private string _mamorandumofFact;
        public string MamorandumofFact
        {
            get { return _mamorandumofFact; }
            set { PropertyChanged.HandleValueChange(this, value, ref _mamorandumofFact, (x) => x.MamorandumofFact); }
        }

        private string _riskProfile;
        public string RiskProfile
        {
            get { return _riskProfile; }
            set { PropertyChanged.HandleValueChange(this, value, ref _riskProfile, (x) => x.RiskProfile); }
        }

        private string _searchParameters;
        public string SearchParameters
        {
            get { return _searchParameters; }
            set { PropertyChanged.HandleValueChange(this, value, ref _searchParameters, (x) => x.SearchParameters); }
        }
        //private string _searchRiskProfile;
        //public string SearchRiskProfileInput
        //{
        //    get { return _searchRiskProfile; }
        //    set { PropertyChanged.HandleValueChange(this, value, ref _searchRiskProfile, (x) => x.SearchRiskProfileInput); }
        //}
        //private string _searchDueDiligence;
        //public string SearchDueDiligenceInput
        //{
        //    get { return _searchDueDiligence; }
        //    set { PropertyChanged.HandleValueChange(this, value, ref _searchDueDiligence, (x) => x.SearchDueDiligenceInput); }
        //}
        //private string _searchMemorandumOfFact;
        //public string SearchMemorandumOfFactInput
        //{
        //    get { return _searchMemorandumOfFact; }
        //    set { PropertyChanged.HandleValueChange(this, value, ref _searchMemorandumOfFact, (x) => x.SearchMemorandumOfFactInput); }
        //}
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
