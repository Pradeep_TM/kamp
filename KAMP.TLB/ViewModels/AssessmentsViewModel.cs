﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.TLB.Models;
using KAMP.Core.Repository.TLB;
using System.Windows.Input;

namespace KAMP.TLB.ViewModels
{
    public class AssessmentsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _totalOccurrences;

        public int TotalOccurrences
        {
            get { return _totalOccurrences; }   
            set { PropertyChanged.HandleValueChange(this, value, ref _totalOccurrences, (x) => x.TotalOccurrences); }
        }

        private int _totalOverallScore;

        public int TotalOverallScore
        {
            get { return _totalOverallScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalOverallScore, (x) => x.TotalOverallScore); }
        }

        private ObservableCollection<Assessment> _assessment;

        public ObservableCollection<Assessment> Assessment
        {
            get { return _assessment; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessment, (x) => x.Assessment); }
        }
       
    }
}
