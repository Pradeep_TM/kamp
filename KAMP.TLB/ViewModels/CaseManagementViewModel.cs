﻿using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using KAMP.Core.FrameworkComponents;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents.UserControls;
using KAMP.TLB.BLL;
using System.Windows.Input;
using KAMP.Core.Workflow.Helpers;
using KAMP.TLB.AppCode;

namespace KAMP.TLB.ViewModels
{
    public class CaseManagementViewModel : INotifyPropertyChanged, IPageControlContract
    {
        public uint _casesCount { get; set; }
        public Action<string> OpenWorkflowProcess;
        public CaseManagementViewModel()
        {
            //ListCases = new ObservableCollection<CaseDetails>();
        }


        private CaseDetails _dGSelectedItem;
        public CaseDetails DGSelectedItem
        {
            get
            {
                return _dGSelectedItem;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _dGSelectedItem, x => x.DGSelectedItem);
            }
        }

        private ICommand _openWorkflow;

        public ICommand OpenWorkflow
        {
            get
            {
                if (_openWorkflow.IsNull())
                {
                    _openWorkflow = new DelegateCommand<string>(x =>
                    {
                        var caseNo = x.ToString();
                        if (OpenWorkflow.IsNotNull())
                            OpenWorkflowProcess(caseNo);

                    });
                }
                return _openWorkflow;
            }
            set { _openWorkflow = value; }
        }


        // private ObservableCollection<CaseDetails> _listCases;
        //public ObservableCollection<CaseDetails> ListCases
        //{
        //    get
        //    {
        //        return _listCases;
        //    }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _listCases, x => x.ListCases);
        //    }
        //}


        #region IPageControlContract Members

        public uint GetTotalCount( object filterTag)
        {
            ObservableCollection<object> result = new ObservableCollection<object>();
            var filters = new CaseFilters();
            if (filterTag.IsNotNull())
                filters = filterTag as CaseFilters;
            if (filters.IsNull())
                filters = new CaseFilters();
            var bLL = new TLBBLL();
            var cases = bLL.GetPageSizeAfterFilters(null, filters.CategoryId, filters.userId , filters.stateId, filters.caseNumber,true);
            return (uint)cases;
        }

        public ObservableCollection<object> GetRecordsBy(uint startingIndex, uint numberOfRecords, object filterTag)
        {
            ObservableCollection<object> result = new ObservableCollection<object>();
            var filters = new CaseFilters();
            if (filterTag.IsNotNull())
                filters = filterTag as CaseFilters;
            if (filters.IsNull())
                filters = new CaseFilters();
            var bLL = new TLBBLL();
            var cases = bLL.GetCasesByIndexandFilters(startingIndex, numberOfRecords, null , filters.CategoryId, filters.userId, filters.stateId, filters.caseNumber,true);

            foreach (var item in cases)
            {
                result.Add(item);
            }
            return result;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;


    }
}
