﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
using KAMP.TLB.Models;

namespace KAMP.TLB.ViewModels
{
   public class SearchDispositionsViewModel:INotifyPropertyChanged 
    {
       public SearchDispositionsViewModel()
       {
           _searchDispositionsModel = new SearchDispositionsModel();
       }
       private SearchDispositionsModel _searchDispositionsModel;
       public SearchDispositionsModel SearchDispositionsModel
        {
            get { return _searchDispositionsModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _searchDispositionsModel, (x) => x.SearchDispositionsModel); }
        }
      
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
