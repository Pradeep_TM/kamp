﻿using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;

namespace KAMP.TLB.ViewModels
{
    public class CaseHistoryViewModel : INotifyPropertyChanged
    {

        private ObservableCollection<HistoryModel> _caseHistoryList;
        public ObservableCollection<HistoryModel> CaseHistoryList
        {
            get { return _caseHistoryList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseHistoryList, (x) => x.CaseHistoryList); }
        }

        private ObservableCollection<TLBUser> _userList; 
        public ObservableCollection<TLBUser> UserList
        {
            get { return _userList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userList, (x) => x.UserList); }
        }

        private int _cFID;
        public int CFID
        {
            get { return _cFID; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cFID, (x) => x.CFID); }
        }

        private string _selectedUser;
        public string SelectedUser
        {
            get { return _selectedUser; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedUser, (x) => x.SelectedUser); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
