﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Windows.Input;

namespace KAMP.TLB.ViewModels
{
    public class ChooseFieldsViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<FieldsViewModel> _fieldsViewModel;
        public ObservableCollection<FieldsViewModel> FieldsViewModel
        {
            get { return _fieldsViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fieldsViewModel, (x) => x.FieldsViewModel); }
        }

        private int _parameterGroupId;

        public int ParameterGroupId
        {
            get { return _parameterGroupId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterGroupId, (x) => x.ParameterGroupId); }
        }
        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        private string _field;

        public string Field
        {
            get { return _field; }
            set { PropertyChanged.HandleValueChange(this, value, ref _field, (x) => x.Field); }
        }

        private int _assessmentId;

        public int AssessmentId
        {
            get { return _assessmentId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentId, (x) => x.AssessmentId); }
        }


        public Action<string> OnSaveClick;
        private ICommand _save;
        public ICommand Save
        {
            get
            {

                if (_save == null)
                    _save = new DelegateCommand<string>((s) =>
                    {
                        if (OnSaveClick.IsNotNull())
                            OnSaveClick(s);
                    });
                return _save;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
