﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
namespace KAMP.TLB.ViewModels
{
    public class AssessmentFilterViewModel : INotifyPropertyChanged
    {
        private int _assessmentId;

        public int AssessmentId
        {
            get { return _assessmentId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentId, (x) => x.AssessmentId); }
        }  

        private ObservableCollection<AvailableComponent> _availableComponent;

        public ObservableCollection<AvailableComponent> AvailableComponent
        {
            get { return _availableComponent; }
            set { PropertyChanged.HandleValueChange(this, value, ref _availableComponent, (x) => x.AvailableComponent); }
        }

        private ObservableCollection<AvailableComponent> _selectedComponent;

        public ObservableCollection<AvailableComponent> SelectedComponent
        {
            get { return _selectedComponent; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedComponent, (x) => x.SelectedComponent); }
        }

        public Action OnNewClick { get; set; }


        private ICommand _new;

        public ICommand New
        {
            get
            {

                if (_new == null)
                    _new = new DelegateCommand(() =>
                    {
                        if (OnNewClick.IsNotNull())
                            OnNewClick();
                    });
                return _new;
            }

        }
       
        //public Action OnCloseClick { get; set; }


        //private ICommand _close;

        //public ICommand Close
        //{
        //    get
        //    {

        //        if (_close == null)
        //            _close = new DelegateCommand(() =>
        //            {
        //                if (OnCloseClick.IsNotNull())
        //                    OnCloseClick();
        //            });
        //        return _close;
        //    }

        //}




        public event PropertyChangedEventHandler PropertyChanged;
    }
}
