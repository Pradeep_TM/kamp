﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.TLB.Models;
    using System.ComponentModel;
   public class RelatedPartiesViewModel : INotifyPropertyChanged
    {
        private int _cfId;
        public int CfId
        {
            get { return _cfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfId, (x) => x.CfId); }
        }

        private ObservableCollection<RelatedParties> _relatedPartiesList;
        public ObservableCollection<RelatedParties> RelatedPartiesList
        { 
            get { return _relatedPartiesList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _relatedPartiesList, (x) => x.RelatedPartiesList); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
