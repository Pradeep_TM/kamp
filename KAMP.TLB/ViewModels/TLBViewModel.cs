﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents.ViewModels;
using KAMP.Core.FrameworkComponents;
using System.ComponentModel;
using KAMP.TLB.UserControls;
using System.Collections.ObjectModel;
using KAMP.TLB.BLL;
using KAMP.TLB.AppCode;
using KAMP.TLB.Models;
using KAMP.Core.Repository;
using KAMP.Core.Repository.TLB;

namespace KAMP.TLB.ViewModels
{
    using KAMP.Core.Workflow;
    using KAMP.Core.Workflow.UserControls;
    using KAMP.Core.Workflow.ViewModels;
    using KAMP.TLB.BLL;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows;
    public class TLBViewModel : INotifyPropertyChanged
    {
        #region PrivateProperties
        private NavigationControlViewModel _navigationControlViewModel;
        private CaseManagementViewModel _caseManagementViewModel;
        private AssignCasesTabViewModel _assignCasesTabViewModel;
        private TLBHistoryViewModel _tLBHistoryViewModel;
        private HeaderViewModel _headerViewModel;
        private ObservableCollection<string> _caseNumberList;
        private ObservableCollection<TLBUser> _userList;
        // private static ObservableCollection<CaseDetails> _caseList;
        private ObservableCollection<MasterEntity> _statusList;
        private ObservableCollection<LookUp> _caseCategoryList;
        private LookUpsViewModel _lookUpViewModel;
        #endregion

        public TLBViewModel()
        {
            new BootStrapper().Configure();
            _navigationControlViewModel = new NavigationControlViewModel();
            _headerViewModel = new HeaderViewModel();

            _assignCasesTabViewModel = new ViewModels.AssignCasesTabViewModel();
            _caseManagementViewModel = new ViewModels.CaseManagementViewModel();
            _tLBHistoryViewModel = new ViewModels.TLBHistoryViewModel();
            _lookUpViewModel = new LookUpsViewModel();

            PuplulateViewModels();
        }
        #region PublicProperties



        public LookUpsViewModel LookUpViewModel
        {
            get { return _lookUpViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lookUpViewModel, x => x.LookUpViewModel);
            }
        }

        public CaseManagementViewModel CaseManagementViewModel
        {
            get { return _caseManagementViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseManagementViewModel, x => x.CaseManagementViewModel);
            }
        }

        public TLBHistoryViewModel TLBHistoryViewModel
        {
            get { return _tLBHistoryViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _tLBHistoryViewModel, x => x.TLBHistoryViewModel);
            }
        }

        public static ObservableCollection<CaseDetails> CaseList;

        public ObservableCollection<LookUp> CaseCategoryList
        {
            get { return _caseCategoryList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseCategoryList, x => x.CaseCategoryList);
            }
        }

        public ObservableCollection<MasterEntity> StatusList
        {
            get { return _statusList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _statusList, x => x.StatusList);
            }
        }

        public ObservableCollection<TLBUser> UserList
        {
            get { return _userList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userList, x => x.UserList);
            }
        }

        public ObservableCollection<string> CaseNumbersList
        {
            get { return _caseNumberList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseNumberList, x => x.CaseNumbersList);
            }
        }

        public HeaderViewModel HeaderViewModel
        {
            get
            {
                return _headerViewModel;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _headerViewModel, x => x.HeaderViewModel);
            }
        }

        public NavigationControlViewModel NavigationControlViewModel
        {
            get
            {
                return _navigationControlViewModel;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _navigationControlViewModel, x => x.NavigationControlViewModel);
            }
        }

        public AssignCasesTabViewModel AssignCasesTabViewModel
        {
            get
            {
                return _assignCasesTabViewModel;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assignCasesTabViewModel, x => x.AssignCasesTabViewModel);
            }
        }

        #endregion



        private void PuplulateViewModels()
        {
            var bLL = new TLBBLL();
            _statusList = bLL.GetAllStatus();
            _userList = bLL.GetUsers();
            _caseCategoryList = bLL.GetCaseCategory();
            CaseList = bLL.GetAllCases();

            NavigationControlViewModel = CreateNavigationControlViewModel();
            if (AssignCasesTabViewModel.IsNull())
                AssignCasesTabViewModel = new AssignCasesTabViewModel();
        }

        public void UpdateCaseRelatedCollection()
        {
            var bLL = new TLBBLL();
            CaseList = new ObservableCollection<CaseDetails>();
            CaseList = bLL.GetAllCases();
            UserList = bLL.GetUsers();
            _caseNumberList = new ObservableCollection<string>();
            var distinctCases = CaseList.Select(x => x.CaseNumber).Distinct().ToList();
            distinctCases.ForEach(_caseNumberList.Add);
            _caseNumberList.Insert(0, TLBConstants.DefaultDDLValue);
        }

        private NavigationControlViewModel CreateNavigationControlViewModel()
        {
            var caseManagementTab = new List<NavigationItemModel>();
            caseManagementTab.Add(new NavigationItemModel { UserControlName = "Assign Cases", ImageUrl = @"../Assets/Images/assigncases.png", EntityType = "TLBCaseFile" });
            caseManagementTab.Add(new NavigationItemModel { UserControlName = "Case Management", ImageUrl = @"../Assets/Images/casemanagement.png", EntityType = "TLBCaseFile" });
            //caseManagementTab.Add(new NavigationItemModel { UserControlName = "Resources", ImageUrl = @"../Assets/Images/resources.png" });
            //TODO:Delete this line after evrything works.
            //caseManagementTab.Add(new NavigationItemModel { UserControlName = "Case File Card", ImageUrl = @"../Assets/Images/resources.png" });

            var riskAsessmentTab = new List<NavigationItemModel>();
            riskAsessmentTab.Add(new NavigationItemModel { UserControlName = "Risk Assessments", ImageUrl = @"../Assets/Images/assessments.png", EntityType = "Assesments" });
            riskAsessmentTab.Add(new NavigationItemModel { UserControlName = "Risk Assessment Parameters", ImageUrl = @"../Assets/Images/parameters.png", EntityType = "TLBParameters" });


            var reportingTab = new List<NavigationItemModel>();
            reportingTab.Add(new NavigationItemModel { UserControlName = "Report", ImageUrl = @"../Assets/Images/analyst.png", EntityType = "CaseAgingReportDetails" });
            //reportingTab.Add(new NavigationItemModel { UserControlName = "Management", ImageUrl = @"../Assets/Images/management.png" });
            //reportingTab.Add(new NavigationItemModel { UserControlName = "System", ImageUrl = @"../Assets/Images/system.png" });
            //reportingTab.Add(new NavigationItemModel { UserControlName = "Override Routing", ImageUrl = @"../Assets/Images/overriderouting.png" });

            var systemManagementTab = new List<NavigationItemModel>();
            systemManagementTab.Add(new NavigationItemModel { UserControlName = "History", ImageUrl = @"../Assets/Images/history.png" , EntityType="LogDetail"});
            systemManagementTab.Add(new NavigationItemModel { UserControlName = "LookUps", ImageUrl = @"../Assets/Images/lookups.png", EntityType = "LookUp" });
            systemManagementTab.Add(new NavigationItemModel { UserControlName = "Audit Configuration", ImageUrl = @"../Assets/Images/wrench.png", EntityType = "ConfigurationTable" });

            var worklfowTab = new List<NavigationItemModel>();
            worklfowTab.Add(new NavigationItemModel() { UserControlName = "Manage Workflow", ImageUrl = @"../Assets/Images/CreateWorkflow.png", EntityType = "WFDefinition" });
            worklfowTab.Add(new NavigationItemModel() { UserControlName = "Manage State", ImageUrl = @"../Assets/Images/State.png", EntityType = "State" });
            worklfowTab.Add(new NavigationItemModel() { UserControlName = "Workflow Monitoring", ImageUrl = @"../Assets/Images/monitoring.png", EntityType = "Assignment" });

            NavigationControlViewModel.PinImage = @"../Assets/Images/pintlb.png";
            NavigationControlViewModel.UnPinImage = @"../Assets/Images/unpintlb.png";
            NavigationControlViewModel.ArrowLeftNavigation = @"../Assets/Images/arrowlefttlb.png";
            NavigationControlViewModel.DarkThemeColor = "#FF223E4A";
            NavigationControlViewModel.ContentBackgroundColor = "#FF83ADBF";
            NavigationControlViewModel.LightThemeColor = "#FF365F70";
            NavigationControlViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "Case Management", Items = caseManagementTab });
            NavigationControlViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "Risk Assessment", Items = riskAsessmentTab });
            NavigationControlViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "Reporting Service", Items = reportingTab });
            NavigationControlViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "System Management", Items = systemManagementTab });
            NavigationControlViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "Workflow", Items = worklfowTab });
            return NavigationControlViewModel;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public Grid wfProcessGrid { get; set; }
    }
}
