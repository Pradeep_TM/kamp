﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
namespace KAMP.TLB.ViewModels
{
    public class HeaderViewModel : INotifyPropertyChanged
    {
        private string _imgModule;
        private string _lightThemeColor;
        private string _darkThemeColor;
        private string _moduleName;
        private string _currentContent;
        private string _contentBackgroundColor;
        
        public HeaderViewModel()
        {
            ModuleName = "Transaction Look Back";
            ImgModule = "../Assets/Images/tlbicon.png";
            _darkThemeColor = "#FF223E4A";
            _lightThemeColor = "#FF365F70";
            _contentBackgroundColor = "#FF83ADBF";
        }

        public string ContentBackgroundColor
        {
            get { return _contentBackgroundColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _contentBackgroundColor, x => x.ContentBackgroundColor);
            }
        }

        public string DarkThemeColor
        {
            get { return _darkThemeColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _darkThemeColor, x => x.DarkThemeColor);
            }
        }

        public string LightThemeColor
        {
            get { return _lightThemeColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lightThemeColor, x => x.LightThemeColor);
            }
        }

        public string CurrentContent
        {
            get { return _currentContent; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _currentContent, x => x.CurrentContent);
            }
        }

        public string ModuleName
        {
            get { return _moduleName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleName, x => x.ModuleName);
            }
        }

        public string ImgModule
        {
            get { return _imgModule; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _imgModule, x => x.ImgModule);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
