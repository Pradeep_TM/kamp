﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.Models;
using System.Collections.ObjectModel;

namespace KAMP.TLB.ViewModels
{
    public class BeneficiariesViewModel : INotifyPropertyChanged
    {
        private decimal _overallTotalAmount;

        public decimal OverallTotalAmount
        {
            get { return _overallTotalAmount; }
            set { PropertyChanged.HandleValueChange(this, value, ref _overallTotalAmount, (x) => x.OverallTotalAmount); }
        }

        private decimal _overallTotalTransactions;

        public decimal OverallTotalTransactions
        {
            get { return _overallTotalTransactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _overallTotalTransactions, (x) => x.OverallTotalTransactions); }
        }

        private decimal _overallTotalScore;

        public decimal OverallTotalScore
        {
            get { return _overallTotalScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _overallTotalScore, (x) => x.OverallTotalScore); }
        }

        private ObservableCollection<Beneficiary> _beneficiary;

        public ObservableCollection<Beneficiary> Beneficiary
        {
            get { return _beneficiary; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiary, (x) => x.Beneficiary); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
