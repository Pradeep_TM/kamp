﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.Models;
using System.Collections.ObjectModel;
using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using KAMP.Core.FrameworkComponents.ViewModels;
using KAMP.Core.Common.ViewModels;
using System.Windows.Input;
using System.Windows;
namespace KAMP.TLB.ViewModels
{
    public class CaseFileCardViewModel : INotifyPropertyChanged
    {
        public Action<string> OpenWorkflowProcess;
        public CaseFileCardViewModel(CaseDetails selectedItemfrmGrid)
        {
            DGSelectedItem = selectedItemfrmGrid;
            _transactionViewModel = new TransactionViewModel();
            _dispositionsViewModel = new DispositionsViewModel();
            _relatedCasesViewModel = new RelatedCasesViewModel();
            _caseHistoryViewModel = new CaseHistoryViewModel();
        }
        private CaseHistoryViewModel _caseHistoryViewModel;
        public CaseHistoryViewModel CaseHistoryViewModel
        {
            get { return _caseHistoryViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseHistoryViewModel, x => x.CaseHistoryViewModel);
            }
        }

        private CaseDetails _dGSelectedItem;
        public CaseDetails DGSelectedItem
        {
            get
            {
                return _dGSelectedItem;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _dGSelectedItem, x => x.DGSelectedItem);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<ClientAccountViewModel> clientAccountViewModel;
        public ObservableCollection<ClientAccountViewModel> ClientAccountViewModel
        {
            get { return clientAccountViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref clientAccountViewModel, (x) => x.ClientAccountViewModel); }
        }

        private AssessmentsViewModel assessmentsViewModel;
        public AssessmentsViewModel AssessmentsViewModel
        {
            get { return assessmentsViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref assessmentsViewModel, (x) => x.AssessmentsViewModel); }
        }

        private BeneficiariesViewModel beneficiariesViewModel;
        public BeneficiariesViewModel BeneficiariesViewModel
        {
            get { return beneficiariesViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref beneficiariesViewModel, (x) => x.BeneficiariesViewModel); }
        }


        private OrderingPartyViewModel _orderingPartyViewModel;
        public OrderingPartyViewModel OrderingPartyViewModel
        {
            get { return _orderingPartyViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _orderingPartyViewModel, (x) => x.OrderingPartyViewModel); }
        }
        private TransactionViewModel _transactionViewModel;
        public TransactionViewModel TransactionViewModel
        {
            get { return _transactionViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionViewModel, (x) => x.TransactionViewModel); }
        }

        private FileLogsViewModel _fileLogViewModel;
        public FileLogsViewModel FileLogViewModel
        {
            get { return _fileLogViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fileLogViewModel, (x) => x.FileLogViewModel); }
        }

        private DispositionsViewModel _dispositionsViewModel;
        public DispositionsViewModel DispositionsViewModel
        {
            get { return _dispositionsViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _dispositionsViewModel, (x) => x.DispositionsViewModel); }
        }

        private RelatedCasesViewModel _relatedCasesViewModel;
        public RelatedCasesViewModel RelatedCasesViewModel
        {
            get { return _relatedCasesViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _relatedCasesViewModel, (x) => x.RelatedCasesViewModel); }
        }

        private CaseDetails selectedItemfrmGrid;

        private ICommand _openWorkflow;

        public ICommand OpenWorkflow
        {
            get
            {
                if (_openWorkflow.IsNull())
                {
                    _openWorkflow = new DelegateCommand<string>(x =>
                    {
                        var caseNo = x.ToString();
                        if (OpenWorkflowProcess.IsNotNull())
                            OpenWorkflowProcess(caseNo);

                    });
                }
                return _openWorkflow;
            }
        }
        public bool IsAssignedUser()
        {
            var context = Application.Current.Properties["Context"] as Context;
            if (DGSelectedItem.User.UserId == context.User.Id && (DGSelectedItem.IsOpen.HasValue && DGSelectedItem.IsOpen.Value == true))
                return true;
            return false;
        }
        public bool AssignedUser
        {
            get
            {
                return IsAssignedUser();
            }
        }
    }
}
