﻿using AutoMapper;
using KAMP.Core.Repository.TLB;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.WF;
using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.ViewModels;
using KAMP.TLB.Models;
using KAMP.Core.Repository.Models.TLB;
using KAMP.Core.Repository.AuditLog;
namespace KAMP.TLB.AppCode
{

    public class TLBMapperProfile : Profile
    {
        protected override void Configure()
        {
            MapViewModelToEntityModel();
            MapEntityModelToViewModel();

            base.Configure();
        }

        private void MapEntityModelToViewModel()
        {
            Mapper.CreateMap<TLBCaseFile, CaseDetails>()
                .ForMember(d => d.CaseName, s => s.MapFrom(x => x.Name))
                .ForMember(d => d.AverageScore, s => s.MapFrom(x => x.TransactionScore / x.TransactionCount))
                .ForMember(d => d.CreateDate, s => s.MapFrom(x => x.CreatedDate))
                //.ForMember(d => d.OpenDate, s => s.MapFrom(x => x.OpenDate))
                //.ForMember(d => d.CloseDate, s => s.MapFrom(x => x.CloseDate))
                //.ForMember(d => d.Analyst, s => s.MapFrom(x => x.LAssesmentTypeAnalyst))
                //.ForMember(d => d.HighestStatus, s => s.MapFrom(x => GetHighestStatus(x.HighStatusId)))
                //.ForMember(d => d.DaysInQueue, s => s.MapFrom(x => GetDaysInQueue(x.StatusDate)))
                .ForMember(d => d.Category, s => s.MapFrom(x => x.LookUp))
                //.ForMember(d => d.State, s => s.MapFrom(x => GetStatus(x)))
                .ForMember(d => d.TransactionCount, s => s.MapFrom(x => x.TransactionCount))
                //.ForMember(d => d.User, s => s.MapFrom(x => GetUser(x.UserId)))
                .ForMember(d => d.CfId, s => s.MapFrom(x => x.CfId))
                .ForMember(d => d.CaseScore, s => s.MapFrom(x => x.TransactionScore))
                .ForMember(d => d.CaseNumber, s => s.MapFrom(x => x.CaseNo)).IgnoreAllNonExisting();

            Mapper.CreateMap<User, TLBUser>()
                .ForMember(d => d.UserId, s => s.MapFrom(x => x.UserId))
                .ForMember(d => d.UserName, s => s.MapFrom(x => x.UserName))
                .ForMember(d => d.StateIdList, s => s.MapFrom(x => GetAllStateIds(x.UserInGroup)));

            Mapper.CreateMap<ClientAccounts, ClientAccountViewModel>()
                .ForMember(d => d.Account, s => s.MapFrom(p => p.Account))
                .ForMember(d => d.AccountName, s => s.MapFrom(p => p.AccountName))
                .ForMember(d => d.Address, s => s.MapFrom(p => p.Address))
                .ForMember(d => d.AccountStatus, s => s.MapFrom(p => p.AccountStatus))
                .ForMember(d => d.ClosedDate, s => s.MapFrom(p => p.AccountCLSDate))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<dynamic, TransactionDetail>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<TLBTransNorm, TransactionDetail>()
                .ForMember(d => d.Id, s => s.MapFrom(p => p.Id))
                .ForMember(d => d.BanktoBeneInfo, s => s.MapFrom(p => p.BanktoBeneInfo))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ScoresTransactions, TransactionHitsModel>()
                .ForMember(d => d.Id, s => s.MapFrom(p => p.Assesments.Id))
                .ForMember(d => d.AssessmentDesc, s => s.MapFrom(p => p.Assesments.Description))
                .ForMember(d => d.TransactionScore, s => s.MapFrom(p => p.Assesments.TransactionScore))
                .ForMember(d => d.ParameterActualValue, s => s.MapFrom(p => p.TLBParameters.ActualValue))
                .ForMember(d => d.ParameterAliasValue, s => s.MapFrom(p => p.TLBParameters.Alias))
                .ForMember(d => d.AssessmentScore, s => s.MapFrom(p => p.Assesments.TransactionScore))
                .ForMember(d => d.ParameterSearchTerm, s => s.MapFrom(p => p.TLBParameters.Value))
                .ForMember(d => d.ScoredField, s => s.MapFrom(p => p.FIELD))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Assesments, Assessment>()
                .ForMember(d => d.AssessmentId, s => s.MapFrom(p => p.Id))
                .ForMember(d => d.AssessmentName, s => s.MapFrom(p => p.Description))
                .ForMember(d => d.AccountScore, s => s.MapFrom(p => p.AccountScore))
                .ForMember(d => d.AssessmentDetail, s => s.MapFrom(p => p.Detail))
                .ForMember(d => d.AssessmentTypeId, s => s.MapFrom(p => p.AssesmentTypeId))
                .ForMember(d => d.LookUpId, s => s.MapFrom(p => p.AssessmentStoredProcedure_ID))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<FilterCriteria, AvailableComponent>()
                .ForMember(d => d.AssessmentId, s => s.MapFrom(p => p.ass_ID))
                .ForMember(d => d.FcrId, s => s.MapFrom(p => p.fcr_ID))
                .ForMember(d => d.Field, s => s.MapFrom(p => p.fcr_FIELD))
                .ForMember(d => d.Criteria, s => s.MapFrom(p => p.fcr_CRITERIA))
                .ForMember(d => d.Operator, s => s.MapFrom(p => p.fcr_OPERATOR))
                .ForMember(d => d.Order, s => s.MapFrom(p => p.fcr_ORDER))
                .ForMember(d => d.Type, s => s.MapFrom(p => p.fcr_TYPE)).IgnoreAllNonExisting();
            Mapper.CreateMap<AvailableComponent, FilterCriteria>()
                .ForMember(d => d.fcr_ID, s => s.MapFrom(p => p.FcrId))
                .ForMember(d => d.fcr_OPERATOR, s => s.MapFrom(p => p.Operator))
                .ForMember(d => d.fcr_ORDER, s => s.MapFrom(p => p.Order))
                .ForMember(d => d.fcr_TYPE, s => s.MapFrom(p => p.Type))
                .ForMember(d => d.fcr_FIELD, s => s.MapFrom(p => p.Field))
                .ForMember(d => d.ass_ID, s => s.MapFrom(p => p.AssessmentId))
                .ForMember(d => d.fcr_CRITERIA, s => s.MapFrom(p => p.Criteria))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<ChooseFieldsViewModel, AssesmentField>()
                .ForMember(d => d.parameterGroupId, s => s.MapFrom(p => p.ParameterGroupId))
                .ForMember(d => d.FieldName, s => s.MapFrom(p => p.Field))
                .ForMember(d => d.AssesmentId, s => s.MapFrom(p => p.AssessmentId))
                .ForMember(d => d.Id, s => s.MapFrom(p => p.Id))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<TLBParameters, RiskParameter>()
                .ForMember(d => d.Id, s => s.MapFrom(p => p.Id))
                .ForMember(d => d.ParameterValue, s => s.MapFrom(p => p.Value))
                .IgnoreAllNonExisting();

        }


        private State GetHighestStatus(int? id)
        {
            if (id.HasValue)
                return new State { Id = id.Value };
            return new State { Id = 0 };
        }

        private int GetDaysInQueue(DateTime? dateTimeofStatus)
        {
            var days = Convert.ToInt32((System.DateTime.Now - dateTimeofStatus.Value).Days);
            return days;
        }

        private TLBUser GetUser(int x)
        {
            return new TLBUser { UserId = x };
        }

        private List<int> GetAllStateIds(ICollection<UserInGroup> collection)
        {
            List<int> stateIds = new List<int>();
            collection.Each((x) =>
            {
                var ids = x.Group.States.Select(y => y.Id).ToList();
                stateIds.AddRange(ids);
            });
            return stateIds;
        }

        private State GetStatus(TLBCaseFile x)
        {
            return new State { Id = x.StatusId };
        }

        private void MapViewModelToEntityModel()
        {

        }
    }
}
