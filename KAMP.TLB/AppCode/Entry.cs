﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
 
namespace KAMP.TLB.AppCode
{
    using TLB.Views;
    using Core.Interface;
    using Core.Models;
    using KAMP.Core.FrameworkComponents.Enums;
    using KAMP.Core.UserManagement.AppCode;
    using KAMP.Core.FrameworkComponents;
    using System.Threading;
    using System.Windows;
    using KAMP.TLB.BLL;

    [Export(typeof(IModule))]
    public class Entry : IModule
    {

        ModuleMetadata _metadata = null;
        public void Initialize()
        {
            var metadata = new ModuleMetadata();
            metadata.ModuleName = ModuleNames.TLB.ToString();
            new BootStrapper().Configure();
            new KAMP.Core.Workflow.AppCode.BootStrapper().Configure();
            metadata.HomeWindow = new  TLBMainWindow();
            Metadata = metadata;
        }       

        public ModuleMetadata Metadata
        {
            get
            {
                return _metadata;
            }
            set
            {
                this._metadata = value;
            }
        }
       
 
    }
}
