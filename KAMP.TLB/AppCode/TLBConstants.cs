﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.AppCode
{
   public static class TLBConstants
    {    
       public static string DefaultDDLValue = "-Select-";
       public static string NotApplicableText = "N.A";
       public static string CloseApplicationText = "Are You Sure You Want To Close The Application?";
    }    
}
