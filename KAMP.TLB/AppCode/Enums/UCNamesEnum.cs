﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.AppCode.Enums
{
    public enum UCNamesEnum
    {
        AssignCases,
        CaseManagement,
        Assessments,
        Parameters,
        Report,
        Management,
        System,
        OverrideRouting,
        History,
        LookUps,
        AppliactionSettings,
        ManageWorkflow,
        ManageState,
        WorkflowProcess,
        WorkflowMonitoring,
        Assignment,
        CaseFileCard,
        RiskAssessmentParameters,
        RiskAssessments,
        AuditConfiguration
    }
}
