﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.AppCode
{
    public class CaseFilters
    {
       // categoryId, userId, stateId, caseNumber

        public int? CategoryId{get;set;}
        public int? stateId { get; set; }
        public string caseNumber { get; set; } 
        public long? userId { get; set; }

    }
}
