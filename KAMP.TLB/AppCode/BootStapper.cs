﻿using AutoMapper;
using KAMP.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.AppCode
{
    public class BootStrapper : IBootStrapper
    {
        public void Configure()
        {
            Mapper.Initialize(x => 
            {
                x.AddProfile<TLBMapperProfile>();
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}
