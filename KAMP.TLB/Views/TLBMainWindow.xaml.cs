﻿using KAMP.TLB.UserControls;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.TLB.AppCode;
using KAMP.TLB.AppCode.Enums;
using KAMP.Core.Workflow.UserControls;
using KAMP.REPORT.Views;
using KAMP.Core.Common.BLL;
using KAMP.Core.Repository.UM;
//using KAMP.REPORT.Views;
//using KAMP.REPORT.ViewModel;
//using KAMP.REPORT.UserControls;
namespace KAMP.TLB.Views
{
    /// <summary>
    /// Interaction logic for TLBMainWindow.xaml
    /// </summary>
    public partial class TLBMainWindow : Window
    {
        public TLBViewModel ViewModel;
        private UCWorkflowMain UCWorkflowMain;
        private UCWorkflowState UCWorkflowState;
        Dictionary<UCNamesEnum, UserControl> dict = new Dictionary<UCNamesEnum, UserControl>();
        private UIElement currentLoadedUC;
        public TLBMainWindow()
        {
            InitializeComponent();
            Loaded += TLBLoaded;
        }

        private void TLBLoaded(object sender, RoutedEventArgs e)
        {

            AdduserLoginHistory();
            AssignDataContext();
            AssignEventHandlers();
        }

        private void AdduserLoginHistory()
        {
            var commonBl = new CommonBL();
            var context = Application.Current.Properties["Context"] as Context;
            commonBl.AddUserLoginHistory(context, DateTime.Now);
        }


        private void InitializeUserControls()
        {
            UCWorkflowMain = new Core.Workflow.UserControls.UCWorkflowMain();
            UCWorkflowState = new Core.Workflow.UserControls.UCWorkflowState();
        }

        private void AssignEventHandlers()
        {
            UCHeader.CloseApp += CloseApplication;
            UCHeader.ConfigureAudit += ConfigureAuditForTLB;
            ViewModel.NavigationControlViewModel.LoadUserControl += LoadUserControl;
        }

        private void ConfigureAuditForTLB()
        {
            var uc = new TLBConfigurationScreen();
            //uc.DataContext = selectedItemfrmGrid;
            // uc.CloseControl = CloseControl;
            transactionDetailsGrid = new Grid();
            transactionDetailsGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            transactionDetailsGrid.Children.Add(lbl);
            transactionDetailsGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(transactionDetailsGrid);
            }
        }

        private void CloseApplication()
        {
            if (currentLoadedUC.IsNotNull())
                if (currentLoadedUC.GetType().ToString().Equals(typeof(ReportMainControl).ToString()))
                    currentLoadedUC.Visibility = Visibility.Hidden;

            var result = KAMP.TLB.UserControls.MessageBoxControl.Show(TLBConstants.CloseApplicationText, MessageBoxButton.YesNo, TLB.UserControls.MessageType.Alert);
            if (result == MessageBoxResult.Yes)
                Application.Current.Shutdown();
            else
            {
                if (currentLoadedUC.IsNull()) return;
                currentLoadedUC.Visibility = Visibility.Visible;
            }

        }

        private void AssignDataContext()
        {
            ViewModel = new TLBViewModel();
            UCNavigation.DataContext = ViewModel.NavigationControlViewModel;

            UCHeader.DataContext = ViewModel.HeaderViewModel;
            ViewModel.HeaderViewModel.CurrentContent = "Transaction Look Back";

        }

        private UserControl GetUserControl(UCNamesEnum userControl)
        {
            switch (userControl)
            {
                case (UCNamesEnum.AssignCases):
                    return new AssignCases() as UserControl;

                case (UCNamesEnum.CaseManagement):
                    return new CaseManagement() as UserControl;

                case (UCNamesEnum.ManageWorkflow):
                    return new UCWorkflowMain() as UserControl;

                case (UCNamesEnum.ManageState):
                    return new UCWorkflowState() as UserControl;

                case (UCNamesEnum.WorkflowMonitoring):
                    return new UCWorkflowMonitoring() as UserControl;

                case (UCNamesEnum.RiskAssessments):
                    return new RiskAssessments() as UserControl;
                case (UCNamesEnum.LookUps):
                    return new LookUps() as UserControl;
                case (UCNamesEnum.RiskAssessmentParameters):
                    return new RiskAssessmentParameters() as UserControl;
                case (UCNamesEnum.AuditConfiguration):
                    return new TLBConfigurationScreen() as UserControl;
                case (UCNamesEnum.History):
                    return new TLBHistory() as UserControl;

                case (UCNamesEnum.Report):
                    {
                        var control = new ReportMainControl() as UserControl;
                        control.Width = 974;
                        control.HorizontalAlignment = HorizontalAlignment.Right;
                        return control;
                    }

                default:
                    return null;

            }
        }

        private void LoadUserControl(string obj)
        {
            new BootStrapper().Configure();

            ViewModel.HeaderViewModel.CurrentContent = obj;
            var str = obj.Trim().GetStringWithoutWhiteSpace();
            var userControlName = (UCNamesEnum)Enum.Parse(typeof(UCNamesEnum), str);

            var userControl = GetUserControl(userControlName);
            if (userControl.IsNull()) return;

            if (currentLoadedUC.IsNotNull() && userControl.IsNotNull() && currentLoadedUC.ToString().Equals(userControl.ToString()) && !userControlName.Equals(UCNamesEnum.Report))
                return;
            CastUserControlToPageControl(userControl);
            GridUCContainer.Children.Remove(currentLoadedUC);

            GridUCContainer.Children.Add(userControl);
            currentLoadedUC = userControl;
        }

        private void CastUserControlToPageControl(UserControl userControl)
        {
            try
            {

                var iTlbPageControl = userControl as ITLBPageControl;
                if (iTlbPageControl.IsNotNull())
                {
                    userControl.DataContext = ViewModel;
                    iTlbPageControl.InitializeData();
                }

                var ipageControl = userControl as KAMP.Core.Workflow.UserControls.IPageControl;

                if (ipageControl.IsNotNull())
                {
                    ipageControl.InitializeData();
                }
            }
            catch
            {
                try
                {
                    var ipageControl = userControl as KAMP.Core.Workflow.UserControls.IPageControl;
                    if (ipageControl.IsNotNull())
                    {
                        ipageControl.InitializeData();
                    }
                }
                catch
                {

                }
            }
        }


        public Grid transactionDetailsGrid { get; set; }
    }
}
