﻿using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KAMP.Core.FrameworkComponents;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using KAMP.Core.Common.ViewModels;
using KAMP.Core.Workflow;
using KAMP.TLB.Models;
using KAMP.Core.Common.UserControls;
using KAMP.TLB.AppCode;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for CaseFileCard.xaml
    /// </summary>
    public partial class CaseFileCard : UserControl
    {
        public Action CloseControl;
        public Action<CaseDetails> UpdateCaseAfterOpening;
        private CaseFileCardViewModel _caseFileCardViewModel;
        public CaseFileCard(CaseFileCardViewModel caseFileCardViewModel)
        {
            this.DataContext = _caseFileCardViewModel = caseFileCardViewModel;
            InitializeComponent();
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

            if (CloseControl.IsNotNull())
                CloseControl();
        }

        private AssessmentsViewModel GetAssessments(CaseFileCardViewModel _caseFileCardVM)
        {
            var tlbBl = new TLBBLL();
            var lstAssessments = tlbBl.GetAssessments(_caseFileCardVM);
            if (lstAssessments.IsCollectionValid())
            {
                var asssessmentViewModel = new AssessmentsViewModel();
                asssessmentViewModel.Assessment = lstAssessments;
                asssessmentViewModel.TotalOccurrences = lstAssessments.Select(x => x.Occurrences).Sum();
                asssessmentViewModel.TotalOverallScore = lstAssessments.Select(x => x.TotalScore).Sum();

                return asssessmentViewModel;
            }

            return new AssessmentsViewModel();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var control = TypeCastToContentControl(e);
            if (control.IsNull() || control.Content.IsNull())
                return;
            if (control.Content.GetType() == typeof(Transactions))
            {
                var bLL = new TLBBLL();
                _caseFileCardViewModel.TransactionViewModel.TransactionList = bLL.GetTransactionsByCaseNo(_caseFileCardViewModel.DGSelectedItem.CfId);
            }

            else if (control.Content.GetType() == typeof(Dispositions))
            {
                var bLL = new TLBBLL();
                //TODO beneficiary related logic to be written
                _caseFileCardViewModel.DispositionsViewModel = bLL.GetDispositions(_caseFileCardViewModel.DGSelectedItem.CfId);
            }

            else if (control.Content.GetType() == typeof(RelatedCases))
            {
                var bLL = new TLBBLL();
                _caseFileCardViewModel.RelatedCasesViewModel.RelatedCasesList = bLL.RelatedCasesList(_caseFileCardViewModel.DGSelectedItem.CfId);
                _caseFileCardViewModel.RelatedCasesViewModel.CfId = _caseFileCardViewModel.DGSelectedItem.CfId;
            }

            else if (control.Content.GetType() == typeof(OrderingParty))
            {
                var bLL = new TLBBLL();
                _caseFileCardViewModel.OrderingPartyViewModel = bLL.GetOrderingParty(_caseFileCardViewModel.DGSelectedItem.CfId);
            }
            else if (control.Content.GetType() == typeof(ClientAccounts))
            {
                var bLL = new TLBBLL();
                _caseFileCardViewModel.ClientAccountViewModel = bLL.GetClientAccount(_caseFileCardViewModel);
            }
            else if (control.Content.GetType() == typeof(Assessments))
            {
                var bLL = new TLBBLL();
                _caseFileCardViewModel.AssessmentsViewModel = GetAssessments(_caseFileCardViewModel);
            }
            else if (control.Content.GetType() == typeof(Beneficiaries))
            {
                var bLL = new TLBBLL();
                _caseFileCardViewModel.BeneficiariesViewModel = bLL.GetBeneficiaries(_caseFileCardViewModel.DGSelectedItem.CfId);
            }

            else if (control.Content.GetType() == typeof(UCCaseHistory))
            {
                var bLL = new TLBBLL();
                _caseFileCardViewModel.CaseHistoryViewModel.CFID = _caseFileCardViewModel.DGSelectedItem.CfId;
                _caseFileCardViewModel.CaseHistoryViewModel.CaseHistoryList = bLL.GetCaseHistory("TLBCaseFile", _caseFileCardViewModel.DGSelectedItem.CfId);
                _caseFileCardViewModel.CaseHistoryViewModel.UserList = bLL.GetUsers();
                _caseFileCardViewModel.CaseHistoryViewModel.SelectedUser = _caseFileCardViewModel.CaseHistoryViewModel.UserList.FirstOrDefault().UserName;

            }
            else if (control.Content.GetType() == typeof(FileLogs))
            {
                _caseFileCardViewModel.FileLogViewModel = new FileLogsViewModel()
                {
                    CfId = _caseFileCardViewModel.DGSelectedItem.CfId,
                    AssignedUser = _caseFileCardViewModel.IsAssignedUser
                };
                UCFIleLog.DataContext = _caseFileCardViewModel.FileLogViewModel;
            }

        }

        private static ContentControl TypeCastToContentControl(SelectionChangedEventArgs e)
        {

            try
            {
                ContentControl cc = ((System.Windows.Controls.ContentControl)(((object[])(e.AddedItems))[0]));
                return cc;
            }
            catch
            {
                return null;
            }

        }

        private void OpenCase(object sender, RoutedEventArgs e)
        {

            var dataContext = DataContext as CaseFileCardViewModel;
            try
            {
                WorkflowAPI.OpenCase(dataContext.DGSelectedItem.CaseNumber);
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, MessageType.Error);

                return;
            }

            var bLL = new TLBBLL();
            new BootStrapper().Configure();
            var updatedCase = bLL.GetCaseDetails(dataContext.DGSelectedItem.CaseNumber);

            //Updating Main Collection
            var oldCase = TLBViewModel.CaseList.FirstOrDefault(x => x.CaseNumber == dataContext.DGSelectedItem.CaseNumber);
            TLBViewModel.CaseList.Remove(oldCase);
            TLBViewModel.CaseList.Add(updatedCase);
            TLBViewModel.CaseList.OrderBy(x => x.CaseNumber);

            dataContext.DGSelectedItem = updatedCase;

            if (UpdateCaseAfterOpening.IsNotNull())
            {
                UpdateCaseAfterOpening(updatedCase);
            }
            ucDispositions.TxtDueDiligence.IsEnabled = true;
            ucDispositions.TxtExecutiveSummary.IsEnabled = true;
            ucDispositions.TxtMamorandumofFact.IsEnabled = true;
            //ucDispositions.TxtRiskProfile.IsEnabled = false;
            ucDispositions.StkPnlDueDiligence.Visibility = Visibility.Visible;
            ucDispositions.StkPnlRiskPrifile.Visibility = Visibility.Visible;
            ucDispositions.StkPnlMOF.Visibility = Visibility.Visible;
            ucDispositions.StkPnlExecSummary.Visibility = Visibility.Visible;
            //UCClientAccounts.imgExport.Visibility = Visibility.Visible;
            //UCTransactions.tdImgExport.Visibility = Visibility.Visible;
            //UCBeneficiaries.stkPnlExport.Visibility = Visibility.Visible;
            //OrderingParty.stkPnlExport.Visibility = Visibility.Visible;
            //ucRelatedCases.rcImgExport.Visibility = Visibility.Visible;
            //UCCaseHistory.hstImgExport.Visibility = Visibility.Visible;
            MessageBoxControl.Show("Case Opened Successfully.", MessageBoxButton.OK, MessageType.Information);

        }

        private void CloseCaseFileCard(object sender, RoutedEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        private void OpenRFI(object sender, RoutedEventArgs e)
        {
            var context = Application.Current.Properties["Context"] as Context;
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.Children.Contains(rFIGrid)) return;
            var rfiViewModel = new RFIViewModel
            {
                CaseNumber = _caseFileCardViewModel.DGSelectedItem.CaseNumber,
                ModuleId = context.Module.Id,
                SenderName = context.User.UserName.Split('\\').LastOrDefault(),
                RFIColorModel = new RFIColorModel
                {
                    DarkColor = "#FF365F70",
                    BackGroundColor = "#FF83ADBF",
                    LightestColor = "#FFBDD0D8"
                    ,
                    InnerBackgroundColor = "#FF365F70",
                    LabelColor = "#FFFFFFFF",
                    AttachButtonColor = "#FFBDD0D8"
                }
            };
            var uc = new RFIMain(rfiViewModel);
            uc.CloseControl = CloseRFI;
            rFIGrid = new Grid();
            rFIGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            rFIGrid.Children.Add(lbl);
            rFIGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            if (container.IsNotNull())
            {
                container.Children.Add(rFIGrid);
            }
        }

        private void CloseRFI()
        {
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(rFIGrid);
            }
        }

        public Grid rFIGrid { get; set; }
    }
}
