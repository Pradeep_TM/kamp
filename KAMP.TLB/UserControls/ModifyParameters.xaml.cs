﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.Models;
using KAMP.TLB.BLL;
using System.ComponentModel;
using System.Collections.ObjectModel;
using KAMP.TLB.ViewModels;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for ModifyParameters.xaml
    /// </summary>
    public partial class ModifyParameters : UserControl, INotifyPropertyChanged
    {
        public Action CloseControl;

        public ModifyParameters(EditParameterListViewModel editParamLstVm)
        {
            // TODO: Complete member initialization
            this.editParamLstVm = editParamLstVm;
            InitializeComponent();
            GetParameter(editParamLstVm.Id);
            if (editParamLstVm.OnSaveClick.IsNull())
                editParamLstVm.OnSaveClick += OnSaveClick;
            this.DataContext = editParamLstVm;
        }

        public void OnSaveClick(int id)
        {
            var tlbBl = new TLBBLL();
            if (id == 0)
            {
                ValidateViewModel(editParamLstVm);
                if (editParamLstVm.ParameterValue.IsNotNull())
                {
                    var paramFrmDb = tlbBl.GetParameter(editParamLstVm.ParameterValue);
                    if (paramFrmDb.IsNull())
                    {
                        tlbBl.AddParameter(editParamLstVm);
                        MessageBoxControl.Show("Saved Successfully.", MessageBoxButton.OK, MessageType.Information);
                    }
                    else
                    {
                        MessageBoxControl.Show("Parameter already exists.", MessageBoxButton.OK, MessageType.Error);
                    }

                    if (CloseControl.IsNotNull())
                        CloseControl();
                }
            }
            else
            {
                var parameterExist = tlbBl.GetParameterById(id);
                if (parameterExist.IsNotNull())
                {
                    if (editParamLstVm.ParameterValue == parameterExist.ParameterValue)
                    {
                        MessageBoxControl.Show("Parameter already exists.", MessageBoxButton.OK, MessageType.Information);
                        return;
                    }
                    tlbBl.UpdateParameter(editParamLstVm);
                    MessageBoxControl.Show("Saved Successfully.", MessageBoxButton.OK, MessageType.Information);
                    if (CloseControl.IsNotNull())
                        CloseControl();
                }
            }


        }

        private void ValidateViewModel(EditParameterListViewModel editParameterVM)
        {
            if (editParameterVM.ParameterValue.IsNull())
            {
                MessageBoxControl.Show("Parameter Value is mandatory.", MessageBoxButton.OK, MessageType.Error);
            }
        }

        private void GetParameter(int parameterId)
        {
            var tlbBl = new TLBBLL();
            var parameterFrmDb = tlbBl.GetParameterById(parameterId);
            editParamLstVm.Id = parameterFrmDb.Id;
            editParamLstVm.ParameterValue = parameterFrmDb.ParameterValue;
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private EditParameterListViewModel editParamLstVm;
    }
}
