﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.ViewModels;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for AssessmentTransactions.xaml
    /// </summary>
    public partial class AssessmentTransactions : UserControl
    {
        public Action CloseControl;
        private TransactionViewModel transactionViewModel;
        public AssessmentTransactions()
        {
            InitializeComponent();
        }

        public AssessmentTransactions(TransactionViewModel transactionViewModel)
        {
            this.transactionViewModel = transactionViewModel;
            InitializeComponent();
            UCTransactions.DataContext = transactionViewModel.TransactionList;  
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }
    }
}
