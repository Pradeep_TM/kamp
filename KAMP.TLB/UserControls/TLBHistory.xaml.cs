﻿using KAMP.TLB.AppCode;
using KAMP.TLB.BLL;
using KAMP.TLB.Models;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.TLB.UserControls
{
    using KAMP.Core.FrameworkComponents;
    /// <summary>
    /// Interaction logic for TLBHistory.xaml
    /// </summary>
    public partial class TLBHistory : UserControl, ITLBPageControl
    {
        public TLBViewModel _viewModel { get; set; }

        public TLBHistory()
        {
            InitializeComponent();
        }

        public void InitializeData()
        {
            _viewModel = DataContext as TLBViewModel;
            var bll = new TLBBLL();
            _viewModel.TLBHistoryViewModel.HistoryCategoryList = new ObservableCollection<string> { TLBConstants.DefaultDDLValue, "Application", "Case", "Login" };
            _viewModel.TLBHistoryViewModel.SelectedCategory = _viewModel.TLBHistoryViewModel.HistoryCategoryList.FirstOrDefault();
        }

        private void HistorySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //TODO Get Category List fron some other place
            var ddl = sender as ComboBox;
            var bll = new TLBBLL();
            if (ddl.SelectedIndex == -1 || ddl.SelectedIndex == 0)
            {
                _viewModel.TLBHistoryViewModel.HistoryList = null;
                StkSubCategory.Visibility = Visibility.Collapsed;
                DgTLBHistory.Visibility = Visibility.Hidden;
                DgTLBLoginHistory.Visibility = Visibility.Hidden;
            }

            else if (ddl.SelectedItem.ToString().Equals("Application"))
            {
                StkSubCategory.Visibility = Visibility.Visible;
                StkUser.Visibility = Visibility.Collapsed;
                DgTLBHistory.Visibility = Visibility.Visible;
                DgTLBLoginHistory.Visibility = Visibility.Hidden;
                CaseNumberColumn.Visibility = Visibility.Collapsed;
                _viewModel.TLBHistoryViewModel.HistoryList = new ObservableCollection<Models.HistoryModel>();
                _viewModel.TLBHistoryViewModel.EntityList = bll.GetApplicationHistory().Select(x => x.Entity).Distinct().ToList();
                _viewModel.TLBHistoryViewModel.EntityList.Insert(0, TLBConstants.DefaultDDLValue);
                _viewModel.TLBHistoryViewModel.SelectedEntity = _viewModel.TLBHistoryViewModel.EntityList.FirstOrDefault();

            }
            else if (ddl.SelectedItem.ToString().Equals("Case"))
            {
                _viewModel.TLBHistoryViewModel.HistoryList = bll.GetCaseHistory("TlbCaseFile", 0);
                CaseNumberColumn.Visibility = Visibility.Visible;
                StkSubCategory.Visibility = Visibility.Collapsed;
                StkUser.Visibility = Visibility.Collapsed;
                DgTLBHistory.Visibility = Visibility.Visible;
                DgTLBLoginHistory.Visibility = Visibility.Hidden;
            }
            else if (ddl.SelectedItem.ToString().Equals("Login"))
            {
                StkSubCategory.Visibility = Visibility.Collapsed;
                StkUser.Visibility = Visibility.Visible;
                CaseNumberColumn.Visibility = Visibility.Collapsed;
                _viewModel.TLBHistoryViewModel.LoginHistoryList = new ObservableCollection<HistoryModel>();
                _viewModel.TLBHistoryViewModel.UserList = bll.GetLoginHistory().Select(x => x.User).Distinct().OrderBy(x => x).ToList();
                _viewModel.TLBHistoryViewModel.UserList.Insert(0, TLBConstants.DefaultDDLValue);
                _viewModel.TLBHistoryViewModel.SelectedUserName = _viewModel.TLBHistoryViewModel.UserList.FirstOrDefault();
                DgTLBHistory.Visibility = Visibility.Hidden;
                DgTLBLoginHistory.Visibility = Visibility.Visible;
            }
            else
            {
                _viewModel.TLBHistoryViewModel.HistoryList = new ObservableCollection<Models.HistoryModel>();
                CaseNumberColumn.Visibility = Visibility.Collapsed;
                StkSubCategory.Visibility = Visibility.Collapsed;
                StkUser.Visibility = Visibility.Collapsed;
                DgTLBHistory.Visibility = Visibility.Hidden;
                DgTLBLoginHistory.Visibility = Visibility.Hidden;
            }
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void ViewDetailsClicked(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = DgTLBHistory.SelectedItem as HistoryModel;

            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var uc = new HistoryDetails();
            uc.DataContext = selectedItemfrmGrid;
            uc.CloseControl = CloseControl;
            tLBHistoryGrid = new Grid();
            tLBHistoryGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            tLBHistoryGrid.Children.Add(lbl);
            tLBHistoryGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(tLBHistoryGrid);
            }
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(tLBHistoryGrid);
            }
        }

        private void ddlHistorySubCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_viewModel.TLBHistoryViewModel.SelectedEntity != null && _viewModel.TLBHistoryViewModel.SelectedEntity.Equals(TLBConstants.DefaultDDLValue)) return;
            var bll = new TLBBLL();
            var list = bll.GetApplicationHistory().Where(x => x.Entity.Equals(_viewModel.TLBHistoryViewModel.SelectedEntity)).OrderBy(x => x.Entity).ToList();
            var collection = new ObservableCollection<HistoryModel>();
            foreach (var item in list)
            {
                collection.Add(item);
            }
            _viewModel.TLBHistoryViewModel.HistoryList = collection;
            _viewModel.TLBHistoryViewModel.IsLoginGridVisible = false;
        }

        private void ddlUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_viewModel.TLBHistoryViewModel.SelectedUserName.IsNull() || _viewModel.TLBHistoryViewModel.SelectedUserName.Equals(TLBConstants.DefaultDDLValue)) return;
            var bll = new TLBBLL();
            var list = bll.GetLoginHistory().Where(x => x.User.Equals(_viewModel.TLBHistoryViewModel.SelectedUserName)).ToList();
            var collection = new ObservableCollection<HistoryModel>();
            foreach (var item in list)
            {
                collection.Add(item);
            }
            _viewModel.TLBHistoryViewModel.LoginHistoryList = collection;
        }


        public Grid tLBHistoryGrid { get; set; }
    }
}
