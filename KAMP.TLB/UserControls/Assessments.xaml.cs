﻿using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.UserControls;
using KAMP.TLB.BLL;
using KAMP.TLB.Models;


namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for Assessments.xaml
    /// </summary>
    public partial class Assessments : UserControl
    {
        private Grid assessmentDetailCardGrid;
        public Assessments()  
        {
            InitializeComponent();
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assessmentDetailCardGrid);
            }
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdAssessments.SelectedItem as Assessment;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var transactionViewModel = new TransactionViewModel();
            var tlbBL = new TLBBLL();
            var trans = tlbBL.GetTransactions(selectedItemfrmGrid.CfId, selectedItemfrmGrid.TransactionIds);
            transactionViewModel.TransactionList = trans;
            AssessmentTransactions transDetail = new AssessmentTransactions(transactionViewModel);
            transDetail.CloseControl = CloseControl;
            assessmentDetailCardGrid = new Grid();
            assessmentDetailCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentDetailCardGrid.Children.Add(lbl);
            assessmentDetailCardGrid.Children.Add(transDetail);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentDetailCardGrid);
            }
        }

        private void DetailImage_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdAssessments.SelectedItem as Assessment;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var assessmentsViewModel = new Assessment();
            assessmentsViewModel = selectedItemfrmGrid;
            assessmentsViewModel.IsVisible = false;
            assessmentsViewModel.IsReadOnly = true;
            assessmentsViewModel.IsEditable = false;
            var uc = new AssessmentDetailCard(assessmentsViewModel);
            uc.CloseControl = CloseControl;
            assessmentDetailCardGrid = new Grid();
            assessmentDetailCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentDetailCardGrid.Children.Add(lbl);
            assessmentDetailCardGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentDetailCardGrid);
            }
        }

        private void ParamByCatImage_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdAssessments.SelectedItem as Assessment;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            AssessmentHitsViewModel assessmentHitsViewModel = new AssessmentHitsViewModel();
            var tlbBL = new TLBBLL();
            var res = tlbBL.GetAssessmentHits(selectedItemfrmGrid);
            var result = res.FirstOrDefault(x => x.AssessmentId == selectedItemfrmGrid.AssessmentId);
            var uc = new CategoryParameterHits(result);
            uc.CloseControl = CloseControl;
            assessmentDetailCardGrid = new Grid();
            assessmentDetailCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentDetailCardGrid.Children.Add(lbl);
            assessmentDetailCardGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentDetailCardGrid);
            }

        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }
       

        private void ParamViewsImage_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdAssessments.SelectedItem as Assessment;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            AssessmentHitsViewModel assessmentHitsViewModel = new AssessmentHitsViewModel();
            var tlbBL = new TLBBLL();
            var res = tlbBL.GetAssessmentHits(selectedItemfrmGrid);
            var uc = new AssessmentHits(res);
            uc.CloseControl = CloseControl;
            assessmentDetailCardGrid = new Grid();
            assessmentDetailCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentDetailCardGrid.Children.Add(lbl);
            assessmentDetailCardGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentDetailCardGrid);
            }

        }
    }
}
