﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for AssessmentHits.xaml     
    /// </summary>
    public partial class AssessmentHits : UserControl
    {
        public Action CloseControl;
        private List<ViewModels.AssessmentHitsViewModel> res;
        public AssessmentHits()
        {
            InitializeComponent();
            //this.DataContext = 
        }

        public AssessmentHits(List<ViewModels.AssessmentHitsViewModel> res)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.res = res;
            DataContext = res;

        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }
    }
}
