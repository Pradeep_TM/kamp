﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using System.Windows.Media.Animation;
using KAMP.TLB.ViewModels;
using KAMP.TLB.BLL;
using KAMP.TLB.Models;
using System.Collections.ObjectModel;
using AutoMapper;
using KAMP.Core.Repository.Models.TLB;


namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for AssessmentFilter.xaml
    /// </summary>
    public partial class AssessmentFilter : UserControl
    {
        public Action CloseControl;
        private AssessmentFilterViewModel assessmentFilterViewModel = new AssessmentFilterViewModel();
        private double _height;
        private double _heightAssigned;
        private bool IsAssignedTabOpened;
        private bool IsUnassignedTabOpened;
        private int assessmentId;
        private Grid editFilterGrid;
        //public AssessmentFilter()
        //{
        //    InitializeComponent();
        //}

        public AssessmentFilter(int assessmentId)
        {
            this.assessmentId = assessmentFilterViewModel.AssessmentId = assessmentId;
            assessmentFilterViewModel.AvailableComponent = GetAssessmentFilter(assessmentId);
            assessmentFilterViewModel.SelectedComponent = GetSelectedComponent(assessmentId);
            this.DataContext = assessmentFilterViewModel;
            InitializeComponent();
            assessmentFilterViewModel.OnNewClick += OnNewClick;
        }

        private void OnNewClick()
        {
            //if (editFilterGrid.IsNotNull())
            //    return;
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.Children.Contains(editFilterGrid)) return;
            var uc = new EditFilterComponent(0, assessmentId);
            uc.CloseControl = CloseControlEditFilter;
            editFilterGrid = new Grid();
            editFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            editFilterGrid.Children.Add(lbl);
            editFilterGrid.Children.Add(uc);
            if (container.IsNotNull())
            {
                container.Children.Add(editFilterGrid);
            }
        }

        private ObservableCollection<AvailableComponent> GetSelectedComponent(int assessmentId)
        {
            var tlbBl = new TLBBLL();
            var selectedComponents = tlbBl.GetSelectedComponent(assessmentId);
            var mappedFilters = Mapper.Map<List<FilterCriteria>, ObservableCollection<AvailableComponent>>(selectedComponents);
            return mappedFilters;
        }


        private ObservableCollection<AvailableComponent> GetAssessmentFilter(int assessmentId)
        {
            var tlbBl = new TLBBLL();
            var assessmentFilters = tlbBl.GetAssessmentFilters(assessmentId);
            var mappedFilters = Mapper.Map<List<FilterCriteria>, ObservableCollection<AvailableComponent>>(assessmentFilters);
            return mappedFilters;
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void AddToSelectedComponent(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdAvailableComponent.SelectedItem as AvailableComponent;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please select a row.",MessageBoxButton.OK,MessageType.Error);
            var uc = new EditFilterComponent(selectedItemfrmGrid.FcrId, assessmentId);
            uc.CloseControl = CloseControlEditFilter;
            editFilterGrid = new Grid();
            editFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            editFilterGrid.Children.Add(lbl);
            editFilterGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(editFilterGrid);
            }
        }

        private void CloseControlEditFilter()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(editFilterGrid);
            }
            LoadFilter();
            //editFilterGrid = null;
        }

        private void LoadFilter()
        {
            assessmentFilterViewModel.AvailableComponent = GetAssessmentFilter(assessmentId);
            assessmentFilterViewModel.SelectedComponent = GetSelectedComponent(assessmentId);
        }

        private void ImageEdit_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdSelectedComponent.SelectedItem as AvailableComponent;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
            var uc = new EditFilterComponent(selectedItemfrmGrid.FcrId, assessmentId);
            uc.CloseControl = CloseControlEditFilter;
            editFilterGrid = new Grid();
            editFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            editFilterGrid.Children.Add(lbl);
            editFilterGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(editFilterGrid);
            }   
        }

        private void ImageDelete_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdSelectedComponent.SelectedItem as AvailableComponent;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
            var res = MessageBoxControl.Show("Are you sure you want to delete this selected component?", MessageBoxButton.YesNo,MessageType.Alert);
            if (res == MessageBoxResult.Yes)
            {
                var tlbBl = new TLBBLL();
                tlbBl.DeleteSelectedComponent(selectedItemfrmGrid);
                LoadFilter();
            }
        }
    }
}
