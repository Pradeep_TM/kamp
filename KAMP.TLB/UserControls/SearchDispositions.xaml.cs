﻿using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KAMP.Core.FrameworkComponents;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for SearchDispositions.xaml
    /// </summary>
    public partial class SearchDispositions : UserControl
    {
        public Action CloseControl;
        private SearchDispositionsViewModel viewModel;
        public SearchDispositions()
        {
            InitializeComponent();
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl != null)
            {
                CloseControl();
            }
        }

        private void ListCases_Clicked(object sender, RoutedEventArgs e)
        {
            if (viewModel == null)
            {
                viewModel = DataContext as SearchDispositionsViewModel;  
            }
            var bll = new TLBBLL();
            viewModel.SearchDispositionsModel.CaseDetailsList = bll.SearchDispositionsExact(viewModel);
        }

        private void HighlightSearchText(string searchText, TextRange txtRange, RichTextBox rtb, SearchDispositionsViewModel isExactSearch)
        {
            if ((viewModel.SearchDispositionsModel.IsExactSearch || viewModel.SearchDispositionsModel.IsSoundAlikeSearch) && !SearchForExactTerm(txtRange.Text,searchText))
            {
                return;
            }
            //Need to search for exact term
            if (searchText.IsNotNull())
            {
                var range = txtRange.Text;
                string pattern;
                if (viewModel.SearchDispositionsModel.IsExactSearch || viewModel.SearchDispositionsModel.IsSoundAlikeSearch)
                {
                    pattern = @"\b"+ Regex.Escape(searchText)+ @"\b";
                }
                else
                {
                     pattern = "("+Regex.Escape(searchText)+")";
                }
                Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

                var start = rtb.Document.ContentStart;
                while (start != null && start.CompareTo(rtb.Document.ContentEnd) < 0)
                {
                    if (start.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                    {
                        var match = reg.Match(start.GetTextInRun(LogicalDirection.Forward));

                        var textrange = new TextRange(start.GetPositionAtOffset(match.Index, LogicalDirection.Forward), start.GetPositionAtOffset(match.Index + match.Length, LogicalDirection.Backward));
                        textrange.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.DarkGreen));
                        textrange.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(Colors.Yellow));
                        textrange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
                        start = textrange.End;
                    }
                    start = start.GetNextContextPosition(LogicalDirection.Forward);
                }
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (viewModel == null)
            {
                viewModel = DataContext as SearchDispositionsViewModel;
            }
            if (viewModel.SearchDispositionsModel.DGSelectedItem == null) return;
            if (viewModel.SearchDispositionsModel.IsSoundAlikeSearch)
            {
                foreach (var item in viewModel.SearchDispositionsModel.DGSelectedItem.MatchedWords)
                {
                    HighlightSearchText(item, new TextRange(TxtExecutiveSummary.Document.ContentStart, TxtExecutiveSummary.Document.ContentEnd), TxtExecutiveSummary, viewModel);
                    HighlightSearchText(item, new TextRange(TxtRiskProfile.Document.ContentStart, TxtRiskProfile.Document.ContentEnd), TxtRiskProfile, viewModel);
                    HighlightSearchText(item, new TextRange(TxtDueDiligence.Document.ContentStart, TxtDueDiligence.Document.ContentEnd), TxtDueDiligence, viewModel);
                    HighlightSearchText(item, new TextRange(TxtMemorandumofFact.Document.ContentStart, TxtMemorandumofFact.Document.ContentEnd), TxtMemorandumofFact, viewModel);
                }
            }
            else
            {
                foreach (var item in viewModel.SearchDispositionsModel.SearchParameters.Split(','))
                {
                    HighlightSearchText(item, new TextRange(TxtExecutiveSummary.Document.ContentStart, TxtExecutiveSummary.Document.ContentEnd), TxtExecutiveSummary, viewModel);
                    HighlightSearchText(item, new TextRange(TxtRiskProfile.Document.ContentStart, TxtRiskProfile.Document.ContentEnd), TxtRiskProfile, viewModel);
                    HighlightSearchText(item, new TextRange(TxtDueDiligence.Document.ContentStart, TxtDueDiligence.Document.ContentEnd), TxtDueDiligence, viewModel);
                    HighlightSearchText(item, new TextRange(TxtMemorandumofFact.Document.ContentStart, TxtMemorandumofFact.Document.ContentEnd), TxtMemorandumofFact, viewModel);
                }
            }
        }

        private bool SearchForExactTerm(string contentForSearch, string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm)) return false;
            contentForSearch = contentForSearch.Replace("\r\n", " ");
            var indexofTerm = contentForSearch.IndexOf(searchTerm,StringComparison.InvariantCultureIgnoreCase);
            bool result = true;

            if (indexofTerm == -1)
            {
                return false;
            }

            //Check alphabet before term
            if (indexofTerm != 0)
            {
                var AsciiValueofelementbefore = Convert.ToInt32(contentForSearch.ElementAt(indexofTerm - 1));

                if ((AsciiValueofelementbefore >= 65 && AsciiValueofelementbefore <= 90) || (AsciiValueofelementbefore >= 97 && AsciiValueofelementbefore <= 122))
                {
                    result = false;
                }
            }

            //Check alphabet after term
            if (indexofTerm + searchTerm.Length < contentForSearch.Length)
            {
                var AsciiValueofelementbefore = Convert.ToInt32(contentForSearch.ElementAt(indexofTerm + searchTerm.Length));

                if ((AsciiValueofelementbefore >= 65 && AsciiValueofelementbefore <= 90) || (AsciiValueofelementbefore >= 97 && AsciiValueofelementbefore <= 122))
                {
                    result = false;
                }
            }

            if (!result && (indexofTerm + searchTerm.Length) < contentForSearch.Length)
            {
                result = SearchForExactTerm(contentForSearch.Substring(indexofTerm + searchTerm.Length), searchTerm);
            }
            return result;
        }

    }
}
