﻿using KAMP.TLB.Models;
using KAMP.TLB.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.ViewModels;
using System.Collections.ObjectModel;
using KAMP.Core.FrameworkComponents.Helpers;
using System.IO;
using System.Diagnostics;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for Transactions.xaml
    /// </summary>
    public partial class Transactions : UserControl
    {
        public Transactions()
        {
            InitializeComponent();
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(transactionDetailsGrid);
            }
        }

        private Grid transactionDetailsGrid { get; set; }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = DgTransaction.SelectedItem as TransactionDetail;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var uc = new TransactionDetails();
            uc.DataContext = selectedItemfrmGrid;
            uc.CloseControl = CloseControl;
            transactionDetailsGrid = new Grid();
            transactionDetailsGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            transactionDetailsGrid.Children.Add(lbl);
            transactionDetailsGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(transactionDetailsGrid);
            }
        }

        private void TransScoreDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = DgTransaction.SelectedItem as TransactionDetail;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var id = selectedItemfrmGrid.Id;
            TransactionHitsViewModel viewmodel = new TransactionHitsViewModel(selectedItemfrmGrid);


            var uc = new TransactionHits();
            uc.DataContext = viewmodel;
            uc.CloseTransactionHits = CloseTransactionHits;
            transactionHitsGrid = new Grid();
            transactionHitsGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            transactionHitsGrid.Children.Add(lbl);
            transactionHitsGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(transactionHitsGrid);
            }
        }

        private void CloseTransactionHits()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(transactionHitsGrid);
            }
        }


        public Grid transactionHitsGrid { get; set; }

        private void ExportToExcel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var transDetails = this.DataContext as ObservableCollection<TransactionDetail>;
            if (transDetails.ToList().IsCollectionValid())
            {
                try
                {
                    var transDetailLst = new List<TransactionDetailExport>();
                    var transDtls = transDetails.ToList();
                    transDtls.ForEach(x =>
                    {
                        var transDetailExport = new TransactionDetailExport()
                        {
                            BeneficiaryBankCountryName = x.BeneficiaryBankCountryName,
                            BeneficiaryBankName = x.BeneficiaryBankName,
                            BenePartyCountryName = x.BenePartyCountryName,
                            BenePartyName = x.BenePartyName,
                            ByOrderPartyCountryName = x.ByOrderPartyCountryName,
                            ByOrderPartyName = x.ByOrderPartyName,
                            DebitBankCountryName = x.DebitBankCountryName,
                            DebitBankName = x.DebitBankName,
                            InternalReferenceNumber = x.InternalReferenceNumber,
                            MessageType = x.MessageType,
                            PaymentAmount = x.PaymentAmount.Value,
                            TotalScore = x.TotalScore.Value,
                            Value = x.Value,
                            ValueDate = x.ValueDate.Value.ToString("dd-MM-yyyy")
                        };
                        transDetailLst.Add(transDetailExport);
                    });
                    Export.CreateExcelDocument<TransactionDetailExport>(transDetailLst);
                }
                catch (Exception ex)
                {
                    MessageBoxControl.Show(ex.Message);
                }


            }
        }
    }
}
