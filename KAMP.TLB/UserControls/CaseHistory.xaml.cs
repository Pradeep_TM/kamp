﻿using KAMP.TLB.BLL;
using KAMP.TLB.Models;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KAMP.Core.FrameworkComponents;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents.Helpers;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for CaseHistory.xaml
    /// </summary>
    public partial class UCCaseHistory : UserControl
    {
        public UCCaseHistory()
        {
            InitializeComponent();
        }
        public Grid caseHistoryGrid { get; set; }

        private void ViewDetailsClicked(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = DgCaseHistory.SelectedItem as HistoryModel;

            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var uc = new HistoryDetails();
            uc.DataContext = selectedItemfrmGrid;
            uc.CloseControl = CloseControl;
            caseHistoryGrid = new Grid();
            caseHistoryGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            caseHistoryGrid.Children.Add(lbl);
            caseHistoryGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(caseHistoryGrid);
            }

        }

        private void CloseControl()
        {
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(caseHistoryGrid);
            }
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void UserSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ddl = sender as ComboBox;
            if (ddl.SelectedIndex == -1) return;
            _viewModel = DataContext as CaseHistoryViewModel;
            var bLL = new TLBBLL();
            _viewModel.CaseHistoryList = bLL.GetCaseHistory("TLBCaseFile",_viewModel.CFID, _viewModel.SelectedUser);
        }


        private CaseHistoryViewModel _viewModel { get; set; }

        private void ExportToWord_Clicked(object sender, MouseButtonEventArgs e)
        {
            var historyList = new List<CaseHistoryExportModel>();
            if (_viewModel.CaseHistoryList.Count == 0) return;

            foreach (var item in _viewModel.CaseHistoryList)
            {
                var data = new CaseHistoryExportModel
                {
                    User = item.User,
                    TimeStamp = item.TimeStamp,
                    FieldName = item.FieldName,
                    Action = item.Action,
                    PreviousValue = item.PreviousValue,
                    ModifiedValue = item.ModifiedValue
                };
                historyList.Add(data);
            }

            try { DocumentCreator.CreateNewWordDocument(historyList); }
            catch (Exception ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }
    }
}
