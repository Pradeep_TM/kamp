﻿using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for UCExportDispositions.xaml
    /// </summary>
    public partial class UCExportDispositions : UserControl
    { 
        public Action<DispositionExportViewModel> ExportDispositions;  
        public Action Close;
        private DispositionExportViewModel viewModel;
        public UCExportDispositions()
        {
            InitializeComponent();
            viewModel = new DispositionExportViewModel();
            DataContext = viewModel;
        }

        private void ExportClicked(object sender, MouseButtonEventArgs e)
        {
            if (ExportDispositions != null)
            {
                ExportDispositions(viewModel);
            }
        }


        private void CloseControl(object sender, RoutedEventArgs e)
        {
            if (Close != null)
            {
                Close();
            }
        }
    }
}
