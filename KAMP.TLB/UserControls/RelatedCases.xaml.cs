﻿using KAMP.TLB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.TLB.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.FrameworkComponents.Helpers;
    using KAMP.TLB.BLL;
    using KAMP.TLB.ViewModels;
    /// <summary>
    /// Interaction logic for RelatedCases.xaml
    /// </summary>
    public partial class RelatedCases : UserControl
    {
        public RelatedCases()
        {
            InitializeComponent();
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

        private void GetRelatedParties(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = DgRelatedCases.SelectedItem as CaseDetails;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var dataContext = this.DataContext as RelatedCasesViewModel;
            //Getting related Parties List
            var bLL = new TLBBLL();
            //Passing cfid and cfid of related case
            RelatedPartiesViewModel viewModel = bLL.GetRelatedParties(dataContext.CfId, selectedItemfrmGrid.CfId);
            var uc = new RelatedParties();
            uc.DataContext = viewModel;
            uc.CloseControl = CloseControl;
            relatedCasesGrid = new Grid();
            relatedCasesGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            relatedCasesGrid.Children.Add(lbl);
            relatedCasesGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(relatedCasesGrid);
            }
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(relatedCasesGrid);
            }
        }

        public Grid relatedCasesGrid { get; set; }

        private void ExportToWord_Clicked(object sender, MouseButtonEventArgs e)
        {
            var viewModel = DataContext as RelatedCasesViewModel;

            var relatedCasesList = new List<RelatedCasesExportModel>();
            if (viewModel.RelatedCasesList.Count == 0) return;

            foreach (var item in viewModel.RelatedCasesList)
            {
                var data = new RelatedCasesExportModel
                {
                   CaseNumber=item.CaseNumber,
                   CaseName=item.CaseName,
                   TransactionCount=item.TransactionCount,
                   TotalScore=item.CaseScore,
                   AverageScore=item.AverageScore
                };
                relatedCasesList.Add(data);
            }

            try { DocumentCreator.CreateNewWordDocument(relatedCasesList); }
            catch (Exception ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }
    }
}
