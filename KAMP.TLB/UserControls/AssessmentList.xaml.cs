﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using KAMP.TLB.Models;
using System.Collections.ObjectModel;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for AssessmentList.xaml
    /// </summary>
    public partial class AssessmentList : UserControl
    {
        private int assessmentId;

        //public AssessmentList()
        //{
        //    InitializeComponent();
        //}
        public Action CloseControl;
        AssessmentListViewModel assessmentListViewModel = new AssessmentListViewModel();
        private Grid assessmentFilterGrid;
        public AssessmentList(int assessmentId)
        {
            // TODO: Complete member initialization
            this.assessmentId = assessmentId;
            assessmentListViewModel = LoadParameterList();
            this.DataContext = assessmentListViewModel;
            InitializeComponent();
        }

        private AssessmentListViewModel LoadParameterList()
        {
            var tlbBl = new TLBBLL();
            var parametersColl = new ObservableCollection<Parameters>();
            var parameters = tlbBl.GetParameters(assessmentId);
            return parameters;
        }

        private void LoadSelectedParameterGrid()
        {
            assessmentListViewModel = LoadParameterList();
            this.DataContext = assessmentListViewModel;
        }

        private void ImageDelete_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = GdSelectedComponent.SelectedItem as Parameters;
            if (selectedItem.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
            var res = MessageBoxControl.Show("Are you sure you want to delete?", MessageBoxButton.YesNo);
            if (res == MessageBoxResult.Yes)
            {
                var tlbBl = new TLBBLL();
                tlbBl.DeleteSelectedField(selectedItem);
                LoadSelectedParameterGrid();
            }
        }

        private void ImageAdd_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = GdSelectedComponent.SelectedItem as Parameters;
            if (selectedItem.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
            var uc = new AssessmentField(selectedItem.GroupId, assessmentId);
            uc.CloseControl = CloseFieldControl;
            assessmentFilterGrid = new Grid();
            assessmentFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentFilterGrid.Children.Add(lbl);
            assessmentFilterGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentFilterGrid);
            }
        }

        private void ImageDetail_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = GdSelectedComponent.SelectedItem as Parameters;
            if (selectedItem.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
            //TODO: Depending on parameter group id load country name from TLB_PARAMETERS.
            bool isParameters = false;
            var uc = new EditParameterList(selectedItem.GroupId, isParameters);
            uc.CloseControl = CloseEditParameterControl;
            assessmentFilterGrid = new Grid();
            assessmentFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentFilterGrid.Children.Add(lbl);
            assessmentFilterGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentFilterGrid);
            }
        }

        private void CloseEditParameterControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assessmentFilterGrid);
            }
        }

        private void AddToAssignedParameters(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = GdAvailableParameter.SelectedItem as Parameters;
            if (selectedItem.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
            var uc = new AssessmentField(selectedItem.GroupId, assessmentId);
            uc.CloseControl = CloseFieldControl;
            assessmentFilterGrid = new Grid();
            assessmentFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentFilterGrid.Children.Add(lbl);
            assessmentFilterGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentFilterGrid);
            }
        }

        private void CloseFieldControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assessmentFilterGrid);
            }
            LoadSelectedParameterGrid();
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }
    }
}
