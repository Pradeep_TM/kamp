﻿using KAMP.Core.Repository.TLB;
using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.TLB.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.TLB.AppCode;
    /// <summary>
    /// Interaction logic for LookUps.xaml
    /// </summary> 
    public partial class LookUps : UserControl, ITLBPageControl
    {
        public LookUps()
        {
            InitializeComponent();
        }

        public void InitializeData()
        {
            _viewModel = DataContext as TLBViewModel;
            var bll = new TLBBLL();
            _viewModel.LookUpViewModel.LookUpGroupList = bll.GetLookGroups();
            _viewModel.LookUpViewModel.SelectedGroup = _viewModel.LookUpViewModel.LookUpGroupList.FirstOrDefault();
        }

        private TLBViewModel _viewModel;

        private void LookUpSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ddl = sender as ComboBox;
            if (ddl.SelectedIndex == -1 || ddl.SelectedIndex == 0)
            {
                _viewModel.LookUpViewModel.LookUpsList = null;
            }
            else
            {
                var item = ddl.SelectedItem as LookUpGroups;
                var bLL = new TLBBLL();
                _viewModel.LookUpViewModel.LookUpsList = bLL.GetLookUp(item.Id);
            }
        }

        private void EditLookUp(object sender, MouseButtonEventArgs e)
        {
            var item = DgLookUps.SelectedItem as LookUp;
            var uc = new AddEditLookUp(item, _viewModel.LookUpViewModel.SelectedGroup);
            uc.Close = CloseLookUp;
            lookUpGrid = new Grid();
            lookUpGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            lookUpGrid.Children.Add(lbl);
            lookUpGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(lookUpGrid);
            }

        }

        private void CloseLookUp()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(lookUpGrid);
            }
            var bLL = new TLBBLL();
            _viewModel.LookUpViewModel.LookUpsList = bLL.GetLookUp(_viewModel.LookUpViewModel.SelectedGroup.Id);
            BtnAdd.IsEnabled = true;
        }

        private void DeleteLookUp(object sender, MouseButtonEventArgs e)
        {
            var result = MessageBoxControl.Show("Are You sure you want to delete this look up?", MessageBoxButton.YesNo, MessageType.Alert);
            if (result == MessageBoxResult.No) return;
            var bLL = new TLBBLL();
            var item = DgLookUps.SelectedItem as LookUp;
            bLL.DeleteLookUp(item);
            _viewModel.LookUpViewModel.LookUpsList.Remove(item);
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }



        public Grid lookUpGrid { get; set; }

        private void AddLookUp(object sender, RoutedEventArgs e)
        {
            BtnAdd.IsEnabled = false;
            if (_viewModel.LookUpViewModel.SelectedGroup.Name.Equals(TLBConstants.DefaultDDLValue)) 
            {
                MessageBoxControl.Show("Please select a look up category.",MessageBoxButton.OK,MessageType.Error);
                BtnAdd.IsEnabled = true;
                return;
            }
            var item = DgLookUps.SelectedItem as LookUp;
            var uc = new AddEditLookUp(null, _viewModel.LookUpViewModel.SelectedGroup);
            uc.Close = CloseLookUp;
            lookUpGrid = new Grid();
            lookUpGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            lookUpGrid.Children.Add(lbl);
            lookUpGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(lookUpGrid);
            }
        }


    }
}
