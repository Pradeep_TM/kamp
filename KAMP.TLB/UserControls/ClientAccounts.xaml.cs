﻿using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.UserControls;
using AutoMapper;
using System.Collections.ObjectModel;
using KAMP.TLB.Models;
using DocumentFormat.OpenXml.Packaging;
using KAMP.Core.FrameworkComponents.Helpers;
using System.Diagnostics;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for ClientAccounts.xaml
    /// </summary>
    public partial class ClientAccounts : UserControl
    {
        private Grid transactionCardGrid;
        public ClientAccounts()
        {
            InitializeComponent();
        }

        private void RowDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdClientAccounts.SelectedItem as ClientAccountViewModel;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var transactionViewModel = new TransactionViewModel();
            var tlbBL = new TLBBLL();
            //var trans = tlbBL.GetTransnormByClientName(selectedItemfrmGrid.Account);
            var trans = tlbBL.GetTransactions(selectedItemfrmGrid.CfId, selectedItemfrmGrid.TransactionId);
            transactionViewModel.TransactionList = trans;
            AssessmentTransactions transDetail = new AssessmentTransactions(transactionViewModel);
            transDetail.CloseControl = CloseControl;
            transactionCardGrid = new Grid();
            transactionCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            transactionCardGrid.Children.Add(lbl);
            transactionCardGrid.Children.Add(transDetail);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(transactionCardGrid);
            }
            // var trans = tlbBL.

        }
        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(transactionCardGrid);
            }
        }

        private void ExportToDoc_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var clientAccounts = this.DataContext as ObservableCollection<ClientAccountViewModel>;
            if (clientAccounts.ToList().IsNotNull())
            {
                try
                {
                    var clientAccts = clientAccounts.ToList();
                    if (clientAccts.IsCollectionValid())
                    {
                        var clientAccountModelLst = new List<ClientAccount>();
                        clientAccts.ForEach(x =>
                        {
                            var clientAccount = new KAMP.TLB.Models.ClientAccount()
                            {
                                Account = x.Account,
                                AccountName = x.AccountName,
                                AccountStatus = x.AccountStatus,
                                ClosedDate = x.ClosedDate.ToString("dd-MM-yyyy"),
                                Transactions = x.Transactions,
                                IncomingUsd = x.IncomingUsd,
                                OutgoingUsd = x.OutgoingUsd,
                                BankTransfersUsd = x.BankTransfersUsd
                            };
                            clientAccountModelLst.Add(clientAccount);
                        });
                        DocumentCreator.CreateNewWordDocument(clientAccountModelLst);
                    }

                }
                catch (Exception ex)
                {
                    MessageBoxControl.Show(ex.Message);
                }

            }

        }
    }
}
