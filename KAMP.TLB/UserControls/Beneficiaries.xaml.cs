﻿using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.Models;
using KAMP.Core.FrameworkComponents.Helpers;
using System.Diagnostics;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for Beneficiaries.xaml
    /// </summary>
    public partial class Beneficiaries : UserControl
    {
        private Grid transactionCardGrid;
        public Beneficiaries()
        {
            InitializeComponent();
        }

        private void RowDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdBeneficiaries.SelectedItem as Beneficiary;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var transactionViewModel = new TransactionViewModel();
            var tlbBL = new TLBBLL();
            int cfid = selectedItemfrmGrid.CfId;
            //  var trans = tlbBL.GetTransactionsByCaseNo(cfid);
            var trans = tlbBL.GetTransactions(cfid, selectedItemfrmGrid.TransactionIds);
            transactionViewModel.TransactionList = trans;
            AssessmentTransactions transDetail = new AssessmentTransactions(transactionViewModel);
            transDetail.CloseControl = CloseControl;
            transactionCardGrid = new Grid();
            transactionCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            transactionCardGrid.Children.Add(lbl);
            transactionCardGrid.Children.Add(transDetail);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(transactionCardGrid);
            }

        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(transactionCardGrid);
            }
        }

        private void ExportToExcel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var beneficiaries = this.DataContext as BeneficiariesViewModel;
            if (beneficiaries.IsNotNull() && beneficiaries.Beneficiary.IsCollectionValid())
            {
                var exportBeneficiaries = beneficiaries.Beneficiary.ToList();
                var beneficiaryExportVM = new List<BeneficiaryExportViewModel>();
                if (exportBeneficiaries.IsCollectionValid())
                {
                    exportBeneficiaries.ForEach(x =>
                    {
                        var beneExpVM = new BeneficiaryExportViewModel()
                        {
                            Beneficiaries = x.Beneficiaries,
                            AvgScore = x.AvgScore,
                            FirstTransaction = x.FirstTransaction.ToString("dd-MM-yyyy"),
                            LastTransaction = x.LastTransaction.ToString("dd-MM-yyyy"),
                            Score = x.Score,
                            TotalAmount = x.TotalAmount,
                            Transactions = x.Transactions
                        };
                        beneficiaryExportVM.Add(beneExpVM);
                    });

                    // Context ctx = Application.Current.Properties["Context"] as Context;
                    // var rand = new Random();
                    //var folder = @"C:\\Users\\shreyasbs\\Documents\\Excel\\Beneficiaries{0}{1}{2}.xlsx".ToFormat(ctx.User.Id, ctx.Module.Id, rand.Next());
                    try { Export.CreateExcelDocument<BeneficiaryExportViewModel>(beneficiaryExportVM); }
                    catch (Exception ex)
                    {
                        MessageBoxControl.Show(ex.Message);
                    }

                }

            }
        }

        private void ExportToWord_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var beneficiaries = this.DataContext as BeneficiariesViewModel;
            if (beneficiaries.IsNotNull() && beneficiaries.Beneficiary.IsCollectionValid())
            {
                var exportBeneficiaries = beneficiaries.Beneficiary.ToList();
                var beneficiaryExportVM = new List<BeneficiaryExportViewModel>();
                if (exportBeneficiaries.IsCollectionValid())
                {
                    exportBeneficiaries.ForEach(x =>
                    {
                        var beneExpVM = new BeneficiaryExportViewModel()
                            {
                                Beneficiaries = x.Beneficiaries,
                                AvgScore = x.AvgScore,
                                FirstTransaction = x.FirstTransaction.ToString("dd-MM-yyyy"),
                                LastTransaction = x.LastTransaction.ToString("dd-MM-yyyy"),
                                Score = x.Score,
                                TotalAmount = x.TotalAmount,
                                Transactions = x.Transactions
                            };
                        beneficiaryExportVM.Add(beneExpVM);
                    });
                    //DocumentCreator creator = new DocumentCreator();
                    //Context ctx = Application.Current.Properties["Context"] as Context;
                    //var rand = new Random();
                    //var folder = @"C:\\Users\\shreyasbs\\Documents\\Excel\\Beneficiaries{0}{1}{2}.docx".ToFormat(ctx.User.Id, ctx.Module.Id, rand.Next());
                    try { DocumentCreator.CreateNewWordDocument(beneficiaryExportVM); }
                    catch (Exception ex)
                    {
                        MessageBoxControl.Show(ex.Message);
                    }
                }
            }
        }
    }
}
