﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.BLL;
using KAMP.Core.FrameworkComponents.Helpers;
using System.Diagnostics;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for CategoryParameterHits.xaml
    /// </summary>
    public partial class CategoryParameterHits : UserControl
    {
        public Action CloseControl;
        private ViewModels.AssessmentHitsViewModel assessmentHitsVM;

        public CategoryParameterHits()
        {
            InitializeComponent();
        }

        public CategoryParameterHits(ViewModels.AssessmentHitsViewModel result)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.assessmentHitsVM = result;
            this.DataContext = result;
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        private void ExportToExcel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
           
            var paramGroupId = assessmentHitsVM.SelectedGroupId;
            var tlbBl = new TLBBLL();             
            var result = tlbBl.GetParametersByParamGrpId(paramGroupId);
            try
            {
                Export.CreateExcelDocument<ViewModels.ExportParameterHitsViewModel>(result);
            }
            catch (Exception ex)
            {
                MessageBoxControl.Show(ex.Message);
            }


        }
    }
}

