﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KAMP.Core.FrameworkComponents;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.Workflow;
using KAMP.TLB.BLL;

namespace KAMP.TLB.UserControls 
{
    /// <summary>
    /// Interaction logic for UCAssignReAssign.xaml
    /// </summary>
    public partial class UCAssignReAssign : UserControl
    {
        TLB.Models.CaseDetails currentCase;
        public Action<int,long> OnRefreshAssignedCollection;
        public Action<int,long> OnRefreshUnAssignedCollection;
        public Action CloseWindow;
        public UCAssignReAssign(Boolean IsAssign, TLB.Models.CaseDetails crntCase)
        {
            InitializeComponent();
            currentCase = crntCase;
            if (IsAssign)
            {
                BtnAssign.Visibility = Visibility.Visible;
                BtnReAssign.Visibility = Visibility.Collapsed;
            }
            else
            {
                BtnReAssign.Visibility = Visibility.Visible;
                BtnAssign.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Assign Case
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAssign_Click(object sender, RoutedEventArgs e)
        {
            var caseNo = currentCase.CaseNumber;
            var cfId = currentCase.CfId;
            var context = Application.Current.Properties["Context"] as Context;
            long userId = Convert.ToInt64(ddlUsers.SelectedValue);
            if (userId == 0)
            {
                MessageBoxControl.Show("Please select a user.", MessageBoxButton.OK, MessageType.Error);
                return;
            }
            try
            {
                WorkflowAPI.Assign(caseNo, userId, cfId, context.Module.Id);
                BtnCancel_Click(null, null);
                MessageBoxControl.Show("Case Successfully Assigned", MessageBoxButton.OK, MessageType.Information);
                //Modify CaseFile to add assigned user
                //new TLBBLL().UpdateCaseFileAfterAssign(userId,currentCase,false);
                if(OnRefreshAssignedCollection.IsNotNull())
                    OnRefreshAssignedCollection(currentCase.CfId,userId);
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, MessageType.Error);
            }
        }

        /// <summary>
        /// Re-Assign Case
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReAssign_Click(object sender, RoutedEventArgs e)
        {
             var context = Application.Current.Properties["Context"] as Context;
            var caseNo = currentCase.CaseNumber;
            var cfId = currentCase.CfId;
            long userId = Convert.ToInt64(ddlUsers.SelectedValue);
            if (userId == 0)
            {
                MessageBoxControl.Show("Please select a user.", MessageBoxButton.OK, MessageType.Error);
                return;
            }
            try
            {   
                WorkflowAPI.ReAssign(caseNo,userId,context.Module.Id);
                BtnCancel_Click(null, null);
                MessageBoxControl.Show("Case Successfully Re-Assigned", MessageBoxButton.OK, MessageType.Information);
                //Modify CaseFile to add assigned user
                //new TLBBLL().UpdateCaseFileAfterAssign(userId, currentCase, true);
                if (OnRefreshUnAssignedCollection.IsNotNull())
                    OnRefreshUnAssignedCollection(currentCase.CfId, userId);
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, MessageType.Error);
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (CloseWindow.IsNotNull())
                CloseWindow();
        }
    }
}
