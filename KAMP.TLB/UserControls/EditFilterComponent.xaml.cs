﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.BLL;
using KAMP.Core.Repository.Models.TLB;
using KAMP.TLB.Models;
using AutoMapper;
using KAMP.TLB.ViewModels;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for EditFilterComponent.xaml
    /// </summary>
    public partial class EditFilterComponent : UserControl, INotifyPropertyChanged
    {
        public Action CloseControl;
        private AvailableComponent _availableComponent = new AvailableComponent();
        public AvailableComponent availableComponent
        {
            get { return _availableComponent; }
            set { PropertyChanged.HandleValueChange(this, value, ref _availableComponent, (x) => x.availableComponent); }
        }

        public EditFilterComponent(int fcrId, int assessmentId)
        {
            InitializeComponent();
            var filterCriteria = GetFilterDetail(fcrId);
            if (filterCriteria.IsNotNull())
            {
                filterCriteria.AssessmentId = assessmentId;
            }
            else
            {
                filterCriteria = new AvailableComponent() { AssessmentId = assessmentId };
            }
            availableComponent = filterCriteria;
            this.DataContext = availableComponent;
            filterCriteria.OnContinueClick += OnContinueClick;
        }

        private void OnContinueClick()
        {
            var errorMessages = ValidateFilterCriteria();
            if (errorMessages.IsCollectionValid())
            {
                var sb = new StringBuilder();
                errorMessages.ForEach(x => sb.AppendLine(x));
                MessageBoxControl.Show(sb.ToString(), MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                AddFilterCriteria(availableComponent);
            }
        }

        private void AddFilterCriteria(AvailableComponent availableComponent)
        {
            var tlbBl = new TLBBLL();
            //availableComponent.FcrId = 0;
            tlbBl.AddFilterCriteria(availableComponent);
            MessageBoxControl.Show("Saved Successfully.", MessageBoxButton.OK, MessageType.Information);
            if (CloseControl.IsNotNull())
                CloseControl();

        }

        private List<string> ValidateFilterCriteria()
        {
            var ctx = this.DataContext as AvailableComponent;
            var errorMessages = new List<string>();
            var errorMessage = string.Empty;
            if (availableComponent.Field.IsEmpty())
            {
                errorMessage = "Please select a Field.";
                errorMessages.Add(errorMessage);
            }
            if (availableComponent.Operator.IsNull() || availableComponent.Operator.IsEmpty())
            {
                errorMessage = "Operator is mandatory.";
                errorMessages.Add(errorMessage);
            }

            if (availableComponent.Operator.IsNotNull() && availableComponent.Operator.Length > 4)
            {
                errorMessage = "Operator cannot be more than 4 character long";
                errorMessages.Add(errorMessage);
            }
            if (!availableComponent.Order.HasValue)
            {
                errorMessage = "Order is mandatory.";
                errorMessages.Add(errorMessage);
            }

            if (availableComponent.Type.IsNull() || availableComponent.Type.IsEmpty())
            {
                errorMessage = "Type is mandatory.";
                errorMessages.Add(errorMessage);
            }
            if (availableComponent.Criteria.IsNull() || availableComponent.Criteria.IsEmpty())
            {
                errorMessage = "Criteria is mandatory.";
                errorMessages.Add(errorMessage);
            }
            return errorMessages;
        }

        private AvailableComponent GetFilterDetail(int fcrId)
        {
            var tlbBl = new TLBBLL();
            var filterCriteria = tlbBl.GetFilterCriteria(fcrId);
            var fields = tlbBl.GetFieldsFromSysTable();
            if (filterCriteria.IsNotNull())
            {
                var mappedFilter = Mapper.Map<FilterCriteria, AvailableComponent>(filterCriteria);
                mappedFilter.FieldsViewModels = fields;
                return mappedFilter;
            }
            else
            {
                var availableComponent = new AvailableComponent();
                availableComponent.FieldsViewModels = fields;
                return availableComponent;
            }


        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OrderTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textField = sender as TextBox;
            var regex = Regex.IsMatch(textField.Text, "[\\s]");
            if (regex)
            {
                MessageBoxControl.Show("Order should not have space as input.", MessageBoxButton.OK, MessageType.Alert);
                BtnEditFilters.IsEnabled = false;
                return;
            }
            if (textField.Text.IsEmpty() || textField.Text == " ")
            {
                MessageBoxControl.Show("Order is mandatory.", MessageBoxButton.OK, MessageType.Alert);
                BtnEditFilters.IsEnabled = false;
                return;
            }
            else
            {
                var textValue = Convert.ToInt64(textField.Text);
                var regexMatch = Regex.IsMatch(textField.Text, "^[0-9+]*$");
                if (textField.Text == "")
                {
                    textField.Text = null;
                    MessageBoxControl.Show("Order is mandatory.", MessageBoxButton.OK, MessageType.Alert);
                    BtnEditFilters.IsEnabled = false;
                    return;
                }
                else if (!regexMatch)
                {
                    //textField.Text = null;
                    MessageBoxControl.Show("Order must be numbers only.", MessageBoxButton.OK, MessageType.Alert);
                    BtnEditFilters.IsEnabled = false;
                }
                else if (textValue > int.MaxValue)
                {
                    MessageBoxControl.Show("Order is too big.Please enter a smaller value",MessageBoxButton.OK,MessageType.Alert);
                    BtnEditFilters.IsEnabled = false;
                    return;
                }
                BtnEditFilters.IsEnabled = true;
            }


        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]+"))
                e.Handled = true;
        }

        private void TextBox_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (!Regex.IsMatch(e.DataObject.GetData(typeof(string)).ToString(), "[^0-9]+"))
            {
                e.CancelCommand();
            }
        }

        //private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    var textField = sender as TextBox;
        //    if (textField.Text.IsNull() || textField.Text.IsEmpty())
        //        MessageBoxControl.Show("Order is mandatory.");
        //}
    }
}
