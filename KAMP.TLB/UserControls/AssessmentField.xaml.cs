﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for AssessmentField.xaml
    /// </summary>
    public partial class AssessmentField : UserControl, INotifyPropertyChanged
    {
        public Action CloseControl;
        ChooseFieldsViewModel chooseFieldViewModel = new ChooseFieldsViewModel();

        public AssessmentField(int parameterGroupId = 0, int assessmentId = 0)
        {
            InitializeComponent();
            chooseFieldViewModel.ParameterGroupId = parameterGroupId;
            chooseFieldViewModel.AssessmentId = assessmentId;
            chooseFieldViewModel.FieldsViewModel = GetFields();
            this.DataContext = chooseFieldViewModel;
            chooseFieldViewModel.OnSaveClick += OnSaveClick;
        }

        private void OnSaveClick(string fieldName)
        {
            if (fieldName.IsNull())
                MessageBoxControl.Show("Please select a field name", MessageBoxButton.OK, MessageType.Error);
            else
            {
                chooseFieldViewModel.Field = fieldName;
                var tlbBl = new TLBBLL();
                tlbBl.AddAssessmentField(chooseFieldViewModel);
                if (CloseControl.IsNotNull())
                    CloseControl();
            }

        }

        private ObservableCollection<FieldsViewModel> GetFields()
        {
            var tlbBl = new TLBBLL();
            var fieldVM = new ObservableCollection<FieldsViewModel>();
            var fields = tlbBl.GetFieldsFromSysTable();
            foreach (var field in fields)
            {
                fieldVM.Add(field);
            }
            return fieldVM;
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
