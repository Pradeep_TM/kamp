﻿using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for RiskAssessmentParameters.xaml
    /// </summary>
    public partial class RiskAssessmentParameters : UserControl, ITLBPageControl
    {
        private Grid assessmentFilterGrid;
        public RiskAssessmentParameters()
        {
            InitializeComponent();
        }

        public void InitializeData()
        {
            bool isParameters = true;
            bool isVisible = false;
            var groupId = 0;
            var uc = new EditParameterList(groupId, isParameters,isVisible);
            gdRiskParameter.Children.Add(uc);
        }

        private void CloseEditParameterControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assessmentFilterGrid);  
            }
        }
    }
}
