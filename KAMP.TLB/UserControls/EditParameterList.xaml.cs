﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.ViewModels;
using KAMP.TLB.BLL;
using KAMP.TLB.Models;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for EditParameterList.xaml
    /// </summary>
    public partial class EditParameterList : UserControl, ITLBPageControl
    {
        public Action CloseControl;
        private Grid assessmentFilterGrid;
        EditParameterListViewModel editParamLstVm = new EditParameterListViewModel() { RiskParamters = new System.Collections.ObjectModel.ObservableCollection<Models.RiskParameter>(), ParameterGroups = new System.Collections.ObjectModel.ObservableCollection<Core.Repository.TLB.TLBParameterGroups>() };
        public EditParameterList(int parameterGroupId = 0, bool isParameters = true, bool isVisible = true)
        {
            InitializeComponent();
            editParamLstVm.ParameterGroupId = parameterGroupId;
            editParamLstVm.IsParameters = isParameters;
            editParamLstVm.IsVisible = isVisible;
            editParamLstVm.IsCategorySelected += IsCategorySelected;
            editParamLstVm.OnNewClick += OnNewClick;
            var parameters = GetParameters(editParamLstVm);
            parameters.ParameterGroups.Add(new Core.Repository.TLB.TLBParameterGroups { Id = 0, GroupName = "-Select-" });
            this.DataContext = parameters;
        }

        private void OnNewClick()
        {
            if (editParamLstVm.IsParameters)
            {
                if (editParamLstVm.ParameterGroupId != 0)
                {
                    editParamLstVm.Id = 0;
                    var uc = new ModifyParameters(editParamLstVm);
                    uc.CloseControl = CloseFilterControl;
                    //uc.OnSaveClick(0);
                    assessmentFilterGrid = new Grid();
                    assessmentFilterGrid.Background = Brushes.Transparent;
                    Label lbl = new Label();
                    lbl.Background = Brushes.Black;
                    lbl.Opacity = .8;
                    assessmentFilterGrid.Children.Add(lbl);
                    assessmentFilterGrid.Children.Add(uc);
                    var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                    if (container.IsNotNull())
                    {
                        container.Children.Add(assessmentFilterGrid);
                    }
                }
                else
                {
                    MessageBoxControl.Show("Please select a parameter group", MessageBoxButton.OK, MessageType.Error);
                }
            }
        }

        private void IsCategorySelected()
        {  
            var tlbBl = new TLBBLL();
            var parameters = tlbBl.GetRiskParameters(editParamLstVm.ParameterGroupId);
            editParamLstVm.RiskParamters = parameters;
            InitializeData();
        }

        private EditParameterListViewModel GetParameters(EditParameterListViewModel editParamLstVm)
        {
            var tlbBl = new TLBBLL();
            var editParameterLstVM = tlbBl.GetParameters(editParamLstVm);
            return editParameterLstVM;
        }

        private void CloseFilterControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assessmentFilterGrid);
            }
            IsCategorySelected();
        }

        private void LoadDataGrid()
        {
            IsCategorySelected();
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }
        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        private void ImageEdit_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

            if (editParamLstVm.IsParameters)
            {
                var selectedItemfrmGrid = GdParameters.SelectedItem as RiskParameter;
                if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
                editParamLstVm.Id = selectedItemfrmGrid.Id;
                editParamLstVm.IsParameters = true;
                var uc = new ModifyParameters(editParamLstVm);
                uc.CloseControl = CloseFilterControl;
                assessmentFilterGrid = new Grid();
                assessmentFilterGrid.Background = Brushes.Transparent;
                Label lbl = new Label();
                lbl.Background = Brushes.Black;
                lbl.Opacity = .8;
                assessmentFilterGrid.Children.Add(lbl);
                assessmentFilterGrid.Children.Add(uc);
                var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                if (container.IsNotNull())
                {
                    container.Children.Add(assessmentFilterGrid);
                }
            }
        }

        private void ImageDelete_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (editParamLstVm.IsParameters)
            {
                var selectedItemfrmGrid = GdParameters.SelectedItem as RiskParameter;
                if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please select a row.", MessageBoxButton.OK, MessageType.Error);
                var res = MessageBoxControl.Show("Are you sure you want to delete?", MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    var tlbBl = new TLBBLL();
                    tlbBl.DeleteParameter(selectedItemfrmGrid.Id);
                    IsCategorySelected();
                }
            }
        }

        public void InitializeData()
        {
            this.DataContext = editParamLstVm;
        }

    }
}
