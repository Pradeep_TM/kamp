﻿using KAMP.TLB.Models;
using KAMP.TLB.ViewModels;
using KAMP.TLB.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KAMP.Core.FrameworkComponents;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for UCCaseManagement.xaml   
    /// </summary>
    ///
    using KAMP.Core.Workflow;
    using KAMP.Core.Workflow.UserControls;
    using KAMP.Core.Workflow.ViewModels;
    using KAMP.TLB.BLL;
    using System.Collections.ObjectModel;
    using KAMP.Core.Repository.WF;
    using KAMP.Core.Workflow.Helpers;
    using KAMP.TLB.AppCode;
    public partial class CaseManagement : UserControl, ITLBPageControl
    {
        private TLBViewModel _viewModel;
        private Grid caseFileCardGrid;
        private CaseFileCard uc;
        public CaseManagement()
        {
            InitializeComponent();
        }

        public void pageControl_PreviewPageChange(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        public void pageControl_PageChanged(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {

            OpenCase();
        }

        private void OpenCase()
        {
            var selectedItemfrmGrid = GdCaseManagement.SelectedItem as CaseDetails;


            if (selectedItemfrmGrid.IsNull())
            {
                MessageBoxControl.Show("Please Select A Case", MessageBoxButton.OK, MessageType.Error);
                return;
            }
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.Children.Contains(caseFileCardGrid)) return;
            CaseFileCardViewModel casefileCardViewModel = new CaseFileCardViewModel(selectedItemfrmGrid);
            casefileCardViewModel.OpenWorkflowProcess += OpenWorkflow;
            uc = new CaseFileCard(casefileCardViewModel);
            uc.CloseControl = CloseControl;
            uc.UpdateCaseAfterOpening += UpdateCaseAfterOpening;
            caseFileCardGrid = new Grid();
            caseFileCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            caseFileCardGrid.Children.Add(lbl);
            caseFileCardGrid.Children.Add(uc);
            if (container.IsNotNull())
            {
                container.Children.Add(caseFileCardGrid);
            }

            var context = Application.Current.Properties["Context"] as Context;
            if (context.User.Id != selectedItemfrmGrid.User.UserId)
            {
                casefileCardViewModel.DGSelectedItem.IsOpen = null;
                uc.ucDispositions.StkPnlDueDiligence.IsEnabled = false;
                uc.ucDispositions.StkPnlRiskPrifile.IsEnabled = false;
                uc.ucDispositions.StkPnlMOF.IsEnabled = false;
                uc.ucDispositions.StkPnlExecSummary.IsEnabled = false;
                //uc.UCClientAccounts.imgExport.Visibility = Visibility.Collapsed;
                //uc.UCTransactions.tdImgExport.Visibility = Visibility.Collapsed;
                //uc.UCBeneficiaries.stkPnlExport.Visibility = Visibility.Collapsed;
                //uc.OrderingParty.stkPnlExport.Visibility = Visibility.Collapsed;
                //uc.ucRelatedCases.rcImgExport.Visibility = Visibility.Collapsed;
                //uc.UCCaseHistory.hstImgExport.Visibility = Visibility.Collapsed;
            }
            if (casefileCardViewModel.DGSelectedItem.IsOpen == false || casefileCardViewModel.DGSelectedItem.IsOpen == null)
            {
                uc.ucDispositions.TxtDueDiligence.IsEnabled = false;
                uc.ucDispositions.TxtExecutiveSummary.IsEnabled = false;
                uc.ucDispositions.TxtMamorandumofFact.IsEnabled = false;
                uc.ucDispositions.TxtRiskProfile.IsEnabled = false;
                uc.ucDispositions.StkPnlDueDiligence.Visibility = Visibility.Collapsed;
                uc.ucDispositions.StkPnlRiskPrifile.Visibility = Visibility.Collapsed;
                uc.ucDispositions.StkPnlMOF.Visibility = Visibility.Collapsed;
                uc.ucDispositions.StkPnlExecSummary.Visibility = Visibility.Collapsed;
                //uc.UCClientAccounts.imgExport.Visibility = Visibility.Collapsed;
                //uc.UCTransactions.tdImgExport.Visibility = Visibility.Collapsed;
                //uc.UCBeneficiaries.stkPnlExport.Visibility = Visibility.Collapsed;
                //uc.OrderingParty.stkPnlExport.Visibility = Visibility.Collapsed;
                //uc.ucRelatedCases.rcImgExport.Visibility = Visibility.Collapsed;
                //uc.UCCaseHistory.hstImgExport.Visibility = Visibility.Collapsed;
            }
        }

        private void UpdateCaseAfterOpening(CaseDetails currentCase)
        {
            var collection = pageControl.ItemsSource.Cast<CaseDetails>().ToList();
            var oldCase = collection.FirstOrDefault(x => x.CaseNumber == currentCase.CaseNumber);
            collection.Remove(oldCase);
            collection.Add(currentCase);
            var orderedCollection = collection.OrderBy(x => x.CaseNumber).ToList();
            var assignedCol = new ObservableCollection<object>();
            orderedCollection.ForEach(assignedCol.Add);
            pageControl.ItemsSource = assignedCol;
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(caseFileCardGrid);
            }
        }

        private void GdUnassignedCases_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            //return;
            //var selectedItemfrmGrid = GdUnassignedCases.SelectedItem as CaseDetails;
            //CaseFileCardViewModel casefileCardViewModel = new CaseFileCardViewModel()
            //{
            //    DGSelectedItem = selectedItemfrmGrid
            //};
            //var _win = new Window();
            //_win.WindowState = WindowState.Maximized;
            //_win.AllowsTransparency = true;
            //_win.WindowStyle = WindowStyle.None;
            //_win.Background = Brushes.Transparent;
            //_win.VerticalAlignment = VerticalAlignment.Center;
            //var uc = new CaseFileCard(casefileCardViewModel);
            //Grid gd = new Grid();
            //gd.Background = Brushes.Transparent;
            //Label lbl = new Label();
            //lbl.Background = Brushes.Black;
            //lbl.Opacity = .5;
            //gd.Children.Add(lbl);
            //gd.Children.Add(uc);
            //_win.Content = gd;
            //_win.ShowDialog();  
        }

        public void InitializeData()
        {
            _viewModel = DataContext as TLBViewModel;
            _viewModel.CaseManagementViewModel.OpenWorkflowProcess = OpenWorkflow;
            GdCaseManagement.UnselectAll();
            _viewModel.CaseManagementViewModel.DGSelectedItem = new CaseDetails();
        }

        private void OpenWorkflow(string caseNo)
        {
            if (wfProcessGrid.IsNotNull())
                return;
            var uc = new UCWorkflowAction();
            var viewModel = new WorkflowActionVm
            {
                CaseNo = caseNo,
            };
            uc.DataContext = viewModel;
            uc.InitializedData();
            viewModel.ClosePopUp += Close;
            viewModel.PostAction += UpdateCase;
            wfProcessGrid = new Grid();
            wfProcessGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            wfProcessGrid.Children.Add(lbl);
            wfProcessGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(wfProcessGrid);
            }
        }

        private void Close()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(wfProcessGrid);
                wfProcessGrid = null;
            }
            

        }
        private void UpdateCase(string caseNo)
        {
            new BootStrapper().Configure();
            var bll = new TLBBLL();
            var updatedCase = bll.GetCaseDetails(caseNo);

            //Updating Main Collection
            var oldCase = TLBViewModel.CaseList.FirstOrDefault(x => x.CaseNumber == caseNo);
            TLBViewModel.CaseList.Remove(oldCase);
            TLBViewModel.CaseList.Add(updatedCase);

            //Updating Grid Data
            var collection = pageControl.ItemsSource.Cast<CaseDetails>().ToList();
            collection.Remove(collection.FirstOrDefault(x => x.CaseNumber == caseNo));
            collection.Add(updatedCase);
            collection.OrderBy(x => x.CaseNumber);
            var assignedCol = new ObservableCollection<object>();
            collection.ForEach(assignedCol.Add);
            pageControl.ItemsSource = assignedCol;

            CloseControl();
        }
        public Grid wfProcessGrid { get; set; }

        private void MyCasesClicked(object sender, RoutedEventArgs e)
        {

            BtnMyCases.Visibility = Visibility.Collapsed;
            BtnAllCases.Visibility = Visibility.Visible;

            var context = Application.Current.Properties["Context"] as Context;
            var userId = context.User.Id;

            var bLL = new TLBBLL();
            var cases = bLL.GetCasesByIndexandFilters(0, pageControl.PageSizes.FirstOrDefault(), null, null, userId).ToList();

            var filter = new CaseFilters();
            filter.userId = userId;
            var casesCol = new ObservableCollection<object>();
            cases.ForEach(casesCol.Add);
            if (cases.Count == 0)
            {
                pageControl.Page = 0;
                pageControl.TotalPages = 0;
            }

            else
            {
                pageControl.Page = 1;
                pageControl.TotalPages = (uint)Math.Ceiling((double)TLBViewModel.CaseList.Where(x => x.User.UserId == userId).ToList().Count / pageControl.PageSizes.FirstOrDefault());

            }
            pageControl.ItemsSource = casesCol;
            pageControl.FilterTag = filter;
            GdCaseManagement.UnselectAll();
            _viewModel.CaseManagementViewModel.DGSelectedItem = new CaseDetails();
        }

        private void AllCasesClicked(object sender, RoutedEventArgs e)
        {
            BtnAllCases.Visibility = Visibility.Collapsed;
            BtnMyCases.Visibility = Visibility.Visible;

            var context = Application.Current.Properties["Context"] as Context;
            var userId = context.User.Id;

            var bLL = new TLBBLL();
            var cases = bLL.GetCasesByIndexandFilters(0, pageControl.PageSizes.FirstOrDefault()).Where(x => x.User.UserId != 0).ToList();

            var casesCol = new ObservableCollection<object>();
            cases.ForEach(casesCol.Add);
            pageControl.FilterTag = null;
            if (cases.Count == 0)
            {
                pageControl.Page = 0;
                pageControl.TotalPages = 0;
            }

            else
            {
                pageControl.Page = 1;
                pageControl.TotalPages = (uint)Math.Ceiling((double)TLBViewModel.CaseList.Where(x => x.User.UserId != 0).ToList().Count / pageControl.PageSizes.FirstOrDefault());

            }
            pageControl.ItemsSource = casesCol;

            GdCaseManagement.UnselectAll();
            _viewModel.CaseManagementViewModel.DGSelectedItem = new CaseDetails();
        }

        private void ViewCaseClicked(object sender, RoutedEventArgs e)
        {
            OpenCase();
        }
    }
}
