﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.TLB.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.TLB;
    using KAMP.TLB.BLL;
    /// <summary>
    /// Interaction logic for AddEditLookUp.xaml
    /// </summary>
    public partial class AddEditLookUp : UserControl
    {
        LookUp lookUpItem;
        public Action Close;

        public AddEditLookUp(Core.Repository.TLB.LookUp item, LookUpGroups group)
        {
            InitializeComponent();
            if (item.IsNull())
            {
                lookUpItem = new Core.Repository.TLB.LookUp();
                isAdd = true;
                BtnAddUpdate.Content = "Add";
            }
            else
            {
                lookUpItem = item;
                BtnAddUpdate.Content = "Update";
            }
            lookUpItem.LookGroupId = group.Id;
            DataContext = lookUpItem;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (Close.IsNotNull())
            {
                Close();
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (lookUpItem.IsNull() || lookUpItem.Value.IsNull() || string.IsNullOrEmpty(lookUpItem.Value.Trim()))
            {
                MessageBoxControl.Show("Look Up value can not be empty.", MessageBoxButton.OK, MessageType.Error);
                return;
            }
            var bLL = new TLBBLL();
            if (isAdd)//Add
            {
                bLL.AddLookUp(lookUpItem);
                MessageBoxControl.Show("Look up added successfully");
            }
            else
            {
                bLL.EditLookUp(lookUpItem);
                MessageBoxControl.Show("Look up updated successfully");
            }

            txtReference.Text = string.Empty;
            txtValue.Text = string.Empty;
        }



        public bool isAdd { get; set; }
    }
}
