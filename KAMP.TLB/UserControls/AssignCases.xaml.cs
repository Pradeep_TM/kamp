﻿using KAMP.TLB.AppCode;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using KAMP.Core.FrameworkComponents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KAMP.Core.Workflow.UserControls;
using KAMP.TLB.BLL;
using KAMP.TLB.Models;
using System.Windows.Media.Animation;
using KAMP.Core.Workflow.Helpers;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for AssignCases.xaml
    /// </summary>
    public partial class AssignCases : UserControl, ITLBPageControl
    {
        private double _height;
        private TLBViewModel _viewModel;
        public AssignCases()
        {
            InitializeComponent();
            Loaded += AssignCases_Loaded;
        }

        private void AssignCases_Loaded(object sender, RoutedEventArgs e)
        {
            BorderAssigned_PreviewMouseDown(null, null);
            IsUnassignedTabOpened = true;
        }


        public void pageControl_PreviewPageChange(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        public void pageControl_PageChanged(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        public void pageControlAsignedCases_PreviewPageChange(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        public void pageControlAsignedCases_PageChanged(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        private void BtnAssign_Click(object sender, RoutedEventArgs e)
        {
            AssignReAssignCase(true, GdUnassignedCases);
        }

        private void AssignReAssignCase(bool isAssign, DataGrid grid)
        {
            var selectedCase = grid.SelectedItem as TLB.Models.CaseDetails;
            if (selectedCase == null)
            {
                MessageBoxControl.Show("Please select a Case", MessageBoxButton.OK, MessageType.Error);
                return;
            }
            if (selectedCase.Status.Trim().ToLower().Equals(Helper.Status.End.Trim().ToLower()))
            {
                MessageBoxControl.Show("The case has been closed.", MessageBoxButton.OK, MessageType.Information);
                return;
            }
             var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if(container.Children.Contains(assignCasesGrid)) return;
            //if (assignCasesGrid.IsNotNull() && assignCasesGrid.Children.Count > 0) return;
            var bLL = new TLBBLL();
            var ucAssignReAssign = new UCAssignReAssign(isAssign, selectedCase);
            ucAssignReAssign.CloseWindow += CloseWindow;
            ucAssignReAssign.OnRefreshAssignedCollection += OnRefreshAssignedCase;
            ucAssignReAssign.OnRefreshUnAssignedCollection += OnRefreshUnAssignedCase;
            var dataContext = this.DataContext as TLBViewModel;
            dataContext.AssignCasesTabViewModel.UserStatusMappedList = null;
            if (selectedCase.User.UserId == 0 || selectedCase.State.Id == 0)
                dataContext.AssignCasesTabViewModel.UserStatusMappedList = bLL.GetUsersFromAnalystGroup().Select(x => new TLBUser { UserId = x.UserId, UserName = x.UserName }).OrderBy(x => x.UserName).ToList();
            else
                dataContext.AssignCasesTabViewModel.UserStatusMappedList = bLL.GetUserByState(selectedCase.State.Id).Select(x => new TLBUser { UserId = x.UserId, UserName = x.UserName }).OrderBy(x => x.UserName).ToList();
            dataContext.AssignCasesTabViewModel.UserStatusMappedList.Insert(0, new Models.TLBUser { UserId = 0, UserName = TLBConstants.DefaultDDLValue });
            ucAssignReAssign.DataContext = this.DataContext;

            assignCasesGrid = new Grid();
            assignCasesGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assignCasesGrid.Children.Add(lbl);
            assignCasesGrid.Children.Add(ucAssignReAssign);
           
            if (container.IsNotNull())
            {
                container.Children.Add(assignCasesGrid);
            }
        }

        private void CloseWindow()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assignCasesGrid);
            }
        }

        private void OnRefreshUnAssignedCase(int cfId, long userId)
        {
            var dataContext = DataContext as TLBViewModel;
            var user = dataContext.UserList.FirstOrDefault(x => x.UserId == userId);
            TLBViewModel.CaseList.FirstOrDefault(x => x.CfId == cfId).User = user;
            TLBViewModel.CaseList.FirstOrDefault(x => x.CfId == cfId).Analyst = user.UserName;
            var Assignedcollection = pageControlAsignedCases.ItemsSource.Cast<CaseDetails>().ToList();
            Assignedcollection.FirstOrDefault(x => x.CfId == cfId).User = dataContext.UserList.FirstOrDefault(x => x.UserId == userId);
            var assignedCol = new ObservableCollection<object>();
            Assignedcollection.ForEach(assignedCol.Add);
            pageControlAsignedCases.ItemsSource = assignedCol;
        }

        private void OnRefreshAssignedCase(int cfId, long userId)
        {
            var dataContext = DataContext as TLBViewModel;
            var user = dataContext.UserList.FirstOrDefault(x => x.UserId == userId);
            var currentCase = TLBViewModel.CaseList.FirstOrDefault(x => x.CfId == cfId);
            currentCase.User = user;
            currentCase.IsOpen = false;
            currentCase.Status = Helper.Status.Assigned;
            currentCase.State = new Core.Repository.WF.State { Id = 0, StateName = Helper.Status.Assigned };
            currentCase.HighestStatus.StateName = Helper.Status.Assigned;
            currentCase.Analyst = user.UserName;
            var UnAssignedcollection = pageControl.ItemsSource.Cast<CaseDetails>().ToList();
            var Assignedcollection = pageControlAsignedCases.ItemsSource.Cast<CaseDetails>().ToList();
            var caseAssigned = UnAssignedcollection.FirstOrDefault(x => x.CfId == cfId);
            caseAssigned.Status = Helper.Status.Assigned;
            caseAssigned.User = dataContext.UserList.FirstOrDefault(x => x.UserId == userId);
            Assignedcollection.Add(caseAssigned);
            UnAssignedcollection.Remove(caseAssigned);
            var unAssignedCol = new ObservableCollection<object>();
            UnAssignedcollection.ForEach(unAssignedCol.Add);
            var assignedCol = new ObservableCollection<object>();
            Assignedcollection.ForEach(assignedCol.Add);
            pageControl.ItemsSource = unAssignedCol;
            pageControlAsignedCases.ItemsSource = assignedCol;
            _viewModel.AssignCasesTabViewModel.AssignedCasesList.Add(caseAssigned.CaseNumber);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AssignReAssignCase(false, GdAssignedCases);
        }


        #region Filters
        internal void FilterSearchUnassignedCases()
        {
            var filters = new CaseFilters();
            var tlbl = new TLBBLL();

            int? categoryId = null;
            if (_viewModel.AssignCasesTabViewModel.SelectedUnassignedCategory.IsNotNull() && _viewModel.AssignCasesTabViewModel.SelectedUnassignedCategory.Id != 0)
            {
                categoryId = _viewModel.AssignCasesTabViewModel.SelectedUnassignedCategory.Id;
                filters.CategoryId = categoryId;
            }
            else
            {
                filters.CategoryId = null;
            }
            var cases = tlbl.GetCasesByIndexandFilters(0, pageControlAsignedCases.PageSizes.FirstOrDefault(), false, categoryId);

            var obitem = new ObservableCollection<object>();
            foreach (var item in cases)
                obitem.Add((object)item);
            pageControl.FilterTag = filters;
            if (cases.Count == 0)
            {
                pageControl.Page = 0;
                pageControl.TotalPages = 0;
            }
            else
            {
                pageControl.Page = 1;
                pageControl.TotalPages = (uint)Math.Ceiling((double)tlbl.GetPageSizeAfterFilters(false, categoryId) / pageControl.PageSizes.FirstOrDefault());
            }

            pageControl.ItemsSource = obitem;

        }

        internal void FilterSearch()
        {
            var filters = new CaseFilters();
            var tlbl = new TLBBLL();
            int? categoryId = null;
            int? stateId = null;
            long? userId = null;
            string caseNumber = null;
            if (_viewModel.AssignCasesTabViewModel.SelectedCategory.IsNotNull() && _viewModel.AssignCasesTabViewModel.SelectedCategory.Id != 0)
            {
                categoryId = _viewModel.AssignCasesTabViewModel.SelectedCategory.Id;
                filters.CategoryId = categoryId;
            }
            else
            {
                filters.CategoryId = null;
            }
            if (_viewModel.AssignCasesTabViewModel.SelectedStatus != 0)
            {
                stateId = _viewModel.AssignCasesTabViewModel.SelectedStatus;
                filters.stateId = stateId;
            }
            else
            {
                filters.stateId = null;
            }

            if (_viewModel.AssignCasesTabViewModel.SelectedUser != 0)
            {
                userId = _viewModel.AssignCasesTabViewModel.SelectedUser;
                filters.userId = userId;
            }
            else
            {
                filters.userId = null;
            }
            if (_viewModel.AssignCasesTabViewModel.SelectedCaseNo.IsNotNull() && _viewModel.AssignCasesTabViewModel.SelectedCaseNo != TLBConstants.DefaultDDLValue)
            {
                caseNumber = _viewModel.AssignCasesTabViewModel.SelectedCaseNo;
                filters.caseNumber = caseNumber;
            }
            else
            {
                filters.caseNumber = null;
            }
            var cases = tlbl.GetCasesByIndexandFilters(0, pageControlAsignedCases.PageSizes.FirstOrDefault(), true, categoryId, userId, stateId, caseNumber);

            var obitem = new ObservableCollection<object>();
            foreach (var item in cases)
                obitem.Add((object)item);
            pageControlAsignedCases.FilterTag = filters;
            if (cases.Count == 0)
            {
                pageControlAsignedCases.Page = 0;
                pageControlAsignedCases.TotalPages = 0;
            }

            else
            {
                pageControlAsignedCases.Page = 1;
                pageControlAsignedCases.TotalPages = (uint)Math.Ceiling((double)tlbl.GetPageSizeAfterFilters(true, categoryId, userId, stateId, caseNumber) / pageControl.PageSizes.FirstOrDefault());

            }

            pageControlAsignedCases.ItemsSource = obitem;
        }
        #endregion

        public void InitializeData()
        {
            var bLL = new TLBBLL();
            _viewModel = DataContext as TLBViewModel;
            _viewModel.UpdateCaseRelatedCollection();
            _viewModel.CaseCategoryList = bLL.GetCaseCategory();
            var assignedCases = TLBViewModel.CaseList.Where(x => x.User.UserId != 0 || x.Status == Helper.Status.End).Select(x => x.CaseNumber).ToList();
            _viewModel.AssignCasesTabViewModel.AssignedCasesList = new ObservableCollection<string>();
            assignedCases.ForEach(_viewModel.AssignCasesTabViewModel.AssignedCasesList.Add);
            _viewModel.AssignCasesTabViewModel.AssignedCasesList.Insert(0, TLBConstants.DefaultDDLValue);
            _viewModel.AssignCasesTabViewModel.FilterSearch += FilterSearch;
            _viewModel.AssignCasesTabViewModel.FilterSearchUnassigendCases += FilterSearchUnassignedCases;
            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            _viewModel.AssignCasesTabViewModel.IsInitialLoad = true;
            _viewModel.AssignCasesTabViewModel.SelectedStatus = _viewModel.StatusList.FirstOrDefault().Code;
            _viewModel.AssignCasesTabViewModel.SelectedCategory = _viewModel.CaseCategoryList.FirstOrDefault();
            _viewModel.AssignCasesTabViewModel.SelectedUnassignedCategory = _viewModel.CaseCategoryList.FirstOrDefault();
            _viewModel.AssignCasesTabViewModel.SelectedCaseNo = _viewModel.CaseNumbersList.FirstOrDefault();
            _viewModel.AssignCasesTabViewModel.SelectedUser = _viewModel.UserList.FirstOrDefault().UserId;
            _viewModel.AssignCasesTabViewModel.IsInitialLoad = false;

        }

        #region Logic for hide unhide tabs
        private void Border_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_height <= 0)
                _height = UnassignedContainer.ActualHeight;

            if (UnassignedContainer.Height != 0)
            {
                HideUnassignedTab();
            }

            if (UnassignedContainer.Height == 0)
            {
                ShowUnassignedTab();
            }
        }

        private void ShowUnassignedTab()
        {
            if (IsAssignedTabOpened)
            {
                DoubleAnimation animation1 = new DoubleAnimation();
                animation1.To = 0;
                animation1.From = 0;
                animation1.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation1);
            }
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = _height;
            animation.From = 0;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
            IsUnassignedTabOpened = true;
        }

        private void HideUnassignedTab()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 0;
            animation.From = _height;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
        }

        private void BorderAssigned_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_heightAssigned <= 0)
                _heightAssigned = AssignedContainer.ActualHeight;

            if (AssignedContainer.Height != 0)
            {
                HideAssignedTab();
            }

            if (AssignedContainer.Height == 0)
            {
                ShowAssignedTab();
            }
        }

        private void ShowAssignedTab()
        {
            if (IsUnassignedTabOpened)
            {
                DoubleAnimation animation1 = new DoubleAnimation();
                animation1.To = 0;
                animation1.From = 0;
                animation1.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation1);
            }
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = _heightAssigned;
            animation.From = 0;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
            IsAssignedTabOpened = true;
        }

        private void HideAssignedTab()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 0;
            animation.From = _heightAssigned;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
        }

        private double _heightAssigned;
        private bool IsAssignedTabOpened;
        private bool IsUnassignedTabOpened;
        #endregion

        public Grid assignCasesGrid { get; set; }
    }
}
