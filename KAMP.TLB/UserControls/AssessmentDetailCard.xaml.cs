﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.ViewModels;
using KAMP.TLB.Models;
using KAMP.Core.Repository.TLB;
using KAMP.TLB.BLL;
using System.Text.RegularExpressions;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for AssessmentDetailCard.xaml
    /// </summary>
    public partial class AssessmentDetailCard : UserControl
    {
        public Action CloseControl;
        private Grid assessmentFilterGrid;
        Assessment assessmentViewModel = new Assessment();
        public AssessmentDetailCard(Assessment AssessmentViewModel)
        {
            var lookUp = GetQueryType();
            AssessmentViewModel.LookUps = lookUp;
            AssessmentViewModel.IsErrorPresent = false;
            this.DataContext = assessmentViewModel = AssessmentViewModel;
            AssessmentViewModel.OnEditFilter += OnEditFilter;
            AssessmentViewModel.OnEditList += OnEditList;
            AssessmentViewModel.OnSaveClick += OnSaveClick;
            InitializeComponent();
        }

        private void OnEditList(int assessmentId)
        {
            var uc = new AssessmentList(assessmentId);
            uc.CloseControl = CloseFilterControl;
            assessmentFilterGrid = new Grid();
            assessmentFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentFilterGrid.Children.Add(lbl);
            assessmentFilterGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentFilterGrid);
            }
        }

        private void OnSaveClick()
        {
            var tlbBl = new TLBBLL();
            assessmentViewModel = this.DataContext as Assessment;
            var errorMessages = ValidateAssessmentDetailCard(assessmentViewModel);
            if (errorMessages.IsCollectionValid())
            {
                var sb = new StringBuilder();
                errorMessages.ForEach(x =>
                {
                    sb.AppendLine(x);
                });
                MessageBoxControl.Show(sb.ToString(), MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                if (!assessmentViewModel.IsErrorPresent)
                {
                    var assessmentModel = tlbBl.UpdateAssessment(assessmentViewModel);
                    if (assessmentModel.IsNotNull())
                    {
                        MessageBoxControl.Show("Saved Successfully.", MessageBoxButton.OK, MessageType.Information);
                    }
                }
                else
                {
                    MessageBoxControl.Show("Please rectify the errors and save again.", MessageBoxButton.OK, MessageType.Error);
                }
            }
        }

        private List<string> ValidateAssessmentDetailCard(Assessment assessmentViewModel)
        {
            var errorMessages = new List<string>();
            if (assessmentViewModel.AssessmentTypeId == 0)
                errorMessages.Add("Please select a Assessment Type.");
            if (!assessmentViewModel.AccountScore.HasValue)
                errorMessages.Add("Account points is mandatory.");
            if (!assessmentViewModel.TransactionScore.HasValue)
                errorMessages.Add("Transaction points is mandatory.");
            if (assessmentViewModel.AssessmentName.IsEmpty())
                errorMessages.Add("Assessment Description is mandatory.");
            if (assessmentViewModel.LookUpId == 0)
                errorMessages.Add("Please select a Query Type.");

            return errorMessages;
        }

        private List<LookUp> GetQueryType()
        {
            var tlbBl = new TLBBLL();
            var lookUp = tlbBl.GetQueryType();
            return lookUp;
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
                CloseControl();
        }

        private void OnEditFilter(int assessmentId)
        {
            var uc = new AssessmentFilter(assessmentId);
            uc.CloseControl = CloseFilterControl;
            assessmentFilterGrid = new Grid();
            assessmentFilterGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentFilterGrid.Children.Add(lbl);
            assessmentFilterGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentFilterGrid);
            }
        }

        private void CloseFilterControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assessmentFilterGrid);
            }
        }

        private void AccountPoints_TextChanged(object sender, TextChangedEventArgs e)
        {
            var res = sender as TextBox;
            if (!System.Text.RegularExpressions.Regex.IsMatch(res.Text, "^[0-9]*$"))
            {
                assessmentViewModel.IsErrorPresent = true;
                MessageBoxControl.Show("Please enter only numbers for Account Points.", MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                if (res.Text == string.Empty)
                    res.Text = null;
                assessmentViewModel.IsErrorPresent = false;
            }
        }

        private void TransactionPoints_TextChanged(object sender, TextChangedEventArgs e)
        {
            var res = sender as TextBox;
            if (!System.Text.RegularExpressions.Regex.IsMatch(res.Text, "^[0-9]*$"))
            {
                assessmentViewModel.IsErrorPresent = true;
                MessageBoxControl.Show("Please enter only numbers for Transaction Points.", MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                if (res.Text == string.Empty)
                    res.Text = null;
                assessmentViewModel.IsErrorPresent = false;
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]+"))
                e.Handled = true;

        }

        private void TextBox_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (!Regex.IsMatch(e.DataObject.GetData(typeof(string)).ToString(), "[^0-9]+"))
            {
                e.CancelCommand();
            }

        }

        private void TextBox_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]+"))
                e.Handled = true;
        }

        private void TextBox_Pasting_1(object sender, DataObjectPastingEventArgs e)
        {
            if (!Regex.IsMatch(e.DataObject.GetData(typeof(string)).ToString(), "[^0-9]+"))
            {
                e.CancelCommand();
            }
        }
    }
}
