﻿using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.TLB.Models;
using KAMP.Core.Repository.TLB;
using AutoMapper;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for RiskAssessments.xaml
    /// </summary>
    public partial class RiskAssessments : UserControl, ITLBPageControl
    {
        private RiskAssessmentViewModel riskAssessmentViewModel;
        private Grid assessmentDetailCardGrid;
        public RiskAssessments()
        {
            InitializeComponent();
        }

        private void AssignCategory()
        {
            var tlbBL = new TLBBLL();
            var assessmentType = tlbBL.GetAssessmentTypes();
            var defaultItem = new AssesmentType()
            {
                Id = 0,
                AssesmentTypeDescription = "All"
            };
            assessmentType.Add(defaultItem);

            if (assessmentType.IsCollectionValid())
            {
                riskAssessmentViewModel.AssessmentTypes = assessmentType;
            }
        }

        public void InitializeData()
        {
            riskAssessmentViewModel = new RiskAssessmentViewModel() { AssessmentTypes = new List<Core.Repository.TLB.AssesmentType>() };
            AssignCategory();
            riskAssessmentViewModel.IsCategorySelected += IsCategorySelected;
            GetAssessments();
            this.DataContext = riskAssessmentViewModel;
        }

        private void IsCategorySelected()
        {
            var assessmentTypeId = riskAssessmentViewModel.AssessmentTypeId;
            GetAssessments(assessmentTypeId);
        }

        private void GetAssessments(int assessmentTypeId = 0)
        {
            var tlbl = new TLBBLL();
            var assessmentCollection = new ObservableCollection<Assesments>();
            var asessments = new ObservableCollection<Assesments>();
            asessments = tlbl.GetRiskAssessment();
            if (assessmentTypeId == 0)
            {
                riskAssessmentViewModel.Assessments = asessments;
            }
            else
            {
                var assessmentByCategory = asessments.Where(x => x.AssesmentTypeId == assessmentTypeId).ToList();
                assessmentByCategory.ForEach(assessmentCollection.Add);
                riskAssessmentViewModel.Assessments = assessmentCollection;
            }
        }

        private void EditAssessment_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedItemfrmGrid = GdAssignments.SelectedItem as Assesments;
            if (selectedItemfrmGrid.IsNull()) MessageBoxControl.Show("Please Select A Row", MessageBoxButton.OK, MessageType.Error);
            var assessmentsViewModel = new Assessment();
            var mappedAssessments = Mapper.Map<Assesments, Assessment>(selectedItemfrmGrid);
            var tlbBL = new TLBBLL();
            var assessmentType = tlbBL.GetAssessmentTypes();
            mappedAssessments.AssessmentTypes = assessmentType;

            assessmentsViewModel = mappedAssessments;
            assessmentsViewModel.IsVisible = true;
            assessmentsViewModel.IsReadOnly = false;
            assessmentsViewModel.IsEditable = true;
            var uc = new AssessmentDetailCard(assessmentsViewModel);
            uc.CloseControl = CloseControl;
            assessmentDetailCardGrid = new Grid();
            assessmentDetailCardGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            assessmentDetailCardGrid.Children.Add(lbl);
            assessmentDetailCardGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(assessmentDetailCardGrid);
            }
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(assessmentDetailCardGrid);
            }
            var assessmentTypeId = riskAssessmentViewModel.AssessmentTypeId;
            GetAssessments(assessmentTypeId);
        }

        private void Row_Click(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            if (row != null)
                row.IsSelected = true;
        }

    }
}
