﻿using KAMP.TLB.BLL;
using KAMP.TLB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using KAMP.Core.FrameworkComponents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents.ViewModels;
using KAMP.Core.FrameworkComponents.Helpers;
using System.Text.RegularExpressions;

namespace KAMP.TLB.UserControls
{
    /// <summary>
    /// Interaction logic for Dispositions.xaml
    /// </summary>
    public partial class Dispositions : UserControl
    {
        public Dispositions()
        {
            InitializeComponent();
        }

        private void UpdateCase(object sender, RoutedEventArgs e)
        {
            DispositionsViewModel viewModel = DataContext as DispositionsViewModel;
            TLBBLL bll = new TLBBLL();
            bll.UpdateDispositions(viewModel);
            MessageBoxControl.Show("Dispositions Saved Successfully.", MessageBoxButton.OK, MessageType.Information);
        }

        private void OpenExportDispositions(object sender, RoutedEventArgs e)
        {
            var uc = new UCExportDispositions();
            uc.Close = CloseExportDispositions;
            uc.ExportDispositions = ExportToWord;
            dispositionsGrid = new Grid();
            dispositionsGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            dispositionsGrid.Children.Add(lbl);
            dispositionsGrid.Children.Add(uc);
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(dispositionsGrid);
            }
        }

        private void ExportToWord(DispositionExportViewModel viewModelExport)
        {
            List<ParagraphViewModel> list = new List<ParagraphViewModel>();
            DispositionsViewModel viewModel = DataContext as DispositionsViewModel;
            if (!viewModelExport.IsDueDillligenceChecked && !viewModelExport.IsESChecked && !viewModelExport.IsMOFChecked && !viewModelExport.IsRiskProfileChecked)
            {
                MessageBoxControl.Show("Please Select any of the Dispositions to export", MessageBoxButton.OK, MessageType.Error);
                return;
            }


            if (viewModelExport.IsDueDillligenceChecked)
            {
                list.Add(new ParagraphViewModel { Content = viewModel.Duediligence, Heading = "Due Diligence" });
            }
            if (viewModelExport.IsESChecked)
            {
                list.Add(new ParagraphViewModel { Content = viewModel.ExecSummary, Heading = "Executive Summary" });
            }
            if (viewModelExport.IsRiskProfileChecked)
            {
                list.Add(new ParagraphViewModel { Content = viewModel.RiskProfile, Heading = "Risk Profile" });
            }
            if (viewModelExport.IsMOFChecked)
            {
                list.Add(new ParagraphViewModel { Content = viewModel.MamorandumofFact, Heading = "Memorandum of Fact" });
            }
            try
            {
                DocumentCreator.CreateNewWordDocument<ParagraphViewModel>(list);
            }
            catch (Exception ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void CloseExportDispositions()
        {
            var win = Window.GetWindow(this);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(dispositionsGrid);
            }
        }

        public Grid dispositionsGrid { get; set; }

        private void SearchDispositionsClick(object sender, RoutedEventArgs e)
        {
            DispositionsViewModel viewModel = DataContext as DispositionsViewModel;
            SearchDispositionsViewModel searchDispositionsViewModel = new SearchDispositionsViewModel();
            searchDispositionsViewModel.SearchDispositionsModel.SearchParameters = viewModel.SearchParameters;
            var uc = new SearchDispositions();
            uc.DataContext = searchDispositionsViewModel;
            uc.CloseControl = CloseControl;
            searchDispositionsGrid = new Grid();
            searchDispositionsGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            searchDispositionsGrid.Children.Add(lbl);
            searchDispositionsGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(searchDispositionsGrid);
            }
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(searchDispositionsGrid);
            }
        }

        //private void HighlightSearchText(string searchText, TextRange txtRange, RichTextBox rtb)
        //{
        //    if (searchText.IsNotNull())
        //    {
        //        var range = txtRange.Text;
        //        txtRange.ClearAllProperties();
        //        Regex reg = new Regex("({0})".ToFormat(searchText), RegexOptions.Compiled | RegexOptions.IgnoreCase);

        //        var start = rtb.Document.ContentStart;
        //        while (start != null && start.CompareTo(rtb.Document.ContentEnd) < 0)
        //        {
        //            if (start.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
        //            {
        //                var match = reg.Match(start.GetTextInRun(LogicalDirection.Forward));

        //                var textrange = new TextRange(start.GetPositionAtOffset(match.Index, LogicalDirection.Forward), start.GetPositionAtOffset(match.Index + match.Length, LogicalDirection.Backward));
        //                textrange.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.DarkGreen));
        //                textrange.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(Colors.Yellow));
        //                textrange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);

        //                start = textrange.End;
        //            }
        //            start = start.GetNextContextPosition(LogicalDirection.Forward);
        //        }
        //    }
        //}

        //private void SearchRiskProfileClick(object sender, RoutedEventArgs e)
        //{
        //    //DispositionsViewModel viewModel = DataContext as DispositionsViewModel;
        //    //TextRange txtRange = new TextRange(TxtRiskProfile.Document.ContentStart, TxtRiskProfile.Document.ContentEnd);
        //    //HighlightSearchText(viewModel.SearchRiskProfileInput, txtRange, TxtRiskProfile);
        //}

        //private void SearchDueDiligenceClick(object sender, RoutedEventArgs e)
        //{
        //    //DispositionsViewModel viewModel = DataContext as DispositionsViewModel;
        //    //TextRange txtRange = new TextRange(TxtDueDiligence.Document.ContentStart, TxtDueDiligence.Document.ContentEnd);
        //    //HighlightSearchText(viewModel.SearchDueDiligenceInput, txtRange, TxtDueDiligence);
        //}

        //private void SearchMemorandumFactClick(object sender, RoutedEventArgs e)
        //{
        //    //DispositionsViewModel viewModel = DataContext as DispositionsViewModel;
        //    //TextRange txtRange = new TextRange(TxtMamorandumofFact.Document.ContentStart, TxtMamorandumofFact.Document.ContentEnd);
        //    //HighlightSearchText(viewModel.SearchMemorandumOfFactInput, txtRange, TxtMamorandumofFact);
        //}

        public Grid searchDispositionsGrid { get; set; }
    }
}
