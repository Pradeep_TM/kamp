﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.TLB.UserControls
{
    using KAMP.Core.FrameworkComponents;
    /// <summary> 
    /// Interaction logic for RelatedParties.xaml
    /// </summary>
    public partial class RelatedParties : UserControl
    {
        public Action CloseControl;

        public RelatedParties()
        {
            InitializeComponent();
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CloseControl.IsNotNull())
            {
                CloseControl();
            }
        }
    }
}
