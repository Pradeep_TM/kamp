﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.Models
{
    public class RelatedCasesExportModel
    {
        public string CaseNumber { get; set; }
        public string CaseName { get; set; }
        public int? TransactionCount { get; set; }
        public int? TotalScore { get; set; }
        public int? AverageScore { get; set; }
    } 
}
