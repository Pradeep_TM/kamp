﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Windows.Input;
using KAMP.TLB.ViewModels;

namespace KAMP.TLB.Models
{
    public class AvailableComponent : INotifyPropertyChanged
    {
        private int _fcrId;

        public int FcrId
        {
            get { return _fcrId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fcrId, (x) => x.FcrId); }
        }

        private int _assessmentId;

        public int AssessmentId
        {
            get { return _assessmentId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentId, (x) => x.AssessmentId); }
        }

        private List<FieldsViewModel> _fieldsViewModels;

        public List<FieldsViewModel> FieldsViewModels
        {
            get { return _fieldsViewModels; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fieldsViewModels, (x) => x.FieldsViewModels); }
        }


        private int _fieldId;
        public int FieldId
        {
            get { return _fieldId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fieldId, (x) => x.FieldId); }
        }

        private string _field;
        public string Field
        {
            get { return _field; }
            set { PropertyChanged.HandleValueChange(this, value, ref _field, (x) => x.Field); }
        }

        private string _operator;

        public string Operator
        {
            get { return _operator; }
            set { PropertyChanged.HandleValueChange(this, value, ref _operator, (x) => x.Operator); }
        }

        private string _criteria;

        public string Criteria
        {
            get { return _criteria; }
            set { PropertyChanged.HandleValueChange(this, value, ref _criteria, (x) => x.Criteria); }
        }

        private string _type;

        public string Type
        {
            get { return _type; }
            set { PropertyChanged.HandleValueChange(this, value, ref _type, (x) => x.Type); }
        }

        private int? _order;

        public int? Order
        {
            get { return _order; }
            set { PropertyChanged.HandleValueChange(this, value, ref _order, (x) => x.Order); }
        }

        public Action OnContinueClick { get; set; }
        private ICommand _continue;

        public ICommand Continue
        {
            get
            {

                if (_continue == null)
                    _continue = new DelegateCommand(() =>
                    {
                        if (OnContinueClick.IsNotNull())
                            OnContinueClick();
                    });
                return _continue;
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
