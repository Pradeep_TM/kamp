﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.TLB.ViewModels;
using System.Collections.ObjectModel;

namespace KAMP.TLB.Models
{
    public class Parameters : INotifyPropertyChanged
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }
        private int _groupId;

        public int GroupId
        {
            get { return _groupId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _groupId, (x) => x.GroupId); }
        }
        private string _groupName;

        public string GroupName
        {
            get { return _groupName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _groupName, (x) => x.GroupName); }
        }
        private string _fieldName;

        public string FieldName
        {
            get { return _fieldName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fieldName, (x) => x.FieldName); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
