﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using KAMP.Core.FrameworkComponents;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace KAMP.TLB.Models
{
    public class HistoryModel : INotifyPropertyChanged
    {
        private string _user; 
        public string User 
        {
            get { return _user; }
            set { PropertyChanged.HandleValueChange(this, value, ref _user, (x) => x.User); }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _firstName, (x) => x.FirstName); }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lastName, (x) => x.LastName); }
        }

        private string _timeStamp;
        public string TimeStamp
        {
            get { return _timeStamp; }
            set { PropertyChanged.HandleValueChange(this, value, ref _timeStamp, (x) => x.TimeStamp); }
        }

        private DateTime loginTime;
        public DateTime LoginTime
        {
            get { return loginTime; }
            set { PropertyChanged.HandleValueChange(this, value, ref loginTime, (x) => x.LoginTime); }
        }

        private string _entity;
        public string Entity
        {
            get { return _entity; }
            set { PropertyChanged.HandleValueChange(this, value, ref _entity, (x) => x.Entity); }
        }

        private string _caseNumber;
        public string CaseNumber
        {
            get { return _caseNumber; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseNumber, (x) => x.CaseNumber); }
        }

        private string _fieldName;
        public string FieldName
        {
            get { return _fieldName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fieldName, (x) => x.FieldName); }
        }

        private string _action;
        public string Action
        {
            get { return _action; }
            set { PropertyChanged.HandleValueChange(this, value, ref _action, (x) => x.Action); }
        }

        private string _previousValue;
        public string PreviousValue
        {
            get { return _previousValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _previousValue, (x) => x.PreviousValue); }
        }

        private string _modifiedValue;
        public string ModifiedValue
        {
            get { return _modifiedValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _modifiedValue, (x) => x.ModifiedValue); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
