﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.Models
{
    using KAMP.Core.FrameworkComponents;
    public class RelatedParties : INotifyPropertyChanged
    {
        private string _caseNumber;
        public string CaseNumber 
        {
            get { return _caseNumber; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseNumber, (x) => x.CaseNumber); }
        }

        private string _partyName;
        public string PartyName
        {
            get { return _partyName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _partyName, (x) => x.PartyName); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
