﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.TLB;
using KAMP.Core.Repository.WF;

namespace KAMP.TLB.Models
{
    public class CaseDetails : INotifyPropertyChanged     
    {
        #region Private Properties
        private int _cfId;
        private DateTime? _createDate;
        private bool? _isOpen;
        private DateTime? _openDate;
        private DateTime? _closeDate;
        private int? _averageScore;
        private int? _daysInQueue;
        private State _highestStatus;
        private string _caseNumber;    
        private string _caseName;
        private int? _caseScore;
        private LookUp _category;
        private string _anlyst;
        private int? _transactionCount;
        private State _state;
        private TLBUser _user;
        #endregion
        #region Public Properties

        public bool? IsOpen
        {
            get { return _isOpen; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isOpen, x => x.IsOpen);
            }
        }

        public DateTime? CloseDate
        {
            get { return _closeDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _closeDate, x => x.CloseDate);
            }
        }

        public DateTime? OpenDate
        {
            get { return _openDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _openDate, x => x.OpenDate);
            }
        }

        public DateTime? CreateDate
        {
            get { return _createDate; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createDate, x => x.CreateDate);
            }
        }

        public int? DaysInQueue
        {
            get { return _daysInQueue; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _daysInQueue, x => x.DaysInQueue);
            }
        }

        public State HighestStatus
        {
            get { return _highestStatus; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _highestStatus, x => x.HighestStatus);
            }
        }

        public int? AverageScore
        {
            get { return _averageScore; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _averageScore, x => x.AverageScore);
            }
        }

        public int? CaseScore
        {
            get { return _caseScore; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseScore, x => x.CaseScore);
            }
        }

        public int CfId
        {
            get { return _cfId; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _cfId, x => x.CfId);
            }
        }

        public string CaseName
        {
            get { return _caseName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseName, x => x.CaseName);
            }
        }

        public string Analyst
        {
            get { return _anlyst; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _anlyst, x => x.Analyst);
            }
        }

        public LookUp Category
        {
            get { return _category; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _category, x => x.Category);
            }
        }

        public int? TransactionCount
        {
            get { return _transactionCount; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _transactionCount, x => x.TransactionCount);
            }
        }

        public State State
        {
            get { return _state; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _state, x => x.State);
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _status, x => x.Status);
            }
        }

        public TLBUser User
        {
            get { return _user; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _user, x => x.User);
            }
        }

        public string CaseNumber
        {
            get { return _caseNumber; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseNumber, x => x.CaseNumber);
            }
        } 
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;



        public string _status;
    }
}
