﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.ComponentModel.DataAnnotations;

namespace KAMP.TLB.Models
{
    public class ClientAccount
    {
        [Display(Name = "Account")]
        public string Account { get; set; }
        [Display(Name = "Name")]
        public string AccountName { get; set; }
        [Display(Name = "Status")]
        public string AccountStatus { get; set; }
        [Display(Name = "Closed Date")]
        public string ClosedDate { get; set; }
        [Display(Name = "Transactions")]
        public long Transactions { get; set; }
        [Display(Name = "Incoming Account")]
        public decimal IncomingUsd { get; set; }
        [Display(Name = "Outgoing Account")]
        public decimal OutgoingUsd { get; set; }
        [Display(Name = "Bank Transfers Account")]
        public decimal BankTransfersUsd { get; set; }
    }
}
