﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using KAMP.Core.FrameworkComponents;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.Models
{
    public class TransactionHitsModel : INotifyPropertyChanged
    {
        private int _Id;  
        public int Id
        {
            get { return _Id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _Id, (x) => x.Id); }
        }

        private string _assessmentDesc;
        public string AssessmentDesc
        {
            get { return _assessmentDesc; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentDesc, (x) => x.AssessmentDesc); }
        }  

        private int _assessmentScore;
        public int AssessmentScore
        {
            get { return _assessmentScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentScore, (x) => x.AssessmentScore); }
        }

        private string _parameterActualValue;
        public string ParameterActualValue
        {
            get { return _parameterActualValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterActualValue, (x) => x.ParameterActualValue); }
        } 

        private string _parameterAliasValue;
        public string ParameterAliasValue
        {
            get { return _parameterAliasValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterAliasValue, (x) => x.ParameterAliasValue); }
        }

        private string _parameterSearchTerm;
        public string ParameterSearchTerm
        {
            get { return _parameterSearchTerm; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterSearchTerm, (x) => x.ParameterSearchTerm); }
        }

        private string _scoredField;
        public string ScoredField
        {
            get { return _scoredField; }
            set { PropertyChanged.HandleValueChange(this, value, ref _scoredField, (x) => x.ScoredField); }
        }

        private string _scoredFieldValue;
        public string ScoredFieldValue
        {
            get { return _scoredFieldValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _scoredFieldValue, (x) => x.ScoredFieldValue); }
        }

        private int _transactionScore;
        public int TransactionScore
        {
            get { return _transactionScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionScore, (x) => x.TransactionScore); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
