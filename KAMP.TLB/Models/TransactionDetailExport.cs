﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.Models
{
    public class TransactionDetailExport
    {
        [Display(Name = "Transaction Score")]
        public int TotalScore { get; set; }
        [Display(Name = "Internal Reference Number")]
        public string InternalReferenceNumber { get; set; }
        [Display(Name = "In/Out")]
        public string Value { get; set; }
        [Display(Name = "Message Type")]
        public string MessageType { get; set; }
        [Display(Name = "Amount")]  
        public decimal PaymentAmount { get; set; }
        [Display(Name = "Value Date")]
        public string ValueDate { get; set; }   
        [Display(Name = "By Order")]
        public string ByOrderPartyName { get; set; }
        [Display(Name = "By Order Country")]
        public string ByOrderPartyCountryName { get; set; }
        [Display(Name = "Beneficiary")]
        public string BenePartyName { get; set; }
        [Display(Name = "Beneficiary Country")]
        public string BenePartyCountryName { get; set; }
        [Display(Name = "Debit Bank Name")]
        public string DebitBankName { get; set; }
        [Display(Name = "Debit Bank Country")]
        public string DebitBankCountryName { get; set; }
        [Display(Name = "Beneficiary Bank Name")]
        public string BeneficiaryBankName { get; set; }
        [Display(Name = "Beneficiary Party Country")]
        public string BeneficiaryBankCountryName { get; set; }
    }
}
