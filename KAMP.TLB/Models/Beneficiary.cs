﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.Models
{
    public class Beneficiary : INotifyPropertyChanged
    {  
        private string _beneficiaries;

        public string Beneficiaries
        {
            get { return _beneficiaries; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaries, (x) => x.Beneficiaries); }
        }

        private int _transactions;
        public int Transactions
        {
            get { return _transactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactions, (x) => x.Transactions); }
        }

        private decimal _totalAmount;

        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalAmount, (x) => x.TotalAmount); }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { PropertyChanged.HandleValueChange(this, value, ref _score, (x) => x.Score); }
        }

        private double _avgScore;
        public double AvgScore
        {
            get { return _avgScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _avgScore, (x) => x.AvgScore); }
        }

        private DateTime _lastTransaction;

        public DateTime LastTransaction
        {
            get { return _lastTransaction; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lastTransaction, (x) => x.LastTransaction); }
        }

        private DateTime _firstTransaction;

        public DateTime FirstTransaction
        {
            get { return _firstTransaction; }
            set { PropertyChanged.HandleValueChange(this, value, ref _firstTransaction, (x) => x.FirstTransaction); }
        }

        private int _totalTransactions;

        public int TotalTransactions
        {
            get { return _totalTransactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalTransactions, (x) => x.TotalTransactions); }
        }


        private int _cfId;

        public int CfId
        {
            get { return _cfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfId, (x) => x.CfId); }
        }

        private List<int> _transactionIds;

        public List<int> TransactionIds
        {
            get { return _transactionIds; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionIds, (x) => x.TransactionIds); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
