﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.Models
{
    public class TLBUser
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public List<int> StateIdList { get; set; }
    } 
}
