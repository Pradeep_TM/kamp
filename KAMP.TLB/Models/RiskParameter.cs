﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Windows.Input;

namespace KAMP.TLB.Models
{
    public class RiskParameter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        private string _parameterValue;

        public string ParameterValue
        {
            get { return _parameterValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _parameterValue, (x) => x.ParameterValue); }
        }

    }
}
