﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.TLB;
using System.Windows.Input;

namespace KAMP.TLB.Models
{
    public class Assessment : INotifyPropertyChanged
    {
        private int _assessmentId;
        public int AssessmentId
        {
            get
            {
                return _assessmentId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assessmentId, (x) => x.AssessmentId);
            }
        }

        public bool IsErrorPresent { get; set; }

        private List<int> _transactionIds;

        public List<int> TransactionIds
        {
            get { return _transactionIds; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionIds, (x) => x.TransactionIds); }
        }

        private List<LookUp> _lookUps;

        public List<LookUp> LookUps
        {
            get { return _lookUps; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lookUps, (x) => x.LookUps); }
        }

        private int _lookUpId;

        public int LookUpId
        {
            get { return _lookUpId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _lookUpId, (x) => x.LookUpId); }
        }


        private int _transactions;
        public int Transactions
        {
            get { return _transactions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactions, (x) => x.Transactions); }
        }

        private string _assessmentName;
        public string AssessmentName
        {
            get { return _assessmentName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentName, (x) => x.AssessmentName); }
        }

        private int _ruleHits;

        public int RuleHits
        {
            get { return _ruleHits; }
            set { PropertyChanged.HandleValueChange(this, value, ref _ruleHits, (x) => x.RuleHits); }
        }

        private int _perHit;

        public int PerHit
        {
            get { return _perHit; }
            set { PropertyChanged.HandleValueChange(this, value, ref _perHit, (x) => x.PerHit); }
        }

        private int _total;

        public int Total
        {
            get { return _total; }
            set { PropertyChanged.HandleValueChange(this, value, ref _total, (x) => x.Total); }
        }

        private int _assessmentTypeId;

        public int AssessmentTypeId
        {
            get { return _assessmentTypeId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentTypeId, (x) => x.AssessmentTypeId); }
        }

        private string _assessmentTypeName;

        public string AssessmentTypeName
        {
            get { return _assessmentTypeName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentTypeName, (x) => x.AssessmentTypeName); }
        }

        private int _cfId;

        public int CfId
        {
            get { return _cfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _cfId, (x) => x.CfId); }
        }

        private int _expr1;

        public int Expr1
        {
            get { return _expr1; }
            set { PropertyChanged.HandleValueChange(this, value, ref _expr1, (x) => x.Expr1); }
        }

        private int _note;

        public int Note
        {
            get { return _note; }
            set { PropertyChanged.HandleValueChange(this, value, ref _note, (x) => x.Note); }
        }

        private int _occurrences;

        public int Occurrences
        {
            get { return _occurrences; }
            set { PropertyChanged.HandleValueChange(this, value, ref _occurrences, (x) => x.Occurrences); }
        }

        private int _score;

        public int Score
        {
            get { return _score; }
            set { PropertyChanged.HandleValueChange(this, value, ref _score, (x) => x.Score); }
        }

        private int _totalScore;

        public int TotalScore
        {
            get { return _totalScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalScore, (x) => x.TotalScore); }
        }

        private int? _accountScore;

        public int? AccountScore
        {
            get { return _accountScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _accountScore, (x) => x.AccountScore); }
        }

        private int? _transactionScore;

        public int? TransactionScore
        {
            get { return _transactionScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionScore, (x) => x.TransactionScore); }
        }

        private string _assessmentDetail;

        public string AssessmentDetail
        {
            get { return _assessmentDetail; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentDetail, (x) => x.AssessmentDetail); }
        }

        private bool _isVisible;
        public bool IsVisible
        {
            get { return _isVisible; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isVisible, (x) => x.IsVisible); }
        }

        private bool _isReadOnly;
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isReadOnly, (x) => x.IsReadOnly); }
        }

        private bool _isEditable;
        public bool IsEditable
        {
            get { return _isEditable; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isEditable, (x) => x.IsEditable); }
        }

        private List<AssesmentType> _assessmentTypes;

        public List<AssesmentType> AssessmentTypes
        {
            get { return _assessmentTypes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assessmentTypes, (x) => x.AssessmentTypes); }
        }

        public Action<int> OnEditFilter;
        private ICommand _editFilter;
        public ICommand EditFilter
        {
            get
            {

                if (_editFilter == null)
                    _editFilter = new DelegateCommand<int>((s) =>
                    {
                        if (OnEditFilter.IsNotNull())
                            OnEditFilter(s);
                    });
                return _editFilter;
            }
        }

        public Action<int> OnEditList;
        private ICommand _editList;
        public ICommand EditList
        {
            get
            {

                if (_editList == null)
                    _editList = new DelegateCommand<int>((s) =>
                    {
                        if (OnEditList.IsNotNull())
                            OnEditList(s);
                    });
                return _editList;
            }
        }

        public Action OnSaveClick;
        private ICommand _save;
        public ICommand Save
        {
            get
            {

                if (_save == null)
                    _save = new DelegateCommand(() =>
                        {
                            if (OnSaveClick.IsNotNull())
                                OnSaveClick();
                        });
                return _save;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
