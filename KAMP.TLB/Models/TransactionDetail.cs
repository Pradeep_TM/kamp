﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.TLB.Models
{
    public class TransactionDetail : INotifyPropertyChanged
    {
        private int _Id;
        public int Id
        {
            get { return _Id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _Id, (x) => x.Id); }
        }

        private int? _totalScore;
        public int? TotalScore
        {
            get { return _totalScore; }
            set { PropertyChanged.HandleValueChange(this, value, ref _totalScore, (x) => x.TotalScore); }
        }

        private string _internalReferenceNo;
        public string InternalReferenceNumber
        {
            get { return _internalReferenceNo; }
            set { PropertyChanged.HandleValueChange(this, value, ref _internalReferenceNo, (x) => x.InternalReferenceNumber); }
        }

        private string _messageType;
        public string MessageType
        {
            get { return _messageType; }
            set { PropertyChanged.HandleValueChange(this, value, ref _messageType, (x) => x.MessageType); }
        }

        private string _paymentType;
        public string PaymentType
        {
            get { return _paymentType; }
            set { PropertyChanged.HandleValueChange(this, value, ref _paymentType, (x) => x.PaymentType); }
        }

        private string _clientAccount1;
        public string ClientAccount1
        {
            get { return _clientAccount1; }
            set { PropertyChanged.HandleValueChange(this, value, ref _clientAccount1, (x) => x.ClientAccount1); }
        }

        private string _clientAccount2;
        public string ClientAccount2
        {
            get { return _clientAccount2; }
            set { PropertyChanged.HandleValueChange(this, value, ref _clientAccount2, (x) => x.ClientAccount1); }
        }


        private decimal? _amount;
        public decimal? PaymentAmount
        {
            get { return _amount; }
            set { PropertyChanged.HandleValueChange(this, value, ref _amount, (x) => x.PaymentAmount); }
        }

        private DateTime? _valueDate;
        public DateTime? ValueDate
        {
            get { return _valueDate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _valueDate, (x) => x.ValueDate); }
        }

        private DateTime? _transactionDate;
        public DateTime? TransactionDate
        {
            get { return _transactionDate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _transactionDate, (x) => x.TransactionDate); }
        }


        #region Ordering Party
        private string _byOrderBIC;
        public string ByOrderPartyNameBIC
        {
            get { return _byOrderBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _byOrderBIC, (x) => x.ByOrderPartyNameBIC); }
        }

        private string _byOrderAddress;
        public string ByOrderPartyNameAddress
        {
            get { return _byOrderAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _byOrderAddress, (x) => x.ByOrderPartyNameAddress); }
        }

        private string _byOrderStanName;
        public string ByOrderStanName
        {
            get { return _byOrderStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _byOrderStanName, (x) => x.ByOrderStanName); }
        }

        private string _byOrder;
        public string ByOrderPartyName
        {
            get { return _byOrder; }
            set { PropertyChanged.HandleValueChange(this, value, ref _byOrder, (x) => x.ByOrderPartyName); }
        }

        private string _byOrderCountry;
        public string ByOrderPartyCountryName
        {
            get { return _byOrderCountry; }
            set { PropertyChanged.HandleValueChange(this, value, ref _byOrderCountry, (x) => x.ByOrderPartyCountryName); }
        } 
        #endregion

        #region Beneficiary Party
        private string _beneficiaryBIC;
        public string BeneficiaryBIC
        {
            get { return _beneficiaryBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBIC, (x) => x.BeneficiaryBIC); }
        }

        private string _beneficiaryAddress;
        public string BeneficiaryAddress
        {
            get { return _beneficiaryAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryAddress, (x) => x.BeneficiaryAddress); }
        }

        private string _beneficiaryStanName;
        public string BeneficiaryStanName
        {
            get { return _beneficiaryStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryStanName, (x) => x.BeneficiaryStanName); }
        }

        private string _beneficiary;
        public string BenePartyName
        {
            get { return _beneficiary; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiary, (x) => x.BenePartyName); }
        }

        private string _beneficiaryCountry;
        public string BenePartyCountryName
        {
            get { return _beneficiaryCountry; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryCountry, (x) => x.BenePartyCountryName); }
        }
        
        #endregion

        #region Debit Bank

        private string _debitBankBIC;
        public string DebitBankBIC
        {
            get { return _debitBankBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _debitBankBIC, (x) => x.DebitBankBIC); }
        }

        private string _debitBankAddress;
        public string DebitBankAddress
        {
            get { return _debitBankAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _debitBankAddress, (x) => x.DebitBankAddress); }
        }

        private string _debitBankStanName;
        public string DebitBankStanName
        {
            get { return _debitBankStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _debitBankStanName, (x) => x.DebitBankStanName); }
        }

        private string _debitBankName;
        public string DebitBankName
        {
            get { return _debitBankName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _debitBankName, (x) => x.DebitBankName); }
        }

        private string _debitBankCountry;
        public string DebitBankCountryName
        {
            get { return _debitBankCountry; }
            set { PropertyChanged.HandleValueChange(this, value, ref _debitBankCountry, (x) => x.DebitBankCountryName); }
        } 

        #endregion

        #region Credit Bank

        private string _creditBankBIC;
        public string CreditBankBIC
        {
            get { return _creditBankBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _creditBankBIC, (x) => x.CreditBankBIC); }
        }

        private string _creditBankAddress;
        public string CreditBankAddress
        {
            get { return _creditBankAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _creditBankAddress, (x) => x.CreditBankAddress); }
        }

        private string _creditBankStanName;
        public string CreditBankStanName
        {
            get { return _creditBankStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _creditBankStanName, (x) => x.CreditBankStanName); }
        }

        private string _creditBankName;
        public string CreditBankName
        {
            get { return _creditBankName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _creditBankName, (x) => x.CreditBankName); }
        }

        private string _creditBankCountryName;
        public string CreditBankCountryName
        {
            get { return _creditBankCountryName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _creditBankCountryName, (x) => x.CreditBankCountryName); }
        } 
        #endregion

        #region Beneficiary Bank
        private string _beneficiaryBankBIC;
        public string BeneficiaryBankBIC
        {
            get { return _beneficiaryBankBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankBIC, (x) => x.BeneficiaryBankBIC); }
        }

        private string _beneficiaryBankAddress;
        public string BeneficiaryBankAddress
        {
            get { return _beneficiaryBankAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankAddress, (x) => x.BeneficiaryBankAddress); }
        }

        private string _beneficiaryBankStanName;
        public string BeneficiaryBankStanName
        {
            get { return _beneficiaryBankStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankStanName, (x) => x.BeneficiaryBankStanName); }
        }

        private string _beneficiaryBankName;
        public string BeneficiaryBankName
        {
            get { return _beneficiaryBankName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankName, (x) => x.BeneficiaryBankName); }
        }

        private string _beneficiaryBankCountry;
        public string BeneficiaryBankCountryName
        {
            get { return _beneficiaryBankCountry; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankCountry, (x) => x.BeneficiaryBankCountryName); }
        } 
        #endregion

        #region Originating Bank

        private string _originatingBankBIC;
        public string OriginatingBankBIC
        {
            get { return _originatingBankBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _originatingBankBIC, (x) => x.OriginatingBankBIC); }
        }

        private string _originatingBankAddress;
        public string OriginatingBankAddress
        {
            get { return _originatingBankAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _originatingBankAddress, (x) => x.OriginatingBankAddress); }
        }

        private string _originatingBankStanName;
        public string OriginatingBankStanName
        {
            get { return _originatingBankStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _originatingBankStanName, (x) => x.OriginatingBankStanName); }
        }

        private string _originatingBankName;
        public string OriginatingBankName
        {
            get { return _originatingBankName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _originatingBankName, (x) => x.OriginatingBankName); }
        }

        private string _originatingBankCountryName;
        public string OriginatingBankCountryName
        {
            get { return _originatingBankCountryName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _originatingBankCountryName, (x) => x.OriginatingBankCountryName); }
        } 

        #endregion

        #region Instructing Bank

        private string _instructingBankBIC;
        public string InstructingBankBIC
        {
            get { return _instructingBankBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _instructingBankBIC, (x) => x.InstructingBankBIC); }
        }

        private string _instructingBankAddress;
        public string InstructingBankAddress
        {
            get { return _instructingBankAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _instructingBankAddress, (x) => x.InstructingBankAddress); }
        }

        private string _instructingBankStanName;
        public string InstructingBankStanName
        {
            get { return _instructingBankStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _instructingBankStanName, (x) => x.InstructingBankStanName); }
        }

        private string _instructingBankName;
        public string InstructingBankName
        {
            get { return _instructingBankName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _instructingBankName, (x) => x.InstructingBankName); }
        }

        private string _instructingBankCountryName;
        public string InstructingBankCountryName
        {
            get { return _instructingBankCountryName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _instructingBankCountryName, (x) => x.InstructingBankCountryName); }
        } 

        #endregion

        #region Intermediary Bank

        private string _intermediaryBankBIC;
        public string IntermediaryBankBIC
        {
            get { return _intermediaryBankBIC; }
            set { PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankBIC, (x) => x.IntermediaryBankBIC); }
        }

        private string _intermediaryBankAddress;
        public string IntermediaryBankAddress
        {
            get { return _intermediaryBankAddress; }
            set { PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankAddress, (x) => x.IntermediaryBankAddress); }
        }

        private string _intermediaryBankStanName;
        public string IntermediaryBankStanName
        {
            get { return _intermediaryBankStanName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankStanName, (x) => x.IntermediaryBankStanName); }
        }

        private string _intermediaryBankName;
        public string IntermediaryBankName
        {
            get { return _intermediaryBankName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankName, (x) => x.IntermediaryBankName); }
        }

        private string _intermediaryBankCountryName;
        public string IntermediaryBankCountryName
        {
            get { return _intermediaryBankCountryName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankCountryName, (x) => x.IntermediaryBankCountryName); }
        } 

        #endregion


        public event PropertyChangedEventHandler PropertyChanged;

        private string _value;
        public string Value
        {
            get { return _value; }
            set { PropertyChanged.HandleValueChange(this, value, ref _value, (x) => x.Value); }
        }

        private string _banktoBeneInfo;
        public string BanktoBeneInfo
        {
            get { return _banktoBeneInfo; }
            set { PropertyChanged.HandleValueChange(this, value, ref _banktoBeneInfo, (x) => x.BanktoBeneInfo); }
        }

        private string _originatortoBeneInfo;
        public string OriginatortoBeneInfo
        {
            get { return _originatortoBeneInfo; }
            set { PropertyChanged.HandleValueChange(this, value, ref _originatortoBeneInfo, (x) => x.OriginatortoBeneInfo); }
        }

        private string _debitBankInstructions;
        public string DebitBankInstructions
        {
            get { return _debitBankInstructions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _debitBankInstructions, (x) => x.DebitBankInstructions); }
        }

        private string _creditBankAdvice;
        public string CreditBankAdvice
        {
            get { return _debitBankInstructions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _creditBankAdvice, (x) => x.CreditBankAdvice); }
        }

        private string _beneficiaryBankAdvice;
        public string BeneficiaryBankAdvice
        {
            get { return _beneficiaryBankAdvice; }
            set { PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankAdvice, (x) => x.BeneficiaryBankAdvice); }
        }


        private string _creditBankInstructions;
        public string CreditBankInstructions
        {
            get { return _creditBankInstructions; }
            set { PropertyChanged.HandleValueChange(this, value, ref _creditBankInstructions, (x) => x.CreditBankInstructions); }
        }

        private bool? _sAR;
        public bool? SAR
        {
            get { return _sAR; }
            set { PropertyChanged.HandleValueChange(this, value, ref _sAR, (x) => x.SAR); }
        }
    }
}
