﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.Models
{
    using KAMP.Core.FrameworkComponents;
    using System.Collections.ObjectModel;
    public class SearchDispositionsModel : INotifyPropertyChanged
    {
        private bool _isExactSearch = true;
        public bool IsExactSearch
        {
            get { return _isExactSearch; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isExactSearch, (x) => x.IsExactSearch); }
        }

        private bool _isSoundAlikeSearch;
        public bool IsSoundAlikeSearch
        {
            get { return _isSoundAlikeSearch; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isSoundAlikeSearch, (x) => x.IsSoundAlikeSearch); }
        }


        private bool _isRiskProfileChecked = true;
        public bool IsRiskProfileChecked
        {
            get { return _isRiskProfileChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isRiskProfileChecked, (x) => x.IsRiskProfileChecked); }
        }

        private bool _isMOFChecked = true;
        public bool IsMOFChecked
        {
            get { return _isMOFChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isMOFChecked, (x) => x.IsMOFChecked); }
        }

        private bool _isDueDillligenceChecked = true;
        public bool IsDueDillligenceChecked
        {
            get { return _isDueDillligenceChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isDueDillligenceChecked, (x) => x.IsDueDillligenceChecked); }
        }

        private string _searchParameters;
        public string SearchParameters
        {
            get { return _searchParameters; }
            set { PropertyChanged.HandleValueChange(this, value, ref _searchParameters, (x) => x.SearchParameters); }
        }


        private bool _isESChecked = true;
        public bool IsESChecked
        {
            get { return _isESChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isESChecked, (x) => x.IsESChecked); }
        }

        private CaseSearchData _dGSelectedItem;
        public CaseSearchData DGSelectedItem
        {
            get { return _dGSelectedItem; }
            set { PropertyChanged.HandleValueChange(this, value, ref _dGSelectedItem, (x) => x.DGSelectedItem); }
        }

        private ObservableCollection<CaseSearchData> _caseDetailsList;
        public ObservableCollection<CaseSearchData> CaseDetailsList
        {
            get { return _caseDetailsList; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseDetailsList, (x) => x.CaseDetailsList); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class CaseSearchData : INotifyPropertyChanged
    {
        private string _caseNumber;
        public string CaseNumber
        {
            get { return _caseNumber; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseNumber, (x) => x.CaseNumber); }
        }

        private List<string> _matchedWords;
        public List<string> MatchedWords
        {
            get { return _matchedWords; }
            set { PropertyChanged.HandleValueChange(this, value, ref _matchedWords, (x) => x.MatchedWords); }
        }


        private string _caseName;
        public string CaseName
        {
            get { return _caseName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseName, (x) => x.CaseName); }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { PropertyChanged.HandleValueChange(this, value, ref _status, (x) => x.Status); }
        }


        private string _riskProfile;
        public string RiskProfile
        {
            get { return _riskProfile; }
            set { PropertyChanged.HandleValueChange(this, value, ref _riskProfile, (x) => x.RiskProfile); }
        }

        private string _memofFact;
        public string MemofFact
        {
            get { return _memofFact; }
            set { PropertyChanged.HandleValueChange(this, value, ref _memofFact, (x) => x.MemofFact); }
        }

        private string _dueDiligence;
        public string DueDiligence
        {
            get { return _dueDiligence; }
            set { PropertyChanged.HandleValueChange(this, value, ref _dueDiligence, (x) => x.DueDiligence); }
        }

        private string _executiveSummary;
        public string ExecutiveSummary
        {
            get { return _executiveSummary; }
            set { PropertyChanged.HandleValueChange(this, value, ref _executiveSummary, (x) => x.ExecutiveSummary); }
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
