﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.TLB.Models
{
    public class CaseHistoryExportModel
    {
        public string User { get; set; }
        public string TimeStamp { get; set; }
        public string FieldName { get; set; }
        public string Action { get; set; } 
        public string PreviousValue { get; set; }
        public string ModifiedValue { get; set; }
    }
}
