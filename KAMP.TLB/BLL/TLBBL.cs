﻿using KAMP.Core.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.TLB;
using System.Collections.ObjectModel;
using KAMP.TLB.Models;
using AutoMapper;
using KAMP.Core.Repository.WF;
using KAMP.Core.Repository;
using KAMP.TLB.AppCode;
using KAMP.Core.Repository.UM;
using KAMP.TLB.ViewModels;
using KAMP.Core.Repository.MV;
using System.Data.Entity.Core.Metadata.Edm;
using System.Windows;
using KAMP.Core.Workflow;
using KAMP.Core.Workflow.Helpers;
using KAMP.TLB.UserControls;
using KAMP.Core.Repository.AuditLog;
using KAMP.Core.Repository.Models.TLB;
using System.Data.Entity.Core.Objects;
using KAMP.Core.Repository.DBHelpers;
using KAMP.Core.FrameworkComponents.Extenstions;
using KAMP.TLB.Models;
using System.Xml;
using KAMP.Core.Repository.Models.UserManagement;
using KAMP.Core.FrameworkComponents.Helpers;

namespace KAMP.TLB.BLL
{

    public class TLBBLL
    {
        KAMPContext _context;
        public TLBBLL()
        {
            _context = new KAMPContext();
        }
        internal ObservableCollection<LookUp> GetCaseCategory()
        {
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var lookUp = tLBUnitOfWork.GetRepository<LookUp>();
                var result = lookUp.Items.Include(x => x.TLBLookUpGroups).Where(x => x.TLBLookUpGroups.Name.Equals("Case Category")).OrderBy(x => x.Value).ToList();
                result.Insert(0, new Core.Repository.TLB.LookUp { Id = 0, Value = TLBConstants.DefaultDDLValue });

                var observableCollection = new ObservableCollection<LookUp>();
                result.ForEach(observableCollection.Add);
                return observableCollection;
            }
        }

        internal ObservableCollection<CaseDetails> GetAllCases()
        {
            var data = new ObservableCollection<CaseDetails>();
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var casesRepo = tLBUnitOfWork.GetRepository<TLBCaseFile>();
                var query = casesRepo.Items.Include(x => x.LookUp).Where(x => x.LookUp.Id == x.CategoryId);

                var resultList = query.ToList();
                List<string> caseNoList = resultList.Select(x => x.CaseNo).ToList();

                var caseRelatedData = WorkflowAPI.GetCaseDetails(caseNoList);

                foreach (var result in resultList)
                {
                    var data1 = Mapper.Map<CaseDetails>(result);
                    var currentCaseData = caseRelatedData.FirstOrDefault(x => x.CaseNo == data1.CaseNumber);
                    data1.State = new State { Id = currentCaseData.StateId, StateName = currentCaseData.State };
                    data1.Status = currentCaseData.Status;
                    data1.OpenDate = currentCaseData.CaseOpenDate;
                    data1.IsOpen = currentCaseData.IsOpen;
                    data1.CloseDate = currentCaseData.CaseClosedDate;
                    data1.DaysInQueue = currentCaseData.DaysInQueue;
                    data1.User = new Models.TLBUser { UserId = currentCaseData.UserId, UserName = currentCaseData.UserName };
                    data1.HighestStatus = new State { StateName = currentCaseData.HighestStatus };
                    data1.Analyst = currentCaseData.LastAnalyst;
                    data.Add(data1);
                }

                //foreach (var result in resultList)
                //{
                //    var data1 = Mapper.Map<CaseDetails>(result);
                //    if (data1.HighestStatus.Id == 0)
                //        data1.HighestStatus = new State { Id = 0, StateName = "UnAssigned" };
                //    else
                //        data1.HighestStatus = stateList.Where(x => x.Id == data1.HighestStatus.Id).FirstOrDefault();

                //    if (data1.Status.Id == 0)
                //        data1.Status = new State { Id = 0, StateName = "UnAssigned" };
                //    else if (data1.Status.Id == -1)
                //        data1.Status = new State { Id = -1, StateName = "Assigned" };
                //    else
                //        data1.Status = stateList.Where(x => x.Id == data1.Status.Id).FirstOrDefault();

                //    data1.User = userList.Where(x => x.UserId == data1.User.UserId).FirstOrDefault();
                //    if (data1.User.IsNull())
                //        data1.User = new TLBUser { UserId = 0, UserName = "Unknown" };
                //    else if (data1.User.UserId == 0)
                //        data1.User = new TLBUser { UserId = 0 };
                //    data.Add(data1);
                //}


            }
            return data;
        }

        //private int? CalculateDaysInQueqe(DateTime? dateTimeofOpen)
        //{
        //    if (!dateTimeofOpen.HasValue) return null;
        //    var days = Convert.ToInt32((System.DateTime.Now - dateTimeofOpen.Value).Days);
        //    return days;
        //}

        private List<State> GetStateList()
        {
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var stateRepo = tLBUnitOfWork.GetRepository<State>();
                return stateRepo.Items.ToList();
            }
        }

        internal ObservableCollection<MasterEntity> GetAllStatus()
        {
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var context = Application.Current.Properties["Context"] as Context;
                var statusList = new ObservableCollection<MasterEntity>();
                var stateRepo = tLBUnitOfWork.GetRepository<State>();
                var stateList = stateRepo.Items.Where(x => x.ModuleTypeId == context.Module.Id).ToList();
                stateList.Add(new State { Id = -1, StateName = Helper.Status.Assigned });
                stateList.Add(new State { Id = -2, StateName = Helper.Status.End });
                var lst = stateList.OrderBy(x => x.StateName).ToList();
                lst.ForEach((x) =>
                {
                    MasterEntity status = new MasterEntity { Code = x.Id, Name = x.StateName };
                    statusList.Add(status);
                });
                statusList.Insert(0, new MasterEntity { Code = 0, Name = TLBConstants.DefaultDDLValue });
                return statusList;
            }
        }

        internal ObservableCollection<TLBUser> GetUsers()
        {
            var userList = new ObservableCollection<TLBUser>();
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var userRepo = tLBUnitOfWork.GetRepository<User>();
                var context = Application.Current.Properties["Context"] as Context;
                if (context.IsNull()) return null;
                var users = userRepo.Items.Include(x => x.UserInGroup.Select(y => y.Group.States)).Where(u => u.UserInGroup.Any(gp => gp.ModuleTypeId == context.Module.Id) && u.IsActive).OrderBy(x => x.UserName).ToList();

                new BootStrapper().Configure();
                foreach (var user in users)
                {
                    var usr = Mapper.Map<TLBUser>(user);
                    userList.Add(usr);
                }
                userList.OrderBy(x => x.UserName);
                userList.Insert(0, new TLBUser { UserId = 0, UserName = TLBConstants.DefaultDDLValue });
                return userList;
            }

        }

        //internal void UpdateCaseFileAfterAssign(long userId, CaseDetails currentCase, bool isReAssign)
        //{
        //    var users = GetUsers();
        //    using (var tLBUnitOfWork = new TLBUnitOfWork())
        //    {
        //        var caseFileRepo = tLBUnitOfWork.GetRepository<TLBCaseFile>();
        //        var caseToUpdate = caseFileRepo.Items.FirstOrDefault(x => x.CfId == currentCase.CfId);
        //        caseToUpdate.UserId = (int)userId;
        //        if (!isReAssign)
        //        {
        //            caseToUpdate.StatusId = -1;
        //            caseToUpdate.LAssesmentTypeAnalyst = users.FirstOrDefault(x => x.UserId == userId).UserName;
        //        }
        //        else
        //        {
        //            if (currentCase.State.Id == -1)
        //                caseToUpdate.LAssesmentTypeAnalyst = users.FirstOrDefault(x => x.UserId == userId).UserName;
        //        }
        //        caseFileRepo.SaveChanges();
        //    }
        //}

        internal uint GetAssignedCasesCount()
        {
            return (uint)TLBViewModel.CaseList.Where(x => x.User.UserId != 0 || x.Status == Helper.Status.End).ToList().Count;
        }

        internal ObservableCollection<CaseDetails> GetCasesByIndexandFilters(uint startingIndex, uint numberOfRecords, bool? isAssigned = null,
            int? categoryId = null, long? userId = null, int? stateId = null, string caseNumber = null, bool isCaseManagement = false)
        {
            var data = new ObservableCollection<CaseDetails>();
            var cases = new List<CaseDetails>();
            cases = TLBViewModel.CaseList.ToList();
            if (isAssigned.HasValue && isAssigned.Value)
                cases = cases.Where(x => x.User.UserId != 0 || x.Status == Helper.Status.End).ToList();
            else if (isAssigned.HasValue && !isAssigned.Value)
                cases = cases.Where(x => x.User.UserId == 0 && x.Status != Helper.Status.End).ToList();

            if (isCaseManagement)
                cases = cases.Where(x => x.User.UserId != 0).ToList();

            if (categoryId.HasValue)
                cases = cases.Where(x => x.Category.Id == categoryId).ToList();

            if (userId.HasValue)
                cases = cases.Where(x => x.User.UserId == userId).ToList();

            if (stateId.HasValue)
            {
                if (stateId == -1)
                    cases = cases.Where(x => x.State.StateName.ToLower().Equals(Helper.Status.Assigned.ToLower())).ToList();
                else if (stateId == -2)
                    cases = cases.Where(x => x.State.StateName.ToLower().Equals(Helper.Status.End.ToLower())).ToList();
                else cases = cases.Where(x => x.State.Id == stateId).ToList();
            }

            if (caseNumber.IsNotNull())
                cases = cases.Where(x => x.CaseNumber == caseNumber).ToList();

            cases = cases.OrderBy(x => x.CfId).Skip((int)startingIndex).Take((int)numberOfRecords).ToList();

            foreach (var item in cases)
            {
                data.Add(item);
            }
            return data;
        }

        internal int GetPageSizeAfterFilters(bool? isAssigned = null, int? categoryId = null, long? userId = null, int? stateId = null, string caseNumber = null, bool isCaseManagement = false)
        {
            var cases = new List<CaseDetails>();
            cases = TLBViewModel.CaseList.ToList();
            if (isAssigned.HasValue && isAssigned.Value)
                cases = cases.Where(x => x.User.UserId != 0 || x.Status == Helper.Status.End).ToList();
            else if (isAssigned.HasValue && !isAssigned.Value)
                cases = cases.Where(x => x.User.UserId == 0 && x.Status != Helper.Status.End).ToList();

            if (isCaseManagement)
                cases = cases.Where(x => x.User.UserId != 0).ToList();

            if (categoryId.HasValue)
                cases = cases.Where(x => x.Category.Id == categoryId).ToList();

            if (userId.HasValue)
                cases = cases.Where(x => x.User.UserId == userId).ToList();

            if (stateId.HasValue)
                cases = cases.Where(x => x.State.Id == stateId).ToList();

            if (caseNumber.IsNotNull())
                cases = cases.Where(x => x.CaseNumber == caseNumber).ToList();

            return cases.Count;
        }

        internal uint GetUnAssignedCasesCount()
        {
            return (uint)TLBViewModel.CaseList.Where(x => x.User.UserId == 0 && x.Status != Helper.Status.End).ToList().Count;
        }

        //internal ObservableCollection<CaseDetails> GetUnAssignedCasesByIndex(uint startingIndex, uint numberOfRecords)
        //{
        //    var data = new ObservableCollection<CaseDetails>();
        //    using (var tLBUnitOfWork = new TLBUnitOfWork())
        //    {
        //        var stateList = GetStateList();
        //        var userList = GetUsers();
        //        var casesRepo = tLBUnitOfWork.GetRepository<TLBCaseFile>();
        //        var assignedCases = casesRepo.Items.Include(x => x.LookUp).
        //            Where(x => x.LookUp.Id == x.CategoryId).Where(x => x.UserId == 0).OrderBy(x => x.CfId).Skip((int)startingIndex).Take((int)numberOfRecords).ToList();
        //        foreach (var assignedCase in assignedCases)
        //        {
        //            var data1 = Mapper.Map<CaseDetails>(assignedCase);
        //            if (data1.HighestStatus.Id == 0)
        //                data1.HighestStatus = new State { Id = 0, StateName = "UnAssigned" };
        //            else
        //                data1.HighestStatus = stateList.Where(x => x.Id == data1.HighestStatus.Id).FirstOrDefault();

        //            if (data1.Status.Id == 0)
        //                data1.Status = new State { Id = 0, StateName = "UnAssigned" };
        //            else
        //                data1.Status = stateList.Where(x => x.Id == data1.Status.Id).FirstOrDefault();

        //            data1.User = userList.Where(x => x.UserId == data1.User.UserId).FirstOrDefault();
        //            if (data1.User.IsNull())
        //                data1.User = new TLBUser { UserId = 0, UserName = "Unknown" };
        //            else if (data1.User.UserId == 0)
        //                data1.User = new TLBUser { UserId = 0 };
        //            data.Add(data1);
        //        }
        //    }
        //    return data;
        //}

        internal ObservableCollection<ClientAccountViewModel> GetClientAccount(CaseFileCardViewModel caseFileCardViewModel)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var custAccounts = new List<string>();
                var caseTransactions = tlbUnitOfWork.GetRepository<CaseTransactions>().Items.Include(x => x.TLBTransNorm).Where(x => x.CfId == caseFileCardViewModel.DGSelectedItem.CfId).ToList();
                if (caseTransactions.IsCollectionValid())
                {
                    foreach (var caseTransaction in caseTransactions)
                    {
                        if (caseTransaction.TLBTransNorm != null)
                        {
                            var tlbTransNorm = caseTransaction.TLBTransNorm;
                            if (!custAccounts.Contains(caseTransaction.TLBTransNorm.CustomerAccount1))
                                custAccounts.Add(caseTransaction.TLBTransNorm.CustomerAccount1);
                            if (!custAccounts.Contains(caseTransaction.TLBTransNorm.CustomerAccount2))
                                custAccounts.Add(caseTransaction.TLBTransNorm.CustomerAccount2);
                            var clientAccounts = GetClientAccountsFromDb(custAccounts, tlbTransNorm);
                            if (clientAccounts.IsCollectionValid())
                                return clientAccounts;
                        }
                    }
                }
            }
            return new ObservableCollection<ClientAccountViewModel>();
        }

        private ObservableCollection<ClientAccountViewModel> GetClientAccountsFromDb(List<string> custAccounts, TLBTransNorm tlbTransNorm)
        {
            using (var tlbUnitofWork = new TLBUnitOfWork())
            {
                ObservableCollection<ClientAccountViewModel> clientAccounts = new ObservableCollection<ClientAccountViewModel>();
                if (custAccounts.IsCollectionValid())
                {
                    AssignClientAccounts(custAccounts, tlbUnitofWork, clientAccounts);
                    if (clientAccounts.IsCollectionValid())
                    {
                        List<decimal> incomingUsd = new List<decimal>();
                        List<decimal> outgoingUsd = new List<decimal>();
                        List<decimal> bankTransferUsd = new List<decimal>();
                        List<decimal> totalTransaction = new List<decimal>();

                        foreach (var clientAccnt in clientAccounts)
                        {
                            clientAccnt.TransactionId = tlbTransNorm.Id;
                            clientAccnt.CfId = tlbTransNorm.TLB_CaseTransactions.FirstOrDefault().CfId.Value;
                            if (tlbTransNorm.RecvPay.HasValue)
                                switch (tlbTransNorm.RecvPay.Value)
                                {
                                    case 1:
                                        clientAccnt.IncomingUsd = tlbTransNorm.PaymentAmount.Value * clientAccnt.Transactions;
                                        clientAccnt.OutgoingUsd = 0;
                                        clientAccnt.BankTransfersUsd = 0;
                                        break;
                                    case 2:
                                        clientAccnt.IncomingUsd = 0;
                                        clientAccnt.OutgoingUsd = tlbTransNorm.PaymentAmount.Value * clientAccnt.Transactions;
                                        clientAccnt.BankTransfersUsd = 0;
                                        break;
                                    case 3:
                                        clientAccnt.IncomingUsd = 0;
                                        clientAccnt.OutgoingUsd = 0;
                                        clientAccnt.BankTransfersUsd = tlbTransNorm.PaymentAmount.Value * clientAccnt.Transactions;
                                        if (clientAccnt.Account == tlbTransNorm.CustomerAccount1)
                                        {
                                            clientAccnt.BankTransfersUsdActual = tlbTransNorm.PaymentAmount.Value;
                                            clientAccnt.TransActual = 1;
                                        }
                                        else
                                        {
                                            clientAccnt.TransActual = 0;
                                        }
                                        break;
                                }
                            var paymentAmt = tlbTransNorm.PaymentAmount.Value * clientAccnt.Transactions;
                            incomingUsd.Add(clientAccnt.IncomingUsd);
                            outgoingUsd.Add(clientAccnt.OutgoingUsd);
                            bankTransferUsd.Add(clientAccnt.BankTransfersUsd);
                            totalTransaction.Add(paymentAmt);
                        }

                        clientAccounts.Each(x =>
                        {
                            x.TotalIncomingUsd = GetUsd(incomingUsd);
                            x.TotalOutgoingUsd = GetUsd(outgoingUsd);
                            x.TotalTransactions = GetUsd(totalTransaction);
                            x.TotalBankTransfersUsd = GetUsd(bankTransferUsd);

                        });
                    }
                    return clientAccounts;
                }
                return clientAccounts;
            }
        }

        private static void AssignClientAccounts(List<string> custAccounts, TLBUnitOfWork tlbUnitofWork, ObservableCollection<ClientAccountViewModel> clientAccounts)
        {
            foreach (var custAccount in custAccounts)
            {
                var clientAccountsFromDb = tlbUnitofWork.GetRepository<KAMP.Core.Repository.TLB.ClientAccounts>().Items.Where(x => x.Account == custAccount).OrderBy(x => x.AccountName).ToList();


                new BootStrapper().Configure();
                var mappedClientViewModel = Mapper.Map<List<KAMP.Core.Repository.TLB.ClientAccounts>, List<ClientAccountViewModel>>(clientAccountsFromDb);
                if (mappedClientViewModel.IsCollectionValid())

                    foreach (var clientAcctFrmDb in mappedClientViewModel)
                    {
                        if (custAccounts.Contains(clientAcctFrmDb.Account))
                        {
                            clientAcctFrmDb.Transactions = mappedClientViewModel.Count;
                            var clientAccnt = clientAccounts.FirstOrDefault(x => x.Account == clientAcctFrmDb.Account);
                            if (clientAccnt.IsNull())
                            {
                                clientAccounts.Add(clientAcctFrmDb);
                            }
                        }
                    }
            }
        }

        private decimal GetUsd(List<decimal> incomingUsd)
        {
            decimal totalIncomingUsd = 0;
            foreach (var incUsd in incomingUsd)
            {
                totalIncomingUsd += incUsd;
            }
            return totalIncomingUsd;
        }

        internal uint GetTotalCasesCount()
        {
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var casesRepo = tLBUnitOfWork.GetRepository<TLBCaseFile>();
                return (uint)casesRepo.Items.Count();
            }
        }

        internal ObservableCollection<TransactionDetail> GetTransactionsByCaseNo(int cfId)
        {
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var tlbTrasactionRepo = tLBUnitOfWork.GetRepository<TLBTransNorm>();
                var caseTransactionsRepo = tLBUnitOfWork.GetRepository<CaseTransactions>();
                var tlbTrasactionLookUpRepo = tLBUnitOfWork.GetRepository<TransnormLookUp>();
                var tlbTransTotalScoreRepo = tLBUnitOfWork.GetRepository<TransTotalScore>();
                var tlbTransScrubbedDataRepo = tLBUnitOfWork.GetRepository<TransactionScrubbed>();
                var tlbClientAccountsRepo = tLBUnitOfWork.GetRepository<KAMP.Core.Repository.TLB.ClientAccounts>();


                var data = (from ct in caseTransactionsRepo.Items
                            join transData in tlbTrasactionRepo.Items on ct.TransactionId equals transData.Id
                            join transLookUp in tlbTrasactionLookUpRepo.Items on transData.RecvPay.ToString() equals transLookUp.Reference
                            //join transSD in tlbTransScrubbedDataRepo.Items on ct.TransactionId equals transSD.Id
                            join transTS in tlbTransTotalScoreRepo.Items on ct.TransactionId equals transTS.Id
                            where ct.CfId == cfId
                            where transLookUp.TransnormLookUpGrpupId == 2
                            select new TransactionDetail
                            {
                                TotalScore = transTS.TotalScore,
                                Value = transLookUp.Value,
                                MessageType = transData.MessageType,
                                InternalReferenceNumber = transData.InternalReferenceNumber,
                                PaymentAmount = transData.PaymentAmount,
                                TransactionDate = transData.TransDate,
                                ValueDate = transData.ValueDate,
                                PaymentType = transData.PaymentType,

                                ClientAccount1 = tlbClientAccountsRepo.Items.FirstOrDefault(x => x.Account == transData.CustomerAccount1).AccountName,
                                ClientAccount2 = tlbClientAccountsRepo.Items.FirstOrDefault(x => x.Account == transData.CustomerAccount2).AccountName,
                                //ByOrderParty
                                ByOrderPartyNameBIC = transData.ByOrderPartyBIC,
                                ByOrderStanName = transData.ByOrderPartyNameNorm,
                                ByOrderPartyNameAddress = transData.ByOrderPartyAddress,
                                ByOrderPartyName = transData.ByOrderPartyName,
                                ByOrderPartyCountryName = transData.ByOrderPartyCountryName,
                                //Debit Bank
                                DebitBankBIC = transData.DebitBankBIC,
                                DebitBankAddress = transData.DebitBankAddress,
                                DebitBankStanName = transData.DebitBankNameNorm,
                                DebitBankName = transData.DebitBankName,
                                DebitBankCountryName = transData.DebitBankCountryName,
                                //Beneficiary Party
                                BeneficiaryBIC = transData.BenePartyBIC,
                                BeneficiaryAddress = transData.BenePartyAddress,
                                BeneficiaryStanName = transData.BenePartyNameNorm,
                                BenePartyName = transData.BenePartyName,
                                BenePartyCountryName = transData.BenePartyCountryName,
                                //Credit Bank
                                CreditBankBIC = transData.CreditBankID,
                                CreditBankAddress = transData.CreditBankAddress,
                                CreditBankStanName = transData.CreditBankNameNorm,
                                CreditBankName = transData.CreditBankName,
                                CreditBankCountryName = transData.CreditBankCountryName,
                                //Originating Bank
                                OriginatingBankBIC = transData.OriginatingBankBIC,
                                OriginatingBankAddress = transData.OriginatingBankAddress,
                                OriginatingBankStanName = transData.OriginatingBankNameNorm,
                                OriginatingBankName = transData.OriginatingBankName,
                                OriginatingBankCountryName = transData.OriginatingBankCountryName,
                                //Instructing Bank
                                InstructingBankBIC = transData.InstructingBankBIC,
                                InstructingBankAddress = transData.InstructingBankAddress,
                                InstructingBankStanName = transData.InstructingBankNameNorm,
                                InstructingBankName = transData.InstructingBankName,
                                InstructingBankCountryName = transData.InstructingBankCountryName,
                                //Intermediary Bank
                                IntermediaryBankBIC = transData.IntermediaryBankBIC,
                                IntermediaryBankAddress = transData.IntermediaryBankAddress,
                                IntermediaryBankStanName = transData.IntermediaryBankNameNorm,
                                IntermediaryBankName = transData.IntermediaryBankName,
                                IntermediaryBankCountryName = transData.IntermediaryBankCountryName,
                                //Beneficiary Bank
                                BeneficiaryBankBIC = transData.BeneficiaryBankBIC,
                                BeneficiaryBankAddress = transData.BeneficiaryBankAddress,
                                BeneficiaryBankStanName = transData.BeneficiaryBankNameNorm,
                                BeneficiaryBankName = transData.BeneficiaryBankName,
                                BeneficiaryBankCountryName = transData.BeneficiaryBankCountryName,
                                BanktoBeneInfo = transData.BanktoBeneInfo,

                                OriginatortoBeneInfo = transData.OriginatortoBeneInfo,
                                Id = transData.Id,
                                DebitBankInstructions = transData.DebitBankInstructions,
                                CreditBankAdvice = transData.CreditBankAdvice,
                                BeneficiaryBankAdvice = transData.BeneficiaryBankAdvice,
                                CreditBankInstructions = transData.CreditBankInstructions,
                                SAR = ct.SAR
                                //transSD.ByOrderPartyNameScrubbed,
                                //transSD.BenePartyNameScrubbed,
                            }).ToList();
                ObservableCollection<TransactionDetail> details = new ObservableCollection<TransactionDetail>();
                data.ForEach(details.Add);
                return details;
            }
        }

        internal ObservableCollection<TransactionDetail> GetTransactions(int cfId, int transId)
        {
            ObservableCollection<TransactionDetail> details = new ObservableCollection<Models.TransactionDetail>();
            if (transId != 0)
            {
                var trans = GetTransactionsByCaseNo(cfId).Where(x => x.Id == transId).ToList();
                trans.ForEach(details.Add);
            }

            return details;
        }

        internal ObservableCollection<TransactionDetail> GetTransactions(int cfId, List<int> transIds)
        {
            ObservableCollection<TransactionDetail> details = new ObservableCollection<Models.TransactionDetail>();
            if (transIds.IsCollectionValid())
            {
                var trans = GetTransactionsByCaseNo(cfId).Where(x => transIds.Contains(x.Id)).ToList();
                trans.ForEach(details.Add);
            }
            return details;
        }

        internal ObservableCollection<Assessment> GetAssessments(CaseFileCardViewModel caseFileCardViewModel)
        {
            if (caseFileCardViewModel.IsNotNull())
            {
                using (var tlbUnitOfWork = new TLBUnitOfWork())
                {
                    var transNorm = tlbUnitOfWork.GetRepository<TLBTransNorm>().Items.Include(x => x.TLB_ScoresTransactions).Include(x => x.TLB_CaseTransactions)
                        .Where(x => x.TLB_CaseTransactions.Any(y => y.CfId == caseFileCardViewModel.DGSelectedItem.CfId)).ToList();
                    List<Assesments> lstAssessments = new List<Assesments>();
                    List<int> asstIds = new List<int>();
                    var assessmentVmLst = new ObservableCollection<Assessment>();
                    var scoresTrans = transNorm.SelectMany(x => x.TLB_ScoresTransactions).ToList();
                    foreach (var scoresTran in scoresTrans)
                    {
                        var assessment = tlbUnitOfWork.GetRepository<Assesments>().Items
                            .FirstOrDefault(x => x.Id == scoresTran.AssesmentId);

                        if (!asstIds.Contains(assessment.Id))
                            asstIds.Add(assessment.Id);
                        lstAssessments.Add(assessment);
                    }
                    if (lstAssessments.IsCollectionValid())
                    {
                        int count = 0;
                        int occurrences = 0;
                        int totalScore = 0;
                        //List<int> transactionIds = new List<int>();
                        foreach (var assmt in lstAssessments)
                        {
                            //transactionIds.Add(assmt.Id);
                            var assessmentViewModel = new Assessment() { TransactionIds = new List<int>() };
                            if (asstIds.Contains(assmt.Id))
                            {
                                assessmentViewModel.Occurrences = lstAssessments.Count(x => x.Id == assmt.Id);
                                if (count < assessmentViewModel.Occurrences)
                                    count++;
                                else if (count > assessmentViewModel.Occurrences)
                                    count = 0;
                                if (!(count > 1))
                                {
                                    assessmentViewModel.AssessmentId = assmt.Id;
                                    assessmentViewModel.Score = assmt.TransactionScore.Value;
                                    occurrences += assessmentViewModel.Occurrences;
                                    assessmentViewModel.TotalScore = assessmentViewModel.Score * assessmentViewModel.Occurrences;
                                    totalScore += assessmentViewModel.TotalScore;
                                    assessmentViewModel.AssessmentName = assmt.Description;
                                    assessmentViewModel.AssessmentTypeId = assmt.AssesmentTypeId;
                                    assessmentViewModel.TransactionScore = assmt.TransactionScore.Value;
                                    assessmentViewModel.AccountScore = assmt.AccountScore.Value;
                                    assessmentViewModel.AssessmentDetail = assmt.Detail;
                                    assessmentViewModel.TransactionIds.Add(assmt.Id);
                                    assessmentViewModel.CfId = caseFileCardViewModel.DGSelectedItem.CfId;
                                    assessmentViewModel.AssessmentTypes = tlbUnitOfWork.GetRepository<KAMP.Core.Repository.TLB.AssesmentType>().Items.ToList();
                                    assessmentVmLst.Add(assessmentViewModel);
                                }
                            }
                        }
                    }
                    return assessmentVmLst;
                }

            }
            return new ObservableCollection<Assessment>();
        }

        internal ObservableCollection<Assesments> GetRiskAssessment()
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var assessmentCollection = new ObservableCollection<Assesments>();
                var assessments = tlbUnitOfWork.GetRepository<Assesments>().Items.ToList();
                assessments.ForEach(x =>
                {
                    if (!x.Compiled.HasValue)
                        x.Compiled = false;
                    if (!x.Approved.HasValue)
                        x.Approved = false;
                    assessmentCollection.Add(x);
                });
                return assessmentCollection;
            }
        }

        private string GetAssessmentTypeName(int p)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var assessTypeName = tlbUnitOfWork.GetRepository<AssesmentType>().Items.FirstOrDefault(x => x.Id == p);
                return assessTypeName.AssesmentTypeDescription;
            }

        }

        internal Module GetModuleDetails(string moduleName)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var abc = tlbUnitOfWork.GetRepository<Module>();
                return tlbUnitOfWork.GetRepository<Module>().Items.FirstOrDefault(x => x.ModuleName.ToLower() == moduleName.ToLower());
            }
        }

        internal ObservableCollection<TransactionDetail> GetTransnormByClientName(string custAccName)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var tlbTransNorm = tlbUnitOfWork.GetRepository<TLBTransNorm>().Items
                    .Include(y => y.TransTotalScore)
                    .Include(y => y.TLB_CaseTransactions)
                    .Where(x => x.CustomerAccount1 == custAccName || x.CustomerAccount2 == custAccName).ToList();
                List<int> cfIdLst = new List<int>();
                foreach (var trnsNorm in tlbTransNorm)
                {
                    if (trnsNorm.TLB_CaseTransactions.IsCollectionValid())
                    {
                        trnsNorm.TLB_CaseTransactions.Each(x =>
                        {
                            cfIdLst.Add(x.CfId.Value);
                        });
                    }
                }
                var transDetail = GetTransactionsByCaseNo(cfIdLst.FirstOrDefault());
                return transDetail;
            }
        }

        internal ObservableCollection<TransactionHitsModel> GetTransaction(int transactionId)
        {
            var data = new ObservableCollection<TransactionHitsModel>();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var collection = tlbUnitOfWork.GetRepository<ScoresTransactions>().Items
                 .Include(x => x.Assesments)
                 .Include(x => x.TLBParameters)
                 .Include(x => x.TLBTransNorm)
                 .Where(x => x.TransactionId == transactionId).ToList();
                foreach (var item in collection)
                {
                    TransactionHitsModel transHits = Mapper.Map<TransactionHitsModel>(item);
                    transHits.ScoredFieldValue = item.TLBTransNorm.GetType().GetProperty(item.FIELD).GetValue(item.TLBTransNorm, null).ToString();
                    data.Add(transHits);
                }
            }
            return data;
        }

        internal List<AssessmentHitsViewModel> GetAssessmentHits(Assessment selectedItemfrmGrid)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                if (selectedItemfrmGrid.TransactionIds.IsCollectionValid())
                {
                    var tlbTransNorm = tlbUnitOfWork.GetRepository<TLBTransNorm>().Items
                      .Include(y => y.TransTotalScore)
                      .Include(y => y.TLB_ScoresTransactions.Select(z => z.TLBParameters.TLBParameterGroups))
                      .Include(y => y.TLB_CaseTransactions)
                      .Where(x => selectedItemfrmGrid.TransactionIds.Contains(x.Id)).ToList();
                    List<AssessmentHitsViewModel> assessmentHitVM = new List<AssessmentHitsViewModel>();
                    if (tlbTransNorm.IsCollectionValid())
                    {
                        foreach (var scoreTrans in tlbTransNorm.Select(x => x.TLB_ScoresTransactions.Where(y => y.AssesmentId == selectedItemfrmGrid.AssessmentId).ToList()))
                        {
                            if (scoreTrans.IsCollectionValid())
                            {
                                scoreTrans.ForEach(x =>
                                {
                                    var assessmentViewModel = new AssessmentHitsViewModel() { AssessmentHitByCategoryViewModel = new List<AssessmentHitByCategoryViewModel>() };
                                    assessmentViewModel.AssessmentId = x.AssesmentId.Value;
                                    assessmentViewModel.Parameter = x.TLBParameters.ActualValue;
                                    assessmentViewModel.SearchTerm = x.TLBParameters.Value;
                                    assessmentViewModel.Occurrences = 1;
                                    assessmentViewModel.TotalOccurrences = scoreTrans.Count;

                                    var assessmentHitsByCatVM = new AssessmentHitByCategoryViewModel()
                                        {
                                            GroupName = x.TLBParameters.TLBParameterGroups.GroupName,
                                            ParameterGroupId = x.TLBParameters.ParameterGroupID
                                        };
                                    assessmentViewModel.AssessmentHitByCategoryViewModel.Add(assessmentHitsByCatVM);
                                    assessmentHitVM.Add(assessmentViewModel);
                                });
                            }
                        }
                    }
                    return assessmentHitVM;
                }
                return new List<AssessmentHitsViewModel>();
            }
        }

        internal DispositionsViewModel GetDispositions(int cfId)
        {
            var context = Application.Current.Properties["Context"] as Context;
            var currentUserId = context.User.Id;
            var data = new DispositionsViewModel();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var caseData = tlbUnitOfWork.GetRepository<TLBCaseFile>().Items.FirstOrDefault(x => x.CfId == cfId);
                data.MamorandumofFact = caseData.MemorandumOfFact;
                data.RiskProfile = caseData.RiskProfile;
                data.Duediligence = caseData.DueDiligence;
                data.ExecSummary = caseData.ExecutiveSummary;
                data.CfId = cfId;

                var caseRelatedData = WorkflowAPI.GetCaseDetails(caseData.CaseNo);

                if (caseData.IsNotNull() && caseRelatedData.IsNotNull())
                {
                    if (caseRelatedData.Status.Equals(Helper.Status.UnAssigned) || caseRelatedData.Status.Equals(Helper.Status.End))
                    {
                        data.IsTabEnabled = false;
                    }
                    if (caseRelatedData.UserId != currentUserId)
                    {
                        data.IsTabEnabled = false;
                    }
                }
            }
            return data;
        }

        internal void UpdateDispositions(DispositionsViewModel viewModel)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var caseDataRepo = tlbUnitOfWork.GetRepository<TLBCaseFile>();
                var caseData = caseDataRepo.Items.FirstOrDefault(x => x.CfId == viewModel.CfId);
                caseData.ExecutiveSummary = viewModel.ExecSummary;
                caseData.DueDiligence = viewModel.Duediligence;
                caseData.RiskProfile = viewModel.RiskProfile;
                caseData.MemorandumOfFact = viewModel.MamorandumofFact;
                tlbUnitOfWork.SaveChanges();
            }
        }

        internal ObservableCollection<CaseDetails> RelatedCasesList(int cfId)
        {
            var collection = new ObservableCollection<CaseDetails>();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var caseDataRepo = tlbUnitOfWork.GetRepository<TLBCaseFile>();
                var caseClientRelatedRepo = tlbUnitOfWork.GetRepository<CaseClientRelated>();
                var cfIds = caseClientRelatedRepo.Items.Where(x => x.cfId != x.cfId2 && x.cfId == cfId && x.IsClient == 0).Select(x => x.cfId2).ToList();
                var cases = caseDataRepo.Items.Where(x => cfIds.Any(y => y == x.CfId)).ToList();
                foreach (var item in cases)
                {
                    new BootStrapper().Configure();
                    var data = Mapper.Map<CaseDetails>(item);
                    collection.Add(data);
                }
            }
            return collection;
        }

        internal RelatedPartiesViewModel GetRelatedParties(int cfId, int relatedCfId)
        {
            var collection = new ObservableCollection<KAMP.TLB.Models.RelatedParties>();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var caseClientRepo = tlbUnitOfWork.GetRepository<CaseClientRelated>();
                var list = caseClientRepo.Items.Where(x => x.cfId == cfId && x.cfId2 == relatedCfId).ToList();
                var caseDataRepo = tlbUnitOfWork.GetRepository<TLBCaseFile>();

                foreach (var item in list)
                {
                    var caseNumber = caseDataRepo.Items.FirstOrDefault(x => x.CfId == item.cfId2).CaseNo;
                    var data = new KAMP.TLB.Models.RelatedParties { CaseNumber = caseNumber, PartyName = item.TransactionPartyName };
                    collection.Add(data);
                }
            }
            return new RelatedPartiesViewModel { CfId = cfId, RelatedPartiesList = collection };
        }

        //internal void UpdateCaseAfterWFProcess(string caseNo, int stateId, long userId)
        //{
        //    using (var tlbUnitOfWork = new TLBUnitOfWork())
        //    {
        //        var tlbCaseFileRepo = tlbUnitOfWork.GetRepository<TLBCaseFile>();
        //        var caseData = tlbCaseFileRepo.Items.FirstOrDefault(x => x.CaseNo == caseNo);
        //        caseData.StatusId = stateId;
        //        if (stateId > caseData.HighStatusId)
        //            caseData.HighStatusId = stateId;
        //        caseData.UserId = Convert.ToInt32(userId);
        //        tlbCaseFileRepo.SaveChanges();
        //    }
        //}

        internal BeneficiariesViewModel GetBeneficiaries(int cfId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var orderingPartyViewModel = new BeneficiariesViewModel();
                var transactionNormsRepo = tlbUnitOfWork.GetRepository<TLBTransNorm>();
                var transNorms = tlbUnitOfWork.GetRepository<TLBTransNorm>().Items.Include(x => x.TransTotalScore).Include(x => x.TLB_ScoresTransactions).Include(x => x.TLB_CaseTransactions)
                    .Where(x => x.TLB_CaseTransactions.Any(y => y.CfId == cfId)).ToList().GroupBy(z => z.BenePartyName == null ? "UNKNOWN" : z.BenePartyName).ToList();

                if (transNorms.IsCollectionValid())
                {
                    orderingPartyViewModel.Beneficiary = new ObservableCollection<Models.Beneficiary>();
                    foreach (var key in transNorms)
                    {
                        int totalScore = 0;
                        int caseTrasanctionCount = 0;
                        decimal totalAmount = 0;
                        var date = new List<DateTime>();
                        var transIds = new List<int>();
                        foreach (var item in key)
                        {
                            totalScore = totalScore + item.TransTotalScore.Select(x => x.TotalScore.Value).Sum();
                            caseTrasanctionCount = caseTrasanctionCount + item.TLB_CaseTransactions.Count;
                            totalAmount = totalAmount + item.PaymentAmount.Value;
                            date.Add(item.ValueDate.Value);
                            transIds.Add(item.Id);
                        }
                        var orderingParty = new Models.Beneficiary
                        {
                            CfId = cfId,
                            Beneficiaries = key.Key,
                            Score = totalScore,
                            AvgScore = totalScore / caseTrasanctionCount,
                            Transactions = caseTrasanctionCount,
                            TransactionIds = transIds,
                            TotalAmount = totalAmount,
                            FirstTransaction = date.Min(x => x),
                            LastTransaction = date.Max(x => x),
                        };
                        orderingPartyViewModel.Beneficiary.Add(orderingParty);
                    }
                    orderingPartyViewModel.OverallTotalAmount = orderingPartyViewModel.Beneficiary.Select(x => x.TotalAmount).Sum();
                    orderingPartyViewModel.OverallTotalScore = orderingPartyViewModel.Beneficiary.Select(x => x.Score).Sum();
                    orderingPartyViewModel.OverallTotalTransactions = orderingPartyViewModel.Beneficiary.Select(x => x.Transactions).Sum();

                }
                return orderingPartyViewModel;
            }
        }

        internal OrderingPartyViewModel GetOrderingParty(int cfId)
        {
            //to do code need to be updated once the requirement is clear.
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var orderingPartyViewModel = new OrderingPartyViewModel();
                var transactionNormsRepo = tlbUnitOfWork.GetRepository<TLBTransNorm>();
                var transNorms = tlbUnitOfWork.GetRepository<TLBTransNorm>().Items.Include(x => x.TransTotalScore).Include(x => x.TLB_ScoresTransactions).Include(x => x.TLB_CaseTransactions)
                    .Where(x => x.TLB_CaseTransactions.Any(y => y.CfId == cfId)).ToList().GroupBy(z => z.ByOrderPartyName == null ? "UNKNOWN" : z.ByOrderPartyName).ToList();

                if (transNorms.IsCollectionValid())
                {
                    orderingPartyViewModel.OrderingParty = new ObservableCollection<Models.OrderingParty>();
                    foreach (var key in transNorms)
                    {
                        int totalScore = 0;
                        int caseTrasanctionCount = 0;
                        decimal totalAmount = 0;
                        var date = new List<DateTime>();
                        var transIds = new List<int>();
                        foreach (var item in key)
                        {
                            totalScore = totalScore + item.TransTotalScore.Select(x => x.TotalScore.Value).Sum();
                            caseTrasanctionCount = caseTrasanctionCount + item.TLB_CaseTransactions.Count;
                            totalAmount = totalAmount + item.PaymentAmount.Value;
                            date.Add(item.ValueDate.Value);
                            transIds.Add(item.Id);
                        }
                        var orderingParty = new Models.OrderingParty
                        {
                            CfId = cfId,
                            OrderingPartyName = key.Key,
                            Score = totalScore,
                            AvgScore = totalScore / caseTrasanctionCount,
                            Transactions = caseTrasanctionCount,
                            TransactionIds = transIds,
                            TotalAmount = totalAmount,
                            FirstTransaction = date.Min(x => x),
                            LastTransaction = date.Max(x => x),
                        };
                        orderingPartyViewModel.OrderingParty.Add(orderingParty);
                    }
                    orderingPartyViewModel.OverallTotalAmount = orderingPartyViewModel.OrderingParty.Select(x => x.TotalAmount).Sum();
                    orderingPartyViewModel.OverallTotalScore = orderingPartyViewModel.OrderingParty.Select(x => x.Score).Sum();
                    orderingPartyViewModel.OverallTotalTransactions = orderingPartyViewModel.OrderingParty.Select(x => x.Transactions).Sum();

                }
                return orderingPartyViewModel;
            }
        }

        /// <summary>
        /// Get user from analyst group
        /// </summary>
        /// <returns>users</returns>
        internal List<User> GetUsersFromAnalystGroup()
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var userRepo = wfUOW.GetRepository<User>();
                return userRepo.Items.Include(x => x.UserInGroup).Where(x => x.IsActive && x.UserInGroup.
                    Any(gp => gp.IsActive && gp.ModuleTypeId == appcontext.Module.Id && gp.Group.GroupName.ToLower() == Helper.Group.Analyst.ToLower())).ToList();
            }
        }



        internal User GetUser(long userId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                return tlbUnitOfWork.GetRepository<User>().Items.FirstOrDefault(x => x.UserId == userId);
            }
        }

        internal List<AssesmentType> GetAssessmentTypes()
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var assessmentTypes = tlbUnitOfWork.GetRepository<AssesmentType>().Items.ToList();
                return assessmentTypes;
            }
        }

        internal ObservableCollection<LookUpGroups> GetLookGroups()
        {
            var collection = new ObservableCollection<LookUpGroups>();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var data = tlbUnitOfWork.GetRepository<LookUpGroups>().Items.OrderBy(x => x.Name).ToList();
                data.Insert(0, new LookUpGroups { Id = 0, Name = TLBConstants.DefaultDDLValue });
                data.ForEach(collection.Add);
            }
            return collection;
        }

        internal ObservableCollection<LookUp> GetLookUp(int lookUpGroupId)
        {
            var collection = new ObservableCollection<LookUp>();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var data = tlbUnitOfWork.GetRepository<LookUp>().Items.Where(x => x.LookGroupId == lookUpGroupId).ToList();
                data.ForEach(collection.Add);
            }
            return collection;
        }

        internal List<LookUp> GetQueryType()
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var lookUp = tlbUnitOfWork.GetRepository<LookUp>().Items.Where(x => x.LookGroupId == 1).ToList();
                return lookUp;
            }
        }


        internal List<FilterCriteria> GetAssessmentFilters(int assessmentId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var assessmentFilters = tlbUnitOfWork.GetRepository<FilterCriteria>().Items.Where(x => x.ass_ID != assessmentId).OrderBy(x => x.fcr_ORDER).ToList();
                return assessmentFilters;
            }
        }

        internal List<FilterCriteria> GetSelectedComponent(int assessmentId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                List<FilterCriteria> lstFilterCriteria = new List<FilterCriteria>();
                var filterCriteria = tlbUnitOfWork.GetRepository<FilterCriteria>().Items
                    .Where(x => x.ass_ID == assessmentId)
                    .GroupBy(x => new { x.fcr_FIELD, x.fcr_CRITERIA, x.fcr_OPERATOR })
                    .OrderBy(y => new { y.Key.fcr_FIELD, y.Key.fcr_OPERATOR }).ToList();
                foreach (var criteria in filterCriteria.ToList())
                {
                    var res = criteria.ToList();
                    foreach (var fc in res)
                    {
                        lstFilterCriteria.Add(fc);
                    }
                }
                return lstFilterCriteria;
            }
        }

        internal FilterCriteria GetFilterCriteria(int fcrId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var filterCriteria = tlbUnitOfWork.GetRepository<FilterCriteria>().Items.FirstOrDefault(x => x.fcr_ID == fcrId);
                return filterCriteria;
            }
        }

        internal void AddFilterCriteria(Models.AvailableComponent availableComponent)
        {
            // new BootStrapper().Configure();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var mappedFIlterCritria = Mapper.Map<AvailableComponent, FilterCriteria>(availableComponent);
                if (mappedFIlterCritria.ass_ID == availableComponent.AssessmentId)
                {
                    var filterCriteriaFrmDb = tlbUnitOfWork.GetRepository<FilterCriteria>().Items.FirstOrDefault(x => x.fcr_ID == availableComponent.FcrId);
                    if (filterCriteriaFrmDb.IsNotNull())
                    {
                        filterCriteriaFrmDb.fcr_ID = mappedFIlterCritria.fcr_ID;
                        filterCriteriaFrmDb.fcr_OPERATOR = mappedFIlterCritria.fcr_OPERATOR;
                        filterCriteriaFrmDb.fcr_FIELD = mappedFIlterCritria.fcr_FIELD;
                        filterCriteriaFrmDb.fcr_ORDER = mappedFIlterCritria.fcr_ORDER;
                        filterCriteriaFrmDb.fcr_TYPE = mappedFIlterCritria.fcr_TYPE;
                        filterCriteriaFrmDb.fcr_CRITERIA = mappedFIlterCritria.fcr_CRITERIA;
                        filterCriteriaFrmDb.ass_ID = mappedFIlterCritria.ass_ID;

                        tlbUnitOfWork.GetRepository<FilterCriteria>().Update(filterCriteriaFrmDb);
                    }
                    else
                    {
                        tlbUnitOfWork.GetRepository<FilterCriteria>().Insert(mappedFIlterCritria);
                    }

                }
                else
                {
                    mappedFIlterCritria.fcr_ID = 0;
                    tlbUnitOfWork.GetRepository<FilterCriteria>().Insert(mappedFIlterCritria);
                }

                tlbUnitOfWork.SaveChanges();
            }
        }

        internal List<FieldsViewModel> GetFieldsFromSysTable()
        {
            //var sysColumns = _tlbDataAccess.GetSystemColumns();
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var fieldVmLst = new List<FieldsViewModel>();
                string entityFilter = "trn_";
                var sysColumns = ObjectContextExtensions.GetAttributesByFilter(_context, entityFilter).ToList();
                if (sysColumns.IsCollectionValid())
                {
                    foreach (var columnName in sysColumns)
                    {
                        var fieldViewModel = new FieldsViewModel()
                                                                    {
                                                                        FieldName = columnName
                                                                    };

                        fieldVmLst.Add(fieldViewModel);
                    }

                }
                return fieldVmLst;
            }
        }

        internal CaseDetails GetCaseDetails(string caseNo)
        {
            CaseDetails data = new CaseDetails();
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var casesRepo = tLBUnitOfWork.GetRepository<TLBCaseFile>();
                var caseData = casesRepo.Items.Include(x => x.LookUp).Where(x => x.LookUp.Id == x.CategoryId).FirstOrDefault(x => x.CaseNo == caseNo);

                var caseRelatedData = WorkflowAPI.GetCaseDetails(caseNo);

                data = Mapper.Map<CaseDetails>(caseData);
                data.State = new State { Id = Convert.ToInt32(caseRelatedData.StateId), StateName = caseRelatedData.State };
                data.Status = caseRelatedData.Status;
                data.OpenDate = caseRelatedData.CaseOpenDate;
                data.IsOpen = caseRelatedData.IsOpen;
                data.CloseDate = caseRelatedData.CaseClosedDate;
                //TODO Need To discuss how to calculate
                data.DaysInQueue = caseRelatedData.DaysInQueue;
                data.User = new Models.TLBUser { UserId = caseRelatedData.UserId, UserName = caseRelatedData.UserName };
                data.HighestStatus = new State { StateName = caseRelatedData.HighestStatus };
                data.Analyst = caseRelatedData.LastAnalyst;
                return data;
            }
        }

        internal void DeleteSelectedComponent(AvailableComponent selectedItemfrmGrid)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var filterCriteria = tlbUnitOfWork.GetRepository<FilterCriteria>().Items.FirstOrDefault(x => x.fcr_ID == selectedItemfrmGrid.FcrId);
                tlbUnitOfWork.GetRepository<FilterCriteria>().Delete(filterCriteria);
                tlbUnitOfWork.SaveChanges();
            }
        }


        internal Assesments UpdateAssessment(Assessment assessmentViewModel)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var assessmentFromDb = tlbUnitOfWork.GetRepository<Assesments>().Items.FirstOrDefault(x => x.Id == assessmentViewModel.AssessmentId);
                assessmentFromDb.AssesmentTypeId = assessmentViewModel.AssessmentTypeId;
                assessmentFromDb.Description = assessmentViewModel.AssessmentName;
                assessmentFromDb.AccountScore = assessmentViewModel.AccountScore.Value;
                assessmentFromDb.TransactionScore = assessmentViewModel.TransactionScore.Value;
                assessmentFromDb.AssessmentStoredProcedure_ID = assessmentViewModel.LookUpId;
                tlbUnitOfWork.GetRepository<Assesments>().Update(assessmentFromDb);
                tlbUnitOfWork.SaveChanges();
                return assessmentFromDb;
            }

        }

        internal AssessmentListViewModel GetParameters(int assessmentId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var availableParameters = new ObservableCollection<KAMP.TLB.Models.Parameters>();
                var parametersColl = new ObservableCollection<KAMP.TLB.Models.Parameters>();
                var assessmentListVm = new AssessmentListViewModel() { Parameters = new ObservableCollection<Models.Parameters>(), SelectedParameters = new ObservableCollection<Models.Parameters>() };
                var parameters = tlbUnitOfWork.GetRepository<ParameterGroups>().Items.ToList();
                var assessmentFields = tlbUnitOfWork.GetRepository<AssesmentField>().Items.Where(x => x.AssesmentId == assessmentId).ToList();
                foreach (var param in parameters)
                {
                    var res = new List<AssesmentField>();
                    if (assessmentFields.IsCollectionValid())
                    {
                        res = assessmentFields.Where(x => x.parameterGroupId == param.Id).ToList();
                        foreach (var result in res)
                        {
                            var parameter = new KAMP.TLB.Models.Parameters()
                            {
                                GroupId = result.parameterGroupId.Value,
                                FieldName = result.FieldName,
                                GroupName = parameters.FirstOrDefault(x => x.Id == result.parameterGroupId).GroupName,
                                Id = result.Id
                            };
                            parametersColl.Add(parameter);
                        }
                    }
                    var avParameters = new KAMP.TLB.Models.Parameters()
                    {
                        GroupId = Convert.ToInt32(param.Id),
                        GroupName = param.GroupName
                    };
                    availableParameters.Add(avParameters);
                }
                assessmentListVm.Parameters = availableParameters;
                assessmentListVm.SelectedParameters = parametersColl;
                return assessmentListVm;
            }
        }

        internal void AddAssessmentField(ChooseFieldsViewModel chooseFieldViewModel)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                if (chooseFieldViewModel.IsNotNull())
                {
                    var mappedAssessmentField = Mapper.Map<ChooseFieldsViewModel, AssesmentField>(chooseFieldViewModel);
                    tlbUnitOfWork.GetRepository<AssesmentField>().Insert(mappedAssessmentField);
                    tlbUnitOfWork.SaveChanges();
                }

            }
        }

        internal void DeleteSelectedField(Models.Parameters selectedItem)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var assessmentFieldFromDb = tlbUnitOfWork.GetRepository<AssesmentField>().Items.FirstOrDefault(x => x.Id == selectedItem.Id);
                if (assessmentFieldFromDb.IsNotNull())
                {
                    tlbUnitOfWork.GetRepository<AssesmentField>().Delete(assessmentFieldFromDb);
                    tlbUnitOfWork.SaveChanges();
                }
            }
        }

        internal EditParameterListViewModel GetParameters(EditParameterListViewModel editParamLstVm)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                if (editParamLstVm.IsNotNull())
                {
                    ObservableCollection<TLBParameterGroups> paramGrps = new ObservableCollection<TLBParameterGroups>();
                    List<TLBParameterGroups> parameterGroups = new List<TLBParameterGroups>();
                    List<TLBParameters> parameterFromDb = new List<TLBParameters>();
                    if (editParamLstVm.ParameterGroupId == 0)
                    {
                        parameterFromDb = tlbUnitOfWork.GetRepository<TLBParameters>().Items.ToList();
                        if (parameterFromDb.IsCollectionValid())
                        {
                            var mappedParameters = Mapper.Map<List<TLBParameters>, ObservableCollection<RiskParameter>>(parameterFromDb);
                            editParamLstVm.RiskParamters = mappedParameters;
                        }
                    }
                    else
                    {
                        editParamLstVm.RiskParamters = GetRiskParameters(editParamLstVm.ParameterGroupId);
                    }
                    parameterGroups = tlbUnitOfWork.GetRepository<TLBParameterGroups>().Items.ToList();

                    parameterGroups.ForEach(paramGrps.Add);
                    editParamLstVm.ParameterGroups = paramGrps;
                }
                return editParamLstVm;
            }
        }

        internal ObservableCollection<RiskParameter> GetRiskParameters(int parameterGrpId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {

                List<TLBParameters> parameterFromDb = new List<TLBParameters>();
                if (parameterGrpId == 0)
                    parameterFromDb = tlbUnitOfWork.GetRepository<TLBParameters>().Items.ToList();
                else
                    parameterFromDb = tlbUnitOfWork.GetRepository<TLBParameters>().Items.Where(x => x.ParameterGroupID == parameterGrpId).ToList();
                if (parameterFromDb.IsCollectionValid())
                {
                    var mappedParameters = Mapper.Map<List<TLBParameters>, ObservableCollection<RiskParameter>>(parameterFromDb);
                    return mappedParameters;
                }
                return new ObservableCollection<Models.RiskParameter>();
            }
        }

        internal RiskParameter GetParameterById(int parameterId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                if (parameterId != 0)
                {
                    var parameter = tlbUnitOfWork.GetRepository<TLBParameters>().Items.FirstOrDefault(x => x.Id == parameterId);
                    if (parameter.IsNotNull())
                    {
                        var mappedParameter = Mapper.Map<TLBParameters, RiskParameter>(parameter);
                        return mappedParameter;
                    }
                }
                return new Models.RiskParameter();
            }
        }
        internal TLBParameters GetParameter(string paramName)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var paramFrmDb = tlbUnitOfWork.GetRepository<TLBParameters>().Items.FirstOrDefault(x => x.Value == paramName);
                return paramFrmDb;
            }
        }

        internal void AddParameter(EditParameterListViewModel editParameterVM)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var paramFrmDb = tlbUnitOfWork.GetRepository<TLBParameters>().Items.FirstOrDefault(x => x.Value == editParameterVM.ParameterValue && x.ParameterGroupID == editParameterVM.ParameterGroupId);
                if (paramFrmDb.IsNull())
                {
                    var tlbPrameters = new TLBParameters()
                        {
                            ParameterGroupID = editParameterVM.ParameterGroupId,
                            Value = editParameterVM.ParameterValue,
                            ValueNoSpace = editParameterVM.ParameterValue.Replace(" ", ""),
                            ActualValue = editParameterVM.ParameterValue
                        };

                    tlbUnitOfWork.GetRepository<TLBParameters>().Insert(tlbPrameters);
                    tlbUnitOfWork.SaveChanges();
                }
            }
        }

        internal void UpdateParameter(EditParameterListViewModel editParameterVM)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var parameterFromDb = tlbUnitOfWork.GetRepository<TLBParameters>().Items.FirstOrDefault(x => x.Id == editParameterVM.Id);
                if (parameterFromDb.IsNotNull())
                {
                    parameterFromDb.Value = editParameterVM.ParameterValue;
                    parameterFromDb.ValueNoSpace = editParameterVM.ParameterValue.Replace(" ", "");
                    parameterFromDb.ActualValue = editParameterVM.ParameterValue;
                    tlbUnitOfWork.GetRepository<TLBParameters>().Update(parameterFromDb);
                    tlbUnitOfWork.SaveChanges();
                }
            }
        }

        internal void DeleteParameter(int parameterId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var parameterFromDb = tlbUnitOfWork.GetRepository<TLBParameters>().Items.FirstOrDefault(x => x.Id == parameterId);
                if (parameterFromDb.IsNotNull())
                {
                    tlbUnitOfWork.GetRepository<TLBParameters>().Delete(parameterFromDb);
                    tlbUnitOfWork.SaveChanges();
                }
            }
        }

        internal ObservableCollection<HistoryModel> GetCaseHistory(string EntityName, int cfID, string userName = null)
        {
            var collection = new ObservableCollection<HistoryModel>();
            using (var auditUOW = new AuditUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var logDetailsRepo = auditUOW.GetRepository<LogDetail>();
                var logDetails = new List<LogDetail>();
                if (string.IsNullOrEmpty(userName) || userName.Equals(TLBConstants.DefaultDDLValue))
                {
                    logDetails = logDetailsRepo.Items.Include(x => x.Logs).Where(x => x.EntityName.ToLower().Equals(EntityName.ToLower()) && x.ModuleId == appcontext.Module.Id).ToList();
                }
                else logDetails = logDetailsRepo.Items.Include(x => x.Logs).Where(x => x.EntityName.ToLower().Equals(EntityName.ToLower()) && x.CreatedBy.ToLower().Equals(userName.ToLower()) && x.ModuleId == appcontext.Module.Id).ToList();

                if (cfID != 0)
                {
                    logDetails = logDetails.Where(x => x.EntityUId == cfID).ToList();
                }

                var cfIDs = logDetails.Select(x => x.EntityUId).Distinct().ToList();
                foreach (var item in logDetails)
                {
                    var list = GetDataFromXlm(item);
                    if (list.IsNotNull())
                        collection.AddRange(list);
                }
            }
            return collection;
        }

        private ObservableCollection<HistoryModel> GetDataFromXlm(LogDetail item)
        {
            var data = new ObservableCollection<HistoryModel>();

            if (item.Logs.IsNull() || item.Logs.Count == 0) return null;
            XmlDocument xDoc = new XmlDocument();

            xDoc.InnerXml = item.Logs.FirstOrDefault().AuditXml;
            var node = xDoc.ChildNodes;
            if (node.Item(0).ChildNodes.Count == 0) return null;

            var nodes = node.Item(0).ChildNodes;
            if (nodes.Count == 0) return null;

            foreach (var itemNode in nodes)
            {
                var caseHistory = new HistoryModel();
                caseHistory.Entity = item.EntityName;
                caseHistory.Action = item.Action;
                caseHistory.CaseNumber = GetCaseNumberByCFID(item.EntityUId);
                caseHistory.TimeStamp = item.DateTime;
                caseHistory.User = item.CreatedBy;
                caseHistory.FieldName = ((System.Xml.XmlElement)(itemNode)).Name;
                var valuesArray = ((System.Xml.XmlElement)(itemNode)).InnerText.Split("#$#".ToCharArray());
                if (item.Action.EqualsIgnoreCase("Added"))
                {
                    caseHistory.ModifiedValue = TLBConstants.NotApplicableText;
                    caseHistory.PreviousValue = valuesArray[0];
                }

                else
                {
                    caseHistory.ModifiedValue = valuesArray[valuesArray.Length - 1];
                    caseHistory.PreviousValue = valuesArray[0];
                }
                data.Add(caseHistory);
            }
            return data;
        }

        private string GetCaseNumberByCFID(int cfId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var caseFileRepo = tlbUnitOfWork.GetRepository<TLBCaseFile>();
                var caseFile = caseFileRepo.Items.FirstOrDefault(x => x.CfId == cfId);

                if (caseFile.IsNotNull())
                    return caseFile.CaseNo;
                else return null;
            }
        }


        internal List<ExportParameterHitsViewModel> GetParametersByParamGrpId(int paramGroupId)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                List<ExportParameterHitsViewModel> exportParamHistVM = new List<ExportParameterHitsViewModel>();
                var tlbParamters = tlbUnitOfWork.GetRepository<TLBParameters>().Items
                    .Include(x => x.ScoresTransactions).Include(x => x.TLBParameterGroups)
                    .Where(x => x.ParameterGroupID == paramGroupId).ToList();
                if (tlbParamters.IsCollectionValid())
                {
                    tlbParamters.ForEach(x =>
                    {
                        x.ScoresTransactions.Each(y =>
                        {
                            ExportParameterHitsViewModel paramHitsVM = new ExportParameterHitsViewModel();
                            paramHitsVM.ActualValue = x.ActualValue;
                            paramHitsVM.ParamValue = x.Value;
                            paramHitsVM.Field = y.FIELD;
                            paramHitsVM.TransactionId = y.TransactionId.Value;
                            exportParamHistVM.Add(paramHitsVM);
                        });

                    });
                }
                return exportParamHistVM;
            }
        }

        internal void DeleteLookUp(LookUp item)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var repo = tlbUnitOfWork.GetRepository<LookUp>();
                var itemToDelete = repo.Items.FirstOrDefault(x => x.Id == item.Id && x.LookGroupId == item.LookGroupId);
                repo.Delete(itemToDelete);
                tlbUnitOfWork.SaveChanges();

            }
        }

        internal void AddLookUp(LookUp lookUpItem)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var repo = tlbUnitOfWork.GetRepository<LookUp>();
                repo.Insert(lookUpItem);
                tlbUnitOfWork.SaveChanges();
            }
        }

        internal void EditLookUp(LookUp lookUpItem)
        {
            using (var tlbUnitOfWork = new TLBUnitOfWork())
            {
                var repo = tlbUnitOfWork.GetRepository<LookUp>();
                var itemToUpdate = repo.Items.FirstOrDefault(x => x.Id == lookUpItem.Id && x.LookGroupId == lookUpItem.LookGroupId);
                itemToUpdate.Value = lookUpItem.Value;
                itemToUpdate.Reference = lookUpItem.Reference;
                tlbUnitOfWork.SaveChanges();
            }
        }

        internal ObservableCollection<Models.HistoryModel> GetApplicationHistory()
        {
            var collection = new ObservableCollection<HistoryModel>();
            using (var auditUOW = new AuditUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var logDetailsRepo = auditUOW.GetRepository<LogDetail>();
                var logDetails = logDetailsRepo.Items.Include(x => x.Logs).Where(x => (!x.EntityName.ToLower().Equals("TLBCaseFile")) && x.ModuleId == appcontext.Module.Id).ToList();

                foreach (var item in logDetails)
                {
                    var list = GetDataFromXlm(item);
                    if (list.IsNotNull())
                        collection.AddRange(list);
                }
            }
            return collection;
        }

        internal ObservableCollection<Models.HistoryModel> GetLoginHistory()
        {
            var collection = new ObservableCollection<HistoryModel>();
            using (var uMUOW = new UMUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var loginDetailsRepo = uMUOW.GetRepository<UserLoginHistory>();
                var loginDetails = loginDetailsRepo.Items.Where(x => x.ModuleTypeId == appcontext.Module.Id).ToList();
                foreach (var item in loginDetails)
                {
                    HistoryModel model = new Models.HistoryModel
                    {
                        User = item.UserName,
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        LoginTime = item.LoginTime
                    };
                    collection.Add(model);
                }
            }
            return collection;
        }

        internal ObservableCollection<CaseSearchData> SearchDispositionsExact(SearchDispositionsViewModel viewModel)
        {
            var casesCollection = new ObservableCollection<CaseSearchData>();
            using (var tLBUnitOfWork = new TLBUnitOfWork())
            {
                var cases = tLBUnitOfWork.GetRepository<TLBCaseFile>().Items.ToList();
                foreach (var caseData in cases)
                {
                    List<string> wordsMatchedList = new List<string>();
                    StringBuilder contentForSearch = new StringBuilder();
                    if (viewModel.SearchDispositionsModel.IsESChecked)
                    {
                        contentForSearch.AppendLine(caseData.ExecutiveSummary);
                    }
                    if (viewModel.SearchDispositionsModel.IsDueDillligenceChecked)
                    {
                        contentForSearch.AppendLine(" " + caseData.DueDiligence);
                    }
                    if (viewModel.SearchDispositionsModel.IsMOFChecked)
                    {
                        contentForSearch.AppendLine(" " + caseData.MemorandumOfFact);
                    }
                    if (viewModel.SearchDispositionsModel.IsRiskProfileChecked)
                    {
                        contentForSearch.AppendLine(" " + caseData.RiskProfile);
                    }
                    #region Sounds Alike Search
                    if (viewModel.SearchDispositionsModel.IsSoundAlikeSearch)
                    {
                        //Sounds Alike Search
                        SoundExAlgoHelper helper = new SoundExAlgoHelper();
                        foreach (var item in viewModel.SearchDispositionsModel.SearchParameters.Trim().Split(',').ToList())
                        {
                            var words = contentForSearch.ToString().GetWordsFromString();
                            if (words.IsCollectionValid())
                            {
                                foreach (var word in words)
                                {
                                    if (helper.Compare(word, item))
                                    {
                                        wordsMatchedList.Add(word);
                                    }
                                }

                            }
                            if (wordsMatchedList.Count > 0)
                            {
                                if (casesCollection.Any(x => x.CaseNumber == caseData.CaseNo)) continue;
                                casesCollection.Add(
                                                    new CaseSearchData
                                                    {
                                                        CaseName = caseData.Name,
                                                        CaseNumber = caseData.CaseNo,
                                                        DueDiligence = caseData.DueDiligence,
                                                        ExecutiveSummary = caseData.ExecutiveSummary,
                                                        MemofFact = caseData.MemorandumOfFact,
                                                        RiskProfile = caseData.RiskProfile,
                                                        MatchedWords=wordsMatchedList,
                                                        Status = WorkflowAPI.GetCaseDetails(caseData.CaseNo).Status
                                                    });
                            }
                        }
                    }
                    #endregion
                    #region Exact Search
                    else if (viewModel.SearchDispositionsModel.IsExactSearch)
                    {
                        //Logic for exact search

                        foreach (var item in viewModel.SearchDispositionsModel.SearchParameters.Trim().Split(',').ToList())
                        {
                            if (SearchForExactTerm(contentForSearch.ToString(), item))
                            {
                                //Search Found

                                if (casesCollection.Any(x => x.CaseNumber == caseData.CaseNo)) continue;
                                casesCollection.Add(
                                                    new CaseSearchData
                                                    {
                                                        CaseName = caseData.Name,
                                                        CaseNumber = caseData.CaseNo,
                                                        DueDiligence = caseData.DueDiligence,
                                                        ExecutiveSummary = caseData.ExecutiveSummary,
                                                        MemofFact = caseData.MemorandumOfFact,
                                                        RiskProfile = caseData.RiskProfile,
                                                        Status = WorkflowAPI.GetCaseDetails(caseData.CaseNo).Status
                                                    });
                            }
                        }

                    }
                    #endregion
                    #region Fuzzy Search
                    else
                    {
                        //Logic for fuzzy search
                        foreach (var item in viewModel.SearchDispositionsModel.SearchParameters.Trim().Split(',').ToList())
                        {
                            if (contentForSearch.ToString().ToLower().Contains(item.ToLower()))
                            {
                                //Search Found
                                if (casesCollection.Any(x => x.CaseNumber == caseData.CaseNo)) continue;
                                casesCollection.Add(
                                                    new CaseSearchData
                                                    {
                                                        CaseName = caseData.Name,
                                                        CaseNumber = caseData.CaseNo,
                                                        DueDiligence = caseData.DueDiligence,
                                                        ExecutiveSummary = caseData.ExecutiveSummary,
                                                        MemofFact = caseData.MemorandumOfFact,
                                                        RiskProfile = caseData.RiskProfile,
                                                        Status = WorkflowAPI.GetCaseDetails(caseData.CaseNo).Status
                                                    });
                            }
                        }

                    }
                    #endregion
                }
            }
            return casesCollection;
        }

        private bool SearchForExactTerm(string contentForSearch, string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm)) return false;
            contentForSearch = contentForSearch.Replace("\r\n", " ");
            var indexofTerm = contentForSearch.IndexOf(searchTerm, StringComparison.InvariantCultureIgnoreCase);
            bool result = true;

            if (indexofTerm == -1)
            {
                return false;
            }

            //Check alphabet before term
            if (indexofTerm != 0)
            {
                var AsciiValueofelementbefore = Convert.ToInt32(contentForSearch.ElementAt(indexofTerm - 1));

                if ((AsciiValueofelementbefore >= 65 && AsciiValueofelementbefore <= 90) || (AsciiValueofelementbefore >= 97 && AsciiValueofelementbefore <= 122))
                {
                    result = false;
                }
            }

            //Check alphabet after term
            if (indexofTerm + searchTerm.Length < contentForSearch.Length)
            {
                var AsciiValueofelementbefore = Convert.ToInt32(contentForSearch.ElementAt(indexofTerm + searchTerm.Length));

                if ((AsciiValueofelementbefore >= 65 && AsciiValueofelementbefore <= 90) || (AsciiValueofelementbefore >= 97 && AsciiValueofelementbefore <= 122))
                {
                    result = false;
                }
            }

            if (!result && (indexofTerm + searchTerm.Length) < contentForSearch.Length)
            {
                result = SearchForExactTerm(contentForSearch.Substring(indexofTerm + searchTerm.Length), searchTerm);
            }
            return result;
        }

        internal List<User> GetUserByState(int stateId)
        {
            using (var wfUOW = new TLBUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var userRepo = wfUOW.GetRepository<User>();
                var users = userRepo.Items.Include(user => user.UserInGroup.Select(x => x.Group)).Where(x => x.IsActive && x.UserInGroup.Any(gm => gm.IsActive && gm.ModuleTypeId == appcontext.Module.Id && gm.Group.States.Any(s => s.ModuleTypeId == appcontext.Module.Id && s.Id == stateId))).ToList();
                return users;
            }
        }
    }
}
