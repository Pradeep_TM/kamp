﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KAMP.Core.FrameworkComponents
{
    public class MyStyleRefExtension : StyleRefExtension
    {
        static MyStyleRefExtension()
        {
            RD = new ResourceDictionary()
            {
                Source = new Uri("pack://application:,,,/KAMP.Core.FrameworkComponents;component/General.xaml")
            };
        }
    }
}
