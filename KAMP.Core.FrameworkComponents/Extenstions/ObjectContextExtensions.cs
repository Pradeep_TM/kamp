﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents.Extenstions
{
    public static class ObjectContextExtensions
    {
        public static List<string> GetEntityNames(this DbContext context, string entityFilter)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;
            var entitySets = objectContext.GetEntitySets();
            var collectionSet = new List<string>();

            foreach (EntitySet item in entitySets.Where(x => x.BuiltInTypeKind == BuiltInTypeKind.EntitySet))
            {
                collectionSet.Add(GetTableName(item, objectContext.MetadataWorkspace));
            }
            return entityFilter == "" ? collectionSet : collectionSet.Where(x => x.StartsWith(entityFilter, true, null)).ToList();
        }

        public static List<string> GetAttributesByFilter(this DbContext context, string filter)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;
            var entitySets = objectContext.GetEntitySets();
            var collectionSet = new List<string>();
            foreach (EntitySet item in entitySets.Where(x => x.BuiltInTypeKind == BuiltInTypeKind.EntitySet))
            {
                var columnSet = GetMappedColumnNames(item, objectContext.MetadataWorkspace);
                var filteredColumnSet = columnSet.Where(x => x.StartsWith(filter, true, null));
                if (filteredColumnSet.FirstOrDefault() != null)
                    collectionSet.AddRange(filteredColumnSet.ToList());
            }
            return collectionSet;
        }

        public static List<string> GetMappedColumnNames(EntitySet type, MetadataWorkspace metadata)
        {

            if (type.BuiltInTypeKind == BuiltInTypeKind.EntitySet)
            {
                var mapping = metadata.GetItems<EntityContainerMapping>(DataSpace.CSSpace).Single().EntitySetMappings.Single(s => s.EntitySet == type);
                var table = mapping.EntityTypeMappings.Single().Fragments.Single().StoreEntitySet;
                return table.ElementType.Properties.Select(x => x.Name).ToList<string>();
            }
            return null;
        }

        public static string GetTableName(EntitySet type, MetadataWorkspace metadata)
        {
            if (type.BuiltInTypeKind == BuiltInTypeKind.EntitySet)
            {
                var mapping = metadata.GetItems<EntityContainerMapping>(DataSpace.CSSpace).Single().EntitySetMappings.Single(s => s.EntitySet == type);
                var table = mapping.EntityTypeMappings.Single().Fragments.Single().StoreEntitySet;
                return (string)table.MetadataProperties["Table"].Value ?? table.Name;
            }
            return string.Empty;
        }


        public static IEnumerable<EntitySetBase> GetEntitySets(this ObjectContext theContext)
        {
            var container = theContext.MetadataWorkspace.GetEntityContainer(theContext.DefaultContainerName, DataSpace.CSpace);

            return container.BaseEntitySets;
        }

        public static List<string> GetAttributeNames(this DbContext context, string entity)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;

            return objectContext.GetAttributeSets(entity);
        }

        public static List<string> GetAttributeSets(this ObjectContext theContext, string entity)
        {
            var container = theContext.MetadataWorkspace.GetEntityContainer(theContext.DefaultContainerName, DataSpace.CSpace);
            entity = entity.Contains("_") == true ? entity.Split('_')[1] : entity;
            var findEntity = container.BaseEntitySets.FirstOrDefault(x => x.Name == entity);
            return findEntity == null ? null : GetMappedColumnNames(container.BaseEntitySets[entity] as EntitySet, theContext.MetadataWorkspace);
        }

        public static List<string> GetAlias(this DbContext context, string entity)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;

            return objectContext.GetAlias(entity);
        }

        public static List<string> GetAlias(this ObjectContext theContext, string entity)
        {
            //var container = theContext.MetadataWorkspace.GetEntityContainer(theContext.DefaultContainerName, DataSpace.CSpace);

            //var findEntity = container.BaseEntitySets.FirstOrDefault(x => x.Name == entity);
            //if (findEntity == null)
            //    return null;

            //var set = container.BaseEntitySets[entity];
            //return set.ElementType.Members.Select(x => x.Name).ToList();

            var container = theContext.MetadataWorkspace.GetEntityContainer(theContext.DefaultContainerName, DataSpace.CSpace);
            entity = entity.Contains("_") == true ? entity.Split('_')[1] : entity;
            var findEntity = container.BaseEntitySets.FirstOrDefault(x => x.Name == entity);
            return findEntity == null ? null : GetMappedColumnNames(container.BaseEntitySets[entity] as EntitySet, theContext.MetadataWorkspace);
        }

        public static string GetTableName<T>(this ObjectContext context) where T : class
        {
            string sql = context.CreateObjectSet<T>().ToTraceString();
            Regex regex = new Regex("FROM (?<table>.*) AS");
            Match match = regex.Match(sql);

            string table = match.Groups["table"].Value;
            return table;
        }

        public static string GetEntitySetName<T>(this ObjectContext theContext, T eo) where T : EntityObject
        {
            string entitySetName = "";
            if (eo.EntityKey != null)
            {
                entitySetName = eo.EntityKey.EntitySetName;
            }
            else
            {
                string className = typeof(T).Name;
                var container =
                       theContext.MetadataWorkspace.GetEntityContainer(theContext.DefaultContainerName, DataSpace.CSpace);
                entitySetName = (from meta in container.BaseEntitySets
                                 where meta.ElementType.Name == className
                                 select meta.Name
                                ).First();

            }

            return entitySetName;
        }

        public static IEnumerable<ObjectQuery> GetObjectQueries(this ObjectContext theContext)
        {
            IEnumerable<ObjectQuery> queries =
                  from pd in theContext
                               .GetType()
                               .GetProperties()
                  where pd.PropertyType
                           .IsSubclassOf(typeof(ObjectQuery))
                  select (ObjectQuery)pd.GetValue(theContext, null);
            return queries;
        }
    }
}
