﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace KAMP.Core.FrameworkComponents
{
    public static class ObjectExtensions
    {
        public static bool IsNotNull<T>(this T obj) where T : class
        {
            return obj != null;
        }

        public static bool IsNull<T>(this T obj) where T : class
        {
            return obj == null;
        }

        public static TValue Sandbox<TParent, TValue>(this TParent obj, Func<TParent, TValue> propertyExpr) where TParent : class
        {
            if (obj == null)
            {
                return default(TValue);
            }
            return propertyExpr(obj);
        }


        public static string GetDisplayName<T>(this T value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo.IsNull())
                return null;
            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null) return string.Empty;
            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
        }

        public static List<Type> GetImplementors(this Type implementee)
        {
            var types = from a in AppDomain.CurrentDomain.GetAssemblies()
                        from t in a.GetTypes()
                        where t != implementee && implementee.IsAssignableFrom(t)
                        select t;

            return types.ToList();
        }


        public static T CopyObject<T>(this object objSource)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, objSource);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
    }
}
