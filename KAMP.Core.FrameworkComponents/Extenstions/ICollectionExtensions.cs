﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KAMP.Core.FrameworkComponents
{
    public static class ICollectionExtensions
    {
        public static bool IsCollectionValid<T>(this ICollection<T> collection)
        {
            return collection != null && collection.Count > 0;
        }

        public static bool IsCollectionValid<T>(this IEnumerable<T> collection)
        {
            return collection != null && collection.Count() > 0;
        }

        public static int? IndexOf<T>(this Dictionary<T, T> source, T key)
        {

            for (int i = 0; i < source.Keys.Count; i++)
            {
                var item = source.ElementAt(i);
                if (key.Equals(item.Key))
                    return i;
            }
            return null;
        }
    }
}
