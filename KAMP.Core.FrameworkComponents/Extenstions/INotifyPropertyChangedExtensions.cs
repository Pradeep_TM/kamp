﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.ComponentModel;

namespace KAMP.Core.FrameworkComponents
{
    public static class INotifyPropertyChangedExtensions
    {
        public static bool HandleValueChange<O, T>(this PropertyChangedEventHandler handler, O obj, T newValue, ref T oldValue, Expression<Func<O, object>> propertyExpr) where O : INotifyPropertyChanged
        {
            if (oldValue == null || newValue == null || !oldValue.Equals(newValue))
            {
                oldValue = newValue;
                if (handler.IsNotNull())
                {
                    handler(obj, new PropertyChangedEventArgs(obj.GetPropertyName(propertyExpr)));
                }

                return true;
            }
            return false;
        }

        public static void Raise<T>(this PropertyChangedEventHandler handler, T obj, Expression<Func<T, object>> propertyExpr) where T : INotifyPropertyChanged
        {
            if (handler.IsNotNull())
            {
                handler(obj, new PropertyChangedEventArgs(obj.GetPropertyName(propertyExpr)));
            }
        }

        //public static void Raise<T>(this PropertyChangedEventHandler handler, T obj, params Expression<Func<T>>[] propertyExpressions) where T : INotifyPropertyChanged
        //{
        //    foreach (var propertyExpression in propertyExpressions)
        //    {
        //        if (handler.IsNotNull())
        //            handler.Raise<T>(obj, propertyExpression);
        //    }
        //}

    }
}
