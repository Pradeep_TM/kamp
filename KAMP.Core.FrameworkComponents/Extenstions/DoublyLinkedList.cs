﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents
{

    public interface IDoublyLinkedListNode<T>
    {
        T NextNode { get; set; }
        T PrevNode { get; set; }
    }


    public class DoublyLinkedList<T> : ObservableCollection<T> where T : IDoublyLinkedListNode<T>
    {

        public DoublyLinkedList()
        {
        }
        public T FirstItem { get; set; }

        public T LastItem { get; set; }

        public void AddItem(T item)
        {
            if (this.Count == 0)
            {

                Add(item);
            }
            else
            {
                var prevItem = this[this.Count - 1];
                item.PrevNode = prevItem;
                prevItem.NextNode = item;
                this.Add(item);
            }
        }

        public void DeleteItem(T item)
        {
            var index = this.IndexOf(item);

            if (index > 0)
            {
                var prevItem = this[index - 1];
                prevItem.NextNode = item.NextNode;

                if (index != this.Count - 1)
                {
                    var nextItem = this[index + 1];
                    nextItem.PrevNode = prevItem;
                }
            }
            else if (index == 0 && this.Count>1)
            {

                var nextItem = this[index + 1];
                if (nextItem != null)
                    nextItem.PrevNode = default(T);
            }
            this.Remove(item);
        }
    }
}
