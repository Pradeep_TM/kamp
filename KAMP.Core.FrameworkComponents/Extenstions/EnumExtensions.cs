﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KAMP.Core.FrameworkComponents
{
    public static class EnumExtensions
    {
        public static List<T> ToList<T>(Type enumType, Action<T, short> codeSetter, Action<T, string> valueSetter) where T : new()
        {
            var res = Enum.GetValues(enumType)
            .Cast<Enum>()
            .Select(x =>
            {
                T obj = new T();
                codeSetter(obj, Convert.ToInt16(x));
                valueSetter(obj, x.GetDisplayName());
                return obj;
            }).ToList();

            return res;

        }
    }
}
