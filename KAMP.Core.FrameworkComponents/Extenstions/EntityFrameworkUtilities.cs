﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents
{
    public class EntityFrameworkUtilities
    {
        /// <summary>
        /// Verifies whether the record is newly inserted or deleted or updated
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <param name="keySelector"></param>
        /// <param name="onInsert"></param>
        /// <param name="onUpdate">OnUpdate(Source, Destination)</param>
        /// <param name="onDelete"></param>
        public static void SyncLists<T>(IEnumerable<T> source, IEnumerable<T> dest, Func<T, long> keySelector,
                                  Action<T> onInsert, Action<T, T> onUpdate, Action<T> onDelete)
        {
            if (onDelete != null && (source == null || !source.Any()) && (dest != null && dest.Any()))
            {
                var destList = dest.ToList();
                foreach (var destItem in destList)
                    onDelete(destItem);
            }                
            else if (source != null && source.Any())
            {
                if (dest.IsCollectionValid() && source.IsCollectionValid() && onInsert != null)
                {
                    var lookupFordest = dest.Where(x => keySelector(x) > 0).ToDictionary(keySelector);
                    var itemsToInsert =
                        source.Where(srcItem => !lookupFordest.ContainsKey(keySelector(srcItem))).ToArray();
                    foreach (var itemToInsert in itemsToInsert)
                        onInsert(itemToInsert);
                }
                else if (source != null && onInsert != null)
                    foreach (var item in source)
                        onInsert(item);


                var lookupForSource = source.Where(x => keySelector(x) > 0)
                    .ToDictionary(keySelector);

                if (dest.IsCollectionValid())
                {
                    if (onUpdate != null)
                    {
                        foreach (var destItem in dest)
                        {
                            if (lookupForSource.ContainsKey(keySelector(destItem)))
                                onUpdate(lookupForSource[keySelector(destItem)], destItem);
                        }
                    }

                    if (onDelete != null)
                    {
                        var itemsToDelete = dest.Where(destItem => !lookupForSource.ContainsKey(keySelector(destItem))).ToList();

                        if (itemsToDelete.IsCollectionValid())
                            foreach (var item in itemsToDelete)
                            {
                                if(keySelector(item) != 0)
                                    onDelete(item);
                            }
                                
                    }
                }
            }

        }
    }
}
