﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Data;

namespace KAMP.Core.FrameworkComponents
{
    public static class IQueryableExtensions
    {
        //public static IQueryable<T> Include<T, P>(this IQueryable<T> items, Expression<Func<T, P>> selector)
        //{
        //    return QueryableExtensions.Include<T, P>(items, selector); 
        //}

        public static IIncluder Includer = new EfIncluder();

        public static IQueryable<T> Include<T, TProperty>(this IQueryable<T> source, Expression<Func<T, TProperty>> path) where T : class
        {
            return Includer.Include(source, path);
        }

        public interface IIncluder
        {
            IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path) where T : class;
        }

        internal class NullIncluder : IIncluder
        {
            public IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path) where T : class
            {
                return source;
            }
        }

        public class EfIncluder : IIncluder
        {
            #region Implementation of IIncluder

            public IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path) where T : class
            {
                    return QueryableExtensions.Include(source, path);
            }

            #endregion
        }
    }
}
