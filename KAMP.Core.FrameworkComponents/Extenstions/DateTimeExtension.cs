﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents
{
    public static class DateTimeExtension
    {
        public static int GetDifferenceInDaysX(this DateTime endDate, DateTime startDate)
        {
            TimeSpan ts = endDate - startDate;
            return (int)ts.TotalDays;
        }
    }
}
