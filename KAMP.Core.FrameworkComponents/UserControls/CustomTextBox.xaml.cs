﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;

namespace KAMP.Core.FrameworkComponents
{
    /// <summary>
    /// Interaction logic for CustomTextBox.xaml
    /// </summary>
    public partial class CustomTextBox : RichTextBox
    {
        public CustomTextBox()
        {
            InitializeComponent();
        }

        public const string RichTextPropertyName = "RichText";

        public static readonly DependencyProperty RichTextProperty =
           DependencyProperty.Register(RichTextPropertyName,
                                       typeof(string),
                                       typeof(CustomTextBox),
                                       new PropertyMetadata(null));
        public string RichText
        {
            get { return (string)GetValue(RichTextProperty); }
            set { SetValue(RichTextProperty, value); }
        }

    }
}
