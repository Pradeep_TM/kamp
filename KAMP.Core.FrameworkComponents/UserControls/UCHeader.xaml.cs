﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.FrameworkComponents.UserControls
{
    /// <summary>
    /// Interaction logic for UCHeader.xaml
    /// </summary>
    public partial class UCHeader : UserControl
    {
        public Action CloseApp;
        public Action ConfigureAudit;
        public UCHeader()
        {
            InitializeComponent();
            Loaded += UCHeader_Loaded;
        }

        void UCHeader_Loaded(object sender, RoutedEventArgs e)
        {
            var context = Application.Current.Properties["Context"] as Context;
            LblUserName.Content = string.Format("{0}"+" "+ "{1}",context.User.FirstName,context.User.LastName);
        }

        private void MinimizeApplication(object sender, MouseButtonEventArgs e)
        {
          var currentWindow=  Window.GetWindow(this);
          currentWindow.WindowState = WindowState.Minimized;
        }

        private void CloseApplication(object sender, MouseButtonEventArgs e)
        {
            if (CloseApp.IsNotNull())
                CloseApp();
        }

        //private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    var context = Application.Current.Properties["Context"] as Context;

        //    if (ConfigureAudit.IsNotNull())
        //    {
        //        ConfigureAudit();
        //    }
        //}
    }
}
