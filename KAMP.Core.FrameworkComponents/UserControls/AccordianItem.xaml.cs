﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.FrameworkComponents.UserControls
{
    /// <summary>
    /// Interaction logic for AccordianItem.xaml
    /// </summary>
    public partial class AccordianItem : UserControl
    {
        private double _height;
        public AccordianItem()
        {
            InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// Height for tab
        /// </summary>
        public string HeightValue
        {
            get { return (string)GetValue(HeightValueProperty); }
            set { SetValue(HeightValueProperty, value); }
        }
        public static readonly DependencyProperty HeightValueProperty =
            DependencyProperty.Register("HeightValue", typeof(string), typeof(AccordianItem),
              new PropertyMetadata(null));

        /// <summary>
        /// Corner Radius for tab
        /// </summary>
        public string CornerRadius
        {
            get { return (string)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(string), typeof(AccordianItem),
              new PropertyMetadata(null));


        /// <summary>
        /// BackGround for tab
        /// </summary>
        public string BackgroundColor
        {
            get { return (string)GetValue(BackgroundColorProperty); }
            set { SetValue(BackgroundColorProperty, value); }
        }
        public static readonly DependencyProperty BackgroundColorProperty =
            DependencyProperty.Register("BackgroundColor", typeof(string), typeof(AccordianItem),
              new PropertyMetadata(null));


        /// <summary>
        /// Foreground for tab
        /// </summary>
        public string ForegroundColor
        {
            get { return (string)GetValue(ForegroundColorProperty); }
            set { SetValue(ForegroundColorProperty, value); }
        }
        public static readonly DependencyProperty ForegroundColorProperty =
            DependencyProperty.Register("ForegroundColor", typeof(string), typeof(AccordianItem),
              new PropertyMetadata(null));


        /// <summary>
        /// Title for tab
        /// </summary>
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(AccordianItem),
              new PropertyMetadata(null));


        /// <summary>
        /// Gets or sets additional content for the UserControl
        /// </summary>
        public object AdditionalContent
        {
            get { return (object)GetValue(AdditionalContentProperty); }
            set { SetValue(AdditionalContentProperty, value); }
        }
        public static readonly DependencyProperty AdditionalContentProperty =
            DependencyProperty.Register("AdditionalContent", typeof(object), typeof(AccordianItem),
              new PropertyMetadata(null));

        private void Border_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_height <= 0)
                _height = MainContent.ActualHeight;

            if (MainContent.ActualHeight == _height)
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 0;
                animation.From = _height;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                MainContent.BeginAnimation(ContentPresenter.HeightProperty, animation);
            }

            if (MainContent.ActualHeight == 0)
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = _height;
                animation.From = 0;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                MainContent.BeginAnimation(ContentPresenter.HeightProperty, animation);
            }
        }

    }
}
