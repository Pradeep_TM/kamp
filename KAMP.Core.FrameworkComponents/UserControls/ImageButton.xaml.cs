﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.FrameworkComponents.UserControls
{
    /// <summary>
    /// Interaction logic for ImageButton.xaml
    /// </summary>
    public partial class ImageButton : Button
    {
        public ImageButton()
        {
            InitializeComponent();
        }

        public string DefaultImageSource
        {
            get { return (string)GetValue(DefaultImageSourceProperty); }
            set { SetValue(DefaultImageSourceProperty, value); }
        }

        public static readonly DependencyProperty DefaultImageSourceProperty =
            DependencyProperty.Register("DefaultImageSource"
                                , typeof(string)
                                , typeof(ImageButton)
                                , new UIPropertyMetadata(null));

        public string MouseHoverImageSource
        {
            get { return (string)GetValue(MouseHoverImageSourceProperty); }
            set { SetValue(MouseHoverImageSourceProperty, value); }
        }

        public static readonly DependencyProperty MouseHoverImageSourceProperty =
            DependencyProperty.Register("MouseHoverImageSource"
                                , typeof(string)
                                , typeof(ImageButton)
                                , new UIPropertyMetadata(null));

        public string MouseButtonDownImageSource
        {
            get { return (string)GetValue(MouseButtonDownImageSourceProperty); }
            set { SetValue(MouseButtonDownImageSourceProperty, value); }
        }

        public static readonly DependencyProperty MouseButtonDownImageSourceProperty =
            DependencyProperty.Register("MouseButtonDownImageSource"
                                , typeof(string)
                                , typeof(ImageButton)
                                , new UIPropertyMetadata(null));
    }
}
