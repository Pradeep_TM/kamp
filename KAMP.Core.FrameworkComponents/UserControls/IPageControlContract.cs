﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace KAMP.Core.FrameworkComponents.UserControls
{
    public interface IPageControlContract
    {
       // uint GetTotalCount();
        ObservableCollection<object> GetRecordsBy(uint StartingIndex, uint NumberOfRecords, object FilterTag);
        uint GetTotalCount(object FilterTagProperty);
    }
}
