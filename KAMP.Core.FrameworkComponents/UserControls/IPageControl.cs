﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents
{
    public interface IMVPageControl
    {
        void InitializeViewModel();
    }
}
