﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using KAMP.Core.FrameworkComponents;
using System.Threading.Tasks;
namespace KAMP.Core.FrameworkComponents.ViewModels
{
    public class HeaderViewModel : INotifyPropertyChanged
    {
        private string _imgModule;
        private string _lightThemeColor;
        private string _darkThemeColor;
        private string _moduleName;
        private string _currentContent;
        private string _contentBackgroundColor;
        
        public HeaderViewModel()
        {
            ModuleName = "User Management";
            ImgModule = "../Assets/Images/tlbicon.png";
            _darkThemeColor = "#3468B1";
            _lightThemeColor = "#3D75BF";
            _contentBackgroundColor = "#E8EAF6";
        }

        public string ContentBackgroundColor
        {
            get { return _contentBackgroundColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _contentBackgroundColor, x => x.ContentBackgroundColor);
            }
        }

        public string DarkThemeColor
        {
            get { return _darkThemeColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _darkThemeColor, x => x.DarkThemeColor);
            }
        }

        public string LightThemeColor
        {
            get { return _lightThemeColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lightThemeColor, x => x.LightThemeColor);
            }
        }

        public string CurrentContent
        {
            get { return _currentContent; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _currentContent, x => x.CurrentContent);
            }
        }

        public string ModuleName
        {
            get { return _moduleName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleName, x => x.ModuleName);
            }
        }

        public string ImgModule
        {
            get { return _imgModule; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _imgModule, x => x.ImgModule);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
