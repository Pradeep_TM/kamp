﻿using KAMP.Core.FrameworkComponents.Enums;
using KAMP.Core.Interface;
using KAMP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace KAMP.Core.FrameworkComponents.ViewModels
{
    public class NavigationControlViewModel : INotifyPropertyChanged
    {

        public NavigationControlViewModel()
        {
            SetVisiBilityForModules();
            ItemsCollection = new ObservableCollection<NavigationControlModel>();
        }

        private void SetVisiBilityForModules()
        {
            foreach (IModule module in ModuleList.ModuleContainer)
            {
                ModuleNames enums;
                bool isParsed = Enum.TryParse(module.Metadata.ModuleName, true, out enums);
                if (!isParsed) continue;
                switch (enums)
                {
                    case ModuleNames.UM:
                        UMVisibility = true;
                        break;
                    case ModuleNames.KYC:
                        KYCVisibility = true;
                        break;
                    case ModuleNames.MV:
                        MVVisibility = true;
                        break;
                    case ModuleNames.TLB:
                        TLBVisibility = true;
                        break;
                }
            }
        }

        #region ModuleVisibility
        private bool _tLBVisibility;
        public bool TLBVisibility
        {
            get { return _tLBVisibility; }
            set { _tLBVisibility = value; }
        }
        private bool _mVVisibility;
        public bool MVVisibility
        {
            get { return _mVVisibility; }
            set { _mVVisibility = value; }
        }
        private bool _kYCVisibility;
        public bool KYCVisibility
        {
            get { return _kYCVisibility; }
            set { _kYCVisibility = value; }
        }
        private bool _uMVisibility;
        public bool UMVisibility
        {
            get { return _uMVisibility; }
            set { _uMVisibility = value; }
        }
        #endregion

        private string _pinImage;
        public string PinImage
        {
            get { return _pinImage; }
            set { _pinImage = value; }
        }

        private string _unPinImage;
        public string UnPinImage
        {
            get { return _unPinImage; }
            set { _unPinImage = value; }
        }

        private string _arrowLeftNavigation;
        public string ArrowLeftNavigation
        {
            get { return _arrowLeftNavigation; }
            set { _arrowLeftNavigation = value; }
        }

        private string _darkThemeColor;
        public string DarkThemeColor
        {
            get { return _darkThemeColor; }
            set { _darkThemeColor = value; }
        }

        private string _lightThemeColor;
        public string LightThemeColor
        {
            get { return _lightThemeColor; }
            set { _lightThemeColor = value; }
        }

        private string _contentBackgroundColor;
        public string ContentBackgroundColor
        {
            get { return _contentBackgroundColor; }
            set { _contentBackgroundColor = value; }
        }

        private ObservableCollection<NavigationControlModel> itemsCollection;
        private double itemControlRowSize { get; set; }
        public Action<string> LoadUserControl;
        public ObservableCollection<NavigationControlModel> ItemsCollection
        {
            get { return itemsCollection; }
            set
            {
                itemsCollection = value;
                RaisePropertyChanged("ItemsCollection");
            }
        }


        private ICommand _openpage;

        public ICommand OpenPage
        {
            get
            {
                if (_openpage == null)
                    _openpage = new DelegateCommand<string>((t) => DisplayUserControl(t));
                return _openpage;
            }

        }

        private ICommand _collapseControl;

        public ICommand CollapseControl
        {
            get
            {
                if (_collapseControl == null)
                    _collapseControl = new DelegateCommand<ItemsControl>((t) => CollapseUncollaseItemControl(t));
                return _collapseControl;
            }

        }

        private ICommand _collapsePanel;

        public ICommand CollapsePanel
        {
            get
            {
                if (_collapsePanel == null)
                    _collapsePanel = new DelegateCommand<UserControl>((t) => CollapseUncollasePanel(t));
                return _collapsePanel;
            }

        }

        private void CollapseUncollasePanel(UserControl t)
        {

            if (t.Width != 20)
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 20;
                animation.From = 220;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.WidthProperty, animation);
            }

            else
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 220;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.WidthProperty, animation);
            }
        }


        private void CollapseUncollaseItemControl(ItemsControl t)
        {
            if (t.Height != 0)
            {
                itemControlRowSize = CalculateOneRowHeight(t);
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 0;
                animation.From = t.ActualHeight;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.HeightProperty, animation);
            }
            else
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = (double)Math.Ceiling((decimal)t.Items.Count / 2) * itemControlRowSize;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.HeightProperty, animation);
            }
        }

        private double CalculateOneRowHeight(ItemsControl t)
        {
            int rowCount = (int)Math.Ceiling((decimal)t.Items.Count / 2);
            return t.ActualHeight / rowCount;
        }


        private void DisplayUserControl(string uCName)
        {
            if (LoadUserControl != null)
                LoadUserControl(uCName);
        }



        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
