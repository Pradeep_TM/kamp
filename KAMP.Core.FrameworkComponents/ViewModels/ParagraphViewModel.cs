﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.Core.FrameworkComponents.ViewModels
{
    public class ParagraphViewModel : INotifyPropertyChanged
    {
        private string _heading;

        public string Heading
        {
            get { return _heading; }
            set { PropertyChanged.HandleValueChange(this, value, ref _heading, (x) => x.Heading); }
        }

        private string _content;

        public string Content
        {
            get { return _content; }
            set { PropertyChanged.HandleValueChange(this, value, ref _content, (x) => x.Content); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
