﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class LogEntityAttribute : Attribute
    {
        string entityName;
        string Description;
        public LogEntityAttribute(string entityName, string description)
        {
            this.entityName = entityName;
            this.Description = description;
        }

        public string GetName()
        {
            return entityName;
        }
        public string GetDescription()
        {
            return Description;
        }
    }
}
