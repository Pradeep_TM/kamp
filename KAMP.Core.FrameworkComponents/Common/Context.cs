﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents
{
    public class Context
    {
        public Context()
        {
            Module = new ModuleDetails();
            User = new UserDetails();
        }
        public ModuleDetails Module { get; set; }
        public UserDetails User { get; set; }
        public Func<string, ModuleDetails> GetModuleDetails;
        public string ConnectionString { get; set; }
    }
    public class ModuleDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class UserDetails
    {
        public long Id { get; set; }
        public string UserName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        //UserVisibility
    }


}
