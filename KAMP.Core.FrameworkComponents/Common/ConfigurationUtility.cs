﻿using KAMP.Core.FrameworkComponents;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameWorkComponents
{
    public static class ConfigurationUtility
    {
        public static string GetConnectionString()
        {
            //SqlConnection myConnection = new SqlConnection();
            SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
            var serverName = ConfigurationManager.AppSettings["ServerName"];
            var databaseName = ConfigurationManager.AppSettings["DatabaseName"];
            if (serverName.IsEmpty() || databaseName.IsEmpty())
                throw new Exception("Application settings is not valid.", new Exception("Server name or database name is not set."));
            myBuilder.DataSource = serverName;
            myBuilder.InitialCatalog = databaseName;
            myBuilder.IntegratedSecurity = true;
            return myBuilder.ConnectionString;
        }
    }
}
