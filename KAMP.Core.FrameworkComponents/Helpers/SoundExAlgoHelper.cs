﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents.Helpers
{
    public class SoundExAlgoHelper
    {
        public string GetSoundex(string value)
        {
            value = value.ToUpper();
            StringBuilder soundex = new StringBuilder();
            foreach (char ch in value)
            {
                if (char.IsLetter(ch))
                    AddCharacter(soundex, ch);
            }
            RemovePlaceholders(soundex);
            FixLength(soundex);
            return soundex.ToString();
        }

        private void AddCharacter(StringBuilder soundex, char ch)
        {
            if (soundex.Length == 0) soundex.Append(ch);
            else
            {
                string code = GetSoundexDigit(ch);
                if (code != soundex[soundex.Length - 1].ToString())
                    soundex.Append(code);
            }
        }

        private string GetSoundexDigit(char ch)
        {
            string chString = ch.ToString();
            if ("BFPV".Contains(chString)) return "1";
            else if ("CGJKQSXZ".Contains(chString)) return "2";
            else if ("DT".Contains(chString)) return "3";
            else if (ch == 'L') return "4";
            else if ("MN".Contains(chString)) return "5";
            else if (ch == 'R') return "6";
            else return ".";
        }

        private void RemovePlaceholders(StringBuilder soundex)
        {
            soundex.Replace(".", "");
        }

        private void FixLength(StringBuilder soundex)
        {
            int length = soundex.Length;
            if (length < 4) soundex.Append(new string('0', 4 - length));
            else soundex.Length = 4;
        }

        public bool Compare(string value1, string value2)
        {
            int matches = 0;
            string soundex1 = GetSoundex(value1);
            string soundex2 = GetSoundex(value2);
            for (int i = 0; i < 4; i++)
                if (soundex1[i].Equals(soundex2[i])) matches++;
            if (matches == 4)
                return true;
            return false;
        }
    }
}
