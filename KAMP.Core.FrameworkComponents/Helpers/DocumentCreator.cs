﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using KAMP.Core.FrameworkComponents.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace KAMP.Core.FrameworkComponents.Helpers
{
    public class DocumentCreator
    {
        public static void CreateNewWordDocument<T>(List<T> list)
        {
            try
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Title = "Please select a path to save the file.";
                dialog.Filter = "Docx files (*.docx)|*.doc|All files (*.*)|*.*";
                var res = dialog.ShowDialog();
                if (res == DialogResult.Cancel)
                    return;
                var path = dialog.FileName;
                using (WordprocessingDocument doc = WordprocessingDocument.Create(path, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = doc.AddMainDocumentPart();
                    mainPart.Document = new Document(new Body());

                    if (typeof(T) == typeof(ParagraphViewModel))
                    {
                        List<ParagraphViewModel> paragarphViewModelLst = list as List<ParagraphViewModel>;
                        mainPart.Document = new Document(new Body());
                        List<Paragraph> parLst = new List<Paragraph>();
                        foreach (var item in paragarphViewModelLst)
                        {
                            Paragraph par;

                            Run run = new Run();
                            Paragraph parHeading = (new Paragraph());


                            RunProperties runProperties = new RunProperties();
                            FontSize fontSize = new FontSize();
                            fontSize.Val = new StringValue("24");
                            runProperties.AppendChild<FontSize>(fontSize);
                            runProperties.AppendChild<Underline>(new Underline() { Val = UnderlineValues.Single });
                            runProperties.AppendChild<Bold>(new Bold());
                            run.AppendChild<RunProperties>(runProperties);
                            run.AppendChild(new Text(item.Heading));
                            parHeading.AppendChild(run);
                            mainPart.Document.Append(parHeading);
                            var splitChar = new string[] { "\r\n" };
                            if (item.Content.IsNull())
                                item.Content = string.Empty;
                            var splitBasedOnNewLine = item.Content.Split(splitChar, StringSplitOptions.None).ToList();
                            splitBasedOnNewLine.ForEach(x =>
                            {
                                par = (new Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Text(x))));
                                mainPart.Document.Append(par);
                            });

                        }
                        mainPart.Document.Save();
                    }
                    else
                    {
                        var dt = ListToDataTable(list);
                        AddTable(path, dt, mainPart.Document);
                    }

                }
                Process.Start(path);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static void AddTable(string fileName, DataTable TableData, Document document)
        {
            var doc = document.MainDocumentPart.Document;

            DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();

            TableProperties props = new TableProperties(
                new TableBorders(
                    new DocumentFormat.OpenXml.Wordprocessing.TopBorder
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 8
                    },
                    new DocumentFormat.OpenXml.Wordprocessing.BottomBorder
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 8
                    },
                    new DocumentFormat.OpenXml.Wordprocessing.LeftBorder
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 8
                    },
                    new DocumentFormat.OpenXml.Wordprocessing.RightBorder
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 8
                    },
                    new InsideHorizontalBorder
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 8
                    },
                    new InsideVerticalBorder
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 8
                    }));
            //Align Table Center
            TableWidth tableWidth = new TableWidth() { Width = "3000", Type = TableWidthUnitValues.Pct };
            TableJustification just_center = new TableJustification() { Val = TableRowAlignmentValues.Left };

            table.AppendChild(just_center);
            props.Append(tableWidth);
            table.AppendChild<TableProperties>(props);
            //work on header
            Run run = new Run();
            Paragraph para = doc.AppendChild(new Paragraph());
            TableRow thr = new TableRow();
            for (int i = 0; i < TableData.Columns.Count; i++)
            {
                string headercolumn = TableData.Columns[i].ToString();
                TableCell header_cell = new TableCell();

                Bold bold = new Bold();
                bold.Val = OnOffValue.FromBoolean(true);
                Run run1 = new Run(new RunProperties(bold));
                run1.AppendChild<Text>(new Text(headercolumn));
                Paragraph para1 = new Paragraph(run1);
                header_cell.Append(para1);
                thr.Append(header_cell);
            }
            table.Append(thr);

            var tableRow = TableData.Rows;


            for (int i = 0; i < TableData.Rows.Count; i++)
            {
                var tr = new TableRow();
                for (int j = 0; j < TableData.Columns.Count; j++)
                {
                    var tc = new TableCell();
                    tc.Append(new Paragraph
                        (new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Text(TableData.Rows[i][j].ToString()))));
                    // Assume you want columns that are automatically sized.
                    tc.Append(new TableCellProperties(
                        new TableCellWidth { Type = TableWidthUnitValues.Auto }));

                    tr.Append(tc);
                }
                table.Append(tr);
            }

            //for (var i = 0; i <= data.GetUpperBound(0); i++)
            //{
            //    var tr = new TableRow();
            //    for (var j = 0; j <= data.GetUpperBound(1); j++)
            //    {
            //        var tc = new TableCell();
            //        tc.Append(new Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Text(data[i, j]))));

            //        // Assume you want columns that are automatically sized.
            //        tc.Append(new TableCellProperties(
            //            new TableCellWidth { Type = TableWidthUnitValues.Auto }));

            //        tr.Append(tc);
            //    }
            //    table.Append(tr);
            //}
            doc.Body.Append(table);

            doc.Save();

            //}
        }



        public static DataTable ListToDataTable<T>(List<T> list)
        {
            DataTable dt = new DataTable();

            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                var p = info.GetCustomAttribute(typeof(DisplayAttribute), false) as DisplayAttribute;
                dt.Columns.Add(new DataColumn(p.IsNotNull() ? p.Name : info.Name, GetNullableType(info.PropertyType)));
                //dt.Columns.Add(new DataColumn(info.Name, GetNullableType(info.PropertyType)));
            }
            foreach (T t in list)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyInfo info in typeof(T).GetProperties())
                {
                    var p = info.GetCustomAttribute(typeof(DisplayAttribute), false) as DisplayAttribute;
                    var name = p.IsNotNull() ? p.Name : info.Name;
                    if (!IsNullableType(info.PropertyType))
                        row[name] = info.GetValue(t, null);
                    else
                        row[name] = (info.GetValue(t, null) ?? DBNull.Value);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        private static Type GetNullableType(Type t)
        {
            Type returnType = t;
            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                returnType = Nullable.GetUnderlyingType(t);
            }
            return returnType;
        }

        private static bool IsNullableType(Type type)
        {
            return (type == typeof(string) ||
                    type.IsArray ||
                    (type.IsGenericType &&
                     type.GetGenericTypeDefinition().Equals(typeof(Nullable<>))));
        }

        private Paragraph CreateParagraph(string str)
        {
            Paragraph par = new Paragraph(new Run(new Text(str)));
            return par;
        }

    }
}
