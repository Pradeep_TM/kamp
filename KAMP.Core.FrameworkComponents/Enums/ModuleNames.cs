﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.FrameworkComponents.Enums
{
    public enum ModuleNames
    {
        TLB,
        MV,
        KYC,
        UM
    }
}
