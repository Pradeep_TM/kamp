﻿using AutoMapper;
using KAMP.Core.Workflow.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.Core.Workflow.BLL;
using KAMP.Core.Workflow.Helpers;
using KAMP.Core.Repository.WF;
using KAMP.Core.Repository.UM;
using System.Windows;
namespace KAMP.Core.Workflow.AppCode
{
    public class WorkflowMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<WorkflowMainVm, WFDefinition>();
            Mapper.CreateMap<WorkflowStepVm, Step>();
            Mapper.CreateMap<WorkflowActivityVm, Activity>();
            Mapper.CreateMap<WFDefinition, WorkflowMainVm>();
            Mapper.CreateMap<Step, WorkflowStepVm>();
            Mapper.CreateMap<Activity, WorkflowActivityVm>();
            Mapper.CreateMap<Routing, WFRouting>();
            Mapper.CreateMap<WFRouting, WFRouting>();
            Mapper.CreateMap<Step, Step>()
                .ForMember(d => d.Activity, opt => opt.Ignore());

            Mapper.CreateMap<WorkflowActivityVm, WorkflowActivityVm>();
            Mapper.CreateMap<WorkflowMainVm, WorkflowMainVm>();
            Mapper.CreateMap<WFRouting, Routing>();

            MapEntityToViewModel();
            MapViewToEntityModel();

            base.Configure();
        }

        private void MapViewToEntityModel()
        {
            MapWorkflowRoutingToEntity();
            MapWorkflowActivityToEntity();
            MapWorkflowStepToEntity();
            MapWorkflowVmToEntity();
        }

        private void MapWorkflowRoutingToEntity()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;
            Mapper.CreateMap<Routing, WFRouting>()
                .AfterMap((src, dest) =>
                {
                    if (dest.Id == 0)
                    {
                        dest.CreatedBy = appcontext.User.Id;
                        dest.CreatedDate = DateTime.Now;
                        dest.ModifiedBy = appcontext.User.Id;
                        dest.ModifiedDate = DateTime.Now;
                    }
                    else if (dest.Id > 0)
                    {
                        dest.ModifiedBy = appcontext.User.Id;
                        dest.ModifiedDate = DateTime.Now;
                    }

                }).IgnoreAllNonExisting();
        }

        private void MapWorkflowActivityToEntity()
        {
            Mapper.CreateMap<WorkflowActivityVm, Activity>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.ToStateId, s => s.MapFrom(x => x.StateId))
                .ForMember(d => d.ActionId, s => s.MapFrom(x => x.WfActionId))
                .ForMember(d => d.StepId, s => s.MapFrom(x => x.StepId))
                .ForMember(d => d.MailTemplate, s => s.MapFrom(x => x.MailTemplate))
                .ForMember(d => d.CreatedBy, opt => opt.Ignore())
                .ForMember(d => d.CreatedDate, opt => opt.Ignore())
                .ForMember(d => d.Step, opt => opt.Ignore())
                .ForMember(d => d.State, opt => opt.Ignore()).IgnoreAllNonExisting();


        }

        private void MapWorkflowStepToEntity()
        {
            Mapper.CreateMap<WorkflowStepVm, Step>()
                .ForMember(d => d.Id, s => s.MapFrom(x => x.StepId))
                .ForMember(d => d.State, s => s.MapFrom(x => x.StepName))
                .ForMember(d => d.StepName, s => s.MapFrom(x => x.StepName))
                .ForMember(d => d.DisplayName, s => s.MapFrom(x => x.DisplayName))
                .ForMember(d => d.SequenceNo, s => s.MapFrom(x => x.SequenceNo))
                .ForMember(d => d.WFId, s => s.MapFrom(x => x.WFId))
                .ForMember(d => d.Activity, s => s.MapFrom(x => GetAllActivities(x)))
                .ForMember(d => d.CreatedBy, opt => opt.Ignore())
                .ForMember(d => d.CreatedDate, opt => opt.Ignore())
                .ForMember(d => d.WFDefinition, opt => opt.Ignore())
                .ForMember(d => d.ModifiedBy, opt => opt.Ignore())
                .ForMember(d => d.ModifiedDate, opt => opt.Ignore()).IgnoreAllNonExisting();
        }



        private void MapWorkflowVmToEntity()
        {
            Mapper.CreateMap<WorkflowMainVm, WFDefinition>()
                .ForMember(d => d.WFId, s => s.MapFrom(x => x.WfId))
                .ForMember(d => d.WFName, s => s.MapFrom(x => x.WfName))
                .ForMember(d => d.WorkFlowDefinition, s => s.MapFrom(x => x.WfDefinition))
                .ForMember(d => d.Steps, s => s.MapFrom(x => x.WfStepsVm))
                .ForMember(d => d.ModuleTypeId, s => s.MapFrom(x => x.ObjectTypeId))
                .ForMember(d => d.CreatedBy, opt => opt.Ignore())
                .ForMember(d => d.CreatedDate, opt => opt.Ignore())
                .ForMember(d => d.ModuleType, opt => opt.Ignore())
                .ForMember(d => d.ModifiedBy, opt => opt.Ignore())
                .ForMember(d => d.ModifiedDate, opt => opt.Ignore()).IgnoreAllNonExisting().AfterMap((s, d) =>
                {

                    if (d.IsNull())
                        return;
                    var appcontext = Application.Current.Properties["Context"] as Context;
                    var userId = appcontext.User.Id;
                    var dateTime = DateTime.Now;
                    if (d.WFId == 0)
                    {
                        d.CreatedBy = userId;
                        d.CreatedDate = dateTime;
                        d.ModifiedBy = userId;
                        d.ModifiedDate = dateTime;
                    }
                    var sequenceNo = 1;

                    foreach (var step in d.Steps)
                    {
                        step.SequenceNo = sequenceNo++;
                        if (step.Id == 0)
                        {
                            step.CreatedBy = userId;
                            step.CreatedDate = dateTime;
                            step.ModifiedBy = userId;
                            step.ModifiedDate = dateTime;
                        }
                        foreach (var activitty in step.Activity)
                        {
                            if (activitty.Id == 0)
                            {
                                activitty.CreatedBy = userId;
                                activitty.CreatedDate = dateTime;
                                activitty.ModifiedBy = userId;
                                activitty.ModifiedDate = dateTime;
                                activitty.MailTemplate = string.Empty;
                            }


                        }
                    }
                });
        }

        private void MapEntityToViewModel()
        {
            MapWorkflowRoutingToViewModel();
            MapWorkflowActivityToViewModel();
            MapWorkflowStepToViewModel();
            MapWorkflowDefinitionToViewModel();

        }

        private void MapWorkflowRoutingToViewModel()
        {
            Mapper.CreateMap<WFRouting, Routing>().AfterMap((src, dest) =>
                {
                    dest.IsSelected = true;
                    var wfBl = new WorkflowBL();
                    if (dest.StateName.IsEmpty())
                        dest.StateName = wfBl.GetStateName(dest.ToStateId);
                }).IgnoreAllNonExisting();
        }

        private void MapWorkflowDefinitionToViewModel()
        {
            Mapper.CreateMap<WFDefinition, WorkflowMainVm>()
                .ForMember(d => d.ObjectTypeId, s => s.MapFrom(x => x.ModuleTypeId))
                .ForMember(d => d.WfDefinition, s => s.MapFrom(x => x.WorkFlowDefinition))
                .ForMember(d => d.WfId, s => s.MapFrom(x => x.WFId))
                .ForMember(d => d.WfName, s => s.MapFrom(x => x.WFName))
                .ForMember(d => d.WfStepsVm, s => s.MapFrom(x => GetWFSteps(x.Steps)))
                .ForMember(d => d.AddMoreStep, opt => opt.Ignore())
                .ForMember(d => d.IsUpdate, opt => opt.Ignore())
                .ForMember(d => d.IsWfStepExists, opt => opt.Ignore())
                .ForMember(d => d.OpenStep, opt => opt.Ignore())
                .ForMember(d => d.RemoveStep, opt => opt.Ignore())
                .ForMember(d => d.SaveWorkFlowSteps, opt => opt.Ignore())
                .ForMember(d => d.SaveWfSteps, opt => opt.Ignore())
                .ForMember(d => d.UpdateStep, opt => opt.Ignore())
                .ForMember(d => d.WfcurrentTaskVm, opt => opt.Ignore())
                .ForMember(d => d.stepNo, opt => opt.Ignore())
                .ForMember(d => d.CancelCommand, opt => opt.Ignore()).IgnoreAllNonExisting();
        }

        private DoublyLinkedList<WorkflowStepVm> GetWFSteps(ICollection<Step> steps)
        {
            var wfSteps = new DoublyLinkedList<WorkflowStepVm>();

            if (steps.IsCollectionValid())
            {
                foreach (var item in steps)
                {
                    var step = Mapper.Map<WorkflowStepVm>(item);
                    wfSteps.AddItem(step);
                }
            }

            return wfSteps;
        }

        private void MapWorkflowStepToViewModel()
        {
            Mapper.CreateMap<Step, WorkflowStepVm>()
                .ForMember(d => d.StepId, s => s.MapFrom(x => x.Id))
                .ForMember(d => d.StepName, d => d.MapFrom(x => x.StepName))
                .ForMember(d => d.SequenceNo, s => s.MapFrom(x => x.SequenceNo))
                .ForMember(d => d.WFId, s => s.MapFrom(x => x.WFId))
                .ForMember(d => d.PromoteStates, s => s.MapFrom(x => GetAllStates(x.Activity, (int)WFActions.Promote)))
                .ForMember(d => d.DemoteStates, s => s.MapFrom(x => GetAllStates(x.Activity, (int)WFActions.Demote))).IgnoreAllNonExisting();
        }

        private void MapWorkflowActivityToViewModel()
        {
            Mapper.CreateMap<Activity, WorkflowActivityVm>()
               .ForMember(d => d.Id, s => s.MapFrom(x => x.Id))
               .ForMember(d => d.MailTemplate, s => s.MapFrom(x => x.MailTemplate))
               .ForMember(d => d.StateId, s => s.MapFrom(x => x.ToStateId))
               .ForMember(d => d.StepId, s => s.MapFrom(x => x.StepId))
               .ForMember(d => d.WfActionId, s => s.MapFrom(x => x.ActionId))
               .ForMember(d => d.PromoteRoutes, s => s.MapFrom(x => x.WFRouting.Where(y => y.ActionId == (int)WFActions.Promote)))
               .ForMember(d => d.DemoteRoutes, s => s.MapFrom(x => x.WFRouting.Where(y => y.ActionId == (int)WFActions.Demote)))
               .ForMember(d => d.IsSelected, opt => opt.Ignore())
               .ForMember(d => d.StateName, opt => opt.Ignore()).IgnoreAllNonExisting();
        }

        private List<WorkflowActivityVm> GetAllActivities(WorkflowStepVm source)
        {
            var activity = new List<WorkflowActivityVm>();

            if (source.IsNotNull())
            {
                var selectedPromotes = source.PromoteStates.Where(x => x.IsSelected).ToList();
                var selectedDemotes = source.DemoteStates.Where(x => x.IsSelected).ToList();

                if (selectedPromotes.IsCollectionValid())
                    activity.AddRange(selectedPromotes);

                if (selectedDemotes.IsCollectionValid())
                    activity.AddRange(selectedDemotes);

            }
            return activity;
        }

        private ObservableCollection<WorkflowActivityVm> GetAllStates(ICollection<Activity> wfActivities, int actionId)
        {
            var activities = new ObservableCollection<WorkflowActivityVm>();
            var workflowBL = new WorkflowBL();
            var states = workflowBL.GetStates();
            if (states.IsCollectionValid())
            {
                states.ForEach(x =>
                {
                    WorkflowActivityVm activityVm = null;

                    var activity = wfActivities.FirstOrDefault(act => act.ToStateId == x.Id && act.ActionId == actionId);
                    if (activity.IsNotNull())
                    {
                        activityVm = Mapper.Map<WorkflowActivityVm>(activity);
                        activityVm.IsSelected = true;
                        // activityVm.Id =activity.Id;
                        //activityVm.StepId = activity.StepId;
                        //activityVm.MailTemplate = activity.MailTemplate;                        
                    }
                    if (activityVm.IsNull())
                        activityVm = new WorkflowActivityVm();

                    activityVm.StateId = x.Id;
                    activityVm.StateName = x.StateName;
                    activityVm.WfActionId = actionId;
                    activities.Add(activityVm);
                });
            }
            return activities;
        }


    }
}
