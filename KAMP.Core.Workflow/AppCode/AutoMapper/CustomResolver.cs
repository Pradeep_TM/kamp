﻿using AutoMapper;
using KAMP.Core.Repository.Models;
using KAMP.Core.Workflow.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
namespace KAMP.Core.Workflow.AppCode
{
    internal class ActivityResolver : ValueResolver<WorkflowStepVm, List<WorkflowActivityVm>>
    {
        protected override List<WorkflowActivityVm> ResolveCore(WorkflowStepVm source)
        {
            var activity = new List<WorkflowActivityVm>();

            if (source.IsNotNull())
            {
                var selectedPromotes = source.PromoteStates.Where(x => x.IsSelected).ToList();
                var selectedDemotes = source.DemoteStates.Where(x => x.IsSelected).ToList();

                if (selectedPromotes.IsCollectionValid())
                    activity.AddRange(selectedPromotes);

                if (selectedDemotes.IsCollectionValid())
                    activity.AddRange(selectedDemotes);
            }
            return activity;    

        }

    }

}
