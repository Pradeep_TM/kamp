﻿using AutoMapper;
using KAMP.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Workflow.AppCode
{
    public  class BootStrapper : IBootStrapper
    {
        public  void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<WorkflowMapperProfile>();
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}
