﻿using KAMP.Core.Workflow.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Workflow.UserControls
{
    using AutoMapper;
    /// <summary>
    /// Interaction logic for UCWorkflowRouting.xaml
    /// </summary>
    using Core.FrameworkComponents;
    using KAMP.Core.FrameworkComponents.Enums;
    using KAMP.Core.Repository.WF;
    using KAMP.Core.Workflow.AppCode;
    using KAMP.Core.Workflow.Helpers;
    using Workflow.BLL;
    public partial class UCWorkflowRouting : UserControl
    {
        private WorkflowStepVm _workflowStep;
        private WorkflowRoutingVm _viewModel;
        private UCWorkflowStep __workflowStep;
        private Action CloseControl;
        private Action _initializedData;
        public UCWorkflowRouting(WorkflowStepVm _workflowStep, Action closeControl, Action initializedData)
        {
            this._workflowStep = _workflowStep;
            this.CloseControl += closeControl;
            InitializeData(_workflowStep);
            InitializedPageStyle();
            InitializeComponent();
            _viewModel.PopulateRoutes += PopulateRoutes;
            this._initializedData += initializedData;
            DataContext = _viewModel;
        }

        private void InitializedPageStyle()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;

            var resource1 = new ResourceDictionary();
            var resource2 = new ResourceDictionary();
            var resource3 = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                case ModuleNames.KYC:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                default:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/Style.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
            }


            this.Resources.MergedDictionaries.Add(resource1);
            this.Resources.MergedDictionaries.Add(resource2);
            this.Resources.MergedDictionaries.Add(resource3);
        }
        private void InitializeData(WorkflowStepVm _workflowStep)
        {
            new BootStrapper().Configure();

            if (_viewModel.IsNull())
                _viewModel = new WorkflowRoutingVm();

            if (_viewModel.States.IsNull())
                _viewModel.States = new List<ActivityState>();

            if (_viewModel.Promotes.IsNull())
                _viewModel.Promotes = new System.Collections.ObjectModel.ObservableCollection<Routing>();

            if (_viewModel.Demotes.IsNull())
                _viewModel.Demotes = new System.Collections.ObjectModel.ObservableCollection<Routing>();

            ResetViewModel();
            var defaultItem = new ActivityState()
            {
                StateId = 0,
                StateName = "Select",
            };
            _viewModel.States.Insert(0, defaultItem);
            _viewModel.SelectedState = defaultItem;

            foreach (var item in _workflowStep.PromoteStates.Where(x => x.IsSelected))
                _viewModel.States.Add(new ActivityState { ActionId = item.WfActionId, ActivityId = item.Id, StateId = item.StateId, StateName = item.StateName });

            foreach (var item in _workflowStep.DemoteStates.Where(x => x.IsSelected))
                _viewModel.States.Add(new ActivityState { ActionId = item.WfActionId, ActivityId = item.Id, StateId = item.StateId, StateName = item.StateName });

            if (_workflowStep.PrevNode.IsNotNull())
                foreach (var item in _workflowStep.PrevNode.DemoteStates.Where(x => x.IsSelected))
                    _viewModel.Demotes.Add(new Routing { ActionId = item.WfActionId, StateName = item.StateName, ToStateId = item.StateId, ToActivityId = item.Id, StepId = _workflowStep.PrevNode.StepId });

            if (_workflowStep.NextNode.IsNotNull())
                foreach (var item in _workflowStep.NextNode.PromoteStates.Where(x => x.IsSelected))
                    _viewModel.Promotes.Add(new Routing { ActionId = item.WfActionId, StateName = item.StateName, ToStateId = item.StateId, ToActivityId = item.Id, StepId = _workflowStep.NextNode.StepId });

        }



        private void PopulateRoutes(ActivityState activityStates)
        {
            ResetRoutes(activityStates.StateId, activityStates.ActivityId);
            WorkflowBL wfBl = new WorkflowBL();
            var routes = wfBl.GetRoutes(activityStates);

            if (!routes.IsCollectionValid())
                return;

            var promotesRoutes = routes.Where(x => x.ActionId == (int)WFActions.Promote).ToList();
            var demoteRoutes = routes.Where(x => x.ActionId == (int)WFActions.Demote).ToList();

            foreach (var item in promotesRoutes)
            {
                var route = _viewModel.Promotes.FirstOrDefault(x => x.ToStateId == item.ToStateId);

                if (route.IsNotNull())
                    route = Mapper.Map<WFRouting, Routing>(item, route);

            }

            foreach (var item in demoteRoutes)
            {
                var route = _viewModel.Demotes.FirstOrDefault(x => x.ToStateId == item.ToStateId);

                if (route.IsNotNull())
                    route = Mapper.Map<WFRouting, Routing>(item, route);

            }
        }

        /// <summary>
        /// Resetting and filling the current selected state id and activity id from the dropdown for defining the route for it.
        /// </summary>
        /// <param name="stateId">stateid of the current selected state from the dropdown</param>
        /// <param name="activityId">activity id of the current selected state</param>
        private void ResetRoutes(int stateId, int activityId)
        {
            if (_viewModel.Promotes.IsCollectionValid())
                foreach (var item in _viewModel.Promotes)
                { item.IsSelected = false; item.FromStateId = stateId; item.ActivityId = activityId; item.Id = 0; }

            if (_viewModel.Demotes.IsCollectionValid())
                foreach (var item in _viewModel.Demotes)
                { item.IsSelected = false; item.FromStateId = stateId; item.ActivityId = activityId; item.Id = 0; }

        }
        private void ResetViewModel()
        {
            _viewModel.States.Clear();
            _viewModel.Promotes.Clear();
            _viewModel.Demotes.Clear();
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_initializedData.IsNotNull())
                _initializedData();
            if (CloseControl.IsNotNull())
                CloseControl();
        }
    }
}
