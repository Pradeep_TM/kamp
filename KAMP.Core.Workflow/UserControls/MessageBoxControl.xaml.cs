﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace KAMP.Core.Workflow.UserControls
{
    /// <summary>
    /// Interaction logic for MessageBoxControl.xaml
    /// </summary>
    public partial class MessageBoxControl
    {
        private static bool _okClicked;
        private static bool _cancelClicked;
        private static bool _yesClicked;
        private static bool _noClicked;
        public static bool IsMsgBoxOpen;
        private static Window _win;
        private static Grid _container;
        private static MessageBoxResult _messageBoxResult;
        private static DispatcherTimer _dispatcherTimer;
        private static MessageBoxControl _msg;
        public static Action<object, RoutedEventArgs> DefaultAction;
        private static MessageBoxColor _viewModel;
        public MessageBoxControl()
        {
            InitializeComponent();
            // _viewModel = new MessageBoxColor();
            this.DataContext = _viewModel;
        }

        private static void OpenControl()
        {
            _msg.MessageContainer.Visibility = Visibility.Collapsed;
            _msg.ButtonContainer.Visibility = Visibility.Collapsed;
            _msg.GdContainer.Height = 0;
            _msg.GdContainer.Width = 0;
            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Tick += dispatcherTimerOpen_Tick;
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            _dispatcherTimer.Start();
        }

        private static void CloseControl()
        {
            _container.Children.Remove(_msg);
            return;

            //if (_container != null) _container.IsEnabled = true;
            //try
            //{
            //    _dispatcherTimer.Stop();
            //}
            //catch { }
            //_dispatcherTimer = new DispatcherTimer();
            //_dispatcherTimer.Tick += dispatcherTimerClose_Tick;
            //_dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            //_dispatcherTimer.Start();
        }

        public static void CloseOnEscape()
        {
            CloseControl();
        }

        public static void ExecuteDefaultAction(object obj, RoutedEventArgs e)
        {
            if (DefaultAction != null)
            {
                DefaultAction(obj, e);
            }
        }

        private static void dispatcherTimerClose_Tick(object sender, EventArgs e)
        {
            _msg.MessageContainer.Visibility = Visibility.Hidden;
            _msg.ButtonContainer.Visibility = Visibility.Hidden;
            if ((_msg.GdContainer.Height > 0))
            {
                _msg.GdContainer.Height = _msg.GdContainer.Height - 20;
            }

            if ((_msg.GdContainer.Width > 0))
            {
                _msg.GdContainer.Width = _msg.GdContainer.Width - 30;
            }
            else
            {
                _dispatcherTimer.Stop();
                _container.Children.Remove(_msg);
                IsMsgBoxOpen = false;
            }
        }

        private static void dispatcherTimerOpen_Tick(object sender, EventArgs e)
        {
            if (!(_msg.GdContainer.Height >= 240))
            {
                _msg.GdContainer.Height = _msg.GdContainer.Height + 20;
            }

            if (!(_msg.GdContainer.Width >= 360))
            {
                _msg.GdContainer.Width = _msg.GdContainer.Width + 30;
            }
            else
            {
                _msg.MessageContainer.Visibility = Visibility.Visible;
                _msg.ButtonContainer.Visibility = Visibility.Visible;
                _dispatcherTimer.Stop();
            }

        }

        public static MessageBoxResult Show(string message, MessageBoxButton messageBoxButton = MessageBoxButton.OK, MessageType type = MessageType.Information)
        {
            AssignMessageboxColor();
            string iconUrl = string.Empty;
            if (type == MessageType.Information)
            {
                iconUrl = "../Assets/Images/Info.png";
            }
            else if (type == MessageType.Alert)
            {
                iconUrl = "../Assets/Images/Warning.png";
            }
            else if (type == MessageType.Error)
            {
                iconUrl = "../Assets/Images/Alert.png";
            }

            if (_msg == null)
            {
                _msg = new MessageBoxControl();
            }
            var windows = Application.Current.MainWindow;
            _win = windows;
            if (_win != null)
            {
                _container = _win.FindName("GdContainer") as Grid;
                if (_container != null)
                {
                    var msgBox = FindChild<MessageBoxControl>(_container, _msg.Name);
                    if (msgBox == null)
                    {
                        // _container.IsEnabled = false;
                        _container.Children.Add(_msg);
                        IsMsgBoxOpen = true;
                    }
                    else
                    {
                        // _container.IsEnabled = false;
                        //_container.Children.Remove(msgBox);
                        //_container.Children.Add(_msg);
                        //IsMsgBoxOpen = true;
                        return MessageBoxResult.Cancel;
                    }

                }
            }
            Panel.SetZIndex(_msg, 1000);
            OpenControl();// To open control in expanding manner
            _okClicked = false;
            _cancelClicked = false;
            _yesClicked = false;
            _noClicked = false;
            DefaultAction = null;
            _msg.ImgIcon.Visibility = Visibility.Collapsed;
            _msg.TxtErrorMessage.Text = message;
            if (!string.IsNullOrEmpty(iconUrl))
            {
                _msg.ImgIcon.Source = new BitmapImage(new Uri(iconUrl, UriKind.RelativeOrAbsolute));
                _msg.ImgIcon.Visibility = Visibility.Visible;
            }

            if (messageBoxButton == MessageBoxButton.OK)
            {
                _msg.BtnOk.Visibility = Visibility.Visible;
                _msg.BtnYes.Visibility = Visibility.Collapsed;
                _msg.BtnNo.Visibility = Visibility.Collapsed;
                _msg.BtnCancel.Visibility = Visibility.Collapsed;
                DefaultAction = _msg.OnOkClick;
            }
            if (messageBoxButton == MessageBoxButton.OKCancel)
            {
                _msg.BtnOk.Visibility = Visibility.Visible;
                _msg.BtnYes.Visibility = Visibility.Collapsed;
                _msg.BtnNo.Visibility = Visibility.Collapsed;
                _msg.BtnCancel.Visibility = Visibility.Visible;
                DefaultAction = _msg.OnOkClick;
            }
            if (messageBoxButton == MessageBoxButton.YesNo)
            {
                _msg.BtnOk.Visibility = Visibility.Collapsed;
                _msg.BtnYes.Visibility = Visibility.Visible;
                _msg.BtnNo.Visibility = Visibility.Visible;
                _msg.BtnCancel.Visibility = Visibility.Collapsed;
                DefaultAction = _msg.OnYesClick;
            }
            if (messageBoxButton == MessageBoxButton.YesNoCancel)
            {
                _msg.BtnOk.Visibility = Visibility.Collapsed;
                _msg.BtnYes.Visibility = Visibility.Visible;
                _msg.BtnNo.Visibility = Visibility.Visible;
                _msg.BtnCancel.Visibility = Visibility.Visible;
                DefaultAction = _msg.OnYesClick;
            }
            //_win = Application.Current.MainWindow;
            //if (_win != null)
            //{
            //    _container = _win.FindName("GdContainer") as Grid;
            //    if (_container != null)
            //    {
            //        var msgBox = FindChild<MessageBoxControl>(_container, _msg.Name);
            //        if (msgBox == null)
            //        {
            //            _container.Children.Add(_msg);
            //            _msg.Focus();
            //            _win.IsEnabled = false;
            //        }
            //    }
            //}


            while (!_okClicked && !_yesClicked && !_noClicked && !_cancelClicked)
            {
                // Stop the thread if the application is about to close
                if (_msg.Dispatcher.HasShutdownStarted ||
                    _msg.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // Simulate "DoEvents"
                _msg.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }
            return _messageBoxResult;
        }



        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            _okClicked = true;
            _messageBoxResult = MessageBoxResult.OK;
            CloseControl();
        }

        private void OnYesClick(object sender, RoutedEventArgs e)
        {
            _yesClicked = true;
            _messageBoxResult = MessageBoxResult.Yes;
            CloseControl();
        }

        private void OnNoClick(object sender, RoutedEventArgs e)
        {
            CloseControl();
            _noClicked = true;
            _messageBoxResult = MessageBoxResult.No;

        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            _cancelClicked = true;
            _messageBoxResult = MessageBoxResult.Cancel;
            CloseControl();
        }

        public static T FindChild<T>(DependencyObject parent, string childName)


        where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }
        // <summary>
        /// this method is used assign the color of the message  box based on module type
        /// </summary>

        private static void AssignMessageboxColor()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            if (_viewModel.IsNull())
                _viewModel = new MessageBoxColor();
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    _viewModel.BdrColor = "#FF83ADBF";
                    _viewModel.ContentColor = "#FF223E4A";
                    _viewModel.ButtonColor = "#FF223E4A";
                    break;
                case ModuleNames.KYC:
                    _viewModel.BdrColor = "#FFDBE4E4";
                    _viewModel.ContentColor = "#FF005B5B";
                    _viewModel.ButtonColor = "#FF005B5B";
                    break;
                default:
                    _viewModel.BdrColor = "#FF5B97B4";
                    _viewModel.ContentColor = "#FF2C3664";
                    _viewModel.ButtonColor = "#002f3f";
                    break;
            }
        }
    }

    public enum MessageType
    {
        Information,
        Error,
        Alert
    }
    public class MessageBoxColor : INotifyPropertyChanged
    {
        private string _bdrColor;

        public string BdrColor
        {
            get { return _bdrColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _bdrColor, (x) => x.BdrColor); }
        }

        private string _contentColor;

        public string ContentColor
        {
            get { return _contentColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _contentColor, (x) => x.ContentColor); }
        }
        private string _buttonColor;

        public string ButtonColor
        {
            get { return _buttonColor; }
            set { PropertyChanged.HandleValueChange(this, value, ref _buttonColor, (x) => x.ButtonColor); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
