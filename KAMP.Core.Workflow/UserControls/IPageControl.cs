﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KAMP.Core.Workflow.UserControls
{
    public interface IPageControl
    {
        void InitializeData();
    }
}
