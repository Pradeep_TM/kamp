﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using KAMP.Core.Workflow.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Workflow.UserControls
{
    /// <summary>
    /// Interaction logic for UCWorkflowStep.xaml
    /// </summary>
    public partial class UCWorkflowStep : UserControl
    {
        public UCWorkflowStep()
        {
            InitializedPageStyle();
            InitializeComponent();
        }
        private void InitializedPageStyle()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;

            var resource1 = new ResourceDictionary();
            var resource2 = new ResourceDictionary();
            var resource3 = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                case ModuleNames.KYC:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                default:
                      resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/Style.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
            }


            this.Resources.MergedDictionaries.Add(resource1);
            this.Resources.MergedDictionaries.Add(resource2);
            this.Resources.MergedDictionaries.Add(resource3);
        }
    }
}
