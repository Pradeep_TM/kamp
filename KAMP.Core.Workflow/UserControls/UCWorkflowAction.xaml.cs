﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Activities;
using System.Activities.XamlIntegration;
using System.Data.Entity;
using System.IO;
namespace KAMP.Core.Workflow.UserControls
{
    /// <summary>
    /// Interaction logic for WorkflowAction.xaml
    /// </summary>
    /// 
    using Workflow.ViewModels;
    using FrameworkComponents;
    using Workflow.Helpers;
    using Repository;
    using Repository.WF;
    using Repository.UM;
    using BLL;
    using System.Transactions;
    using KAMP.Core.FrameworkComponents.Enums;
    public partial class UCWorkflowAction : UserControl
    {
        private WorkflowActionVm _viewModel = null;
        public UCWorkflowAction()
        {
            InitializedPageStyle();
            InitializeComponent();
            _viewModel = this.DataContext as WorkflowActionVm;
            if (_viewModel.IsNotNull())
                InitializedData();
        }

        /// <summary>
        /// this method is used to get the style for the page based on module.
        /// </summary>
        private void InitializedPageStyle()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;

            var resource1 = new ResourceDictionary();
            var resource2 = new ResourceDictionary();
            var resource3 = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                case ModuleNames.KYC:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                default:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/Style.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
            }


            this.Resources.MergedDictionaries.Add(resource1);
            this.Resources.MergedDictionaries.Add(resource2);
            this.Resources.MergedDictionaries.Add(resource3);
        }

        private void OnSubmitClick()
        {
            if (_viewModel.Comments.IsEmpty())
            {
                OpenMessageBox("Please provide comments to proceed.", MessageType.Error);
                return;
            }

            if ((_viewModel.SelectedStateWithAction.IsNotNull() && _viewModel.SelectedStateWithAction.StateId > 0 && _viewModel.SelecteduserId > 0)

            || (_viewModel.IsFinal && _viewModel.SelectedStateWithAction.ActionId == (int)WFActions.Finalized) || (_viewModel.SelecteduserId > 0 && _viewModel.SelectedStateWithAction.ActionId > 0))
            {
                ProcessWorkflow();
                SetDefaultValues();
            }
            else if (_viewModel.SelectedStateWithAction.IsNull() || _viewModel.SelectedStateWithAction.StateId == 0)
                OpenMessageBox("Please select state.", MessageType.Error);

            else if (_viewModel.SelecteduserId == 0)
                OpenMessageBox("Please select user.", MessageType.Error);

        }



        private void SetDefaultValues()
        {

            if (_viewModel.UsersInGroups.IsCollectionValid())
                _viewModel.UsersInGroups.Clear();

            if (_viewModel.StatesWithAction.IsCollectionValid())
                _viewModel.StatesWithAction.Clear();

            _viewModel.CurrentStatus = string.Empty;

            _viewModel.SelecteduserId = 0;
            _viewModel.SelectedStateWithAction = null;
            _viewModel.IsCurrentAssignUser = false;
            _viewModel.Comments = "";
        }


        public void InitializedData()
        {
            _viewModel = this.DataContext as WorkflowActionVm;

            if (_viewModel.OnSubmitClick.IsNull())
                _viewModel.OnSubmitClick += OnSubmitClick;

            if (_viewModel.PopulateUsers.IsNull())
                _viewModel.PopulateUsers += PopulateUsers;

            if (_viewModel.OnParkUnParkedClick.IsNull())
                _viewModel.OnParkUnParkedClick += OnParkUnParkedClick;

            var appcontext = Application.Current.Properties["Context"] as Context;

            var workflowBL = new WorkflowBL();
            var assignment = workflowBL.GetAssignment(_viewModel.CaseNo, appcontext.User.Id, appcontext.Module.Id);

            if (assignment == null)
                return;

            _viewModel.AssignmentId = assignment.Id;


            _viewModel.IsFinal = assignment.NextState == Helper.Status.End ? true : false;

            var assigned = assignment.AssignedTo.FirstOrDefault(x => x.UserId == appcontext.User.Id && x.WorkflowStatus.ToLower() == Helper.Status.InProgess.ToLower());

            if (assigned.IsNull())
                return;


            _viewModel.CurrentStatus = string.Format("{0}-{1}", assigned.IsPark ? Helper.Status.Parked : workflowBL.GetDisplayName(assignment.Status, appcontext.Module.Id), assigned.State);

            _viewModel.IsCurrentAssignUser = true;
            _viewModel.IsParked = assigned.IsPark;

            if (!_viewModel.IsParked)
                PopulateState(assignment);

        }

        private void OnParkUnParkedClick()
        {
            var actionName = _viewModel.IsParked ? "UnPark" : "Park";
            var actionResult = MessageBoxControl.Show(string.Format("Are you sure want to {0} the case?", actionName), MessageBoxButton.YesNo, MessageType.Alert);
            if (actionResult == MessageBoxResult.Yes)
            {
                if (_viewModel.Comments.IsEmpty())
                {
                    OpenMessageBox(string.Format("Please provide {0} reason in comments.", actionName), type: MessageType.Error);
                    return;
                }
                var wfBl = new WorkflowBL();
                try
                {
                    wfBl.UpdateParkStatus(_viewModel.CaseNo, _viewModel.Comments);
                    SetDefaultValues();
                    InitializedData();
                }
                catch (BusinessException ex)
                {
                    OpenMessageBox(ex.Message, MessageType.Error);
                }
            }
        }

        private void PopulateUsers(int stateId)
        {
            var workflowBL = new WorkflowBL();
            //var users = workflowBL.GetUsersByStateId(stateId);

            //This coding can be remove if we create a Assigned state and map it with Analyst group.
            List<User> users = null;
            if (stateId == -1)
                users = workflowBL.GetUsersFromAnalystGroup();
            else
                users = workflowBL.GetUsersByStateId(stateId);
            if (!users.IsCollectionValid())
                return;
            //
            users.Insert(0, new User() { UserId = 0, UserName = "Select" });
            _viewModel.UsersInGroups = new ObservableCollection<User>();
            _viewModel.UsersInGroups.AddRange(users);
            _viewModel.SelecteduserId = 0;
        }

        private void PopulateState(Assignment assignment)
        {
            ResetViewModel();

            if (assignment.Status == Helper.Status.End)
                return;

            var workflowBL = new WorkflowBL();
            var stateWithAction = new List<StateWithAction>();
            var appcontext = Application.Current.Properties["Context"] as Context;
            var currentStep = workflowBL.GetWorkflowStep(assignment.Status);

            if (assignment.Status == Helper.Status.Assigned)
            {
                var promotestep = workflowBL.GetWorkflowStep(assignment.NextState);
                if (promotestep.IsNotNull())
                {
                    var promotestates = promotestep.Activity.Where(x => x.ActionId == (int)WFActions.Promote).
                        Select(x =>
                        new StateWithAction()
                        {
                            ActionId = x.ActionId,
                            StateId = x.ToStateId,
                            ActivityId = x.Id,
                        }).ToList();
                    stateWithAction.AddRange(promotestates);
                }
            }
            else
            {
                var assignTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus.ToLower() == Helper.Status.InProgess.ToLower() && x.UserId == appcontext.User.Id);
                var routes = workflowBL.GetRoutes(assignTo.ActivityId);
                if (routes.IsCollectionValid())
                {
                    routes.ForEach(x =>
                    {
                        stateWithAction.Add(new StateWithAction
                        {
                            ActionId = x.ActionId,
                            ActivityId = x.ToActivityId,
                            StateId = x.ToStateId,
                        });
                    });
                }

            }


            if (currentStep.IsNotNull() && currentStep.SequenceNo == 1)
            {
                stateWithAction.Add(new StateWithAction
                {
                    ActionId = (int)WFActions.Demote,
                    StateId = -1,
                    StateName = Helper.Status.Assigned,
                });
            }

            stateWithAction.ForEach(x =>
            {
                var stateName = workflowBL.GetStateName(x.StateId);
                if (!string.IsNullOrEmpty(stateName))
                    x.StateName = stateName;
            });

            stateWithAction = stateWithAction.OrderByDescending(x => x.StateNameWithAction).ToList();

            stateWithAction.Insert(0, new StateWithAction { StateId = 0, StateName = "Select" });


            if (_viewModel.IsFinal)
                stateWithAction.Insert(1, new StateWithAction { ActionId = (int)WFActions.Finalized, StateName = "Finalize workflow" });


            if (_viewModel.StatesWithAction.IsNull())
                _viewModel.StatesWithAction = new ObservableCollection<StateWithAction>();

            _viewModel.StatesWithAction.Clear();
            _viewModel.StatesWithAction.AddRange(stateWithAction);
            _viewModel.IsCurrentAssignUser = _viewModel.StatesWithAction.IsCollectionValid();
            _viewModel.SelectedStateWithAction = _viewModel.StatesWithAction.FirstOrDefault();
        }

        private void ResetViewModel()
        {
            _viewModel.StatesWithAction = null;
            _viewModel.UsersInGroups = null;
            _viewModel.IsCurrentAssignUser = false;
        }

        private string GetMessage()
        {
            // action = (int)_viewModel.TypOfAction;
            var action = _viewModel.SelectedStateWithAction.IsNotNull() ? _viewModel.SelectedStateWithAction.ActionId : 0;
            string msg = string.Empty;
            switch (action)
            {
                case (int)WFActions.Promote:
                    msg = "Promoted and move to next level.";
                    break;
                case (int)WFActions.Demote:
                    msg = "Demoted and move to previous level.";
                    break;
                case (int)WFActions.Park:
                    msg = "Workflow has parked.";
                    break;
                default:
                    if (_viewModel.IsFinal)
                        msg = "All states of workflow has been completed.";
                    else
                        msg = "Promoted and move to next level.";
                    break;
            }
            return msg;
        }



        public void ProcessWorkflow()
        {

            var appcontext = Application.Current.Properties["Context"] as Context;
            var workflowBl = new WorkflowBL();
            var workFlow = workflowBl.GetWorkflowDef(appcontext.Module.Id);
            if (workFlow == null)
                return;

            var assignmentId = _viewModel.AssignmentId;
            var comments = _viewModel.Comments;
            var stateName = _viewModel.SelectedStateWithAction.StateName;
            long userId = _viewModel.SelecteduserId;
            var action = _viewModel.SelectedStateWithAction.ActionId;
            var activityId = _viewModel.SelectedStateWithAction.ActivityId;
            var wfUtilities = new WorkflowUtilities();

            using (var scope = new TransactionScope())
            {
                try
                {

                    if (_viewModel.IsFinal && action == (int)WFActions.Finalized)
                        wfUtilities.UpdateWorkflowEntries(assignmentId, userId, stateName, comments);
                    else
                        wfUtilities.UpdateWorkflowEntries(assignmentId, userId, stateName, comments, action, activityId);

                    if (_viewModel.PostAction.IsNotNull())
                        _viewModel.PostAction(_viewModel.CaseNo);


                    OpenMessageBox(GetMessage());
                    _viewModel.IsCurrentAssignUser = false;

                    if (_viewModel.ClosePopUp.IsNotNull())
                        _viewModel.ClosePopUp();

                    scope.Complete();
                }

                catch (Exception ex)
                {
                    scope.Dispose();
                    OpenMessageBox(ex.ToString(), MessageType.Error);
                }
            }

        }

        private void OpenMessageBox(string msg, MessageType type = MessageType.Information)
        {
            var messagebox = MessageBoxControl.Show(msg, type: type);
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_viewModel.ClosePopUp.IsNotNull())
                _viewModel.ClosePopUp();
        }
    }
}
