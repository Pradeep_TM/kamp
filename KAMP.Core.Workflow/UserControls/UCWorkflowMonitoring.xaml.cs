﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Workflow.UserControls
{
    /// <summary>
    /// Interaction logic for UCWorkflowMonitoring.xaml
    /// </summary>
    using BLL;
    using FrameworkComponents;
    using KAMP.Core.Workflow.ViewModels;
    using Repository.WF;
    using Helpers;
    using KAMP.Core.FrameworkComponents.Enums;
    public partial class UCWorkflowMonitoring : UserControl, IPageControl, Core.FrameworkComponents.IPageControl
    {
        const int xvalue = 200;
        const int yvalue = 5;
        int marginTop = 40;
        int x = xvalue;
        int y = yvalue;
        int height = 30;
        int width = 150;
        int padding = 20;
        static List<List<StepInformation>> WorkflowSteps = new List<List<StepInformation>>();
        private WorkflowMonitoringVm _viewModel;
        public UCWorkflowMonitoring()
        {
            _viewModel = new WorkflowMonitoringVm()
            {
                BdrBrushColor = "#FF82BCD6",
                LblForegroundColor = "#0339b1",
                WindowBackGroundColor = "#FFCBE4E6"
            };
            DataContext = _viewModel;
            InitializedPageStyle();
            InitializeComponent();
            InitializeData();
        }
        private void InitializedPageStyle()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;

            var resource1 = new ResourceDictionary();
            var resource2 = new ResourceDictionary();
            var resource3 = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    break;
                case ModuleNames.KYC:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    break;
                default:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/Style.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    break;
            }


            this.Resources.MergedDictionaries.Add(resource1);
            this.Resources.MergedDictionaries.Add(resource2);
            this.Resources.MergedDictionaries.Add(resource3);
        }
        public void InitializeData()
        {
            cmb_Cases.SelectedItem = null;
            _viewModel.IsStepsVisible = false;
            if (StepArea.Children.IsNotNull())
                StepArea.Children.Clear();
            FillCases();
        }
        private void FillCases()
        {

            // Change ModuleType == 1 from context
            // Change Delivered to Enum
            var wfBL = new WorkflowBL();
            var appcontext = Application.Current.Properties["Context"] as Context;
            //var temp = context.WF_WorkflowTracking.
            //    Where(x => x.ModuleTypeId == 1)
            //    .Join(context.WF_Assignment, y => y.AssignmentId, z => z.Id, (y, z) => z)
            //    .Where(u => u.NextState != "Delivered")
            //    .Distinct<WF_Assignment>()
            //    .ToList<WF_Assignment>();
            var assignment = wfBL.GetAssignment(appcontext.Module.Id);
            if (!assignment.IsCollectionValid())
                return;
            cmb_Cases.ItemsSource = assignment;
            cmb_Cases.DisplayMemberPath = "CaseNo";
            cmb_Cases.SelectedValuePath = "Id";
        }

        private void ConnectStates(string stepName)
        {
            PointCollection fromxy = null;
            PointCollection toxy = null;
            var stages = WorkflowSteps.Where(u => u.Any(v => v.DisplayName == stepName)).ToList()[0].ToList<StepInformation>();

            List<string> actions = new List<string> { "Promote", "Demote" };

            foreach (var item in actions)
            {
                var states = stages.Where(m => m.Action == item).ToList<StepInformation>();
                fromxy = states[0].ReferenceCoordinate;
                foreach (var state in states)
                {
                    var lineConnector = new Line();
                    lineConnector.Stroke = Brushes.Red;
                    lineConnector.StrokeThickness = 2;
                    toxy = state.Coordinate;
                    lineConnector.X1 = (Convert.ToInt16(fromxy[2].X) + Convert.ToInt16(fromxy[0].X)) / 2;
                    lineConnector.Y1 = Convert.ToInt16(fromxy[3].Y);
                    lineConnector.X2 = (Convert.ToInt16(toxy[0].X) + Convert.ToInt16(toxy[2].X)) / 2;
                    lineConnector.Y2 = Convert.ToInt16(toxy[1].Y);
                    DrawArea.Children.Add(lineConnector);
                }
            }
        }

        private void DrawMainActivity()
        {
            // Hardcoded to Module Type ID to 1.
            // This has to be changed
            var appcontext = Application.Current.Properties["Context"] as Context;
            var wfBl = new WorkflowBL();
            var steps = wfBl.GetSteps(appcontext.Module.Id);
            if (steps.IsCollectionValid())
            {
                steps.ForEach(ProcessChain);
                _viewModel.IsStepsVisible = true;
            }
        }

        private void ProcessChain(Step wfStep)
        {
            var wfBl = new WorkflowBL();
            var activities = wfStep.Activity.Select(x => new StepInformation
            {
                StepName = wfStep.StepName,
                DisplayName = wfStep.DisplayName,
                StateName = wfBl.GetStateName(x.ToStateId),
                Action = GetAction(x.ActionId)

            }).ToList<StepInformation>();

            WorkflowSteps.Add(DrawSteps(activities));

        }

        private string GetAction(int actionId)
        {
            if ((int)WFActions.Promote == actionId)
                return WFActions.Promote.ToString();
            else if ((int)WFActions.Demote == actionId)
                return WFActions.Demote.ToString();
            return string.Empty;
        }

        private List<StepInformation> DrawSteps(List<StepInformation> stepInfo)
        {
            var stepName = stepInfo.Select(x => x.StepName).FirstOrDefault();
            var displayName = stepInfo.Select(x => x.DisplayName).FirstOrDefault();
            long selectedValue = Convert.ToInt32(cmb_Cases.SelectedValue);
            string statusCheck = "";
            var appcontext = Application.Current.Properties["Context"] as Context;
            var wfbl = new WorkflowBL();
            var assignmentTo = wfbl.GetAssignTo(selectedValue, stepName, appcontext.Module.Id);

            foreach (var item in assignmentTo)
            {
                WFActions action = (WFActions)item.ActionId;
                var stepInto = stepInfo.Where(x => x.StateName == item.State && action.ToString() == x.Action).FirstOrDefault<StepInformation>();
                var itemStatusinfo = new WorkflowStatusInfo()
                {
                    Status = item.WorkflowStatus,
                    DateStarted = Convert.ToDateTime(item.CreatedDate),
                    DateCompleted = Convert.ToDateTime(item.ModifiedDate),
                    User = wfbl.GetUserName(item.UserId),
                    Comments = item.Comments,
                    Sequence = item.Sequence
                };

                if (stepInto.IsNotNull() && stepInto.WorkflowStatusInfo == null)
                {
                    var WStatusinfo = new List<WorkflowStatusInfo>();
                    WStatusinfo.Add(itemStatusinfo);
                    stepInto.WorkflowStatusInfo = WStatusinfo;
                }
                else if (stepInto.IsNotNull())
                {
                    stepInto.WorkflowStatusInfo.Add(itemStatusinfo);
                }
                statusCheck = statusCheck != item.WorkflowStatus ? item.WorkflowStatus : statusCheck;
            }


            Label label = null;
            SolidColorBrush labelBrush = null;
            label = new Label();
            label.Content = displayName;
            label.Width = 180;
            label.FontWeight = FontWeights.Bold;
            label.FontFamily = new System.Windows.Media.FontFamily("Verdana");
            label.Foreground = Brushes.Black;
            label.Margin = new Thickness(10, 5, 10, 5);
            label.PreviewMouseLeftButtonUp += label_PreviewMouseLeftButtonUp;
            switch (statusCheck)
            {
                case "Completed":

                    labelBrush = new SolidColorBrush(Colors.LightGreen);
                    label.Background = labelBrush;
                    StepArea.Children.Add(label);
                    break;
                case "InProgress":
                    labelBrush = new SolidColorBrush(Colors.LightYellow);
                    label.Background = labelBrush;
                    StepArea.Children.Add(label);
                    break;
                default:
                    labelBrush = new SolidColorBrush(Colors.LightCyan);
                    label.Background = labelBrush;
                    StepArea.Children.Add(label);
                    break;
            }


            return stepInfo;
        }

        void label_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            x = xvalue;
            y = yvalue;
            DrawArea.Children.Clear();
            var label = sender as Label;
            var stages = WorkflowSteps.Where(u => u.Any(v => v.DisplayName == label.Content.ToString())).ToList()[0].ToList<StepInformation>();
            List<string> actions = new List<string> { "Promote", "Demote" };
            foreach (var item in actions)
            {
                var actionRender = new StepInformation() { Action = item, StateName = item, FloatDown = true };
                x = xvalue - 2;
                var RefPoly = Render(actionRender);
                var states = stages.Where(m => m.Action == item).ToList<StepInformation>();
                var offset = 0;
                foreach (var state in states)
                {
                    if (states.IndexOf(state) == 0)
                    {
                        state.FloatDown = true;
                        var stageCount = states.Count / 2;
                        offset = (width * stageCount + padding * stageCount - 1);
                        if (states.Count % 2 == 0)
                        {
                            offset = (width * stageCount + padding * stageCount - 1) - width / 2;
                        }
                    }
                    state.ReferenceCoordinate = RefPoly;
                    state.FloatLeft = true;
                    RefPoly = Render(state, offset);
                    state.Coordinate = RefPoly;
                    offset = 0;
                }
            }
            ConnectStates(label.Content.ToString());
        }

        private PointCollection Render(StepInformation StepInfo = null, int offset = 0)
        {
            if (StepInfo.FloatLeft)
            {
                if (offset != 0)
                {
                    x = x - offset;
                }
                else
                {
                    x = Convert.ToInt16(StepInfo.ReferenceCoordinate[0].X) + width + padding;
                }
            }
            if (StepInfo.FloatDown)
            {
                y = y + height + marginTop;
            }

            var stageName = new Label();
            stageName.Content = StepInfo.StateName;
            ///added code
            var length = System.Windows.Forms.TextRenderer.MeasureText(StepInfo.StateName, font: new System.Drawing.Font("Arial", 12));
            var size = width - length.Width;
            var space = size >= 10 ? size / 2 : 10;
            ////
            var thick = new Thickness(x + space, y, 0, 0);
            stageName.Margin = thick;



            var Point1 = new Point(x, y);
            var Point2 = new Point(x + width, y);
            var Point3 = new Point(x + width, y + height);
            var Point4 = new Point(x, y + height);

            PointCollection polygonPoints = new PointCollection();
            polygonPoints.Add(Point1);
            polygonPoints.Add(Point2);
            polygonPoints.Add(Point3);
            polygonPoints.Add(Point4);


            SolidColorBrush FillBrush = new SolidColorBrush();
            SolidColorBrush strokeBrush = new SolidColorBrush();
            strokeBrush.Color = Colors.Black;
            var metadata = new Label();
            var metadatacontent = new StringBuilder();
            if (StepInfo.WorkflowStatusInfo != null)
            {
                foreach (var item in StepInfo.WorkflowStatusInfo)
                {
                    metadatacontent = metadatacontent.Append("Sequence :" + item.Sequence + "\r\n");
                    metadatacontent.Append("User :" + item.User + "\r\n");
                    metadatacontent.Append("Date Started :" + item.DateStarted.ToShortDateString() + "\r\n");
                    switch (StepInfo.WorkflowStatusInfo == null ? "Default" : item.Status)
                    {
                        case "Completed":
                            FillBrush.Color = Colors.LightGreen;
                            //var thicks = new Thickness(x, y + 25, 0, 0);
                            // metadata.Margin = thicks;
                            break;
                        case "InProgress":
                            FillBrush.Color = Colors.LightYellow;
                            break;
                        case "Default":
                            FillBrush.Color = Colors.White;
                            break;
                    }
                }
            }

            // refactor this.
            metadata.Content = metadatacontent;
            metadata.FontSize = 10;
            metadata.FontWeight = FontWeights.Bold;
            metadata.FontFamily = new System.Windows.Media.FontFamily("Verdana");

            Polygon polygon = new Polygon();
            polygon.Stroke = strokeBrush;
            polygon.Fill = FillBrush;
            polygon.Points = polygonPoints;
            polygon.ToolTip = metadatacontent;
            stageName.ToolTip = metadatacontent;
            DrawArea.Children.Add(polygon);
            DrawArea.Children.Add(stageName);
            // DrawArea.Children.Add(metadata);

            return polygonPoints;
        }

        private void cmb_Cases_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StepArea.Children.Clear();
            if (DrawArea.Children.IsNotNull())
                DrawArea.Children.Clear();
            WorkflowSteps = new List<List<StepInformation>>();
            DrawMainActivity();
        }



        public dynamic InitializeViewModel()
        {
            InitializeData();
            return _viewModel;

        }

        public string PageName
        {
            get { return "Workflow Monitoring"; }
        }
    }

    public class StepInformation
    {
        public string StepName { get; set; }
        public string DisplayName { get; set; }
        public string StateName { get; set; }
        public string Action { get; set; }
        public PointCollection Coordinate { get; set; }
        public PointCollection ReferenceCoordinate { get; set; }
        public bool FloatDown { get; set; }
        public bool FloatLeft { get; set; }
        public List<WorkflowStatusInfo> WorkflowStatusInfo { get; set; }
    }

    public class WorkflowStatusInfo
    {
        //  public string State { get; set; }
        public string User { get; set; }
        public DateTime DateStarted { get; set; }
        public DateTime DateCompleted { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public int? Sequence { get; set; }
    }
}
