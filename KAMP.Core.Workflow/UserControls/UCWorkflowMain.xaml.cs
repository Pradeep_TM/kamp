﻿using KAMP.Core.Workflow.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Workflow.UserControls
{
    using FrameworkComponents;
    using BLL;
    using AutoMapper;
    using Repository.WF;
    using Repository.UM;
    using Helpers;
    using KAMP.Core.Workflow.AppCode;
    using KAMP.Core.FrameworkComponents.Enums;
    /// <summary>
    /// Interaction logic for UCWorkflowMain.xaml
    /// </summary>
    public partial class UCWorkflowMain : UserControl, IPageControl, Core.FrameworkComponents.IPageControl, Core.FrameworkComponents.IMVPageControl
    {
        internal WorkflowMainVm ViewModel = null;
        private Action _backToMain;
        public UCWorkflowMain()
        {

            var appcontext = Application.Current.Properties["Context"] as Context;
            ViewModel = new WorkflowMainVm()
            {
                WfName = string.Format("{0}Workflow", appcontext.Module.Name),
                WfDefinition = string.Format("Definition For {0}", appcontext.Module.Name),
                ObjectTypeId = appcontext.Module.Id,
            };
            InitializeData();
            DataContext = ViewModel;
            InitializedPageStyle();
            InitializeComponent();     
        }

        private void InitializedPageStyle()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;

            var resource1 = new ResourceDictionary();
            var resource2 = new ResourceDictionary();
            var resource3 = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                case ModuleNames.KYC:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
                default:
                       resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/Style.xaml", UriKind.RelativeOrAbsolute);
                       resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/ScrollViewerNavigation.xaml", UriKind.RelativeOrAbsolute);
                    break;
            }


            this.Resources.MergedDictionaries.Add(resource1);
            this.Resources.MergedDictionaries.Add(resource2);
            this.Resources.MergedDictionaries.Add(resource3);
        }
        private void SaveWorkFlowSteps()
        {
            if (!ViewModel.WfStepsVm.IsCollectionValid())
                return;

            new BootStrapper().Configure();
            var workflowBL = new WorkflowBL();
            if (ViewModel.WfId == 0)
            {
                try
                {
                    var workFlowDefinition = Mapper.Map<WFDefinition>(ViewModel);
                    workflowBL.SaveWorkflow(workFlowDefinition);
                    ViewModel.OpenMessageBox("Workflow created successfully.");
                    ViewModel.WfStepsVm = null;
                    ViewModel.IsWfStepExists = false;
                    InitializeData();
                }
                catch (BusinessException ex)
                {
                    ViewModel.OpenMessageBox(ex.Message, MessageType.Error);
                }

            }

            else if (ViewModel.WfId > 0)
            {
                try
                {
                    var workflowDefinition = Mapper.Map<WFDefinition>(ViewModel);
                    workflowBL.UpdateWorkflow(workflowDefinition);
                    ViewModel.OpenMessageBox("Workflow updated successfully.");
                    ViewModel.WfStepsVm = null;
                    ViewModel.WfId = 0;
                    InitializeData();
                }
                catch (BusinessException ex)
                {
                    ViewModel.OpenMessageBox(ex.Message, MessageType.Error);
                }
            }
        }

        private void OpenExistingWorkflow(WFDefinition workflowDef)
        {
            new BootStrapper().Configure();
            var viewmodel = Mapper.Map<WorkflowMainVm>(workflowDef);

            if (viewmodel.IsNotNull())
            {
                ViewModel = Mapper.Map<WorkflowMainVm, WorkflowMainVm>(viewmodel, ViewModel);
                ViewModel.IsWfStepExists = ViewModel.WfStepsVm.IsCollectionValid();
            }

        }

        public void InitializeData()
        {
            var workflowBL = new WorkflowBL();
            var appcontext = Application.Current.Properties["Context"] as Context;
            var workflowDef = workflowBL.GetWorkflowDef(appcontext.Module.Id);

            if (workflowDef.IsNotNull())
                OpenExistingWorkflow(workflowDef);

            if (ViewModel.SaveWorkFlowSteps.IsNull())
                ViewModel.SaveWorkFlowSteps += SaveWorkFlowSteps;

            if (ViewModel.RemoveWfStep.IsNull())
                ViewModel.RemoveWfStep += RemoveWfStep;

            if (ViewModel.IntializedData.IsNull())
                ViewModel.IntializedData += InitializeData;

            var states = workflowBL.GetStates();

            if (!states.IsCollectionValid())
                return;

            if (ViewModel.WfcurrentTaskVm == null)
                ViewModel.WfcurrentTaskVm = new WorkflowStepVm();

            InitializedActivityStates(states);


        }

        private void RemoveWfStep(int stepId)
        {
            try
            {
                var wfBl = new WorkflowBL();
                wfBl.RemoveWfStep(stepId);
            }
            catch (BusinessException ex)
            {

                ViewModel.OpenMessageBox("Cannot remove the workflow step because workflow is in process.", MessageType.Error);
            }
            InitializeData();
        }

        private void InitializedActivityStates(List<State> states)
        {
            if (ViewModel.WfcurrentTaskVm.PromoteStates.IsNull())
                ViewModel.WfcurrentTaskVm.PromoteStates = new System.Collections.ObjectModel.ObservableCollection<WorkflowActivityVm>();

            if (ViewModel.WfcurrentTaskVm.DemoteStates.IsNull())
                ViewModel.WfcurrentTaskVm.DemoteStates = new System.Collections.ObjectModel.ObservableCollection<WorkflowActivityVm>();

            //if (ViewModel.WfcurrentTaskVm.ParkStates.IsNull())
            //    ViewModel.WfcurrentTaskVm.ParkStates = new System.Collections.ObjectModel.ObservableCollection<WorkflowActivityVm>();

            ViewModel.WfcurrentTaskVm.PromoteStates.Clear();
            ViewModel.WfcurrentTaskVm.DemoteStates.Clear();
            //ViewModel.WfcurrentTaskVm.ParkStates.Clear();
            foreach (var state in states)
            {
                ViewModel.WfcurrentTaskVm.PromoteStates.Add(new WorkflowActivityVm() { StateId = (int)state.Id, StateName = state.StateName, WfActionId = (int)WFActions.Promote, NotifyPropertyChanged = ViewModel.SetDirty });
                ViewModel.WfcurrentTaskVm.DemoteStates.Add(new WorkflowActivityVm() { StateId = (int)state.Id, StateName = state.StateName, WfActionId = (int)WFActions.Demote, NotifyPropertyChanged = ViewModel.SetDirty });
               // ViewModel.WfcurrentTaskVm.ParkStates.Add(new WorkflowActivityVm() { StateId = (int)state.Id, StateName = state.StateName, WfActionId = (int)WFActions.Park, NotifyPropertyChanged = ViewModel.SetDirty });
            }

            if (ViewModel.WfStepsVm.IsCollectionValid())
                foreach (var item in ViewModel.WfStepsVm)
                    item.NotifyPropertyChanged = ViewModel.SetDirty;

            ViewModel.IsDirty = false;
        }

        public dynamic InitializeViewModel()
        {
            InitializeData();
            return ViewModel;
        }

        public string PageName
        {
            get { return Helper.WFConstant.ManageWorkflow; }
        }

        void IMVPageControl.InitializeViewModel()
        {
            InitializeData();
        }
    }
}
