﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.Workflow.UserControls
{
    using BLL;
    using FrameworkComponents;
    using Models;
    using AutoMapper;
    using Repository.WF;
    using Repository.UM;
    using Helpers;
    using KAMP.Core.Workflow.AppCode;
    using KAMP.Core.Workflow.ViewModels;
    using System.Collections.ObjectModel;
    using KAMP.Core.FrameworkComponents.Enums;
    /// <summary>
    /// Interaction logic for UCWorkflowState.xaml
    /// </summary>
    public partial class UCWorkflowState : UserControl, IPageControl, Core.FrameworkComponents.IPageControl, Core.FrameworkComponents.IMVPageControl
    {
        internal WorkflowStateVm ViewModel = null;
        public UCWorkflowState()
        {
            if (ViewModel.IsNull())
                ViewModel = new WorkflowStateVm();

            InitializeData();
            AssignAllState();
            this.DataContext = ViewModel;
            InitializedPageStyle();
            InitializeComponent();

        }

        private void InitializedPageStyle()
        {
            var appcontext = Application.Current.Properties["Context"] as Context;

            var resource1 = new ResourceDictionary();
            var resource2 = new ResourceDictionary();
            var resource3 = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appcontext.Module.Name, true);
            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/TLB/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    break;
                case ModuleNames.KYC:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/StyleSheet.xaml", UriKind.RelativeOrAbsolute);
                    resource2.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/KYC/ScrollViewer.xaml", UriKind.RelativeOrAbsolute);
                    break;
                default:
                    resource1.Source = new Uri(@"pack://application:,,,/KAMP.Core.Workflow;component/Assets/Style/Style.xaml", UriKind.RelativeOrAbsolute);
                    break;
            }


            this.Resources.MergedDictionaries.Add(resource1);
            this.Resources.MergedDictionaries.Add(resource2);
            this.Resources.MergedDictionaries.Add(resource3);
        }
        public void InitializeData()
        {
            if (ViewModel.SaveState.IsNull())
                ViewModel.SaveState += this.SaveState;

            if (ViewModel.RemoveState.IsNull())
                ViewModel.RemoveState += RemoveState;

            if (ViewModel.Reset.IsNull())
                ViewModel.Reset += ResetViewModel;

            var workFloBL = new WorkflowBL();
            var groups = workFloBL.GetGroups();

            if (!groups.IsCollectionValid())
                return;

            if (ViewModel.AvailableGroup == null)
                ViewModel.AvailableGroup = new ObservableCollection<Group>();

            ViewModel.AvailableGroup.Clear();

            groups.ForEach(ViewModel.AvailableGroup.Add);

            ViewModel.AvailableGroup.Insert(0, new Group { GroupId = 0, GroupName = "Select" });

            ViewModel.SelectedGroup = ViewModel.AvailableGroup[0];

        }

        private void SaveState()
        {
            var workflowBl = new WorkflowBL();
            if (ViewModel.Id == 0)
            {
                workflowBl.SaveState(ViewModel);
                ViewModel.StateName = string.Empty;
                ViewModel.SelectedGroup = ViewModel.AvailableGroup[0];
                ViewModel.StateType = string.Empty;
                AssignAllState();
                ViewModel.OpenMessageBox("State added successfully.", MessageType.Information);
            }
            else if (ViewModel.Id > 0)
            {
                var isUpdated=workflowBl.UpdateState(ViewModel);
                if (isUpdated)
                {
                    ViewModel.OpenMessageBox("State updated successfully.", MessageType.Information);
                    AssignAllState();
                    ResetViewModel();
                }
                else
                    ViewModel.OpenMessageBox("There are no Changes To update.", MessageType.Information);

            }

        }
        private void AssignAllState()
        {

            ViewModel.IsStatesExists = false;

            var workflowBl = new WorkflowBL();
            var states = workflowBl.GetStates();

            if (ViewModel.States == null)
                ViewModel.States = new ObservableCollection<State>();

            ViewModel.States.Clear();

            if (!states.IsCollectionValid())
                return;

            foreach (var state in states)
            {
                state.GroupName = state.Groups.Select(x => x.GroupName).FirstOrDefault();
                ViewModel.States.Add(state);
            }

            ViewModel.IsStatesExists = true;

        }

        public void RemoveState(int id)
        {
            try
            {
                var workflowBL = new WorkflowBL();
                workflowBL.RemoveState(id);

                ViewModel.OpenMessageBox("State removed successfully.");
            }
            catch (BusinessException ex)
            {

                ViewModel.OpenMessageBox(ex.Message.ToString());

            }
            AssignAllState();
        }


        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            ResetViewModel();
        }

        private void ResetViewModel()
        {
            ViewModel.Id = 0;
            ViewModel.SelectedGroup = ViewModel.AvailableGroup.FirstOrDefault();
            ViewModel.StateName = "";
            ViewModel.IsUpdate = false;
            ViewModel.IsStatesExists = ViewModel.States.IsCollectionValid();
            ViewModel.StateType = "";
        }



        public dynamic InitializeViewModel()
        {
            InitializeData();
            AssignAllState();
            return ViewModel;
        }

        public string PageName
        {
            get { return Helper.WFConstant.ManageState; }
        }

        void IMVPageControl.InitializeViewModel()
        {
            InitializeData();
            AssignAllState();
        }
    }
}
