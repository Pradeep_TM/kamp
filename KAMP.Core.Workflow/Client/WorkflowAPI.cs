﻿using KAMP.Core.Repository.WF;
using KAMP.Core.Workflow.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Workflow
{
    using FrameworkComponents;
    using BLL;
    public class WorkflowAPI
    {
        /// <summary>
        /// Method used to assign the cases to user
        /// </summary>
        /// <param name="caseNo">case no for the case</param>
        /// <param name="userId">userid of the user whom we are assgning the case</param>
        /// <param name="CFId"></param>
        /// <param name="moduleTypeId"></param>
        /// <param name="assignValues">id of the thing that we are assigning for example in MV we are assigning the rule</param>
        /// <returns></returns>
        public static void Assign(string caseNo, long userId, long CFId, int moduleTypeId, List<int> assignValues = null)
        {
            if (caseNo.IsNull() || userId == 0 || CFId == 0 || moduleTypeId == 0)
                throw new BusinessException("Information are missing for assigning the case.");

            var workflowBL = new WorkflowBL();
            workflowBL.Assign(caseNo, userId, moduleTypeId, CFId, assignValues);
        }

        /// <summary>
        /// Method used to Reassign the cases to user
        /// </summary>
        /// <param name="caseNo">case no for the case</param>
        /// <param name="userId">userid of the user whom we are assgning the case</param>
        /// <param name="stateId">stateId of the current case</param>
        /// <param name="moduleTypeId">it represents the current module</param>
        /// <returns></returns>
        public static void ReAssign(string caseNo, long userId, int moduleTypeId)
        {
            if (caseNo.IsNull() || userId == 0)
                throw new BusinessException("Information are missing for re-assigning the case.");

            var workfloBL = new WorkflowBL();
            workfloBL.ReAssign(caseNo, userId, moduleTypeId);
        }

        /// <summary>
        /// Method to return the case details for the particuler case number
        /// </summary>
        /// <param name="caseNo">case number</param>
        /// <returns></returns>
        public static CaseDetail GetCaseDetails(string caseNo)
        {
            if(caseNo.IsEmpty())
                throw new BusinessException("Case number is empty.");
            var workflowBl = new WorkflowBL();
           return workflowBl.GetCaseDetails(caseNo);
        }
        /// <summary>
        /// Getting the case details for all the cases
        /// </summary>
        /// <param name="caseNos">List of case number</param>
        /// <returns>List of case details</returns>
        public static List<CaseDetail> GetCaseDetails(List<string> caseNos)
        {
            if (!caseNos.IsCollectionValid())
                return null;
            var workflowBl = new WorkflowBL();
            return workflowBl.GetCaseDetails(caseNos);
        }
        /// <summary>
        /// Open a case when it's in assigned state
        /// </summary>
        /// <param name="caseNo">case number of the case</param>
        public static void OpenCase(string caseNo)
        {
            if (caseNo.IsEmpty())
                throw new BusinessException("Case number is empty."); 
            var workflowBl = new WorkflowBL();
            workflowBl.OpenCase(caseNo);
        }
    }
}
