﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Workflow
{
    public class CaseDetail
    {
        public long AssignmentId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }
        public int StateId { get; set; }
        public string State { get; set; }
        public DateTime? AssignDate { get; set; }
        public string CaseNo { get; set; }
        public bool? IsOpen { get; set; }
        public DateTime? CaseOpenDate { get; set; }
        public DateTime? CaseClosedDate { get; set; }
        public DateTime? LevelOpenDate { get; set; }
        public DateTime? ParkedDate { get; set; }
        public int DaysInQueue { get; set; }
        public string ParkReason { get; set; }
        public long LastAnalystId { get; set; }
        public string LastAnalyst { get; set; }
        public string HighestStatus { get; set; }
    }
}
