﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;
namespace KAMP.Core.Workflow.BLL
{
    public class EmailNotification
    {

        public static Microsoft.Office.Interop.Outlook.Application outlookApp;
        public static Microsoft.Office.Interop.Outlook.NameSpace oNS;
        public static Microsoft.Office.Interop.Outlook.MailItem OutlookMsg;
        public static void SendEmail(string message,string email)
        {
 
            // Create the Outlook application.
            Microsoft.Office.Interop.Outlook.Application outlookApp = new Microsoft.Office.Interop.Outlook.Application();

            // Get the NameSpace and Logon information.
            oNS = outlookApp.GetNamespace("mapi");

            oNS.Logon();
            // Create a new mail item.
            OutlookMsg = (Microsoft.Office.Interop.Outlook.MailItem)outlookApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
            OutlookMsg.BodyFormat = Microsoft.Office.Interop.Outlook.OlBodyFormat.olFormatHTML;
            OutlookMsg.HTMLBody = GetHtml(message);
            OutlookMsg.Subject = "Items Assigned.";
            var set = OutlookMsg.PropertyAccessor;
            //Adding recipients.
            Microsoft.Office.Interop.Outlook.Recipients MailRecipients = (Microsoft.Office.Interop.Outlook.Recipients)OutlookMsg.Recipients;
            MailRecipients.Add(email);
            //MailRecipients.Add("pushpenderdubey1irfan@kpmg.com");
           // OutlookMsg.CC = TxtCc.Text.Trim();
            foreach (var account in oNS.Session.Accounts)
            {
                Microsoft.Office.Interop.Outlook.Account acc = (Microsoft.Office.Interop.Outlook.Account)account;
                var username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\').LastOrDefault();
                if (acc.UserName != null && acc.UserName.ToLower() == username.ToLower())
                {
                    //Using a specified account to send the mail
                    OutlookMsg.SendUsingAccount = acc;
                }
            }

            if (OutlookMsg.SendUsingAccount != null)
            {
                OutlookMsg.Send();
            }

            MailRecipients = null;
            OutlookMsg = null;
            // outlookApp = null;
        }

        private static string GetHtml(string message)
        {
            StringBuilder builder = new StringBuilder();
            List<string> messageLines = message.Replace("\r\n", "$").Split('$').ToList();
            var messages = messageLines.Where(x => x.Length > 0).ToList();
            messages.ForEach(x => builder.AppendLine(string.Format("<p>{0}</p>", x)));
            return builder.ToString();
        }
    }
}
