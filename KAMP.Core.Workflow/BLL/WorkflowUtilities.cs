﻿using KAMP.Core.Repository;
using KAMP.Core.Repository.Models;
using KAMP.Core.Workflow.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.Windows;
namespace KAMP.Core.Workflow.BLL
{
    using Repository.WF;
    using Repository.UM;
    using FrameworkComponents;
    using Common.BLL;
    public class WorkflowUtilities
    {
        private string _currentState;
        public string UpdateWorkflowEntries(long assignmentId, long userId, string toState, string comments, int action = 1, int activityId = 0)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var canSendMail = new CommonBL().GetConfigurationValue("CanSendMail").AsBool();
                var wFDefinitionRepo = wfUOW.GetRepository<WFDefinition>();
                var assignmentRepo = wfUOW.GetRepository<Assignment>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var assignment = assignmentRepo.Items.Include(x => x.AssignedTo).FirstOrDefault(x => x.Id == assignmentId);

                if (assignment.IsNull() || assignment.Status == Helper.Status.End)
                    throw new BusinessException(Helper.Messages.WorkflowAborted);

                var workFlowDef = wFDefinitionRepo.Items.Include(x => x.Steps).FirstOrDefault(x => x.ModuleTypeId == appcontext.Module.Id);

                if (workFlowDef.IsNull())
                    throw new BusinessException(Helper.Messages.WorkflowAborted);
                try
                {
                    string emailTemplate = string.Empty;
                    switch (action)
                    {
                        case (int)WFActions.Promote:
                            UpdateEntryToNextState(workFlowDef, wfUOW, assignment, action, userId, toState, comments, activityId);
                            emailTemplate = Helper.EmailTemplate.PromoteEmailTemplate;
                            break;
                        case (int)WFActions.Demote:
                            UpdateEntryToPreviousState(workFlowDef, wfUOW, assignment, action, userId, toState, comments, activityId);
                            emailTemplate = Helper.EmailTemplate.DemoteEmailTemplate;
                            break;
                    }

                    if (canSendMail)
                        SendNotificationToUser(emailTemplate, _currentState, userId, toState, assignment.CaseNo, wfUOW);

                    assignment.ModifiedBy = appcontext.User.Id;
                    assignment.ModifiedDate = DateTime.Now;
                    wfUOW.SaveChanges();
                    return assignment.Status;
                }
                catch (Exception ex)
                {
                    throw new BusinessException(Helper.Messages.WorkflowAborted);
                }
            }

        }

        private void SendNotificationToUser(string emailTemplate, string _currentState, long userId, string state, string caseNo, WFUnitOfWork wfUOW)
        {
            if (emailTemplate.IsEmpty())
                return;
            var userRepo = wfUOW.GetRepository<User>();
            var user = userRepo.Items.FirstOrDefault(x => x.UserId == userId);
            if (user.IsNull())
                return;
            var appcontext = Application.Current.Properties["Context"] as Context;
            var body = string.Format(emailTemplate, user.UserName, caseNo, _currentState, DateTime.Now, state, appcontext.Module.Name);
            EmailNotification.SendEmail(body, user.Email);
        }

        private void UpdateEntryToPreviousState(WFDefinition workFlowDef, WFUnitOfWork wfUOW, Assignment assignment, int action, long userId, string state, string comments, int activityId)
        {
            var currentWorkFlowStep = workFlowDef.Steps.FirstOrDefault(x => x.State.Trim().ToLower() == assignment.Status.Trim().ToLower());

            if (currentWorkFlowStep.IsNull())
                return;

            var previousSequenceNo = currentWorkFlowStep.SequenceNo - 1;

            assignment.NextState = assignment.Status;
            assignment.Status = AssignNextState(workFlowDef.Steps, previousSequenceNo);

            var previousWorkflowStep = workFlowDef.Steps.FirstOrDefault(x => x.SequenceNo == previousSequenceNo);
            var previousState = previousWorkflowStep != null ? previousWorkflowStep.State : Helper.Status.Assigned;

            UpdateAssignmentToPreviousEntry(assignment, action, state, userId, comments, activityId);

        }



        private void UpdateEntryToNextState(WFDefinition workFlowDef, WFUnitOfWork wfUOW, Assignment assignment, int action, long userId, string toState, string comments, int activityId)
        {
            var appcontext = Application.Current.Properties["Context"] as Context;
            if (assignment.NextState == Helper.Status.End)
            {
                assignment.Status = assignment.NextState;
                assignment.NextState = string.Empty;
                var assignmentTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus == Helper.Status.InProgess);

                if (assignmentTo.IsNotNull())
                { assignmentTo.WorkflowStatus = Helper.Status.Completed; assignmentTo.Comments = assignmentTo.Comments + string.Format("\r\n Finalize Comment:{0} \r\n", comments); _currentState = assignmentTo.State; assignmentTo.ModifiedBy = appcontext.User.Id; assignmentTo.ModifiedDate = DateTime.Now; }

                return;
            }

            var workFlowSteps = workFlowDef.Steps.FirstOrDefault(x => x.State == assignment.NextState);


            if (workFlowSteps.IsNull())
                return;

            var nextSequenceNo = workFlowSteps.SequenceNo;

            assignment.Status = assignment.NextState;
            assignment.NextState = AssignNextState(workFlowDef.Steps, ++nextSequenceNo);

            UpdateAssignmentToNextEntry(toState, assignment, action, userId, comments, activityId);

        }

        private void UpdateAssignmentToNextEntry(string nextState, Assignment assignment, int action, long userId, string comments, int activityId)
        {
            var appcontext = Application.Current.Properties["Context"] as Context;
            var assignmentTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus == Helper.Status.InProgess);

            if (assignmentTo.IsNotNull())
            { assignmentTo.WorkflowStatus = Helper.Status.Completed; assignmentTo.Comments = assignmentTo.Comments + string.Format("\r\n Promote Comment:{0} \r\n", comments); _currentState = assignmentTo.State; assignmentTo.ModifiedBy = appcontext.User.Id; assignmentTo.ModifiedDate = DateTime.Now; }

            var seq = assignmentTo.Sequence + 1;
            if (userId > 0)
                assignment.AssignedTo.Add(new AssignedTo { AssignmentId = assignment.Id, UserId = userId, WorkflowStatus = Helper.Status.InProgess, State = nextState, StepName = assignment.Status, StepSequenceNo = GetSequenceNo(assignment.Status), Sequence = seq, ActionId = action, ActivityId = activityId, CreatedBy = appcontext.User.Id, CreatedDate = DateTime.Now, ModifiedBy = appcontext.User.Id, ModifiedDate = DateTime.Now,FromState=_currentState });
        }


        private void UpdateAssignmentToPreviousEntry(Assignment assignment, int action, string previousState, long userId, string comments, int activityId)
        {
            var appcontext = Application.Current.Properties["Context"] as Context;
            var assignmentTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus == Helper.Status.InProgess);

            if (assignmentTo.IsNotNull())
            { assignmentTo.WorkflowStatus = Helper.Status.Completed; assignmentTo.Comments = assignmentTo.Comments + string.Format("\r\n Demote Comment:{0} \r\n", comments); _currentState = assignmentTo.State; assignmentTo.ModifiedBy = appcontext.User.Id; assignmentTo.ModifiedDate = DateTime.Now; }

            var seq = assignmentTo.Sequence + 1;
            if (userId > 0)
                assignment.AssignedTo.Add(new AssignedTo { AssignmentId = assignment.Id, UserId = userId, WorkflowStatus = Helper.Status.InProgess, State = previousState, StepName = assignment.Status, StepSequenceNo = GetSequenceNo(assignment.Status), Sequence = seq, ActionId = action, ActivityId = activityId, CreatedBy = appcontext.User.Id, CreatedDate = DateTime.Now, ModifiedBy = appcontext.User.Id, ModifiedDate = DateTime.Now, FromState = _currentState });
        }

        private string AssignNextState(ICollection<Step> steps, int nextSequenceNo)
        {
            if (nextSequenceNo == 0)
                return Helper.Status.Assigned;

            if (!steps.IsCollectionValid())
                return string.Empty;

            var state = steps.FirstOrDefault(x => x.SequenceNo == nextSequenceNo);

            return state != null ? state.State : Helper.Status.End;

        }

        public string AssignCurrentState(long asignmentId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wfUOW.GetRepository<Assignment>();
                var assignment = assignmentRepo.Items.FirstOrDefault(x => x.Id == asignmentId);
                var currentState = assignment != null ? assignment.Status : string.Empty;
                return currentState;
            }
        }

        private int GetSequenceNo(string stepName)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stepRepo = wfUOW.GetRepository<Step>();

                if (stepName.Equals(Helper.Status.Assigned))
                    return 0;

                return stepRepo.Items.Where(x => x.StepName.Trim().ToLower().Equals(stepName.Trim().ToLower())).
                    Select(x => x.SequenceNo).FirstOrDefault();
            }
        }
    }
}
