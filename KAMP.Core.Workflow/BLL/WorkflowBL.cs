﻿using KAMP.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using System.Transactions;
namespace KAMP.Core.Workflow.BLL
{
    using Helpers;
    using Repository.WF;
    using Repository.UM;
    using FrameworkComponents;
    using System.Windows;
    using Common.BLL;
    internal class WorkflowBL
    {

        /// <summary>
        /// Used to get all states available in Database
        /// </summary>
        /// <returns>states List</returns>
        public List<State> GetStates()
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var stateRepo = wfUOW.GetRepository<State>();
                return stateRepo.Items.Include(x => x.Groups).Include(x => x.StateMetadata).Where(x => x.ModuleTypeId == appcontext.Module.Id).OrderBy(x => x.StateName).ToList();
            }
        }

        /// <summary>
        /// Return the statename based on stateId
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns>StateName</returns>
        public string GetStateName(int stateId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stateRepo = wfUOW.GetRepository<State>();
                return (from state in stateRepo.Items
                        where state.Id == stateId
                        select state.StateName).FirstOrDefault();
            }

        }
        /// <summary>
        /// Return the stateId based on StateName
        /// </summary>
        /// <param name="stateName"></param>
        /// <returns>StateId</returns>
        public int GetStateId(string stateName)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var stateRepo = wfUOW.GetRepository<State>();
                return (from state in stateRepo.Items
                        where state.StateName.Trim().ToLower() == stateName.Trim().ToLower() && state.ModuleTypeId == appcontext.Module.Id
                        select state.Id).FirstOrDefault();
            }

        }


        /// <summary>
        /// Get All groups exists in the database
        /// </summary>
        /// <returns></returns>
        public List<Group> GetGroups()
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var groupRepo = wfUOW.GetRepository<Group>();
                return groupRepo.Items.Where(x => x.IsActive).ToList();
            }
        }

        /// <summary>
        /// this method is used to get all the groups based on the groupids
        /// </summary>
        /// <param name="groupIds"></param>
        /// <param name="wfUOW">Unit of work object to get the repository</param>
        /// <returns>List of group types</returns>
        public List<Group> GetGroupsbyIds(IEnumerable<long> groupIds, WFUnitOfWork wfUOW)
        {
            var groupRepo = wfUOW.GetRepository<Group>();
            return groupRepo.Items.Where(x => groupIds.Contains(x.GroupId)).ToList();
        }

        /// <summary>
        /// This method is used to save the state
        /// </summary>
        /// <param name="ViewModel"></param>
        internal void SaveState(ViewModels.WorkflowStateVm ViewModel)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stateRepo = wfUOW.GetRepository<State>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var stateTypeId = GetStateTypeId(ViewModel.StateType, appcontext, wfUOW);
                var state = new State()
                {
                    StateName = ViewModel.StateName,
                    StateTypeId = stateTypeId,
                    CreatedBy = appcontext.User.Id,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = appcontext.User.Id,
                    ModifiedDate = DateTime.Now,
                    ModuleTypeId = appcontext.Module.Id
                };
                state.Groups.Add(GetGroupById(ViewModel.SelectedGroup.GroupId, wfUOW));
                stateRepo.Insert(state);
                wfUOW.SaveChanges();
            }
        }


        /// <summary>
        /// Method to remove the state from the daatabse
        /// </summary>
        /// <param name="id">Id of the state that is going to be remove from the database</param>
        internal void RemoveState(int id)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stateRepo = wfUOW.GetRepository<State>();
                var state = stateRepo.Items.FirstOrDefault(x => x.Id == id);
                if (state.IsNull())
                    throw new BusinessException("State does not exists in db.");
                if (IsStatePartOfWorkflow(wfUOW, id))
                    throw new BusinessException("This state cannot be removed because it's a part of existing workflow.");
                stateRepo.Delete(state);
                wfUOW.SaveChanges();
            }
        }



        /// <summary>
        /// Method to update the state
        /// </summary>
        /// <param name="ViewModel">State View model</param>
        internal bool UpdateState(ViewModels.WorkflowStateVm ViewModel)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stateRepo = wfUOW.GetRepository<State>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var dbState = stateRepo.Items.Include(x => x.Groups).Include(x => x.StateMetadata).FirstOrDefault(x => x.Id == ViewModel.Id);
                if (dbState.IsNull())
                    throw new BusinessException("Unable to update the state.");

                //if (IsStatePartOfWorkflow(wfUOW, ViewModel.Id))
                //    throw new BusinessException("This state cannot be updated because it's a part of existing workflow.");

                bool isUpdated = false;

                if (ViewModel.StateName.ToLower() != dbState.StateName.ToLower())
                {
                    dbState.StateName = ViewModel.StateName;
                    isUpdated = true;
                }
                var group = dbState.Groups.FirstOrDefault(x => x.GroupId == ViewModel.SelectedGroup.GroupId);
                if (group.IsNull())
                {
                    dbState.Groups.Clear();
                    var newGroup = GetGroupById(ViewModel.SelectedGroup.GroupId, wfUOW);
                    if (newGroup.IsNotNull())
                    { dbState.Groups.Add(newGroup); isUpdated = true; }
                }
                var stateTypeId = GetStateTypeId(ViewModel.StateType, appcontext, wfUOW);
                if (dbState.StateMetadata.Id != stateTypeId)
                {
                    dbState.StateTypeId = stateTypeId;
                    isUpdated = true;
                }
                if (isUpdated)
                {
                    dbState.ModifiedBy = appcontext.User.Id;
                    dbState.ModifiedDate = DateTime.Now;
                    stateRepo.Update(dbState);
                    wfUOW.SaveChanges();
                }
                return isUpdated;
            }

        }


        /// <summary>
        /// Getting all the state type(metadata for state)
        /// </summary>
        /// <param name="_stateType"></param>
        /// <returns></returns>
        internal List<string> GetStateTypes(string stateType)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stateMetadataRepo = wfUOW.GetRepository<StateMetadata>();
                return stateMetadataRepo.Items.Where(x => x.StateType.ToLower().StartsWith(stateType.Trim().ToLower())).Select(x => x.StateType).Take(10).ToList();
            }
        }
        /// <summary>
        /// This method will return the id of the stateType Selected while creating the state.
        /// and if the stateType is not in the database then it will create the statetype and will retunr the id
        /// of created statetype
        /// </summary>
        /// <param name="p"></param>
        /// <param name="appcontext"></param>
        /// <returns></returns>
        private int GetStateTypeId(string stateType, Context appcontext, WFUnitOfWork wfUOW)
        {
            var stateMetadataRepo = wfUOW.GetRepository<StateMetadata>();
            var stateMetadata = stateMetadataRepo.Items.FirstOrDefault(x => x.StateType.Trim().ToLower().Equals(stateType.Trim().ToLower()));
            if (stateMetadata.IsNotNull())
                return stateMetadata.Id;
            else
            {
                var stateMetaData = new StateMetadata
                {
                    StateType = stateType,
                    CreatedBy = appcontext.User.Id,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = appcontext.User.Id,
                    ModifiedDate = DateTime.Now
                };
                stateMetadataRepo.Insert(stateMetaData);
                wfUOW.SaveChanges();
                return stateMetaData.Id;
            }
        }


        /// <summary>
        /// Get Assignment based on case no
        /// </summary>
        /// <param name="_applicationContext"></param>
        /// <returns></returns>
        internal Assignment GetAssignment(string CaseNo, long UserId, int moduleTypeId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wfUOW.GetRepository<Assignment>();
                return assignmentRepo.Items.Include(x => x.AssignedTo).Include(x => x.WFTracking)
                  .FirstOrDefault(x => x.CaseNo.ToLower() == CaseNo.ToLower() && x.Status.ToLower() != Helper.Status.End.ToLower() && x.WFTracking.Any(wfTrack => wfTrack.ModuleTypeId == moduleTypeId) &&
                  x.AssignedTo.Any(assignTo => assignTo.WorkflowStatus.Trim().ToLower() ==
                  Helper.Status.InProgess.Trim().ToLower()));
            }
        }
        /// <summary>
        /// Get all assignment which are in progress.
        /// </summary>
        /// <param name="moduleTypeId">module type Id</param>
        /// <returns></returns>
        internal List<Assignment> GetAssignment(int moduleTypeId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wfUOW.GetRepository<Assignment>();
                return assignmentRepo.Items.Include(x => x.AssignedTo).Include(x => x.WFTracking)
                  .Where(x => x.WFTracking.Any(wfTrack => wfTrack.ModuleTypeId == moduleTypeId)).ToList();
            }
        }

        internal List<AssignedTo> GetAssignTo(long selectedValue, string stepName, int moduleTypeId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var assignTo = wfUOW.GetRepository<AssignedTo>();
                var users = wfUOW.GetRepository<User>();

                return assignTo.Items.Include(x => x.Assignment).Where(x => x.AssignmentId == selectedValue
                    && x.StepName.Trim().ToLower().Equals(stepName.Trim().ToLower()) && x.Assignment.WFTracking.
                    Any(z => z.ModuleTypeId == moduleTypeId))
                    .ToList();

                //return assignTo.Items.Include(x => x.Assignment).Where(x => x.AssignmentId == selectedValue)
                //    .ToList<AssignedTo>()
                //    .Where(u => u.StepName == stepName && u.Assignment.WFTracking.Any(x => x.ModuleTypeId == moduleTypeId))
                //    .Join(users.Items, u => u.UserId, z => z.UserId, (u, z) => u
                //   ).ToList();
            }
        }

        /// <summary>
        /// Get Workflow Step
        /// </summary>
        /// <param name="state">stepname of the workflow step</param>
        /// <returns>step object</returns>
        internal Step GetWorkflowStep(string state)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stepRepo = wfUOW.GetRepository<Step>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                return stepRepo.Items.Include(x => x.Activity).Include(x => x.WFDefinition).FirstOrDefault(x => x.StepName == state && x.WFDefinition.ModuleTypeId == appcontext.Module.Id);
            }
        }

        //public void FinalizeWorkflow(WFDefinition workflowDefinition)
        //{
        //    ConfigureWorkflow(workflowDefinition);
        //}


        //private void ConfigureWorkflow(WFDefinition workflowDefinition)
        //{
        //    if (workflowDefinition == null)
        //        return;
        //    var stateMachine = StateConfiguration.ConfigureStates(workflowDefinition.Steps);

        //    if (stateMachine == null && stateMachine.States.Count == 0)
        //        return;

        //    WorkflowConfiguration.stateMachineworkflow.Name = workflowDefinition.WFName;

        //    WorkflowConfiguration.ConfigureWorkflow(stateMachine);

        //    var serealizedWorkflow = WorkflowConfiguration.SerializedWorkFlow();
        //    workflowDefinition.WorkFlowDefinition = serealizedWorkflow;
        //}

        /// <summary>
        /// Get all user based on the userId
        /// </summary>
        /// <param name="userIdsInGroup">List of userId</param>
        /// <returns>List of usertype</returns>
        internal List<User> GetUsers(List<long> userIdsInGroup)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var userRepo = wfUOW.GetRepository<User>();
                return userRepo.Items.Where(x => userIdsInGroup.Contains(x.UserId)).ToList();
            }
        }

        internal string GetUserName(long userId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var userRepo = wfUOW.GetRepository<User>();
                return userRepo.Items.Where(x => x.UserId == userId).Select(x => x.UserName).FirstOrDefault();
            }
        }

        /// <summary>
        /// Get Workflow based on moduleTypeId
        /// </summary>
        /// <param name="moduleTypeId">Identifies the current module</param>
        /// <returns>Workflow</returns>
        internal WFDefinition GetWorkflowDef(int moduleTypeId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var wfDefinitionRepo = wfUOW.GetRepository<WFDefinition>();
                return wfDefinitionRepo.Items.Include(x => x.Steps.Select(y => y.Activity.Select(z => z.WFRouting))).FirstOrDefault(x => x.ModuleTypeId == moduleTypeId);
            }
        }

        /// <summary>
        ///Methos to save the workflow 
        /// </summary>
        /// <param name="workFlowDefinition">WFDefenition object</param>
        internal void SaveWorkflow(WFDefinition workFlowDefinition)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var wfDefinitionRepo = wfUOW.GetRepository<WFDefinition>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                if (appcontext.IsNull())
                    throw new BusinessException("Unable to save workflow.");

                var IsworkflowExists = IsWorkflowExists(appcontext.Module.Id, wfDefinitionRepo);
                if (IsworkflowExists)
                    throw new BusinessException("Workflow is already exists for this module.");

                wfDefinitionRepo.Insert(workFlowDefinition);
                wfUOW.SaveChanges();
            }
        }

        /// <summary>
        /// Method to update the workflow
        /// </summary>
        /// <param name="viewWorkflow"></param>
        internal void UpdateWorkflow(WFDefinition viewWorkflow)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var wfDefinitionRepo = wfUOW.GetRepository<WFDefinition>();
                var stepRepo = wfUOW.GetRepository<Step>();
                var activityRepo = wfUOW.GetRepository<Activity>();
                var routingRepo = wfUOW.GetRepository<WFRouting>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var dbWorkflow = wfDefinitionRepo.Items.Include(x => x.Steps.Select(y => y.Activity)).FirstOrDefault(x => x.WFId == viewWorkflow.WFId);
                if (dbWorkflow.IsNull())
                    return;

               // IsWorkflowInProcess(wfUOW);

                dbWorkflow.WFName = viewWorkflow.WFName;
                dbWorkflow.WorkFlowDefinition = viewWorkflow.WorkFlowDefinition;
                dbWorkflow.ModifiedBy = appcontext.User.Id;
                dbWorkflow.ModifiedDate = DateTime.Now;

                //var viewStepsIds = viewWorkflow.Steps.Select(x => x.Id).ToList();
                //var dbStepsNotInView = dbWorkflow.Steps.Where(x => !viewStepsIds.Contains(x.Id)).ToList();

                //if (dbStepsNotInView.IsCollectionValid())
                //    RemoveWFSteps(wfUOW, dbStepsNotInView);

                foreach (var step in viewWorkflow.Steps)
                {
                    if (step.Id > 0)
                    {
                        var dbStep = dbWorkflow.Steps.FirstOrDefault(x => x.Id == step.Id);
                        if (dbStep.IsNotNull())
                        {
                            dbStep.SequenceNo = step.SequenceNo;
                            dbStep.State = step.State;
                            dbStep.StepName = step.State;
                            dbStep.DisplayName = step.DisplayName;
                            dbStep.ModifiedBy = appcontext.User.Id;
                            dbStep.ModifiedDate = DateTime.Now;
                            var viewActivityIds = step.Activity.Select(x => x.Id).ToList();
                            var dbActivityNotInView = dbStep.Activity.Where(x => !viewActivityIds.Contains(x.Id)).ToList();

                            if (dbActivityNotInView.IsCollectionValid())
                            {
                                IsActivityPartOfAssignment(wfUOW,dbActivityNotInView.Select(x => x.Id).ToList());

                                dbActivityNotInView.ForEach(x =>
                                {
                                    var route = routingRepo.Items.FirstOrDefault(rout => rout.ToActivityId == x.Id);

                                    if (route.IsNotNull())
                                        routingRepo.Delete(route);

                                    activityRepo.Delete(x);

                                });
                            }

                            foreach (var activity in step.Activity)
                            {
                                if (activity.Id > 0)
                                {
                                    var dbActivity = dbStep.Activity.FirstOrDefault(x => x.Id == activity.Id);
                                    if (dbActivity.IsNotNull())
                                    {
                                        dbActivity.ToStateId = activity.ToStateId;
                                        dbActivity.MailTemplate = activity.MailTemplate;
                                        dbActivity.ModifiedBy = appcontext.User.Id;
                                        dbActivity.ModifiedDate = DateTime.Now;
                                    }
                                }
                                else if (activity.Id == 0)
                                {
                                    activity.StepId = step.Id;
                                    activityRepo.Insert(activity);
                                }

                            }
                        }
                    }
                    else if (step.Id == 0)
                    {
                        step.WFId = dbWorkflow.WFId;
                        stepRepo.Insert(step);
                    }


                }
                using (var scope = new TransactionScope())
                {
                    try
                    {
                        wfUOW.SaveChanges();
                        scope.Complete();
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        throw new BusinessException("Unable To update the workflow.");
                    }
                }
            }
        }

        private void IsActivityPartOfAssignment(WFUnitOfWork wfUOW,List<int> activityId)
        {
            var assignTo = wfUOW.GetRepository<AssignedTo>();
            var states= assignTo.Items.Where(x => x.WorkflowStatus.Equals(Helper.Status.InProgess) && 
                activityId.Contains(x.ActivityId)).Select(x=>x.State);

            if (states.IsCollectionValid())
                throw new BusinessException(string.Format("Please select {0} states because it's been used by other cases.", string.Join(",", states)));

        }

        /// <summary>
        /// Method to remove the workflow step
        /// </summary>
        /// <param name="wfUOW">Unit of work to get the repositories</param>
        /// <param name="dbStepsNotInView">Steps from database which is not in view</param>
        private void RemoveWFSteps(WFUnitOfWork wfUOW, List<Step> dbStepsNotInView)
        {
            var activityRepo = wfUOW.GetRepository<Activity>();
            var stepRepo = wfUOW.GetRepository<Step>();
            var routingRepo = wfUOW.GetRepository<WFRouting>();
            dbStepsNotInView.ForEach(step =>
            {
                if (step.Activity.IsCollectionValid())
                {
                    var activity = step.Activity.ToList();
                    activity.ForEach(x =>
                    {
                        var route = routingRepo.Items.FirstOrDefault(rout => rout.ToActivityId == x.Id);

                        if (route.IsNotNull())
                            routingRepo.Delete(route);

                        activityRepo.Delete(x);

                    });
                }
                stepRepo.Delete(step);
            });
        }

        /// <summary>
        /// Check whether the workflow is in process
        /// </summary>
        /// <param name="wfUOW">Unit of work to get all type of repository</param>
        private void IsWorkflowInProcess(WFUnitOfWork wfUOW)
        {
            //Todo get the objecttypeid from the application context object
            var wfTrackingRepo = wfUOW.GetRepository<WFTracking>();
            var assignedTooRepo = wfUOW.GetRepository<AssignedTo>();
            var appcontext = Application.Current.Properties["Context"] as Context;
            if (appcontext.IsNull())
                throw new BusinessException("Error while updating the process.");

            var isInProcess = (from wf in wfTrackingRepo.Items
                               join assignTo in assignedTooRepo.Items on wf.AssignmentId equals assignTo.AssignmentId
                               where wf.ModuleTypeId == appcontext.Module.Id && assignTo.WorkflowStatus == Helpers.Helper.Status.InProgess
                               select assignTo).ToList();

            if (isInProcess.IsCollectionValid())
                throw new BusinessException("Cannot update the workflow because it's in process.");
        }


        /// <summary>
        /// Get group based on groupid
        /// </summary>
        /// <param name="id">id of the group</param>
        /// <param name="wfUOW">Unit of work to get all types of repositories</param>
        /// <returns></returns>
        private Group GetGroupById(long id, WFUnitOfWork wfUOW)
        {
            var groupRepo = wfUOW.GetRepository<Group>();
            return groupRepo.Items.FirstOrDefault(x => x.GroupId == id);
        }

        /// <summary>
        /// Get All State based on stateIds
        /// </summary>
        /// <param name="stateIds"><List of stateIds/param>
        /// <returns>List of states</returns>
        internal List<State> GetStates(List<int> stateIds)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stateRepo = wfUOW.GetRepository<State>();
                return stateRepo.Items.Where(x => stateIds.Contains(x.Id)).ToList();
            }
        }

        /// <summary>
        /// Get User by StateId
        /// </summary>
        /// <param name="stateId">stateId</param>
        /// <returns>List of users</returns>
        internal List<User> GetUsersByStateId(int stateId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {

                var appcontext = Application.Current.Properties["Context"] as Context;
                var userRepo = wfUOW.GetRepository<User>();
                var users = userRepo.Items.Include(user => user.UserInGroup.Select(x => x.Group)).Where(x => x.IsActive && x.UserInGroup.Any(gm => gm.IsActive && gm.ModuleTypeId == appcontext.Module.Id && gm.Group.States.Any(s => s.ModuleTypeId == appcontext.Module.Id && s.Id == stateId))).ToList();
                return users;


            }
        }
        /// <summary>
        /// used to get all step of workflow.
        /// </summary>
        /// <param name="moduleId">module id to get the workflow</param>
        /// <returns></returns>
        internal List<Step> GetSteps(int moduleId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var stepRepo = wfUOW.GetRepository<Step>();
                return stepRepo.Items.Include(x => x.WFDefinition).Include(ac => ac.Activity).
                    Where(x => x.WFDefinition.ModuleTypeId == moduleId).ToList();

            }
        }

        /// <summary>
        /// Method used to assign the cases to user
        /// </summary>
        /// <param name="caseNo">case no for the case</param>
        /// <param name="userId">userid of the user whom we are assgning the case</param>
        /// <param name="CFId"></param>
        /// <param name="moduleTypeId"></param>
        /// <param name="assignValues">id of the object that we are assigning for example in MV we are assigning the rules</param>
        /// <returns></returns>
        internal void Assign(string caseNo, long userId, int moduleTypeId, long CFId = 0, List<int> assignValues = null)
        {
            using (var wFUOW = new WFUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var assignmentRepo = wFUOW.GetRepository<Assignment>();
                var wfDefinition = wFUOW.GetRepository<WFDefinition>();
                var canSendMail = new CommonBL().GetConfigurationValue("CanSendMail").AsBool();
                var IsworkflowExists = IsWorkflowExists(moduleTypeId, wfDefinition);
                if (!IsworkflowExists)
                    throw new BusinessException("Workflow does not exists for this module.Please create a workflow then assign cases.");

                IsCaseAlreadyAssign(caseNo, moduleTypeId);
                var assignment = new Assignment()
                {
                    CaseNo = caseNo,
                    Status = Helper.Status.Assigned,
                    AssignedTo = new List<AssignedTo>() { new AssignedTo { UserId = userId, WorkflowStatus = Helper.Status.InProgess, State = Helper.Status.Assigned, CreatedBy = appcontext.User.Id, CreatedDate = DateTime.Now, ModifiedBy = appcontext.User.Id, ModifiedDate = DateTime.Now } },
                    WFTracking = AssignTask(CFId, assignValues, moduleTypeId),
                    CreatedBy = appcontext.User.Id,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = appcontext.User.Id,
                    ModifiedDate = DateTime.Now

                };

                //To DO object type id is hardcoded here we need to get it from the UI
                assignment.NextState = AssignNextState(wfDefinition.Items.Include(x => x.Steps).FirstOrDefault(x => x.ModuleTypeId == moduleTypeId));
                assignmentRepo.Insert(assignment);
                using (var scope = new TransactionScope())
                {
                    try
                    {
                        if (canSendMail)
                            SendNotificationToUser(Helper.EmailTemplate.AssignedEmailTemplate, Helper.Status.Assigned, userId, caseNo, wFUOW);

                        wFUOW.SaveChanges();
                        scope.Complete();
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        throw new BusinessException(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// This method is used to check whethre the case is already assigned to user or not.
        /// </summary>
        /// <param name="caseNo"></param>
        /// <param name="moduleTypeId"></param>
        private void IsCaseAlreadyAssign(string caseNo, int moduleTypeId)
        {
            using (var wFUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wFUOW.GetRepository<Assignment>();
                var assignment = assignmentRepo.Items.Include(x => x.AssignedTo).Include(x => x.WFTracking).Where
                                        (assign => assign.WFTracking.Any(wf => wf.ModuleTypeId == moduleTypeId) &&
                                         assign.AssignedTo.Any(assignTo => assignTo.WorkflowStatus.ToLower() == Helper.Status.InProgess.ToLower())
                                         && assign.CaseNo.ToLower() == caseNo.ToLower() && assign.Status.ToLower() != Helper.Status.End.ToLower()).ToList();

                if (assignment.IsCollectionValid())
                    throw new BusinessException(string.Format("CaseNumber={0} is already assigned.", caseNo));
            }

        }

        private static ICollection<WFTracking> AssignTask(long CFId, List<int> assignValues, int moduleTypeId)
        {
            if (!assignValues.IsCollectionValid())
                assignValues = new List<int>() { 0 };

            var appcontext = Application.Current.Properties["Context"] as Context;
            var assignmentList = new List<WFTracking>();

            foreach (var ruleId in assignValues)
                assignmentList.Add(new WFTracking { CFId = CFId, ModuleTypeId = moduleTypeId, ObjectId = ruleId, CreatedBy = appcontext.User.Id, CreatedDate = DateTime.Now, ModifiedBy = appcontext.User.Id, ModifiedDate = DateTime.Now });

            return assignmentList;
        }

        private static string AssignNextState(WFDefinition wF_Definition)
        {
            var step = wF_Definition.Steps.FirstOrDefault(x => x.SequenceNo == 1);
            if (step.IsNull())
                throw new BusinessException("There is no steps in the workflow for this module.Please add atleast one step.");
            return step.State;
        }

        /// <summary>
        /// Method used to Reassign the cases to user
        /// </summary>
        /// <param name="caseNo">case no for the case</param>
        /// <param name="userId">userid of the user whom we are assgning the case</param>
        /// <param name="stateId">stateId of the current case</param>
        /// <param name="moduleTypeId">it represents the current module</param>
        /// <returns></returns>
        internal void ReAssign(string caseNo, long userId, int moduleTypeId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var wfDefinition = wfUOW.GetRepository<WFDefinition>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var canSendMail = new CommonBL().GetConfigurationValue("CanSendMail").AsBool();

                var IsworkflowExists = IsWorkflowExists(moduleTypeId, wfDefinition);
                if (!IsworkflowExists)
                    throw new BusinessException("Workflow does not exists for this module.Please create a workflow then assign cases.");

                //IsCaseAlreadyAssignToUser(caseNo, moduleTypeId, userId);

                //var userRepo = wfUOW.GetRepository<User>();
                //var isUserExist = userRepo.Items.Include(user => user.UserInGroup.Select(x => x.Group)).Any(x => x.UserId == userId && x.UserInGroup.Any(gm => gm.Group.States.Any(s => s.Id == stateId)));

                //if (!isUserExist)
                //    throw new BusinessException("User does not exists in the given state.");

                var assignmentRepo = wfUOW.GetRepository<Assignment>();
                //var assignments = assignmentRepo.Items.Include(x => x.WFTracking).Include(y => y.AssignedTo).
                //    Where(x => x.WFTracking.Any(wft => wft.ModuleTypeId == moduleTypeId)).ToList();

                var assignment = assignmentRepo.Items.Include(x => x.WFTracking).Include(y => y.AssignedTo).
                  FirstOrDefault(x => x.WFTracking.Any(wft => wft.ModuleTypeId == moduleTypeId) && x.Status.ToLower() != Helper.Status.End.ToLower()
                  && x.CaseNo.ToLower().Equals(caseNo.ToLower()));

                //if (!assignments.IsCollectionValid())
                //    throw new BusinessException("No assignment exists for this module type.");

                //var assignment = assignments.FirstOrDefault(x => x.CaseNo.ToLower().Equals(caseNo.ToLower()));

                if (assignment.IsNull())
                    throw new BusinessException("No assignment exists for this case number.");

                var assignTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus.ToLower() == Helper.Status.InProgess.ToLower());

                if (assignTo.IsNull())
                    throw new BusinessException("No assigment found for this case number.");

                //To Do Get the cuurent user id from the application context object and change the userId int to long
                assignTo.UserId = userId;
                assignTo.ModifiedBy = appcontext.User.Id;
                assignTo.ModifiedDate = DateTime.Now;
                using (var scope = new TransactionScope())
                {
                    try
                    {
                        if (canSendMail)
                            SendNotificationToUser(Helper.EmailTemplate.AssignedEmailTemplate, assignment.Status, userId, caseNo, wfUOW);

                        wfUOW.SaveChanges();
                        scope.Complete();
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        throw new BusinessException(ex.Message);
                    }
                }
            }

        }
        ///// <summary>
        ///// check whether case is already assigned to same user and whether the case is in progress.
        ///// </summary>
        ///// <param name="caseNo"></param>
        ///// <param name="moduleTypeId"></param>
        ///// <param name="userId"></param>
        //private void IsCaseAlreadyAssignToUser(string caseNo, int moduleTypeId, long userId)
        //{
        //    using (var wFUOW = new WFUnitOfWork())
        //    {
        //        var assignmentRepo = wFUOW.GetRepository<Assignment>();

        //        var assignment = assignmentRepo.Items.Include(x => x.AssignedTo).Include(x => x.WFTracking).Where
        //                  (assign => assign.WFTracking.Any(wf => wf.ModuleTypeId == moduleTypeId) &&
        //                   assign.AssignedTo.Any(assignTo => assignTo.UserId == userId
        //                       && assignTo.WorkflowStatus.ToLower() == Helper.Status.InProgess.ToLower())
        //                   && assign.CaseNo.ToLower() == caseNo.ToLower() && assign.Status.ToLower() != Helper.Status.End.ToLower()).ToList();
        //        if (assignment.IsCollectionValid())
        //            throw new BusinessException("User is already assign with same Case Number.");
        //    }
        //}
        /// <summary>
        /// Send notification to user while assigning the cases.
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <param name="_currentState"></param>
        /// <param name="userId"></param>
        /// <param name="caseNo"></param>
        /// <param name="wfUOW"></param>
        private void SendNotificationToUser(string emailTemplate, string _currentState, long userId, string caseNo, WFUnitOfWork wfUOW)
        {
            var userRepo = wfUOW.GetRepository<User>();
            var user = userRepo.Items.FirstOrDefault(x => x.UserId == userId);
            if (user.IsNull())
                return;
            var appcontext = Application.Current.Properties["Context"] as Context;
            var body = string.Format(emailTemplate, user.UserName, caseNo, _currentState, DateTime.Now, appcontext.Module.Name);
            EmailNotification.SendEmail(body, user.Email);
        }
        /// <summary>
        /// Check whether the workflow for the current module is exists in db.
        /// </summary>
        /// <param name="moduleTypeId"></param>
        /// <param name="wfDefinition"></param>
        private bool IsWorkflowExists(int moduleTypeId, Repository<WFDefinition> wfDefinition)
        {
            var IsworkflowExists = wfDefinition.Items.Any(x => x.ModuleTypeId == moduleTypeId);
            return IsworkflowExists;
        }

        /// <summary>
        /// Checking weather the state is belongs to any workflow steps 
        /// </summary>
        /// <param name="wfUOW"></param>
        private bool IsStatePartOfWorkflow(WFUnitOfWork wfUOW, int stateId)
        {
            var activity = wfUOW.GetRepository<Activity>();
            var isPartOfWorkflow = activity.Items.Any(x => x.ToStateId == stateId);
            return isPartOfWorkflow;
        }
        /// <summary>
        /// This method is used to remove the workflow step from the workflow
        /// </summary>
        /// <param name="stepId">step Id of the workflow</param>
        internal void RemoveWfStep(int stepId)
        {
            using (var WfUOW = new WFUnitOfWork())
            {
                var stepRepo = WfUOW.GetRepository<Step>();
                var activityRepo = WfUOW.GetRepository<Activity>();
                var routingRepo = WfUOW.GetRepository<WFRouting>();
                var step = stepRepo.Items.Include(x => x.Activity).FirstOrDefault(x => x.Id == stepId);
                if (step.IsNull())
                    return;
                IsWorkflowInProcess(WfUOW);
                var wfId = step.WFId;
                if (step.Activity.IsCollectionValid())
                {
                    var activity = step.Activity.ToList();
                    activity.ForEach(x =>
                    {
                        var route = routingRepo.Items.FirstOrDefault(rout => rout.ToActivityId == x.Id);

                        if (route.IsNotNull())
                            routingRepo.Delete(route);

                        activityRepo.Delete(x);
                    });
                }
                stepRepo.Delete(step);
                WfUOW.SaveChanges();

                var sequenceNo = 1;
                var steps = stepRepo.Items.Where(x => x.WFId == wfId).ToList();
                if (steps.IsCollectionValid())
                {
                    steps.ForEach((x) => { x.SequenceNo = sequenceNo++; });
                    WfUOW.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get Workflow Step
        /// </summary>
        /// <param name="state">sequence no of the workflow step</param>
        /// <returns>step object</returns>
        internal Step GetWorkflowStep(int sequence)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var stepRepo = wfUOW.GetRepository<Step>();
                return stepRepo.Items.Include(x => x.WFDefinition).Include(x => x.Activity).FirstOrDefault(x => x.SequenceNo == sequence && x.WFDefinition.ModuleTypeId == appcontext.Module.Id);
            }
        }

        /// <summary>
        /// Get user from analyst group
        /// </summary>
        /// <returns>users</returns>
        internal List<User> GetUsersFromAnalystGroup()
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var userRepo = wfUOW.GetRepository<User>();
                return userRepo.Items.Include(x => x.UserInGroup).Where(x => x.IsActive && x.UserInGroup.
                    Any(gp => gp.IsActive && gp.ModuleTypeId == appcontext.Module.Id && gp.Group.GroupName.ToLower() == Helper.Group.Analyst.ToLower())).ToList();
            }
        }
        /// <summary>
        /// Getting routes of workflow states 
        /// </summary>
        /// <param name="activityStates">Model</param>
        /// <returns></returns>
        internal List<WFRouting> GetRoutes(ViewModels.ActivityState activityStates)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var routeRepo = wfUOW.GetRepository<WFRouting>();
                return routeRepo.Items.Where(x => x.ActivityId == activityStates.ActivityId && activityStates.StateId == x.FromStateId).ToList();

            }

        }
        /// <summary>
        /// Method used to save the routes
        /// </summary>
        /// <param name="routes"></param>
        internal string SaveRoute(List<WFRouting> routes, int ActivityId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var msg = string.Empty;
                var routeRepo = wfUOW.GetRepository<WFRouting>();
                var routesInDb = routeRepo.Items.Where(x => x.ActivityId == ActivityId);
                var routeIdsInView = routes.Select(x => x.Id);
                var routesNotInView = routesInDb.Where(x => !routeIdsInView.Contains(x.Id)).ToList();

                if (routesNotInView.IsCollectionValid())
                    routesNotInView.ForEach(routeRepo.Delete);

                msg = routesNotInView.IsCollectionValid() ? "Saved Successfully." : "Nothing to Save.";
                foreach (var route in routes)
                {
                    if (route.Id == 0)
                    {
                        routeRepo.Insert(route);
                        msg = "Saved Successfully.";
                    }
                    else
                    {
                        var routeInDb = routeRepo.Items.FirstOrDefault(x => x.Id == route.Id);
                        if (routeInDb.IsNull())
                        {
                            route.Id = 0;
                            var newRoute = Mapper.Map<WFRouting>(route);
                            routeRepo.Insert(newRoute);
                            msg = "Saved Successfully.";
                        }

                    }
                }
                wfUOW.SaveChanges();
                return msg;
            }
        }

        /// <summary>
        /// Retunrn routes for the particuler activity.
        /// </summary>
        /// <param name="activityId"></param>
        /// <returns>WFRouting</returns>
        internal List<WFRouting> GetRoutes(int activityId)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var routeRepo = wfUOW.GetRepository<WFRouting>();
                return routeRepo.Items.Where(x => x.ActivityId == activityId).ToList();
            }
        }

        /// <summary>
        /// Method is used to park and un park cases
        /// </summary>
        /// <param name="p">caseNo</param>
        internal void UpdateParkStatus(string caseNo, string comments)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wfUOW.GetRepository<Assignment>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var assignment = assignmentRepo.Items.Include(x => x.AssignedTo).Include(x => x.WFTracking)
                  .FirstOrDefault(x => x.CaseNo.ToLower() == caseNo.ToLower() && x.Status.ToLower() != Helper.Status.End.ToLower() && x.WFTracking.Any(wfTrack => wfTrack.ModuleTypeId == appcontext.Module.Id) &&
                  x.AssignedTo.Any(assignTo => assignTo.WorkflowStatus.Trim().ToLower() ==
                  Helper.Status.InProgess.Trim().ToLower()));

                if (assignment.IsNull())
                    throw new BusinessException("Case is not assigned.");

                var assignedTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus.Trim().ToLower()
                    == Helper.Status.InProgess.Trim().ToLower());

                var dateTime = DateTime.Now;

                if (assignedTo.IsPark)
                {
                    assignedTo.IsPark = false;
                    assignedTo.UnParkDate = dateTime;
                    assignedTo.Comments = comments;
                }
                else
                {
                    assignedTo.IsPark = true;
                    assignedTo.ParkDate = dateTime;
                    assignedTo.ParkReason = comments;
                }

                assignment.ModifiedDate = dateTime;
                assignment.ModifiedBy = appcontext.User.Id;
                assignedTo.ModifiedDate = dateTime;
                assignedTo.ModifiedBy = appcontext.User.Id;

                wfUOW.SaveChanges();

            }
        }

        /// <summary>
        /// Method to return the case details for the particuler case number
        /// </summary>
        /// <param name="caseNo">case number</param>
        /// <returns></returns>
        internal CaseDetail GetCaseDetails(string caseNo)
        {
            using (var wFUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wFUOW.GetRepository<Assignment>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var assignedCase = assignmentRepo.Items.Include(x => x.WFTracking).Include(x => x.AssignedTo).
                    Where(x => x.CaseNo.ToLower().Equals(caseNo.ToLower()) &&
                        x.WFTracking.Any(y => y.ModuleTypeId == appcontext.Module.Id)).OrderByDescending(x => x.Id).
                        FirstOrDefault();

                var assignment = assignmentRepo.Items.Include(x => x.WFTracking).Include(x => x.AssignedTo)
                    .FirstOrDefault(x => x.CaseNo.ToLower().Equals(caseNo.ToLower()) && x.WFTracking.Any(wfTrack => wfTrack.ModuleTypeId == appcontext.Module.Id)
                    && x.AssignedTo.Any(y => y.WorkflowStatus.Equals(Helper.Status.InProgess)) && !x.Status.Equals(Helper.Status.End));

                if (assignment.IsNotNull())
                {
                    var assignTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus.Equals(Helper.Status.InProgess));
                    var status = assignTo.OpenDate.HasValue ? assignTo.WorkflowStatus : Helper.Status.Assigned;
                    var maxValue = assignment.AssignedTo.Max(x => x.StepSequenceNo);
                    var caseOpenDate = assignment.AssignedTo.Where(x => x.State.Equals(Helper.Status.Assigned)).
                          OrderBy(x => x.Id).FirstOrDefault().OpenDate;
                    var maxStatus = assignment.AssignedTo.Where(x => x.StepSequenceNo == maxValue);
                    var lastAnalystId = assignment.AssignedTo.Where(x => x.State.Equals(Helper.Status.Assigned)).OrderByDescending(x => x.Id).FirstOrDefault().UserId;
                    return new CaseDetail
                    {
                        AssignmentId = assignment.Id,
                        CaseNo = caseNo,
                        CaseOpenDate = caseOpenDate,
                        LevelOpenDate = assignTo.OpenDate,
                        State = assignTo.State,
                        AssignDate = assignTo.CreatedDate,
                        ParkedDate = assignTo.ParkDate,
                        ParkReason = assignTo.ParkReason,
                        StateId = GetStateId(assignTo.State),
                        Status = assignTo.IsPark ? Helper.Status.Parked : status,
                        UserId = assignTo.UserId,
                        UserName = GetUserName(assignTo.UserId),
                        HighestStatus = maxStatus.Any(x => x.WorkflowStatus.Equals(Helper.Status.Assigned)) ?
                                     Helper.Status.Assigned : string.Join(",", maxStatus.Select(x => x.State).Distinct()),
                        LastAnalyst = GetUserName(lastAnalystId),
                        IsOpen = assignTo.OpenDate.HasValue,
                        DaysInQueue = assignTo.OpenDate.HasValue ? CalculateDaysInQueue(assignTo) : default(int)
                    };
                }
                return new CaseDetail
                {
                    AssignmentId = assignedCase.IsNotNull() ? assignedCase.Id : 0,
                    CaseNo = caseNo,
                    CaseOpenDate = assignedCase.IsNotNull() ? assignedCase.AssignedTo.Where
                            (x => x.State.Equals(Helper.Status.Assigned)).OrderBy(x => x.Id).FirstOrDefault().OpenDate : null,

                    CaseClosedDate = assignedCase.IsNotNull() ? assignedCase.ModifiedDate : null,
                    Status = assignedCase.IsNotNull() ? Helper.Status.End : Helper.Status.UnAssigned,
                    State = assignedCase.IsNotNull() ? Helper.Status.End : Helper.Status.UnAssigned,
                    HighestStatus = assignedCase.IsNotNull() ? Helper.Status.End : Helper.Status.UnAssigned,
                    LastAnalyst = assignedCase.IsNotNull() ? GetUserName(assignedCase.AssignedTo.
                                  Where(x => x.State.Equals(Helper.Status.Assigned)).
                                  OrderByDescending(x => x.Id).FirstOrDefault().UserId) : string.Empty,
                    LastAnalystId = assignedCase.IsNotNull() ? assignedCase.AssignedTo.
                          Where(x => x.State.Equals(Helper.Status.Assigned)).
                          OrderByDescending(x => x.Id).FirstOrDefault().UserId : default(int)
                };
            }
        }


        /// <summary>
        /// Getting the case details for all the cases
        /// </summary>
        /// <param name="caseNos">List of case number</param>
        /// <returns>List of case details</returns>
        internal List<CaseDetail> GetCaseDetails(List<string> caseNos)
        {
            var caseDetails = new List<CaseDetail>();
            //caseNos.ForEach(x => caseDetails.Add(GetCaseDetails(x)));
            using (var wFUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wFUOW.GetRepository<Assignment>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var assignments = assignmentRepo.Items.Include(x => x.WFTracking).Include(x => x.AssignedTo)
                    .Where(x => caseNos.Contains(x.CaseNo) && x.WFTracking.Any(wfTrack => wfTrack.ModuleTypeId == appcontext.Module.Id));

                foreach (var caseNo in caseNos)
                {
                    var assignment = assignments.FirstOrDefault(x => x.CaseNo.Equals(caseNo) && x.Status != Helper.Status.End &&
                        x.AssignedTo.Any(assignTo => assignTo.WorkflowStatus.Equals(Helper.Status.InProgess)));
                    if (assignment.IsNotNull())
                    {
                        var assignTo = assignment.AssignedTo.FirstOrDefault(x => x.WorkflowStatus.Equals(Helper.Status.InProgess));
                        var caseOpenDate = assignment.AssignedTo.Where(x => x.State.Equals(Helper.Status.Assigned)).
                            OrderBy(x => x.Id).FirstOrDefault().OpenDate;

                        var status = assignTo.OpenDate.HasValue ? assignTo.WorkflowStatus : Helper.Status.Assigned;
                        var maxValue = assignment.AssignedTo.Max(x => x.StepSequenceNo);
                        var maxStatus = assignment.AssignedTo.Where(x => x.StepSequenceNo == maxValue);
                        var lastAnalystId = assignment.AssignedTo.Where(x => x.State.Equals(Helper.Status.Assigned)).OrderByDescending(x => x.Id).FirstOrDefault().UserId;
                        var caseDetail = new CaseDetail
                        {
                            AssignmentId = assignment.Id,
                            CaseNo = caseNo,
                            CaseOpenDate = caseOpenDate,
                            LevelOpenDate = assignTo.OpenDate,
                            State = assignTo.State,
                            AssignDate = assignTo.CreatedDate,
                            ParkedDate = assignTo.ParkDate,
                            ParkReason = assignTo.ParkReason,
                            StateId = GetStateId(assignTo.State),
                            Status = assignTo.IsPark ? Helper.Status.Parked : status,
                            UserId = assignTo.UserId,
                            HighestStatus = maxStatus.Any(x => x.WorkflowStatus.Equals(Helper.Status.Assigned)) ?
                                         Helper.Status.Assigned : string.Join(",", maxStatus.Select(x => x.State).Distinct()),
                            LastAnalystId = lastAnalystId,
                            IsOpen = assignTo.OpenDate.HasValue,
                            DaysInQueue = assignTo.OpenDate.HasValue ? CalculateDaysInQueue(assignTo) : default(int)
                        };
                        caseDetails.Add(caseDetail);
                    }
                    else
                    {
                        var assignedCase = assignments.Where(x => x.CaseNo.Equals(caseNo)).OrderByDescending(x => x.Id).FirstOrDefault();
                        var caseDetail = new CaseDetail
                        {
                            AssignmentId = assignedCase.IsNotNull() ? assignedCase.Id : 0,
                            CaseNo = caseNo,
                            CaseOpenDate = assignedCase.IsNotNull() ? assignedCase.AssignedTo.Where
                            (x => x.State.Equals(Helper.Status.Assigned)).OrderBy(x => x.Id).FirstOrDefault().OpenDate : null,
                            CaseClosedDate = assignedCase.IsNotNull() ? assignedCase.ModifiedDate : null,
                            State = assignedCase.IsNotNull() ? Helper.Status.End : Helper.Status.UnAssigned,
                            Status = assignedCase.IsNotNull() ? Helper.Status.End : Helper.Status.UnAssigned,
                            HighestStatus = assignedCase.IsNotNull() ? Helper.Status.End : Helper.Status.UnAssigned,
                            LastAnalystId = assignedCase.IsNotNull() ? assignedCase.AssignedTo.
                                            Where(x => x.State.Equals(Helper.Status.Assigned)).
                                            OrderByDescending(x => x.Id).FirstOrDefault().UserId : default(int),
                        };
                        caseDetails.Add(caseDetail);
                    }
                }
            }
            FillUserName(caseDetails);
            return caseDetails;
        }
        /// <summary>
        /// this method is used to fill the username based on userId 
        /// </summary>
        /// <param name="caseDetails">case Details List</param>
        private void FillUserName(List<CaseDetail> caseDetails)
        {
            var userIds = new List<long>();
            caseDetails.ForEach(x =>
            {
                if (x.UserId > 0)
                    userIds.Add(x.UserId);
                if (x.LastAnalystId > 0)
                    userIds.Add(x.LastAnalystId);
            });
            using (var wfUOW = new WFUnitOfWork())
            {
                var userRepo = wfUOW.GetRepository<User>();
                var users = userRepo.Items.Where(x => userIds.Contains(x.UserId)).Select(x => new { Id = x.UserId, Name = x.UserName }).ToList();
                foreach (var item in caseDetails)
                {
                    var user = users.FirstOrDefault(x => x.Id == item.UserId);
                    var analyst = users.FirstOrDefault(x => x.Id == item.LastAnalystId);
                    item.UserName = user.IsNotNull() ? user.Name : string.Empty;
                    item.LastAnalyst = analyst.IsNotNull() ? analyst.Name : string.Empty;
                }
            }

        }

        /// <summary>
        /// This method is used to Get the display name of the step.
        /// </summary>
        /// <param name="stepName">stepname</param>
        /// <param name="moduleId">module Id of current module</param>
        /// <returns></returns>
        internal string GetDisplayName(string stepName, int moduleId)
        {
            using (var wFUOW = new WFUnitOfWork())
            {
                var wfStepRepo = wFUOW.GetRepository<Step>();
                var step = wfStepRepo.Items.Include(x => x.WFDefinition).FirstOrDefault(x => x.WFDefinition.ModuleTypeId == moduleId
                    && x.StepName.ToLower().Equals(stepName));
                return step.IsNotNull() ? step.DisplayName : stepName;
            }
        }
        /// <summary>
        /// this method is used to Open a case after assignment.
        /// </summary>
        /// <param name="caseNo">case number of the case.</param>
        internal void OpenCase(string caseNo)
        {
            using (var wfUOW = new WFUnitOfWork())
            {
                var assignmentRepo = wfUOW.GetRepository<Assignment>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var assignment = assignmentRepo.Items.Include(x => x.AssignedTo).Include(x => x.WFTracking)
                  .FirstOrDefault(x => x.CaseNo.ToLower() == caseNo.ToLower() && x.Status.ToLower() != Helper.Status.End.ToLower() && x.WFTracking.Any(wfTrack => wfTrack.ModuleTypeId == appcontext.Module.Id) &&
                  x.AssignedTo.Any(assignTo => assignTo.WorkflowStatus.Trim().ToLower().Equals(
                  Helper.Status.InProgess.Trim().ToLower())));

                if (assignment.IsNull())
                    throw new BusinessException("Case is not assigned.");

                var assigned = assignment.AssignedTo.FirstOrDefault(assignTo => assignTo.WorkflowStatus.Trim().ToLower().Equals(
                  Helper.Status.InProgess.Trim().ToLower()));

                if (assigned.OpenDate.HasValue)
                    throw new BusinessException("Case is already open.");

                var dateTime = DateTime.Now;
                assigned.OpenDate = dateTime;
                assigned.ModifiedDate = dateTime;
                assigned.ModifiedBy = appcontext.User.Id;
                assignment.ModifiedDate = dateTime;
                assignment.ModifiedBy = appcontext.User.Id;
                wfUOW.SaveChanges();
            }

        }

        private int CalculateDaysInQueue(AssignedTo assignTo)
        {
            var createdDate = assignTo.CreatedDate;
            int daysInPark = 0;
            int daysInQueue = 0;
            var dateTime = DateTime.Now;

            //if (assignTo.ParkDate.HasValue && assignTo.UnParkDate.HasValue)
            //    daysInPark = assignTo.UnParkDate.Value.GetDifferenceInDaysX(assignTo.ParkDate.Value);

            //else if (assignTo.ParkDate.HasValue && !assignTo.UnParkDate.HasValue)
            //    daysInPark = dateTime.GetDifferenceInDaysX(assignTo.ParkDate.Value);

            //if (assignTo.WorkflowStatus.Equals(Helper.Status.InProgess))
            //    daysInQueue = dateTime.GetDifferenceInDaysX(assignTo.CreatedDate.Value) - daysInPark;
            //else
            //    daysInQueue = assignTo.ModifiedDate.Value.GetDifferenceInDaysX(assignTo.CreatedDate.Value) - daysInPark;

            daysInQueue = dateTime.GetDifferenceInDaysX(assignTo.OpenDate.Value);

            return daysInQueue;
        }

    }
}

