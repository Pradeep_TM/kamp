﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.Core.Repository.Models;
using System.Windows.Input;
using KAMP.Core.Workflow.UserControls;
using KAMP.Core.Workflow.BLL;
using System.Windows;
using KAMP.Core.Repository.WF;
using KAMP.Core.Repository.UM;
namespace KAMP.Core.Workflow.ViewModels
{
    internal class WorkflowStateVm : INotifyPropertyChanged
    {
        public Action SaveState;
        public Action<int> RemoveState;
        public Action Reset;
        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, x => x.Id); }
        }

        private string _stateName;

        public string StateName
        {
            get { return _stateName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stateName, x => x.StateName); }
        }


        private bool _IsStatesExists;

        public bool IsStatesExists
        {
            get { return _IsStatesExists; }
            set { PropertyChanged.HandleValueChange(this, value, ref _IsStatesExists, x => x.IsStatesExists); }
        }


        private bool _isUpdate;

        public bool IsUpdate
        {
            get { return _isUpdate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isUpdate, x => x.IsUpdate); }
        }

        private ObservableCollection<Group> _availableGroup;

        public ObservableCollection<Group> AvailableGroup
        {
            get { return _availableGroup; }
            set { PropertyChanged.HandleValueChange(this, value, ref _availableGroup, x => x.AvailableGroup); }
        }

        private Group _selectedGroup;

        public Group SelectedGroup
        {
            get { return _selectedGroup; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedGroup, x => x.SelectedGroup); }
        }

        private ObservableCollection<State> _states;

        public ObservableCollection<State> States
        {
            get { return _states; }
            set { PropertyChanged.HandleValueChange(this, value, ref _states, x => x.States); }
        }

        private List<string> _stateTypes;

        public List<string> StateTypes
        {
            get { return _stateTypes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stateTypes, x => x.StateTypes); }
        }

        private string _stateType;

        public string StateType
        {
            get { return _stateType; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _stateType, x => x.StateType);
                if (!string.IsNullOrEmpty(_stateType))
                {
                    var bl = new WorkflowBL();
                    StateTypes = bl.GetStateTypes(_stateType);
                }
            }
        }

        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new DelegateCommand(() =>
                    {
                        if (!string.IsNullOrEmpty(StateName) && SelectedGroup.IsNotNull() && SelectedGroup.GroupId > 0 && StateType.IsNotEmpty())
                        {
                            if (IsStatenameExists()) { OpenMessageBox("Statename already exists in Database.", MessageType.Error); return; }

                            if (SaveState.IsNotNull())
                            {

                                try
                                {
                                    SaveState();
                                }
                                catch (BusinessException ex)
                                {
                                    var msg = ex.Message.ToString();
                                    OpenMessageBox(msg, MessageType.Error);
                                }

                            }
                        }
                        else if (string.IsNullOrEmpty(StateName))
                            OpenMessageBox("Please enter state name.", MessageType.Error);
                        else if (SelectedGroup.IsNotNull() && SelectedGroup.GroupId == 0)
                            OpenMessageBox("Please select a group from the dropdown.", MessageType.Error);
                        else if (StateType.IsEmpty())
                            OpenMessageBox("Please select or enter state type.", MessageType.Error);
                    });
                }
                return _saveCommand;
            }
        }

        private ICommand _edit;

        public ICommand Edit
        {
            get
            {

                if (_edit.IsNull())
                {
                    _edit = new DelegateCommandWithParam((param) =>
                    {
                        var id = int.Parse(param.ToString());
                        if (id > 0)
                        {
                            var state = States.FirstOrDefault(x => x.Id == id);
                            if (state.IsNotNull())
                            {
                                if (StateTypes.IsCollectionValid())
                                    StateTypes.Clear();

                                StateName = state.StateName;
                                Id = (int)state.Id;
                                var group = state.Groups.FirstOrDefault();
                                if (group.IsNotNull())
                                    SelectedGroup = AvailableGroup.FirstOrDefault(x => x.GroupId == group.GroupId);
                                StateType = state.StateMetadata.StateType;
                                IsUpdate = true;
                                IsStatesExists = !IsUpdate;
                            }
                        }
                    });
                }
                return _edit;
            }

        }

        private ICommand _removeStateCommand;

        public ICommand RemoveStateCommand
        {
            get
            {

                if (_removeStateCommand.IsNull())
                {
                    _removeStateCommand = new DelegateCommandWithParam((param) =>
                    {
                        var actionResult = MessageBoxControl.Show("Are you sure want to delete this state?", MessageBoxButton.YesNo, MessageType.Alert);
                        if (actionResult == MessageBoxResult.Yes)
                        {
                            var id = int.Parse(param.ToString());
                            if (id > 0)
                            {
                                if (RemoveState.IsNotNull())
                                    RemoveState(id);
                            }
                        }

                        IsUpdate = false;
                    });
                }
                return _removeStateCommand;
            }

        }
       
        private ICommand _cancel;

        public ICommand Cancel
        {
            get
            {
                if (_cancel.IsNull())
                    _cancel = new DelegateCommand(() =>
                    {
                        if (Reset.IsNotNull())
                            Reset();
                    });

                return _cancel;
            }

        }


        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsStatenameExists()
        {
            if (Id == 0)
            {
                if (!States.IsCollectionValid())
                    return false;
                var state = States.Where(x => x.StateName.ToLower() == StateName.ToLower());

                return state.Any();
            }
            else if (Id > 0)
            {
                var state = States.Where(x => x.StateName.ToLower() == StateName.ToLower() && x.Id != Id);

                return state.Any();
            }
            return false;
        }
        public void OpenMessageBox(string msg, MessageType type = MessageType.Information)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, type);
        }
    }
}
