﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
using KAMP.Core.Workflow.UserControls;
using KAMP.Core.Repository;
using KAMP.Core.Repository.Models;
using KAMP.Core.Workflow.BLL;
using System.Windows;
using KAMP.Core.Repository.WF;
using AutoMapper;
using KAMP.Core.Workflow.AppCode;
using System.Windows.Controls;
using System.Windows.Media;
namespace KAMP.Core.Workflow.ViewModels
{
    internal class WorkflowMainVm : INotifyPropertyChanged
    {
        public WorkflowMainVm()
        {

        }
        public int stepNo
        {
            get
            {
                var lastItem = WfStepsVm.LastOrDefault();
                if (lastItem.IsNotNull())
                {
                    var stepno = lastItem.StepName.Split('e');
                    return int.Parse(stepno[1]) + 1;
                }
                return 1;
            }
        }
        public Action SaveWorkFlowSteps;
        public Action IntializedData;
        public Action<int> RemoveWfStep;
        private Grid grid;
        private string _wfName;

        public string WfName
        {
            get { return _wfName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _wfName, (x) => x.WfName); }
        }

        private int _wfId;

        public int WfId
        {
            get { return _wfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _wfId, (x) => x.WfId); }
        }

        private int _objectTypeId;

        public int ObjectTypeId
        {
            get { return _objectTypeId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _objectTypeId, (x) => x.ObjectTypeId); }
        }

        private string _wfDefinition;

        public string WfDefinition
        {
            get { return _wfDefinition; }
            set { PropertyChanged.HandleValueChange(this, value, ref _wfDefinition, (x) => x.WfDefinition); }
        }


        private DoublyLinkedList<WorkflowStepVm> _wfStepsVm;

        public DoublyLinkedList<WorkflowStepVm> WfStepsVm
        {
            get { return _wfStepsVm; }
            set { PropertyChanged.HandleValueChange(this, value, ref _wfStepsVm, (x) => x.WfStepsVm); }
        }

        private WorkflowStepVm _wfcurrentTaskVm;

        public WorkflowStepVm WfcurrentTaskVm
        {
            get { return _wfcurrentTaskVm; }
            set { PropertyChanged.HandleValueChange(this, value, ref _wfcurrentTaskVm, (x) => x.WfcurrentTaskVm); }
        }

        private bool _isUpdate;

        public bool IsUpdate
        {
            get { return _isUpdate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isUpdate, (x) => x.IsUpdate); }
        }

        private bool _isStepExits;
        public bool IsWfStepExists
        {
            get { return _isStepExits; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isStepExits, (x) => x.IsWfStepExists); }
        }

        private bool _isDirty;
        public bool IsDirty
        {
            get { return _isDirty; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isDirty, (x) => x.IsDirty); }
        }

        private ICommand _addMoreStep;

        public ICommand AddMoreStep
        {
            get
            {

                if (_addMoreStep.IsNull())
                {
                    _addMoreStep = new DelegateCommand(() =>
                    {

                        if (this.WfStepsVm.IsNull())
                            WfStepsVm = new DoublyLinkedList<WorkflowStepVm>();

                        if (!IsStepFine())
                            return;
                        var step = new WorkflowStepVm()
                        {
                            StepName = string.Format("Stage{0}", stepNo),
                            DisplayName = WfcurrentTaskVm.DisplayName,
                            DemoteStates = GetStates(WfcurrentTaskVm.DemoteStates),
                            PromoteStates = GetStates(WfcurrentTaskVm.PromoteStates),
                        };

                        WfStepsVm.AddItem(step);

                        ResetWFCurrentStep();

                        IsWfStepExists = WfStepsVm.IsCollectionValid();
                    });
                }
                return _addMoreStep;
            }

        }

        private void ResetWFCurrentStep()
        {
            WfcurrentTaskVm.StepName = string.Empty;
            WfcurrentTaskVm.DisplayName = string.Empty;
            foreach (var step in WfcurrentTaskVm.PromoteStates)
                step.IsSelected = false;
            foreach (var step in WfcurrentTaskVm.DemoteStates)
                step.IsSelected = false;
            //foreach (var step in WfcurrentTaskVm.ParkStates)
            //    step.IsSelected = false;
        }

        private ObservableCollection<WorkflowActivityVm> GetStates(ObservableCollection<WorkflowActivityVm> activity)
        {
            var activities = new ObservableCollection<WorkflowActivityVm>();
            foreach (var state in activity)
            {
                activities.Add(new WorkflowActivityVm()
                {
                    Id = state.Id,
                    IsSelected = state.IsSelected,
                    MailTemplate = state.MailTemplate,
                    StateId = state.StateId,
                    StateName = state.StateName,
                    StepId = state.StepId,
                    WfActionId = state.WfActionId,
                    NotifyPropertyChanged = state.NotifyPropertyChanged
                }
               );
            }
            return activities;
        }

        private ICommand _openStep;

        public ICommand OpenStep
        {
            get
            {

                if (_openStep.IsNull())
                {
                    _openStep = new DelegateCommandWithParam((param) =>
                    {
                        var stepName = (string)param;
                        var step = WfStepsVm.FirstOrDefault(x => x.StepName.Trim().ToLower() == stepName.Trim().ToLower());
                        if (step == null)
                            return;
                        step.IsStepDetailsVisible = true;
                        var steps = WfStepsVm.Where(x => x.StepName.Trim().ToLower() != stepName.Trim().ToLower()).ToList();
                        steps.ForEach(x =>
                        {
                            x.IsStepEditable = true;
                        });
                        WfcurrentTaskVm = new WorkflowStepVm()
                        {
                            StepName = step.StepName,
                            DisplayName = step.DisplayName,
                            PromoteStates = GetStates(step.PromoteStates),
                            DemoteStates = GetStates(step.DemoteStates),
                            // ParkStates = GetStates(step.ParkStates),
                            NotifyPropertyChanged = SetDirty
                        };
                        oldStepname = stepName;
                        IsUpdate = true;
                        IsDirty = false;
                    });
                }
                return _openStep;
            }

        }

        private ICommand _openRouting;
        public ICommand OpenRouting
        {
            get
            {

                if (_openRouting.IsNull())
                {
                    _openRouting = new DelegateCommandWithParam((param) =>
                    {
                        var stepName = (string)param;
                        var step = WfStepsVm.FirstOrDefault(x => x.StepName.Trim().ToLower() == stepName.Trim().ToLower());
                        if (step == null)
                            return;
                        if (step.StepId == 0 || (step.NextNode.IsNotNull() && step.NextNode.StepId == 0) || (step.PrevNode.IsNotNull() && step.PrevNode.StepId == 0))
                        {
                            OpenMessageBox("Please save the workflow step first then create the route.");
                            return;
                        }
                        IsDirty = false;
                        OpenRoutingPopUp(step);
                    });
                }
                return _openRouting;
            }

        }

        private void OpenRoutingPopUp(WorkflowStepVm step)
        {
            if (grid.IsNotNull())
                return;

            grid = new Grid();
            Action ClosePopUp = CloseControl;
            var ucWorkflowRouting = new UCWorkflowRouting(step, ClosePopUp, IntializedData);
            grid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            grid.Children.Add(lbl);
            grid.Children.Add(ucWorkflowRouting);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(grid);
            }
        }

        private void CloseControl()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(grid);
                grid = null;
            }
        }

        public void SetDirty()
        {
            IsDirty = true;
        }

        private ICommand _openStepDetails;

        public ICommand OpenStepDetails
        {
            get
            {

                if (_openStepDetails.IsNull())
                {
                    _openStepDetails = new DelegateCommandWithParam((param) =>
                    {
                        var stepName = (string)param;
                        var step = WfStepsVm.FirstOrDefault(x => x.StepName.Trim().ToLower() == stepName.Trim().ToLower());
                        if (step == null)
                            return;
                        step.IsStepDetailsVisible = !step.IsStepDetailsVisible;
                    });
                }
                return _openStepDetails;
            }

        }


        private ICommand _removeStep;

        public ICommand RemoveStep
        {
            get
            {

                if (_removeStep.IsNull())
                {
                    _removeStep = new DelegateCommandWithParam((param) =>
                    {
                        var actionResult = MessageBoxControl.Show("Are you sure want to delete this step?", MessageBoxButton.YesNo, MessageType.Alert);
                        //var actionResult = MessageBox.Show("Are you sure want to delete this step?", "Confirmation", MessageBoxButton.YesNo,MessageBoxImage.Warning);
                        if (actionResult == MessageBoxResult.Yes)
                        {
                            var stepName = (string)param;

                            var step = WfStepsVm.FirstOrDefault(x => x.StepName.ToLower() == stepName.ToLower());
                            if (step == null)
                                return;

                            var stepId = step.StepId;

                            if (stepId > 0 && RemoveWfStep.IsNotNull())
                                RemoveWfStep(stepId);
                            else if (stepId == 0)
                                WfStepsVm.DeleteItem(step);

                            IsWfStepExists = WfStepsVm.IsCollectionValid();

                        }

                    });
                }
                return _removeStep;
            }

        }

        private ICommand _saveWfSteps;

        public ICommand SaveWfSteps
        {
            get
            {

                if (_saveWfSteps.IsNull())
                {
                    _saveWfSteps = new DelegateCommand(() =>
                    {
                        if (!WfStepsVm.IsCollectionValid())
                        { OpenMessageBox("Please add atleast one step before saving the workflow.", MessageType.Error); return; }

                        if (!IsDirty)
                        { OpenMessageBox("There are no changes to save."); return; }

                        if (SaveWorkFlowSteps.IsNotNull())
                            SaveWorkFlowSteps();
                    });
                }

                return _saveWfSteps;
            }

        }


        private ICommand _updateStep;

        public ICommand UpdateStep
        {
            get
            {

                if (_updateStep.IsNull())
                {
                    _updateStep = new DelegateCommand(() =>
                    {
                        if (IsDirty)
                        {
                            var step = WfStepsVm.FirstOrDefault(x => x.StepName.ToLower() == oldStepname.ToLower());
                            if (step == null)
                                return;

                            if (!IsStepFine())
                                return;

                            step.PromoteStates = GetStates(WfcurrentTaskVm.PromoteStates);
                            step.DemoteStates = GetStates(WfcurrentTaskVm.DemoteStates);
                            step.DisplayName = WfcurrentTaskVm.DisplayName;

                            if (SaveWorkFlowSteps.IsNotNull() && WfId > 0)
                                SaveWorkFlowSteps();

                            if(WfId==0)
                            {
                                foreach (var item in WfStepsVm)
                                {
                                    item.IsStepEditable = false;
                                    item.IsStepDetailsVisible = false;
                                }

                                ResetWFCurrentStep();
                                IsUpdate = false;
                            }
                        }
                        else
                        {
                            OpenMessageBox("No changes to updated.");
                        }

                    });
                }

                return _updateStep;
            }

        }

        private ICommand _cancelCommand;

        public ICommand CancelCommand
        {
            get
            {
                if (_cancelCommand.IsNull())
                    _cancelCommand = new DelegateCommand(() =>
                    {

                        foreach (var steps in WfStepsVm)
                        {
                            steps.IsStepDetailsVisible = false;
                            steps.IsStepEditable = false;
                        }
                        IsUpdate = false;
                        oldStepname = string.Empty;
                        IsWfStepExists = WfStepsVm.IsCollectionValid();
                        ResetWFCurrentStep();
                        IsDirty = false;
                    });

                return _cancelCommand;
            }

        }
        private bool IsStepFine()
        {
            var isStepFine = true;


            var selectedPromoteStates = WfcurrentTaskVm.PromoteStates.Where(x => x.IsSelected).ToList();
            var selectedDemoteStates = WfcurrentTaskVm.DemoteStates.Where(x => x.IsSelected).ToList();
            var selectedPromotesIds = selectedPromoteStates.Select(x => x.StateId);
            var selectedDemotesIds = selectedDemoteStates.Select(x => x.StateId);
            var displayNames = WfStepsVm.Where(x => !x.StepName.ToLower().Equals(oldStepname.ToLower())).Select(x => x.DisplayName);
            if (WfcurrentTaskVm.DisplayName.IsEmpty())
            {
                OpenMessageBox("Please enter the level name.", MessageType.Error);
                isStepFine = false;
            }
            else if (!IsUpdate && WfStepsVm.Any(x => x.DisplayName.Trim().ToLower().Equals(WfcurrentTaskVm.DisplayName.Trim().ToLower())))
            {
                OpenMessageBox("Level name is already exists in the workflow.", MessageType.Error);
                isStepFine = false;
            }
            else if (IsUpdate && displayNames.Any(x => x.Trim().ToLower().Equals(WfcurrentTaskVm.DisplayName.Trim().ToLower())))
            {
                OpenMessageBox("Level name is already exists in the workflow.", MessageType.Error);
                isStepFine = false;
            }
            else if (!selectedPromoteStates.IsCollectionValid() || !selectedDemoteStates.IsCollectionValid())
            {
                OpenMessageBox("Please select a state from every option list.", MessageType.Error);
                isStepFine = false;
            }

            else if (selectedPromotesIds.Any(x => selectedDemotesIds.Contains(x)))
            {
                OpenMessageBox("Please remove duplicate selection from the options.", MessageType.Error);
                isStepFine = false;
            }

            return isStepFine;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private string oldStepname;

        public void OpenMessageBox(string msg, MessageType type = MessageType.Information)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, type);
        }

    }
}
