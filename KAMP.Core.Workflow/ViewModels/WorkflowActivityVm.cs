﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Collections.ObjectModel;
namespace KAMP.Core.Workflow.ViewModels
{
    public class WorkflowActivityVm : INotifyPropertyChanged
    {
        public Action NotifyPropertyChanged;

        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        private int _stepId;

        public int StepId
        {
            get { return _stepId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stepId, (x) => x.StepId); }
        }
        private int _stateId;

        public int StateId
        {
            get { return _stateId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stateId, (x) => x.StateId); }
        }

        private string _mailTemplate;

        public string MailTemplate
        {
            get { return _mailTemplate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _mailTemplate, (x) => x.MailTemplate); }
        }

        private int _wfActionId;

        public int WfActionId
        {
            get { return _wfActionId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _wfActionId, (x) => x.WfActionId); }
        }

        private string _stateName;

        public string StateName
        {
            get { return _stateName; }
            set { _stateName = value; }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected))
                    if (NotifyPropertyChanged.IsNotNull())
                        NotifyPropertyChanged();
            }
        }

        private ObservableCollection<Routing> _promoteRoutes;

        public ObservableCollection<Routing> PromoteRoutes
        {
            get { return _promoteRoutes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _promoteRoutes, (x) => x.PromoteRoutes); }
        }

        private ObservableCollection<Routing> _demoteRoutes;

        public ObservableCollection<Routing> DemoteRoutes
        {
            get { return _demoteRoutes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _demoteRoutes, (x) => x.DemoteRoutes); }
        }

        public string RoutesForPromote
        {
            get
            {
                return string.Join(",", PromoteRoutes.Select(x => x.StateName));
            }
        }
        public string RoutesForDemote
        {
            get
            {
                return string.Join(",", DemoteRoutes.Select(x => x.StateName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
