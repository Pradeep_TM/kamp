﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Workflow.ViewModels
{
    using AutoMapper;
    using Core.FrameworkComponents;
    using Core.Repository;
    using KAMP.Core.Repository.WF;
    using KAMP.Core.Workflow.AppCode;
    using KAMP.Core.Workflow.BLL;
    using KAMP.Core.Workflow.Helpers;
    using KAMP.Core.Workflow.UserControls;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    public class WorkflowRoutingVm : INotifyPropertyChanged
    {

        private ObservableCollection<Routing> _promotes;

        public ObservableCollection<Routing> Promotes
        {
            get { return _promotes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _promotes, (x) => x.Promotes); }
        }

        private ObservableCollection<Routing> _demotes;

        public ObservableCollection<Routing> Demotes
        {
            get { return _demotes; }
            set { PropertyChanged.HandleValueChange(this, value, ref _demotes, (x) => x.Demotes); }
        }

        private List<ActivityState> _states;

        public List<ActivityState> States
        {
            get { return _states; }
            set { PropertyChanged.HandleValueChange(this, value, ref _states, (x) => x.States); }
        }

        private ActivityState _selectedState;

        public ActivityState SelectedState
        {
            get { return _selectedState; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedState, (x) => x.SelectedState);
                if (_selectedState.IsNotNull() && _selectedState.StateId >= 0 && PopulateRoutes.IsNotNull())
                    PopulateRoutes(_selectedState);
            }
        }


        public bool IsPromoteRoutesAvailable
        {
            get
            {
                return Promotes.IsCollectionValid();
            }

        }


        public bool IsDemoteRoutesAvailable
        {
            get
            {
                return Demotes.IsCollectionValid();
            }

        }

        private ICommand _saveRoute;

        public ICommand SaveRoute
        {
            get
            {
                if (_saveRoute.IsNull())
                {
                    _saveRoute = new DelegateCommand(() =>
                    {
                        new BootStrapper().Configure();
                        if (!ValidateRoute())
                            return;
                        var routes = new List<WFRouting>();
                        var promoteRoutes = Promotes.Where(x => x.IsSelected).Select(x => Mapper.Map<WFRouting>(x)).ToList();
                        var demoteRoutes = Demotes.Where(x => x.IsSelected).Select(x => Mapper.Map<WFRouting>(x)).ToList();

                        if (promoteRoutes.IsCollectionValid())
                            routes.AddRange(promoteRoutes);
                        if (demoteRoutes.IsCollectionValid())
                            routes.AddRange(demoteRoutes);

                        WorkflowBL bl = new WorkflowBL();
                        var msg = bl.SaveRoute(routes, SelectedState.ActivityId);
                        OpenMessageBox(msg);
                    });
                }
                return _saveRoute;
            }
        }

        /// <summary>
        /// To Check whether the state is selected and whether user has selected any route
        /// for this state.
        /// </summary>
        /// <returns>bool</returns>
        private bool ValidateRoute()
        {
            if (SelectedState.StateId == 0)
            {
                OpenMessageBox("Select a state from the dropdown", MessageType.Error);
                return false;
            }
            //else if (Promotes.IsCollectionValid() && !Promotes.Any(x => x.IsSelected))
            //{
            //    OpenMessageBox("Please Select atleast one promote route.", MessageType.Error);
            //    return false;
            //}
            //else if (Demotes.IsCollectionValid() && !Demotes.Any(x => x.IsSelected))
            //{
            //    OpenMessageBox("Please Select atleast one demote route.", MessageType.Error);
            //    return false;
            //}
            return true;
        }

        public Action<ActivityState> PopulateRoutes { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OpenMessageBox(string msg, MessageType type = MessageType.Information)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, type);
        }
    }

    public class ActivityState
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int ActivityId { get; set; }
        public int ActionId { get; set; }
        public string StateNameWithAction
        {
            get
            {
                if (ActionId == (int)WFActions.Promote)
                    return "Promote Options-> " + StateName;
                if (ActionId == (int)WFActions.Demote)
                    return "Demote Options<- " + StateName;
                if (ActionId == (int)WFActions.Park)
                    return "Park - " + StateName;
                return StateName;
            }
        }
    }
    public class Routing : INotifyPropertyChanged
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { PropertyChanged.HandleValueChange(this, value, ref _id, (x) => x.Id); }
        }

        private int _stepId;

        public int StepId
        {
            get { return _stepId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stepId, (x) => x.StepId); }
        }
        private int _fromStateId;

        public int FromStateId
        {
            get { return _fromStateId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _fromStateId, (x) => x.FromStateId); }
        }
        private int _toStateId;

        public int ToStateId
        {
            get { return _toStateId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _toStateId, (x) => x.ToStateId); }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected); }
        }

        public string StateName { get; set; }
        public int ActivityId { get; set; }
        public int ToActivityId { get; set; }
        public int ActionId { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
