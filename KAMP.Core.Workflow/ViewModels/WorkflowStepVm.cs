﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.Models;
using System.Collections.ObjectModel;
namespace KAMP.Core.Workflow.ViewModels
{
    public class WorkflowStepVm : INotifyPropertyChanged, IDoublyLinkedListNode<WorkflowStepVm>
    {
        public Action NotifyPropertyChanged;
        private int _stepId;

        public int StepId
        {
            get { return _stepId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stepId, (x) => x.StepId); }
        }


        private int _wfId;

        public int WFId
        {
            get { return _wfId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _wfId, (x) => x.WFId); }
        }

        private string _stepName;

        public string StepName
        {
            get { return _stepName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stepName, (x) => x.StepName); }
        }

        private string _displayName;

        public string DisplayName
        {
            get { return _displayName; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref _displayName, (x) => x.DisplayName))
                    RaiseNotifyChanged();
            }
        }

        private int _sequenceNo;
        public int SequenceNo
        {
            get { return _sequenceNo; }
            set { PropertyChanged.HandleValueChange(this, value, ref _sequenceNo, (x) => x.SequenceNo); }
        }

        private ObservableCollection<WorkflowActivityVm> _promoteStates;

        public ObservableCollection<WorkflowActivityVm> PromoteStates
        {
            get
            {
                if (_promoteStates.IsNull())
                {
                    _promoteStates = new ObservableCollection<WorkflowActivityVm>();
                    _promoteStates.CollectionChanged += (s, e) =>
                    {
                        if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                        {
                            foreach (var item in e.NewItems)
                            {
                                (item as WorkflowActivityVm).NotifyPropertyChanged += RaiseNotifyChanged;
                            }
                        }
                    };
                }
                return _promoteStates;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _promoteStates, (x) => x.PromoteStates);
                PropertyChanged.Raise(this, (x) => x.StatesInPromote);
            }
        }

        private void RaiseNotifyChanged()
        {
            if (NotifyPropertyChanged.IsNotNull())
                NotifyPropertyChanged();
        }

        private ObservableCollection<WorkflowActivityVm> _demoteStates;

        public ObservableCollection<WorkflowActivityVm> DemoteStates
        {
            get
            {
                if (_demoteStates.IsNull())
                {
                    _demoteStates = new ObservableCollection<WorkflowActivityVm>();
                    _demoteStates.CollectionChanged += (s, e) =>
                    {
                        if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                        {
                            foreach (var item in e.NewItems)
                            {
                                (item as WorkflowActivityVm).NotifyPropertyChanged += RaiseNotifyChanged;
                            }
                        }
                    };
                }
                return _demoteStates;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _demoteStates, (x) => x.DemoteStates);
                PropertyChanged.Raise(this, (x) => x.StatesInDemote);
            }
        }

        /// <summary>
        /// To display All states routes  on UI 
        /// </summary>
        public ObservableCollection<WorkflowActivityVm> AllActivity
        {
            get
            {
                var activity = new ObservableCollection<WorkflowActivityVm>();
                activity.AddRange(PromoteStates.Where(x => x.IsSelected && ((x.PromoteRoutes.IsNotNull() && x.PromoteRoutes.Any()) ||(x.DemoteRoutes.IsNotNull() && x.DemoteRoutes.Any()))));
                activity.AddRange(DemoteStates.Where(x => x.IsSelected && ((x.PromoteRoutes.IsNotNull() && x.PromoteRoutes.Any()) ||( x.DemoteRoutes.IsNotNull() && x.DemoteRoutes.Any()))));
                return activity;
            }
        }

        public bool IsPromoteRoutesAvailable
        {
            get
            {
                return PromoteStates.Any(x => x.IsSelected && (x.PromoteRoutes.IsNotNull() && x.PromoteRoutes.Any()))
                    || DemoteStates.Any(x => x.IsSelected && (x.PromoteRoutes.IsNotNull() && x.PromoteRoutes.Any()));
            }
        }
        public bool IsDemoteRoutesAvailable
        {
            get
            {
                return PromoteStates.Any(x => x.IsSelected && (x.DemoteRoutes.IsNotNull() && x.DemoteRoutes.Any()))
                   || DemoteStates.Any(x => x.IsSelected && (x.DemoteRoutes.IsNotNull() && x.DemoteRoutes.Any()));
            }
        }
       
        public string StatesInPromote
        {
            get
            {
                return string.Join(",", PromoteStates.Where(x => x.IsSelected).Select(x => x.StateName));
            }

        }

        public string StatesInDemote
        {
            get
            {
                return string.Join(",", DemoteStates.Where(x => x.IsSelected).Select(x => x.StateName));
            }

        }
      
        private bool _isStepEditable;

        public bool IsStepEditable
        {
            get { return _isStepEditable; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isStepEditable, (x) => x.IsStepEditable); }
        }
        private bool _isStepDetailsVisible;

        public bool IsStepDetailsVisible
        {
            get { return _isStepDetailsVisible; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isStepDetailsVisible, (x) => x.IsStepDetailsVisible); }
        }
        private WorkflowStepVm _nextNode;
        public WorkflowStepVm NextNode
        {
            get { return _nextNode; }
            set { PropertyChanged.HandleValueChange(this, value, ref _nextNode, (x) => x.NextNode); }
        }

        private WorkflowStepVm _prevNode;
        public WorkflowStepVm PrevNode
        {
            get { return _prevNode; }
            set { PropertyChanged.HandleValueChange(this, value, ref _prevNode, (x) => x.PrevNode); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
