﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Workflow.ViewModels
{
    using FrameworkComponents;
    public class WorkflowMonitoringVm : INotifyPropertyChanged
    {
        public string BdrBrushColor { get; set; }
        public string LblForegroundColor { get; set; }
        public string WindowBackGroundColor { get; set; }


        private bool _isStepsVisible;

        public bool IsStepsVisible
        {
            get { return _isStepsVisible; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isStepsVisible, (x) => x.IsStepsVisible); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }

}
