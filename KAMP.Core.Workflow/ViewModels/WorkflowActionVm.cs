﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Workflow.Helpers;
using KAMP.Core.Repository.WF;
using KAMP.Core.Repository.UM;
namespace KAMP.Core.Workflow.ViewModels
{
    public class WorkflowActionVm : INotifyPropertyChanged
    {

        private int _userId;
        public int UserId
        {
            get { return _userId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userId, (x) => x.UserId); }
        }

        private string _caseNo;

        public string CaseNo
        {
            get { return _caseNo; }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseNo, (x) => x.CaseNo); }
        }

        private long _assignmentId;

        public long AssignmentId
        {
            get { return _assignmentId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assignmentId, (x) => x.AssignmentId); }
        }

        private string _comments;

        public string Comments
        {
            get { return _comments; }
            set { PropertyChanged.HandleValueChange(this, value, ref _comments, (x) => x.Comments); }
        }


        private string _currentStatus;

        public string CurrentStatus
        {
            get { return _currentStatus; }
            set { PropertyChanged.HandleValueChange(this, value, ref _currentStatus, (x) => x.CurrentStatus); }
        }

        private bool _isCurrentAssignUser;

        public bool IsCurrentAssignUser
        {
            get { return _isCurrentAssignUser; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isCurrentAssignUser, (x) => x.IsCurrentAssignUser); }
        }

        private bool _isParked;

        public bool IsParked
        {
            get { return _isParked; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isParked, (x) => x.IsParked); }
        }

        private bool _isFinal;

        public bool IsFinal
        {
            get { return _isFinal; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isFinal, (x) => x.IsFinal); }
        }


        private ObservableCollection<User> _usersInGroups;

        public ObservableCollection<User> UsersInGroups
        {
            get { return _usersInGroups; }
            set { PropertyChanged.HandleValueChange(this, value, ref _usersInGroups, (x) => x.UsersInGroups); }
        }

        private long _selecteduserId;

        public long SelecteduserId
        {
            get { return _selecteduserId; }
            set
            {
                if (!PropertyChanged.HandleValueChange(this, value, ref _selecteduserId, (x) => x.SelecteduserId))
                    if (_selecteduserId == 0) PropertyChanged.Raise(this, (x) => x.SelecteduserId);
            }
        }


        private ObservableCollection<StateWithAction> _stateswithAction;

        public ObservableCollection<StateWithAction> StatesWithAction
        {
            get { return _stateswithAction; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stateswithAction, (x) => x.StatesWithAction); }
        }

        private StateWithAction _selectedStateWithAction;

        public StateWithAction SelectedStateWithAction
        {
            get { return _selectedStateWithAction; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedStateWithAction, (x) => x.SelectedStateWithAction);

                if (_selectedStateWithAction.IsNotNull() &&_selectedStateWithAction.StateId != 0 && PopulateUsers.IsNotNull())
                    PopulateUsers(_selectedStateWithAction.StateId);

                else if (_selectedStateWithAction.IsNotNull() &&_selectedStateWithAction.StateId == 0 && _selectedStateWithAction.ActionId == 0 && UsersInGroups.IsCollectionValid())
                {
                    UsersInGroups.Clear();
                }
            }
        }



        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }
        private ICommand _parkUnParkCase;

        public ICommand ParkUnParkCase
        {
            get
            {

                if (_parkUnParkCase == null)
                    _parkUnParkCase = new DelegateCommand(() =>
                    {
                        if (OnParkUnParkedClick.IsNotNull())
                            OnParkUnParkedClick();
                    });
                return _parkUnParkCase;
            }

        }


  




        public Action OnSubmitClick;
        public Action<int> PopulateUsers;
        public Action ClosePopUp;
        public Action<string> PostAction;
        public Action OnParkUnParkedClick;
        public event PropertyChangedEventHandler PropertyChanged;

    }

    public class StateWithAction
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int ActionId { get; set; }
        public int ActivityId { get; set; }
        public string StateNameWithAction
        {
            get
            {
                if (ActionId == (int)WFActions.Promote)
                    return "Promote To -> " + StateName;
                if (ActionId == (int)WFActions.Demote)
                    return "Demote To <- " + StateName;
                if (ActionId == (int)WFActions.Park)
                    return "Park To - " + StateName;
                return StateName;
            }
        }
    }
}
