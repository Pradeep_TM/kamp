﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Workflow.Helpers
{
    public enum  WFActions:short
    {
        Promote=1,
        Demote,
        Park,
        Finalized
    }
}
