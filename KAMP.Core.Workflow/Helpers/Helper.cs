﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Workflow.Helpers
{
    public class Helper
    {
        public class Status
        {
            public static readonly string Assigned = "Assigned";
            public static readonly string UnAssigned = "UnAssigned";
            public static readonly string InProgess = "InProgress";
            public static readonly string Parked = "Parked";
            public static readonly string Completed = "Completed";
            public static readonly string End = "Delivered";
        }
        public class Group
        {
            public static readonly string Analyst = "Analyst";

        }
        public class Messages
        {
            public static readonly string WorkflowAborted = "Workflow aborted";
            public static readonly string workflowApproved = "Approved and moved to next level";
        }

        public class Color
        {
            public static readonly string BdrBrushColor = "#FF82BCD6";
            public static readonly string LblForegroundColor = "#0339b1";
            public static readonly string WindowBackGroundColor = "#FFCBE4E6";
            public static readonly string CheckBoxFillColor = "#FF0074B9";
        }

        public class WFConstant
        {
            public const string ManageWorkflow = "Manage Workflow";
            public const string ManageState = "Manage State";
        }
        public class EmailTemplate
        {
            public static readonly string PromoteEmailTemplate = @"Hi {0},
                                    You have a work item in your queue to attend. 
                                    Details of Work Item.
                                    <WorkItem>
                                    Case No:{1}
                                    Promoted From:{2}
                                    Promoted Date:{3}
                                    Current State:{4}
                                    </Workitem>
                                    Thanks,
                                    {5}";

            public static readonly string DemoteEmailTemplate = @"Hi {0},
                                    You have a work item in your queue to attend.
                                    Details of Work Item.
                                    <WorkItem>
                                    Case No:{1}
                                    Demoted From:{2}
                                    Demoted Date:{3}
                                    Current State:{4}
                                    </Workitem>
                                    Thanks,
                                    {5}";

            public static readonly string ParkEmailTemplate = @"Hi {0},
                                     You have a work item in your queue to attend.
                                     Details of Work Item.
                                     <WorkItem>
                                     Case No:{1}
                                     Park From:{2}
                                     Park Date:{3}
                                     Current State:{4}
                                     </Workitem>
                                     Thanks,
                                     {5}";

            public static readonly string AssignedEmailTemplate = @"Hi {0},
                                     You have a work item in your queue to attend.
                                     Details of Work Item.
                                     <WorkItem>
                                     Case No:{1}
                                     Current Status:{2}
                                     Assigned Date:{3}
                                     </Workitem>
                                     Thanks,
                                     {4}";

        }
    }
    //public enum Status
    //{
    //    Assigned=1,
    //    InProgess,
    //    Completed,
    //    End
    //}
}
