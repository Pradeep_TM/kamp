﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace WpfNavigationControl
{
    public class NavigationControlViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<NavigationControlModel> itemsCollection;
        private double itemControlRowSize { get; set; }
        public Action<string> LoadUserControl;
        public ObservableCollection<NavigationControlModel> ItemsCollection
        {
            get { return itemsCollection; }
            set
            {
                itemsCollection = value;
                RaisePropertyChanged("ItemsCollection");
            }
        }
        private ICommand _openpage;

        public ICommand OpenPage
        {
            get
            {
                if (_openpage == null)
                    _openpage = new DelegateCommand<string>((t) =>DisplayUserControl(t));
                return _openpage;
            }

        }

        private ICommand _collapseControl;

        public ICommand CollapseControl
        {
            get
            {
                if (_collapseControl == null)
                    _collapseControl = new DelegateCommand<ItemsControl>((t) => CollapseUncollaseItemControl(t));
                return _collapseControl;
            }

        }

        private ICommand _collapsePanel;

        public ICommand CollapsePanel
        {
            get
            {
                if (_collapsePanel == null)
                    _collapsePanel = new DelegateCommand<UserControl>((t) => CollapseUncollasePanel(t));
                return _collapsePanel;
            }

        }

        private void CollapseUncollasePanel(UserControl t)
        {
            
            if (t.Width != 20)
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 20;
                animation.From = 220;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.WidthProperty, animation);
            }

            else
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 220;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.WidthProperty, animation);
            }
        }


        private void CollapseUncollaseItemControl(ItemsControl t)
        {
            if (t.Height != 0)
            {
                itemControlRowSize = CalculateOneRowHeight(t);
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 0;
                animation.From = t.ActualHeight;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.HeightProperty, animation);
            }
            else
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = (double)Math.Ceiling((decimal)t.Items.Count / 2) * itemControlRowSize;
                animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 160));
                t.BeginAnimation(ItemsControl.HeightProperty, animation);
            }
        }

        private double CalculateOneRowHeight(ItemsControl t)
        {
            int rowCount = (int)Math.Ceiling((decimal)t.Items.Count / 2);
            return t.ActualHeight / rowCount;
        }

     
        private void DisplayUserControl(string uCName)
        {
            if (LoadUserControl != null)
                LoadUserControl(uCName);
        }

        public NavigationControlViewModel()
        {
            ItemsCollection = new ObservableCollection<NavigationControlModel>();
            BindData();
        }
        private void BindData()
        {
            var items3 = new List<NavigationItemModel>();
            items3.Add(new NavigationItemModel { UserControlName = "Icon1", ImageUrl = @"icon2.png" });
            items3.Add(new NavigationItemModel { UserControlName = "Icon2", ImageUrl = @"icon2.png" });
            items3.Add(new NavigationItemModel { UserControlName = "Icon3", ImageUrl = @"icon2.png" });

            var items2 = new List<NavigationItemModel>();
            items2.Add(new NavigationItemModel { UserControlName = "Icon4", ImageUrl = @"icon2.png" });
            items2.Add(new NavigationItemModel { UserControlName = "Icon5", ImageUrl = @"icon2.png" });


            var items4 = new List<NavigationItemModel>();
            items4.Add(new NavigationItemModel { UserControlName = "Icon6", ImageUrl = @"icon2.png" });
            items4.Add(new NavigationItemModel { UserControlName = "Icon7", ImageUrl = @"icon2.png" });
            items4.Add(new NavigationItemModel { UserControlName = "Icon8", ImageUrl = @"icon2.png" });
            items4.Add(new NavigationItemModel { UserControlName = "Icon9", ImageUrl = @"icon2.png" });

            ItemsCollection.Add(new NavigationControlModel { TabName = "Case Management", Items = items3 });
            ItemsCollection.Add(new NavigationControlModel { TabName = "Risk Asessment", Items = items2 });
            ItemsCollection.Add(new NavigationControlModel { TabName = "Reporting", Items = items4 });
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}

