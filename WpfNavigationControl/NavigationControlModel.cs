﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfNavigationControl
{
    public class NavigationControlModel : INotifyPropertyChanged
    {
        private string _tabName { get; set; }
        public string TabName
        {
            get { return _tabName; }
            set
            {
                _tabName = value;
                RaisePropertyChanged("TabName");
            }
        }
        private List<NavigationItemModel> _items { get; set; }
        public List<NavigationItemModel> Items
        {
            get { return _items; }
            set
            {
                _items = value;
              RaisePropertyChanged("Items");
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
    public class NavigationItemModel : INotifyPropertyChanged
    {
        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }
        private string _userControlName { get; set; }
        public string UserControlName
        {
            get { return _userControlName; }
            set
            {
                _userControlName = value;
                RaisePropertyChanged("UserControlName");
            }
        }
        public string ImageUrl { get; set; }

        #region INotifyPropertyChanged

       

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

     
}
