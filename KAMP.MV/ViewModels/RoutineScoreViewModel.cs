﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using Core.FrameworkComponents;
    using Core.Repository.MV;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using BLL;
    public class RoutineScoreViewModel : INotifyPropertyChanged
    {
        #region PrivateFields
        private Grid _rouitneScoreGd;
        private int _tranNo;
        private List<int> _routineIds;
        #endregion

        #region Contructor
        public RoutineScoreViewModel(int tranNo, Grid rouitneScoreGd)
        {
            this._rouitneScoreGd = rouitneScoreGd;
            this._tranNo = tranNo;
            GetRoutineScore();
        }
        #endregion

        private List<Routine> _rotineScores;
        public List<Routine> RoutineScores
        {
            get { return _rotineScores; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _rotineScores, (x) => x.RoutineScores);
                PropertyChanged.Raise(this, (x) => x.TotalScore);
            }
        }

        public int? TotalScore
        {
            get
            {
                return RoutineScores.Select(x => x.RoutineAccountScore).Sum(y => y);
            }
        }

        private ICommand _close;

        public ICommand Close
        {
            get
            {
                if (_close.IsNull())
                {
                    _close = new DelegateCommand(() =>
                    {
                        var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                        if (container.IsNotNull())
                        {
                            container.Children.Remove(_rouitneScoreGd);
                        }

                    });
                }
                return _close;

            }
        }
        private PaginationVM _pagination;
        public PaginationVM Pagination
        {
            get { return _pagination; }
            set { PropertyChanged.HandleValueChange(this, value, ref _pagination, (x) => x.Pagination); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region PrivateMethod
        private void GetRoutineScore()
        {
            // var displayItemCount = 10;
            // int totalCount;
            var bl = new ModelValidationBL();
            _routineIds = bl.GetRoutineIds(_tranNo);
            this.RoutineScores = bl.GetRoutineScore(_routineIds);
            //this.RoutineScores = bl.GetRoutineScore(_routineIds, 0, displayItemCount, out totalCount);
            //Pagination = new PaginationVM
            // {
            //     TotalItem = totalCount,
            //     StartIndex = RoutineScores.IsCollectionValid() ? 1 : 0,
            //     CurrentIndex = RoutineScores.IsCollectionValid() ? 1 : 0,
            //     DisplayitemCount = displayItemCount,
            //     PageClick = PageClick,
            // };
        }

        //private void PageClick()
        //{
        //    int skipItem = Pagination.StartIndex - 1;
        //    var bl = new ModelValidationBL();
        //    this.RoutineScores = bl.GetRoutineScore(_routineIds, skipItem, Pagination.DisplayitemCount);
        //}
        #endregion
    }
}
