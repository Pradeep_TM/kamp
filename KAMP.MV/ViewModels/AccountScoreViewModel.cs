﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;

    public class AccountScoreViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public AccountScoreViewModel()
        {
            PropertyChanged += AccountScoreViewModel_PropertyChanged;
        }

        void AccountScoreViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
           
        }

        #region BusinessViewModel

        private int _Id;
        public int Id
        {
            get
            {
                return _Id;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _Id, (x) => x.Id);
            }
        }

        private int? _tranNo;
        public int? TranNo
        {
            get
            {
                return _tranNo;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _tranNo, (x) => x.TranNo);
            }
        }

        private string _accountNumber;
        public string AccountNumber
        {
            get
            {
                return _accountNumber;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _accountNumber, (x) => x.AccountNumber);
            }
        }

        private int _accountScore;
        public int AccountScore
        {
            get
            {
                return _accountScore;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _accountScore, (x) => x.AccountScore);
            }
        }

        private int _createdBy;
        public int CreatedBy
        {
            get
            {
                Context context = System.Windows.Application.Current.Properties["Context"] as Context;
                return _createdBy = Convert.ToInt32(context.User.Id);
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy);
            }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate = System.DateTime.Now;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdDate, (x) => x.CreatedDate);
            }
        }

        private int _modifiedBy;
        public int ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedBy, (x) => x.ModifiedBy);
            }
        }

        private DateTime _modifiedDate;
        public DateTime ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedDate, (x) => x.ModifiedDate);
            }
        }

        #endregion
    }
}
