﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;

    public class StandardDeviationViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public StandardDeviationViewModel()
        {
            PropertyChanged += StandardDeviationViewModel_PropertyChanged;
        }

        void StandardDeviationViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        #region BusinessViewModel

        private double confidenceValue;
        public double ConfidenceValue
        {
            get { return confidenceValue; }
            set { confidenceValue = value; }
        }


        private double samplingValue;
        public double SamplingValue
        {
            get { return samplingValue; }
            set { samplingValue = value; }
        }

        private bool isConfidenceLevel99;
        public bool IsConfidenceLevel99
        {
            get
            {
                return isConfidenceLevel99;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isConfidenceLevel99, (x) => x.IsConfidenceLevel99);
                if (isConfidenceLevel99)
                    ConfidenceValue = 2.58;

            }
        }

        private bool isConfidenceLevel95;
        public bool IsConfidenceLevel95
        {
            get
            {
                return isConfidenceLevel95;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isConfidenceLevel95, (x) => x.IsConfidenceLevel95);
                if (IsConfidenceLevel95)
                    ConfidenceValue = 1.96;
            }
        }

        private bool isConfidenceLevel90;
        public bool IsConfidenceLevel90
        {
            get
            {
                return isConfidenceLevel90;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isConfidenceLevel90, (x) => x.IsConfidenceLevel90);
                if (IsConfidenceLevel90)
                    ConfidenceValue = 1.64;
            }
        }

        private bool isConfidenceLevel80;
        public bool IsConfidenceLevel80
        {
            get
            {
                return isConfidenceLevel80;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isConfidenceLevel80, (x) => x.IsConfidenceLevel80);
                if (IsConfidenceLevel80)
                    ConfidenceValue = 1.28;
            }
        }

        private bool isSamplingError3;
        public bool IsSamplingError3
        {
            get
            {
                return isSamplingError3;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isSamplingError3, (x) => x.IsSamplingError3);
                if (IsSamplingError3)
                    SamplingValue = .03;
            }
        }

        private bool isSamplingError5;
        public bool IsSamplingError5
        {
            get
            {
                return isSamplingError5;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isSamplingError5, (x) => x.IsSamplingError5);
                if (IsSamplingError5)
                    SamplingValue = .05;
            }
        }

        private bool isSamplingError7;
        public bool IsSamplingError7
        {
            get
            {
                return isSamplingError7;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isSamplingError7, (x) => x.IsSamplingError7);
                if (IsSamplingError7)
                    SamplingValue = .07;
            }
        }

        private bool isSamplingError10;
        public bool IsSamplingError10
        {
            get
            {
                return isSamplingError10;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref isSamplingError10, (x) => x.IsSamplingError10);
                if (IsSamplingError10)
                    SamplingValue = .1;
            }
        }

        private double standardDeviation;
        public double StandardDeviation
        {
            get
            {
                return standardDeviation;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref standardDeviation, (x) => x.StandardDeviation);
            }
        }


        private int population;
        public int Population
        {
            get
            {
                return population;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref population, (x) => x.Population);
            }
        }

        private double _sampleSize;
        public double SampleSize
        {
            get
            {
                return _sampleSize;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _sampleSize, (x) => x.SampleSize);
            }
        }

        #endregion
    }
}
