﻿using KAMP.Core.Repository.UM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using Core.FrameworkComponents;
    public class AssignReAssignViewModel : INotifyPropertyChanged
    {
        private List<User> _users;

        public List<User> Users
        {
            get { return _users; }
            set { PropertyChanged.HandleValueChange(this, value, ref _users, x => x.Users); }
        }

        private long _selectedUserId;

        public long SelectedUserId
        {
            get { return _selectedUserId; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedUserId, x => x.SelectedUserId); }
        }

        private AssignedRoutineViewModel _selectedCase;

        public AssignedRoutineViewModel SelectedCase
        {
            get { return _selectedCase; }
            set { PropertyChanged.HandleValueChange(this, value, ref _selectedCase, x => x.SelectedCase); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
