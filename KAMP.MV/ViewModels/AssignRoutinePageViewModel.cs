﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using AutoMapper;
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.FrameworkComponents.UserControls;
    using KAMP.Core.Repository.MV;
    using KAMP.Core.Repository.UM;
    using KAMP.MV.AppCode;
    using KAMP.MV.BLL;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;

    public class AssignRoutinePageViewModel : INotifyPropertyChanged, IPageControlContract
    {
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        public Action AnalystDropdownSelected;
        public uint _routineWithOutVariantsCount;

        public AssignRoutinePageViewModel()
        {
            PropertyChanged += AssignRoutineViewModel_PropertyChanged;
            _unAssignroutineViewModelCollection = new ObservableCollection<RoutineViewModel>();
            _assignedRoutineViewModelCollection = new ObservableCollection<AssignedRoutineViewModel>();
        }

        void AssignRoutineViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region BusinessViewModel

        private List<User> _analysts;
        [Required(ErrorMessage = "Select a Analyst.")]
        public List<User> Analysts
        {
            get
            {
                var _analysts = modelValidationBL.GetUsersFromAnalystGroup();
                _analysts.Insert(0, new User { UserId = 0, UserName= "Select" });
                return _analysts;                    
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _analysts, (x) => x.Analysts);
            }
        }

        private long _analystSelectedValue;
        public long AnalystSelectedValue
        {
            get { return _analystSelectedValue; }
            set 
            { 
                PropertyChanged.HandleValueChange(this, value, ref _analystSelectedValue, (x) => x.AnalystSelectedValue);

                if (AnalystSelectedValue != 0 && AnalystDropdownSelected !=null)
                {
                    this.AnalystDropdownSelected();
                }
            }
        }

        private List<User> _userStatusMappedList;
        public List<User> UserStatusMappedList
        {
            get { return _userStatusMappedList; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userStatusMappedList, x => x.UserStatusMappedList);
            }
        }

        private ObservableCollection<RoutineViewModel> _unAssignroutineViewModelCollection;
        public ObservableCollection<RoutineViewModel> UnAssignRoutineViewModelCollection
        {
            get
            {
                return _unAssignroutineViewModelCollection;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _unAssignroutineViewModelCollection, (x) => x.UnAssignRoutineViewModelCollection);
            }
        }

        private ObservableCollection<AssignedRoutineViewModel> _assignedRoutineViewModelCollection;
        public ObservableCollection<AssignedRoutineViewModel> AssignedRoutineViewModelCollection
        {
            get { return _assignedRoutineViewModelCollection; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assignedRoutineViewModelCollection, (x) => x.AssignedRoutineViewModelCollection); }
        }

        private AssignedRoutineViewModel _selectedItem;
        public AssignedRoutineViewModel SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }
        #endregion

        #region StylesViewModel

        private string _bdrBrushColor;
        public string BdrBrushColor
        {
            get
            {
                return _bdrBrushColor = "#FF82BCD6";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _bdrBrushColor, (x) => x.BdrBrushColor);
            }
        }

        private string _lblForegroundColor;
        public string LblForegroundColor
        {
            get
            {
                return _lblForegroundColor = "#0339b1";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lblForegroundColor, (x) => x.LblForegroundColor);
            }
        }
        #endregion

        #region IPageControlContract Members

        public uint GetTotalCount(object filterTag)
        {
            var bll = new ModelValidationBL();
            {
                _routineWithOutVariantsCount = bll.GetRoutineDetailsWithOutVariantsCount();
            }

            return _routineWithOutVariantsCount;
        }

        public ObservableCollection<object> GetRecordsBy(uint startingIndex, uint numberOfRecords, object filterTag)
        {
            var analystSelected = AnalystSelectedValue;
            ObservableCollection<object> result = new ObservableCollection<object>();
            if (startingIndex >= _routineWithOutVariantsCount)
            {
                return result;
            }

            var bll = new ModelValidationBL();
            {
                ObservableCollection<Routine> lstRoutinesWithoutVariants = bll.GetRoutinesWithoutVariantsByIndex(startingIndex, numberOfRecords, false, analystSelected);
                UnAssignRoutineViewModelCollection.Clear();

                if (lstRoutinesWithoutVariants.IsCollectionValid())
                {
                    new BootStrapper().Configure();
                    List<Routine> lstSortedRoutines = new List<Routine>();
                    
                    lstSortedRoutines.AddRange(lstRoutinesWithoutVariants.OrderBy(x => x.RoutineName));
                    lstSortedRoutines.Each(x => UnAssignRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));

                    foreach (var item in UnAssignRoutineViewModelCollection)
                    {
                        result.Add(item);
                    }
                }
            }
            return result;
        }
        #endregion
    }
}
