﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.FrameworkComponents.ViewModels;
    using System.Collections.ObjectModel;

    public class MVMainViewModel : INotifyPropertyChanged
    {
        public MVMainViewModel()
        {
            FillNavigationViewModel();
            PropertyChanged += MVMainViewModel_PropertyChanged;

            _headerViewModel = new HeaderViewModel();
            _routinePageViewModel = new RoutinePageViewModel();
            _assignRoutinePageViewModel = new AssignRoutinePageViewModel();
            _queryBuilderPageViewModel = new QueryBuilderPageViewModel();
            _analysisPageViewModel = new AnalysisPageViewModel();
        }

        private void MVMainViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        private string _currentPageName;
        public string CurrentPageName
        {
            get { return _currentPageName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _currentPageName, (x) => x.CurrentPageName); }
        }

        private HeaderViewModel _headerViewModel;
        public HeaderViewModel HeaderViewModel
        {
            get
            {
                return _headerViewModel;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _headerViewModel, x => x.HeaderViewModel);
            }
        }

        private NavigationControlViewModel _navigationViewModel;
        public NavigationControlViewModel NavigationViewModel
        {
            get
            {
                return _navigationViewModel;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _navigationViewModel, x => x.NavigationViewModel);
            }
        }

        private void FillNavigationViewModel()
        {
            NavigationViewModel = new NavigationControlViewModel();
            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "Data Management",
                Items = new List<NavigationItemModel> 
                {
                   new NavigationItemModel { UserControlName = MVPageControls.DataManagement, ImageUrl = @"../Assets/Images/analyst.png" ,EntityType="Transnorm"}
                }
            });  
            var menuItems = new List<NavigationItemModel>();            
            menuItems.Add(new NavigationItemModel { UserControlName = MVPageControls.ManageRoutine, ImageUrl = @"../Assets/Images/create-rule.png",EntityType="Routine" });
            menuItems.Add(new NavigationItemModel { UserControlName = MVPageControls.AssignRoutine, ImageUrl = @"../Assets/Images/assign-rule.png", EntityType = "MVCaseFile" });
            menuItems.Add(new NavigationItemModel { UserControlName = MVPageControls.QueryBuilder, ImageUrl = @"../Assets/Images/query-builder.png", EntityType = "SQLQuery" });
           
            var worklfowTab = new List<NavigationItemModel>();
            worklfowTab.Add(new NavigationItemModel() { UserControlName = MVPageControls.ManageWorkflow, ImageUrl = @"../Assets/Images/CreateWorkflow.png" , EntityType = "WFDefinition"});
            worklfowTab.Add(new NavigationItemModel() { UserControlName = MVPageControls.ManageState, ImageUrl = @"../Assets/Images/State.png", EntityType = "State" });
            worklfowTab.Add(new NavigationItemModel() { UserControlName = MVPageControls.WorkflowMonitoring, ImageUrl = @"../Assets/Images/monitoring.png", EntityType = "Assignment" });

          
            NavigationViewModel.PinImage = @"../Assets/Images/pin.png";
            NavigationViewModel.UnPinImage = @"../Assets/Images/unpin.png";
            NavigationViewModel.ArrowLeftNavigation = @"../Assets/Images/arrowleftnavigation.png";
            NavigationViewModel.DarkThemeColor = "#3468B1";
            NavigationViewModel.ContentBackgroundColor = "#E8EAF6";
            NavigationViewModel.LightThemeColor = "#3D75BF";
            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "Routine Management", Items = menuItems });
            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "Workflow", Items = worklfowTab });
            NavigationViewModel.ItemsCollection.Add(new NavigationControlModel
            {
                TabName = "Reporting Service",
                Items = new List<NavigationItemModel> 
                {
                   new NavigationItemModel { UserControlName = MVPageControls.Report, ImageUrl = @"../Assets/Images/analyst.png", EntityType = "CaseAgingReportDetails" }
                }
            });  
        }

        //Routine
        private RoutinePageViewModel _routinePageViewModel;
        public RoutinePageViewModel RoutinePageViewModel
        {
            get { return _routinePageViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _routinePageViewModel, x => x.RoutinePageViewModel); }
        }

        //Assign Routines
        private AssignRoutinePageViewModel _assignRoutinePageViewModel;
        public AssignRoutinePageViewModel AssignRoutinePageViewModel
        {
            get { return _assignRoutinePageViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _assignRoutinePageViewModel, x => x.AssignRoutinePageViewModel); }
        }

        //Query Builder
        private QueryBuilderPageViewModel _queryBuilderPageViewModel;
        public QueryBuilderPageViewModel QueryBuilderPageViewModel
        {
            get { return _queryBuilderPageViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _queryBuilderPageViewModel, x => x.QueryBuilderPageViewModel); }
        }

        //Analysis - Standard Deviation
        private AnalysisPageViewModel _analysisPageViewModel;
        public AnalysisPageViewModel AnalysisPageViewModel
        {
            get { return _analysisPageViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _analysisPageViewModel, x => x.AnalysisPageViewModel); }
        }

        //Pagination VM
        private PaginationVM _paginationVM;
        public PaginationVM PaginationVM
        {
            get { return _paginationVM;}
            set { PropertyChanged.HandleValueChange(this, value, ref _paginationVM, x => x.PaginationVM); }
        }
       //Bkp Assignecases
        private ObservableCollection<AssignedRoutineViewModel> _bkpassignedRoutineViewModelCollection;
        public ObservableCollection<AssignedRoutineViewModel> BkpAssignedRoutineViewModelCollection
        {
            get { return _bkpassignedRoutineViewModelCollection; }
            set { PropertyChanged.HandleValueChange(this, value, ref _bkpassignedRoutineViewModelCollection, (x) => x.BkpAssignedRoutineViewModelCollection); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
