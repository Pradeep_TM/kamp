﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;

    public class MVTransNormViewModel : INotifyPropertyChanged, IEqualityComparer<MVTransNormViewModel>
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public MVTransNormViewModel()
        {
            PropertyChanged += MVTransNormViewModel_PropertyChanged;
        }

        private void MVTransNormViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        #region BusinessViewModel
        private int _Id;
        public int Id
        {
            get
            {
                return _Id;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _Id, (x) => x.Id);
            }
        }

        private int _tranNo;
        public int TranNo
        {
            get
            {
                return _tranNo;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _tranNo, (x) => x.TranNo);
            }
        }

        private string _internalReferenceNumber;
        public string InternalReferenceNumber
        {
            get
            {
                return _internalReferenceNumber;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _internalReferenceNumber, (x) => x.InternalReferenceNumber);
            }
        }

        private string _messageType;
        public string MessageType
        {
            get
            {
                return _messageType;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _messageType, (x) => x.MessageType);
            }
        }

        private string _paymentType;
        public string PaymentType
        {
            get
            {
                return _paymentType;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _paymentType, (x) => x.PaymentType);
            }
        }

        private Decimal? _paymentAmount;
        public Decimal? PaymentAmount
        {
            get
            {
                return _paymentAmount;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _paymentAmount, (x) => x.PaymentAmount);
            }
        }

        private DateTime? _transDate;
        public DateTime? TransDate
        {
            get
            {
                return _transDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _transDate, (x) => x.TransDate);
            }
        }

        private DateTime? _valueDate;
        public DateTime? ValueDate
        {
            get
            {
                return _valueDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _valueDate, (x) => x.ValueDate);
            }
        }

        private int? _recvPay;
        public int? RecvPay
        {
            get
            {
                return _recvPay;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _recvPay, (x) => x.RecvPay);
            }
        }

        private string _partyType;
        public string PartyType
        {
            get
            {
                return _partyType;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _partyType, (x) => x.PartyType);
            }
        }

        private string _sourceCode;
        public string SourceCode
        {
            get
            {
                return _sourceCode;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _sourceCode, (x) => x.SourceCode);
            }
        }

        private string _creditAdviceType;
        public string CreditAdviceType
        {
            get
            {
                return _creditAdviceType;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditAdviceType, (x) => x.CreditAdviceType);
            }
        }

        private string _mtReference;
        public string MTReference
        {
            get
            {
                return _mtReference;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _mtReference, (x) => x.MTReference);
            }
        }

        private string _fileName;
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _fileName, (x) => x.FileName);
            }
        }

        private string _customerReference;
        public string CustomerReference
        {
            get
            {
                return _customerReference;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _customerReference, (x) => x.CustomerReference);
            }
        }

        private string _customerAccount1;
        public string CustomerAccount1
        {
            get
            {
                return _customerAccount1;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _customerAccount1, (x) => x.CustomerAccount1);
            }
        }

        private string _customerAccount2;
        public string CustomerAccount2
        {
            get
            {
                return _customerAccount2;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _customerAccount2, (x) => x.CustomerAccount2);
            }
        }

        private string _byOrderPartyID;
        public string ByOrderPartyID
        {
            get
            {
                return _byOrderPartyID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyID, (x) => x.ByOrderPartyID);
            }
        }

        private string _byOrderAcct;
        public string ByOrderAcct
        {
            get
            {
                return _byOrderAcct;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderAcct, (x) => x.ByOrderAcct);
            }
        }

        private string _byOrderPartyBIC;
        public string ByOrderPartyBIC
        {
            get
            {
                return _byOrderPartyBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyBIC, (x) => x.ByOrderPartyBIC);
            }
        }

        private string _byOrderPartyABA;
        public string ByOrderPartyABA
        {
            get
            {
                return _byOrderPartyABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyABA, (x) => x.ByOrderPartyABA);
            }
        }

        private string _byOrderPartyName;
        public string ByOrderPartyName
        {
            get
            {
                return _byOrderPartyName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyName, (x) => x.ByOrderPartyName);
            }
        }

        private string _byOrderPartyNameNorm;
        public string ByOrderPartyNameNorm
        {
            get
            {
                return _byOrderPartyNameNorm;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyNameNorm, (x) => x.ByOrderPartyNameNorm);
            }
        }

        private string _byOrderPartyAddress;
        public string ByOrderPartyAddress
        {
            get
            {
                return _byOrderPartyAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyAddress, (x) => x.ByOrderPartyAddress);
            }
        }

        private string _byOrderPartyStateName;
        public string ByOrderPartyStateName
        {
            get
            {
                return _byOrderPartyStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyStateName, (x) => x.ByOrderPartyStateName);
            }
        }

        private string _byOrderPartyCountryName;
        public string ByOrderPartyCountryName
        {
            get
            {
                return _byOrderPartyCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _byOrderPartyCountryName, (x) => x.ByOrderPartyCountryName);
            }
        }

        private string _benePartyID;
        public string BenePartyID
        {
            get
            {
                return _benePartyID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyID, (x) => x.BenePartyID);
            }
        }

        private string _benePartyAcct;
        public string BenePartyAcct
        {
            get
            {
                return _benePartyAcct;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyAcct, (x) => x.BenePartyAcct);
            }
        }

        private string _benePartyBIC;
        public string BenePartyBIC
        {
            get
            {
                return _benePartyBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyBIC, (x) => x.BenePartyBIC);
            }
        }

        private string _benePartyABA;
        public string BenePartyABA
        {
            get
            {
                return _benePartyABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyABA, (x) => x.BenePartyABA);
            }
        }

        private string _benePartyName;
        public string BenePartyName
        {
            get
            {
                return _benePartyName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyName, (x) => x.BenePartyName);
            }
        }

        private string _benePartyNameNorm;
        public string BenePartyNameNorm
        {
            get
            {
                return _benePartyNameNorm;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyNameNorm, (x) => x.BenePartyNameNorm);
            }
        }

        private string _benePartyAddress;
        public string BenePartyAddress
        {
            get
            {
                return _benePartyAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyAddress, (x) => x.BenePartyAddress);
            }
        }

        private string _benePartyStateName;
        public string BenePartyStateName
        {
            get
            {
                return _benePartyStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyStateName, (x) => x.BenePartyStateName);
            }
        }

        private string _benePartyCountryName;
        public string BenePartyCountryName
        {
            get
            {
                return _benePartyCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _benePartyCountryName, (x) => x.BenePartyCountryName);
            }
        }

        private string _creditBankID;
        public string CreditBankID
        {
            get
            {
                return _creditBankID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankID, (x) => x.CreditBankID);
            }
        }

        private string _creditBankAcct;
        public string CreditBankAcct
        {
            get
            {
                return _creditBankAcct;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankAcct, (x) => x.CreditBankAcct);
            }
        }

        private string _creditBankBIC;
        public string CreditBankBIC
        {
            get
            {
                return _creditBankBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankBIC, (x) => x.CreditBankAcct);
            }
        }

        private string _creditBankABA;
        public string CreditBankABA
        {
            get
            {
                return _creditBankABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankABA, (x) => x.CreditBankABA);
            }
        }

        private string _creditBankName;
        public string CreditBankName
        {
            get
            {
                return _creditBankName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankName, (x) => x.CreditBankName);
            }
        }

        private string _creditBankAddress;
        public string CreditBankAddress
        {
            get
            {
                return _creditBankAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankAddress, (x) => x.CreditBankAddress);
            }
        }

        private string _creditBankStateName;
        public string CreditBankStateName
        {
            get
            {
                return _creditBankStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankStateName, (x) => x.CreditBankStateName);
            }
        }

        private string _creditBankCountryName;
        public string CreditBankCountryName
        {
            get
            {
                return _creditBankCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankCountryName, (x) => x.CreditBankCountryName);
            }
        }

        private string _creditBankAdvice;
        public string CreditBankAdvice
        {
            get
            {
                return _creditBankAdvice;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankAdvice, (x) => x.CreditBankAdvice);
            }
        }

        private string _creditBankAdviceCountryName;
        public string CreditBankAdviceCountryName
        {
            get
            {
                return _creditBankAdviceCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankAdviceCountryName, (x) => x.CreditBankAdviceCountryName);
            }
        }

        private string _creditBankInstructions;
        public string CreditBankInstructions
        {
            get
            {
                return _creditBankInstructions;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankInstructions, (x) => x.CreditBankInstructions);
            }
        }

        private string _creditBankInstructionsCountryName;
        public string CreditBankInstructionsCountryName
        {
            get
            {
                return _creditBankInstructionsCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _creditBankInstructionsCountryName, (x) => x.CreditBankInstructionsCountryName);
            }
        }

        private string _intermediaryBankID;
        public string IntermediaryBankID
        {
            get
            {
                return _intermediaryBankID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankID, (x) => x.IntermediaryBankID);
            }
        }

        private string _intermediaryBankAcct;
        public string IntermediaryBankAcct
        {
            get
            {
                return _intermediaryBankAcct;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankAcct, (x) => x.IntermediaryBankAcct);
            }
        }

        private string _intermediaryBankBIC;
        public string IntermediaryBankBIC
        {
            get
            {
                return _intermediaryBankBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankBIC, (x) => x.IntermediaryBankBIC);
            }
        }

        private string _intermediaryBankABA;
        public string IntermediaryBankABA
        {
            get
            {
                return _intermediaryBankABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankABA, (x) => x.IntermediaryBankABA);
            }
        }

        private string _intermediaryBankName;
        public string IntermediaryBankName
        {
            get
            {
                return _intermediaryBankName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankName, (x) => x.IntermediaryBankName);
            }
        }

        private string _intermediaryBankNameNorm;
        public string IntermediaryBankNameNorm
        {
            get
            {
                return _intermediaryBankNameNorm;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankNameNorm, (x) => x.IntermediaryBankNameNorm);
            }
        }

        private string _intermediaryBankAddress;
        public string IntermediaryBankAddress
        {
            get
            {
                return _intermediaryBankAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankAddress, (x) => x.IntermediaryBankAddress);
            }
        }

        private string _intermediaryBankStateName;
        public string IntermediaryBankStateName
        {
            get
            {
                return _intermediaryBankStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankStateName, (x) => x.IntermediaryBankStateName);
            }
        }

        private string _intermediaryBankCountryName;
        public string IntermediaryBankCountryName
        {
            get
            {
                return _intermediaryBankCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _intermediaryBankCountryName, (x) => x.IntermediaryBankCountryName);
            }
        }

        private string _beneficiaryBankID;
        public string BeneficiaryBankID
        {
            get
            {
                return _beneficiaryBankID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankID, (x) => x.BeneficiaryBankID);
            }
        }

        private string _beneficiaryBankABA;
        public string BeneficiaryBankABA
        {
            get
            {
                return _beneficiaryBankABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankABA, (x) => x.BeneficiaryBankABA);
            }
        }

        private string _beneficiaryBankBIC;
        public string BeneficiaryBankBIC
        {
            get
            {
                return _beneficiaryBankBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankBIC, (x) => x.BeneficiaryBankBIC);
            }
        }

        private string _beneficiaryBankName;
        public string BeneficiaryBankName
        {
            get
            {
                return _beneficiaryBankName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankName, (x) => x.BeneficiaryBankName);
            }
        }

        private string _beneficiaryBankNameNorm;
        public string BeneficiaryBankNameNorm
        {
            get
            {
                return _beneficiaryBankNameNorm;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankNameNorm, (x) => x.BeneficiaryBankNameNorm);
            }
        }

        private string _beneficiaryBankAddress;
        public string BeneficiaryBankAddress
        {
            get
            {
                return _beneficiaryBankAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankAddress, (x) => x.BeneficiaryBankAddress);
            }
        }

        private string _beneficiaryBankStateName;
        public string BeneficiaryBankStateName
        {
            get
            {
                return _beneficiaryBankStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankStateName, (x) => x.BeneficiaryBankStateName);
            }
        }

        private string _beneficiaryBankCountryName;
        public string BeneficiaryBankCountryName
        {
            get
            {
                return _beneficiaryBankCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankCountryName, (x) => x.BeneficiaryBankCountryName);
            }
        }

        private string _beneficiaryBankAdvice;
        public string BeneficiaryBankAdvice
        {
            get
            {
                return _beneficiaryBankAdvice;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankAdvice, (x) => x.BeneficiaryBankAdvice);
            }
        }

        private string _beneficiaryBankAdviceCountryName;
        public string BeneficiaryBankAdviceCountryName
        {
            get
            {
                return _beneficiaryBankAdviceCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _beneficiaryBankAdviceCountryName, (x) => x.BeneficiaryBankAdviceCountryName);
            }
        }

        private string _debitBankID;
        public string DebitBankID
        {
            get
            {
                return _debitBankID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankID, (x) => x.DebitBankID);
            }
        }

        private string _debitBankAcct;
        public string DebitBankAcct
        {
            get
            {
                return _debitBankAcct;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankAcct, (x) => x.DebitBankAcct);
            }
        }

        private string _debitBankBIC;
        public string DebitBankBIC
        {
            get
            {
                return _debitBankBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankBIC, (x) => x.DebitBankBIC);
            }
        }

        private string _debitBankABA;
        public string DebitBankABA
        {
            get
            {
                return _debitBankABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankABA, (x) => x.DebitBankABA);
            }
        }

        private string _debitBankName;
        public string DebitBankName
        {
            get
            {
                return _debitBankName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankName, (x) => x.DebitBankName);
            }
        }

        private string _debitBankNameNorm;
        public string DebitBankNameNorm
        {
            get
            {
                return _debitBankNameNorm;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankNameNorm, (x) => x.DebitBankNameNorm);
            }
        }

        private string _debitBankAddress;
        public string DebitBankAddress
        {
            get
            {
                return _debitBankAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankAddress, (x) => x.DebitBankAddress);
            }
        }

        private string _debitBankStateName;
        public string DebitBankStateName
        {
            get
            {
                return _debitBankStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankStateName, (x) => x.DebitBankStateName);
            }
        }

        private string _debitBankCountryName;
        public string DebitBankCountryName
        {
            get
            {
                return _debitBankCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankCountryName, (x) => x.DebitBankCountryName);
            }
        }

        private string _debitBankInstructions;
        public string DebitBankInstructions
        {
            get
            {
                return _debitBankInstructions;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankInstructions, (x) => x.DebitBankInstructions);
            }
        }

        private string _debitBankInstructionsCountryName;
        public string DebitBankInstructionsCountryName
        {
            get
            {
                return _debitBankInstructionsCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _debitBankInstructionsCountryName, (x) => x.DebitBankInstructionsCountryName);
            }
        }

        private string _originatingBankID;
        public string OriginatingBankID
        {
            get
            {
                return _originatingBankID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankID, (x) => x.OriginatingBankID);
            }
        }

        private string _originatingBankAcct;
        public string OriginatingBankAcct
        {
            get
            {
                return _originatingBankAcct;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankAcct, (x) => x.OriginatingBankAcct);
            }
        }

        private string _originatingBankBIC;
        public string OriginatingBankBIC
        {
            get
            {
                return _originatingBankBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankBIC, (x) => x.OriginatingBankBIC);
            }
        }

        private string _originatingBankABA;
        public string OriginatingBankABA
        {
            get
            {
                return _originatingBankABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankABA, (x) => x.OriginatingBankABA);
            }
        }

        private string _originatingBankName;
        public string OriginatingBankName
        {
            get
            {
                return _originatingBankName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankName, (x) => x.OriginatingBankName);
            }
        }

        private string _originatingBankNameNorm;
        public string OriginatingBankNameNorm
        {
            get
            {
                return _originatingBankNameNorm;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankNameNorm, (x) => x.OriginatingBankNameNorm);
            }
        }

        private string _originatingBankAddress;
        public string OriginatingBankAddress
        {
            get
            {
                return _originatingBankAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankAddress, (x) => x.OriginatingBankAddress);
            }
        }

        private string _originatingBankStateName;
        public string OriginatingBankStateName
        {
            get
            {
                return _originatingBankStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankStateName, (x) => x.OriginatingBankStateName);
            }
        }

        private string _originatingBankCountryName;
        public string OriginatingBankCountryName
        {
            get
            {
                return _originatingBankCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatingBankCountryName, (x) => x.OriginatingBankCountryName);
            }
        }

        private string _originatortoBeneInfo;
        public string OriginatortoBeneInfo
        {
            get
            {
                return _originatortoBeneInfo;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatortoBeneInfo, (x) => x.OriginatortoBeneInfo);
            }
        }

        private string _originatortoBeneInfoCountryName;
        public string OriginatortoBeneInfoCountryName
        {
            get
            {
                return _originatortoBeneInfoCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _originatortoBeneInfoCountryName, (x) => x.OriginatortoBeneInfoCountryName);
            }
        }

        private string _banktoBeneInfo;
        public string BanktoBeneInfo
        {
            get
            {
                return _banktoBeneInfo;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _banktoBeneInfo, (x) => x.BanktoBeneInfo);
            }
        }

        private string _bankToBeneInfoCountryName;
        public string BankToBeneInfoCountryName
        {
            get
            {
                return _bankToBeneInfoCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _bankToBeneInfoCountryName, (x) => x.BankToBeneInfoCountryName);
            }
        }

        private string _instructingBankID;
        public string InstructingBankID
        {
            get
            {
                return _instructingBankID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankID, (x) => x.InstructingBankID);
            }
        }

        private string _instructingBankAcct;
        public string InstructingBankAcct
        {
            get
            {
                return _instructingBankAcct;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankAcct, (x) => x.InstructingBankAcct);
            }
        }

        private string _instructingBankBIC;
        public string InstructingBankBIC
        {
            get
            {
                return _instructingBankBIC;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankBIC, (x) => x.InstructingBankBIC);
            }
        }

        private string _instructingBankABA;
        public string InstructingBankABA
        {
            get
            {
                return _instructingBankABA;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankABA, (x) => x.InstructingBankABA);
            }
        }

        private string _instructingBankName;
        public string InstructingBankName
        {
            get
            {
                return _instructingBankName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankName, (x) => x.InstructingBankName);
            }
        }

        private string _instructingBankNameNorm;
        public string InstructingBankNameNorm
        {
            get
            {
                return _instructingBankNameNorm;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankNameNorm, (x) => x.InstructingBankNameNorm);
            }
        }

        private string _instructingBankAddress;
        public string InstructingBankAddress
        {
            get
            {
                return _instructingBankAddress;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankAddress, (x) => x.InstructingBankAddress);
            }
        }

        private string _instructingBankStateName;
        public string InstructingBankStateName
        {
            get
            {
                return _instructingBankStateName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankStateName, (x) => x.InstructingBankStateName);
            }
        }

        private string _instructingBankCountryName;
        public string InstructingBankCountryName
        {
            get
            {
                return _instructingBankCountryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _instructingBankCountryName, (x) => x.InstructingBankCountryName);
            }
        }

        private int? _caseCount;
        public int? CaseCount
        {
            get
            {
                return _caseCount;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseCount, (x) => x.CaseCount);
            }
        }

        private bool? _caseCheck;
        public bool? CaseCheck
        {
            get
            {
                return _caseCheck;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseCheck, (x) => x.CaseCheck);
            }
        }
        #endregion

        public bool Equals(MVTransNormViewModel x, MVTransNormViewModel y)
        {
            if(x == null || y == null)
            {
                return false;
            }
            return x.Id == y.Id;
        }

        public int GetHashCode(MVTransNormViewModel obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
