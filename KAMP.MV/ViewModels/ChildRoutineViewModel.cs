﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using Core.FrameworkComponents;
    using KAMP.MV.AppCode.Enums;
    using System.Windows;
    public class ChildRoutineViewModel : INotifyPropertyChanged
    {
        #region BusinessViewModel
        private int _routineID;
        public int RoutineID
        {
            get
            {
                return _routineID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineID, (x) => x.RoutineID);
            }
        }

        private int _parentroutineID;
        public int ParentRoutineID
        {
            get
            {
                return _parentroutineID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _parentroutineID, (x) => x.ParentRoutineID);
            }
        }

        private string _routineName;
        public string RoutineName
        {
            get
            {
                return _routineName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineName, (x) => x.RoutineName);
            }
        }

        private string _routineDesc;
        public string RoutineDescription
        {
            get
            {
                return _routineDesc;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineDesc, (x) => x.RoutineDescription);
            }
        }

        //display of sourcetype in grid
        private SourceTypeEnum _sourceTypeValue;
        public SourceTypeEnum SourceTypeValue
        {
            get
            {
                return _sourceTypeValue;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _sourceTypeValue, (x) => x.SourceTypeValue);
            }
        }

        private int _routineScore;
        public int RoutineScore
        {
            get
            {
                return _routineScore;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineScore, (x) => x.RoutineScore);
            }
        }

        private int _createdBy;
        public int CreatedBy
        {
            get
            {
                Context context = Application.Current.Properties["Context"] as Context;
                return _createdBy = Convert.ToInt32(context.User.Id);
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy);
            }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate = System.DateTime.Now;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdDate, (x) => x.CreatedDate);
            }
        }

        private DateTime _modifiedDate;
        public DateTime ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedDate, (x) => x.ModifiedDate);
            }
        }

        private int _modifiedBy;
        public int ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedBy, (x) => x.ModifiedBy);
            }
        }


        private bool? _variantValue;
        public bool? VariantValue
        {
            get { return _variantValue; }
            set { PropertyChanged.HandleValueChange(this, value, ref _variantValue, (x) => x.VariantValue); }
        }

        //assign routine ischecked checkedbox.
        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref isChecked, (x) => x.IsChecked); }
        }

        //sql query builder routine checked.
        private bool isRoutineChecked;
        public bool IsRoutineChecked
        {
            get { return isRoutineChecked; }
            set
            {
                if (PropertyChanged.HandleValueChange(this, value, ref isRoutineChecked, (x) => x.IsRoutineChecked) && IsRoutineChecked && OnRoutineChecked.IsNotNull())
                {
                    OnRoutineChecked(RoutineID);
                };
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Action<int> OnRoutineChecked;
    }
        #endregion
}
