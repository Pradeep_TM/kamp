﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    public static class MVPageControls
    {
        public const string Home = "Model Validation";

        public const string ManageRoutine = "Manage Routine";

        public const string AssignRoutine = "Assign Routine";

        public const string QueryBuilder = "Query Builder";

        public const string Analysis = "Analysis";

        public const string Report = "Report";

        public const string ManageWorkflow = "Manage Workflow";

        public const string ManageState = "Manage State";

        public const string WorkflowMonitoring = "Workflow Monitoring";

        public const string DataManagement = "Data Management";

    }
}
