﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using System.Collections.ObjectModel;

    public class AnalysisPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public AnalysisPageViewModel()
        {
            PropertyChanged += AnalysisPageViewModel_PropertyChanged;
            _accountScoreViewModel = new AccountScoreViewModel();
            _standardDeviationViewModel = new StandardDeviationViewModel();
            _analysisResultViewModelCollection = new ObservableCollection<SQLQueryViewModel>();
        }

        void AnalysisPageViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        #region BusinessViewModel

        private ObservableCollection<SQLQueryViewModel> _analysisResultViewModelCollection;
        public ObservableCollection<SQLQueryViewModel> AnalysisResultViewModelCollection
        {
            get
            {
                return _analysisResultViewModelCollection;
            }
            set
            {
                if (_analysisResultViewModelCollection != value)
                {
                    _analysisResultViewModelCollection = value;
                    PropertyChanged(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.AnalysisResultViewModelCollection)));
                }
            }
        }

        private AccountScoreViewModel _accountScoreViewModel;
        public AccountScoreViewModel AccountScoreViewModel
        {
            get { return _accountScoreViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _accountScoreViewModel, (x) => x.AccountScoreViewModel); }
        }

        private StandardDeviationViewModel _standardDeviationViewModel;
        public StandardDeviationViewModel StandardDeviationViewModel
        {
            get { return _standardDeviationViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _standardDeviationViewModel, (x) => x.StandardDeviationViewModel); }
        }

        private bool isCurrentAssignedUser;
        public bool IsCurrentAssignedUser
        {
            get { return isCurrentAssignedUser; }
            set { PropertyChanged.HandleValueChange(this, value, ref isCurrentAssignedUser, (x) => x.IsCurrentAssignedUser); }
        }


        public Action BackButtonClick;
        #endregion
    }
}
