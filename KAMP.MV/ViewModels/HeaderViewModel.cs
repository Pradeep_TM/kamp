﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;

namespace KAMP.MV.ViewModels
{
    public class HeaderViewModel : INotifyPropertyChanged
    {
        private string _imgModule;
        private string _lightThemeColor;
        private string _darkThemeColor;
        private string _moduleName;
        private string _currentContent;
        private string _contentBackgroundColor;

        public HeaderViewModel()
        {
            ModuleName = "Model Validation";
            ImgModule = "../Assets/Images/mv_logo1.png";
            _darkThemeColor = "#3468B1";
            _lightThemeColor = "#3D75BF";
            _contentBackgroundColor = "#E8EAF6";
        }

        public string ContentBackgroundColor
        {
            get { return _contentBackgroundColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _contentBackgroundColor, x => x.ContentBackgroundColor);
            }
        }

        public string DarkThemeColor
        {
            get { return _darkThemeColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _darkThemeColor, x => x.DarkThemeColor);
            }
        }

        public string LightThemeColor
        {
            get { return _lightThemeColor; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lightThemeColor, x => x.LightThemeColor);
            }
        }

        public string CurrentContent
        {
            get { return _currentContent; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _currentContent, x => x.CurrentContent);
            }
        }

        public string ModuleName
        {
            get { return _moduleName; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleName, x => x.ModuleName);
            }
        }

        public string ImgModule
        {
            get { return _imgModule; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _imgModule, x => x.ImgModule);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

}
