﻿using KAMP.Core.FrameworkComponents.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.MV;
    using KAMP.Core.Workflow;
    using KAMP.MV.BLL;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    public class QueryBuilderPageViewModel : INotifyPropertyChanged
    {
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        public event PropertyChangedEventHandler PropertyChanged;
        public Action CaseNumbersDropdownSelected;
        public Action<int> RoutineDropdownSelected;

        public QueryBuilderPageViewModel()
        {
            PropertyChanged += QueryBuilderPageViewModel_PropertyChanged;
            _assignedRoutineViewModelCollection = new ObservableCollection<RoutineViewModel>();
            _databaseViewModelCollection = new ObservableCollection<DatabaseTablesViewModel>();
            _tablesSelectedViewModelCollection = new ObservableCollection<DatabaseTablesViewModel>();
            _sqlQueryViewModel = new SQLQueryViewModel();
            _queryResultViewModelCollection = new ObservableCollection<SQLQueryViewModel>();
        }

        void QueryBuilderPageViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        #region StylesViewModel

        private string _bdrBrushColor;
        public string BdrBrushColor
        {
            get
            {
                return _bdrBrushColor = "#FF82BCD6";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _bdrBrushColor, (x) => x.BdrBrushColor);
            }
        }

        private string _lblForegroundColor;
        public string LblForegroundColor
        {
            get
            {
                return _lblForegroundColor = "#0339b1";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lblForegroundColor, (x) => x.LblForegroundColor);
            }
        }
        #endregion

        #region BusinessViewModel

        //get the case number in the dropdown.
        private List<CaseDetail> _caseNumbers;
        public List<CaseDetail> CaseNumbers
        {
            get
            {
                return _caseNumbers;
            }
            set { PropertyChanged.HandleValueChange(this, value, ref _caseNumbers, (x) => x.CaseNumbers); }
        }

        //routine dropdown selected value is passed for filter/search
        private string _caseNumberSelectedValue;
        public string CaseNumberSelectedValue
        {
            get { return _caseNumberSelectedValue; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseNumberSelectedValue, (x) => x.CaseNumberSelectedValue);

                if (CaseNumbersDropdownSelected.IsNotNull() && _caseNumberSelectedValue.IsNotNull())
                {
                    this.CaseNumbersDropdownSelected();
                }
            }
        }

        private int _routineSelectedValue;
        public int RoutineSelectedValue
        {
            get { return _routineSelectedValue; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineSelectedValue, (x) => x.RoutineSelectedValue);  
             
                if(RoutineDropdownSelected.IsNotNull())
                {
                    this.RoutineDropdownSelected(RoutineSelectedValue);
                }
            }
        }

       // public string OnCaseNumberSelectedValue { get; set; }

        private int _routineScore;
        public int RoutineScore
        {
            get
            {
                return _routineScore;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineScore, (x) => x.RoutineScore);
            }
        }

        private ObservableCollection<RoutineViewModel> _assignedRoutineViewModelCollection;
        public ObservableCollection<RoutineViewModel> AssignedRoutineViewModelCollection
        {
            get
            {
                return _assignedRoutineViewModelCollection;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _assignedRoutineViewModelCollection, (x) => x.AssignedRoutineViewModelCollection);
            }
        }

        private ObservableCollection<DatabaseTablesViewModel> _databaseViewModelCollection;
        public ObservableCollection<DatabaseTablesViewModel> DatabaseViewModelCollection
        {
            get
            {
                return _databaseViewModelCollection;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _databaseViewModelCollection, (x) => x.DatabaseViewModelCollection);
            }
        }


        private ObservableCollection<DatabaseTablesViewModel> _tablesSelectedViewModelCollection;
        public ObservableCollection<DatabaseTablesViewModel> TablesSelectedViewModelCollection
        {
            get
            {
                return _tablesSelectedViewModelCollection;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _tablesSelectedViewModelCollection, (x) => x.TablesSelectedViewModelCollection);
            }
        }

        private SQLQueryViewModel _sqlQueryViewModel;
        public SQLQueryViewModel SQLQueryViewModel
        {
            get { return _sqlQueryViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _sqlQueryViewModel, (x) => x.SQLQueryViewModel); }
        }

        private ObservableCollection<SQLQueryViewModel> _queryResultViewModelCollection;
        public ObservableCollection<SQLQueryViewModel> QueryResultViewModelCollection
        {
            get
            {
                return _queryResultViewModelCollection;
            }
            set
            {
                if (_queryResultViewModelCollection != value)
                {
                    _queryResultViewModelCollection = value;
                    PropertyChanged(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.QueryResultViewModelCollection)));
                }
            }
        }
        private string _querySyntax;

        public string QuerySyntax
        {
            get { return _querySyntax; }
            set {PropertyChanged.HandleValueChange(this, value, ref _querySyntax, (x) => x.QuerySyntax);  }
        }
        
        private SQLQueryViewModel _selectedSQLQueryViewModel;
        public SQLQueryViewModel SelectedSQLQueryViewModel
        {
            get { return _selectedSQLQueryViewModel; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedSQLQueryViewModel, (x) => x.SelectedSQLQueryViewModel);
                if (TabSelectionChanged.IsNotNull())
                    TabSelectionChanged();
            }
        }


        public Action TabSelectionChanged;
        private bool? _isOpen;
        public bool? IsOpen
        {
            get { return _isOpen; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isOpen, (x) => x.IsOpen); }
        }

        private bool? _isAssignedUser;
        public bool? IsAssignedUser
        {
            get { return _isAssignedUser; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isAssignedUser, (x) => x.IsAssignedUser); }
        }
        #endregion

        #region UIActions

        public Action OnSelectClick;
        private ICommand _select;
        public ICommand Select
        {
            get
            {
                if (_select == null)
                    _select = new DelegateCommand(() =>
                    {
                        if (OnSelectClick.IsNotNull())
                            OnSelectClick();
                    });
                return _select;
            }
        }
        #endregion
    }
}
