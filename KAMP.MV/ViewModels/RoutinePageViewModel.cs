﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.MV;
    using KAMP.MV.BLL;
    using System.Collections.ObjectModel;
    using KAMP.Core.FrameworkComponents.UserControls;
    using System.Windows.Input;
    using KAMP.MV.AppCode;
    using AutoMapper;

    public class RoutinePageViewModel : INotifyPropertyChanged, IPageControlContract
    {
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        public uint _routineCount;

        public RoutinePageViewModel()
        {
            PropertyChanged += RoutinePageViewModel_PropertyChanged;
            _createRoutineViewModel = new CreateRoutineViewModel();

            _manageRoutineViewModelCollection = new ObservableCollection<RoutineViewModel>();
            _manageVariantViewModelCollection = new ObservableCollection<RoutineViewModel>();
            //_searchRoutineViewModelCollection = new ObservableCollection<RoutineViewModel>();

            ManageRoutineViewModelCollection.CollectionChanged += RoutinesViewModel_CollectionChanged;
            ManageVariantViewModelCollection.CollectionChanged += ManageVariantViewModelCollection_CollectionChanged;
            //SearchRoutineViewModelCollection.CollectionChanged += SearchRoutineViewModelCollection_CollectionChanged;
        }

        void SearchRoutineViewModelCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
        }

        void RoutinePageViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        void RoutinesViewModel_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
        }

        private void ManageVariantViewModelCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;


        #region BusinessViewModel

        //routine creation
        private CreateRoutineViewModel _createRoutineViewModel;
        public CreateRoutineViewModel CreateRoutineViewModel
        {
            get { return _createRoutineViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _createRoutineViewModel, (x) => x.CreateRoutineViewModel); }
        }

        private List<string> _routines;
        public List<string> Routines
        {
            get { return _routines; }
            set { PropertyChanged.HandleValueChange(this, value, ref _routines, x => x.Routines); }
        }

        private string _routineTextSearch;
        public string RoutineTextSearch
        {
            get { return _routineTextSearch; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineTextSearch, (x) => x.RoutineTextSearch);
                Routines = modelValidationBL.GetAllRoutinesForSearch(_routineTextSearch);
            }
        }     

        //loading of all routines on page load and Search.
        private ObservableCollection<RoutineViewModel> _manageRoutineViewModelCollection;
        public ObservableCollection<RoutineViewModel> ManageRoutineViewModelCollection
        {
            get
            {
                return _manageRoutineViewModelCollection;
            }
            set
            {
                if (_manageRoutineViewModelCollection != value)
                {
                    _manageRoutineViewModelCollection = value;
                    PropertyChanged(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.ManageRoutineViewModelCollection)));
                }
            }
        }

        private ObservableCollection<RoutineViewModel> _manageVariantViewModelCollection;
        public ObservableCollection<RoutineViewModel> ManageVariantViewModelCollection
        {
            get
            {
                return _manageVariantViewModelCollection;
            }
            set
            {
                if (_manageVariantViewModelCollection != value)
                {
                    _manageVariantViewModelCollection = value;
                    PropertyChanged(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.ManageVariantViewModelCollection)));
                }
            }
        }

        //private ObservableCollection<RoutineViewModel> _searchRoutineViewModelCollection;
        //public ObservableCollection<RoutineViewModel> SearchRoutineViewModelCollection
        //{
        //    get
        //    {
        //        return _searchRoutineViewModelCollection;
        //    }
        //    set
        //    {
        //        if (_searchRoutineViewModelCollection != value)
        //        {
        //            _searchRoutineViewModelCollection = value;
        //            PropertyChanged(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.SearchRoutineViewModelCollection)));
        //        }
        //    }
        //}
        #endregion

        #region StylesViewModel
        private string _bdrBrushColor;
        public string BdrBrushColor
        {
            get
            {
                return _bdrBrushColor = "#FF82BCD6";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _bdrBrushColor, (x) => x.BdrBrushColor);
            }
        }

        private string _lblForegroundColor;
        public string LblForegroundColor
        {
            get
            {
                return _lblForegroundColor = "#0339b1";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lblForegroundColor, (x) => x.LblForegroundColor);
            }
        }
        #endregion

        #region IPageControlContract Members

        public uint GetTotalCount(object filterTag)
        {
            var bll = new ModelValidationBL();
            {
                _routineCount = bll.GetRoutineCount(filterTag);
            }

            return _routineCount;
        }

        public ObservableCollection<object> GetRecordsBy(uint startingIndex, uint numberOfRecords, object filterTag)
        {
            ObservableCollection<object> result = new ObservableCollection<object>();
            ObservableCollection<Routine> lstRoutinesWithVariant;
            if (startingIndex >= _routineCount)
            {
                return result;
            }

            var bll = new ModelValidationBL();
            {
                if (filterTag.IsNull())
                {
                    ObservableCollection<Routine> lstRoutinesWithOutVariant = bll.GetRoutinesWithoutVariantsByIndex(startingIndex, numberOfRecords, false);
                    lstRoutinesWithVariant = bll.GetOnlyRoutinesVariants();
                    ManageRoutineViewModelCollection.Clear();
                    ManageVariantViewModelCollection.Clear();

                    if (lstRoutinesWithOutVariant.IsCollectionValid())
                    {
                        new BootStrapper().Configure();

                        ObservableCollection<Routine> lstOrderedByRoutinesWithOutVariant = new ObservableCollection<Routine>();
                        lstOrderedByRoutinesWithOutVariant.AddRange(lstRoutinesWithOutVariant.OrderBy(x => x.RoutineName));
                        lstOrderedByRoutinesWithOutVariant.Each(x => ManageRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));
                        ManageRoutineViewModelCollection.OrderBy(x => x.RoutineName.ToList());

                        if (lstRoutinesWithVariant.IsCollectionValid())
                        {
                            new BootStrapper().Configure();
                            lstRoutinesWithVariant.Each(x => ManageVariantViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));
                        }

                        var allVariants = new List<RoutineViewModel>();
                        if (ManageRoutineViewModelCollection.IsCollectionValid())
                        {
                            foreach (var item in ManageRoutineViewModelCollection)
                            {
                                var variants = ManageVariantViewModelCollection.Where(x => x.ParentRoutineID == item.RoutineID);

                                if (variants.IsCollectionValid())
                                {
                                    item.VariantRoutineViewModelCollection = new ObservableCollection<RoutineViewModel>(variants.OrderBy(x => x.RoutineName));
                                    allVariants.AddRange(variants);

                                    if (allVariants.Count > 0)
                                    {
                                        item.VariantValue = true;
                                    }
                                }
                            }

                            foreach (var item in ManageRoutineViewModelCollection)
                            {
                                result.Add(item);
                            }

                            allVariants.ForEach(x => result.Remove(x));
                            allVariants = null;
                        }
                    }
                }
                else
                {
                    string searchText;
                    searchText = (string)filterTag;
                    if (searchText != string.Empty)
                    {
                        List<Routine> lstRoutines = new List<Routine>();
                        lstRoutines = modelValidationBL.GetRoutineAfterFilterByIndex(searchText, startingIndex, numberOfRecords, false);
                        ManageRoutineViewModelCollection.Clear();
                        ManageVariantViewModelCollection.Clear();
                        lstRoutines.Each(x => ManageRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));
                        lstRoutinesWithVariant = bll.GetOnlyRoutinesVariants();

                        if (lstRoutinesWithVariant.IsCollectionValid())
                        {
                            new BootStrapper().Configure();
                            lstRoutinesWithVariant.Each(x => ManageVariantViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));
                        }

                        var allVariants = new List<RoutineViewModel>();
                        if (ManageRoutineViewModelCollection.IsCollectionValid())
                        {
                            foreach (var item in ManageRoutineViewModelCollection)
                            {
                                var variants = ManageVariantViewModelCollection.Where(x => x.ParentRoutineID == item.RoutineID);

                                if (variants.IsCollectionValid())
                                {
                                    item.VariantRoutineViewModelCollection = new ObservableCollection<RoutineViewModel>(variants.OrderBy(x => x.RoutineName));
                                    allVariants.AddRange(variants);

                                    if (allVariants.Count > 0)
                                    {
                                        item.VariantValue = true;
                                    }
                                }
                            }

                            foreach (var item in ManageRoutineViewModelCollection)
                            {
                                result.Add(item);
                            }

                            allVariants.ForEach(x => result.Remove(x));
                            allVariants = null;
                        }
                    }
                }
                return result;
            }
        }

        #endregion

        #region UIActions
        public Action<int> OnEditClick;
        public Action<int> OnSelectClick;

        private ICommand _edit;
        public ICommand Edit
        {
            get
            {
                if (_edit == null)
                    _edit = new DelegateCommand<int>((s) =>
                    {
                        if (OnEditClick.IsNotNull())
                            OnEditClick(s);
                    });
                return _edit;
            }
        }

        private ICommand _select;
        public ICommand Select
        {
            get
            {
                if (_select == null)
                    _select = new DelegateCommand<int>((s) =>
                    {
                        if (OnSelectClick.IsNotNull())
                            OnSelectClick(s);
                    });
                return _select;
            }
        }
        #endregion
    }
}
