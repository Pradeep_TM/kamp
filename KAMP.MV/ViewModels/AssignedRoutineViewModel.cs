﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;

    public class AssignedRoutineViewModel : INotifyPropertyChanged
    {
        public AssignedRoutineViewModel()
        {
            PropertyChanged += AssignedRoutineViewModel_PropertyChanged;
        }

        void AssignedRoutineViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }
        public event PropertyChangedEventHandler PropertyChanged;

        #region BusinessViewModel


        private int userID;
        public int UserID
        {
            get
            {
                return userID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref userID, (x) => x.UserID);
            }
        }      

        private string _userName;
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userName, (x) => x.UserName);
            }
        }

        private string  _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _status, (x) => x.Status);
            }
        }

        private string _caseNumber;
        public string CaseNumber
        {
            get
            {
                return _caseNumber;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _caseNumber, (x) => x.CaseNumber);
            }
        }

        private int stateID;
        public int StateID
        {
            get
            {
                return stateID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref stateID, (x) => x.StateID);
            }
        }      

        private string _workFlowState;
        public string WorkFlowState
        {
            get
            {
                return _workFlowState;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _workFlowState, (x) => x.WorkFlowState);
            }
        }

        private int _routineID;
        public int RoutineID
        {
            get
            {
                return _routineID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineID, (x) => x.RoutineID);
            }
        }

        private string _routineName;
        public string RoutineName
        {
            get
            {
                return _routineName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineName, (x) => x.RoutineName);
            }
        }

        private long cfID;
        public long CFid
        {
            get 
            { 
                return cfID; 
            }
            set 
            {
                PropertyChanged.HandleValueChange(this, value, ref cfID, (x) => x.CFid);
            }
        }      
     
        #endregion
    }
}
