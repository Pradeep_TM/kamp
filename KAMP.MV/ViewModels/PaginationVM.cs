﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KAMP.MV.ViewModels
{
    using Core.FrameworkComponents;
    public class PaginationVM : INotifyPropertyChanged
    {

        private ICommand _firstPage;
        public ICommand FirstPage
        {
            get
            {
                if (_firstPage.IsNull())
                    _firstPage = new DelegateCommand(() =>
             {
                 StartIndex = 1;
                 CurrentIndex = 1;

                 if (StartIndex == TotalPages)
                     return;

                 if (PageClick.IsNotNull())
                     PageClick();
             });
                return _firstPage;
            }

        }

        private ICommand _lastPage;
        public ICommand LastPage
        {
            get
            {
                if (_lastPage.IsNull())
                    _lastPage = new DelegateCommand(() =>
                    {
                        StartIndex = ((int.Parse(TotalPages.ToString()) - 1) * DisplayitemCount) + 1;

                        if (StartIndex <= 1)
                            return;

                        CurrentIndex = int.Parse(TotalPages.ToString());
                        if (PageClick.IsNotNull())
                            PageClick();
                    });
                return _lastPage;
            }

        }

        public Action PageClick;
        private ICommand _nextPage;
        public ICommand NextPage
        {
            get
            {
                if (_nextPage.IsNull())
                    _nextPage = new DelegateCommand(() =>
                    {

                        if (CurrentIndex == TotalPages)
                            return;

                        StartIndex = StartIndex + DisplayitemCount;

                        if (StartIndex > TotalItem)
                            return;

                        if (PageClick.IsNotNull())
                        {
                            PageClick();
                            CurrentIndex = CurrentIndex + 1;
                        }
                    });
                return _nextPage;
            }

        }

        private ICommand _prevPage;
        public ICommand PrevPage
        {
            get
            {
                if (_prevPage.IsNull())
                    _prevPage = new DelegateCommand(() =>
                    {
                        if (CurrentIndex == 1)
                            return;
                        StartIndex = StartIndex - DisplayitemCount;

                        if (StartIndex < 0)
                            return;

                        if (PageClick.IsNotNull())
                        {
                            CurrentIndex = CurrentIndex - 1;
                            PageClick();
                        }
                    });
                return _prevPage;
            }

        }
        private int _startIndex;
        public int StartIndex
        {
            get { return _startIndex; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _startIndex, (x) => x.StartIndex);
            }
        }
        public int NextIndex { get; set; }
        private int currentIndex;
        public int CurrentIndex
        {
            get { return currentIndex; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref currentIndex, (x) => x.CurrentIndex);
            }
        }

        private double _totalItem;
        public double TotalItem
        {
            get { return _totalItem; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _totalItem, (x) => x.TotalItem);
                PropertyChanged.Raise(this, (x) => x.TotalPages);
            }
        }

        public double TotalPages
        {
            get
            {
                return Math.Ceiling(TotalItem / DisplayitemCount);
            }
        }
        public int DisplayitemCount { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
