﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository;
    using KAMP.MV.BLL;
    using System.Windows;

    public class CreateRoutineViewModel : INotifyPropertyChanged
    {
        ModelValidationBL modelValidationBL = new ModelValidationBL();

        public CreateRoutineViewModel()
        {
            PropertyChanged += CreateRoutineViewModel_PropertyChanged;
        }

        void CreateRoutineViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region BusinessViewModel

        private int _routineID;
        public int RoutineID
        {
            get
            {
                return _routineID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineID, (x) => x.RoutineID);
            }
        }

        private string _routineName;
        //[Required(ErrorMessage = "Routine name is mandatory.")]
        public string RoutineName
        {
            get
            {
                return _routineName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineName, (x) => x.RoutineName);
            }
        }

        private string _parentroutinePresent;
        //[Required(ErrorMessage = "Routine name is mandatory.")]
        public string ParentRoutinePresent
        {
            get
            {
                return _parentroutinePresent;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _parentroutinePresent, (x) => x.ParentRoutinePresent);
            }
        }

        private string _routineDescription;
        //[Required(ErrorMessage = "Routine description is mandatory.")]
        public string RoutineDescription
        {
            get
            {
                return _routineDescription;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineDescription, (x) => x.RoutineDescription);
            }
        }

        private List<MasterEntity> _sourceType;
        //private MasterEntity _sourceType = new MasterEntity { Code = 0, Name = "Select" };
        //[Required(ErrorMessage = "Select a source type.")]
        public List<MasterEntity> SourceType
        {
            get
            {
                _sourceType = modelValidationBL.GetSourceType();               
                 _sourceType.Insert(0, new MasterEntity { Code = 0, Name = "Select" });
                 return _sourceType;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _sourceType, (x) => x.SourceType);
            }
        }

        private int _sourceTypeValue;
        public int SourceTypeValue
        {
            get
            {
                return _sourceTypeValue;
            }
            set
            {
                if (_sourceTypeValue != value)
                {
                    _sourceTypeValue = value;
                    PropertyChanged(this, new PropertyChangedEventArgs(this.GetPropertyName(x => x.SourceTypeValue)));
                }
            }
        }

        private int? _routineScore;  
        public int? RoutineScore
        {
            get
            {
                return _routineScore;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineScore, (x) => x.RoutineScore);
            }
        }      

        private int _createdBy;
        public int CreatedBy
        {
            get
            {
                Context context = Application.Current.Properties["Context"] as Context;
                return _createdBy = Convert.ToInt32(context.User.Id);
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy);
            }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate = System.DateTime.Now;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdDate, (x) => x.CreatedDate);
            }
        }

        //private DateTime _modifiedDate;
        //public DateTime ModifiedDate
        //{
        //    get
        //    {
        //        return _modifiedDate;
        //    }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _modifiedDate, (x) => x.ModifiedDate);
        //    }
        //}

        private int _modifiedBy;
        public int ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedBy, (x) => x.ModifiedBy);
            }
        }

        #endregion

        #region StylesViewModel

        private string _bdrBrushColor;
        public string BdrBrushColor
        {
            get
            {
                return _bdrBrushColor = "#FF82BCD6";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _bdrBrushColor, (x) => x.BdrBrushColor);
            }
        }

        private string _lblForegroundColor;
        public string LblForegroundColor
        {
            get
            {
                return _lblForegroundColor = "#0339b1";
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _lblForegroundColor, (x) => x.LblForegroundColor);
            }
        }
        #endregion
    }
}
