﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using System.Collections.ObjectModel;

    public class DatabaseTablesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public DatabaseTablesViewModel()
        {
            PropertyChanged += DatabaseTablesViewModel_PropertyChanged;
        }

        private void DatabaseTablesViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        #region BusinessViewModel
        private string _tableName;
        public string TableName
        {
            get
            {
                return _tableName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _tableName, (x) => x.TableName);
            }
        }

        private bool isTableChecked;
        public bool IsTableChecked
        {
            get { return isTableChecked; }
            set { PropertyChanged.HandleValueChange(this, value, ref isTableChecked, (x) => x.IsTableChecked); }
        }
        #endregion
    }
}
