﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.ViewModels
{
    using Core.FrameworkComponents;
    using System.Data;
    using System.Windows;

    public class SQLQueryViewModel : INotifyPropertyChanged
    {
        public SQLQueryViewModel()
        {
            PropertyChanged += SQLQueryViewModel_PropertyChanged;
            _queryResult = new DataTable();
            //_queryResult = new List<MVTransNormViewModel>();
            //IQueryable<MVTransNormViewModel> _queryResult = null;
        }

        private void SQLQueryViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        #region BusinessViewModel
        private int _routineID;
        public int RoutineID
        {
            get
            {
                return _routineID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineID, (x) => x.RoutineID);
            }
        }

        private int _queryID;
        public int QueryID
        {
            get
            {
                return _queryID;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _queryID, (x) => x.QueryID);
            }
        }

        private string _querySyntax;
        public string QuerySyntax
        {
            get
            {
                return _querySyntax;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _querySyntax, (x) => x.QuerySyntax);
            }
        }

        private string _queryName;
        public string QueryName
        {
            get
            {
                return _queryName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _queryName, (x) => x.QueryName);
            }
        }

        //private object _queryResult;
        //public object QueryResult
        //{
        //    get
        //    {
        //        return _queryResult;
        //    }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _queryResult, (x) => x.QueryResult);
        //    }
        //}

        private DataTable _queryResult;
        public DataTable QueryResult
        {
            get
            {
                return _queryResult;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _queryResult, (x) => x.QueryResult);
            }
        }

        private string _routineName;
        public string RoutineName
        {
            get
            {
                return _routineName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _routineName, (x) => x.RoutineName);
            }
        }


        //private IQueryable<MVTransNormViewModel> _queryResult;
        //public IQueryable<MVTransNormViewModel> QueryResult
        //{
        //    get
        //    {
        //        return _queryResult;
        //    }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _queryResult, (x) => x.QueryResult);
        //    }
        //}


        private string _errorResult;
        public string ErrorResult
        {
            get
            {
                return _errorResult;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _errorResult, (x) => x.ErrorResult);
            }
        }

        private bool _hasError;
        public bool HasError
        {
            get { return _hasError; }
            set { PropertyChanged.HandleValueChange(this, value, ref _hasError, (x) => x.HasError); }
        }


        private bool _isSelectedForSampling;
        public bool IsSelectedForSampling
        {
            get { return _isSelectedForSampling; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isSelectedForSampling, (x) => x.IsSelectedForSampling); }
        }


        private int _createdBy;
        public int CreatedBy
        {
            get
            {
                Context context = Application.Current.Properties["Context"] as Context;
                return _createdBy = Convert.ToInt32(context.User.Id);
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy);
            }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate = System.DateTime.Now;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdDate, (x) => x.CreatedDate);
            }
        }

        private DateTime _modifiedDate;
        public DateTime ModifiedDate
        {
            get
            {
                return _modifiedDate = System.DateTime.Now; ;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedDate, (x) => x.ModifiedDate);
            }
        }

        private int _modifiedBy;
        public int ModifiedBy
        {
            get
            {
                Context context = Application.Current.Properties["Context"] as Context;
                return _createdBy = Convert.ToInt32(context.User.Id);
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedBy, (x) => x.ModifiedBy);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
