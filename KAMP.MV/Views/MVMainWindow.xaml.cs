﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KAMP.MV.Views
{
    using KAMP.MV.AppCode.Enums;
    using KAMP.MV.UserControls;
    using KAMP.MV.ViewModels;
    using KAMP.Core.FrameworkComponents;
    using KAMP.MV.AppCode;
    using KAMP.REPORT.Views;
    using KAMP.Core.Workflow.UserControls;
    using KAMP.Core.Common.BLL;
    using KAMP.Core.Common.UserControls;

    /// <summary>
    /// Interaction logic for MVMainWindow.xaml
    /// </summary>
    public partial class MVMainWindow : Window
    {
        //Dictionary<string, KAMP.Core.FrameworkComponents.IMVPageControl> _pageCtrlsLkp;
        private MVMainViewModel _viewModel;
        private UserControl currentLoadedUC;

        public MVMainWindow()
        {
            InitializeComponent();

            Loaded += MVLoaded;
        }

        private void MVLoaded(object sender, RoutedEventArgs e)
        {
            AdduserLoginHistory();
            AssignDataContext();
            //InitializeUserControls();
            LoadPageControl(MVPageControls.Home);
        }

        private void AdduserLoginHistory()
        {
            var commonBl = new CommonBL();
            var context = Application.Current.Properties["Context"] as Context;
            commonBl.AddUserLoginHistory(context, DateTime.Now);
        }

        private UserControl InitializeUserControls(string userControl)
        {
            switch (userControl)
            {
                case (MVPageControls.Home):
                    return new UCHome() as UserControl;

                case (MVPageControls.ManageRoutine):
                    return new UCRoutine() as UserControl;

                case (MVPageControls.AssignRoutine):
                    return new UCAssignRoutine() as UserControl;

                case (MVPageControls.QueryBuilder):
                    return new UCQueryBuilder() as UserControl;

                case (MVPageControls.Analysis):
                    return new UCAnalysis() as UserControl;

                case (MVPageControls.Report):
                    return new ReportMainControl() as UserControl;

                case (MVPageControls.ManageWorkflow):
                    return new UCWorkflowMain() as UserControl;

                case (MVPageControls.ManageState):
                    return new UCWorkflowState() as UserControl;

                case (MVPageControls.WorkflowMonitoring):
                    return new UCWorkflowMonitoring() as UserControl;
                case (MVPageControls.DataManagement):
                    return new UCDataManagement() as UserControl;

                default: return null;
            }
        }

        private void AssignDataContext()
        {
            _viewModel = new MVMainViewModel { };
            AssignEventHandlers();

            this.DataContext = _viewModel;
        }

        private void AssignEventHandlers()
        {
            UCHeader.CloseApp += CloseApplication;
            _viewModel.NavigationViewModel.LoadUserControl += LoadPageControl;
        }

        private void CloseApplication()
        {
            if (currentLoadedUC.GetType().ToString().Equals(typeof(ReportMainControl).ToString()))
                currentLoadedUC.Visibility = Visibility.Hidden;
            var result = KAMP.MV.UserControls.MessageBoxControl.Show(MVConstants.CloseApplicationText, MessageBoxButton.YesNo, MV.UserControls.MessageType.Alert);
            if (result == MessageBoxResult.Yes)
                Application.Current.Shutdown();
            else
                currentLoadedUC.Visibility = Visibility.Visible;
        }

        public void LoadPageControl(string ucName)
        {
            new BootStrapper().Configure();
            _viewModel.HeaderViewModel.CurrentContent = ucName;
            //var selPageCntrl = _pageCtrlsLkp[ucName];
            var selPageCntrl = InitializeUserControls(ucName);
            var userControl = selPageCntrl as UserControl;
            if (currentLoadedUC == userControl && !ucName.Equals(MVPageControls.Report))
                return;
            if (ucName.Equals(MVPageControls.Report))
            {
                userControl.Width = 974;
                userControl.HorizontalAlignment = HorizontalAlignment.Right;
            }
            if (currentLoadedUC.IsNotNull() && userControl.IsNotNull() && currentLoadedUC.ToString().Equals(userControl.ToString()) && !ucName.Equals(MVPageControls.Report))
                return;
            GridUCContainer.Children.Remove(currentLoadedUC);
            if (!GridUCContainer.Children.Contains(userControl))
                GridUCContainer.Children.Add(userControl);
            //if (currentLoadedUC.IsNotNull())
            //    currentLoadedUC.Visibility = Visibility.Collapsed;


            CastUserControlToPageControl(userControl, ucName);
            //selPageCntrl.InitializeViewModel();
            userControl.Visibility = Visibility.Visible;
            currentLoadedUC = userControl;
        }

        private void CastUserControlToPageControl(UserControl userControl, string ucName)
        {
            try
            {
                var iMVPageControl = userControl as KAMP.Core.FrameworkComponents.IMVPageControl;
                if (iMVPageControl.IsNotNull())
                {
                    bool IsWorkflow = ucName.Equals(MVPageControls.ManageWorkflow) || ucName.Equals(MVPageControls.ManageState) || ucName.Equals(MVPageControls.WorkflowMonitoring);
                    if (!IsWorkflow)
                        userControl.DataContext = _viewModel;
                    iMVPageControl.InitializeViewModel();
                }
            }
            catch
            {

            }
        }
    }
}
