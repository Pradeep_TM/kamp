﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.AppCode
{
    public static class MVConstants
    {
        public static string CloseApplicationText = "Are You Sure You Want To Close The Application?";
        public static string SaveRoutine = "Routine saved successfully.";
        public static string SaveVariant = "Routine Variant saved successfully.";
        public static string RoutineName = "Routine Name is mandatory.";
        public static string RoutineDescription = "Routine Description is mandatory.";
        public static string Source = "Please select a Source.";
        public static string RoutineScore = "Routine Score is mandatory.";
        public static string RoutineScoreNumeric = "Please enter a numeric value except 0.";
        public static string RoutineSearch = "No search found. reloading the Routines.";

        public static string SelectAnalyst = "Please select a Analyst.";
        public static string SelectRoutines = "Please select Routine(s).";
        public static string RoutineAssigned = "Routine(s) assigned successfully.";
        public static string AllCases = "All Cases";
        public static string MyCases = "My Cases";

        public static string SelectCaseNumber = "Please select a CaseNumber.";
        public static string Select = "Select";
        public static string SQLQueryName = "SQL Name is mandatory.";
        public static string SelectRoutineorRoutineVaraint = "Please select Routine or Routine Variant.";
        public static string QuerySyntax = "QuerySyntax is mandatory.";
        public static string SaveSQLQuery = "Query saved successfully.";
        public static string UpdateSQLQuery = "Query updated successfully.";
        public static string CaseAssigned = "Case opened successfully.";
        public static string CaseUnAssigned = "Case is not assigned to you.";
        public static string SamplingCount = "Sampling count limit exceeded more than 3.";


        public static string StandardDeviation = "Standard Deviation must be between .5 and .0001.";
        public static string SaveAnalysis = "Analyzed data saved successfully.";
        public static string TranNo = "TranNo";
        public static string RoutineScoreName = "RoutineScore";
        public static string StandardDeviationName = "Standard Deviation";
        public static string SampledData = "Sampled Data";

        public static string ModuleSelected = "MV_";
        public static string QueryCount = "150";

        public static string UnAssigned = "UnAssigned";        
    }   
}