﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.AppCode.Enums
{
    public enum SourceTypeEnum
    {        
        KPMG = 1,
        Actimize = 2,
        SAS = 3,
        Fortent = 4,
        Prime = 5
    }
}
