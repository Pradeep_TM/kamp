﻿using AutoMapper;
using KAMP.Core.Repository.MV;
using KAMP.MV.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.AppCode
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Workflow;
    using KAMP.MV.AppCode.Enums;
    public class MVMapperProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();

            Mapper.CreateMap<CreateRoutineViewModel, Routine>();

            Mapper.CreateMap<Routine, RoutineViewModel>();

            Mapper.CreateMap<RoutineViewModel, Routine>();

            Mapper.CreateMap<SQLQueryViewModel, SQLQuery>();

            Mapper.CreateMap<SQLQuery,SQLQueryViewModel>();

            Mapper.CreateMap<CaseDetail, AssignedRoutineViewModel>();

            Mapper.CreateMap<RoutineViewModel, RoutineViewModel>().IgnoreAllNonExisting();

            Mapper.CreateMap<RoutineViewModel, ChildRoutineViewModel>().IgnoreAllNonExisting();

            MapViewModelToEntityModel();

            MapEntityModelToViewModel();
        }

        private void MapViewModelToEntityModel()
        {
            MapCreateRoutineToRoutineEntity();
            MapRoutineViewModelToRoutine();
            MapSQLQueryViewModelToSQLQuery();
        }

        private void MapEntityModelToViewModel()
        {
            MapRoutineEntityToRoutineViewModel();
            MapSQLQueryEntityToSQLQueryViewModel();
            MapCaseDetailToAssignedRoutineViewModel();
        }

        private void MapCreateRoutineToRoutineEntity()
        {
            Mapper.CreateMap<CreateRoutineViewModel, Routine>()
                //.ForMember(d => d.Id, s => s.MapFrom(x => x.RoutineID != 0 ? x.RoutineID : 0))               
                .ForMember(d => d.ParentRoutineID, s => s.MapFrom(x => x.RoutineID != 0 ? x.RoutineID : 0))
                .ForMember(d => d.RoutineName, s => s.MapFrom(x => x.RoutineName))
                .ForMember(d => d.RoutineDescription, s => s.MapFrom(x => x.RoutineDescription))
                .ForMember(d => d.SourceTypeID, s => s.MapFrom(p => p.SourceTypeValue))
                .ForMember(d => d.RoutineAccountScore, s => s.MapFrom(x => x.RoutineScore))
                .ForMember(d => d.SourceType, s => s.Ignore())
                .ForMember(d => d.CreatedBy, s => s.MapFrom(x => x.CreatedBy))
                .ForMember(d => d.CreatedDate, s => s.MapFrom(x => x.CreatedDate)).IgnoreAllNonExisting();
        }

        private void MapRoutineEntityToRoutineViewModel()
        {
            Mapper.CreateMap<Routine, RoutineViewModel>()
          .ForMember(d => d.RoutineID, s => s.MapFrom(x => x.Id))
          .ForMember(d => d.ParentRoutineID, s => s.MapFrom(x => x.ParentRoutineID != 0 && x.ParentRoutineID != null ? x.ParentRoutineID : 0))
          .ForMember(d => d.RoutineName, s => s.MapFrom(x => x.RoutineName))
          .ForMember(d => d.RoutineDescription, s => s.MapFrom(x => x.RoutineDescription))
          .ForMember(d => d.SourceTypeValue, s => s.MapFrom(x => x.SourceTypeID != 0 ? (SourceTypeEnum)x.SourceTypeID : 0))
          .ForMember(d => d.CreatedBy, s => s.Ignore())
          .ForMember(d => d.CreatedDate, s => s.Ignore())
          .ForMember(d => d.ModifiedBy, s => s.Ignore())
          .ForMember(d => d.ModifiedDate, s => s.Ignore())
          .ForMember(d => d.RoutineScore, s => s.MapFrom(x => x.RoutineAccountScore)).IgnoreAllNonExisting();
        }

        private void MapRoutineViewModelToRoutine()
        {
            Mapper.CreateMap<RoutineViewModel, Routine>()
            .ForMember(d => d.Id, s => s.MapFrom(x => x.RoutineID))            
            .ForMember(d => d.RoutineName, s => s.MapFrom(x => x.RoutineName)).IgnoreAllNonExisting();
        }

        private void MapSQLQueryViewModelToSQLQuery()
        {
            Mapper.CreateMap<SQLQueryViewModel, SQLQuery>()
             .ForMember(d => d.RoutineID, s => s.MapFrom(x => x.RoutineID))
             .ForMember(d => d.QuerySyntax, s => s.MapFrom(x => x.QuerySyntax))
             .ForMember(d => d.QueryName, s => s.MapFrom(x => x.QueryName))
             .ForMember(d => d.CreatedOn, s => s.MapFrom(x => x.CreatedDate))
             .ForMember(d => d.ModifedOn, s => s.MapFrom(x => x.ModifiedDate)).IgnoreAllNonExisting();
        }

        private void MapSQLQueryEntityToSQLQueryViewModel()
        {
            Mapper.CreateMap<SQLQuery, SQLQueryViewModel>()
            .ForMember(d => d.RoutineID, s => s.MapFrom(x => x.RoutineID))
            .ForMember(d => d.QuerySyntax, s => s.MapFrom(x => x.QuerySyntax))
            .ForMember(d => d.QueryName, s => s.MapFrom(x => x.QueryName)).IgnoreAllNonExisting();
        }

        private void MapCaseDetailToAssignedRoutineViewModel()
        {
            Mapper.CreateMap<CaseDetail, AssignedRoutineViewModel>()
           .ForMember(d => d.CaseNumber, s => s.MapFrom(x => x.CaseNo))
           .ForMember(d => d.UserID, s => s.MapFrom(x => (int)x.UserId))
           .ForMember(d => d.UserName, s => s.MapFrom(x => x.UserName))
           .ForMember(d => d.WorkFlowState, s => s.MapFrom(x => x.State))
           .ForMember(d => d.StateID, s => s.MapFrom(x => x.StateId))
           .ForMember(d => d.Status, s => s.MapFrom(x => x.Status)).IgnoreAllNonExisting();
        }
    }
}
