﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.AppCode
{
    public class BootStrapper
    {
        public void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<MVMapperProfile>();
            });

            Mapper.AssertConfigurationIsValid();
        }      
    }
}
