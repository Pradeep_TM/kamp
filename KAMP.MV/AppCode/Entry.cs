﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.AppCode
{
    using Core.Interface;
    using Core.Models;
    using KAMP.Core.FrameworkComponents.Enums;
    using KAMP.MV.Views;

    [Export(typeof(IModule))]
    public class Entry : IModule
    {
        ModuleMetadata _metadata = null;
        public void Initialize()
        {
            var metadata = new ModuleMetadata();
            metadata.ModuleName = ModuleNames.MV.ToString();
            //metadata.HomeWindow = new CreateRoutine();

            //metadata.HomeWindow = new AssignRoutines();

            metadata.HomeWindow = new MVMainWindow();

            Metadata = metadata;

            new BootStrapper().Configure();
        }

        public ModuleMetadata Metadata
        {
            get
            {
                return _metadata;
            }
            set
            {
                this._metadata = value;
            }
        }


    }
}
