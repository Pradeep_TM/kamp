﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.MV.AppCode;
    using KAMP.MV.ViewModels;
    using System.Data;

    /// <summary>
    /// Interaction logic for UCStandardDeviation.xaml
    /// </summary>
    public partial class UCStandardDeviation : UserControl
    {
        //private StandardDeviationViewModel _viewModel;
        private Action _closePopup;
        private AnalysisPageViewModel _viewModel;

        public UCStandardDeviation(Action closePopup, AnalysisPageViewModel analysisPageViewModel)
        {
            InitializeComponent();
            _closePopup = closePopup;

            _viewModel = analysisPageViewModel;

            _viewModel.StandardDeviationViewModel = new StandardDeviationViewModel
            {
                IsConfidenceLevel95 = true,
                IsSamplingError5 = true,
                StandardDeviation = 0.5,
                ConfidenceValue = 1.96,
                SamplingValue = 0.05,
                Population = analysisPageViewModel.StandardDeviationViewModel.Population,
            };

            DataContext = _viewModel.StandardDeviationViewModel;
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            CalculateSD();
        }       

        private void CalculateSD()
        {
            if (_viewModel.StandardDeviationViewModel.StandardDeviation < .0001 || _viewModel.StandardDeviationViewModel.StandardDeviation > .5)
            {
                MessageBoxControl.Show(MVConstants.StandardDeviation, MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                _viewModel.StandardDeviationViewModel.SampleSize = Convert.ToInt32((Math.Pow(_viewModel.StandardDeviationViewModel.ConfidenceValue, 2) * Math.Pow(_viewModel.StandardDeviationViewModel.StandardDeviation, 2) /
                          Math.Pow(_viewModel.StandardDeviationViewModel.SamplingValue, 2)) / (1 + ((Math.Pow(_viewModel.StandardDeviationViewModel.ConfidenceValue, 2) *
                          Math.Pow(_viewModel.StandardDeviationViewModel.StandardDeviation, 2) / Math.Pow(_viewModel.StandardDeviationViewModel.SamplingValue, 2)) - 1) / _viewModel.StandardDeviationViewModel.Population));
                
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_closePopup.IsNotNull())
                {
                    _closePopup();
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void Continue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var sampleSize = _viewModel.StandardDeviationViewModel.SampleSize;

                if ((int)sampleSize != 0)
                {

                    int sampledDataCount = 0;
                    for (int i = 0; i < _viewModel.AnalysisResultViewModelCollection.AsEnumerable().Count(); i++)
                    {
                        if (_viewModel.AnalysisResultViewModelCollection.AsEnumerable().Any(x => x.QueryName != MVConstants.SampledData))
                        {
                            sampledDataCount = i;
                        }
                    }
                    //var sampledDataIndexCount =  _viewModel.AnalysisResultViewModelCollection.Count();
                    //_viewModel.AnalysisResultViewModelCollection[3] (Issue 29)
                    if (sampledDataCount != 0)
                    {
                        DataTable dtStandardDeviation = _viewModel.AnalysisResultViewModelCollection[sampledDataCount]
                                                    .QueryResult.AsEnumerable()
                                                    .Distinct()
                                                    .GroupBy(x => x["TranNo"]).Select(u => u.First())
                                                    .Where(d => d["TranNo"] != DBNull.Value)
                                                    .OrderByDescending(y => y["RoutineScore"])
                                                    .Take((int)sampleSize)
                                                    .CopyToDataTable();

                        SQLQueryViewModel sqlQueryViewModel = new SQLQueryViewModel
                        {
                            QueryName = "Standard Deviation",
                            QueryResult = dtStandardDeviation
                        };


                        if (!_viewModel.AnalysisResultViewModelCollection.Any(x => x.QueryName == MVConstants.StandardDeviationName))
                        {
                            _viewModel.AnalysisResultViewModelCollection.Add(sqlQueryViewModel);
                        }
                        else
                        {
                            _viewModel.AnalysisResultViewModelCollection.RemoveAt(_viewModel.AnalysisResultViewModelCollection.Count() - 1);
                            _viewModel.AnalysisResultViewModelCollection.Add(sqlQueryViewModel);
                        }
                    }
                }
                _closePopup();
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }
    }
}
