﻿using KAMP.MV.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    /// <summary>
    /// Interaction logic for UCRoutoneScore.xaml
    /// </summary>
    public partial class UCRoutoneScore : UserControl
    {
        private RoutineScoreViewModel _viewModel;
        public UCRoutoneScore(int tranNo,Grid rouitneScoreGd)
        {
            _viewModel = new RoutineScoreViewModel(tranNo, rouitneScoreGd);
            InitializeComponent();
            DataContext = _viewModel;
        }
    }
}
