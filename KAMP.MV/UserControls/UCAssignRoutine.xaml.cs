﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using AutoMapper;
    using KAMP.Core.Repository.MV;
    using KAMP.MV.BLL;
    using KAMP.MV.ViewModels;
    using KAMP.Core.FrameworkComponents;
    using KAMP.MV.AppCode;
    using System.Collections.ObjectModel;
    using KAMP.Core.Repository.UM;
    using KAMP.MV.Views;
    using System.Windows.Media.Animation;
    using KAMP.Core.Workflow;
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// Interaction logic for UCAssignRoutine.xaml
    /// </summary>
    public partial class UCAssignRoutine : UserControl, KAMP.Core.FrameworkComponents.IMVPageControl
    {
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        private MVMainViewModel _viewModel;
        private double _height;
        private Grid gridUCAssignReAssign;
        private Action _closeWindow;

        public UCAssignRoutine()
        {
            InitializeComponent();
            cbAnalyst.SelectedIndex = 0;
            _closeWindow += ClosePopup;
            btnCases.Content = MVConstants.MyCases;
        }

        private void ClosePopup()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                if (gridUCAssignReAssign != null)
                {
                    container.Children.Remove(gridUCAssignReAssign);
                    LoadAssignedRoutines();
                }
            }
        }

        public void InitializeViewModel()
        {         
            cbAnalyst.SelectedIndex = 0;
            LoadUnAssignedRoutines();
            RegisterEventHandlers();
            LoadAssignedRoutines();
            BorderAssigned_PreviewMouseDown(null,null);
            IsUnassignedTabOpened = true;
        }     

        private void RegisterEventHandlers()
        {
            if (_viewModel != null)
            {
                _viewModel.AssignRoutinePageViewModel.AnalystDropdownSelected += GetUnAssignedRoutinesWithOutVariants;
            }
        }

        private void GetUnAssignedRoutinesWithOutVariants()
        {
            if (_viewModel.AssignRoutinePageViewModel.AnalystSelectedValue != 0)
            {
                var analystSelected = _viewModel.AssignRoutinePageViewModel.AnalystSelectedValue;
                List<Routine> lstUnAssignedRoutines = new List<Routine>();
                lstUnAssignedRoutines = modelValidationBL.GetUnAssignedRoutinesWithOutVariants(analystSelected);

                //List<Routine> lstUnAssignedSortedRoutines = new List<Routine>();
                //lstUnAssignedSortedRoutines.AddRange(lstUnAssignedRoutines.OrderBy(x => x.RoutineName));

                if (lstUnAssignedRoutines.IsCollectionValid())
                {
                    _viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Clear();
                    new BootStrapper().Configure();
                    lstUnAssignedRoutines.ForEach(x => _viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));

                    ReloadingPaginationControl();
                }
            }
        }

        private void LoadUnAssignedRoutines()
        {
            _viewModel = DataContext as MVMainViewModel;

            if (_viewModel != null)
            {
                cbAnalyst.SelectedIndex = 0;
                _viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Clear();
                List<Routine> lstRoutines = new List<Routine>();
                lstRoutines = modelValidationBL.GetRoutineDetailsWithOutVariants();

                List<Routine> lstSortedRoutines = new List<Routine>();
                lstSortedRoutines.AddRange(lstRoutines.OrderBy(x => x.RoutineName));

                if (lstSortedRoutines.IsCollectionValid())
                {
                    new BootStrapper().Configure();
                    lstSortedRoutines.ForEach(x => _viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));

                    ReloadingPaginationControl();
                }
            }
        }

        private void ReloadingPaginationControl()
        {
            ObservableCollection<object> result = new ObservableCollection<object>();
            var totalCount = _viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Count;
            if (totalCount > 10)
            {
                result.AddRange(_viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Take(10));
            }
            else
            {
                result.AddRange(_viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Take(totalCount));
            }
            pageControl.ItemsSource = result;
            pageControl.TotalPages = 0;
            pageControl.Page = 1;
            pageControl.TotalPages = (uint)Math.Ceiling(Convert.ToDouble(_viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Count) / 10);
        }

        private void btnAssign_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_viewModel.AssignRoutinePageViewModel.AnalystSelectedValue != 0)
                {
                    List<RoutineViewModel> lstAssignedRoutines = new List<RoutineViewModel>();
                    lstAssignedRoutines.AddRange(_viewModel.AssignRoutinePageViewModel.UnAssignRoutineViewModelCollection.Where(x => x.IsChecked));

                    if (lstAssignedRoutines.IsCollectionValid())
                    {
                        List<Routine> lstRoutines = new List<Routine>();
                        lstAssignedRoutines.ForEach(x => lstRoutines.Add(Mapper.Map<Routine>(x)));
                        MVCaseFile mvCaseFile = new MVCaseFile();
                        var userID = Convert.ToInt32(_viewModel.AssignRoutinePageViewModel.AnalystSelectedValue);
                        var context = Application.Current.Properties["Context"] as Context;
                        mvCaseFile.CreatedDate = DateTime.Now;
                        mvCaseFile.CreatedBy = Convert.ToInt32(context.User.Id);
                        mvCaseFile = modelValidationBL.SaveCaseFiles(lstRoutines, mvCaseFile, userID);                       
                        //WorkflowAPI.Assign(mvCaseFile.CaseNo, userID, mvCaseFile.CfId, context.Module.Id);                       
                        MessageBoxControl.Show(MVConstants.RoutineAssigned, MessageBoxButton.OK);
                        //LoadRoutines();    
                        LoadUnAssignedRoutines();
                        LoadAssignedRoutines();
                        cbAnalyst.SelectedIndex = 0;
                    }
                    else
                    {
                        MessageBoxControl.Show(MVConstants.SelectRoutines, MessageBoxButton.OK, MessageType.Error);
                    }
                }
                else
                {
                    MessageBoxControl.Show(MVConstants.SelectAnalyst, MessageBoxButton.OK, MessageType.Error);
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, MessageType.Error); ;
            }
        }

        private void pageControl_PreviewPageChange(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        private void pageControl_PageChanged(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        private void LoadAssignedRoutines()
        {
            _viewModel = DataContext as MVMainViewModel;
            _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Clear();
            if (_viewModel != null)
            {
                List<string> lstCaseNumbers = new List<string>();
                lstCaseNumbers = modelValidationBL.GetAllCaseNumbers();

                if (lstCaseNumbers.IsCollectionValid())
                {
                    var caseRelatedRoutines = WorkflowAPI.GetCaseDetails(lstCaseNumbers);

                    foreach (var item in caseRelatedRoutines.ToList())
                    {
                        if (item.Status == MVConstants.UnAssigned || item.State == MVConstants.UnAssigned)
                        {
                            List<string> lstUnAssignedCaseNumbers = new List<string>();
                            lstUnAssignedCaseNumbers.Add(item.CaseNo);
                            modelValidationBL.DeleteUnAssignedCaseNumber(lstUnAssignedCaseNumbers);
                            caseRelatedRoutines.Remove(item);
                        }
                    }

                    new BootStrapper().Configure();
                    caseRelatedRoutines.ForEach(x => _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Add(Mapper.Map<AssignedRoutineViewModel>(x)));

                    foreach (var item in caseRelatedRoutines)
                    {
                        var routine = modelValidationBL.GetRoutinesDetailsByCaseNo(item.CaseNo);
                        long cfID = modelValidationBL.GetCFid(item.CaseNo);
                        //commented because when the case is finalize userid is 0
                        //if (item.UserId > 0)
                        //{
                        _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Where(x => x.CaseNumber == item.CaseNo).FirstOrDefault().RoutineID = routine.Id;
                        _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Where(x => x.CaseNumber == item.CaseNo).FirstOrDefault().RoutineName = routine.RoutineName;
                        _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Where(x => x.CaseNumber == item.CaseNo).FirstOrDefault().CFid = cfID;
                        //}
                    }
                }
                _viewModel.BkpAssignedRoutineViewModelCollection = new ObservableCollection<AssignedRoutineViewModel>(_viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection);
                var totalcount = _viewModel.BkpAssignedRoutineViewModelCollection.Count;
                _viewModel.PaginationVM = new PaginationVM()
                {
                    TotalItem = totalcount,
                    StartIndex = totalcount > 0 ? 1 : 0,
                    CurrentIndex = totalcount > 0 ? 1 : 0,
                    DisplayitemCount = 10,
                    PageClick = PageClick,
                };
                _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection = new ObservableCollection<AssignedRoutineViewModel>(_viewModel.BkpAssignedRoutineViewModelCollection.Take(_viewModel.PaginationVM.DisplayitemCount));
            }
        }

        private void PageClick()
        {
            _viewModel = DataContext as MVMainViewModel;
            if(_viewModel.IsNotNull())
            {
                var skipItem = _viewModel.PaginationVM.StartIndex - 1;
                _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection = new ObservableCollection<AssignedRoutineViewModel>(_viewModel.BkpAssignedRoutineViewModelCollection.Skip(skipItem).Take(_viewModel.PaginationVM.DisplayitemCount));

            }

        }                

        private void btnReAssign_Click(object sender, RoutedEventArgs e)
        {
            var selectedCase = _viewModel.AssignRoutinePageViewModel.SelectedItem;
            if (selectedCase == null)
            {
                MessageBoxControl.Show("Please select a Case", MessageBoxButton.OK, MessageType.Error);
                return;
            }

            if (selectedCase.WorkFlowState.Trim().ToLower().Equals(Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower()))
            {
                MessageBoxControl.Show("The case has been closed.", MessageBoxButton.OK, MessageType.Information);
                return;
            }

            _viewModel = DataContext as MVMainViewModel;
            //var uc = new UCAssignReAssign(_viewModel.AssignRoutinePageViewModel, _closeWindow);
            var uc = new UCAssignReAssign(selectedCase, _closeWindow);
            gridUCAssignReAssign = new Grid();
            gridUCAssignReAssign.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            gridUCAssignReAssign.Children.Add(lbl);
            gridUCAssignReAssign.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(gridUCAssignReAssign);
            }

            LoadAssignedRoutines();
        }

        private void RowDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (_viewModel.QueryBuilderPageViewModel.CaseNumbers.IsCollectionValid())
                _viewModel.QueryBuilderPageViewModel.CaseNumbers.Clear();
            _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue = null;
            _viewModel.QueryBuilderPageViewModel.RoutineSelectedValue = 0;
            //_viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Clear();
            MVMainWindow mainWindow = Application.Current.MainWindow as MVMainWindow;
            mainWindow.LoadPageControl(MVPageControls.QueryBuilder);
        }

        #region Logic for hide unhide tabs
        private void Border_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_height <= 0)
                _height = UnassignedContainer.ActualHeight;

            if (UnassignedContainer.Height != 0)
            {
                HideUnassignedTab();
            }

            if (UnassignedContainer.Height == 0)
            {
                ShowUnassignedTab();
            }
        }

        private void ShowUnassignedTab()
        {
            if (IsAssignedTabOpened)
            {
                DoubleAnimation animation1 = new DoubleAnimation();
                animation1.To = 0;
                animation1.From = 0;
                animation1.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation1);
            }
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = _height;
            animation.From = 0;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
            IsUnassignedTabOpened = true;
        }

        private void HideUnassignedTab()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 0;
            animation.From = _height;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
        }

        private void BorderAssigned_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_heightAssigned <= 0)
                _heightAssigned = AssignedContainer.ActualHeight;

            if (AssignedContainer.Height != 0)
            {
                HideAssignedTab();
            }

            if (AssignedContainer.Height == 0)
            {
                ShowAssignedTab();
            }
        }

        private void ShowAssignedTab()
        {
            if (IsUnassignedTabOpened)
            {
                DoubleAnimation animation1 = new DoubleAnimation();
                animation1.To = 0;
                animation1.From = 0;
                animation1.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
                UnassignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation1);
            }
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = _heightAssigned;
            animation.From = 0;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
            IsAssignedTabOpened = true;
        }

        private void HideAssignedTab()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 0;
            animation.From = _heightAssigned;
            animation.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, 300));
            AssignedContainer.BeginAnimation(ContentPresenter.HeightProperty, animation);
        }

        private double _heightAssigned;
        private bool IsAssignedTabOpened;
        private bool IsUnassignedTabOpened;
        #endregion

        private void btnCases_Click(object sender, RoutedEventArgs e)
        {
            Context context = System.Windows.Application.Current.Properties["Context"] as Context;
            var userID = context.User.Id;   
            if (btnCases.Content.ToString() == MVConstants.AllCases)
            {
                _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Clear();
                LoadAssignedRoutines();
                btnCases.Content = MVConstants.MyCases;
            }
            else
            {
                var assignedLoggedinUserRoutineViewModelCollection = _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Where(x => x.UserID == userID).ToList();
                _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.Clear();
                _viewModel.AssignRoutinePageViewModel.AssignedRoutineViewModelCollection.AddRange(assignedLoggedinUserRoutineViewModelCollection);
                btnCases.Content = MVConstants.AllCases;
            }
        }        
    }
}
