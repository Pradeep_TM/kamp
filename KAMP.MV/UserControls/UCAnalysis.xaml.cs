﻿using KAMP.MV.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.MV;
    using KAMP.Core.Workflow;
    using KAMP.MV.BLL;
    using KAMP.MV.Views;
    using System.Collections.ObjectModel;
    using System.Data;
    using Core.Workflow.ViewModels;
    using Core.Workflow.UserControls;
    using System.ComponentModel;
    using KAMP.MV.AppCode;
    /// <summary>
    /// Interaction logic for UCAnalysis.xaml
    /// </summary>
    public partial class UCAnalysis : UserControl, KAMP.Core.FrameworkComponents.IMVPageControl
    {
        private MVMainViewModel _viewModel;
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        private Grid gridUCStandardDeviation;
        private Action _closeWindow;
        DataTable dtDistinctSampledData = new DataTable();
        List<AccountScores> lstAccountScores = new List<AccountScores>();
        List<TransactionScores> lstTransactionScores = new List<TransactionScores>();
        Context context = Application.Current.Properties["Context"] as Context;
        private Grid wfProcessGrid;
        bool isparentRoutinePresent;

        public UCAnalysis()
        {
            InitializeComponent();
            _closeWindow += ClosePopup;
        }

        public void InitializeViewModel()
        {
            _viewModel = DataContext as MVMainViewModel;

            LoadSampledData();

            var caseDetail = WorkflowAPI.GetCaseDetails(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);
            if (caseDetail.IsOpen != null)
            {
                if (caseDetail.IsOpen.Value && caseDetail.UserId == context.User.Id)
                {
                    _viewModel.AnalysisPageViewModel.IsCurrentAssignedUser = true;
                }
            }
            ReviewerView();
        }

        private void LoadSampledData()
        {
            var sampleCollectionItems = new Dictionary<int, DataTable>() { { 0, null }, { 1, null }, { 2, null }, { 3, null } };

            if (_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.IsCollectionValid())
            {
                _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.Clear();
                _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.AddRange(_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Where(x => x.IsSelectedForSampling));

                TabSampling.SelectedIndex = 0;
                var resultSet = new DataTable();
                foreach (var item in typeof(MVTransNormViewModel).GetProperties())
                {
                    resultSet.Columns.Add(item.Name, Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType);
                }

                var resultsetOne = resultSet.Clone();
                var resultsetTwo = resultSet.Clone();

                for (int i = 0; i < _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.Count; i++)
                {
                    var queryResult = _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection[i].QueryResult;
                    sampleCollectionItems[i] = queryResult;
                    if (sampleCollectionItems.ContainsKey(i - 1) == true)
                    {
                        var Prevkey = i - 1;
                        var CurrKey = i;
                        if (sampleCollectionItems[3] != null)
                        {
                            CurrKey = 3;
                            Prevkey = i;
                        }

                        resultsetOne.Merge(sampleCollectionItems[CurrKey]);
                        resultsetTwo.Merge(sampleCollectionItems[Prevkey]);

                        var intersectionRows = resultsetOne.AsEnumerable()
                            .Intersect(resultsetTwo.AsEnumerable(), DataRowComparer.Default);
                        var unionRows = resultsetOne.AsEnumerable()
                            .Union(resultsetTwo.AsEnumerable(), DataRowComparer.Default);


                        if (intersectionRows.IsCollectionValid())
                            resultSet.Merge(intersectionRows.CopyToDataTable(), true, MissingSchemaAction.Add);

                        if (unionRows.IsCollectionValid())
                            resultSet.Merge(unionRows.CopyToDataTable(), true, MissingSchemaAction.Add);

                        sampleCollectionItems[3] = resultSet.Copy();
                        resultSet.Clear();
                        resultsetOne.Clear();
                        resultsetTwo.Clear();
                    }
                }

                if (sampleCollectionItems[3] == null)
                {
                    btnStandardDeviation.Visibility = Visibility.Collapsed;
                    btnSave.Visibility = Visibility.Collapsed;
                    btnPromoteDemote.Visibility = Visibility.Collapsed;
                }
                else
                {
                    sampleCollectionItems[3].Columns.Add(MVConstants.RoutineScoreName).SetOrdinal(0);
                    var accountScoreModel = modelValidationBL.GetAccountScoreByTranNo(sampleCollectionItems[3]);
                    var items = sampleCollectionItems[3].AsEnumerable().ToList();
                    items.ForEach(x =>
                    {
                        var transactionNo = x[MVConstants.TranNo] == DBNull.Value ? 0 : Convert.ToInt16(x[MVConstants.TranNo]);
                        var accountScore = accountScoreModel.Where(y => y.TranNo == transactionNo).FirstOrDefault();

                        int parentRoutine = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.FirstOrDefault().RoutineID;
                        isparentRoutinePresent = modelValidationBL.CheckParentRoutine(parentRoutine, transactionNo);
                        if (!isparentRoutinePresent)
                        {
                            var stepupAccountScore = accountScore == null ? x[MVConstants.RoutineScoreName] = _viewModel.QueryBuilderPageViewModel.RoutineScore
                                : x[MVConstants.RoutineScoreName] = _viewModel.QueryBuilderPageViewModel.RoutineScore + accountScore.AccountScore;
                        }
                        else
                        {
                            var stepupAccountScore = accountScore == null ? x[MVConstants.RoutineScoreName] = _viewModel.QueryBuilderPageViewModel.RoutineScore
                                : x[MVConstants.RoutineScoreName] = accountScore.AccountScore;
                        }
                    });

                    SQLQueryViewModel sqlQueryViewModel = new SQLQueryViewModel
                    {
                        QueryName = MVConstants.SampledData,
                        QueryResult = sampleCollectionItems[3].AsEnumerable().OrderByDescending(x => x[MVConstants.RoutineScoreName]).CopyToDataTable()
                    };

                    _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.Add(sqlQueryViewModel);
                    _viewModel.AnalysisPageViewModel.StandardDeviationViewModel.Population = sampleCollectionItems[3].AsEnumerable().Distinct().GroupBy(x => x[MVConstants.TranNo]).Count();

                    lstAccountScores = sampleCollectionItems[3].AsEnumerable().Distinct().GroupBy(x => x[MVConstants.TranNo])
                    .Select(groupData => new AccountScores()
                    {
                        TranNo = groupData.Key == DBNull.Value ? 0 : Convert.ToInt16(groupData.Key),
                        AccountScore = Convert.ToInt16(groupData.First()[MVConstants.RoutineScoreName])
                    })
                     .Where(r => r.TranNo != 0)
                     .ToList<AccountScores>();
                }
            }
        }

        private void ReviewerView()
        {
            //int transactionScoreCount = modelValidationBL.GetTransactionScoresCount();

            //if (isparentRoutinePresent && transactionScoreCount > 0)
            //{
            //    DataTable dtStandardDeviation = ((DataTable)_viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection[3]
            //                .QueryResult).AsEnumerable()
            //                .Distinct()
            //                .GroupBy(x => x[MVConstants.TranNo]).Select(u => u.First())
            //                .Where(d => d[MVConstants.TranNo] != DBNull.Value)
            //                .OrderByDescending(y => y[MVConstants.RoutineScoreName])
            //                .Take((int)transactionScoreCount)
            //                .CopyToDataTable();

            //    SQLQueryViewModel sqlQueryViewModel = new SQLQueryViewModel
            //    {
            //        QueryName = MVConstants.StandardDeviationName,
            //        QueryResult = dtStandardDeviation
            //    };

            //    _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.Add(sqlQueryViewModel);
            //}

            if (isparentRoutinePresent)
            {
                int parentRoutineID = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.FirstOrDefault().RoutineID;
                var standardDeviationData = modelValidationBL.GetStandardDeviationData(parentRoutineID);
                DataTable dtStandardDeviation = ConvertToDataTable(standardDeviationData);

                dtStandardDeviation.Columns.Add(MVConstants.RoutineScoreName).SetOrdinal(0);
                var accountScoreModel = modelValidationBL.GetAccountScoreByTranNo(dtStandardDeviation);

                var items = dtStandardDeviation.AsEnumerable().ToList();
                items.ForEach(x =>
                    {
                        var transactionNo = x[MVConstants.TranNo] == DBNull.Value ? 0 : Convert.ToInt16(x[MVConstants.TranNo]);
                        var accountScore = accountScoreModel.Where(y => y.TranNo == transactionNo).FirstOrDefault();
                        x[MVConstants.RoutineScoreName] = accountScore.AccountScore;
                    }
                    );

                SQLQueryViewModel sqlQueryViewModel = new SQLQueryViewModel
                {
                    QueryName = MVConstants.StandardDeviationName,
                    QueryResult = dtStandardDeviation.AsEnumerable().OrderByDescending(x => x[MVConstants.RoutineScoreName]).CopyToDataTable()
                };

                _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.Add(sqlQueryViewModel);
            }
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        private void btnStandardDeviation_Click(object sender, RoutedEventArgs e)
        {
            _viewModel = DataContext as MVMainViewModel;
            var uc = new UCStandardDeviation(_closeWindow, _viewModel.AnalysisPageViewModel);
            gridUCStandardDeviation = new Grid();
            gridUCStandardDeviation.Background = System.Windows.Media.Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = System.Windows.Media.Brushes.Black;
            lbl.Opacity = .5;
            gridUCStandardDeviation.Children.Add(lbl);
            gridUCStandardDeviation.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(gridUCStandardDeviation);
            }
        }

        private void ClosePopup()
        {
            try
            {
                var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                if (container.IsNotNull())
                {
                    if (gridUCStandardDeviation != null)
                    {
                        container.Children.Remove(gridUCStandardDeviation);
                        if (_viewModel.IsNotNull() && _viewModel.AnalysisPageViewModel.IsNotNull() && _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.IsCollectionValid())
                        {
                            int sdTabIndex = _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.Count();
                            TabSampling.SelectedIndex = sdTabIndex - 1;
                        }
                    }
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int parentRoutine = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.FirstOrDefault().RoutineID;
                var context = Application.Current.Properties["Context"] as Context;

                //Sampled Data
                modelValidationBL.SaveRoutineAccountScores(lstAccountScores, parentRoutine);

                //Standard Deviation Data
                //(Issue 29) - Begin
                int sampledDataCount = 0;
                for (int i = 0; i < _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.AsEnumerable().Count(); i++)
                {
                    if (_viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection.AsEnumerable().Any(x => x.QueryName != MVConstants.SampledData))
                    {
                        sampledDataCount = i;
                    }
                }
                //(Issue 29) - End (AnalysisPageViewModel.AnalysisResultViewModelCollection[4])
                if (_viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection[sampledDataCount].QueryResult.AsEnumerable().IsCollectionValid())
                {
                    lstTransactionScores = _viewModel.AnalysisPageViewModel.AnalysisResultViewModelCollection[sampledDataCount].QueryResult.AsEnumerable().ToList().GroupBy(x => x[MVConstants.TranNo])
                        .Select(groupData => new TransactionScores()
                        {
                            TranNo = groupData.Key == DBNull.Value ? 0 : Convert.ToInt16(groupData.Key),
                            RoutineID = parentRoutine,
                            ParameterID = 102,
                            PriorityID = 2,
                            CreatedBy = Convert.ToInt32(context.User.Id),
                            CreatedDate = System.DateTime.Now,
                            ModifiedDate = System.DateTime.Now,
                            ModifiedBy = Convert.ToInt32(context.User.Id)
                        })
                         .Where(r => r.TranNo != 0)
                         .ToList<TransactionScores>();
                    modelValidationBL.SaveTransactionScores(lstTransactionScores, parentRoutine);

                    MessageBoxControl.Show(MVConstants.SaveAnalysis, MessageBoxButton.OK);
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void btnPromoteDemote_Click(object sender, RoutedEventArgs e)
        {
            OpenWorkflow(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);
        }

        private void OpenWorkflow(string caseNo)
        {
            if (wfProcessGrid.IsNotNull())
                return;
            var uc = new UCWorkflowAction();
            var viewModel = new WorkflowActionVm
            {
                CaseNo = caseNo,
            };
            uc.DataContext = viewModel;
            uc.InitializedData();
            viewModel.ClosePopUp += Close;
            viewModel.PostAction += UpdateCase;
            wfProcessGrid = new Grid();
            wfProcessGrid.Background = Brushes.Transparent;
            Label lbl = new Label();
            lbl.Background = Brushes.Black;
            lbl.Opacity = .8;
            wfProcessGrid.Children.Add(lbl);
            wfProcessGrid.Children.Add(uc);
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Add(wfProcessGrid);
            }
        }

        private void UpdateCase(string caseNo)
        {
            var caseDetail = WorkflowAPI.GetCaseDetails(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);
            if (caseDetail.IsOpen.HasValue)
            {
                _viewModel.AnalysisPageViewModel.IsCurrentAssignedUser = caseDetail.IsOpen.Value;
                _viewModel.QueryBuilderPageViewModel.IsOpen = caseDetail.IsOpen;
            }
            if (caseDetail.IsOpen == null)
            {
                _viewModel.AnalysisPageViewModel.IsCurrentAssignedUser = false;
                _viewModel.QueryBuilderPageViewModel.IsOpen = false;
            }
        }

        private void Close()
        {
            var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
            if (container.IsNotNull())
            {
                container.Children.Remove(wfProcessGrid);
                wfProcessGrid = null;
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MVMainWindow mainWindow = Application.Current.MainWindow as MVMainWindow;
            mainWindow.LoadPageControl(MVPageControls.QueryBuilder);
            if (_viewModel.AnalysisPageViewModel.BackButtonClick.IsNotNull())
                _viewModel.AnalysisPageViewModel.BackButtonClick();
        }

        public void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (_isStandardDeviationClick)
            {
                var datarow = sender as DataGridRow;
                var datarowView = datarow.Item as DataRowView;
                var tranNo = int.Parse(datarowView["TranNo"].ToString());

                if (tranNo == 0)
                    return;
                var accountScoreGd = new Grid();
                accountScoreGd.Background = Brushes.Transparent;
                Label lbl = new Label();
                lbl.Background = Brushes.Black;
                lbl.Opacity = .8;
                var ucRoutineScore = new UCRoutoneScore(tranNo, accountScoreGd);
                accountScoreGd.Children.Add(lbl);
                accountScoreGd.Children.Add(ucRoutineScore);
                var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                if (container.IsNotNull())
                {
                    container.Children.Add(accountScoreGd);
                }
            }
        }

        private void TabSampling_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;
            var row = e.AddedItems[0] as DataRowView;
            if (e.AddedItems.Count > 0 && row.IsNotNull() && row.Row.RowState == DataRowState.Unchanged)
                return;
            var tabControl = e.AddedItems[0] as SQLQueryViewModel;
            if (tabControl.IsNotNull() && e.RemovedItems.Count > 0 && tabControl.QueryName.Equals(MVConstants.StandardDeviationName))
            {
                _isStandardDeviationClick = true;
                return;
            }
            else if (e.RemovedItems.Count != 0)
                _isStandardDeviationClick = false;
        }
        private bool _isStandardDeviationClick;
    }
}

