﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using AutoMapper;
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.MV;
    using KAMP.MV.AppCode;
    using KAMP.MV.BLL;
    using KAMP.MV.ViewModels;
    using KAMP.MV.Views;
    using System.Collections.ObjectModel;
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// Interaction logic for UCRoutine.xaml
    /// </summary>
    public partial class UCRoutine : UserControl, KAMP.Core.FrameworkComponents.IMVPageControl
    {
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        private Action _closeWindow;
        private MVMainViewModel _viewModel;
        private Grid gridUCCreateRoutine;

        public UCRoutine()
        {
            InitializeComponent();
            _closeWindow += ClosePopup;
        }

        public void InitializeViewModel()
        {    
            LoadUIActions();
        }

        private void LoadUIActions()
        {
            _viewModel = DataContext as MVMainViewModel;
            _viewModel.RoutinePageViewModel.OnEditClick += OnEditClick;
            if (_viewModel.RoutinePageViewModel.OnSelectClick.IsNull())
                _viewModel.RoutinePageViewModel.OnSelectClick += OnSelectClick;


        }

        private void OnSelectClick(int routineID)
        {
            try
            {
                if (_viewModel.IsNotNull() && _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection.IsCollectionValid())
                {
                    var items = _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection;

                    foreach (var item in items)
                    {
                        if (item.VariantRoutineViewModelCollection.Count > 0)
                        {
                            foreach (var variant in item.VariantRoutineViewModelCollection)
                            {
                                _viewModel.RoutinePageViewModel.ManageVariantViewModelCollection.Count();
                                if (item.VariantValue == null)
                                {
                                    item.VariantValue = true;
                                }
                                if (variant.ParentRoutineID == routineID)
                                {
                                    if (item.VariantValue == true)
                                    {
                                        //loading the variants to the child rows.                            
                                        _viewModel.RoutinePageViewModel.ManageVariantViewModelCollection.Clear();
                                        _viewModel.RoutinePageViewModel.ManageVariantViewModelCollection.AddRange(item.VariantRoutineViewModelCollection);
                                    }
                                    else
                                    {
                                        _viewModel.RoutinePageViewModel.ManageVariantViewModelCollection.Clear();
                                    }
                                }
                            }

                            foreach (var obj in item.VariantRoutineViewModelCollection)
                            {
                                if (obj.ParentRoutineID == routineID)
                                {
                                    item.VariantValue = !item.VariantValue;
                                }

                                if (obj.ParentRoutineID == item.RoutineID && obj.ParentRoutineID != routineID)
                                {
                                    item.VariantValue = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void OnEditClick(int routineID)
        {
            try
            {
                DisplayCreateRoutinePage(routineID);
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }      

        private void ClosePopup()
        {
            try
            {
                var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                if (container.IsNotNull())
                {
                    container.Children.Remove(gridUCCreateRoutine);
                    _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection.Clear();                 
                    var totalRecords = _viewModel.RoutinePageViewModel.GetRecordsBy(0, 10, null);
                    pageControl.ItemsSource = totalRecords;
                    pageControl.TotalPages = 0;
                    pageControl.Page = 1;
                    pageControl.TotalPages = (uint)Math.Ceiling(Convert.ToDouble(_viewModel.RoutinePageViewModel.GetTotalCount(null)) / 10);                 
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void btnCreateRoutine_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DisplayCreateRoutinePage(0);
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void DisplayCreateRoutinePage(int routineID)
        {
            try
            {
                _viewModel = DataContext as MVMainViewModel;
                if (_viewModel.IsNotNull() && _viewModel.RoutinePageViewModel.CreateRoutineViewModel != null)
                {
                    _viewModel.RoutinePageViewModel.CreateRoutineViewModel.RoutineName = null;
                    _viewModel.RoutinePageViewModel.CreateRoutineViewModel.RoutineDescription = null;
                    _viewModel.RoutinePageViewModel.CreateRoutineViewModel.SourceTypeValue = 0;

                    if (routineID != 0)
                    {
                        Routine routine = new Routine();
                        routine = modelValidationBL.GetRoutineDetails(routineID);
                        _viewModel.RoutinePageViewModel.CreateRoutineViewModel.RoutineScore = routine.RoutineAccountScore;
                        _viewModel.RoutinePageViewModel.CreateRoutineViewModel.ParentRoutinePresent = routine.RoutineName;                        
                    }
                    else
                    {
                        _viewModel.RoutinePageViewModel.CreateRoutineViewModel.RoutineScore = null;
                        _viewModel.RoutinePageViewModel.CreateRoutineViewModel.ParentRoutinePresent = string.Empty;
                    }

                    var uc = new UCCreateRoutine(_closeWindow, routineID);
                    uc.DataContext = _viewModel.RoutinePageViewModel.CreateRoutineViewModel;
                    gridUCCreateRoutine = new Grid();
                    gridUCCreateRoutine.Background = Brushes.Transparent;
                    Label lbl = new Label();
                    lbl.Background = Brushes.Black;
                    lbl.Opacity = .5;
                    gridUCCreateRoutine.Children.Add(lbl);
                    gridUCCreateRoutine.Children.Add(uc);
                    var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                    if (container.IsNotNull())
                    {
                        container.Children.Add(gridUCCreateRoutine);
                    }
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _viewModel = DataContext as MVMainViewModel;
                _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection.Clear();
                List<Routine> lstRoutines = new List<Routine>();
                string searchText = _viewModel.RoutinePageViewModel.RoutineTextSearch;

                if (searchText != string.Empty)
                {
                    lstRoutines = modelValidationBL.GetRoutineByFilter(searchText);
                }

                if (lstRoutines.IsCollectionValid())
                {
                    new BootStrapper().Configure();
                    lstRoutines.ForEach(x => _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));

                    var allVariants = new List<RoutineViewModel>();
                    foreach (var item in _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection)
                    {
                        var variants = _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection.Where(x => x.ParentRoutineID == item.RoutineID);

                        if (variants.IsCollectionValid())
                        {
                            item.VariantRoutineViewModelCollection = new ObservableCollection<RoutineViewModel>(variants);
                            allVariants.AddRange(variants);

                            if (allVariants.Count > 0)
                            {
                                item.VariantValue = true;
                            }
                        }
                    }

                    allVariants.ForEach(x => _viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection.Remove(x));
                    allVariants = null;

                    ObservableCollection<object> result = new ObservableCollection<object>();
                    result.AddRange(_viewModel.RoutinePageViewModel.ManageRoutineViewModelCollection);
                    _viewModel.RoutinePageViewModel._routineCount = (uint)result.Count;
                    pageControl.ItemsSource = result;
                    pageControl.TotalPages = 0;
                    pageControl.Page = 1;
                    pageControl.TotalPages = (uint)Math.Ceiling(Convert.ToDouble(result.Count) / 10);
                    pageControl.FilterTag = searchText;
                }
                else
                {
                    MessageBoxControl.Show(MVConstants.RoutineSearch, MessageBoxButton.OK);
                    var obj = _viewModel.RoutinePageViewModel.GetRecordsBy(0, 10, null);
                    pageControl.ItemsSource = obj;
                    pageControl.TotalPages = 0;
                    pageControl.Page = 1;
                    pageControl.TotalPages = (uint)Math.Ceiling(Convert.ToDouble(_viewModel.RoutinePageViewModel.GetTotalCount(null)) / 10);
                    _viewModel.RoutinePageViewModel.RoutineTextSearch = string.Empty;
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void pageControl_PreviewPageChange(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }

        private void pageControl_PageChanged(object sender, PageChangedEventArgs args)
        {
            List<Object> items = pageControl.ItemsSource.ToList();
            int count = items.Count;
        }
    }
}
