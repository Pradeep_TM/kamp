﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using AutoMapper;
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository;
    using KAMP.Core.Repository.MV;
    using KAMP.MV.AppCode;
    using KAMP.MV.BLL;
    using KAMP.MV.ViewModels;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Interaction logic for UCCreateRoutine.xaml
    /// </summary>
    public partial class UCCreateRoutine : UserControl
    {
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        private CreateRoutineViewModel _createRoutineViewModel;
        private Action _closePopup;
        private int _routineID;
        private Action _NewlyAddedChildRoutine;

        public UCCreateRoutine(Action closePopup, int routineID, Action NewlyAddedChildRoutine = null)
        {
            _closePopup = closePopup;
            _NewlyAddedChildRoutine = NewlyAddedChildRoutine;
            _routineID = routineID;
            InitializeComponent();

            if (routineID != 0)
            {
                txtRoutineScore.IsEnabled = false;
                lblParentRoutine.Visibility = Visibility.Visible;
                txtRoutinePresent.Visibility = Visibility.Visible;
                txtRoutinePresent.IsEnabled = false;                
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool _isValid = false;
                new BootStrapper().Configure();
                _createRoutineViewModel = DataContext as CreateRoutineViewModel;
                _createRoutineViewModel.RoutineID = _routineID;
                var routineModel = Mapper.Map<Routine>(_createRoutineViewModel);

                if (_createRoutineViewModel.RoutineName == string.Empty || _createRoutineViewModel.RoutineName == null)
                {
                    _isValid = true;
                    MessageBoxControl.Show(MVConstants.RoutineName, MessageBoxButton.OK, MessageType.Error);
                }

                if ((_createRoutineViewModel.RoutineName != null && _createRoutineViewModel.RoutineName != "") && _createRoutineViewModel.SourceTypeValue == 0)
                {
                    _isValid = true;
                    MessageBoxControl.Show(MVConstants.Source, MessageBoxButton.OK, MessageType.Error);
                }

                if ((_createRoutineViewModel.RoutineDescription == null || _createRoutineViewModel.RoutineDescription == "") && (_createRoutineViewModel.RoutineName != null && _createRoutineViewModel.RoutineName != "" && _createRoutineViewModel.SourceTypeValue != 0))
                {
                    _isValid = true;
                    MessageBoxControl.Show(MVConstants.RoutineDescription, MessageBoxButton.OK, MessageType.Error);
                }

                if (_createRoutineViewModel.RoutineScore == 0 || _createRoutineViewModel.RoutineScore == null && (_createRoutineViewModel.RoutineDescription != null && _createRoutineViewModel.RoutineDescription != "" && _createRoutineViewModel.RoutineName != null && _createRoutineViewModel.RoutineName != "" && _createRoutineViewModel.SourceTypeValue != 0))
                {
                    if (_createRoutineViewModel.RoutineScore == 0)
                    {
                        _isValid = true;
                        MessageBoxControl.Show(MVConstants.RoutineScoreNumeric, MessageBoxButton.OK, MessageType.Error);
                    }
                    else
                    {
                        _isValid = true;
                        MessageBoxControl.Show(MVConstants.RoutineScore, MessageBoxButton.OK, MessageType.Error);
                    }
                }
              
                if (!_isValid)
                {
                    modelValidationBL.SaveRoutine(routineModel);

                    if (_createRoutineViewModel.RoutineID == 0)
                    {
                        MessageBoxControl.Show(MVConstants.SaveRoutine, MessageBoxButton.OK, MessageType.Information);
                        _closePopup();
                    }
                    else
                    {
                        MessageBoxControl.Show(MVConstants.SaveVariant, MessageBoxButton.OK, MessageType.Information);
                        if(_NewlyAddedChildRoutine.IsNotNull())
                        {
                            _NewlyAddedChildRoutine();
                        }
                        _closePopup();
                    }
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _createRoutineViewModel = DataContext as CreateRoutineViewModel;

                if (_createRoutineViewModel.IsNotNull())
                {
                    _createRoutineViewModel.RoutineName = string.Empty;
                    _createRoutineViewModel.RoutineDescription = string.Empty;
                    _createRoutineViewModel.RoutineID = _routineID;
                    if (_createRoutineViewModel.RoutineID == 0)
                    {
                        _createRoutineViewModel.RoutineScore = null;
                    }
                    _createRoutineViewModel.SourceType = new List<MasterEntity>();
                    _createRoutineViewModel.SourceType.Insert(0, new MasterEntity { Code = 0, Name = "Select" });
                    cbSource.SelectedIndex = 0;
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_closePopup.IsNotNull())
                {
                    _closePopup();
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void txtRoutineScore_TextChanged(object sender, TextChangedEventArgs e)
        {
            var txtValue = sender as TextBox;
            int result=0;
            bool isint = Int32.TryParse(txtValue.Text.Trim(), out result);
            if(!isint)
            {
                txtRoutineScore.Text = null;
            }
        }
    }
}
