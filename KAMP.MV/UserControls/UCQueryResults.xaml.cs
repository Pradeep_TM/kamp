﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.MV.ViewModels;
    using System.ComponentModel;
    using System.Data;

    /// <summary>
    /// Interaction logic for UCQueryResults.xaml
    /// </summary>
    public partial class UCQueryResults : UserControl
    {
        private Action _closePopup;

        public UCQueryResults(Action closePopup, DataTable dtResults)
        {
            InitializeComponent();
            _closePopup = closePopup;
            DataContext = new QueryResultViewModel(dtResults);
        }
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_closePopup.IsNotNull())
                {
                    _closePopup();
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        public class QueryResultViewModel : INotifyPropertyChanged
        {
            private int _displayItemCount = 10;
            private DataTable _queryResult;
            public QueryResultViewModel(DataTable queryResult)
            {
                _queryResult = queryResult;
                int totalcount = queryResult.AsEnumerable().Count();
                PaginationVM = new PaginationVM()
                {
                    TotalItem = totalcount,
                    StartIndex = totalcount > 0 ? 1 : 0,
                    CurrentIndex = totalcount > 0 ? 1 : 0,
                    DisplayitemCount = _displayItemCount,
                    PageClick = PageClick,
                };
                DataQueryResult = _queryResult.AsEnumerable().Take(PaginationVM.DisplayitemCount).CopyToDataTable().DefaultView;
            }

            private void PageClick()
            {
                var skip = PaginationVM.StartIndex - 1;
                DataQueryResult = _queryResult.AsEnumerable().Skip(skip).Take(PaginationVM.DisplayitemCount).CopyToDataTable().DefaultView;
            }
            private DataView _dataQueryResult;

            public DataView DataQueryResult
            {
                get { return _dataQueryResult; }
                set { PropertyChanged.HandleValueChange(this, value, ref _dataQueryResult, (x) => x.DataQueryResult); }
            }

            private PaginationVM _paginationVM;

            public PaginationVM PaginationVM
            {
                get { return _paginationVM; }
                set { PropertyChanged.HandleValueChange(this, value, ref _paginationVM, (x) => x.PaginationVM); }
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }
    }
}
