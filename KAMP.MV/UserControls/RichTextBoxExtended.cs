﻿using KAMP.Core.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using KAMP.Core.FrameworkComponents;
using KAMP.MV.ViewModels;
using System.Windows.Media;
using System.ComponentModel;


namespace KAMP.MV.UserControls
{
    public class RichTextBoxExtended : RichTextBox
    {
        private MVMainViewModel _viewModel;
        List<string> source = null;


       public static readonly DependencyProperty RichTextProperty =
       DependencyProperty.Register("Text", typeof(string),
       typeof(RichTextBoxExtended), new FrameworkPropertyMetadata
       (null, new PropertyChangedCallback(OnDocumentChanged)));

        public string Text
        {
            get
            {
                return (string)this.GetValue(RichTextProperty);
            }

            set
            {
                this.SetValue(RichTextProperty, value);
            }
        }

        public static void OnDocumentChanged(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            RichTextBox rtb = (RichTextBox)obj;
            rtb.Document.Blocks.Clear();
            rtb.Document.Blocks.Add(new Paragraph(new Run((string)args.NewValue)));
        }


        public bool AutoAddWhiteSpaceAfterTriggered
        {
            get { return (bool)GetValue(AutoAddWhiteSpaceAfterTriggeredProperty); }
            set { SetValue(AutoAddWhiteSpaceAfterTriggeredProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutoAddWhiteSpaceAfterTriggered.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoAddWhiteSpaceAfterTriggeredProperty =
            DependencyProperty.Register("AutoAddWhiteSpaceAfterTriggered", typeof(bool), typeof(RichTextBoxExtended), new UIPropertyMetadata(true));

        public IList<String> ContentAssistSource
        {
            get { return (IList<String>)GetValue(ContentAssistSourceProperty); }
            set { SetValue(ContentAssistSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ContentAssistSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentAssistSourceProperty =
            DependencyProperty.Register("ContentAssistSource", typeof(IList<String>), typeof(RichTextBoxExtended), new UIPropertyMetadata(new List<string>()));


        public static readonly DependencyProperty ContentAssistSQLKeysTriggersProperty =
              DependencyProperty.Register("ContentAssistSQLKeysTriggers", typeof(IList<String>), typeof(RichTextBoxExtended), new UIPropertyMetadata(new List<string>()));


        public List<string> KAMPTables
        {
            get;
            set;
        }

        public IList<char> ContentAssistTriggers
        {
            get { return (IList<char>)GetValue(ContentAssistTriggersProperty); }
            set { SetValue(ContentAssistTriggersProperty, value); }
        }

        public IList<string> ContentAssistSQLKeysTriggers
        {
            get { return (IList<String>)GetValue(ContentAssistSQLKeysTriggersProperty); }
            set { SetValue(ContentAssistSQLKeysTriggersProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ContentAssistSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentAssistTriggersProperty =
            DependencyProperty.Register("ContentAssistTriggers", typeof(IList<char>), typeof(RichTextBoxExtended), new UIPropertyMetadata(new List<char>()));

        #region constructure
        public RichTextBoxExtended()
        {
            this.Loaded += new RoutedEventHandler(RichTextBoxEx_Loaded);
        }

        void RichTextBoxEx_Loaded(object sender, RoutedEventArgs e)
        {
            //init the assist list box
            if (this.Parent.GetType() != typeof(Grid))
            {
                throw new Exception("this control must be put in Grid control");
            }

            if (ContentAssistTriggers.Count == 0)
            {
                ContentAssistTriggers.Add('.');

            }

            if (ContentAssistSQLKeysTriggers.Count == 0)
            {
                ContentAssistSQLKeysTriggers.Add("FROM");
                //  ContentAssistSQLKeysTriggers.Add("ON");
                ContentAssistSQLKeysTriggers.Add("WHERE");
                //  ContentAssistSQLKeysTriggers.Add("=");
                ContentAssistSQLKeysTriggers.Add("JOIN");
            }

            _viewModel = DataContext as MVMainViewModel;
            if (source == null && _viewModel.QueryBuilderPageViewModel.TablesSelectedViewModelCollection.Count < 0)
            {
                AssistListBox.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                (this.Parent as Grid).Children.Add(AssistListBox);
                AssistListBox.MaxHeight = 100;
                AssistListBox.MinWidth = 100;
                AssistListBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                AssistListBox.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                AssistListBox.Visibility = System.Windows.Visibility.Collapsed;
                AssistListBox.MouseDoubleClick += new MouseButtonEventHandler(AssistListBox_MouseDoubleClick);
                AssistListBox.PreviewKeyDown += new KeyEventHandler(AssistListBox_PreviewKeyDown);

                (this.Parent as Grid).Children.Add(TablesListBox);
                TablesListBox.MaxHeight = 100;
                TablesListBox.MinWidth = 100;
                TablesListBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                TablesListBox.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                TablesListBox.Visibility = System.Windows.Visibility.Collapsed;
                TablesListBox.MouseDoubleClick += new MouseButtonEventHandler(AssistListBox_MouseDoubleClick);
                TablesListBox.PreviewKeyDown += new KeyEventHandler(AssistListBox_PreviewKeyDown);
            }
        }

        void AssistListBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //if Enter\Tab\Space key is pressed, insert current selected item to richtextbox
            if (e.Key == Key.Enter || e.Key == Key.Tab || e.Key == Key.Space)
            {
                InsertAssistWord();
                e.Handled = true;
            }
            else if (e.Key == Key.Back)
            {
                //Baskspace key is pressed, set focus to richtext box
                if (sbLastWords.Length >= 1)
                {
                    sbLastWords.Remove(sbLastWords.Length - 1, 1);
                }
                this.Focus();
            }
        }

        void AssistListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            InsertAssistWord();
        }

        private bool InsertAssistWord()
        {
            bool isInserted = false;

            if (AssistListBox.IsEnabled == true && AssistListBox.SelectedIndex != -1)
            {
                string selectedString = AssistListBox.SelectedItem.ToString().Remove(0, sbLastWords.Length);
                if (AutoAddWhiteSpaceAfterTriggered)
                {
                    selectedString += " ";
                }
                this.InsertText(selectedString);
                isInserted = true;
            }

            //if (AssistListBox.IsEnabled == false && TablesListBox.SelectedIndex != -1)
            //{
            //    string selectedStringForTables = TablesListBox.SelectedItem.ToString().Remove(0, sbLastWords.Length);
            //    if (AutoAddWhiteSpaceAfterTriggered)
            //    {
            //        selectedStringForTables += " ";
            //    }
            //    this.InsertText(selectedStringForTables);
            //    isInserted = true;
            //}

            AssistListBox.Visibility = System.Windows.Visibility.Collapsed;
            //TablesListBox.Visibility = System.Windows.Visibility.Collapsed;
            sbLastWords.Clear();
            IsAssistKeyPressed = false;
            return isInserted;
        }
        #endregion

        #region check richtextbox's document.blocks is available
        private void CheckMyDocumentAvailable()
        {
            if (this.Document == null)
            {
                this.Document = new System.Windows.Documents.FlowDocument();
            }
            if (Document.Blocks.Count == 0)
            {
                Paragraph para = new Paragraph();
                Document.Blocks.Add(para);
            }
        }
        #endregion

        #region Insert Text
        public void InsertText(string text)
        {
            Focus();
            CaretPosition.InsertTextInRun(text);
            TextPointer pointer = CaretPosition.GetPositionAtOffset(text.Length);
            if (pointer != null)
            {
                CaretPosition = pointer;
            }
        }
        #endregion

        #region Content Assist
        private bool IsAssistKeyPressed = false;
        private System.Text.StringBuilder sbLastWords = new System.Text.StringBuilder();

        public ListBox AssistListBox = new ListBox();
        public ListBox TablesListBox = new ListBox();

        public string RichText
        {
            get
            {
                var textRange = new TextRange(this.Document.ContentStart, this.Document.ContentEnd);
                return textRange.Text;
            }

            set
            {
                var textRange = new TextRange(this.Document.ContentStart, this.Document.ContentEnd);
                textRange.Text = value;
            }
        }
        public bool IsEscapeButtonPressedDown;

        private Dictionary<string, string> EntityCollection = new Dictionary<string, string>();

        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                //IsEscapeButtonPressedDown = true;
                AssistListBox.Visibility = System.Windows.Visibility.Collapsed;
                //TablesListBox.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (e.Key == Key.Space || e.Key == Key.OemComma)
            {
                string[] getObject = CaretPosition.GetTextInRun(LogicalDirection.Backward).ToString().ToUpper().Split(' ');
                string setObject = getObject[getObject.Length - 1].Replace("\r\n", "");
                if (ContentAssistSQLKeysTriggers.Contains(setObject.ToUpper()) || e.Key == Key.OemComma && getObject.ToList().Find(x => x == "FROM") != null)
                {
                    ResetAssistListBoxLocation();
                    IsAssistKeyPressed = false;
                    FilterAssistBoxItemsSource();
                    return;
                }
            }

            if (!IsAssistKeyPressed)
            {
                base.OnPreviewKeyDown(e);
                return;
            }

            ResetAssistListBoxLocation();

            if (e.Key == System.Windows.Input.Key.Back)
            {
                if (sbLastWords.Length > 0)
                {
                    sbLastWords.Remove(sbLastWords.Length - 1, 1);
                    //FilterAssistBoxItemsSource();
                }
                else
                {
                    IsAssistKeyPressed = false;
                    sbLastWords.Clear();
                    AssistListBox.Visibility = System.Windows.Visibility.Collapsed;
                    //TablesListBox.Visibility = System.Windows.Visibility.Collapsed;
                }
            }

            //enter key pressed, insert the first item to richtextbox
            if ((e.Key == Key.Enter || e.Key == Key.Space || e.Key == Key.Tab))
            {
                AssistListBox.SelectedIndex = 0;
                if (InsertAssistWord())
                {
                    e.Handled = true;
                }
            }

            if (e.Key == Key.Down)
            {
                AssistListBox.Focus();
                TablesListBox.Focus();
            }

            base.OnPreviewKeyDown(e);
        }

        public void FilterAssistBoxItemsSource(string setObject = "Entity")
        {
            source = null;
            if (setObject == "Entity")
                source = GetEntities();
            else
                source = GetEntityMembers(setObject);

            if (source == null && setObject != "Entity" && setObject != "")
            {
                source = CheckForAlias(setObject);
            }

            AssistListBox.ItemsSource = source;
            AssistListBox.SelectedIndex = 0;
            if (source == null || source.Count == 0)
            {
                AssistListBox.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                var converter = new System.Windows.Media.BrushConverter();
                var brush = (Brush)converter.ConvertFromString("#0185C6");
                AssistListBox.Foreground = brush;
                AssistListBox.BorderBrush = brush;
                AssistListBox.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private List<string> CheckForAlias(string Entity)
        {
            string[] getObject = new TextRange(this.Document.ContentStart, this.Document.ContentEnd).Text
                .Replace("\r\n", "")
                .Replace(",", " ")
                .Trim()
                .Split(' ');
            var arrayAsString = new List<string>(getObject);
            arrayAsString.RemoveAll(x => x == "");
            arrayAsString.RemoveAll(x => x.ToUpper() == "AS");
            // var index = arrayAsString.FindLastIndex(x => x.StartsWith(Entity));


            while (true)
            {
                var index = arrayAsString.FindLastIndex(x => x.StartsWith(Entity, true, null));
                if (index > 0)
                {
                    var tableEntity = arrayAsString.ElementAt(index - 1);
                    //  EntityCollection.Add(Entity, tableEntity);
                    var result = GetEntityMembers(tableEntity);
                    arrayAsString.RemoveAt(index);
                    if (result != null)
                        return result;
                }
                else
                    break;
                //else
                //{
                //    var arrayAsString1 = new List<string>(getObject);
                //    var index1 = arrayAsString1.FindLastIndex(x => x.StartsWith(Entity, true, null));
                //    arrayAsString.RemoveAll(x => x.ToUpper() == "from");
                //    var tableEntity = arrayAsString.ElementAt(index1 + 1);
                //    var result = GetEntityMembers(tableEntity);
                //    return result;
                //}
            }
            return null;
        }

        private List<string> GetEntities()
        {
            using (var context = new MVUnitOfWork())
            {
                source = null;
                _viewModel = DataContext as MVMainViewModel;
                _viewModel.QueryBuilderPageViewModel.TablesSelectedViewModelCollection.Clear();
                List<string> lstTablesSelected = new List<string>();
                //_viewModel.QueryBuilderPageViewModel.DatabaseViewModelCollection.Where(x => x.IsTableChecked == true).Select(x => x).ToList();
                _viewModel.QueryBuilderPageViewModel.TablesSelectedViewModelCollection.AddRange(_viewModel.QueryBuilderPageViewModel.DatabaseViewModelCollection.Where(x => x.IsTableChecked).ToList());

                string entityFilter = "MV_";
                KAMPTables = context.GetEntityNames(entityFilter);

                if (_viewModel.QueryBuilderPageViewModel.TablesSelectedViewModelCollection.IsCollectionValid())
                {
                    foreach (var item in _viewModel.QueryBuilderPageViewModel.TablesSelectedViewModelCollection)
                    {
                        foreach (var kamptables in KAMPTables)
                        {
                            if (item.TableName == kamptables)
                            {
                                lstTablesSelected.Add(item.TableName);
                            }
                        }
                    }
                }

                else
                {
                    lstTablesSelected.AddRange(KAMPTables.Select(x => x));
                }

                return lstTablesSelected;
            }
        }

        private List<string> GetEntityMembers(string entity)
        {
            using (var context = new MVUnitOfWork())
            {
                IEnumerable<string> lookAtMe = context.GetAttributeNames(entity);

                IEnumerable<string> alias = context.GetAlias(entity);
                if (!alias.IsCollectionValid())
                {
                    return null;
                }

                return lookAtMe.ToList();
            }
        }

        protected override void OnTextInput(System.Windows.Input.TextCompositionEventArgs e)
        {
            base.OnTextInput(e);
            var setObject = "";

            if (IsAssistKeyPressed == false && e.Text.Length == 1)
            {
                if (ContentAssistTriggers.Contains(char.Parse(e.Text)))
                {
                    string[] getObject = CaretPosition.GetTextInRun(LogicalDirection.Backward).ToString()
                                                     .Replace("\r\n", "")
                                                     .Replace(",", " ")
                                                     .Replace("=", " ")
                                                     .Trim()
                                                     .Split(' ');

                    setObject = getObject[getObject.Length - 1].Replace("\r\n", "");
                    ResetAssistListBoxLocation();
                    IsAssistKeyPressed = true;
                    FilterAssistBoxItemsSource(setObject);
                    return;
                }
            }

            if (IsAssistKeyPressed)
            {
                sbLastWords.Append(e.Text);
                FilterAssistBoxItemsSource(setObject);
            }
        }

        public void ResetAssistListBoxLocation()
        {
            Rect rect = this.CaretPosition.GetCharacterRect(LogicalDirection.Forward);
            double left = rect.X >= 20 ? rect.X : 20;
            double top = rect.Y >= 20 ? rect.Y + 20 : 20;
            left += this.Padding.Left;
            top += this.Padding.Top;
            AssistListBox.SetCurrentValue(ListBox.MarginProperty, new Thickness(left, top, 0, 0));

            //if (AssistListBox.IsEnabled == false && IsEscapeButtonPressedDown)
            //{
            //    double tableleft = rect.X >= 20 ? rect.X : 0;
            //    double tabletop = rect.Y >= 20 ? rect.Y + 20 : 20;
            //    tableleft += this.Padding.Left;
            //    tabletop += this.Padding.Top;
            //    TablesListBox.SetCurrentValue(ListBox.MarginProperty, new Thickness(tableleft, tabletop, 0, 0));
            //    var converter = new System.Windows.Media.BrushConverter();
            //    var brush = (Brush)converter.ConvertFromString("#0185C6");
            //    TablesListBox.Foreground = brush;
            //    TablesListBox.BorderBrush = brush;
            //    TablesListBox.Visibility = System.Windows.Visibility.Visible;
            //}
        }
        #endregion
    }
}
