﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using AutoMapper;
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository;
    using KAMP.Core.Repository.MV;
    using KAMP.Core.Repository.UnitOfWork;
    using KAMP.Core.Workflow;
    using KAMP.Core.Workflow.Helpers;
    using KAMP.MV.AppCode;
    using KAMP.MV.BLL;
    using KAMP.MV.ViewModels;
    using KAMP.MV.Views;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Data.Entity.Infrastructure;
    using System.Drawing;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Interaction logic for UCQueryBuilder.xaml
    /// </summary>
    public partial class UCQueryBuilder : UserControl, KAMP.Core.FrameworkComponents.IMVPageControl
    {
        private Action _closeWindow;
        private Grid gridUCCreateRoutine;
        private MVMainViewModel _viewModel;
        private RichTextBoxExtended RT_QueryText;
        ModelValidationBL modelValidationBL = new ModelValidationBL();
        private Grid gridUCQueryResults;
        private bool isAdded;
        private string caseSelectedValue;
        DataTable dtResults = new DataTable();
        //bool IsExecutionClicked = false;
        SQLQueryViewModel sqlQueryViewModel = new SQLQueryViewModel();
        int routineCount = 0;

        public UCQueryBuilder()
        {
            InitializeComponent();
            _closeWindow += ClosePopup;
        }

        public void InitializeViewModel()
        {
            LoadUIActions();

            if (_viewModel != null && _viewModel.AssignRoutinePageViewModel.SelectedItem != null)
            {
                var caseDetail = WorkflowAPI.GetCaseDetails(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);
                _viewModel.QueryBuilderPageViewModel.IsOpen = caseDetail.IsOpen;

                Context context = Application.Current.Properties["Context"] as Context;
                _viewModel.QueryBuilderPageViewModel.IsAssignedUser = caseDetail.UserId == context.User.Id;

                if (caseDetail.IsNotNull() && caseDetail.Status.Trim().ToLower().Equals(Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower()) && caseDetail.UserId == 0)
                {
                    _viewModel.QueryBuilderPageViewModel.IsOpen = true;
                }

                if (context.User.Id != caseDetail.UserId && caseDetail.State != Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower())
                {
                    _viewModel.QueryBuilderPageViewModel.IsOpen = true;
                }
            }
            else
            {
                var caseDetail = WorkflowAPI.GetCaseDetails(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);
                if (caseDetail.IsNotNull() && caseDetail.Status.Trim().ToLower().Equals(Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower()) && caseDetail.UserId == 0)
                {
                    //_viewModel.QueryBuilderPageViewModel.IsOpen = false;
                    _viewModel.QueryBuilderPageViewModel.IsAssignedUser = false;
                }
            }

            GetEntityNames();
        }

        private void LoadUIActions()
        {
            _viewModel = DataContext as MVMainViewModel;
            caseSelectedValue = _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue;
            RegisterEventHandlers();
            //Case file dropdown data.
            LoadCaseFiles();

            //LoadRichTextBox(string.Empty);
            dgResults.Visibility = Visibility.Collapsed;
            lblError.Visibility = Visibility.Collapsed;

            if (_viewModel.IsNotNull() && _viewModel.QueryBuilderPageViewModel.IsNotNull() && _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.IsCollectionValid())
                btnGrid.Visibility = Visibility.Visible;
            else
                btnGrid.Visibility = Visibility.Collapsed;

            _viewModel.QueryBuilderPageViewModel.OnSelectClick += OnSelectClick;
        }

        private void LoadRichTextBox(string querySyntax)
        {
            _viewModel.QueryBuilderPageViewModel.QuerySyntax = querySyntax;
        }

        private void BackButtonClick()
        {
            List<SQLQuery> lstSQLQuery = modelValidationBL.GetSQLQueryName(_viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection);
            CacheContainer<SQLQueryViewModel> cache = new CacheContainer<SQLQueryViewModel>();

            foreach (var item in lstSQLQuery)
            {
                if (item.IsNotNull())
                {
                    cache.ContainerName = item.RoutineID.ToString();
                    var getCache = cache.GetCacheObject(true);
                    if (!_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Contains((SQLQueryViewModel)getCache)
                        && getCache.IsNotNull())
                        _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Add(getCache);
                }
            }

            //lstSQLQuery.ForEach(x =>
            //{
            //    cache.ContainerName = x.RoutineID.ToString();
            //    var getCache = cache.GetCacheObject(true);
            //    if (!_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Contains((SQLQueryViewModel)getCache)
            //        && getCache.IsNotNull())
            //        _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Add(getCache);
            //});
        }

        private void OnSelectClick()
        {
            try
            {
                _viewModel = DataContext as MVMainViewModel;

                if ((sqlQueryViewModel.IsNotNull() && sqlQueryViewModel.QuerySyntax.IsNotNull()) && !isAdded)
                {
                    string strQuerySyntax = string.Empty;
                    if (sqlQueryViewModel.QuerySyntax != null)
                    {
                        strQuerySyntax = sqlQueryViewModel.QuerySyntax.ToString();
                    }

                    if (strQuerySyntax != string.Empty)
                        dtResults = modelValidationBL.ExecuteQuery(strQuerySyntax);
                    var uc = new UCQueryResults(_closeWindow, dtResults);
                    // uc.DataContext = dtResults;
                    gridUCQueryResults = new Grid();
                    gridUCQueryResults.Background = System.Windows.Media.Brushes.Transparent;
                    Label lbl = new Label();
                    lbl.Background = System.Windows.Media.Brushes.Black;
                    lbl.Opacity = .5;
                    gridUCQueryResults.Children.Add(lbl);
                    gridUCQueryResults.Children.Add(uc);
                    var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                    if (container.IsNotNull() && !isAdded)
                    {
                        container.Children.Add(gridUCQueryResults);
                        isAdded = true;
                    }
                }
                else
                {
                    if (QueryText.Text.IsNotNull() && QueryText.Text != "")
                    {
                        dtResults = modelValidationBL.ExecuteQuery(QueryText.Text);
                        var uc = new UCQueryResults(_closeWindow, dtResults);
                        //uc.DataContext = dtResults;
                        gridUCQueryResults = new Grid();
                        gridUCQueryResults.Background = System.Windows.Media.Brushes.Transparent;
                        Label lbl = new Label();
                        lbl.Background = System.Windows.Media.Brushes.Black;
                        lbl.Opacity = .5;
                        gridUCQueryResults.Children.Add(lbl);
                        gridUCQueryResults.Children.Add(uc);
                        var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                        if (container.IsNotNull() && isAdded)
                        {
                            container.Children.Add(gridUCQueryResults);
                            isAdded = true;
                        }
                    }
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void LoadCaseFiles()
        {
            long userId = 0;
            Context context = Application.Current.Properties["Context"] as Context;

            if (_viewModel.AssignRoutinePageViewModel.SelectedItem.IsNull())
            {

                userId = Convert.ToInt32(context.User.Id);
            }
            else if (_viewModel.AssignRoutinePageViewModel.SelectedItem.IsNotNull())
            {
                userId = _viewModel.AssignRoutinePageViewModel.SelectedItem.UserID;
            }

            if (_viewModel.AssignRoutinePageViewModel.SelectedItem.IsNotNull() && _viewModel.AssignRoutinePageViewModel.SelectedItem.Status.Trim().ToLower() == Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower())
            {
                if (_viewModel.IsNotNull() && _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue != string.Empty)
                {
                    var caseDetail = WorkflowAPI.GetCaseDetails(_viewModel.AssignRoutinePageViewModel.SelectedItem.CaseNumber);
                    if (caseDetail.LastAnalystId == context.User.Id && caseDetail.Status.Trim().ToLower().Equals(Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower()))
                        userId = Convert.ToInt32(context.User.Id);
                }
            }

            //Issue 16
            //if (!_viewModel.QueryBuilderPageViewModel.CaseNumbers.IsCollectionValid())
            //{
            List<CaseDetail> lstCaseNumbers = new List<CaseDetail>();
            lstCaseNumbers.Select(x => new CaseDetail { CaseNo = x.CaseNo }).ToList();
            lstCaseNumbers.Insert(0, new CaseDetail { CaseNo = MVConstants.Select });
            lstCaseNumbers.AddRange(modelValidationBL.GetCaseFiles(userId));
            _viewModel.QueryBuilderPageViewModel.CaseNumbers = lstCaseNumbers;
            cbCaseFile.IsEnabled = true;

            if (caseSelectedValue.IsNotEmpty())
                _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue = caseSelectedValue;
            //}

            if (_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue.IsNull())
                _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue = _viewModel.AssignRoutinePageViewModel.SelectedItem.IsNotNull() ? _viewModel.AssignRoutinePageViewModel.SelectedItem.CaseNumber : "Select";
            else
                _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue = _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue;
            cbCaseFile.SelectedIndex = 0;
            TabAnalysis.SelectedIndex = 0;
        }

        private void RegisterEventHandlers()
        {
            if (_viewModel.QueryBuilderPageViewModel.CaseNumbersDropdownSelected.IsNull())
                _viewModel.QueryBuilderPageViewModel.CaseNumbersDropdownSelected += GetAssignedRoutinesWithVariants;

            if (_viewModel.AnalysisPageViewModel.BackButtonClick.IsNull())
                _viewModel.AnalysisPageViewModel.BackButtonClick += BackButtonClick;

            if (_viewModel.QueryBuilderPageViewModel.TabSelectionChanged.IsNull())
                _viewModel.QueryBuilderPageViewModel.TabSelectionChanged += TabAnalysis_Selected;

            if (_viewModel.QueryBuilderPageViewModel.RoutineDropdownSelected.IsNull())
                _viewModel.QueryBuilderPageViewModel.RoutineDropdownSelected += Routine_Checked;
        }

        private void GetAssignedRoutinesWithVariants()
        {
            txtSQLName.Text = string.Empty;
            dgResults.ItemsSource = null;
            dgResults.Visibility = Visibility.Collapsed;
            _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Clear();
            _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Clear();


            string caseNumberSelected = string.Empty;
            caseNumberSelected = _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue.IsNull() ? _viewModel.AssignRoutinePageViewModel.SelectedItem.CaseNumber : _viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue;

            if (_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue == MVConstants.Select)
            {
                _viewModel.QueryBuilderPageViewModel.IsOpen = false;
                _viewModel.QueryBuilderPageViewModel.QuerySyntax = string.Empty;
                _viewModel.QueryBuilderPageViewModel.RoutineSelectedValue = 0;
            }
            else
            {
                var caseDetail = WorkflowAPI.GetCaseDetails(caseNumberSelected);
                _viewModel.QueryBuilderPageViewModel.IsOpen = caseDetail.IsOpen;

                if (caseDetail.Status.Trim().ToLower().Equals(Core.Workflow.Helpers.Helper.Status.End.Trim().ToLower()))
                {
                    _viewModel.QueryBuilderPageViewModel.IsAssignedUser = false;
                    //_viewModel.QueryBuilderPageViewModel.IsOpen = false;
                }
                else
                {
                    _viewModel.QueryBuilderPageViewModel.IsAssignedUser = true;
                    //_viewModel.QueryBuilderPageViewModel.IsOpen = true;
                }
            }

            long userId = 0;
            if (_viewModel.AssignRoutinePageViewModel.SelectedItem.IsNull())
            {
                Context context = Application.Current.Properties["Context"] as Context;
                userId = Convert.ToInt32(context.User.Id); ;
            }
            else if (_viewModel.AssignRoutinePageViewModel.SelectedItem.IsNotNull())
            {
                userId = _viewModel.AssignRoutinePageViewModel.SelectedItem.UserID;
            }

            if (caseNumberSelected != string.Empty)
            {
                List<Routine> lstAssignedRoutineWithVariant = new List<Routine>();
                lstAssignedRoutineWithVariant = modelValidationBL.GetAssignedRoutinesWithVariants(caseNumberSelected);
                if (lstAssignedRoutineWithVariant.IsCollectionValid())
                {
                    lstAssignedRoutineWithVariant.ForEach(x => _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));
                    _viewModel.QueryBuilderPageViewModel.RoutineSelectedValue = 0;
                    _viewModel.QueryBuilderPageViewModel.RoutineSelectedValue = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.FirstOrDefault().RoutineID;

                    _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Each(x =>
                    {
                        if (x.ParentRoutineID != 0)
                        {
                            x.RoutineName = string.Concat("......", x.RoutineName);
                        }
                    });

                    routineCount = lstAssignedRoutineWithVariant.Count;
                    if (_viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.IsCollectionValid())
                        _viewModel.QueryBuilderPageViewModel.RoutineScore = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.FirstOrDefault().RoutineScore;
                }
                else
                {
                    _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Clear();
                }

                if (_viewModel.IsNotNull() && _viewModel.QueryBuilderPageViewModel.IsNotNull() && _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.IsCollectionValid())
                    btnGrid.Visibility = Visibility.Visible;
                else
                    btnGrid.Visibility = Visibility.Collapsed;
            }
        }

        private void GetEntityNames()
        {
            _viewModel = DataContext as MVMainViewModel;
            _viewModel.QueryBuilderPageViewModel.DatabaseViewModelCollection.Clear();
            var context = new MVUnitOfWork();
            string entityFilter = MVConstants.ModuleSelected;
            List<string> lookAtMe = context.GetEntityNames(entityFilter);

            foreach (var item in lookAtMe)
            {
                _viewModel.QueryBuilderPageViewModel.DatabaseViewModelCollection.Add(new DatabaseTablesViewModel { TableName = item.ToString() });
            }
        }

        private void btnVariant_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue != MVConstants.Select)
                {
                    int routineID = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Select(x => new { x.ParentRoutineID, x.RoutineID }).FirstOrDefault(x => x.ParentRoutineID == 0).RoutineID;
                    DisplayCreateRoutinePage(routineID);
                }
                else
                {
                    MessageBoxControl.Show(MVConstants.SelectCaseNumber, MessageBoxButton.OK, MessageType.Error);
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void DisplayCreateRoutinePage(int routineID)
        {
            try
            {
                _viewModel = DataContext as MVMainViewModel;
                CreateRoutineViewModel createRoutineViewModel = new CreateRoutineViewModel();

                //if (routineID != 0)
                //{
                //    var routineScore = modelValidationBL.GetRoutineScore(routineID);
                //    createRoutineViewModel.RoutineScore = routineScore;
                //}

                if (routineID != 0)
                {
                    Routine routine = new Routine();
                    routine = modelValidationBL.GetRoutineDetails(routineID);
                    createRoutineViewModel.RoutineScore = routine.RoutineAccountScore;
                    createRoutineViewModel.ParentRoutinePresent = routine.RoutineName;
                }

                var uc = new UCCreateRoutine(_closeWindow, routineID, GetChildRoutine);
                _viewModel.RoutinePageViewModel.CreateRoutineViewModel = createRoutineViewModel;
                uc.DataContext = _viewModel.RoutinePageViewModel.CreateRoutineViewModel;
                gridUCCreateRoutine = new Grid();
                gridUCCreateRoutine.Background = System.Windows.Media.Brushes.Transparent;
                Label lbl = new Label();
                lbl.Background = System.Windows.Media.Brushes.Black;
                lbl.Opacity = .5;
                gridUCCreateRoutine.Children.Add(lbl);
                gridUCCreateRoutine.Children.Add(uc);
                var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                if (container.IsNotNull())
                {
                    container.Children.Add(gridUCCreateRoutine);
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void ClosePopup()
        {
            try
            {
                var container = Application.Current.MainWindow.FindName("GdContainer") as Grid;
                if (container.IsNotNull())
                {
                    if (gridUCCreateRoutine != null)
                    {
                        container.Children.Remove(gridUCCreateRoutine);
                    }

                    if (gridUCQueryResults != null)
                    {
                        container.Children.Remove(gridUCQueryResults);
                        isAdded = false;
                    }
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }

        private void btnExecute_Click(object sender, RoutedEventArgs e)
        {
            _viewModel = DataContext as MVMainViewModel;

            _viewModel.QueryBuilderPageViewModel.QuerySyntax = QueryText.RichText.Trim();

            if (_viewModel.QueryBuilderPageViewModel.QuerySyntax != string.Empty || _viewModel.QueryBuilderPageViewModel.QuerySyntax != "")
            {
                _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.QuerySyntax = _viewModel.QueryBuilderPageViewModel.QuerySyntax;
                try
                {
                    dtResults = modelValidationBL.ExecuteQuery(_viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.QuerySyntax.Trim());
                    //IsExecutionClicked = true;
                    isAdded = true;
                    _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.ErrorResult = string.Empty;
                    _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.HasError = false;
                    _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.QueryResult = dtResults;
                    dgResults.Visibility = Visibility.Visible;
                    lblError.Visibility = Visibility.Collapsed;
                    TabAnalysis.Visibility = Visibility.Collapsed;
                    btnGrid.Visibility = Visibility.Visible;
                    if (dtResults.Rows.Count > 0)
                    {
                        dgResults.ItemsSource = dtResults.AsEnumerable().Take(Convert.ToInt32(MVConstants.QueryCount)).CopyToDataTable().DefaultView;
                    }
                    //isTabSelected = false;
                }
                catch (BusinessException ex)
                {
                    dgResults.Visibility = Visibility.Collapsed;
                    TabAnalysis.Visibility = Visibility.Collapsed;
                    lblError.Visibility = Visibility.Visible;
                    btnGrid.Visibility = Visibility.Collapsed;
                    _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.HasError = true;
                    _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.ErrorResult = ex.Message;
                    QueryCorrection(_viewModel.QueryBuilderPageViewModel.QuerySyntax);
                }
            }
            else
            {
                MessageBoxControl.Show(MVConstants.QuerySyntax, MessageBoxButton.OK, MessageType.Error);
            }
        }

        private void QueryCorrection(string query)
        {
            var queryTxt = query.ToString().Replace("\r\n", " ").Replace("\t", "").Replace(",", "").Replace("[", "").Replace("]", "");
            var queryText = query.Split(' ');
            var stringList = new List<string>(queryText);
            string errorMessage = _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.ErrorResult;

            if (errorMessage != null)
            {
                var exceptionMsg = errorMessage.Trim().Replace(".", "").Replace("'", "").Replace("\r\n", " ");
                var exceptionMessage = exceptionMsg.Replace("\\", "").Split(' ');
                var exceptionMsgList = new List<string>(exceptionMessage);
                var exceptions = queryText.Intersect(exceptionMessage).ToList();


                var flowDocument = new FlowDocument();
                var paragraph = new Paragraph();

                var matches = Regex.Matches(_viewModel.QueryBuilderPageViewModel.QuerySyntax, @"[A-Za-z*;()._,=]*");
                var checkException = string.Empty;

                foreach (Match match in matches)
                {
                    checkException = match.Value.Contains(".") ? match.Value.Split('.')[1] : match.Value;
                    checkException = checkException.Contains(",") ? checkException.Split(',')[0] : checkException;

                    if (exceptions.Contains(checkException))
                    {
                        var formatText = new Underline();
                        formatText.Inlines.Add(match.Value);
                        paragraph.Inlines.Add(formatText);
                        flowDocument.Blocks.Add(paragraph);
                    }
                    else
                    {
                        var value = match.Value == "" ? " " : match.Value;
                        var plainText = new Run(value);
                        paragraph.Inlines.Add(plainText);
                        flowDocument.Blocks.Add(paragraph);
                    }
                }

                if (paragraph.TextDecorations.Count > 0)
                {
                    RT_QueryText.Document = new FlowDocument(paragraph);
                    var contextMenu = new ContextMenu();
                    var converter = new System.Windows.Media.BrushConverter();
                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString("#0185C6");
                    contextMenu.BorderBrush = brush;
                    contextMenu.Foreground = brush;
                    MenuItem menuResolveTable = new MenuItem();
                    menuResolveTable.Header = "Resolve Table";
                    menuResolveTable.Click += menuResolveTable_Click;

                    MenuItem menuResolveColumn = new MenuItem();
                    menuResolveColumn.Header = "Resolve Column";
                    menuResolveColumn.Click += menuResolveColumn_Click;

                    contextMenu.Items.Add(menuResolveTable);
                    contextMenu.Items.Add(menuResolveColumn);

                    RT_QueryText.ContextMenu = contextMenu;
                }
            }
        }

        private void menuResolveTable_Click(object sender, RoutedEventArgs e)
        {
            List<string> lstTablesSelected = new List<string>();
            if (_viewModel.QueryBuilderPageViewModel.DatabaseViewModelCollection.IsCollectionValid())
            {
                foreach (var item in _viewModel.QueryBuilderPageViewModel.DatabaseViewModelCollection)
                {
                    lstTablesSelected.Add(item.TableName);
                }
                RT_QueryText.ResetAssistListBoxLocation();
                RT_QueryText.AssistListBox.ItemsSource = lstTablesSelected;
                RT_QueryText.AssistListBox.Visibility = Visibility.Visible;
            }
        }

        void menuResolveColumn_Click(object sender, RoutedEventArgs e)
        {
            RT_QueryText.FilterAssistBoxItemsSource(RT_QueryText.Selection.Text.Trim());
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = true;
            _viewModel = DataContext as MVMainViewModel;

            if (_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue == MVConstants.Select)
            {
                MessageBoxControl.Show(MVConstants.SelectCaseNumber, MessageBoxButton.OK, MessageType.Error);
                isValid = false;
            }

            _viewModel.QueryBuilderPageViewModel.QuerySyntax = QueryText.RichText.Trim();

            if (_viewModel.QueryBuilderPageViewModel.QuerySyntax == string.Empty || _viewModel.QueryBuilderPageViewModel.QuerySyntax == "")
            {
                MessageBoxControl.Show(MVConstants.QuerySyntax, MessageBoxButton.OK, MessageType.Error);
                isValid = false;
            }

            if (txtSQLName.Text == string.Empty)
            {
                MessageBoxControl.Show(MVConstants.SQLQueryName, MessageBoxButton.OK, MessageType.Error);
                isValid = false;
            }

            if (isValid)
            {
                var routineID = _viewModel.QueryBuilderPageViewModel.RoutineSelectedValue;
                _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.RoutineID = (int)routineID;
                var sqlQueryViewModel = new SQLQueryViewModel();
                CacheContainer<SQLQueryViewModel> cache = new CacheContainer<SQLQueryViewModel>(sqlQueryViewModel);
                cache.ContainerName = routineID.ToString();
                sqlQueryViewModel = (SQLQueryViewModel)cache.GetCacheObject();
                sqlQueryViewModel.QueryName = txtSQLName.Text.Trim();
                sqlQueryViewModel.RoutineID = (int)routineID;
                sqlQueryViewModel.QuerySyntax = _viewModel.QueryBuilderPageViewModel.QuerySyntax;
                bool isExists = _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Any(x => x.QueryName.Equals(sqlQueryViewModel.QueryName));

                //if (IsExecutionClicked)
                //{
                //    sqlQueryViewModel.QueryResult = dtResults;
                //    IsExecutionClicked = false;
                //}  

                //Issue 51
                sqlQueryViewModel.QueryResult = modelValidationBL.ExecuteQuery(sqlQueryViewModel.QuerySyntax.Trim());

                cache.CacheObject = sqlQueryViewModel;
                cache.SetCacheObject();
                new BootStrapper().Configure();
                var sqlQueryModel = Mapper.Map<SQLQuery>(sqlQueryViewModel);
                modelValidationBL.SaveSQLQuery(sqlQueryModel);
                MessageBoxControl.Show(MVConstants.SaveSQLQuery, MessageBoxButton.OK, MessageType.Information);

                if (_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Any(x => x.RoutineID == sqlQueryViewModel.RoutineID))
                {
                    var item = _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.FirstOrDefault(x => x.RoutineID == sqlQueryViewModel.RoutineID);
                    item = sqlQueryViewModel;
                }
                else
                {
                    _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Add(sqlQueryViewModel);
                }

                if (_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.IsCollectionValid() && !isExists)
                {
                    _viewModel.QueryBuilderPageViewModel.SelectedSQLQueryViewModel = _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.LastOrDefault();
                }


                btnGrid.Visibility = Visibility.Visible;
                dgResults.Visibility = Visibility.Collapsed;
                TabAnalysis.Visibility = Visibility.Visible;
            }
        }

        private void Routine_Checked(int routineID)
        {
            if (_viewModel.IsNull())
            {
                Window win = Application.Current.MainWindow as MVMainWindow;
                _viewModel = win.DataContext as MVMainViewModel;
                DataContext = _viewModel;
            }

            _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.RoutineID = (int)routineID;
            SQLQuery sqlQueryModel = new SQLQuery();
            sqlQueryModel = modelValidationBL.GetQuerySyntaxByRoutineID(routineID);

            if (sqlQueryModel != null)
            {
                new BootStrapper().Configure();
                var sqlQueryViewModelMapped = Mapper.Map<SQLQueryViewModel>(sqlQueryModel);
                var sqlQueryViewModel = new SQLQueryViewModel();
                CacheContainer<SQLQueryViewModel> cache = new CacheContainer<SQLQueryViewModel>(sqlQueryViewModel);
                cache.ContainerName = sqlQueryViewModelMapped.RoutineID.ToString();
                sqlQueryViewModel = (SQLQueryViewModel)cache.GetCacheObject();

                if (sqlQueryViewModel.QueryResult.Rows.Count == 0)
                {
                    dtResults = modelValidationBL.ExecuteQuery(sqlQueryViewModelMapped.QuerySyntax);
                    DataTable dtFilteredResult = dtResults.AsEnumerable().Take(Convert.ToInt32(MVConstants.QueryCount)).CopyToDataTable();
                    sqlQueryViewModel.QueryResult = dtFilteredResult;
                    sqlQueryViewModel.QueryName = sqlQueryViewModelMapped.QueryName;
                    sqlQueryViewModel.RoutineID = (int)sqlQueryViewModelMapped.RoutineID;
                    sqlQueryViewModel.QuerySyntax = sqlQueryViewModelMapped.QuerySyntax;
                    cache.CacheObject = sqlQueryViewModel;
                    cache.SetCacheObject();

                    if (!_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Contains(sqlQueryViewModel))
                        _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Add(sqlQueryViewModel);

                    if (_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Count <= 1)
                    {
                        LoadRichTextBox(_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.FirstOrDefault().QuerySyntax);
                        TabAnalysis.SelectedIndex = 0;
                    }
                    else if (_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Count > 1)
                    {
                        foreach (var item in TabAnalysis.Items)
                        {
                            if (((SQLQueryViewModel)item).QueryName == sqlQueryViewModelMapped.QueryName)
                            {
                                LoadRichTextBox(((SQLQueryViewModel)item).QuerySyntax);
                                TabAnalysis.SelectedItem = (SQLQueryViewModel)item;
                            }
                        }
                    }
                }
                else
                {
                    // adding SQLQueryViewModel to collection when picking from cache.
                    if (!_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Contains(sqlQueryViewModel))
                    {
                        _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Add(sqlQueryViewModel);
                    }

                    if (_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.IsCollectionValid())
                    {
                        TabAnalysis.ItemsSource = _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection;
                    }

                    foreach (var item in TabAnalysis.Items)
                    {
                        if (((SQLQueryViewModel)item).QueryName == sqlQueryViewModelMapped.QueryName)
                        {
                            LoadRichTextBox(((SQLQueryViewModel)item).QuerySyntax);
                            TabAnalysis.SelectedItem = (SQLQueryViewModel)item;
                        }
                    }
                }

                dgResults.Visibility = Visibility.Collapsed;
                TabAnalysis.Visibility = Visibility.Visible;
                btnGrid.Visibility = Visibility.Visible;
            }
            else
            {
                sqlQueryViewModel = null;
                LoadRichTextBox(string.Empty);
                txtSQLName.Text = string.Empty;
            }
        }

        private void TabAnalysis_Selected()
        {
            _viewModel = DataContext as MVMainViewModel;

            if (_viewModel.IsNull())
            {
                Window win = Application.Current.MainWindow as MVMainWindow;
                _viewModel = win.DataContext as MVMainViewModel;
                DataContext = _viewModel;
            }
            //sqlQueryViewModel = TabAnalysis.SelectedItem as SQLQueryViewModel;
            sqlQueryViewModel = _viewModel.QueryBuilderPageViewModel.SelectedSQLQueryViewModel;
            if (sqlQueryViewModel != null)
            {
                if (_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.IsCollectionValid())
                    _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.FirstOrDefault().IsSelectedForSampling = true;
                _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.QueryName = sqlQueryViewModel.QueryName;
                LoadRichTextBox(sqlQueryViewModel.QuerySyntax);
                //TabItem Tooltip Display
                _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.RoutineName = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Where(x => x.RoutineID == sqlQueryViewModel.RoutineID).Select(y => y.RoutineName).FirstOrDefault();
                var item = _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.FirstOrDefault(x => x.RoutineID == sqlQueryViewModel.RoutineID);
                item.RoutineName = _viewModel.QueryBuilderPageViewModel.SQLQueryViewModel.RoutineName;
            }
            TabAnalysis.Visibility = Visibility.Visible;
            dgResults.Visibility = Visibility.Collapsed;
            btnGrid.Visibility = Visibility.Visible;
        }

        private void SelectSampling_Click(object sender, RoutedEventArgs e)
        {
            _viewModel = DataContext as MVMainViewModel;

            if (_viewModel != null && _viewModel.QueryBuilderPageViewModel != null && _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection != null)
            {
                var samplingCount = _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.Count(x => x.IsSelectedForSampling);
                if (samplingCount < 3)
                {
                    var selectedTabItem = TabAnalysis.SelectedItem as SQLQueryViewModel;
                    selectedTabItem.IsSelectedForSampling = true;
                }
                else
                {
                    MessageBoxControl.Show(MVConstants.SamplingCount, MessageBoxButton.OK, MessageType.Error);
                }
            }
        }

        private void UnSelectSampling_Click(object sender, RoutedEventArgs e)
        {
            _viewModel = DataContext as MVMainViewModel;

            if (_viewModel != null && _viewModel.QueryBuilderPageViewModel != null && _viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection != null)
            {
                var selectedTabItem = TabAnalysis.SelectedItem as SQLQueryViewModel;
                selectedTabItem.IsSelectedForSampling = false;
            }
        }

        private void btnAnalysis_Click(object sender, RoutedEventArgs e)
        {
            if (!_viewModel.QueryBuilderPageViewModel.QueryResultViewModelCollection.IsCollectionValid())
                MessageBoxControl.Show("Execute Queries and Select for Sampling.", MessageBoxButton.OK, MessageType.Error);
            else
            {
                MVMainWindow mainWindow = Application.Current.MainWindow as MVMainWindow;
                mainWindow.LoadPageControl(MVPageControls.Analysis);
            }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Context context = Application.Current.Properties["Context"] as Context;
                var caseDetail = WorkflowAPI.GetCaseDetails(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);

                if (caseDetail.CaseNo != MVConstants.Select)
                {
                    if (_viewModel.AssignRoutinePageViewModel.SelectedItem != null)
                    {
                        if (context.User.Id != _viewModel.AssignRoutinePageViewModel.SelectedItem.UserID || caseDetail.UserId != context.User.Id)
                        {
                            _viewModel.QueryBuilderPageViewModel.IsOpen = false;
                            MessageBoxControl.Show(MVConstants.CaseUnAssigned, MessageBoxButton.OK, MessageType.Error);
                        }
                        else
                        {
                            WorkflowAPI.OpenCase(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);
                            _viewModel.QueryBuilderPageViewModel.IsOpen = true;
                            MessageBoxControl.Show(MVConstants.CaseAssigned, MessageBoxButton.OK, type: MessageType.Information);
                        }
                    }
                    else
                    {
                        if (_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue != MVConstants.Select)
                        {
                            if (caseDetail.UserId != context.User.Id)
                            {
                                _viewModel.QueryBuilderPageViewModel.IsOpen = false;
                                MessageBoxControl.Show(MVConstants.CaseUnAssigned, MessageBoxButton.OK, MessageType.Error);
                            }
                            else
                            {
                                WorkflowAPI.OpenCase(_viewModel.QueryBuilderPageViewModel.CaseNumberSelectedValue);
                                _viewModel.QueryBuilderPageViewModel.IsOpen = true;
                                _viewModel.QueryBuilderPageViewModel.IsAssignedUser = true;
                                MessageBoxControl.Show(MVConstants.CaseAssigned, MessageBoxButton.OK, type: MessageType.Information);
                            }
                        }
                        else
                        {
                            _viewModel.QueryBuilderPageViewModel.IsOpen = false;
                            MessageBoxControl.Show(MVConstants.SelectCaseNumber, MessageBoxButton.OK, MessageType.Error);
                        }
                    }
                }
                else
                {
                    MessageBoxControl.Show(MVConstants.SelectCaseNumber, MessageBoxButton.OK, type: MessageType.Error);
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, type: MessageType.Error);
            }
        }

        private void GetChildRoutine()
        {
            int routineID = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Select(x => new { x.ParentRoutineID, x.RoutineID }).FirstOrDefault(x => x.ParentRoutineID == 0).RoutineID;
            var childIds = _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Where(x => x.ParentRoutineID == routineID).Select(x => x.RoutineID).ToList();
            var lstNewlyAddedRoutines = modelValidationBL.GetChildRoutinesByID(routineID, childIds);

            lstNewlyAddedRoutines.Each(x =>
            {
                if (x.ParentRoutineID != 0)
                {
                    x.RoutineName = string.Concat("......", x.RoutineName);
                }
            });


            lstNewlyAddedRoutines.ForEach(x => _viewModel.QueryBuilderPageViewModel.AssignedRoutineViewModelCollection.Add(Mapper.Map<RoutineViewModel>(x)));
        }
    }
}
