﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.MV.UserControls
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.UM;
    using KAMP.Core.Workflow;
    using KAMP.Core.Workflow.Helpers;
    using KAMP.MV.AppCode;
    using KAMP.MV.BLL;
    using KAMP.MV.ViewModels;

    /// <summary>
    /// Interaction logic for UCAssignReAssign.xaml
    /// </summary>
    public partial class UCAssignReAssign : UserControl
    {
        //private ViewModels.AssignedRoutineViewModel selectedCase;
        private Action _closePopup;
        private AssignReAssignViewModel _viewModel;
        ModelValidationBL modelValidationBL = new ModelValidationBL();

        public UCAssignReAssign(ViewModels.AssignedRoutineViewModel selectedCase, Action _closeWindow)
        {
            InitializeComponent();
            // TODO: Complete member initialization
            _viewModel = new AssignReAssignViewModel()
            {
                SelectedCase = selectedCase,
            };

            _closePopup = _closeWindow;
            LoadMappedUsers();
            DataContext = _viewModel;
        }

        private void LoadMappedUsers()
        {
            var state = _viewModel.SelectedCase.WorkFlowState;

            if (state.Equals(Helper.Status.Assigned))
            {
                var _analysts = modelValidationBL.GetUsersFromAnalystGroup();
                _analysts.Insert(0, new User { UserId = 0, UserName = "Select" });
                _viewModel.Users = _analysts;
            }
            else
            {
                var _analysts = modelValidationBL.GetUsersByStateId(_viewModel.SelectedCase.StateID);
                _analysts.Insert(0, new User { UserId = 0, UserName = "Select" });
                _viewModel.Users = _analysts;
            }
        }

        private void btnReAssign_Click(object sender, RoutedEventArgs e)
        {
            var caseNo = _viewModel.SelectedCase.CaseNumber;
            var routineID = _viewModel.SelectedCase.RoutineID;
            var cfID = _viewModel.SelectedCase.CFid;
            var context = Application.Current.Properties["Context"] as Context;
            long userId = _viewModel.SelectedUserId;
            try
            {
                if (userId != 0)
                {
                    WorkflowAPI.ReAssign(caseNo, userId, context.Module.Id);
                    modelValidationBL.UpdateCaseFile(caseNo, userId, routineID, cfID);
                    MessageBoxControl.Show("Routine successfully Re-Assigned.", MessageBoxButton.OK, MessageType.Information);
                    _closePopup();
                }
                else
                {
                    MessageBoxControl.Show("Please select a User.", MessageBoxButton.OK, MessageType.Error);
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message, MessageBoxButton.OK, MessageType.Error);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_closePopup.IsNotNull())
                {
                    _closePopup();
                }
            }
            catch (BusinessException ex)
            {
                MessageBoxControl.Show(ex.Message);
            }
        }
    }
}
