﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.MV.BLL
{
    public class CacheContainer<T>
    {
        public CacheContainer()
        {

        }

        ObjectCache cache = MemoryCache.Default;

        public CacheContainer(T cacheObject)
        {
            _cacheObject = cacheObject;
        }

        private string _containerName;

        public string ContainerName
        {
            get { return _containerName; }
            set { _containerName = value; }
        }

        private double _expiration;

        public double CacheExpiration
        {
            get { return _expiration; }
            set { _expiration = value; }
        }

        private T _cacheObject;

        public T CacheObject
        {
            set { _cacheObject = value; }
        }
        public void SetCacheObject()
        {
            var policy = new CacheItemPolicy();
            if (CacheExpiration == 0.0)
                policy = null;
            else
            {
                policy.AbsoluteExpiration =
                    DateTimeOffset.Now.AddSeconds(CacheExpiration);
            }
            cache.Set(ContainerName, _cacheObject, policy);
        }

        public T GetCacheObject()
        {
            if (cache[_containerName] == null)
            {
                SetCacheObject();
            }
            return (T)cache[_containerName];
        }

        public T GetCacheObject(bool CacheExists)
        {
            return (T)cache[_containerName];
        }
    }
}
