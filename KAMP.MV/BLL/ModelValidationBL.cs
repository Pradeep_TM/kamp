﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using System.Collections.ObjectModel;

namespace KAMP.MV.BLL
{
    using KAMP.Core.Repository;
    using KAMP.Core.Repository.MV;
    using KAMP.Core.Repository.UM;
    using KAMP.Core.Repository.UnitOfWork;
    using KAMP.Core.Repository.WF;
    using Core.FrameworkComponents;
    using System.Data;
    using KAMP.MV.ViewModels;
    using KAMP.Core.Workflow;
    using KAMP.Core.Workflow.Helpers;
    using System.Windows;
    using System.Text.RegularExpressions;

    public class ModelValidationBL
    {
        Context context = System.Windows.Application.Current.Properties["Context"] as Context;

        /// <summary>
        /// Gets the source type.
        /// </summary>
        /// <returns></returns>
        public List<MasterEntity> GetSourceType()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<SourceType>().Items.Where(w => w.IsActive).Select(x => new MasterEntity { Code = x.Id, Name = x.Name }).OrderBy(y => y.Name).ToList();
            }
        }

        /// <summary>
        /// Gets analysts.
        /// </summary>
        /// <returns></returns>
        public List<User> GetUsersFromAnalystGroup()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                var userRepo = mvUOW.GetRepository<User>();
                return userRepo.Items.Include(x => x.UserInGroup).Where(x => x.IsActive && x.UserInGroup.
                    Any(gp => gp.IsActive && gp.ModuleTypeId == appcontext.Module.Id && gp.Group.GroupName.ToLower() == Core.Workflow.Helpers.Helper.Group.Analyst.ToLower())).OrderBy(z => z.UserName).ToList();
            }
        }

        /// <summary>
        /// Saves each routine in the Routine table in db.
        /// </summary>3
        /// <param name="routineModel"></param>
        public void SaveRoutine(Routine routineModel)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                mvUOW.GetRepository<Routine>().Insert(routineModel);
                mvUOW.SaveChanges();
            }
        }

        //<summary>
        //Gets all the routines details.
        //</summary>
        //<returns></returns>
        public List<Routine> GetRoutineDetailsWithVariants()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<Routine>().Items.ToList();
            }
        }

        //<summary>
        //Gets all the routines details Assigned Screen.
        //</summary>
        //<returns></returns>
        public List<Routine> GetRoutineDetailsWithOutVariants()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<Routine>().Items.Where(x => x.ParentRoutineID == 0).ToList();
            }
        }

        internal uint GetRoutineDetailsWithOutVariantsCount()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var routines = mvUOW.GetRepository<Routine>().Items.Where(x => x.ParentRoutineID == 0).ToList();
                return (uint)routines.Count();
            }
        }

        //<summary>
        //Gets all the routines details.
        //</summary>
        //<returns></returns>
        public List<Routine> GetUnAssignedRoutinesWithOutVariants(long analystSelected)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                List<MVCaseFile> lstCFids = mvUOW.GetRepository<MVCaseFile>().Items.Where(x => x.UserId == analystSelected).ToList();
                List<Routine> lstUnAssignedRoutines = new List<Routine>();
                List<int> routineIDs = new List<int>();
                List<Routine> lstAssignedRoutines = new List<Routine>();

                foreach (var id in lstCFids)
                {
                    routineIDs.AddRange(mvUOW.GetRepository<CaseRoutine>().Items.Where(x => x.CaseFileID == id.CfId).Select(y => y.RoutineID).ToList());
                }

                foreach (var id in routineIDs)
                {
                    lstAssignedRoutines.AddRange(mvUOW.GetRepository<Routine>().Items.Where(x => x.ParentRoutineID == 0 && x.Id == id).ToList());
                }

                lstUnAssignedRoutines = mvUOW.GetRepository<Routine>().Items.Where(x => x.ParentRoutineID == 0).ToList();

                if (lstUnAssignedRoutines.IsCollectionValid())
                    lstAssignedRoutines.ForEach(x => lstUnAssignedRoutines.Remove(x));

                return lstUnAssignedRoutines;
            }
        }

        public List<Routine> GetRoutineByFilter(string searchText)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                List<Routine> lstRoutines = new List<Routine>();
                List<Routine> lstParentRoutine = new List<Routine>();
                lstRoutines = mvUOW.GetRepository<Routine>().Items.Where(x => x.RoutineName.Contains(searchText)).ToList();

                foreach (var item in lstRoutines)
                {
                    if (item.ParentRoutineID != 0)
                    {
                        var routines = mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == item.ParentRoutineID).Distinct().ToList();

                        foreach (var routine in routines)
                        {
                            if (!lstRoutines.Contains(routine) && !lstParentRoutine.Contains(routine))
                            {
                                lstParentRoutine.AddRange(routines);
                            }
                        }
                    }
                }

                if (lstParentRoutine.Count != 0 || lstParentRoutine.IsCollectionValid())
                {
                    lstRoutines.AddRange(lstParentRoutine);
                }


                return lstRoutines;
                //return mvUOW.GetRepository<Routine>().Items.Where(x => x.RoutineName.Contains(searchText)).ToList();
            }
        }

        public List<Routine> GetRoutineAfterFilterByIndex(string searchText, uint startingIndex, uint numberOfRecords, bool p)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                List<Routine> lstRoutines = new List<Routine>();
                List<Routine> lstParentRoutine = new List<Routine>();
                lstRoutines = mvUOW.GetRepository<Routine>().Items.Where(x => x.RoutineName.Contains(searchText)).OrderBy(y => y.RoutineName).Skip((int)startingIndex).Take((int)numberOfRecords).ToList();

                foreach (var item in lstRoutines)
                {
                    if (item.ParentRoutineID != 0)
                    {
                        var routines = mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == item.ParentRoutineID).Distinct().ToList();

                        foreach (var routine in routines)
                        {
                            if (!lstRoutines.Contains(routine) && !lstParentRoutine.Contains(routine))
                            {
                                lstParentRoutine.AddRange(routines);
                            }
                        }
                    }
                }

                if (lstParentRoutine.Count != 0 || lstParentRoutine.IsCollectionValid())
                {
                    lstRoutines.AddRange(lstParentRoutine);
                }
                return lstRoutines;
            }
        }

        internal uint GetRoutineCount(object searchText)
        {
            string strsearchText = (string)searchText;
            using (var mvUOW = new MVUnitOfWork())
            {
                var routines = mvUOW.GetRepository<Routine>();

                if (!strsearchText.IsNotEmpty() || !strsearchText.IsNotNull())
                {
                    return (uint)routines.Items.Where(x => x.ParentRoutineID == 0).Count();
                }
                else
                {
                    return (uint)routines.Items.Where(x => x.ParentRoutineID == 0 && x.RoutineName.Contains(strsearchText)).Count();
                }
                //return (uint)routines.Items.Count();
            }
        }

        internal ObservableCollection<Routine> GetRoutinesByIndex(uint startingIndex, uint numberOfRecords, bool p)
        {
            var lstRoutines = new ObservableCollection<Routine>();

            using (var mvUOW = new MVUnitOfWork())
            {
                var query = mvUOW.GetRepository<Routine>().Items.OrderBy(x => x.Id).Skip((int)startingIndex).Take((int)numberOfRecords).ToList();
                query.ForEach(lstRoutines.Add);
            }

            return lstRoutines;
        }

        internal int GetRoutineScore(int routineID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var routineScore = mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == routineID).Select(y => y.RoutineAccountScore).First();

                return routineScore.Value;
            }
        }

        internal Routine GetRoutineDetails(int routineID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == routineID).FirstOrDefault();                                   
            }
        }

        internal List<CaseDetail> GetCaseFiles(long selectedAnalyst)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var caseFile = mvUOW.GetRepository<MVCaseFile>().Items.Select(x => x.CaseNo).ToList();
                var caseDetails = WorkflowAPI.GetCaseDetails(caseFile).Where(x => x.UserId == selectedAnalyst || (x.UserId == 0 && x.LastAnalystId == selectedAnalyst) || x.LastAnalystId == selectedAnalyst).ToList();
                return caseDetails;
            }
        }

        internal MVCaseFile SaveCaseFiles(List<Routine> lstRoutines, MVCaseFile mvCaseFile, int userID)
        {
            var clientName = System.Configuration.ConfigurationManager.AppSettings["ClientName"];
            MVCaseFile caseFile = new MVCaseFile();
            var context = System.Windows.Application.Current.Properties["Context"] as Context;

            using (var mvUOW = new MVUnitOfWork())
            {
                var rep = mvUOW.GetRepository<MVCaseFile>();
                var cases = lstRoutines.Select(r => new MVCaseFile()
                {
                    UserId = userID,
                    CreatedBy = mvCaseFile.CreatedBy,
                    CreatedDate = mvCaseFile.CreatedDate
                }).ToList();

                for (int i = 0; i < cases.Count; i++)
                {
                    rep.Insert(cases[i]);
                }
                mvUOW.SaveChanges();

                var cfids = cases.Select(c => c.CfId).ToList();
                var casesDb = rep.Items.Where(c => cfids.Contains(c.CfId)).ToList();

                casesDb.ForEach(c =>
                {
                    c.CaseNo = string.Format("{0}{1}", clientName, c.CfId.ToString().PadLeft(5, '0'));
                    rep.Update(c);
                    caseFile.CaseNo = c.CaseNo;
                    caseFile.CfId = c.CfId;
                    WorkflowAPI.Assign(caseFile.CaseNo, userID, caseFile.CfId, context.Module.Id);
                });


                int count = 0;
                for (int i = 0; i < cfids.Count; i++)
                {
                    foreach (var item in lstRoutines)
                    {
                        if (count == i)
                        {
                            mvUOW.GetRepository<CaseRoutine>().Insert(new CaseRoutine { RoutineID = item.Id, CaseFileID = cfids[i], CreatedBy = (int)context.User.Id, CreatedDate = DateTime.Now });
                            count++;
                        }
                        i++;
                    }
                }

                mvUOW.SaveChanges();
                return caseFile;
            }
        }

        //querybuilder screen
        internal List<Routine> GetAssignedRoutinesWithVariants(string caseNumberSelected, long selectedAnalyst)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var cfid = mvUOW.GetRepository<MVCaseFile>().Items.Where(x => x.CaseNo == caseNumberSelected && x.UserId == selectedAnalyst).ToList();

                List<CaseRoutine> lstCaseRoutine = new List<CaseRoutine>();
                foreach (var item in cfid)
                {
                    lstCaseRoutine = mvUOW.GetRepository<CaseRoutine>().Items.Where(x => x.CaseFileID == item.CfId).ToList();
                }

                List<Routine> lstAssignedRoutineWithVariant = new List<Routine>();
                foreach (var item in lstCaseRoutine)
                {
                    lstAssignedRoutineWithVariant.AddRange(mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == item.RoutineID || x.ParentRoutineID == item.RoutineID).ToList());
                }

                return lstAssignedRoutineWithVariant;
            }
        }

        internal List<Routine> GetAssignedRoutinesWithVariants(string caseNumberSelected)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var cfid = mvUOW.GetRepository<MVCaseFile>().Items.Where(x => x.CaseNo == caseNumberSelected).ToList();

                List<CaseRoutine> lstCaseRoutine = new List<CaseRoutine>();
                foreach (var item in cfid)
                {
                    lstCaseRoutine = mvUOW.GetRepository<CaseRoutine>().Items.Where(x => x.CaseFileID == item.CfId).ToList();
                }

                List<Routine> lstAssignedRoutineWithVariant = new List<Routine>();
                foreach (var item in lstCaseRoutine)
                {
                    lstAssignedRoutineWithVariant.AddRange(mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == item.RoutineID || x.ParentRoutineID == item.RoutineID).ToList());
                }

                return lstAssignedRoutineWithVariant;
            }
        }

        //assignroutine view model
        internal ObservableCollection<Routine> GetRoutinesWithoutVariantsByIndex(uint startingIndex, uint numberOfRecords, bool p, long selectedAnalyst)
        {
            var lstRoutines = new ObservableCollection<Routine>();

            using (var mvUOW = new MVUnitOfWork())
            {
                if (selectedAnalyst == 0)
                {
                    var query = mvUOW.GetRepository<Routine>().Items.OrderBy(x => x.Id).Where(y => y.ParentRoutineID == 0).Skip((int)startingIndex).Take((int)numberOfRecords).ToList();
                    query.ForEach(lstRoutines.Add);
                }
                else
                {
                    List<MVCaseFile> lstCFids = mvUOW.GetRepository<MVCaseFile>().Items.Where(x => x.UserId == selectedAnalyst).ToList();
                    List<Routine> lstUnAssignedRoutines = new List<Routine>();
                    List<int> routineIDs = new List<int>();
                    List<Routine> lstAssignedRoutines = new List<Routine>();

                    foreach (var id in lstCFids)
                    {
                        routineIDs.AddRange(mvUOW.GetRepository<CaseRoutine>().Items.Where(x => x.CaseFileID == id.CfId).Select(y => y.RoutineID).ToList());
                    }

                    foreach (var id in routineIDs)
                    {
                        lstAssignedRoutines.AddRange(mvUOW.GetRepository<Routine>().Items.Where(x => x.ParentRoutineID == 0 && x.Id == id).ToList());
                    }

                    lstUnAssignedRoutines = mvUOW.GetRepository<Routine>().Items.Where(x => x.ParentRoutineID == 0).ToList();

                    if (lstUnAssignedRoutines.IsCollectionValid())
                        lstAssignedRoutines.ForEach(x => lstUnAssignedRoutines.Remove(x));
                    lstRoutines.AddRange(lstUnAssignedRoutines.Skip((int)startingIndex).Take((int)numberOfRecords));
                }
            }

            return lstRoutines;
        }

        //Routine page view model - without variant
        internal ObservableCollection<Routine> GetRoutinesWithoutVariantsByIndex(uint startingIndex, uint numberOfRecords, bool p)
        {
            var lstRoutinesWithOutVariant = new ObservableCollection<Routine>();

            using (var mvUOW = new MVUnitOfWork())
            {
                var query = mvUOW.GetRepository<Routine>().Items.OrderBy(x => x.Id).Where(y => y.ParentRoutineID == 0).Skip((int)startingIndex).Take((int)numberOfRecords).ToList();
                query.ForEach(lstRoutinesWithOutVariant.Add);
            }

            return lstRoutinesWithOutVariant;
        }

        //Routine page view model - with variant
        internal ObservableCollection<Routine> GetOnlyRoutinesVariants()
        {
            var lstRoutinesWithVariant = new ObservableCollection<Routine>();

            using (var mvUOW = new MVUnitOfWork())
            {
                var query = mvUOW.GetRepository<Routine>().Items.OrderBy(x => x.Id).Where(y => y.ParentRoutineID != 0).ToList();
                query.ForEach(lstRoutinesWithVariant.Add);
            }

            return lstRoutinesWithVariant;
        }

        //internal List<T> ExecuteQuery<T>(string sqlQuery)
        //{
        //    using (var mvUOW = new MVUnitOfWork())
        //    {
        //        List<T> QueryResult = mvUOW.ExecuteSqlQuery<T>(sqlQuery);
        //        return QueryResult;
        //    }
        //}

        //internal IQueryable<T> ExecuteQuery<T>(string sqlQuery)
        //{
        //    //using (var mvUOW = new MVUnitOfWork())
        //    //{
        //    //    return mvUOW.ExecuteSqlQuery<T>(sqlQuery).AsQueryable<T>();                
        //    //}

        //    var mvUOW = new MVUnitOfWork();            
        //    return mvUOW.ExecuteSqlQuery<T>(sqlQuery).AsQueryable<T>();

        //}

        internal DataTable ExecuteQuery(string sqlQuery)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.ExecuteSqlQuery(sqlQuery);
            }
        }

        internal SQLQuery GetQuerySyntaxByRoutineID(int? routineID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var sqlQuery = mvUOW.GetRepository<SQLQuery>().Items.Where(x => x.RoutineID == routineID).FirstOrDefault();
                return sqlQuery;
            }
        }

        internal bool GetRoutineID(int? routineID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                bool ifRoutineExists;
                var routine = mvUOW.GetRepository<SQLQuery>().Items.Where(x => x.RoutineID == routineID).FirstOrDefault();
                if (routine.IsNotNull())
                {
                    ifRoutineExists = true;
                }
                else
                {
                    ifRoutineExists = false;
                }
                return ifRoutineExists;
            }
        }

        internal void SaveSQLQuery(SQLQuery sqlQueryModel)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var tblSqlQuery = mvUOW.GetRepository<SQLQuery>();
                var sqlQuery = tblSqlQuery.Items.FirstOrDefault(x => x.RoutineID == sqlQueryModel.RoutineID);
                if (sqlQuery != null)
                {
                    sqlQuery.QuerySyntax = sqlQueryModel.QuerySyntax;
                    sqlQuery.QueryName = sqlQueryModel.QueryName;
                }
                else
                {
                    mvUOW.GetRepository<SQLQuery>().Insert(sqlQueryModel);
                }
                mvUOW.SaveChanges();
            }
        }

        internal void UpdateSQLQuery(SQLQuery sqlQueryModel)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var tblSqlQuery = mvUOW.GetRepository<SQLQuery>();
                var sqlQuery = tblSqlQuery.Items.FirstOrDefault(x => x.RoutineID == sqlQueryModel.RoutineID);
                sqlQuery.QuerySyntax = sqlQueryModel.QuerySyntax;
                sqlQuery.QueryName = sqlQueryModel.QueryName;
                tblSqlQuery.SaveChanges();
            }
        }

        //internal List<ViewModels.MVTransNormViewModel> ExecuteQuery1<T1>(string p)
        //{
        //    throw new NotImplementedException();
        //}

        internal SQLQuery GetParentRoutineID(int? routineID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                int? routine = mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == routineID).Select(y => y.ParentRoutineID).FirstOrDefault();
                var sqlQuery = mvUOW.GetRepository<SQLQuery>().Items.Where(x => x.RoutineID == routine).FirstOrDefault();
                return sqlQuery;
            }
        }

        internal List<AccountScores> GetAccountScoreByTranNo(DataTable sampleData)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<AccountScores>().Items.ToList()
                  .Join(sampleData.AsEnumerable(), x => x.TranNo, y => y["TranNo"], (x, y) => (x))
                  .ToList<AccountScores>();
            }
        }

        public List<SQLQuery> GetSQLQueryName(ObservableCollection<RoutineViewModel> observableCollection)
        {
            var sqlQuery = new List<SQLQuery>();
            observableCollection.Each(x =>
            {
                sqlQuery.Add(GetSQLQueryName(x.RoutineID));
                x.ChildRoutineViewModelCollection.Each(v =>
                {
                    sqlQuery.Add(GetSQLQueryName(v.RoutineID));
                });
            });
            return sqlQuery;
        }

        private SQLQuery GetSQLQueryName(int routineID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<SQLQuery>().Items.Where(x => x.RoutineID == routineID).FirstOrDefault();
            }
        }

        internal void SaveRoutineAccountScores(List<AccountScores> lstAccountScores, int parentRoutine)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var tlbAccountScore = mvUOW.GetRepository<AccountScores>();
                var tlbTransactionSavedScore = mvUOW.GetRepository<TransactionScores>().Items.Where(x => x.RoutineID == parentRoutine);
                var tlbTransactionScore = new List<AccountScores>();
                tlbTransactionSavedScore.Each(c =>
                {
                    var accountScore = new AccountScores() { TranNo = c.TranNo };
                    tlbTransactionScore.Add(accountScore);
                });
                var savedAccountTransactions = tlbAccountScore.Items.Include(f => f.AccountScoreRoutine).ToList()
                   .Join(lstAccountScores, x => x.TranNo, y => y.TranNo, (x, y) => (x)).ToList<AccountScores>();

                lstAccountScores.ForEach(x =>
                {
                    var savedEntity = savedAccountTransactions.Where(y => y.TranNo == x.TranNo).FirstOrDefault<AccountScores>();
                    if (savedEntity != null)
                    {
                        savedEntity.TranNo = x.TranNo;
                        savedEntity.AccountScore = x.AccountScore;
                        savedEntity.ModifiedBy = Convert.ToInt32(context.User.Id);
                        savedEntity.ModifiedDate = System.DateTime.Now;
                        if (savedEntity.AccountScoreRoutine.Where(t => t.RoutineID == parentRoutine).FirstOrDefault() == null)
                        {
                            var newAccountScoreRoutine = new AccountScoreRoutine
                            {
                                AccountScoreID = savedEntity.Id,
                                TranNo = (int)savedEntity.TranNo,
                                RoutineID = parentRoutine,
                                CreatedBy = Convert.ToInt32(context.User.Id),
                                CreatedDate = System.DateTime.Now,
                                AccountScores = savedEntity
                            };
                            savedEntity.AccountScoreRoutine.Add(newAccountScoreRoutine);
                        }
                        tlbAccountScore.Context.Entry(savedEntity).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        x.CreatedBy = Convert.ToInt32(context.User.Id);
                        x.CreatedDate = System.DateTime.Now;
                        x.AccountScoreRoutine = new List<AccountScoreRoutine>()
                        {
                            new AccountScoreRoutine
                           {
                               TranNo = x.TranNo.Value,
                               RoutineID = parentRoutine,
                               CreatedBy = Convert.ToInt32(context.User.Id),
                               CreatedDate = System.DateTime.Now
                           }
                        };
                        tlbAccountScore.Context.Entry(x).State = System.Data.Entity.EntityState.Added;
                    }
                });

                var deleteTransaction = tlbTransactionScore
                                             .Select(x => x.TranNo).ToList()
                                             .Except(lstAccountScores.Select(y => y.TranNo)
                                             .ToList());

                tlbTransactionSavedScore.Each(y =>
                {
                    if (deleteTransaction.Contains(y.TranNo))
                    {
                        var updateAccountScore = tlbAccountScore.Items.Where(x => x.TranNo == y.TranNo).Include(x => x.AccountScoreRoutine).First();
                        if (updateAccountScore.AccountScore > 0)
                        {
                            var routineScore = (int)mvUOW.GetRepository<Routine>()
                                .Items
                                .Where(a => a.ParentRoutineID == y.RoutineID)
                                .Select(t => t.RoutineAccountScore).FirstOrDefault();
                            updateAccountScore.AccountScore -= routineScore;
                            updateAccountScore.ModifiedBy = Convert.ToInt32(context.User.Id);
                            updateAccountScore.ModifiedDate = System.DateTime.Now;
                            var removeAccountScoreRoutine = updateAccountScore.AccountScoreRoutine.Where(t => t.RoutineID == parentRoutine).First();
                            //  updateAccountScore.AccountScoreRoutine.Remove(removeAccountScoreRoutine);
                            tlbAccountScore.Context.Entry(removeAccountScoreRoutine).State = System.Data.Entity.EntityState.Deleted;
                            tlbAccountScore.Context.Entry(updateAccountScore).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                });
                tlbAccountScore.Context.SaveChanges();
            }
        }

        internal DataTable GetStandardDeviationResults(double sampleSize)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var tlbAccountScore = mvUOW.GetRepository<AccountScores>().Items.ToList();

                tlbAccountScore.OrderBy(x => x.AccountScore).Take(Convert.ToInt32(sampleSize));

                DataTable dtAccountScores = new DataTable();
                foreach (var item in typeof(AccountScores).GetProperties())
                {
                    dtAccountScores.Columns.Add(item.Name, Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType);
                }

                foreach (var item in tlbAccountScore)
                {
                    dtAccountScores.Rows.Add(item.Id, item.TranNo, item.AccountScore, item.AccountNumber);
                }

                return dtAccountScores;
            }
        }

        internal List<Priority> GetPriority()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<Priority>().Items.ToList();
            }
        }

        internal void SaveTransactionScores(List<TransactionScores> lstTransactionScores, int parentRoutine)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var tlbtransactionScore = mvUOW.GetRepository<TransactionScores>();
                var savedTransactions = tlbtransactionScore.Items.ToList()
                    .Where(x => x.RoutineID == parentRoutine).ToList<TransactionScores>();
                lstTransactionScores.ForEach(x =>
                {
                    var savedEntity = savedTransactions.Where(y => y.TranNo == x.TranNo).FirstOrDefault<TransactionScores>();
                    if (savedEntity != null)
                    {
                        savedEntity.ModifiedBy = Convert.ToInt32(context.User.Id);
                        savedEntity.ModifiedDate = System.DateTime.Now;
                        tlbtransactionScore.Context.Entry(savedEntity).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        tlbtransactionScore.Context.Entry(x).State = System.Data.Entity.EntityState.Added;
                    }
                });
                var deleteTransaction = savedTransactions
                                             .Select(x => x.TranNo).Except(lstTransactionScores.Select(y => y.TranNo).ToList());
                savedTransactions.ForEach(y =>
                {
                    if (deleteTransaction.Contains(y.TranNo))
                        tlbtransactionScore.Context.Entry(y).State = System.Data.Entity.EntityState.Deleted;

                });
                tlbtransactionScore.Context.SaveChanges();
            }
        }

        internal List<string> GetAllCaseNumbers()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<MVCaseFile>().Items.Select(x => x.CaseNo).ToList();
            }
        }

        internal Routine GetRoutinesDetailsByCaseNo(string caseNo)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var cfid = mvUOW.GetRepository<MVCaseFile>().Items.Where(x => x.CaseNo == caseNo).FirstOrDefault().CfId;
                Routine routine = new Routine();
                var routineid = mvUOW.GetRepository<CaseRoutine>().Items.Where(x => x.CaseFileID == cfid).FirstOrDefault().RoutineID;

                return mvUOW.GetRepository<Routine>().Items.Where(x => x.Id == routineid).FirstOrDefault();
            }
        }

        internal long GetCFid(string caseNo)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<MVCaseFile>().Items.Where(x => x.CaseNo == caseNo).FirstOrDefault().CfId;
            }
        }

        internal void UpdateCaseFile(string caseNo, long userId, int routineID, long cfID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var tlbCaseFile = mvUOW.GetRepository<MVCaseFile>().Items.Where(x => x.CaseNo == caseNo).FirstOrDefault();

                if (tlbCaseFile != null)
                {
                    tlbCaseFile.UserId = (int)userId;
                    tlbCaseFile.ModifedDate = DateTime.Now;
                    tlbCaseFile.ModifiedBy = (int)userId;
                    mvUOW.SaveChanges();

                    var tlbCaseRoutine = mvUOW.GetRepository<CaseRoutine>().Items.Where(x => x.CaseFileID == cfID).FirstOrDefault();
                    tlbCaseRoutine.ModifiedDate = DateTime.Now;
                    tlbCaseRoutine.ModifiedBy = (int)userId;
                    mvUOW.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get User by StateId
        /// </summary>
        /// <param name="stateId">stateId</param>
        /// <returns>List of users</returns>
        internal List<User> GetUsersByStateId(int stateId)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var userRepo = mvUOW.GetRepository<User>();
                var appcontext = Application.Current.Properties["Context"] as Context;
                var users = userRepo.Items.Include(user => user.UserInGroup.Select(x => x.Group)).Where(x => x.IsActive && x.UserInGroup.Any(gm => gm.IsActive && gm.ModuleTypeId == appcontext.Module.Id && gm.Group.States.Any(s => s.ModuleTypeId == appcontext.Module.Id && s.Id == stateId))).ToList();
                return users;
            }
        }

        internal int GetTransactionScoresCount()
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                //return mvUOW.GetRepository<TransactionScores>().Items.Where(x => new TransactionScores  {  })

                return mvUOW.GetRepository<TransactionScores>().Items.Count();
            }
        }

        internal bool CheckParentRoutine(int parentRoutine, int transactionNo)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<TransactionScores>().Items.Any(x => x.RoutineID == parentRoutine && x.TranNo == transactionNo);
            }
        }

        internal List<int> GetRoutineIds(int _tranNo)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var accountScoreRepo = mvUOW.GetRepository<AccountScoreRoutine>();
                return accountScoreRepo.Items.Where(x => x.TranNo == _tranNo).Select(x => x.RoutineID).ToList();
            }
        }

        internal List<Routine> GetRoutineScore(List<int> routineIds)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                var routineRepo = mvUOW.GetRepository<Routine>();
                return routineRepo.Items.Where(x => routineIds.Contains(x.Id)).ToList();
            }
        }

        //internal List<Routine> GetRoutineScore(List<int> rotineIds, int startIndex, int displayItemCount, out int totalCount)
        //{
        //    using (var mvUOW = new MVUnitOfWork())
        //    {
        //        var routineRepo = mvUOW.GetRepository<Routine>();
        //        totalCount = routineRepo.Items.Where(x => rotineIds.Contains(x.Id)).Count();
        //        return routineRepo.Items.Where(x => rotineIds.Contains(x.Id)).OrderBy(x=>x.RoutineName).Skip(startIndex).Take(displayItemCount).ToList();
        //    }
        //}

        //internal List<Routine> GetRoutineScore(List<int> rotineIds, int startIndex, int displayItemCount)
        //{
        //    using (var mvUOW = new MVUnitOfWork())
        //    {
        //        var routineRepo = mvUOW.GetRepository<Routine>();
        //        return routineRepo.Items.Where(x => rotineIds.Contains(x.Id)).OrderBy(x => x.RoutineName).Skip(startIndex).Take(displayItemCount).ToList();
        //    }
        //}          

        public List<TransNorm> GetStandardDeviationData(int routineID)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<TransactionScores>().Items.Where(x => x.RoutineID == routineID)
                    .Join(mvUOW.GetRepository<TransNorm>().Items, t => t.TranNo, o => o.TranNo, (t, o) => (o)).ToList<TransNorm>();

                //var transactionScores = mvUOW.GetRepository<TransactionScores>().Items;
                //var transnorm = mvUOW.GetRepository<TransNorm>().Items;
                //var accountScoreRoutine = mvUOW.GetRepository<AccountScoreRoutine>().Items;
                //var accountScores = mvUOW.GetRepository<AccountScores>().Items;

                //var data= from ts in transactionScores join asr in accountScoreRoutine on ts.RoutineID equals asr.RoutineID
                //          join accs in accountScores on asr.AccountScoreID equals accs.Id
                //          join tn in transnorm on ts.TranNo equals tn.TranNo
                //          where ts.RoutineID==routineID
                //          select new TransNorm
                //          {
                //              Ro
                //          }

            }
        }

        /// <summary>
        /// AutoComplete Search 
        /// </summary>
        /// <param name="_stateType"></param>
        /// <returns></returns>
        internal List<string> GetAllRoutinesForSearch(string searchText)
        {
            using (var wfUOW = new MVUnitOfWork())
            {
                var routines = wfUOW.GetRepository<Routine>();
                return routines.Items.Where(x => x.RoutineName.ToLower().StartsWith(searchText.Trim().ToLower())).Select(x => x.RoutineName).Take(10).ToList();
            }
        }

        internal void DeleteUnAssignedCaseNumber(List<string> unAssignedCaseNumbers)
        {
            if (!unAssignedCaseNumbers.IsCollectionValid())
                throw new BusinessException("Case number list is empty.");
            else
            {
                using (var mvUOW = new MVUnitOfWork())
                {
                    foreach (var item in unAssignedCaseNumbers)
                    {
                        var caseFileRepo = mvUOW.GetRepository<MVCaseFile>();
                        var caseFile = caseFileRepo.Items.FirstOrDefault(x => x.CaseNo == item.ToString());
                        if (caseFile.IsNotNull())
                        {
                            var caseRoutineRepo = mvUOW.GetRepository<CaseRoutine>();
                            var caseRoutine = caseRoutineRepo.Items.FirstOrDefault(x => x.CaseFileID == caseFile.CfId);
                            if (caseRoutine.IsNotNull())
                            {
                                caseRoutineRepo.Delete(caseRoutine);
                                caseFileRepo.Delete(caseFile);
                            }
                        }
                    }
                    mvUOW.SaveChanges();
                }
            }
        }

        internal List<Routine> GetChildRoutinesByID(int routineID,List<int> childIds)
        {
            using (var mvUOW = new MVUnitOfWork())
            {
                return mvUOW.GetRepository<Routine>().Items.Where(x => x.ParentRoutineID == routineID && !childIds.Contains(x.Id)).ToList();
            }
        }
    }
}
