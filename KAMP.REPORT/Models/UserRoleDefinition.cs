﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.REPORT.Models
{
   public class UserRoleDefinition
    {
       public long UserID { get; set; }
       public string RoleName { get; set; }
    }
}
