﻿using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.WF;
using KAMP.REPORT.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.REPORT.Models
{
   public class ReportContext
    {
        public static dynamic DataSource { get; set; }
        public static ReportDTO ReportInfo { get; set; }
        public static ObjectCacheContainer CacheContainer { get; set; }
    }
}
