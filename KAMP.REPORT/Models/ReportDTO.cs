﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.REPORT.Models
{
    public class ReportDTO
    {
        public string ReportName { get; set; }
        public string ReportRDLC { get; set; }
        public string CaseNo { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string SelectedUserName { get; set; }
        public string SelectedRoleName { get; set; }
        public string SelectedKPI { get; set; }
        public string SelectedCompliance { get; set; }
        
    }
}
