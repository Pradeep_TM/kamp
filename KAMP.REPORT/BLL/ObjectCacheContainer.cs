﻿using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.WF;
using KAMP.REPORT.Common;
using KAMP.REPORT.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace KAMP.REPORT.BLL
{
    public class ObjectCacheContainer
    {

        #region Private Members
        private MemoryCache memoryCache = null;
        private CacheItemPolicy cip = null;
        private Dictionary<string, dynamic> CachedItems = null;
        private byte[] result = null;
        #endregion

        public ObjectCacheContainer()
        {
            memoryCache = MemoryCache.Default;
        }

        #region Private Methods

        private byte[] GetSizeofCollection(byte[] a, byte[] b, byte[] c)
        {
            try
            {
                result = new byte[a.Length + b.Length + c.Length];
                System.Buffer.BlockCopy(a, 0, result, 0, a.Length);
                System.Buffer.BlockCopy(b, 0, result, a.Length, b.Length);
                System.Buffer.BlockCopy(c, 0, result, b.Length, c.Length);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally { a = null; b = null; c = null; }
        }

        private Dictionary<string, dynamic> GetCachedItems(List<AssignedTo> casesByModule, List<User> lstOfUsers, List<UserRoleDefinition> userRole)
        {
            CachedItems = new Dictionary<string, dynamic>();
            CachedItems.Add(ReportConstants.CASES_BY_MODULE, new List<AssignedTo>(casesByModule));
            CachedItems.Add(ReportConstants.LST_OF_USRS, new List<User>(lstOfUsers));
            CachedItems.Add(ReportConstants.USRS_ROLE, new List<UserRoleDefinition>(userRole));
            casesByModule.Clear();
            lstOfUsers.Clear();
            userRole.Clear();
            return CachedItems;
        }

        private CacheItemPolicy GetCachePolicy()
        {
            try
            {
                cip = new CacheItemPolicy()
                {
                    Priority = CacheItemPriority.Default,
                    UpdateCallback = new CacheEntryUpdateCallback(UpdateHandler),
                    AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddMinutes(15))
                };
                return cip;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        private void UpdateHandler(CacheEntryUpdateArguments args)
        {
            var cacheItem = memoryCache.GetCacheItem(args.Key);
            var cacheObj = cacheItem.Value;
            cacheItem.Value = cacheObj;
            args.UpdatedCacheItem = cacheItem;
            args.UpdatedCacheItemPolicy = GetCachePolicy();

        }

        #endregion

        #region Public Methods

        public void AddObject(List<AssignedTo> casesByModule, List<User> lstOfUsers, List<UserRoleDefinition> userRole)
        {
            try
            {
                ClearAll();
                if (!memoryCache.Contains(ReportConstants.REPORT_CACHE_ITEM))
                    memoryCache.Set(ReportConstants.REPORT_CACHE_ITEM, GetCachedItems(casesByModule, lstOfUsers, userRole), GetCachePolicy(), null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public bool HasMemoryLimit(byte[] a, byte[] b, byte[] c)
        {
            return GetSizeofCollection(a, b, c).LongLength <= memoryCache.CacheMemoryLimit;
        }

        public dynamic GetOjectFromCache(string value)
        {
            try
            {
                if (memoryCache != null)
                {
                    var getObject = (Dictionary<string, dynamic>)memoryCache.Get(ReportConstants.REPORT_CACHE_ITEM, null);
                    var getObjectItem = getObject.FirstOrDefault(obj => obj.Key.Equals(value)).Value;
                    return getObjectItem;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        private void ClearCachedItems()
        {
            List<string> cacheKeys = MemoryCache.Default.Select(x => x.Key).ToList();
            foreach (string cacheKey in cacheKeys)
                MemoryCache.Default.Remove(cacheKey);

        }

        public void ClearAll()
        {
            ClearCachedItems();
            if (CachedItems != null)
            { CachedItems.Clear(); CachedItems = null; }

            result = null;
        }

        #endregion


    }
}
