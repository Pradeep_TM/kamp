﻿using KAMP.Core.Interfaces;
using KAMP.Core.Repository.Report;
using KAMP.Core.Repository.UM;
using KAMP.REPORT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.Repository.UnitOfWork;
using KAMP.Core.Repository.Context;

namespace KAMP.REPORT.BLL
{
    using Core.Repository;
    using Core.Repository.UnitOfWork;
    using KAMP.Core.Repository.WF;
    using KAMP.Core.FrameworkComponents;
    using KAMP.REPORT.Common;
    using KAMP.Core.Repository.RFI;
    using System.Web;
    using System.Text.RegularExpressions;
    using KAMP.Core.Repository.Models.Report;
    public class ProductivityReports : IReports<ProductivityReports>
    {
        private string tempDaysAssigned = string.Empty;
        private ObjectCacheContainer cacheContainer = null;
        private List<AssignedTo> casesByModule = null;
        private List<User> lstOfUsers = null;
        private List<UserRoleDefinition> usrRoles = null;

        public ProductivityReports(int moduleTypeId)
        {
            try
            {
                casesByModule = GetCasesByModule(moduleTypeId);
                if (casesByModule.IsCollectionValid())
                {
                    lstOfUsers = GetUsers(casesByModule.Select(x => x.UserId).ToList());
                    usrRoles = GetRoles(casesByModule.Select(x => x.UserId).Distinct().ToList());
                }

                cacheContainer = ReportContext.CacheContainer = new ObjectCacheContainer();

                if (cacheContainer.HasMemoryLimit(casesByModule.SelectMany(s => System.Text.Encoding.UTF8.GetBytes(s.ToString())).ToArray(),
                                 lstOfUsers.SelectMany(s => System.Text.Encoding.UTF8.GetBytes(s.ToString())).ToArray(),
                                 usrRoles.SelectMany(s => System.Text.Encoding.UTF8.GetBytes(s.ToString())).ToArray()))
                {
                    cacheContainer.AddObject(casesByModule, lstOfUsers, usrRoles);
                    ClearCollection();
                }
                else
                {
                    cacheContainer = ReportContext.CacheContainer = null;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        ~ProductivityReports()
        {
            if (cacheContainer != null)
                cacheContainer = null;
            if (casesByModule.IsCollectionValid())
                casesByModule.Clear();
            if (lstOfUsers.IsCollectionValid())
                lstOfUsers.Clear();
            if (usrRoles.IsCollectionValid())
                usrRoles.Clear();
        }

        #region Private Methods

        private void ClearCollection()
        {
            if (cacheContainer.IsNotNull())
            {
                if (casesByModule.IsCollectionValid())
                    casesByModule.Clear();
                if (lstOfUsers.IsCollectionValid())
                    lstOfUsers.Clear();
                if (usrRoles.IsCollectionValid())
                    usrRoles.Clear();
            }
        }

        private List<AssignedTo> ModuleSpecificCases()
        {
            return cacheContainer.GetOjectFromCache(ReportConstants.CASES_BY_MODULE) as List<AssignedTo>;
        }

        private List<User> UsersSpecificToCases()
        {
            return cacheContainer.GetOjectFromCache(ReportConstants.LST_OF_USRS) as List<User>;
        }

        private List<UserRoleDefinition> RolesSpecificToUsers()
        {
            return cacheContainer.GetOjectFromCache(ReportConstants.USRS_ROLE) as List<UserRoleDefinition>;
        }

        internal List<AssignedTo> GetCasesByModule(int moduleTypeId)
        {
            using (var RptUOW = new ReportUnitOfWork())
            {
                var getCasesByModule = RptUOW.GetRepository<AssignedTo>().Items.Include(x => x.Assignment)
                    .Include(x => x.Assignment.WFTracking).Where(x => x.AssignmentId == x.Assignment.Id &&
                        x.Assignment.WFTracking.Any(wfTrack => wfTrack.ModuleTypeId == moduleTypeId));
                return getCasesByModule.ToList();
            }
        }

        internal List<User> GetUsers(List<long> userIds)
        {
            using (var RptUOW = new ReportUnitOfWork())
            {
                var userRepo = RptUOW.GetRepository<User>();
                return userRepo.Items.Where(x => userIds.Contains(x.UserId)).ToList();
            }
        }

        internal List<UserRoleDefinition> GetRoles(List<long> userIds)
        {
            using (var RptUOW = new ReportUnitOfWork())
            {
                var userInRoleRepo = RptUOW.GetRepository<UserInRole>();
                var users = userInRoleRepo.Items.Include(x => x.Role).Where(x => userIds.Contains(x.UserId)).ToList();

                var userRepo = RptUOW.GetRepository<User>();
                var superAdmin = userRepo.Items.Where(x => userIds.Contains(x.UserId) && x.IsSuperAdmin == true).ToList();

                var roleDef = new List<UserRoleDefinition>();

                if (users.IsCollectionValid())
                {
                    users.ForEach(u => roleDef.Add(new UserRoleDefinition()
                    {
                        UserID = u.UserId,
                        RoleName = u.Role.RoleName
                    }));
                }

                if (superAdmin.IsCollectionValid())
                {
                    superAdmin.ForEach(u => roleDef.Add(new UserRoleDefinition()
                    {
                        UserID = u.UserId,
                        RoleName = ReportConstants.SUPER_ADMIN
                    }));
                }

                return roleDef;

            }

        }

        private string IntegerToString(int? val)
        {
            return Convert.ToString(val);
        }

        private string DiffInDate(TimeSpan? days)
        {
            return days.HasValue ? Convert.ToString(days.Value.Days) : string.Empty;
        }

        private string DateTimetoString(DateTime? _dateTime)
        {
            return _dateTime.HasValue ? _dateTime.Value.ToString(ReportConstants.DATE_FORMAT) : ReportConstants.NOT_APPLICABLE;
        }

        private string AssignClassification(string days)
        {
            long initialValue = 20;
            tempDaysAssigned = null;
            if (!string.IsNullOrEmpty(days))
            {
                long val = Convert.ToInt64(days);

                if (val >= 0 && val <= 20)
                    return string.Format("{0}-{1}", Convert.ToString(0), Convert.ToString(initialValue));

                if (val > 20)
                    return GetCategory(initialValue, val);
            }

            return string.Empty;
        }

        private string GetCategory(long initialValue, long val)
        {
            int interval = 20;

            if (val > initialValue && val <= (initialValue + interval))
            {
                return string.Format("{0}-{1}", Convert.ToString(initialValue), Convert.ToString(initialValue + interval));
            }
            return GetCategory(initialValue + interval, val);
        }

        private string GetDaysAssigned(DateTime? createdDate, DateTime? openDate)
        {
            if (openDate != null)
            {
                tempDaysAssigned = DiffInDate(openDate.Value.Date - createdDate.Value.Date);
                return tempDaysAssigned;
            }
            else
            {
                tempDaysAssigned = DiffInDate(DateTime.Now.Date - createdDate.Value.Date);
                return tempDaysAssigned;
            }
        }

        private string GetDaysOpen(string status, DateTime? openDate, DateTime? modifiedDate, DateTime? parkDate, DateTime? unparkDate)
        {
            if (!string.IsNullOrEmpty(status) && status.Equals(ReportConstants.COMPLETED))
            {
                if (parkDate != null && unparkDate != null)
                    return (DiffInDate(modifiedDate.Value.Date - openDate.Value.Date) + "(" + DiffInDate(unparkDate.Value.Date - parkDate.Value.Date) + ")");
                else
                    return (DiffInDate(modifiedDate.Value.Date - openDate.Value.Date));
            }
            else
            {
                if (openDate != null && unparkDate != null)
                {
                    return (DiffInDate(DateTime.Now.Date - openDate.Value.Date) + "(" + DiffInDate(unparkDate.Value.Date - parkDate.Value.Date) + ")");
                }
                else if (openDate != null && parkDate != null)
                {
                    return (DiffInDate(DateTime.Now.Date - openDate.Value.Date) + "(" + DiffInDate(DateTime.Now.Date - parkDate.Value.Date) + ")" + "(" + ReportConstants.STATE_PARKED + ")");
                }
                else if (openDate.HasValue)
                {
                    return (DiffInDate(DateTime.Now.Date - openDate.Value.Date));
                }
                else
                {
                    return ReportConstants.STATE_NOT_OPEN;
                }
            }

        }

        private string GetRoleName(List<string> roles)
        {
            string temp = string.Empty;
            if (roles.Count == 1)
            {
                return roles[0].ToString();
            }
            else
            {
                for (int i = 0; i < roles.Count; i++)
                {
                    if (i == 0)
                        temp = roles[i];
                    else
                        temp = temp + "," + roles[i];
                }
                return temp;
            }
        }

        private string GetEncodedText(string message)
        {
            return Regex.Replace(message, "<(.|\n)*?>", "").Replace("\r\n", string.Empty);
        }


        #endregion

        #region Public Methods

        public object Execute(ProductivityReports ReportType, Func<ProductivityReports, object> ReportKPI)
        {
            return ReportKPI.Invoke(ReportType);
        }

        public List<User> GetUsersByRole(string role)
        {
            try
            {
                using (var context = new ReportUnitOfWork())
                {
                    var getObjects = context.GetRepository<Role>().Items.Where(r => r.RoleName == role)
                         .Join(context.GetRepository<UserInRole>().Items, r => r.RoleId, ur => ur.RoleId, (r, ur) => new { r, ur })
                         .Join(context.GetRepository<User>().Items, r => r.ur.UserId, ro => ro.UserId, (r, ro) => new { r, ro }).ToList()
                         .Select(m => new User { UserName = m.ro.UserName }).ToList<User>();

                    return getObjects;

                }
            }
            catch (Exception)
            {
                return null;
            }

        }

        public List<string> GetExistingRoles()
        {
            try
            {
                if (!usrRoles.IsCollectionValid())
                    usrRoles = new List<UserRoleDefinition>(RolesSpecificToUsers());
                return usrRoles.IsCollectionValid() ? usrRoles.Select(r => r.RoleName).Distinct().ToList() : null;
            }
            catch (Exception)
            {
                return null;
            }
            finally { ClearCollection(); }

        }

        public List<User> GetUsersByDefinedRole(string role)
        {
            try
            {
                
                if (!lstOfUsers.IsCollectionValid())
                    lstOfUsers = new List<User>(UsersSpecificToCases());
                if (!usrRoles.IsCollectionValid())
                    usrRoles = new List<UserRoleDefinition>(RolesSpecificToUsers());
                return (lstOfUsers.IsCollectionValid() && usrRoles.IsCollectionValid()) ?
                    lstOfUsers.Where(u => usrRoles.Where(r => r.RoleName.Equals(role)).Select(r => r.UserID).ToList().Contains(u.UserId))
                    .Select(u => new User { UserName = u.UserName }).ToList<User>() : null;
            }
            catch (Exception)
            {
                return null;
            }
            finally { ClearCollection(); }
        }

        public List<CaseAgingReportDetails> GetCaseAging(ReportDTO reportDTO)
        {
            try
            {
                if (!casesByModule.IsCollectionValid())
                    casesByModule = new List<AssignedTo>(ModuleSpecificCases());
                if (!lstOfUsers.IsCollectionValid())
                    lstOfUsers = new List<User>(UsersSpecificToCases());
                if (!usrRoles.IsCollectionValid())
                    usrRoles = new List<UserRoleDefinition>(RolesSpecificToUsers());

                if (casesByModule.IsCollectionValid() && lstOfUsers.IsCollectionValid() && usrRoles.IsCollectionValid())
                {
                    return casesByModule.Select(cs => new CaseAgingReportDetails
                    {
                        CaseNumber = cs.Assignment.CaseNo,
                        CaseLevel = GetRoleName(usrRoles.Where(x => x.UserID == cs.UserId).Select(x => x.RoleName).ToList()),
                        Opened = DateTimetoString(cs.OpenDate),
                        CurrentReviewer = lstOfUsers.FirstOrDefault(x => x.UserId == cs.UserId).UserName,
                        CaseStatus = cs.State,
                        DaysAssigned = GetDaysAssigned(cs.CreatedDate, cs.OpenDate),
                        DaysOpen = GetDaysOpen(cs.WorkflowStatus, cs.OpenDate, cs.ModifiedDate, cs.ParkDate, cs.UnParkDate),
                        ClassifiedByDays = AssignClassification(tempDaysAssigned),
                        WorkflowStatus = cs.WorkflowStatus
                    }).ToList<CaseAgingReportDetails>();
                }
                return null;
            }
            catch (Exception) { return null; }
            finally
            {
                tempDaysAssigned = null;
                ClearCollection();
            }
        }

        public List<CaseAgingReportDetails> GetCaseAgingByUser(ReportDTO reportDTO)
        {
            try
            {
                if (!casesByModule.IsCollectionValid())
                    casesByModule = new List<AssignedTo>(ModuleSpecificCases());
                if (!lstOfUsers.IsCollectionValid())
                    lstOfUsers = new List<User>(UsersSpecificToCases());
                if (!usrRoles.IsCollectionValid())
                    usrRoles = new List<UserRoleDefinition>(RolesSpecificToUsers());

                if (casesByModule.IsCollectionValid() && lstOfUsers.IsCollectionValid() && usrRoles.IsCollectionValid())
                {
                    var casesByUser = lstOfUsers.Where(u => u.UserName.Equals(reportDTO.SelectedUserName))
                      .Join(casesByModule, u => u.UserId, cs => cs.UserId, (u, cs) => new { u, cs });
                    if (casesByUser.IsCollectionValid())
                    {
                        return casesByUser.Select(r => new CaseAgingReportDetails
                        {
                            CaseNumber = r.cs.Assignment.CaseNo,
                            CaseLevel = GetRoleName(usrRoles.Where(x => x.UserID == r.cs.UserId).Select(x => x.RoleName).ToList()),
                            Opened = DateTimetoString(r.cs.OpenDate),
                            CurrentReviewer = r.u.UserName,
                            CaseStatus = r.cs.State,
                            DaysAssigned = GetDaysAssigned(r.cs.CreatedDate, r.cs.OpenDate),
                            DaysOpen = GetDaysOpen(r.cs.WorkflowStatus, r.cs.OpenDate, r.cs.ModifiedDate, r.cs.ParkDate, r.cs.UnParkDate),
                            ClassifiedByDays = AssignClassification(tempDaysAssigned),
                            WorkflowStatus = r.cs.WorkflowStatus
                        }).ToList<CaseAgingReportDetails>();
                    }
                }
                return null;
            }
            catch (Exception) { return null; }
            finally { tempDaysAssigned = null; ClearCollection(); }
        }

        public List<CaseAgingReportDetails> GetClosedCases(ReportDTO reportDTO)
        {
            try
            {
                if (!casesByModule.IsCollectionValid())
                    casesByModule = new List<AssignedTo>(ModuleSpecificCases());
                if (!lstOfUsers.IsCollectionValid())
                    lstOfUsers = new List<User>(UsersSpecificToCases());
                if (!usrRoles.IsCollectionValid())
                    usrRoles = new List<UserRoleDefinition>(RolesSpecificToUsers());

                if (casesByModule.IsCollectionValid() && lstOfUsers.IsCollectionValid() && usrRoles.IsCollectionValid())
                {
                    var closedCases = casesByModule.Where(c => c.Assignment.Status.Equals(ReportConstants.DELIVERED) && c.WorkflowStatus.Equals(ReportConstants.COMPLETED)).ToList();
                    if (closedCases.IsCollectionValid())
                    {
                        return closedCases.Select(cs => new CaseAgingReportDetails
                        {
                            CaseNumber = cs.Assignment.CaseNo,
                            CaseLevel = GetRoleName(usrRoles.Where(x => x.UserID == cs.UserId).Select(x => x.RoleName).ToList()),
                            Opened = DateTimetoString(cs.OpenDate),
                            CurrentReviewer = lstOfUsers.Where(x => x.UserId == cs.UserId).Select(u => u.UserName).FirstOrDefault(),
                            CaseStatus = cs.State,
                            DaysAssigned = GetDaysAssigned(cs.CreatedDate, cs.OpenDate),
                            DaysOpen = GetDaysOpen(cs.WorkflowStatus, cs.OpenDate, cs.ModifiedDate, cs.ParkDate, cs.UnParkDate),
                            ClassifiedByDays = AssignClassification(tempDaysAssigned),
                            WorkflowStatus = cs.WorkflowStatus
                        }).ToList<CaseAgingReportDetails>();
                    }

                }
                return null;
            }
            catch (Exception) { return null; }
            finally { tempDaysAssigned = null; ClearCollection(); }
        }

        public List<CaseAgingReportDetails> GetCasesByRole(ReportDTO reportDTO)
        {
            try
            {
                if (!casesByModule.IsCollectionValid())
                    casesByModule = new List<AssignedTo>(ModuleSpecificCases());
                if (!lstOfUsers.IsCollectionValid())
                    lstOfUsers = new List<User>(UsersSpecificToCases());
                if (!usrRoles.IsCollectionValid())
                    usrRoles = new List<UserRoleDefinition>(RolesSpecificToUsers());

                if (casesByModule.IsCollectionValid() && lstOfUsers.IsCollectionValid() && usrRoles.IsCollectionValid())
                {
                    var casesByRole = lstOfUsers.Where(u => usrRoles.Where(r => r.RoleName.Equals(reportDTO.SelectedRoleName)).Select(r => r.UserID).ToList().Contains(u.UserId))
                      .Join(casesByModule, u => u.UserId, cs => cs.UserId, (u, cs) => new { u, cs });
                    if (casesByRole.IsCollectionValid())
                    {
                        return casesByRole.Select(r => new CaseAgingReportDetails
                        {
                            CaseNumber = r.cs.Assignment.CaseNo,
                            CaseLevel = GetRoleName(usrRoles.Where(x => x.UserID == r.cs.UserId).Select(x => x.RoleName).ToList()),
                            Opened = DateTimetoString(r.cs.OpenDate),
                            CurrentReviewer = r.u.UserName,
                            CaseStatus = r.cs.State,
                            DaysAssigned = GetDaysAssigned(r.cs.CreatedDate, r.cs.OpenDate),
                            DaysOpen = GetDaysOpen(r.cs.WorkflowStatus, r.cs.OpenDate, r.cs.ModifiedDate, r.cs.ParkDate, r.cs.UnParkDate),
                            ClassifiedByDays = AssignClassification(tempDaysAssigned),
                            WorkflowStatus = r.cs.WorkflowStatus
                        }).ToList<CaseAgingReportDetails>();
                    }

                }
            }
            catch (Exception)
            {
                return null;
            }
            finally { tempDaysAssigned = null; ClearCollection(); }
            return null;
        }

        public List<RFISummaryReporting> GetRFISummary(ReportDTO reportDTO)
        {
            try
            {
                if (!casesByModule.IsCollectionValid())
                    casesByModule = new List<AssignedTo>(ModuleSpecificCases());
             
                if (casesByModule.IsCollectionValid())
                {
                    using (var Rpt = new ReportUnitOfWork())
                    {
                        var getRFISummary = Rpt.GetRepository<RequestForInformation>();
                        var cases = casesByModule.Select(x => x.Assignment.CaseNo).Distinct();
                        if (getRFISummary.Items.IsCollectionValid() && cases.IsCollectionValid())
                        {
                            var RFISummary = getRFISummary.Items.Where(x => cases.Contains(x.CaseNumber)).ToList();
                            if (RFISummary.IsCollectionValid())
                            {
                                return RFISummary.Select(x => new RFISummaryReporting
                                {
                                    Id = x.Id,
                                    CaseNumber = x.CaseNumber,
                                    SentTo = x.SentTo,
                                    SentFrom = x.SentFrom,
                                    Subject = x.Subject,
                                    SentDate = DateTimetoString(x.SentDate),
                                    Body = x.Body

                                }).ToList<RFISummaryReporting>();
                            }

                        }
                    }

                }
            }
            catch (Exception)
            {
                return null;
            }
            finally { ClearCollection(); }
            return null;
        }

        public List<RFIDetailsReporting> GetRFIDetails(ReportDTO reportDTO)
        {
            try
            {
                if (!casesByModule.IsCollectionValid())
                    casesByModule = new List<AssignedTo>(ModuleSpecificCases());
              
                if (casesByModule.IsCollectionValid())
                {
                    using (var Rpt = new ReportUnitOfWork())
                    {
                        var getRFIDetails = Rpt.GetRepository<RFIResponses>();
                        var cases = casesByModule.Select(x => x.Assignment.CaseNo);
                        if (getRFIDetails.Items.IsCollectionValid() && cases.IsCollectionValid())
                        {
                            var RFIDetails = getRFIDetails.Items.Where(x => cases.Contains(x.CaseNumber)).ToList();
                            if (RFIDetails.IsCollectionValid())
                            {
                                return RFIDetails.Select(x => new RFIDetailsReporting
                                {
                                    Id = x.Id,
                                    CaseNumber = x.CaseNumber,
                                    SentTo = x.SentTo,
                                    SentFrom = x.SentFrom,
                                    Subject = x.Subject,
                                    SentDate = DateTimetoString(x.SentDate),
                                    CreadtedBy = x.CreatedBy,
                                    CreadtedDate = DateTimetoString(x.CreatedDate)
                                }).ToList<RFIDetailsReporting>();
                            }

                        }
                    }

                }
            }
            catch (Exception)
            {
                return null;
            }
            finally { ClearCollection(); }
            return null;
        }

        public List<StateMetadata> GetStateTypes(ReportDTO reportDTO)
        {
            using (var RptUOW = new ReportUnitOfWork())
            {
                return RptUOW.GetRepository<State>().Items.Include(x => x.StateMetadata).Where(x => x.ModuleTypeId == reportDTO.ModuleId)
                   .Select(s => s.StateMetadata).Distinct().ToList<StateMetadata>();
            }

        }

        public List<CaseAgingReportDetails> ComplianceReporting(ReportDTO reportDTO)
        {
            try
            {
                if (!casesByModule.IsCollectionValid())
                    casesByModule = new List<AssignedTo>(ModuleSpecificCases());
                if (!lstOfUsers.IsCollectionValid())
                    lstOfUsers = new List<User>(UsersSpecificToCases());
                if (!usrRoles.IsCollectionValid())
                    usrRoles = new List<UserRoleDefinition>(RolesSpecificToUsers());

                using (var RptUOW = new ReportUnitOfWork())
                {
                    var stateRepo = RptUOW.GetRepository<State>().Items.Include(s => s.StateMetadata).Where(s => s.StateMetadata.StateType.Equals(reportDTO.SelectedCompliance))
                       .Select(x => x.StateName).Distinct().ToList();

                    if (casesByModule.IsCollectionValid() && lstOfUsers.IsCollectionValid() && usrRoles.IsCollectionValid() && stateRepo.IsCollectionValid())
                    {
                        var casesByState = casesByModule.Where(c => stateRepo.Contains(c.State)).ToList();
                        if (casesByState.IsCollectionValid())
                        {
                            return casesByState.Select(cs => new CaseAgingReportDetails
                            {
                                CaseNumber = cs.Assignment.CaseNo,
                                CaseLevel = GetRoleName(usrRoles.Where(x => x.UserID == cs.UserId).Select(x => x.RoleName).ToList()),
                                Opened = DateTimetoString(cs.OpenDate),
                                CurrentReviewer = lstOfUsers.FirstOrDefault(x => x.UserId == cs.UserId).UserName,
                                CaseStatus = cs.State,
                                DaysAssigned = GetDaysAssigned(cs.CreatedDate, cs.OpenDate),
                                DaysOpen = GetDaysOpen(cs.WorkflowStatus, cs.OpenDate, cs.ModifiedDate, cs.ParkDate, cs.UnParkDate),
                                ClassifiedByDays = AssignClassification(tempDaysAssigned),
                                WorkflowStatus = cs.WorkflowStatus
                            }).ToList<CaseAgingReportDetails>();
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally { tempDaysAssigned = null; ClearCollection(); }
            return null;
        }
        #endregion


    }
}
