﻿
using KAMP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.REPORT.BLL
{
   public class ReportBLL<T>
    {
       private IReports<T> ReportInterface;
       T ReportInstance;

       public ReportBLL(T reportInstance)
       {
           ReportInstance = reportInstance;
       }

       public object Execute(IReports<T> ReportType,Func<T,object> ReportKPI)
       {
           ReportInterface = ReportType;
           return ReportInterface.Execute(ReportInstance,ReportKPI);

       }
    }
}
