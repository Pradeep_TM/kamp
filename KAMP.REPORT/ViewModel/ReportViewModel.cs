﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.WF;
using KAMP.REPORT.BLL;
using KAMP.REPORT.Common;
using KAMP.REPORT.Helper;
using KAMP.REPORT.Models;
using KAMP.REPORT.UserControls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace KAMP.REPORT.ViewModel
{
    public class ReportViewModel : INotifyPropertyChanged
    {
        public List<User>  _usersByRole;
        public bool IsBackgroundWorkCompleted = false;

        public ReportViewModel()
        {
            PropertyChanged += RaiseOnPropertyChanged;
            IsAgingActionSelected = false;
            BindUserControl = null;
            IsUpdate = false;
            IsComplianceSelected = false;
            IsCaseByRoleSelected = false;
            IsDefaultComboBoxValueShown = false;
            IsDefaultComplianceShown = false;
            IsDefaultRoleValueShown = false;
            IsDefaultReportActionValueShown = true;
        }

        ~ReportViewModel()
        {
            PropertyChanged -= RaiseOnPropertyChanged;
            if(_usersByRole.IsCollectionValid())
            {
                _usersByRole.Clear();
                _usersByRole = null;
            }
        }

        public IEnumerable<string> ComplianceDetails { get; set; }

        public IEnumerable<string> ReportKPI { get; set; }

        private long _userID;

        public long UserId
        {
            get { return this._userID; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userID, (x) => x.UserId); }
        }


        private string _userName = null;

        public string UserName
        {
            get { return this._userName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userName, (x) => x.UserName); }
        }


        private int _moduleID;

        public int ModuleId
        {
            get { return this._moduleID; }
            set { PropertyChanged.HandleValueChange(this, value, ref _moduleID, (x) => x.ModuleId); }
        }


        private string _moduleName = null;

        public string ModuleName
        {
            get { return this._moduleName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _moduleName, (x) => x.ModuleName); }
        }


        private dynamic _instance = null;

        public dynamic Instance
        {
            get { return _instance; }
            set { _instance = value; }
        }


        private ReportDTO _reportDTO = null;

        public ReportDTO ReportDTO
        {
            get { return this._reportDTO; }
            set
            {

                PropertyChanged.HandleValueChange(this, value, ref _reportDTO, (x) => x.ReportDTO);
            }
        }


        private object _bindUserControl = null;

        public object BindUserControl
        {
            get { return this._bindUserControl; }
            set { PropertyChanged.HandleValueChange(this, value, ref _bindUserControl, (x) => x.BindUserControl); }
        }

        private bool _isDefaultComplianceShown;

        public bool IsDefaultComplianceShown
        {
            get { return _isDefaultComplianceShown; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isDefaultComplianceShown, (x) => x.IsDefaultComplianceShown); }
        }


        private bool _isDefaultComboBoxValueShown;

        public bool IsDefaultComboBoxValueShown
        {
            get { return _isDefaultComboBoxValueShown; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isDefaultComboBoxValueShown, (x) => x.IsDefaultComboBoxValueShown); }
        }

        private bool _isDefaultReportActionValueShown;

        public bool IsDefaultReportActionValueShown
        {
            get { return _isDefaultReportActionValueShown; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isDefaultReportActionValueShown, (x) => x.IsDefaultReportActionValueShown); }
        }


        private bool _isAgingActionSelected;
        public bool IsAgingActionSelected
        {
            get { return _isAgingActionSelected; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isAgingActionSelected, (x) => x.IsAgingActionSelected); }
        }

        private bool _isUpdate;

        public bool IsUpdate
        {
            get { return _isUpdate; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isUpdate, (x) => x.IsUpdate); }
        }


        private bool _isComplianceSelected;

        public bool IsComplianceSelected
        {
            get { return _isComplianceSelected; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isComplianceSelected, (x) => x.IsComplianceSelected); }
        }


        private string _selectedAction = null;

        public string SelectedAction
        {
            get { return this._selectedAction; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedAction, (x) => x.SelectedAction);
                IsDefaultReportActionValueShown = SelectedAction == null;
            }
        }


        private StateMetadata _selectedComplianceValue = null;

        public StateMetadata SelectedComplianceValue
        {
            get { return this._selectedComplianceValue; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedComplianceValue, (x) => x.SelectedComplianceValue);
                IsDefaultComplianceShown = SelectedComplianceValue == null;
                IsUpdate = SelectedComplianceValue!=null;
            }
        }


        private User _selectedUserValue = null;

        public User SelectedUserValue
        {
            get { return this._selectedUserValue; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedUserValue, (x) => x.SelectedUserValue);
                IsDefaultComboBoxValueShown = SelectedUserValue == null;
                IsUpdate = SelectedUserValue != null;
            }
        }


        private List<User> _userNames;

        public List<User> UserNames
        {
            get { return this._userNames; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userNames, (x) => x.UserNames); }
        }

        private List<string> _roles = null;

        public List<string> Roles
        {
            get { return this._roles; }
            set { PropertyChanged.HandleValueChange(this, value, ref _roles, (x) => x.Roles); }
        }

        private bool _isCaseByRoleSelected;

        public bool IsCaseByRoleSelected
        {
            get { return _isCaseByRoleSelected; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isCaseByRoleSelected, (x) => x.IsCaseByRoleSelected); }
        }

        private bool _isDefaultRoleValueShown;

        public bool IsDefaultRoleValueShown
        {
            get { return _isDefaultRoleValueShown; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isDefaultRoleValueShown, (x) => x.IsDefaultRoleValueShown); }
        }

        private string _selectedRole = null;

        public string SelectedRole
        {
            get { return this._selectedRole; }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _selectedRole, (x) => x.SelectedRole);
                if (SelectedRole != null)
                    BindReportBySelectedRole(SelectedRole);

                IsAgingActionSelected = SelectedRole != null;
                IsDefaultComboBoxValueShown = SelectedRole != null;
                IsDefaultRoleValueShown = SelectedRole == null;
            }
        }

        private List<StateMetadata> _stateMetadata = null;
        public List<StateMetadata> StateMetaData
        {
            get { return _stateMetadata; }
            set { PropertyChanged.HandleValueChange(this, value, ref _stateMetadata, (x) => x.StateMetaData); }
        }

        private ICommand _submit;

        public ICommand Submit
        {
            get
            {
                if (_submit == null)
                {
                    _submit = new DelegateCommand(() =>
                    {
                        if (string.IsNullOrEmpty(_selectedAction))
                            return;
                        ReportDTO.SelectedKPI = _selectedAction;
                        ReportContext.ReportInfo = ReportDTO;

                        Mouse.OverrideCursor = Cursors.Wait;
                        try
                        {
                            if (IsBackgroundWorkCompleted)
                            {
                                switch (_selectedAction)
                                {
                                    case ReportActions.CaseAging:
                                        BindUserControl = new ReportHelper().CaseAging(Instance, ReportDTO);
                                        break;
                                    case ReportActions.CaseAgingByRole:
                                        ReportDTO.SelectedUserName = _selectedUserValue.UserName.ToString();
                                        BindUserControl = new ReportHelper().CaseAgingByUser(Instance, ReportDTO);
                                        break;

                                    case ReportActions.ClosedCases:
                                        BindUserControl = new ReportHelper().ClosedCases(Instance, ReportDTO);
                                        break;
                                    case ReportActions.RFISummary:
                                        BindUserControl = new ReportHelper().RFISummaryReporting(Instance, ReportDTO);
                                        break;
                                    case ReportActions.RFIDetails:
                                        BindUserControl = new ReportHelper().RFIDetailsReporting(Instance, ReportDTO);
                                        break;
                                    case ReportActions.CaseAgingByCompilance:
                                        ReportDTO.SelectedKPI = ReportActions.CaseAgingByCompilance + " " + _selectedComplianceValue.StateType;
                                        ReportDTO.SelectedCompliance = _selectedComplianceValue.StateType;
                                        BindUserControl = new ReportHelper().ComplianceReporting(Instance, ReportDTO);
                                        break;

                                    default:
                                        ReportContext.ReportInfo = null;
                                        ReportContext.DataSource = null;
                                        BindUserControl = new ReportHelper().GetUserControlInstance();
                                        break;
                                }
                            }
                            else
                            {
                                BindUserControl = new ReportHelper().GetUserControlInstance();
                                var control = BindUserControl as ReportViewerControl;
                                control.rvcLblContent.Text = string.Empty;
                                control.rvcLblContent.Text = "Loading Records.Please Wait...";
                            }
                        }
                        catch (Exception ex)
                        {
                            ReportContext.ReportInfo = null;
                            ReportContext.DataSource = null;
                            BindUserControl = new ReportHelper().GetUserControlInstance();
                        }
                        finally
                        {
                            Application.Current.Dispatcher.Invoke(() =>{Mouse.OverrideCursor = null;});
                            
                            if (SelectedRole != null)
                            {
                                IsDefaultComboBoxValueShown = false;
                              
                                _isDefaultRoleValueShown = false;
                            }
                            IsUpdate = false;
                        }
 
                    });
                }
                return _submit;
            }

        }

        private void BindReportBySelectedRole(string role)
        {
            ReportDTO.SelectedKPI = ReportActions.CaseAgingByRole+" "+role;
            ReportDTO.SelectedRoleName = role;
            ReportContext.ReportInfo = ReportDTO;
            BindUserControl = new ReportHelper().CaseAgingByRole(Instance, ReportDTO);
            UserNames = new ReportHelper().RenderReport(Instance, Instance, (Func<ProductivityReports, object>)(t => t.GetUsersByDefinedRole(role)));
          
        }
       

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaiseOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
            if (e.PropertyName == this.GetPropertyName(x => x.SelectedAction))
            {
                if (string.IsNullOrEmpty(_selectedAction))
                    return;
                switch (_selectedAction)
                {
                    case ReportActions.CaseAging:
                        IsUpdate = true;
                        IsAgingActionSelected = false;
                        IsComplianceSelected = false;
                        IsCaseByRoleSelected = false;
                        IsDefaultComplianceShown = false;
                        IsDefaultRoleValueShown = false;
                        IsDefaultComboBoxValueShown = false;
                        break;
                    case ReportActions.CaseAgingByRole:
                        IsUpdate = false;
                        IsDefaultComboBoxValueShown = false;
                        IsCaseByRoleSelected = true;
                        IsDefaultRoleValueShown = true;
                        IsComplianceSelected = false;
                        IsAgingActionSelected = false;
                        IsDefaultComplianceShown = false;
                        if (SelectedUserValue != null)
                            SelectedUserValue = null;
                        if (SelectedRole != null)
                            SelectedRole = null;
                        break;

                   case ReportActions.CaseAgingByCompilance:
                        StateMetaData = new ReportHelper().RenderReport(Instance, Instance, (Func<ProductivityReports, object>)(t => t.GetStateTypes(ReportDTO)));
                        IsUpdate = false;
                        IsAgingActionSelected = false;
                        IsComplianceSelected = true;
                        IsCaseByRoleSelected = false;
                        IsDefaultComplianceShown = true;
                        IsDefaultComboBoxValueShown = false;
                        IsDefaultRoleValueShown = false;
                        break;

                    case ReportActions.ClosedCases:
                        IsUpdate = true;
                        IsCaseByRoleSelected = false;
                        IsAgingActionSelected = false;
                        IsComplianceSelected = false;
                        IsDefaultComplianceShown = false;
                        IsDefaultComboBoxValueShown = false;
                        IsDefaultRoleValueShown = false;
                        break;

                    default:

                        IsDefaultRoleValueShown = false;
                        IsAgingActionSelected = false;
                        _selectedUserValue = null;
                        SelectedUserValue = null;
                        IsDefaultComboBoxValueShown = false;
                        IsCaseByRoleSelected = false;
                        IsComplianceSelected = false;
                        IsDefaultComplianceShown = false;
                        IsUpdate = true;
                        break;
                }
            }
        }

        private void OnpropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }

        }

    }
}
