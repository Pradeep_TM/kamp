﻿using KAMP.Core.FrameworkComponents;
using KAMP.Core.FrameworkComponents.Enums;
using KAMP.REPORT.BLL;
using KAMP.REPORT.Common;
using KAMP.REPORT.Helper;
using KAMP.REPORT.Models;
using KAMP.REPORT.UserControls;
using KAMP.REPORT.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.REPORT.Views
{
    /// <summary>
    /// Interaction logic for ReportMainControl.xaml
    /// </summary>
    public partial class ReportMainControl : UserControl,IPageControl,IMVPageControl
    {
        public ReportViewModel ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = value; }
        }
        private ReportViewModel _viewModel = new ReportViewModel();
        private BackgroundWorker _bwForQueryingDB = null;
        private ReportViewerControl control = null;

        public ReportMainControl()
        {
            AssignResources();
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(OnWindowLoaded);
            this.Unloaded += new RoutedEventHandler(OnWindowUnload);

        }

        #region Private Methods

        private void AssignResources()
        {
            var appContext = Application.Current.Properties["Context"] as Context;
            var style = new ResourceDictionary();
            var scroll = new ResourceDictionary();
            ModuleNames moduleNames = (ModuleNames)Enum.Parse(typeof(ModuleNames), appContext.Module.Name, true);

            switch (moduleNames)
            {
                case ModuleNames.TLB:
                    style.Source = new Uri(ReportConstants.TLB_STYLESHEET, UriKind.RelativeOrAbsolute);
                    scroll.Source = new Uri(ReportConstants.TLB_SCROLLDDL, UriKind.RelativeOrAbsolute);
                    this.Resources.MergedDictionaries.Add(style);
                    this.Resources.MergedDictionaries.Add(scroll);
                    break;
                case ModuleNames.MV:
                    style.Source = new Uri(ReportConstants.MV_STYLESHEET, UriKind.RelativeOrAbsolute);
                    scroll.Source = new Uri(ReportConstants.MV_SCROLLNAVIGATION, UriKind.RelativeOrAbsolute);
                    this.Resources.MergedDictionaries.Add(style);
                    this.Resources.MergedDictionaries.Add(scroll);
                    break;
                case ModuleNames.KYC:
                    style.Source = new Uri(ReportConstants.KYC_STYLESHEET, UriKind.RelativeOrAbsolute);
                    scroll.Source = new Uri(ReportConstants.KYC_SCROLLNAVIGATION, UriKind.RelativeOrAbsolute);
                    this.Resources.MergedDictionaries.Add(style);
                    this.Resources.MergedDictionaries.Add(scroll);
                    break;
                default:
                    break;
            }
        }

        private void BindViewModel()
        {
            if (_bwForQueryingDB == null)
            {
                if (ViewModel.IsNull())
                {
                    _viewModel = new ReportViewModel();
                }

                ViewModel.ReportKPI = new ReportHelper().GetReportActions();
                ViewModel.BindUserControl = control = new ReportHelper().GetUserControlInstance();
                control.rvcLblContent.Text = string.Empty;
                control.rvcLblContent.Text = "Loading Records.Please Wait...";
                this.DataContext = ViewModel;
                _bwForQueryingDB = new BackgroundWorker();
                _bwForQueryingDB.DoWork += new DoWorkEventHandler(BackgroundWorkerForQueryingDB);
                _bwForQueryingDB.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_Completed);
                _bwForQueryingDB.RunWorkerAsync();
            }

        }

        private void BackgroundWorkerForQueryingDB(object sender, DoWorkEventArgs e)
        {
            ReportHelper _helper = null;
            try
            {
                var appcontext = Application.Current.Properties["Context"] as Context;
                ViewModel.ModuleId = appcontext.Module.Id;
                ViewModel.ModuleName = appcontext.Module.Name;
                ViewModel.UserId = appcontext.User.Id;
                ViewModel.UserName = appcontext.User.UserName;
                _helper = new ReportHelper();

                //Binding of EnumCompliance type to IEnumerable for Compliance ComboBox
                ViewModel.ComplianceDetails = _helper.GetComplianceTypes();
                //System.Threading.Thread.Sleep(3000);
                ViewModel.Instance = _helper.GetReportInstance(ViewModel.ModuleId);
                ViewModel.ReportDTO = _helper.GetReportDTO(ViewModel);
                ViewModel.Roles = _helper.RenderReport(ViewModel.Instance, ViewModel.Instance, (Func<ProductivityReports, object>)(t => t.GetExistingRoles()));
                ViewModel.IsBackgroundWorkCompleted = true;
                e.Result = ViewModel;

            }
            catch (Exception ex)
            {
                ViewModel = null;
                e.Result = ViewModel;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (control.IsNotNull())
                    {
                        control.rvcLblContent.Text = string.Empty;
                        control.rvcLblContent.Text = "An Error Occurred while Loading Report Viewer";
                        UserActions.IsEnabled = false;
                    }
                });
            }
            finally { _helper = null; }
        }

        private void worker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result.IsNotNull())
            {
                var model = (ReportViewModel)e.Result;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (control.IsNotNull())
                    {
                        control.rvcLblContent.Text = string.Empty;
                        control.rvcLblContent.Text = "Please Select Report Category.";
                        UserActions.IsEnabled = true;
                    }
                });
                this.DataContext = model;
            }
        }

        private void OnWindowUnload(object sender, RoutedEventArgs e)
        {
            if(ReportContext.CacheContainer.IsNotNull())
                ReportContext.CacheContainer.ClearAll();

            ReportContext.ReportInfo = null;
            ReportContext.DataSource = null;
            ViewModel.Instance = null;
            ViewModel = null;
            this.DataContext = null;
            control = null;
            if (_bwForQueryingDB.IsNotNull())
            {
                _bwForQueryingDB.Dispose();
                _bwForQueryingDB = null;
            }
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            ReportContext.ReportInfo = null;
            ReportContext.DataSource = null;
            BindViewModel();

            /// <summary>
            /// Get and Sets value for elements
            /// </summary>
        }
                
        #endregion

        #region Public Methods

        public dynamic InitializeViewModel()
        {
            BindViewModel();
            while (true)
            {
                if (ViewModel.IsNotNull())
                {
                    if (ViewModel.IsBackgroundWorkCompleted)
                        return _viewModel;
                }
            }

        }

        public string PageName
        {
            get { return ReportConstants.REPORT_MAINPAGE_NAME; }
        }

        void IMVPageControl.InitializeViewModel()
        {

        }

        #endregion
        
    }
}
