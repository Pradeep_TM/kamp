﻿using KAMP.Core.Interfaces;
using KAMP.Core.Repository;
using KAMP.Core.Repository.RFI;
using KAMP.Core.Repository.UnitOfWork;
using KAMP.REPORT.BLL;
using KAMP.REPORT.Common;
using KAMP.REPORT.Models;
using KAMP.REPORT.UserControls;
using KAMP.REPORT.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KAMP.REPORT.Helper
{
    public class ReportHelper 
    {
      BackgroundWorker _bwForProcessStart = null;

      #region Private Methods

      private void BackgroundWorkerForProcessStart(int id)
      {
          var binaryData = GetRFI_BinaryData(id);
          var path = Environment.CurrentDirectory + @"\RFITemp";

          if (!System.IO.Directory.Exists(path))
              System.IO.Directory.CreateDirectory(path);

          var filePath = path + @"\Message.msg";

          if (File.Exists(filePath))
              File.Delete(filePath);
          if (binaryData != null)
              File.WriteAllBytes(filePath, binaryData);

          Process.Start(filePath);
          binaryData = null;

      }

      private bool GrantAccess(string fullPath)
      {
          DirectoryInfo dInfo = new DirectoryInfo(fullPath);
          DirectorySecurity dSecurity = dInfo.GetAccessControl();
          dSecurity.AddAccessRule(new FileSystemAccessRule("everyone", FileSystemRights.FullControl,
                                                           InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                                                           PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
          dInfo.SetAccessControl(dSecurity);
          return true;
      }

      private void worker_Completed(object sender, RunWorkerCompletedEventArgs e)
      {
          _bwForProcessStart.Dispose();
      }

      private byte[] GetRFI_BinaryData(int id)
      {
          using (var Rpt = new ReportUnitOfWork())
          {
              var getRFIAttachment = Rpt.GetRepository<RFIResponses>();
              return getRFIAttachment.Items.Count() > 0 ?
                  getRFIAttachment.Items.Where(rfi => rfi.Id == id).Select(b => b.ResponseUploaded).FirstOrDefault() : null;
          }

      }

      private string GetExecutionPath()
      {
          return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
      }

      #endregion

      #region Public Methods

      public List<string> GetReportActions()
      {
          return new List<string>() { ReportActions.CaseAging, ReportActions.CaseAgingByRole,ReportActions.CaseAgingByCompilance,
                         ReportActions.ClosedCases,ReportActions.RFISummary,ReportActions.RFIDetails};

      }

      public string[] GetComplianceTypes()
      {
          return Enum.GetNames(typeof(CompilanceType));
      }

      public dynamic GetReportInstance(int moduleId)
      {
          try
          {
              return new ProductivityReports(moduleId);
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public dynamic GetUserControlInstance()
      {
          return new ReportViewerControl();
      }
        
      public ReportDTO GetReportDTO(ReportViewModel ViewModel)
      {
          return new ReportDTO()
          {
              ReportName = ReportConstants.DATASET_NAME,
              ReportRDLC = string.Empty,
              CaseNo = string.Empty,
              UserId = ViewModel.UserId,
              UserName = ViewModel.UserName,
              ModuleId = ViewModel.ModuleId,
              ModuleName = ViewModel.ModuleName
          };
      }

      public dynamic RenderReport<T>(IReports<T> ReportType, T ReportInstance, Func<T, object> ReportKPI)
      {
          ReportBLL<T> reportBLL = new ReportBLL<T>(ReportInstance);
          return reportBLL.Execute(ReportType, ReportKPI);

      }

      public dynamic CaseAging(ProductivityReports _rptInstance, ReportDTO _dto)
      {
          try
          {
              var _usrCtrl = GetUserControlInstance();
              ReportContext.ReportInfo.ReportRDLC = string.Format("{0}" + ReportConstants.PATH + ReportConstants.CASE_AGING_REPORT, GetExecutionPath());
              _usrCtrl.RenderReport(_rptInstance, _rptInstance, (Func<ProductivityReports, object>)(x => x.GetCaseAging(_dto)));
              return _usrCtrl;

          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public dynamic CaseAgingByUser(ProductivityReports _rptInstance, ReportDTO _dto)
      {
          try
          {
              var _usrCtrl = GetUserControlInstance();
              ReportContext.ReportInfo.ReportRDLC = string.Format("{0}" + ReportConstants.PATH + ReportConstants.CASE_AGING_BY_USER, GetExecutionPath());
              _usrCtrl.RenderReport(_rptInstance, _rptInstance, (Func<ProductivityReports, object>)(t => t.GetCaseAgingByUser(_dto)));
              return _usrCtrl;
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public dynamic ClosedCases(ProductivityReports _rptInstance, ReportDTO _dto)
      {
          try
          {
              var _usrCtrl = GetUserControlInstance();
              ReportContext.ReportInfo.ReportRDLC = string.Format("{0}" + ReportConstants.PATH + ReportConstants.CLOSED_CASES, GetExecutionPath());
              _usrCtrl.RenderReport(_rptInstance, _rptInstance, (Func<ProductivityReports, object>)(t => t.GetClosedCases(_dto)));
              return _usrCtrl;
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public dynamic CaseAgingByRole(ProductivityReports _rptInstance, ReportDTO _dto)
      {
          try
          {
              var _usrCtrl = GetUserControlInstance();
              ReportContext.ReportInfo.ReportRDLC = string.Format("{0}" + ReportConstants.PATH + ReportConstants.CASE_AGING_BY_ROLE, GetExecutionPath());
              _usrCtrl.RenderReport(_rptInstance, _rptInstance, (Func<ProductivityReports, object>)(t => t.GetCasesByRole(_dto)));
              return _usrCtrl;
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public dynamic RFISummaryReporting(ProductivityReports _rptInstance, ReportDTO _dto)
      {
          try
          {
              var _usrCtrl = GetUserControlInstance();
              ReportContext.ReportInfo.ReportRDLC = string.Format("{0}" + ReportConstants.PATH + ReportConstants.RFISUMMARY, GetExecutionPath());
              _usrCtrl.RenderReport(_rptInstance, _rptInstance, (Func<ProductivityReports, object>)(x => x.GetRFISummary(_dto)));
              return _usrCtrl;
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public dynamic RFIDetailsReporting(ProductivityReports _rptInstance, ReportDTO _dto)
      {
          try
          {
              var _usrCtrl = GetUserControlInstance();
              ReportContext.ReportInfo.ReportRDLC = string.Format("{0}" + ReportConstants.PATH + ReportConstants.RFIDETAILS, GetExecutionPath());
              _usrCtrl.RenderReport(_rptInstance, _rptInstance, (Func<ProductivityReports, object>)(x => x.GetRFIDetails(_dto)));
              return _usrCtrl;
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public dynamic ComplianceReporting(ProductivityReports _rptInstance, ReportDTO _dto)
      {
          try
          {
              var _usrCtrl = GetUserControlInstance();
              ReportContext.ReportInfo.ReportRDLC = string.Format("{0}" + ReportConstants.PATH + ReportConstants.CASE_AGING_REPORT, GetExecutionPath());
              _usrCtrl.RenderReport(_rptInstance, _rptInstance, (Func<ProductivityReports, object>)(x => x.ComplianceReporting(_dto)));
              return _usrCtrl;
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message.ToString());
          }
      }

      public void GetByteAndStartOutlook(int id)
      {
          _bwForProcessStart = new BackgroundWorker();
          _bwForProcessStart.DoWork += (obj, e) => BackgroundWorkerForProcessStart(id);
          _bwForProcessStart.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_Completed);
          _bwForProcessStart.RunWorkerAsync();
      }
      #endregion

    }
}
