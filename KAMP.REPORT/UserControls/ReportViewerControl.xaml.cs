﻿using KAMP.Core.Interfaces;
using KAMP.REPORT.BLL;
using KAMP.REPORT.Common;
using KAMP.REPORT.Helper;
using KAMP.REPORT.Models;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.REPORT.UserControls
{
    /// <summary>
    /// Interaction logic for ReportViewerControl.xaml
    /// </summary>
    public partial class ReportViewerControl : UserControl
    {
        private List<ReportParameter> setReportParam = null;
        private string messageContent = string.Empty;

        public ReportViewerControl()
        {
            InitializeComponent();
            this.Unloaded += new RoutedEventHandler(OnWindowUnloaded);
            _reportViewer.Drillthrough += new DrillthroughEventHandler(DrillThroughEventHandler);

        }

        #region Private Methods

        private void LocalReportProcessing(LocalReport localReport)
        {
            localReport.DataSources.Clear();
            HidePdfFormat(localReport, "PDF");
            localReport.DataSources.Add(new ReportDataSource(ReportContext.ReportInfo.ReportName, ReportContext.DataSource));
            localReport.ReportPath = ReportContext.ReportInfo.ReportRDLC;
            localReport.SetBasePermissionsForSandboxAppDomain(new System.Security.PermissionSet(System.Security.Permissions.PermissionState.Unrestricted));
            localReport.SetParameters(SetReportParamerters());

        }

        private void ReportViewerProcessing(ReportViewer reportViewer)
        {
            reportViewer.ShowBackButton = true;
            reportViewer.ShowContextMenu = true;
            reportViewer.ShowExportButton = true;
            reportViewer.ShowFindControls = true;
            reportViewer.ShowPrintButton = true;
            reportViewer.ShowProgress = true;
            reportViewer.ShowToolBar = true;
            reportViewer.ShowZoomControl = true;
            reportViewer.ShowStopButton = true;
            reportViewer.UseWaitCursor = false;
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.ShowParameterPrompts = false;
            reportViewer.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
            reportViewer.RefreshReport();

        }

        private IEnumerable<ReportParameter> SetReportParamerters()
        {
            if (setReportParam == null)
            {
                setReportParam = new List<ReportParameter>();
                setReportParam.Add(new ReportParameter(ReportConstants.MODULE_HEADER, ReportContext.ReportInfo.ModuleName));
                setReportParam.Add(new ReportParameter(ReportConstants.REPORT_KPI, ReportContext.ReportInfo.SelectedKPI));
                return setReportParam.AsEnumerable();
            }
            else
            {
                bool isModuleChanged = setReportParam.TrueForAll(p => p.Name.Equals(ReportConstants.MODULE_HEADER) && !(p.Values.Equals(ReportContext.ReportInfo.ModuleName)));
                bool isKPIChanged = setReportParam.TrueForAll(p => p.Name.Equals(ReportConstants.REPORT_KPI) && !(p.Values.Equals(ReportContext.ReportInfo.SelectedKPI)));

                if (isModuleChanged || isKPIChanged)
                {
                    setReportParam.Clear();
                    setReportParam = null;
                    setReportParam = new List<ReportParameter>();
                    setReportParam.Add(new ReportParameter(ReportConstants.REPORT_KPI, ReportContext.ReportInfo.SelectedKPI));
                    setReportParam.Add(new ReportParameter(ReportConstants.MODULE_HEADER, ReportContext.ReportInfo.ModuleName));
                }

                return setReportParam.AsEnumerable();
            }

        }

        private void DrillThroughEventHandler(object sender, DrillthroughEventArgs e)
        {
            LocalReport report = null;
            IList<ReportParameter> listofparam = null;
            try
            {
                report = (LocalReport)e.Report;
                listofparam = report.OriginalParametersToDrillthrough;

                if (listofparam.Count > 0)
                {
                    if (listofparam.Any(p => p.Name.Contains(ReportConstants.MESSAGE_PARAM)))
                    {
                        messageContent = Convert.ToString(listofparam.FirstOrDefault(x => x.Name.Equals(ReportConstants.MESSAGE_PARAM)).Values[0]).Trim();
                        WBControl.NavigateToString(messageContent);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            ReportControl.Visibility = Visibility.Collapsed;
                            MailDesc.Visibility = Visibility.Visible;
                        });

                    }

                    if (listofparam.Any(p => p.Name.Contains(ReportConstants.RESPONSEID_PARAM)))
                    {
                        new ReportHelper().GetByteAndStartOutlook(Convert.ToInt32(listofparam.FirstOrDefault(x => x.Name.Equals(ReportConstants.RESPONSEID_PARAM)).Values[0]));
                    }
                }
                HidePdfFormat(report, "PDF");
                report.DataSources.Add(new ReportDataSource(ReportContext.ReportInfo.ReportName, ReportContext.DataSource));
                report.SetParameters(SetReportParamerters());


            }
            catch (Exception)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    ReportControl.Visibility = Visibility.Collapsed;
                    MailDesc.Visibility = Visibility.Collapsed;
                    lblNoRecordFound.Visibility = Visibility.Visible;

                });
            }

        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReportContext.ReportInfo != null && ReportContext.DataSource != null)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        lblNoRecordFound.Visibility = Visibility.Collapsed;
                        MailDesc.Visibility = Visibility.Collapsed;
                    });

                    LocalReportProcessing(_reportViewer.LocalReport);
                    ReportViewerProcessing(_reportViewer);
                    ReportControl.Visibility = Visibility.Visible;

                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        ReportControl.Visibility = Visibility.Collapsed;
                        MailDesc.Visibility = Visibility.Collapsed;
                        lblNoRecordFound.Visibility = Visibility.Visible;

                    });

                }
            }
            catch (Exception)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    ReportControl.Visibility = Visibility.Collapsed;
                    MailDesc.Visibility = Visibility.Collapsed;
                    lblNoRecordFound.Visibility = Visibility.Visible;

                });
            }

        }

        private void OnWindowUnloaded(object sender, RoutedEventArgs e)
        {
            if (setReportParam != null)
            {
                setReportParam.Clear();
                setReportParam = null;
            }
            if (_reportViewer.LocalReport.DataSources.Count > 0)
            {
                _reportViewer.LocalReport.DataSources.Clear();
                _reportViewer.Dispose();
            }
           
            WBControl.Dispose();
        }

        private void OnMsgDescCloseBtnClk(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                messageContent = string.Empty;
                lblNoRecordFound.Visibility = Visibility.Collapsed;
                MailDesc.Visibility = Visibility.Collapsed;
                ReportControl.Visibility = Visibility.Visible;

            });

        }

        private void HidePdfFormat(LocalReport ReportViewerID, string strFormatName)
        {
            FieldInfo info;

            foreach (RenderingExtension extension in ReportViewerID.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                    info.SetValue(extension, false);
                    break;
                }
            }
        }

        #endregion

        #region Public Methods
        public void RenderReport<T>(IReports<T> ReportType, T ReportInstance, Func<T, object> ReportKPI)
        {
            try
            {
                ReportBLL<T> reportBLL = new ReportBLL<T>(ReportInstance);
                if (ReportContext.DataSource != null)
                    ReportContext.DataSource = null;
                ReportContext.DataSource = reportBLL.Execute(ReportType, ReportKPI);
                reportBLL = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        #endregion
    }
   
}
