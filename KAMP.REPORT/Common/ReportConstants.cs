﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.REPORT.Common
{
   public class ReportConstants
    {
       public const string DATASET_NAME = "CaseAgingDataSet";
       public const string PATH = "\\ReportDefinitions\\ProductivityReportDefinitions\\";
       public const string CASE_AGING_REPORT = "CaseAgingReport.rdlc";
       public const string CASE_AGING_BY_USER = "CaseAgingByUser.rdlc";
       public const string CLOSED_CASES = "ClosedCases.rdlc";
       public const string CASE_AGING_BY_ROLE = "CaseAgingByRoles.rdlc";
       public const string RFISUMMARY = "RFISummaryReporting.rdlc";
       public const string RFIDETAILS = "RFIDetails.rdlc";
       
       public const string REPORT_MAINPAGE_NAME = "Report";

       public const string ASSIGNED = "Assigned";
       public const string COMPLETED = "Completed";
       public const string DELIVERED = "Delivered";
       public const string STATE_NOT_OPEN = "Not Yet Opened";
       public const string STATE_PARKED = "Case Parked";
       public const string NOT_APPLICABLE = "N/A";

       public const string DATE_FORMAT = "dd-MMM-yyyy";
       public const string SUPER_ADMIN = "Super Admin";

       public const string KYC_STYLESHEET =@"pack://application:,,,/KAMP.KYC;component/Assets/Styles/StyleSheet.xaml";
       public const string KYC_SCROLL = @"pack://application:,,,/KAMP.KYC;component/Assets/Styles/ScrollViewer.xaml";
       public const string KYC_SCROLLNAVIGATION = @"pack://application:,,,/KAMP.KYC;component/Assets/Styles/ScrollViewerNavigation.xaml";

       public const string TLB_STYLESHEET =@"pack://application:,,,/KAMP.TLB;component/Assets/Styles/StyleSheet.xaml";
       public const string TLB_SCROLL = @"pack://application:,,,/KAMP.TLB;component/Assets/Styles/ScrollViewer.xaml";
       public const string TLB_SCROLLDDL = @"pack://application:,,,/KAMP.TLB;component/Assets/Styles/ScrollViewerDDL.xaml";

       public const string MV_STYLESHEET  =@"pack://application:,,,/KAMP.MV;component/Assets/Styles/Style.xaml";
       public const string MV_SCROLLDDL = @"pack://application:,,,/KAMP.MV;component/Assets/Styles/ScrollViewerDDL.xaml";
       public const string MV_SCROLLNAVIGATION = @"pack://application:,,,/KAMP.MV;component/Assets/Styles/ScrollViewerNavigation.xaml";


       public const string MODULE_HEADER = "ModuleHeader";
       public const string REPORT_KPI = "ReportKPI";
       public const string MESSAGE_PARAM = "MessageContent";
       public const string RESPONSEID_PARAM = "RFIResponseID";

       public const string REPORT_CACHE_ITEM = "ReportCacheItem";
       public const string CASES_BY_MODULE = "CasesByModule";
       public const string LST_OF_USRS = "ListOfUsers";
       public const string USRS_ROLE = "ListOfUserRoles";


    }

   public class ReportActions
   {
       public const string CaseAging = "Case Aging";
       public const string CaseAgingByRole = "Case Aging By Role";
       public const string CaseAgingByCompilance = "Case Aging By Compliance";
       public const string ClosedCases = "Closed Cases";
       public const string RFISummary = "RFI Summary";
       public const string RFIDetails = "RFI Details";

   }
}
