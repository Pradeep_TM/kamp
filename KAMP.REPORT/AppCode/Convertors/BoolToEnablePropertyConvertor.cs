﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace KAMP.REPORT.AppCode.Convertors
{
    public class BoolToEnablePropertyConvertor :IValueConverter
    {
         #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
           value = System.Convert.ToBoolean(value);
            if (value==null)
                return false;

            if ((bool)value)
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
    }

