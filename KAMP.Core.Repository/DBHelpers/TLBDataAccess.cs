﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace KAMP.Core.Repository.DBHelpers
{
    using KAMP.Core.Repository.Models.TLB;
    using System.Data;
    using TLB;

    public class TLBDataAccess
    {
        public List<SysColumns> GetSystemColumns()
        {
            return ConnectionHelper.KAMPConnection
                .Query<SysColumns>("Select DISTINCT COLUMN_NAME AS [FieldName] from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME like 'tr%' AND (COLUMN_NAME <> 'trn_id')", commandType: CommandType.Text).ToList();
        }
    }
}