﻿using KAMP.Core.FrameWorkComponents;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    public class ConnectionHelper
    {
        public static SqlConnection KAMPConnection
        {
            get { return new SqlConnection() { ConnectionString = ConfigurationUtility.GetConnectionString() }; }
        }
    }
}
