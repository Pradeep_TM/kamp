﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace KAMP.Core.Repository
{
    using KYC;

    /// <summary>
    /// Designed to  assistthe data acess of KYC module using traditional ADO.net approach.
    /// This helper class is used along with entity framework to assist fetching data of the tables whose schema is not fixed at the application design time.
    /// </summary>
    public class KYCDataAccess
    {
        public List<FormElement> GetTableSchema(string tableName)
        {
            return ConnectionHelper.KAMPConnection
                .Query<FormElement>("Select COLUMN_NAME AS [Key] from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @in_TableName", param: new { @in_TableName = tableName }, commandType: CommandType.Text).ToList();
        }

        public DataTable GetReadOnlyData(string query)
        {
            using (SqlConnection con = ConnectionHelper.KAMPConnection)
            {
                con.Open();
                // 2
                // Create new DataAdapter
                using (SqlDataAdapter a = new SqlDataAdapter(query, con))
                {
                    // 3
                    // Use DataAdapter to fill DataTable
                    DataTable t = new DataTable();
                    a.Fill(t);

                    // 4
                    // Render data onto the screen
                    // dataGridView1.DataSource = t; // <-- From your designer
                    return t;
                }
            }
        }
    }
}
