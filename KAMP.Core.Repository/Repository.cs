﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    public class Repository<TEntity> where TEntity : class
    {
        public DbContext Context;
        internal DbSet<TEntity> _items;
                
        public Repository(DbContext context)
        {            
            this.Context = context;
            this._items = context.Set<TEntity>();
        }                

        public virtual void Insert(TEntity entity)
        {
            _items.Add(entity);
        }

        public virtual void InsertAll(IList<TEntity> entities)
        {
            _items.AddRange(entities);
        }
        
        public virtual void Delete(TEntity entityToDelete)
        {
            if (Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _items.Attach(entityToDelete);
            }
            _items.Remove(entityToDelete);
        }

        public virtual void DeleteAll(IList<TEntity> entitiesToDelete)
        {
            //if (Context.Entry(entityToDelete).State == EntityState.Detached)
            //{
            //    _items.Attach(entityToDelete);
            //}
            _items.RemoveRange(entitiesToDelete);
            
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            _items.Attach(entityToUpdate);
            Context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public IQueryable<TEntity> Items
        {
            get { return _items; }
        }

        public virtual void SaveChanges()
        {
            Context.SaveChanges();
        } 
    }
}
