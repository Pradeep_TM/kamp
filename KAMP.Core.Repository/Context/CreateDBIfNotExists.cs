﻿using KAMP.Core.FrameWorkComponents;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    public class CreateDBIfNotExists : System.Data.Entity.CreateDatabaseIfNotExists<KAMPContext>
    {
        protected override void Seed(KAMPContext context)
        {
            string script = File.ReadAllText(@".\DbScript\SeedData.sql");
            string kycMasterScript = File.ReadAllText(@".\DbScript\KYC_MasterTables.sql");
            SqlConnection conn = new SqlConnection(ConfigurationUtility.GetConnectionString());
            Server server = new Server(new ServerConnection(conn));
            server.ConnectionContext.ExecuteNonQuery(script);
            server.ConnectionContext.ExecuteNonQuery(kycMasterScript);
            base.Seed(context);
        }

        //private string GetConnectionString()
        //{
        //    //SqlConnection myConnection = new SqlConnection();
        //    SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
        //    var serverName=System.Configuration.ConfigurationManager.AppSettings["ServerName"];
        //    var databaseName=System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
        //    if (string.IsNullOrEmpty(serverName.Trim()) || string.IsNullOrEmpty(databaseName.Trim()))
        //        throw new Exception("Application settings is not valid.", new Exception("Server name or database name is not set."));
        //    myBuilder.DataSource = serverName;
        //    myBuilder.InitialCatalog = databaseName;
        //    myBuilder.IntegratedSecurity = true;
        //    return myBuilder.ConnectionString;
        //}
    }
}
