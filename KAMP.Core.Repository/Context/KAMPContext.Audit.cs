﻿using KAMP.Core.Repository.AuditLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    public partial class KAMPContext
    {
        public DbSet<ConfigurationTable> ConfigurationTable { get; set; }
        public DbSet<EntityModelXml> EntityModelXmls { get; set; }
        public DbSet<LogDetail> LogDetails { get; set; }
        public DbSet<Log> Logs { get; set; }
    }
}
