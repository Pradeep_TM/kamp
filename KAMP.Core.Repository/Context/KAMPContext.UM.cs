﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KAMP.Core.Repository.UM;
using System.Threading.Tasks;
using System.Data.Entity;
using KAMP.Core.Repository.Models.UserManagement;

namespace KAMP.Core.Repository
{
    public partial class KAMPContext
    {
        #region UM
        public DbSet<Group> Groups { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PermissionSet> PermissionSet { get; set; }
        public DbSet<PermissionInSet> PermissionInSet { get; set; }
        public DbSet<RoleInPermission> RoleInPermission { get; set; }
        public DbSet<ModuleObjects> ModuleObjects { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserInGroup> UserInGroup { get; set; }
        public DbSet<UserInRole> UserInRole { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Module> mstr_Module { get; set; }
        public DbSet<UserLoginHistory> UserLoginHistory { get; set; }
        #endregion
    }
}
