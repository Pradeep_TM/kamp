﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    using KAMP.Core.Repository.KYC;
    using KYC;

    public partial class KAMPContext
    {
        public DbSet<AdditionalHighRiskFactor> AdditionalHighRiskFactor { get; set; }
        public DbSet<KYCCaseFile> KYCCaseFile { get; set; }
        public DbSet<FileLog> FileLog { get; set; }
        public DbSet<DocumentCode> DocumentCode { get; set; }
        public DbSet<DuplicateCase> DuplicateCase { get; set; }
        public DbSet<FormElement> FormElement { get; set; }
        public DbSet<FormElementOption> FormElementOption { get; set; }
        public DbSet<KPMGEditable> KPMGEditable { get; set; }
        public DbSet<PartyType> PartyType { get; set; }
        public DbSet<ReqCategory> ReqCategory { get; set; }
        public DbSet<ReqClassification> ReqClassification { get; set; }
        public DbSet<ReqGridColumn> ReqGridColumn { get; set; }
        public DbSet<ReqOption> ReqOption { get; set; }
        public DbSet<ReqType> ReqType { get; set; }
        public DbSet<Requirement> Requirement { get; set; }
        public DbSet<Response> Response { get; set; }
        public DbSet<ResponseGridCell> ResponseGridCell { get; set; }
        public DbSet<ResponseOption> ResponseOption { get; set; }
        public DbSet<KYCLookUp> KYCLookUp { get; set; }
        public DbSet<KYCLookUpGroups> KYCLookUpGroups { get; set; }
    }
}
