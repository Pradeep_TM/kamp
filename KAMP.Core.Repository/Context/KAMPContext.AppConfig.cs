﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Context
{
   public partial class KAMPContext
    {
        public DbSet<Client> Client { get; set; }
        public DbSet<ClientEngagement> ClientEngagement { get; set; }
    }
}
