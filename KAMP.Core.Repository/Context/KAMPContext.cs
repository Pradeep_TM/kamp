using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace KAMP.Core.Repository
{
    using Models.Mapping;
    using Models;
    using KAMP.Core.Repository.UM;
    using WF;
    using KAMP.Core.Repository.KYC;
    using KAMP.Core.Repository.MV;
    using KAMP.Core.Repository.AuditLog;
    using KAMP.Core.Repository.TLB;
    using KAMP.Core.Repository.Models.TLB.Mapping;
    using KAMP.Core.Repository.RFI;
    using KAMP.Core.Repository.Models.UserManagement.Mapping;
    using System.Windows;
    using System.Data.SqlClient;
    using System.Configuration;
    using System;
using KAMP.Core.FrameWorkComponents;

    public partial class KAMPContext : DbContext
    {
        static KAMPContext()
        {
            Database.SetInitializer<KAMPContext>((new CreateDBIfNotExists()));
        }

        public KAMPContext(string conString)
            : base(conString)
        {
            // TODO: Complete member initialization
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        public KAMPContext()
            : base(ConfigurationUtility.GetConnectionString())
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;

            //string serverName = (string)Application.Current.Properties["ServerName"];
            //string dbName = (string)Application.Current.Properties["DBName"];

            //if (serverName != null && dbName != null)
            //{
            //    //SqlConnection myConnection = new SqlConnection();
            //    SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
            //    myBuilder.DataSource = serverName;
            //    myBuilder.InitialCatalog = dbName;
            //    myBuilder.IntegratedSecurity = true;
            //    myConnection.ConnectionString = myBuilder.ConnectionString;
            //    this.conString = myBuilder.ConnectionString;
            //}
            //else
            //{
            //    string connectionString = ConfigurationManager.ConnectionStrings["KAMPContext"].ConnectionString;
            //}
        }

        //private static string GetConnectionString()
        //{
        //    //SqlConnection myConnection = new SqlConnection();
        //    SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
        //    var serverName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
        //    var databaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
        //    if (string.IsNullOrEmpty(serverName.Trim()) || string.IsNullOrEmpty(databaseName.Trim()))
        //        throw new Exception("Application settings is not valid.", new Exception("Server name or database name is not set."));
        //    myBuilder.DataSource = serverName;
        //    myBuilder.InitialCatalog = databaseName;
        //    myBuilder.IntegratedSecurity = true;
        //    return myBuilder.ConnectionString;
        //}

        public DbSet<CommonConfiguration> CommonConfigs { get; set; }
        public DbSet<DataManagement> DataManagement { get; set; }
        public DbSet<UploadType> UploadType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            #region Common
            modelBuilder.Configurations.Add(new CommonConfigMap());
            modelBuilder.Configurations.Add(new DataManagementMap());
            modelBuilder.Configurations.Add(new UploadTypeMap());
            modelBuilder.Configurations.Add(new EngagementMap());
            modelBuilder.Configurations.Add(new ClientMap());
            #endregion

            #region AuditLog
            modelBuilder.Configurations.Add(new ConfigurationTableMap());
            modelBuilder.Configurations.Add(new EntityModelXmlMap());
            modelBuilder.Configurations.Add(new LogDetailMap());
            modelBuilder.Configurations.Add(new LogMap());
            #endregion
            #region RFI
            modelBuilder.Configurations.Add(new RequestForInformationMap());
            modelBuilder.Configurations.Add(new RFIAttachmentsMap());
            modelBuilder.Configurations.Add(new RFIResponsesMap());
            #endregion

            #region UM
            modelBuilder.Configurations.Add(new GroupMap());
            modelBuilder.Configurations.Add(new ModuleMap());
            modelBuilder.Configurations.Add(new PermissionMap());
            modelBuilder.Configurations.Add(new PermissionSetMap());
            modelBuilder.Configurations.Add(new PermissionInSetMap());
            modelBuilder.Configurations.Add(new RoleInPermissionMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new ModuleObjectsMap());
            modelBuilder.Configurations.Add(new UserInGroupMap());
            modelBuilder.Configurations.Add(new UserInRoleMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserLoginHistoryMap());
            #endregion

            #region WF
            modelBuilder.Configurations.Add(new AssignedToMap());
            modelBuilder.Configurations.Add(new AssignmentMap());
            modelBuilder.Configurations.Add(new StateMap());
            modelBuilder.Configurations.Add(new ActivityMap());
            modelBuilder.Configurations.Add(new WFDefinitionMap());
            modelBuilder.Configurations.Add(new StepMap());
            modelBuilder.Configurations.Add(new WFTrackingMap());
            modelBuilder.Configurations.Add(new WFRoutingMap());
            modelBuilder.Configurations.Add(new StateMetadataMap());
            #endregion

            #region KYC MODEL MAPPINGS
            modelBuilder.Configurations.Add(new AdditionalHighRiskFactorMap());
            modelBuilder.Configurations.Add(new KAMP.Core.Repository.KYC.CaseFileMap());
            modelBuilder.Configurations.Add(new DocumentCodeMap());
            modelBuilder.Configurations.Add(new DuplicateCaseMap());
            modelBuilder.Configurations.Add(new FileLogMap());
            modelBuilder.Configurations.Add(new FormElementMap());
            modelBuilder.Configurations.Add(new KPMGEditableMap());
            modelBuilder.Configurations.Add(new FormElementOptionMap());
            modelBuilder.Configurations.Add(new PartyTypeMap());
            modelBuilder.Configurations.Add(new ReqCategoryMap());
            modelBuilder.Configurations.Add(new ReqClassificationMap());
            modelBuilder.Configurations.Add(new ReqGridColumnMap());
            modelBuilder.Configurations.Add(new ReqOptionMap());
            modelBuilder.Configurations.Add(new ReqTypeMap());
            modelBuilder.Configurations.Add(new RequirementMap());
            modelBuilder.Configurations.Add(new ResponseMap());
            modelBuilder.Configurations.Add(new ResponseGridCellMap());
            modelBuilder.Configurations.Add(new ResponseOptionMap());
            modelBuilder.Configurations.Add(new KYCLookUpGroupsMap());
            modelBuilder.Configurations.Add(new KYCLookUpMap());
            #endregion

            #region MV
            modelBuilder.Configurations.Add(new PriorityMap());
            modelBuilder.Configurations.Add(new SourceTypeMap());
            modelBuilder.Configurations.Add(new AccountScoresMap());
            modelBuilder.Configurations.Add(new AccountScoreRoutineMap());
            modelBuilder.Configurations.Add(new KAMP.Core.Repository.MV.CaseFileMap());
            modelBuilder.Configurations.Add(new CaseRoutineMap());
            modelBuilder.Configurations.Add(new ParameterGroupsMap());
            modelBuilder.Configurations.Add(new ParametersMap());
            modelBuilder.Configurations.Add(new ResultantMap());
            modelBuilder.Configurations.Add(new RoutineMap());
            modelBuilder.Configurations.Add(new SQLQueryMap());
            modelBuilder.Configurations.Add(new TransactionScoresMap());
            modelBuilder.Configurations.Add(new TransNormMap());
            #endregion

            #region TLB
            modelBuilder.Configurations.Add(new AssesmentFieldMap());
            modelBuilder.Configurations.Add(new AssesmentsMap());
            modelBuilder.Configurations.Add(new AssesmentStoredProceduresMap());
            modelBuilder.Configurations.Add(new AssesmentTypeMap());
            modelBuilder.Configurations.Add(new BatchMap());
            modelBuilder.Configurations.Add(new BatchStepsMap());
            modelBuilder.Configurations.Add(new BatchTypesMap());
            modelBuilder.Configurations.Add(new BinaryLargeObjectMap());
            modelBuilder.Configurations.Add(new CaseAssesmentNoteMap());
            modelBuilder.Configurations.Add(new CaseClientRelatedMap());
            modelBuilder.Configurations.Add(new TLBCaseFileMap());
            modelBuilder.Configurations.Add(new CaseRelatedTransactionMap());
            modelBuilder.Configurations.Add(new CaseTransactionsMap());
            modelBuilder.Configurations.Add(new ClientAccountsMap());
            modelBuilder.Configurations.Add(new ClientNameVariationsMap());
            modelBuilder.Configurations.Add(new HistoryDefaultMessageMap());
            modelBuilder.Configurations.Add(new HistoryGroupsMap());
            modelBuilder.Configurations.Add(new HistoryMap());
            modelBuilder.Configurations.Add(new HistoryVariablesMap());
            modelBuilder.Configurations.Add(new LookUpGroupsMap());
            modelBuilder.Configurations.Add(new LookUpMap());
            modelBuilder.Configurations.Add(new ParameterAliasMap());
            modelBuilder.Configurations.Add(new TLBParameterGroupsMap());
            modelBuilder.Configurations.Add(new TLBParametersMap());
            modelBuilder.Configurations.Add(new ScoresAccountsMap());
            modelBuilder.Configurations.Add(new ScoresTransactionsMap());
            modelBuilder.Configurations.Add(new TransnormLookUpGroupsMap());
            modelBuilder.Configurations.Add(new TransnormLookUpMap());
            modelBuilder.Configurations.Add(new TLBTransNormMap());
            modelBuilder.Configurations.Add(new TransTotalScoreMap());
            modelBuilder.Configurations.Add(new FilterCriteriaMap());
            #endregion
        }

    }
}
