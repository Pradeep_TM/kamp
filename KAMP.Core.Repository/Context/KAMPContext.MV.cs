﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace KAMP.Core.Repository
{
    using MV;    

    public partial class KAMPContext
    {
        public DbSet<Priority> Priority { get; set; }
        public DbSet<SourceType> SourceType { get; set; }
        public DbSet<AccountScores> AccountScores { get; set; }
        public DbSet<MVCaseFile> MVCaseFile { get; set; }
        public DbSet<CaseRoutine> CaseRoutine { get; set; }
        public DbSet<ParameterGroups> ParameterGroups { get; set; }
        public DbSet<Parameters> Parameters { get; set; }
        public DbSet<Resultant> Resultant { get; set; }
        public DbSet<Routine> Routine { get; set; }
        public DbSet<SQLQuery> SQLQuery { get; set; }
        public DbSet<TransactionScores> TransactionScores { get; set; }
        public DbSet<TransNorm> TransNorm { get; set; }
    }
}
