﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    using TLB;
    using System.Data.Entity;
    using KAMP.Core.Repository.Models.TLB;
    public partial class KAMPContext
    {
        public DbSet<AssesmentField> AssesmentField { get; set; }
        public DbSet<Assesments>  Assesments{ get; set; }
        public DbSet<AssesmentStoredProcedures> AssesmentStoredProcedures { get; set; }
        public DbSet<AssesmentType> AssesmentType { get; set; }
        public DbSet<Batch> Batch { get; set; }
        public DbSet<BatchSteps> BatchSteps { get; set; }
        public DbSet<BatchTypes> BatchTypes { get; set; }
        public DbSet<BinaryLargeObject> BinaryLargeObject { get; set; }
        public DbSet<CaseAssesmentNote> CaseAssesmentNote { get; set; }
        public DbSet<CaseClientRelated> CaseClientRelated { get; set; }
        public DbSet<CaseRelatedTransaction> CaseRelatedTransaction { get; set; }
        public DbSet<CaseTransactions> CaseTransactions { get; set; }
        public DbSet<ClientAccounts> ClientAccounts { get; set; }
        public DbSet<ClientNameVariations> ClientNameVariations { get; set; }
        public DbSet<History> History { get; set; }
        public DbSet<HistoryDefaultMessage> HistoryDefaultMessage { get; set; }
        public DbSet<HistoryGroups> HistoryGroups { get; set; }
        public DbSet<HistoryVariables> HistoryVariables { get; set; }
        public DbSet<LookUp> LookUp { get; set; }
        public DbSet<LookUpGroups> LookUpGroups { get; set; }
        public DbSet<ParameterAlias> ParameterAlias { get; set; }
        public DbSet<ScoresAccounts> ScoresAccounts { get; set; }
        public DbSet<ScoresTransactions> ScoresTransactions { get; set; }
        public DbSet<TLBCaseFile> TLBCaseFile { get; set; }
        public DbSet<TLBParameterGroups> TLBParameterGroups { get; set; }
        public DbSet<TLBParameters> TLBParameters { get; set; }
        public DbSet<TLBTransNorm> TLBTransNorm { get; set; }
        public DbSet<TransnormLookUp> TransnormLookUp { get; set; }
        public DbSet<TransnormLookUpGroups> TransnormLookUpGroups { get; set; }
        public DbSet<TransTotalScore> TransTotalScore { get; set; }
        public DbSet<FilterCriteria> FilterCriteria { get; set; }
        public DbSet<DoNotAssignCaseList> DoNotAssignCaseList { get; set; }
     
    }
}
