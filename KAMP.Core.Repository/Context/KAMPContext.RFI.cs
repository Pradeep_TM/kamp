﻿using KAMP.Core.Repository.RFI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Context
{
    public partial class KAMPContext
    {
        public DbSet<RequestForInformation> ConfigurationTable { get; set; }
        public DbSet<RFIAttachments> EntityModelXmls { get; set; }
        public DbSet<RFIResponses> LogDetails { get; set; }
    }
}
