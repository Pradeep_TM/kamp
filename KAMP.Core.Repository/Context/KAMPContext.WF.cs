﻿using KAMP.Core.Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    using WF;
    public partial class KAMPContext
    {
        public DbSet<AssignedTo> AssignmentTo { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<WFDefinition> WfDefinitions { get; set; }
        public DbSet<Step> WfSteps { get; set; }
        public DbSet<WFTracking> WfTrackings { get; set; }
        public DbSet<WFRouting> WFRoutings { get; set; }
        public DbSet<StateMetadata> StateMetadata { get; set; }
    }
}
