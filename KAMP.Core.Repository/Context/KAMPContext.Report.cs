﻿using KAMP.Core.Repository.MV;
using KAMP.Core.Repository.Report;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.WF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Context
{
    public partial class KAMPContext
    {
        public DbSet<CaseAgingReportDetails> reportDetails { get; set; }
       
    }
}
