﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Enum
{
    public class Enums
    {
        public enum PermissionSet
        {
            CanAdd = 1,
            CanDelete = 2,
            CanView = 3,
            CanExport = 4,
            CanEdit = 5
        }
        public enum DataImportMode : short
        {
            Insert = 1,
            Append
        }
    }
}
