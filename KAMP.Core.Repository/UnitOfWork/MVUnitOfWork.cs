﻿using KAMP.Core.Interfaces;
using KAMP.Core.Repository.MV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KAMP.Core.Repository.UnitOfWork
{
    using FrameworkComponents;

    public class MVUnitOfWork : BaseUnitOfWork
    {
        public MVUnitOfWork()
            : base(new KAMPContext())
        {

        }
    }
}
