﻿using KAMP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.TLB;


namespace KAMP.Core.Repository.UnitOfWork
{
    public class TLBUnitOfWork : BaseUnitOfWork
    {
        public TLBUnitOfWork():base(new KAMPContext())
        {
            
        }
    }
}
