﻿using KAMP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace KAMP.Core.Repository.UnitOfWork
{
    using FrameworkComponents;
    using System.Data.Entity.Core.Metadata.Edm;
    using FrameworkComponents.Extenstions;
    using System.Data;
    using System.Data.SqlClient;
    using System.Xml;
    using System.Data.Entity.Infrastructure;
    using KAMP.Core.Repository.AuditLog;
    public class BaseUnitOfWork : IUnitOfWork
    {
        protected DbContext Context = null;

        private IDictionary<string, dynamic> _repositoryCache;

        public BaseUnitOfWork(DbContext context)
        {
            if (Context.IsNull())
                Context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Repository<T> GetRepository<T>() where T : class
        {
            var typeFullName = typeof(T).FullName;

            if (_repositoryCache == null)
            {
                _repositoryCache = new Dictionary<string, dynamic>();
            }

            if (!_repositoryCache.ContainsKey(typeFullName))
            {
                _repositoryCache.Add(typeFullName, new Repository<T>(Context));
            }

            return _repositoryCache[typeFullName] as Repository<T>;
        }

        public List<string> GetEntityNames(string entityFilter)
        {
            return Context.GetEntityNames(entityFilter);
        }

        public IEnumerable<string> GetAttributeNames(string entity)
        {
            return Context.GetAttributeNames(entity);
        }

        public IEnumerable<string> GetAlias(string entity)
        {
            return Context.GetAlias(entity);
        }

        //public List<T> ExecuteSqlQuery<T>(string sqlQuery)
        //{
        //    try
        //    {
        //        return Context.Database.SqlQuery<T>(sqlQuery).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new BusinessException(ex.Message);
        //    }        
        //}

        //public IQueryable<T> ExecuteSqlQuery<T>(string sqlQuery)
        //{
        //    try
        //    {
        //        return Context.Database.SqlQuery<T>(sqlQuery).AsQueryable<T>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new BusinessException(ex.Message);
        //    }
        //} 

        public DataTable ExecuteSqlQuery(string query)
        {
            try
            {
                using (SqlConnection con = ConnectionHelper.KAMPConnection)
                {
                    con.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(query, con))
                    {
                        DataTable t = new DataTable();
                        a.Fill(t);
                        return t;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BusinessException(ex.Message);
            }
        }

        #region IUnitOfWork
        public void SaveChanges()
        {
            var entry = Context.ChangeTracker.Entries();
            var count = entry.Count();
            Dictionary<DbEntityEntry, XmlDocument> listXMLDoc = new Dictionary<DbEntityEntry, XmlDocument>();
            if (count != 0)
            {
                listXMLDoc = GenerateXmlListToBeLogged();
            }

            try
            {
                if (Context.IsNotNull())
                    Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (count != 0 && listXMLDoc.Count > 0)
            {
                //TODO Need to change the logic for unique identifier
                SaveXmlToDb(listXMLDoc);
            }
        }



        #region Audit
        public IEnumerable<string> KeysFor(DbContext context, Type entityType)
        {
            var metadataWorkspace = ((IObjectContextAdapter)context).ObjectContext.MetadataWorkspace;
            var objectItemCollection = (ObjectItemCollection)metadataWorkspace.GetItemCollection(DataSpace.OSpace);
            var ospaceType = metadataWorkspace.GetItems<EntityType>(DataSpace.OSpace).SingleOrDefault(t => objectItemCollection.GetClrType(t) == entityType);
            if (ospaceType == null)
            {
                throw new ArgumentException(string.Format("The type '{0}' is not mapped as an entity type.", entityType.Name), "entityType");
            }
            return ospaceType.KeyMembers.Select(k => k.Name);
        }

        private Dictionary<DbEntityEntry, XmlDocument> GenerateXmlListToBeLogged()
        {
            //creating XML Before Saving The Changes
            Dictionary<DbEntityEntry, XmlDocument> listXMLDoc = new Dictionary<DbEntityEntry, XmlDocument>();

            var entries = Context.ChangeTracker.Entries();
            foreach (var entry in entries)
            {
                StringBuilder xmlToLog = new StringBuilder();

                string entityName = GetTypeFromEntry(entry);
                XmlDocument doc = new XmlDocument();

                //Getting Configuration Data
                ConfigurationTable configEntry = null;

                using (var auditUnitOfWork = new AuditUnitOfWork())
                {
                    var repo = auditUnitOfWork.GetRepository<ConfigurationTable>();
                    configEntry = repo.Items.Where(x => x.EntityName == entityName).FirstOrDefault();
                }


                if (configEntry == null) break;
                var configXml = configEntry.ConfigurationXml;

                doc.InnerXml = configXml;

                XmlNode root = doc.DocumentElement;
                var nodes = root.SelectNodes("descendant::" + entityName);

                for (int nodesCount = 0; nodesCount < nodes.Count; nodesCount++)
                {
                    var statusConfigured = nodes.Item(nodesCount).Attributes.GetNamedItem("Action").Value;
                    if (statusConfigured.ToString().ToLower().Equals(entry.State.ToString().ToLower()))
                    {
                        //Needs to log the audit for current status of Entity
                        LogAudit(ref xmlToLog, entry, nodes.Item(nodesCount));
                    }
                }
                XmlDocument xmlDocToLog = new XmlDocument();

                var context = System.Windows.Application.Current.Properties["Context"] as Context;
                if (xmlToLog.Length == 0) return listXMLDoc;
                xmlDocToLog.InnerXml = xmlToLog.ToString();
                xmlDocToLog.DocumentElement.SetAttribute("DateTime", DateTime.Now.ToString());
                xmlDocToLog.DocumentElement.SetAttribute("Action", entry.State.ToString());
                xmlDocToLog.DocumentElement.SetAttribute("Createdby", context.User.UserName.Split('\\').LastOrDefault());
                listXMLDoc.Add(entry, xmlDocToLog);
            }
            return listXMLDoc;
        }

        private void SaveXmlToDb(Dictionary<DbEntityEntry, XmlDocument> listXMLDoc)
        {
            //Log after save changes
            
            foreach (var doc in listXMLDoc)
            {
                var idprop = KeysFor(Context, doc.Key.Entity.GetType());
                var id = doc.Key.CurrentValues.GetValue<object>(idprop.FirstOrDefault());
                int? cfID = null;

                try
                {
                    cfID = doc.Key.CurrentValues.GetValue<int>("cf_ID");
                }
                catch (Exception)
                {   
                }

                LogDetail entity = new LogDetail();
                entity.EntityName = doc.Value.DocumentElement.Name;
                entity.DateTime = doc.Value.DocumentElement.GetAttribute("DateTime");
                entity.Action = doc.Value.DocumentElement.GetAttribute("Action");
                entity.CreatedBy = doc.Value.DocumentElement.GetAttribute("Createdby");
                entity.EntityUId = Convert.ToInt32(id);
                var context = System.Windows.Application.Current.Properties["Context"] as Context;
                entity.ModuleId = context.Module.Id;
                entity.Logs.Add(new Log { AuditXml = doc.Value.DocumentElement.OuterXml });
                entity.cf_ID = (int?)cfID;
                //Adding entity to be logged

                using (var auditUnitOfWork = new AuditUnitOfWork())
                {
                    var repo = auditUnitOfWork.GetRepository<LogDetail>();
                    repo.Insert(entity);
                    auditUnitOfWork.SaveChanges();
                }
                //LoggingEntities.Add(entity);

            }
        }

        private void LogAudit(ref StringBuilder xmlToLog, DbEntityEntry entityEntry, XmlNode xmlNode)
        {
            xmlToLog.AppendLine("<" + xmlNode.Name + ">");
            //Getting Property list from the XMl Node
            var nodes = xmlNode.ChildNodes;
            foreach (var node in nodes)
            {
                XmlNode innerNode = (XmlNode)node;
                if (innerNode != null && !innerNode.HasChildNodes)
                {
                    CreateXMLForAudit(ref xmlToLog, innerNode, entityEntry);
                }
                else
                {
                    foreach (var entry in Context.ChangeTracker.Entries())
                    {
                        string type = GetTypeFromEntry(entry);
                        if (type == innerNode.Name)
                            LogAudit(ref xmlToLog, entry, innerNode);
                    }
                }
            }
            xmlToLog.AppendLine("</" + xmlNode.Name + ">");
        }

        private void CreateXMLForAudit(ref StringBuilder xmlToLog, XmlNode innerNode, DbEntityEntry entityEntry)
        {
            try
            {
                if (entityEntry.State == EntityState.Modified)
                {
                    var oldValue = entityEntry.OriginalValues.GetValue<object>(innerNode.Name);
                    if (oldValue.IsNull()) oldValue = string.Empty;
                    var newValue = entityEntry.CurrentValues.GetValue<object>(innerNode.Name);
                    if (newValue.IsNull()) newValue = string.Empty;

                    if (!oldValue.Equals(newValue))
                    {
                        xmlToLog.AppendLine("<" + innerNode.Name + ">" + oldValue + "#$#" + newValue + "</" + innerNode.Name + ">");
                    }
                }
                if (entityEntry.State == EntityState.Added)
                {
                    var value = entityEntry.CurrentValues.GetValue<object>(innerNode.Name);
                    xmlToLog.AppendLine("<" + innerNode.Name + ">" + value + "</" + innerNode.Name + ">");
                }
            }
            catch (Exception ex)
            {                
             //Commented to aviod error in case of Ignored properties i.e Properties ignore in Entity mapping
            }
        }

        private string GetTypeFromEntry(DbEntityEntry entry)
        {
            var type = entry.Entity.GetType().BaseType.Name;
            if (type.Equals("Object")) return entry.Entity.GetType().Name;
            return type;
        }
        #endregion

        public void Dispose()
        {
            if (Context.IsNotNull())
                Context.Dispose();
            Context = null;
            //GC.SuppressFinalize(Context);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

 
