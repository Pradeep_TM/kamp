﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UnitOfWork
{
    public class AuditUnitOfWork : BaseUnitOfWork
    {
        public AuditUnitOfWork()
            : base(new KAMPContext())
        {

        }
    }
}
