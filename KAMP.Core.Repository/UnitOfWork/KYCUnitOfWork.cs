﻿using KAMP.Core.Interfaces;
using KAMP.Core.Repository.MV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KAMP.Core.Repository.KYC
{
    using FrameworkComponents;
    using KAMP.Core.Repository.UnitOfWork;    

    public class KYCUnitOfWork : BaseUnitOfWork
    {
        public KYCUnitOfWork() : base(new KAMPContext())
        {
            
        }
    }
}
