﻿using KAMP.Core.Repository.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UnitOfWork
{
    public class DataImportUnitOfWork : BaseUnitOfWork
    {
        public DataImportUnitOfWork(string conString)
            : base(new KAMPContext(conString))
        {

        }
    }
}
