﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.WF
{
    using Repository;
    using Repository.UM;
    using FrameworkComponents;
    using Interfaces;
    using KAMP.Core.Repository.UnitOfWork;

    public class WFUnitOfWork : BaseUnitOfWork
    {
       // private KAMPContext _dbContext = null;
        public WFUnitOfWork():base(new KAMPContext())
        {

            //if (_dbContext.IsNull())
            //    _dbContext = new KAMPContext();
        }

        //private Repository<AssignedTo> _assignTo;

        //public Repository<AssignedTo> AssignedTo
        //{
        //    get
        //    {
        //        if (_assignTo.IsNull())
        //            _assignTo = new Repository<AssignedTo>(_dbContext);
        //        return _assignTo;
        //    }

        //}

        //private Repository<Assignment> _assignment;

        //public Repository<Assignment> Assignment
        //{
        //    get
        //    {
        //        if (_assignment.IsNull())
        //            _assignment = new Repository<Assignment>(_dbContext);
        //        return _assignment;
        //    }

        //}

        //private Repository<State> _state;

        //public Repository<State> State
        //{
        //    get
        //    {
        //        if (_state.IsNull())
        //            _state = new Repository<State>(_dbContext);
        //        return _state;
        //    }

        //}

        //private Repository<Activity> _activity;

        //public Repository<Activity> Activity
        //{
        //    get
        //    {
        //        if (_activity.IsNull())
        //            _activity = new Repository<Activity>(_dbContext);
        //        return _activity;
        //    }

        //}

        //private Repository<WFDefinition> _wFDefinition;

        //public Repository<WFDefinition> WFDefinition
        //{
        //    get
        //    {
        //        if (_wFDefinition.IsNull())
        //            _wFDefinition = new Repository<WFDefinition>(_dbContext);
        //        return _wFDefinition;
        //    }

        //}

        //private Repository<Document> _document;

        //public Repository<Document> Document
        //{
        //    get
        //    {
        //        if (_document.IsNull())
        //            _document = new Repository<Document>(_dbContext);
        //        return _document;
        //    }

        //}

        //private Repository<LookupAction> _wflkpAction;

        //public Repository<LookupAction> WflkpAction
        //{
        //    get
        //    {
        //        if (_wflkpAction.IsNull())
        //            _wflkpAction = new Repository<LookupAction>(_dbContext);
        //        return _wflkpAction;
        //    }

        //}

        //private Repository<Step> _steps;

        //public Repository<Step> Steps
        //{
        //    get
        //    {
        //        if (_steps.IsNull())
        //            _steps = new Repository<Step>(_dbContext);
        //        return _steps;
        //    }

        //}

        //private Repository<WFTracking> _wfTrackings;

        //public Repository<WFTracking> WfTrackings
        //{
        //    get
        //    {
        //        if (_wfTrackings.IsNull())
        //            _wfTrackings = new Repository<WFTracking>(_dbContext);
        //        return _wfTrackings;
        //    }

        //}

        //private Repository<User> _user;

        //public Repository<User> User
        //{
        //    get
        //    {
        //        if (_user.IsNull())
        //            _user = new Repository<User>(_dbContext);
        //        return _user;
        //    }

        //}

        //private Repository<Group> _group;

        //public Repository<Group> Group
        //{
        //    get
        //    {
        //        if (_group.IsNull())
        //            _group = new Repository<Group>(_dbContext);
        //        return _group;
        //    }

        //}

        //private Repository<Module> _module;

        //public Repository<Module> Module
        //{
        //    get
        //    {
        //        if (_module.IsNull())
        //            _module = new Repository<Module>(_dbContext);
        //        return _module;
        //    }

        //}


        //#region IUnitOfWork
        //public void SaveChanges()
        //{
        //    if (_dbContext.IsNotNull())
        //        _dbContext.SaveChanges();
        //}

        //public void Dispose()
        //{
        //    if (_dbContext.IsNotNull())
        //        _dbContext.Dispose();

        //    GC.SuppressFinalize(this);
        //}
        //#endregion

        
    }
}
