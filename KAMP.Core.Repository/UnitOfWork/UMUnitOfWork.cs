﻿using KAMP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UnitOfWork
{
    using FrameworkComponents;
    using KAMP.Core.Repository.UM;
    public class UMUnitOfWork : BaseUnitOfWork
    {
        public UMUnitOfWork()
            : base(new KAMPContext())
        {

        }
        //private bool _isDiposable;
        //private KAMPContext _dbContext = null;
        //public UMUnitOfWork()
        //{
        //    _isDiposable = true;
        //    if (_dbContext.IsNull())
        //        _dbContext = new KAMPContext();
        //}


        //private Repository<User> _user;
        //public Repository<User> User
        //{
        //    get
        //    {
        //        if (_user.IsNull())
        //            _user = new Repository<User>(_dbContext);
        //        return _user;
        //    }
        //}

        //private Repository<Module> _module;
        //public Repository<Module> Module
        //{
        //    get
        //    {
        //        if (_module.IsNull())
        //            _module = new Repository<Module>(_dbContext);
        //        return _module;
        //    }
        //}

        //private Repository<Role> _role;
        //public Repository<Role> Role
        //{
        //    get
        //    {
        //        if (_role.IsNull())
        //            _role = new Repository<Role>(_dbContext);
        //        return _role;
        //    }
        //}

        //private Repository<Group> _group;
        //public Repository<Group> Group
        //{
        //    get
        //    {
        //        if (_group.IsNull())
        //            _group = new Repository<Group>(_dbContext);
        //        return _group;
        //    }
        //}

        //private Repository<ModuleObjects> _moduleObjects;
        //public Repository<ModuleObjects> ModuleObjects
        //{
        //    get
        //    {
        //        if (_moduleObjects.IsNull())
        //            _moduleObjects = new Repository<ModuleObjects>(_dbContext);
        //        return _moduleObjects;
        //    }
        //}

        //private Repository<Permission> _permission;
        //public Repository<Permission> Permission
        //{
        //    get
        //    {
        //        if (_permission.IsNull())
        //            _permission = new Repository<Permission>(_dbContext);
        //        return _permission;
        //    }
        //}

        //private Repository<PermissionInSet> _permissionInSet;
        //public Repository<PermissionInSet> PermissionInSet
        //{
        //    get
        //    {
        //        if (_permissionInSet.IsNull())
        //            _permissionInSet = new Repository<PermissionInSet>(_dbContext);
        //        return _permissionInSet;
        //    }
        //}

        //private Repository<PermissionSet> _permissionSet;
        //public Repository<PermissionSet> PermissionSet
        //{
        //    get
        //    {
        //        if (_permissionSet.IsNull())
        //            _permissionSet = new Repository<PermissionSet>(_dbContext);
        //        return _permissionSet;
        //    }
        //}

        //private Repository<RoleInPermission> _roleInPermission;
        //public Repository<RoleInPermission> RoleInPermission
        //{
        //    get
        //    {
        //        if (_roleInPermission.IsNull())
        //            _roleInPermission = new Repository<RoleInPermission>(_dbContext);
        //        return _roleInPermission;
        //    }
        //}

        //private Repository<UserInGroup> _userInGroup;
        //public Repository<UserInGroup> UserInGroup
        //{
        //    get
        //    {
        //        if (_userInGroup.IsNull())
        //            _userInGroup = new Repository<UserInGroup>(_dbContext);
        //        return _userInGroup;
        //    }
        //}

        //private Repository<UserInRole> _userInRole;
        //public Repository<UserInRole> UserInRole
        //{
        //    get
        //    {
        //        if (_userInRole.IsNull())
        //            _userInRole = new Repository<UserInRole>(_dbContext);
        //        return _userInRole;
        //    }
        //}

        //#region IUnitOfWork
        //public void SaveChanges()
        //{
        //    if (_dbContext.IsNotNull())
        //        _dbContext.SaveChanges();
        //}

        //public void Dispose()
        //{
        //    if (_dbContext.IsNotNull())
        //        _dbContext.Dispose();

        //    GC.SuppressFinalize(this);
        //}
        //#endregion
    }
}
