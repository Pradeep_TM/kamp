﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UnitOfWork
{
   public class AppConfigUnitOfWork: BaseUnitOfWork
    {
       public AppConfigUnitOfWork(string conString)
           : base(new KAMPContext(conString))
        {
            
        }


       public AppConfigUnitOfWork()
           : base(new KAMPContext())
       {
            
       }
    }
}
