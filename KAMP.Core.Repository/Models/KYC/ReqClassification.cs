using KAMP.Core.FrameworkComponents;
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [KAMPObjectAttribute("KYC", "Use this Entity to Set permissions to access Requirement Classification screen")]
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in Requirement Classification")]
    public partial class ReqClassification
    {
        public ReqClassification()
        {

        }

        public int Id { get; set; }
        public string ClassificationName { get; set; }
        public bool IsValid { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual ICollection<ReqCategory> ReqCategory { get; set; }
    }
}
