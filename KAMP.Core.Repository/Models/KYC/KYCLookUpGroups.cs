﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.KYC
{
   public partial class KYCLookUpGroups
    {
        public KYCLookUpGroups()
        {
            this.KYCLookUp = new List<KYCLookUp>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<KYCLookUp> KYCLookUp { get; set; }
    }
}
