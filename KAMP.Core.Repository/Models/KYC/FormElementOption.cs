using KAMP.Core.FrameworkComponents;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in Screen Configuration")]
    public partial class FormElementOption
    {
        public int Id { get; set; }
        public int FormElementId { get; set; }
        public string OptionText { get; set; }
        public bool Valid { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int? Value { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual FormElement FormElement { get; set; }
        public virtual ICollection<KPMGEditable> KPMGEditables { get; set; }
    }
}
