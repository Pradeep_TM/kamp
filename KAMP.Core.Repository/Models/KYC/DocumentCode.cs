using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    public partial class DocumentCode
    {
        public DocumentCode()
        {
            
        }

        public int Id { get; set; }
        public string DocumentCodes { get; set; }
        public Nullable<bool> IsValid { get; set; }
    }
}
