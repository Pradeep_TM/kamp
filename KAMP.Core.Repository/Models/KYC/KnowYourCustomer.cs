﻿using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.KYC
{
    [KAMPObjectAttribute("KYC", "Use this Entity to Set permissions to access KYC Module from Home Screen")]
    public class KnowYourCustomer
    {
    }
}
