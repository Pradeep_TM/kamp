using KAMP.Core.FrameworkComponents;
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [KAMPObjectAttribute("KYC", "Use this Entity to Set permissions to access KYC Configuration Screen")]
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in Screen Configuration")]
    public partial class FormElement
    {
        public FormElement()
        {
        }

        public int Id { get; set; }
        public string Key { get; set; }
        public string DisplayName { get; set; }
        public int ValueType { get; set; }        
        public bool IsValid { get; set; }
        public bool IsMandatory { get; set; }
        public string OptionsHeading { get; set; }
        public int FormId { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Tooltip { get; set; }
        public virtual ICollection<FormElementOption> FormElementOption { get; set; }
        public virtual ReqType ReqType { get; set; }
        public virtual ICollection<KPMGEditable> KPMGEditable { get; set; }
    }
}
