using KAMP.Core.FrameworkComponents;
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [KAMPObjectAttribute("KYC", "Use this Entity to Set permissions to access Requirement screen")]
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in Requirement")]
    public partial class Requirement
    {
        public Requirement()
        {
        }

        public long Id { get; set; }
        public string RequirementText { get; set; }
        public int ReqType { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsDefault { get; set; }
        public Nullable<int> ReqCategoryId { get; set; }
        public Nullable<long> ParentReqId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int ModifiedBy { get; set; }
        public virtual ReqCategory ReqCategory { get; set; }
        public virtual ICollection<ReqGridColumn> ReqGridColumn { get; set; }
        public virtual ICollection<ReqOption> ReqOption { get; set; }
        public virtual ICollection<Requirement> ChildRequirements { get; set; }
        public virtual Requirement ParentRequirement { get; set; }
        public virtual ICollection<Response> Response { get; set; }
    }
}
