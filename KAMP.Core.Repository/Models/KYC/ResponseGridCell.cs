using KAMP.Core.FrameworkComponents;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in grid response")]
    public partial class ResponseGridCell
    {
        public ResponseGridCell()
        {
            
        }

        public long Id { get; set; }
        public long ResponseId { get; set; }
        public long ColumnId { get; set; }
        public int RowId { get; set; }
        public string ResponseText { get; set; }
        public virtual ReqGridColumn ReqGridColumn { get; set; }
        public virtual Response Response { get; set; }
        public virtual ICollection<ResponseOption> ResponseOptions { get; set; }
    }
}
