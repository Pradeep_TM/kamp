using KAMP.Core.FrameworkComponents;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in Requirement Category")]
    public partial class ReqCategory
    {
        public ReqCategory()
        {
            
        }

        public int Id { get; set; }
        public string CategoryName { get; set; }
        public bool IsValid { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int ReqClassificationId { get; set; }
        public virtual ICollection<Requirement> Requirement { get; set; }
        public virtual ReqClassification ReqClassification { get; set; }

        public virtual ICollection<KYCCaseFile> CaseFiles { get; set; }
    }
}
