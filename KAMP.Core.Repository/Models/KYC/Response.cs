using KAMP.Core.FrameworkComponents;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in Response")]
    public partial class Response
    {
        public Response()
        {            
        }

        public long Id { get; set; }
        public int cf_ID { get; set; }
        public string ResponseText { get; set; }
        public long RequirementId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ResponseJson { get; set; }
        public short ResponseState { get; set; }
        public string Comment { get; set; }
        public virtual KYCCaseFile CaseFile { get; set; }
        public virtual Requirement Requirement { get; set; }
        public virtual ICollection<ResponseGridCell> ResponseGridCell { get; set; }
        public virtual ICollection<ResponseOption> ResponseOption { get; set; }
    }
}
