using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    public partial class ReqGridColumn
    {
        public ReqGridColumn()
        {
            
        }

        public long Id { get; set; }
        public long RequirementId { get; set; }
        public string ColumnName { get; set; }
        public int ReqTypeId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int ModifiedBy { get; set; }
        public virtual ICollection<ResponseGridCell> ResponseGridCell { get; set; }
        public virtual ReqType ReqType { get; set; }
        public virtual Requirement Requirement { get; set; }
        public virtual ICollection<ReqOption> ReqOptions { get; set; }
    }
}
