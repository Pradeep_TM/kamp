﻿using KAMP.Core.Interfaces.Models;
using KAMP.Core.Repository.KYC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.KYC
{
    [KAMPObjectAttribute("KYC", "Use this entity to give permission to LookUp screen.")]
    public partial class KYCLookUp
    {
        public int Id { get; set; }
        public int LookGroupId { get; set; }
        public string Value { get; set; }
        public string Reference { get; set; }
        public string OldValue { get; set; }
        public virtual KYCLookUpGroups KYCLookUpGroups { get; set; }
        // public virtual ICollection<KYCCaseFile> KYCCaseFile { get; set; }
    }
}
