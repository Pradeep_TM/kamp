using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [KAMPObjectAttribute("Common", "Use this Entity to Set permissions to access File Log. ")]
    public partial class FileLog
    {
        public int Id { get; set; }
        public string DocumentType { get; set; }
        public byte[] Content { get; set; }
        public int ContentLength { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public int PartyTypeId { get; set; }
        public int DocumentCodeId { get; set; }
        public int cf_ID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModuleId { get; set; }
        public int CreatedBy { get; set; }
       
    }
}
