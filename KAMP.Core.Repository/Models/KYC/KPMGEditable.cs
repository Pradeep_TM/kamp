using KAMP.Core.FrameworkComponents;
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [KAMPObjectAttribute("KYC", "Use this Entity to Set permissions to access KPMG Editable screen")]
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in KPMG Editable")]
    public partial class KPMGEditable
    {
        public long Id { get; set; }
        public int cf_ID { get; set; }
        public string Value { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int GroupId { get; set; }
        public Nullable<int> FormElementId { get; set; }
        public bool IsUpdated { get; set; }
        //public Nullable<int> FormElementOptionId { get; set; }
        public virtual KYCCaseFile CaseFile { get; set; }
        public virtual FormElement FormElement { get; set; }
        public virtual List<FormElementOption> FormElementOptions { get; set; }
    }
}
