using KAMP.Core.FrameworkComponents;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    [LogEntityAttribute("KYC", "Configure this entity to track changes done in single/multi select response")]
    public partial class ResponseOption
    {
        public long Id { get; set; }
        public Nullable<long> ResponseId { get; set; }
        public long ReqOptionId { get; set; }
        public Nullable<long> GridCellId { get; set; }
        public virtual ReqOption RequirementOption { get; set; }
        public virtual Response Response { get; set; }
        public virtual ResponseGridCell ResponseGridCell { get; set; }
    }
}
