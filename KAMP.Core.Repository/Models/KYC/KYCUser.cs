﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.KYC
{
   public class KYCUser
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public List<int> StateIdList { get; set; }
    }
}
