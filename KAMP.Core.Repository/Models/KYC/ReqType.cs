using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    public partial class ReqType
    {
        public ReqType()
        {
            
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<FormElement> FormElement { get; set; }
        public virtual ICollection<ReqGridColumn> ReqGridColumn { get; set; }
    }
}
