using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class KPMGEditableMap : EntityTypeConfiguration<KPMGEditable>
    {
        public KPMGEditableMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //this.Property(t => t.Value)
            //    .IsRequired();

            //this.Property(t => t.CreatedBy)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //this.Property(t => t.ModifiedBy)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //this.Property(t => t.GroupId)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("KYC_KPMGEditable");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.cf_ID).HasColumnName("cf_ID");
            this.Property(t => t.Value).HasColumnName("Value").IsOptional();
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.GroupId).HasColumnName("GroupId");
            this.Property(t => t.FormElementId).HasColumnName("FormElementId");
            Ignore(t => t.IsUpdated);

            // Relationships
            this.HasRequired(t => t.CaseFile)
                .WithMany(t => t.KPMGEditable)
                .HasForeignKey(d => d.cf_ID);
            this.HasOptional(t => t.FormElement)
                .WithMany(t => t.KPMGEditable)
                .HasForeignKey(d => d.FormElementId);

            HasMany(t => t.FormElementOptions)
                .WithMany(t => t.KPMGEditables)
                .Map(m =>
                {
                    m.ToTable("KYC_KPMGEditableOption");
                    m.MapLeftKey("KPMGEditableId"); 
                    m.MapRightKey("OptionId");
                });
            //this.HasOptional(t => t.FormElementOption)
            //    .WithMany(t => t.KPMGEditables)
            //    .HasForeignKey(d => d.FormElementOptionId);
        }
    }
}
