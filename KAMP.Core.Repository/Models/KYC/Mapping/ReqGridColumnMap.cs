using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class ReqGridColumnMap : EntityTypeConfiguration<ReqGridColumn>
    {
        public ReqGridColumnMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ColumnName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("KYC_ReqGridColumn");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RequirementId).HasColumnName("RequirementId");
            this.Property(t => t.ColumnName).HasColumnName("ColumnName");
            this.Property(t => t.ReqTypeId).HasColumnName("ReqTypeId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            //this.Ignore(t => t.ReqOptions);

            // Relationships
            this.HasRequired(t => t.ReqType)
                .WithMany(t => t.ReqGridColumn)
                .HasForeignKey(d => d.ReqTypeId);            

            this.HasMany(t => t.ReqOptions)
                .WithOptional(t => t.ReqGridCol)
                .HasForeignKey(d => d.GridColumnId)
                .WillCascadeOnDelete(false);
        }
    }
}
