using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class FormElementMap : EntityTypeConfiguration<FormElement>
    {
        public FormElementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Key)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DisplayName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.OptionsHeading)
                .HasMaxLength(300);

            this.Property(t => t.GroupName)
                .HasMaxLength(100);

            this.Property(t => t.Tooltip)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("KYC_FormElement");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Key).HasColumnName("Key");
            this.Property(t => t.DisplayName).HasColumnName("DisplayName");
            this.Property(t => t.ValueType).HasColumnName("ValueType");
            this.Property(t => t.IsValid).HasColumnName("IsValid");
            this.Property(t => t.IsMandatory).HasColumnName("IsMandatory");
            this.Property(t => t.OptionsHeading).HasColumnName("OptionsHeading");
            this.Property(t => t.FormId).HasColumnName("FormId");
            this.Property(t => t.GroupId).HasColumnName("GroupId");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.Tooltip).IsOptional().HasColumnName("Tooltip");

            // Relationships
            this.HasRequired(t => t.ReqType)
                .WithMany(t => t.FormElement)
                .HasForeignKey(d => d.ValueType);

        }
    }
}
