using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class DuplicateCaseMap : EntityTypeConfiguration<DuplicateCase>
    {
        public DuplicateCaseMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.SourceID)
                .HasMaxLength(255);

            this.Property(t => t.CustomerName)
                .HasMaxLength(255);

            this.Property(t => t.RemediationPopulation)
                .HasMaxLength(255);

            this.Property(t => t.CISCode)
                .HasMaxLength(255);

            this.Property(t => t.Duplicate)
                .IsRequired();

            this.Property(t => t.DuplicateOf)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("KYC_DuplicateCase");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.SourceID).HasColumnName("SourceID");
            this.Property(t => t.CMTCaseNumber).HasColumnName("CMTCaseNumber");
            this.Property(t => t.CustomerName).HasColumnName("CustomerName");
            this.Property(t => t.RemediationPopulation).HasColumnName("RemediationPopulation");
            this.Property(t => t.CISCode).HasColumnName("CISCode");
            this.Property(t => t.Duplicate).HasColumnName("Duplicate");
            this.Property(t => t.DuplicateOf).HasColumnName("DuplicateOf");
            this.Property(t => t.Consider).HasColumnName("Consider");
            this.Property(t => t.Master).HasColumnName("Master");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.SpecialComments).HasColumnName("SpecialComments");
        }
    }
}
