using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class ReqCategoryMap : EntityTypeConfiguration<ReqCategory>
    {
        public ReqCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CategoryName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("KYC_ReqCategory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.IsValid).HasColumnName("IsValid");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ReqClassificationId).HasColumnName("ReqClassificationId");

            // Relationships
            this.HasRequired(t => t.ReqClassification)
                .WithMany(t => t.ReqCategory)
                .HasForeignKey(d => d.ReqClassificationId);

        }
    }
}
