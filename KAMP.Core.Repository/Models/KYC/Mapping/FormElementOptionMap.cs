using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class FormElementOptionMap : EntityTypeConfiguration<FormElementOption>
    {
        public FormElementOptionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.OptionText)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("KYC_FormElementOption");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FormElementId).HasColumnName("FormElementId");
            this.Property(t => t.OptionText).HasColumnName("OptionText");
            this.Property(t => t.Valid).HasColumnName("Valid");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.Value).HasColumnName("Value").IsOptional();

            // Relationships
            this.HasRequired(t => t.FormElement)
                .WithMany(t => t.FormElementOption)
                .HasForeignKey(d => d.FormElementId);

        }
    }
}
