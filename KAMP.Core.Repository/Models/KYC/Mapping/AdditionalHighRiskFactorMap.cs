using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class AdditionalHighRiskFactorMap : EntityTypeConfiguration<AdditionalHighRiskFactor>
    {
        public AdditionalHighRiskFactorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_AdditionalHighRiskFactor");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.RiskFactor).HasColumnName("AdditionalHighRiskFactor");
            this.Property(t => t.Valid).HasColumnName("Valid");
        }
    }
}
