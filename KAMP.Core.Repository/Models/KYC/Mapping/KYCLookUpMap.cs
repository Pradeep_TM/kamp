﻿using KAMP.Core.Repository.KYC;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.KYC
{
    class KYCLookUpMap : EntityTypeConfiguration<KYCLookUp>
    {
        public KYCLookUpMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Reference)
                .HasMaxLength(220);

            this.Property(t => t.OldValue)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("KYC_LookUp");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LookGroupId).HasColumnName("LookGroupId");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Reference).HasColumnName("Reference");
            this.Property(t => t.OldValue).HasColumnName("OldValue");

            // Relationships
            this.HasRequired(t => t.KYCLookUpGroups)
                .WithMany(t => t.KYCLookUp)
                .HasForeignKey(d => d.LookGroupId);

        }
    }
}
