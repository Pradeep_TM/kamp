using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class ResponseGridCellMap : EntityTypeConfiguration<ResponseGridCell>
    {
        public ResponseGridCellMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_ResponseGridCell");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ResponseId).HasColumnName("ResponseId");
            this.Property(t => t.ColumnId).HasColumnName("ColumnId");
            this.Property(t => t.RowId).HasColumnName("RowId");
            this.Property(t => t.ResponseText).HasColumnName("Response");

            // Relationships
            this.HasRequired(t => t.ReqGridColumn)
                .WithMany(t => t.ResponseGridCell)
                .HasForeignKey(d => d.ColumnId).WillCascadeOnDelete(false);
            this.HasRequired(t => t.Response)
                .WithMany(t => t.ResponseGridCell)
                .HasForeignKey(d => d.ResponseId).WillCascadeOnDelete(false);

        }
    }
}
