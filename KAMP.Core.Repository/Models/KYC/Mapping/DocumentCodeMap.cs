using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class DocumentCodeMap : EntityTypeConfiguration<DocumentCode>
    {
        public DocumentCodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_DocumentCode");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DocumentCodes).HasColumnName("DocumentCodes");
            this.Property(t => t.IsValid).HasColumnName("IsValid");
        }
    }
}
