using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class RequirementMap : EntityTypeConfiguration<Requirement>
    {
        public RequirementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RequirementText)
                .IsRequired();
                

            // Table & Column Mappings
            this.ToTable("KYC_Requirement");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RequirementText).HasColumnName("Requirement");
            this.Property(t => t.ReqType).HasColumnName("ReqType");
            this.Property(t => t.IsMandatory).HasColumnName("IsMandatory");
            this.Property(t => t.IsDefault).HasColumnName("IsDefault");
            this.Property(t => t.ReqCategoryId).HasColumnName("ReqCategoryId");
            this.Property(t => t.ParentReqId).HasColumnName("ParentReqId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");

            // Relationships
            this.HasOptional(t => t.ReqCategory)
                .WithMany(t => t.Requirement)
                .HasForeignKey(d => d.ReqCategoryId);

            this.HasOptional(t => t.ParentRequirement)
                .WithMany(t => t.ChildRequirements)
                .HasForeignKey(d => d.ParentReqId);

            this.HasMany(t => t.ReqOption)
                .WithOptional(t => t.Requirement)
                .HasForeignKey(d => d.RequirementId)
                .WillCascadeOnDelete();

            this.HasMany(t => t.ReqGridColumn)
                .WithRequired(t => t.Requirement)
                .HasForeignKey(d => d.RequirementId)
                .WillCascadeOnDelete();
        }
    }
}
