using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class FileLogMap : EntityTypeConfiguration<FileLog>
    {
        public FileLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.DocumentType)
                .HasMaxLength(100);

            this.Property(t => t.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            this.Property(t => t.ContentLength);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Extension)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.PartyTypeId);

            this.Property(t => t.DocumentCodeId);

            this.Property(t => t.cf_ID);

            this.Property(t => t.CreatedBy);

            // Table & Column Mappings
            this.ToTable("KYC_FileLog");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DocumentType).HasColumnName("DocumentType");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.ContentLength).HasColumnName("ContentLength");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Extension).HasColumnName("Extension");
            this.Property(t => t.PartyTypeId).HasColumnName("PartyTypeId");
            this.Property(t => t.DocumentCodeId).HasColumnName("DocumentCodeId");
            this.Property(t => t.cf_ID).HasColumnName("cf_ID");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
        }
    }
}
