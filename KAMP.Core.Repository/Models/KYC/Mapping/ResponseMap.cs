using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class ResponseMap : EntityTypeConfiguration<Response>
    {
        public ResponseMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_Response");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.cf_ID).HasColumnName("cf_ID");
            this.Property(t => t.ResponseText).HasColumnName("Response");
            this.Property(t => t.RequirementId).HasColumnName("RequirementId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ResponseJson).HasColumnName("ResponseJson");
            this.Property(t => t.ResponseState).HasColumnName("ResponseState");
            this.Property(t => t.Comment)                
                .HasColumnName("Comment");

            // Relationships
            this.HasRequired(t => t.CaseFile)
                .WithMany(t => t.Response)
                .HasForeignKey(d => d.cf_ID);
            this.HasRequired(t => t.Requirement)
                .WithMany(t => t.Response)
                .HasForeignKey(d => d.RequirementId);

        }
    }
}
