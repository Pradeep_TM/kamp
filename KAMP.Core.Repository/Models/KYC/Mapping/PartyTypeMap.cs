using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class PartyTypeMap : EntityTypeConfiguration<PartyType>
    {
        public PartyTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_PartyType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("PartyType");
            this.Property(t => t.Valid).HasColumnName("Valid");
        }
    }
}
