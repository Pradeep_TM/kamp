using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.KYC
{
    public class ResponseOptionMap : EntityTypeConfiguration<ResponseOption>
    {
        public ResponseOptionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_ResponseOption");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ResponseId).HasColumnName("ResponseId");
            this.Property(t => t.ReqOptionId).HasColumnName("ReqOptionId");
            this.Property(t => t.GridCellId).HasColumnName("GridCellId");

            // Relationships
            this.HasOptional(t => t.Response)
                .WithMany(t => t.ResponseOption)
                .HasForeignKey(d => d.ResponseId).WillCascadeOnDelete(false);
            this.HasOptional(t => t.ResponseGridCell)
                .WithMany(t => t.ResponseOptions)
                .HasForeignKey(d => d.GridCellId).WillCascadeOnDelete(false);
            this.HasRequired(t => t.RequirementOption)
                .WithMany(d => d.ResponseOptions)
                .HasForeignKey(d => d.ReqOptionId).WillCascadeOnDelete(false);

        }
    }
}
