using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    public partial class PartyType
    {
        public PartyType()
        {
            
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Valid { get; set; }
    }
}
