using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    public partial class AdditionalHighRiskFactor
    {
        public int ID { get; set; }
        public string RiskFactor { get; set; }
        public Nullable<bool> Valid { get; set; }
    }
}
