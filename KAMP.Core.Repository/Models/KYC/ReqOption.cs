using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.KYC
{
    public partial class ReqOption
    {
        public long Id { get; set; }
        public Nullable<long> RequirementId { get; set; }
        public Nullable<long> GridColumnId { get; set; }
        public string OptionName { get; set; }
        public bool IsValid { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual Requirement Requirement { get; set; }
        public virtual ICollection<ResponseOption> ResponseOptions { get; set; }
        public virtual ReqGridColumn ReqGridCol { get; set; }
    }
}
