using KAMP.Core.Interfaces.Models;
using KAMP.Core.Repository.Models.TLB;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    [KAMPObjectAttribute("TLB","Use this entity to give permission to Risk Assessment screen.")]
    public partial class Assesments
    {
        public Assesments()
        {
            this.TLB_CaseAssesmentNote = new List<CaseAssesmentNote>();
            this.TLB_ScoresTransactions = new List<ScoresTransactions>();
        }

        public int Id { get; set; }
        public int AssesmentTypeId { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public Nullable<int> AccountScore { get; set; }
        public Nullable<int> TransactionScore { get; set; }
        public Nullable<bool> Compiled { get; set; }
        public Nullable<bool> Approved { get; set; }
        public Nullable<int> AssessmentStoredProcedure_ID { get; set; }
        public Nullable<System.DateTime> RanOn { get; set; }
        public string BusinessRule { get; set; }
        public Nullable<bool> IsUsedInOneMonth { get; set; }
        public Nullable<bool> IsUsedInLookBack { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public string DescriptionOriginal { get; set; }
        public string StoredProcedureName { get; set; }
        public virtual AssesmentType TLBAssesmentType { get; set; }
        public virtual ICollection<CaseAssesmentNote> TLB_CaseAssesmentNote { get; set; }
        public virtual ICollection<ScoresTransactions> TLB_ScoresTransactions { get; set; }
        public virtual ICollection<FilterCriteria> FilterCriteria { get; set; }
    }
}
