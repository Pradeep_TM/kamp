using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class HistoryVariables
    {
        public int Id { get; set; }
        public string FKeyTable { get; set; }
        public string FKeyField { get; set; }
        public Nullable<int> FKeyValue { get; set; }
        public string MessField { get; set; }
        public string MessValue { get; set; }
        public Nullable<int> HistoryId { get; set; }
    }
}
