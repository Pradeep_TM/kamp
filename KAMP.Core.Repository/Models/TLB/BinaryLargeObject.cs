using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class BinaryLargeObject
    {
        public int Id { get; set; }
        public byte[] Object { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public int cf_Id { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
    }
}
