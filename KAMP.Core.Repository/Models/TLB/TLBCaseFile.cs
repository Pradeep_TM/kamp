using KAMP.Core.FrameworkComponents;
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    [KAMPObjectAttribute("TLB", "Use this entity to set permission on Assign Case, Case Management, Case file card")]
    [LogEntityAttribute("TLB", "Configure this entity to track changes done in dispositions tab in case file card.")]
    public partial class TLBCaseFile
    {
        public TLBCaseFile()
        {
            this.TLB_CaseAssesmentNote = new List<CaseAssesmentNote>();
            this.TLB_CaseTransactions = new List<CaseTransactions>();
        }

        public int CfId { get; set; }
        public string CaseNo { get; set; }
        public string AMLCaseKey { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string RiskProfile { get; set; }
        public int UserId { get; set; }//To be updated after assign case
        public int StatusId { get; set; }
        public string DueDiligence { get; set; }
        public string MemorandumOfFact { get; set; }
        public Nullable<int> TransactionCount { get; set; }
        public Nullable<int> TransactionScore { get; set; }
        public Nullable<int> AccountCount { get; set; }
        public Nullable<int> AccountScore { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> OpenDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
        public string StatusBy { get; set; }
        public int DemoteCount { get; set; }
        public string ExecutiveSummary { get; set; }
        public int SARStatus { get; set; }
        public Nullable<int> HighStatusId { get; set; }
        public string ProjectSupervisor { get; set; }
        public string CaseReviewCommittee { get; set; }
        public string LAssesmentTypeAnalyst { get; set; }
        public string LAssesmentTypeCaseTeamLeader { get; set; }
        public string LAssesmentTypeCaseReview { get; set; }
        public string LAssesmentTypeCaseReviewTeamLeader { get; set; }
        public string LAssesmentTypeCompliance { get; set; }
        public Nullable<int> CaseCreationPart { get; set; }
        public Nullable<int> CaseCreationAssesmentId { get; set; }
        public string LAssesmentTypeSARPreparer { get; set; }
        public string NAICSCode { get; set; }
        public string NAICSName { get; set; }
        public string NAICSCategory { get; set; }
        public virtual LookUp LookUp { get; set; }
        public virtual ICollection<CaseAssesmentNote> TLB_CaseAssesmentNote { get; set; }
        public virtual ICollection<CaseTransactions> TLB_CaseTransactions { get; set; }
    }
}
