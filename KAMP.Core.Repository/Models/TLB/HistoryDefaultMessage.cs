using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class HistoryDefaultMessage
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int HistoryGroupId { get; set; }
    }
}
