using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    [KAMPObjectAttribute("TLB")]
    public partial class History
    {
        public int Id { get; set; }
        public Nullable<int> HistoryGroupId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string User { get; set; }
        public string Description { get; set; }
        public int FKey { get; set; }
        public byte[] HistoryBlob { get; set; }
        public string HistoryBlobFileName { get; set; }
    }
}
