﻿using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.TLB
{
    [KAMPObjectAttribute("TLB","Use this Entity to Set permissions to access TLB Module from Home Screen")]
    public class TransactionLookBack
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
