using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class ParameterAlias
    {
        public int Id { get; set; }
        public Nullable<int> ParameterId { get; set; }
        public string Alias { get; set; }
    }
}
