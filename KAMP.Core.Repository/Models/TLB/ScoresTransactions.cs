using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class ScoresTransactions
    {
        public ScoresTransactions()
        {
           
        }
        public int Id { get; set; }
        public Nullable<int> TransactionId { get; set; }
        public Nullable<int> AssesmentId { get; set; }
        public Nullable<int> ParameterId { get; set; }
        public string FIELD { get; set; }
        public string FilterCriteriaId { get; set; }
        public Nullable<bool> IsScored { get; set; }
        public virtual Assesments Assesments { get; set; }
        public virtual TLBParameters TLBParameters { get; set; }
        public virtual TLBTransNorm TLBTransNorm { get; set; }
    }
}
