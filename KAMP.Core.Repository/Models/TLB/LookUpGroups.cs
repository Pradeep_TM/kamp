using KAMP.Core.FrameworkComponents;
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class LookUpGroups
    {
        public LookUpGroups()
        {
            this.TLBLookUp = new List<LookUp>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<LookUp> TLBLookUp { get; set; }
    }
}
