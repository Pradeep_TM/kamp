﻿using KAMP.Core.Repository.TLB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.TLB
{
    public class FilterCriteria
    {
        public int fcr_ID { get; set; }
        public int ass_ID { get; set; }
        public string fcr_FIELD { get; set; }
        public string fcr_OPERATOR { get; set; }
        public string fcr_CRITERIA { get; set; }
        public string fcr_TYPE { get; set; }
        public int? fcr_ORDER { get; set; }
        public virtual Assesments Assesments { get; set; }
    }
}
