using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class TLBParameterGroups
    {
        public TLBParameterGroups()
        {
            this.TLB_Parameters = new List<TLBParameters>();
        }

        public int Id { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> IsIndustryGroup { get; set; }
        public Nullable<bool> IsHRG { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public virtual ICollection<TLBParameters> TLB_Parameters { get; set; }
    }
}
