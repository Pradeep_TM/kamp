using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class AssesmentStoredProcedures
    {
        public int Id { get; set; }
        public string AssesmentRuleName { get; set; }
        public string AssesmentStoredProcedureName { get; set; }
    }
}
