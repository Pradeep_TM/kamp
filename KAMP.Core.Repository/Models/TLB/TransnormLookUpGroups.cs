using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class TransnormLookUpGroups
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
