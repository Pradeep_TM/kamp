using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class ClientNameVariations
    {
        public string FieldScrubbed { get; set; }
        public Nullable<int> IsValid { get; set; }
    }
}
