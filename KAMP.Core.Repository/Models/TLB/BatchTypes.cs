using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class BatchTypes
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Score { get; set; }
        public Nullable<bool> ASSESS { get; set; }
    }
}
