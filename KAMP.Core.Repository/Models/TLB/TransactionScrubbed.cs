﻿using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class TransactionScrubbed
    {
        public int Id { get; set; }
        public string ByOrderPartyNameScrubbed { get; set; }
        public string ByOrderPartyNameNormScrubbed { get; set; }
        public string ByOrderPartyIDNameAddressScrubbed { get; set; }
        public string BenePartyNameScrubbed { get; set; }
        public string BenePartyNameNormScrubbed { get; set; }
        public string BenePartyIDNameAddressScrubbed { get; set; }
        public string CreditBankIDNameAddressScrubbed { get; set; }
        public string CreditBankAdviceScrubbed { get; set; }
        public string CreditBankInstructionsScrubbed { get; set; }
        public string IntermediaryBankIDNameAddressScrubbed { get; set; }
        public string BeneficiaryBankIDNameAddressScrubbed { get; set; }
        public string BeneficiaryBankAdviceScrubbed { get; set; }
        public string DebitBankInstructionsScrubbed { get; set; }
        public string DebitBankIDNameAddressScrubbed { get; set; }
        public string OriginatortoBeneInfoScrubbed { get; set; }
        public string OriginatingBankIDNameAddressScrubbed { get; set; }
        public string BanktoBeneInfoScrubbed { get; set; }
        public string InstructingBankIDNameAddressScrubbed { get; set; }
        public string OriginatingBankNameScrubbed { get; set; }
        public string OriginatingBankNameNormScrubbed { get; set; }
        public string InstructingBankNameScrubbed { get; set; }
        public string InstructingBankNameNormScrubbed { get; set; }
        public string DebitBankNameScrubbed { get; set; }
        public string DebitBankNameNormScrubbed { get; set; }
        public string CreditBankNameScrubbed { get; set; }
        public string CreditBankNameNormScrubbed { get; set; }
        public string IntermediaryBankNameScrubbed { get; set; }
        public string IntermediaryBankNameNormScrubbed { get; set; }
        public string BeneficiaryBankNameScrubbed { get; set; }
        public string BeneficiaryBankNameNormScrubbed { get; set; }
    }
}
