using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class CaseAssesmentNote
    {
        public int Id { get; set; }
        public Nullable<int> AssesmentId { get; set; }
        public Nullable<int> cf_Id { get; set; }
        public string Note { get; set; }
        public virtual Assesments TLBAssesments { get; set; }
        public virtual TLBCaseFile TLBCaseFile { get; set; }
    }
}
