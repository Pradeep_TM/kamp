using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class BatchSteps
    {
        public int Id { get; set; }
        public int BatchTypeId { get; set; }
        public Nullable<int> AssesmentId { get; set; }
        public string BatchStepName { get; set; }
    }
}
