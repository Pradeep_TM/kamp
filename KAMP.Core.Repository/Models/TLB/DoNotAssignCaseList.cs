﻿using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.TLB
{
    public partial class DoNotAssignCaseList
    {
        public int Id { get; set; }
        public Nullable<int> CfId { get; set; }
        public string Reason { get; set; }
    }
}
