using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class CaseTransactions
    {
        public int Id { get; set; }
        public Nullable<int> TransactionId { get; set; }
        public Nullable<int> IsClear { get; set; }
        public Nullable<int> CfId { get; set; }
        public Nullable<bool> SAR { get; set; }
        public virtual TLBCaseFile TLBCaseFile { get; set; }
        public virtual TLBTransNorm TLBTransNorm { get; set; }
    }
}
