using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class CaseRelatedTransaction
    {
        public Nullable<int> CfId { get; set; }
        public string CfName { get; set; }
        public int TransactionId { get; set; }
        public string PartyName { get; set; }
        public string PartySource { get; set; }
    }
}
