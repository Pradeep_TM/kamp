using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class ParameterAliasMap : EntityTypeConfiguration<ParameterAlias>
    {
        public ParameterAliasMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Alias)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("TLB_ParameterAlias");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ParameterId).HasColumnName("ParameterId");
            this.Property(t => t.Alias).HasColumnName("Alias");
        }
    }
}
