﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.TLB.Mapping
{
    public class FilterCriteriaMap : EntityTypeConfiguration<FilterCriteria>
    {
        public FilterCriteriaMap()
        {
            // Primary Key
            this.HasKey(t => t.fcr_ID);

            // Properties
            this.Property(t => t.fcr_FIELD)
                .HasMaxLength(100);

            this.Property(t => t.fcr_OPERATOR)
                .HasMaxLength(4);

            this.Property(t => t.fcr_CRITERIA)
                .HasMaxLength(50);

            this.Property(t => t.fcr_TYPE)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TLB_FilterCriteria");
            this.Property(t => t.fcr_ID).HasColumnName("fcr_ID");
            this.Property(t => t.ass_ID).HasColumnName("ass_ID");
            this.Property(t => t.fcr_FIELD).HasColumnName("fcr_FIELD");
            this.Property(t => t.fcr_OPERATOR).HasColumnName("fcr_OPERATOR");
            this.Property(t => t.fcr_CRITERIA).HasColumnName("fcr_CRITERIA");
            this.Property(t => t.fcr_TYPE).HasColumnName("fcr_TYPE");
            this.Property(t => t.fcr_ORDER).HasColumnName("fcr_ORDER");

            // Relationships
            this.HasRequired(t => t.Assesments)
                .WithMany(t => t.FilterCriteria)
                .HasForeignKey(d => d.ass_ID);
        }
    }
}
