using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class BatchStepsMap : EntityTypeConfiguration<BatchSteps>
    {
        public BatchStepsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.BatchStepName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TLB_BatchSteps");
            this.Property(t => t.Id).HasColumnName("ID");
            this.Property(t => t.BatchTypeId).HasColumnName("BatchTypeId");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.BatchStepName).HasColumnName("BatchStepName");
        }
    }
}
