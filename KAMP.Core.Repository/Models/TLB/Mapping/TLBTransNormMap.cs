using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class TLBTransNormMap : EntityTypeConfiguration<TLBTransNorm>
    {
        public TLBTransNormMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.InternalReferenceNumber)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.MessageType)
                .HasMaxLength(4);

            this.Property(t => t.PaymentType)
                .HasMaxLength(50);

            this.Property(t => t.PartyType)
                .HasMaxLength(15);

            this.Property(t => t.SourceCode)
                .HasMaxLength(3);

            this.Property(t => t.CreditAdviceType)
                .HasMaxLength(3);

            this.Property(t => t.MTReference)
                .HasMaxLength(50);

            this.Property(t => t.FileName)
                .HasMaxLength(50);

            this.Property(t => t.CustomerReference)
                .HasMaxLength(50);

            this.Property(t => t.CustomerAccount1)
                .HasMaxLength(50);

            this.Property(t => t.CustomerAccount2)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyID)
                .HasMaxLength(25);

            this.Property(t => t.ByOrderAcct)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyBIC)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyABA)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyName)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyAddress)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyStateName)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyID)
                .HasMaxLength(25);

            this.Property(t => t.BenePartyAcct)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyBIC)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyABA)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyName)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyAddress)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyStateName)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyCountryName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankID)
                .HasMaxLength(25);

            this.Property(t => t.CreditBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankABA)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankName)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankAdvice)
                .HasMaxLength(105);

            this.Property(t => t.CreditBankAdviceCountryName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankInstructions)
                .HasMaxLength(105);

            this.Property(t => t.CreditBankInstructionsCountryName)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankID)
                .HasMaxLength(25);

            this.Property(t => t.IntermediaryBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankABA)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankName)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankID)
                .HasMaxLength(25);

            this.Property(t => t.BeneficiaryBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankABA)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankName)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankAdvice)
                .HasMaxLength(105);

            this.Property(t => t.BeneficiaryBankAdviceCountryName)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankID)
                .HasMaxLength(25);

            this.Property(t => t.DebitBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankABA)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankName)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankInstructions)
                .HasMaxLength(105);

            this.Property(t => t.DebitBankInstructionsCountryName)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankID)
                .HasMaxLength(25);

            this.Property(t => t.OriginatingBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankABA)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankName)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.OriginatortoBeneInfo)
                .HasMaxLength(300);

            this.Property(t => t.OriginatortoBeneInfoCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BanktoBeneInfo)
                .HasMaxLength(300);

            this.Property(t => t.BankToBeneInfoCountryName)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankID)
                .HasMaxLength(25);

            this.Property(t => t.InstructingBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankABA)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankName)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankCountryName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TLB_TransNorm");
            this.Property(t => t.Id).HasColumnName("trn_Id");
            this.Property(t => t.TranNo).HasColumnName("trn_TranNo");
            this.Property(t => t.InternalReferenceNumber).HasColumnName("trn_InternalReferenceNumber");
            this.Property(t => t.MessageType).HasColumnName("trn_MessageType");
            this.Property(t => t.PaymentType).HasColumnName("trn_PaymentType");
            this.Property(t => t.PaymentAmount).HasColumnName("trn_PaymentAmount");
            this.Property(t => t.TransDate).HasColumnName("trn_TransDate");
            this.Property(t => t.ValueDate).HasColumnName("trn_ValueDate");
            this.Property(t => t.RecvPay).HasColumnName("trn_RecvPay");
            this.Property(t => t.PartyType).HasColumnName("trn_PartyType");
            this.Property(t => t.SourceCode).HasColumnName("trn_SourceCode");
            this.Property(t => t.CreditAdviceType).HasColumnName("trn_CreditAdviceType");
            this.Property(t => t.MTReference).HasColumnName("trn_MTReference");
            this.Property(t => t.FileName).HasColumnName("trn_FileName");
            this.Property(t => t.CustomerReference).HasColumnName("trn_CustomerReference");
            this.Property(t => t.CustomerAccount1).HasColumnName("trn_CustomerAccount1");
            this.Property(t => t.CustomerAccount2).HasColumnName("trn_CustomerAccount2");
            this.Property(t => t.ByOrderPartyID).HasColumnName("trn_ByOrderPartyID");
            this.Property(t => t.ByOrderAcct).HasColumnName("trn_ByOrderAcct");
            this.Property(t => t.ByOrderPartyBIC).HasColumnName("trn_ByOrderPartyBIC");
            this.Property(t => t.ByOrderPartyABA).HasColumnName("trn_ByOrderPartyABA");
            this.Property(t => t.ByOrderPartyName).HasColumnName("trn_ByOrderPartyName");
            this.Property(t => t.ByOrderPartyNameNorm).HasColumnName("trn_ByOrderPartyNameNorm");
            this.Property(t => t.ByOrderPartyAddress).HasColumnName("trn_ByOrderPartyAddress");
            this.Property(t => t.ByOrderPartyStateName).HasColumnName("trn_ByOrderPartyStateName");
            this.Property(t => t.ByOrderPartyCountryName).HasColumnName("trn_ByOrderPartyCountryName");
            this.Property(t => t.BenePartyID).HasColumnName("trn_BenePartyID");
            this.Property(t => t.BenePartyAcct).HasColumnName("trn_BenePartyAcct");
            this.Property(t => t.BenePartyBIC).HasColumnName("trn_BenePartyBIC");
            this.Property(t => t.BenePartyABA).HasColumnName("trn_BenePartyABA");
            this.Property(t => t.BenePartyName).HasColumnName("trn_BenePartyName");
            this.Property(t => t.BenePartyNameNorm).HasColumnName("trn_BenePartyNameNorm");
            this.Property(t => t.BenePartyAddress).HasColumnName("trn_BenePartyAddress");
            this.Property(t => t.BenePartyStateName).HasColumnName("trn_BenePartyStateName");
            this.Property(t => t.BenePartyCountryName).HasColumnName("trn_BenePartyCountryName");
            this.Property(t => t.CreditBankID).HasColumnName("trn_CreditBankID");
            this.Property(t => t.CreditBankAcct).HasColumnName("trn_CreditBankAcct");
            this.Property(t => t.CreditBankBIC).HasColumnName("trn_CreditBankBIC");
            this.Property(t => t.CreditBankABA).HasColumnName("trn_CreditBankABA");
            this.Property(t => t.CreditBankName).HasColumnName("trn_CreditBankName");
            this.Property(t => t.CreditBankNameNorm).HasColumnName("trn_CreditBankNameNorm");
            this.Property(t => t.CreditBankAddress).HasColumnName("trn_CreditBankAddress");
            this.Property(t => t.CreditBankStateName).HasColumnName("trn_CreditBankStateName");
            this.Property(t => t.CreditBankCountryName).HasColumnName("trn_CreditBankCountryName");
            this.Property(t => t.CreditBankAdvice).HasColumnName("trn_CreditBankAdvice");
            this.Property(t => t.CreditBankAdviceCountryName).HasColumnName("trn_CreditBankAdviceCountryName");
            this.Property(t => t.CreditBankInstructions).HasColumnName("trn_CreditBankInstructions");
            this.Property(t => t.CreditBankInstructionsCountryName).HasColumnName("trn_CreditBankInstructionsCountryName");
            this.Property(t => t.IntermediaryBankID).HasColumnName("trn_IntermediaryBankID");
            this.Property(t => t.IntermediaryBankAcct).HasColumnName("trn_IntermediaryBankAcct");
            this.Property(t => t.IntermediaryBankBIC).HasColumnName("trn_IntermediaryBankBIC");
            this.Property(t => t.IntermediaryBankABA).HasColumnName("trn_IntermediaryBankABA");
            this.Property(t => t.IntermediaryBankName).HasColumnName("trn_IntermediaryBankName");
            this.Property(t => t.IntermediaryBankNameNorm).HasColumnName("trn_IntermediaryBankNameNorm");
            this.Property(t => t.IntermediaryBankAddress).HasColumnName("trn_IntermediaryBankAddress");
            this.Property(t => t.IntermediaryBankStateName).HasColumnName("trn_IntermediaryBankStateName");
            this.Property(t => t.IntermediaryBankCountryName).HasColumnName("trn_IntermediaryBankCountryName");
            this.Property(t => t.BeneficiaryBankID).HasColumnName("trn_BeneficiaryBankID");
            this.Property(t => t.BeneficiaryBankAcct).HasColumnName("trn_BeneficiaryBankAcct");
            this.Property(t => t.BeneficiaryBankABA).HasColumnName("trn_BeneficiaryBankABA");
            this.Property(t => t.BeneficiaryBankBIC).HasColumnName("trn_BeneficiaryBankBIC");
            this.Property(t => t.BeneficiaryBankName).HasColumnName("trn_BeneficiaryBankName");
            this.Property(t => t.BeneficiaryBankNameNorm).HasColumnName("trn_BeneficiaryBankNameNorm");
            this.Property(t => t.BeneficiaryBankAddress).HasColumnName("trn_BeneficiaryBankAddress");
            this.Property(t => t.BeneficiaryBankStateName).HasColumnName("trn_BeneficiaryBankStateName");
            this.Property(t => t.BeneficiaryBankCountryName).HasColumnName("trn_BeneficiaryBankCountryName");
            this.Property(t => t.BeneficiaryBankAdvice).HasColumnName("trn_BeneficiaryBankAdvice");
            this.Property(t => t.BeneficiaryBankAdviceCountryName).HasColumnName("trn_BeneficiaryBankAdviceCountryName");
            this.Property(t => t.DebitBankID).HasColumnName("trn_DebitBankID");
            this.Property(t => t.DebitBankAcct).HasColumnName("trn_DebitBankAcct");
            this.Property(t => t.DebitBankBIC).HasColumnName("trn_DebitBankBIC");
            this.Property(t => t.DebitBankABA).HasColumnName("trn_DebitBankABA");
            this.Property(t => t.DebitBankName).HasColumnName("trn_DebitBankName");
            this.Property(t => t.DebitBankNameNorm).HasColumnName("trn_DebitBankNameNorm");
            this.Property(t => t.DebitBankAddress).HasColumnName("trn_DebitBankAddress");
            this.Property(t => t.DebitBankStateName).HasColumnName("trn_DebitBankStateName");
            this.Property(t => t.DebitBankCountryName).HasColumnName("trn_DebitBankCountryName");
            this.Property(t => t.DebitBankInstructions).HasColumnName("trn_DebitBankInstructions");
            this.Property(t => t.DebitBankInstructionsCountryName).HasColumnName("trn_DebitBankInstructionsCountryName");
            this.Property(t => t.OriginatingBankID).HasColumnName("trn_OriginatingBankID");
            this.Property(t => t.OriginatingBankAcct).HasColumnName("trn_OriginatingBankAcct");
            this.Property(t => t.OriginatingBankBIC).HasColumnName("trn_OriginatingBankBIC");
            this.Property(t => t.OriginatingBankABA).HasColumnName("trn_OriginatingBankABA");
            this.Property(t => t.OriginatingBankName).HasColumnName("trn_OriginatingBankName");
            this.Property(t => t.OriginatingBankNameNorm).HasColumnName("trn_OriginatingBankNameNorm");
            this.Property(t => t.OriginatingBankAddress).HasColumnName("trn_OriginatingBankAddress");
            this.Property(t => t.OriginatingBankStateName).HasColumnName("trn_OriginatingBankStateName");
            this.Property(t => t.OriginatingBankCountryName).HasColumnName("trn_OriginatingBankCountryName");
            this.Property(t => t.OriginatortoBeneInfo).HasColumnName("trn_OriginatortoBeneInfo");
            this.Property(t => t.OriginatortoBeneInfoCountryName).HasColumnName("trn_OriginatortoBeneInfoCountryName");
            this.Property(t => t.BanktoBeneInfo).HasColumnName("trn_BanktoBeneInfo");
            this.Property(t => t.BankToBeneInfoCountryName).HasColumnName("trn_BankToBeneInfoCountryName");
            this.Property(t => t.InstructingBankID).HasColumnName("trn_InstructingBankID");
            this.Property(t => t.InstructingBankAcct).HasColumnName("trn_InstructingBankAcct");
            this.Property(t => t.InstructingBankBIC).HasColumnName("trn_InstructingBankBIC");
            this.Property(t => t.InstructingBankABA).HasColumnName("trn_InstructingBankABA");
            this.Property(t => t.InstructingBankName).HasColumnName("trn_InstructingBankName");
            this.Property(t => t.InstructingBankNameNorm).HasColumnName("trn_InstructingBankNameNorm");
            this.Property(t => t.InstructingBankAddress).HasColumnName("trn_InstructingBankAddress");
            this.Property(t => t.InstructingBankStateName).HasColumnName("trn_InstructingBankStateName");
            this.Property(t => t.InstructingBankCountryName).HasColumnName("trn_InstructingBankCountryName");
            this.Property(t => t.CaseCount).HasColumnName("trn_CaseCount");
            this.Property(t => t.CaseCheck).HasColumnName("trn_CaseCheck");
        }
    }
}
