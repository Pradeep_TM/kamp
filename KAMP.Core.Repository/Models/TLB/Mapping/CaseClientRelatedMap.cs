using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class CaseClientRelatedMap : EntityTypeConfiguration<CaseClientRelated>
    {
        public CaseClientRelatedMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.TransactionPartyName)
                .HasMaxLength(150);

            //this.Property(t => t.IsClient)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TLB_CaseClientRelated");
            this.Property(t => t.cfId).HasColumnName("cfId");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.cfId2).HasColumnName("cfId2");
            this.Property(t => t.TransactionPartyName).HasColumnName("TransactionPartyName");
            this.Property(t => t.IsClient).HasColumnName("IsClient");
        }
    }
}
