using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class TLBParameterGroupsMap : EntityTypeConfiguration<TLBParameterGroups>
    {
        public TLBParameterGroupsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.GroupName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TLB_ParameterGroups");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.IsIndustryGroup).HasColumnName("IsIndustryGroup");
            this.Property(t => t.IsHRG).HasColumnName("IsHRG");
            this.Property(t => t.LastUpdate).HasColumnName("LastUpdate");
        }
    }
}
