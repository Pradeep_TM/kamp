using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class CaseTransactionsMap : EntityTypeConfiguration<CaseTransactions>
    {
        public CaseTransactionsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TLB_CaseTransactions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.IsClear).HasColumnName("IsClear");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.SAR).HasColumnName("SAR");

            // Relationships
            this.HasOptional(t => t.TLBCaseFile)
                .WithMany(t => t.TLB_CaseTransactions)
                .HasForeignKey(d => d.CfId);
            this.HasOptional(t => t.TLBTransNorm)
                .WithMany(t => t.TLB_CaseTransactions)
                .HasForeignKey(d => d.TransactionId);

        }
    }
}
