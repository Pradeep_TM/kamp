using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class TransnormLookUpMap : EntityTypeConfiguration<TransnormLookUp>
    {
        public TransnormLookUpMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Reference)
                .HasMaxLength(220);

            // Table & Column Mappings
            this.ToTable("TLB_TransnormLookUp");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransnormLookUpGrpupId).HasColumnName("TransnormLookUpGrpupId");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Reference).HasColumnName("Reference");
        }
    }
}
