using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class AssesmentStoredProceduresMap : EntityTypeConfiguration<AssesmentStoredProcedures>
    {
        public AssesmentStoredProceduresMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.AssesmentRuleName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.AssesmentStoredProcedureName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TLB_AssesmentStoredProcedures");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AssesmentRuleName).HasColumnName("AssesmentRuleName");
            this.Property(t => t.AssesmentStoredProcedureName).HasColumnName("AssesmentStoredProcedureName");
        }
    }
}
