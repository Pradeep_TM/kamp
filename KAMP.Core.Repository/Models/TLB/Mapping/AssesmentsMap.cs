using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class AssesmentsMap : EntityTypeConfiguration<Assesments>
    {
        public AssesmentsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(255);

            this.Property(t => t.BusinessRule)
                .HasMaxLength(800);

            this.Property(t => t.DescriptionOriginal)
                .HasMaxLength(255);

            this.Property(t => t.StoredProcedureName)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TLB_Assesments");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AssesmentTypeId).HasColumnName("AssesmentTypeId");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Detail).HasColumnName("Detail");
            this.Property(t => t.AccountScore).HasColumnName("AccountScore");
            this.Property(t => t.TransactionScore).HasColumnName("TransactionScore");
            this.Property(t => t.Compiled).HasColumnName("Compiled");
            this.Property(t => t.Approved).HasColumnName("Approved");
            this.Property(t => t.AssessmentStoredProcedure_ID).HasColumnName("AssessmentStoredProcedure_ID");
            this.Property(t => t.RanOn).HasColumnName("RanOn");
            this.Property(t => t.BusinessRule).HasColumnName("BusinessRule");
            this.Property(t => t.IsUsedInOneMonth).HasColumnName("IsUsedInOneMonth");
            this.Property(t => t.IsUsedInLookBack).HasColumnName("IsUsedInLookBack");
            this.Property(t => t.LastUpdateDate).HasColumnName("LastUpdateDate");
            this.Property(t => t.DescriptionOriginal).HasColumnName("DescriptionOriginal");
            this.Property(t => t.StoredProcedureName).HasColumnName("StoredProcedureName");

            // Relationships
            this.HasRequired(t => t.TLBAssesmentType)
                .WithMany(t => t.TLBAssesments)
                .HasForeignKey(d => d.AssesmentTypeId);

        }
    }
}
