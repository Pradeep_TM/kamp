using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class HistoryDefaultMessageMap : EntityTypeConfiguration<HistoryDefaultMessage>
    {
        public HistoryDefaultMessageMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Description)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("TLB_HistoryDefaultMessage");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.HistoryGroupId).HasColumnName("HistoryGroupId");
        }
    }
}
