using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class LookUpMap : EntityTypeConfiguration<LookUp>
    {
        public LookUpMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Reference)
                .HasMaxLength(220);

            this.Property(t => t.OldValue)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("TLB_LookUp");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LookGroupId).HasColumnName("LookGroupId");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Reference).HasColumnName("Reference");
            this.Property(t => t.OldValue).HasColumnName("OldValue");

            // Relationships
            this.HasRequired(t => t.TLBLookUpGroups)
                .WithMany(t => t.TLBLookUp)
                .HasForeignKey(d => d.LookGroupId);

        }
    }
}
