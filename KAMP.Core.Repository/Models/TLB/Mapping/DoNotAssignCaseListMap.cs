﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.TLB
{
    public class DoNotAssignCaseListMap : EntityTypeConfiguration<DoNotAssignCaseList>
    {
        public DoNotAssignCaseListMap()
        {
            HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Reason)
                .IsRequired()
                .HasMaxLength(200);
            

            // Table & Column Mappings
            this.ToTable("TLB_DoNotAssignCaseList");
            this.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.Reason).HasColumnName("Reason");
        }
    }
}
