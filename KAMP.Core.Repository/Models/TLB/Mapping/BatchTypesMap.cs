using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class BatchTypesMap : EntityTypeConfiguration<BatchTypes>
    {
        public BatchTypesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TLB_BatchTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Score).HasColumnName("Score");
            this.Property(t => t.ASSESS).HasColumnName("ASSESS");
        }
    }
}
