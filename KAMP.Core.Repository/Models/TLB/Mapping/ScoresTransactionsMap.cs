using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class ScoresTransactionsMap : EntityTypeConfiguration<ScoresTransactions>
    {
        public ScoresTransactionsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FIELD)
                .HasMaxLength(50);

            this.Property(t => t.FilterCriteriaId)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TLB_ScoresTransactions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.ParameterId).HasColumnName("ParameterId");
            this.Property(t => t.FIELD).HasColumnName("FIELD");
            this.Property(t => t.FilterCriteriaId).HasColumnName("FilterCriteriaId");
            this.Property(t => t.IsScored).HasColumnName("IsScored");

            // Relationships
            this.HasOptional(t => t.Assesments)
                .WithMany(t => t.TLB_ScoresTransactions)
                .HasForeignKey(d => d.AssesmentId);
            this.HasOptional(t => t.TLBParameters)
                .WithMany(t => t.ScoresTransactions)
                .HasForeignKey(d => d.ParameterId);
            this.HasOptional(t => t.TLBTransNorm)
                .WithMany(t => t.TLB_ScoresTransactions)
                .HasForeignKey(d => d.TransactionId);

        }
    }
}
