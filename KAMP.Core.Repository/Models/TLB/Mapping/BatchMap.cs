using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class BatchMap : EntityTypeConfiguration<Batch>
    {
        public BatchMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.StartDate)
                .IsRequired()
                .HasMaxLength(12);

            this.Property(t => t.StartTime)
                .IsRequired()
                .HasMaxLength(8);

            this.Property(t => t.EndTime)
                .HasMaxLength(8);

            this.Property(t => t.Status)
                .HasMaxLength(50);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ServerJobId)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("TLB_Batch");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.StartTime).HasColumnName("StartTime");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.EndTime).HasColumnName("EndTime");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.Approved).HasColumnName("Approved");
            this.Property(t => t.ServerJobId).HasColumnName("ServerJobId");
        }
    }
}
