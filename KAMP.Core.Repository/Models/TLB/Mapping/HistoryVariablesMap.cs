using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class HistoryVariablesMap : EntityTypeConfiguration<HistoryVariables>
    {
        public HistoryVariablesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FKeyTable)
                .HasMaxLength(50);

            this.Property(t => t.FKeyField)
                .HasMaxLength(50);

            this.Property(t => t.MessField)
                .HasMaxLength(50);

            this.Property(t => t.MessValue)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TLB_HistoryVariables");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FKeyTable).HasColumnName("FKeyTable");
            this.Property(t => t.FKeyField).HasColumnName("FKeyField");
            this.Property(t => t.FKeyValue).HasColumnName("FKeyValue");
            this.Property(t => t.MessField).HasColumnName("MessField");
            this.Property(t => t.MessValue).HasColumnName("MessValue");
            this.Property(t => t.HistoryId).HasColumnName("HistoryId");
        }
    }
}
