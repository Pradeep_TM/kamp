using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class TransTotalScoreMap : EntityTypeConfiguration<TransTotalScore>
    {
        public TransTotalScoreMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Table & Column Mappings
            this.ToTable("TLB_TransTotalScore");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TotalScore).HasColumnName("trn_TotalScore");
            this.Property(t => t.TransId).HasColumnName("TransId");

            // Relationships
            this.HasOptional(t => t.TransNorm)
                .WithMany(t => t.TransTotalScore)
                .HasForeignKey(d => d.TransId);
        }
    }
}
