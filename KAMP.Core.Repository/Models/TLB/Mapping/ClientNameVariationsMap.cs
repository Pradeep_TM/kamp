using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class ClientNameVariationsMap : EntityTypeConfiguration<ClientNameVariations>
    {
        public ClientNameVariationsMap()
        {
            // Primary Key
            this.HasKey(t => t.FieldScrubbed);

            // Properties
            this.Property(t => t.FieldScrubbed)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TLB_ClientNameVariations");
            this.Property(t => t.FieldScrubbed).HasColumnName("FieldScrubbed");
            this.Property(t => t.IsValid).HasColumnName("IsValid");
        }
    }
}
