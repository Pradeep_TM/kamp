using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class TLBParametersMap : EntityTypeConfiguration<TLBParameters>
    {
        public TLBParametersMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ActualValue)
                .HasMaxLength(150);

            this.Property(t => t.Alias)
                .HasMaxLength(85);

            this.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(85);

            this.Property(t => t.SourceKey)
                .HasMaxLength(250);

            this.Property(t => t.ValueNoSpace)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TLB_Parameters");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ParameterGroupID).HasColumnName("ParameterGroupID");
            this.Property(t => t.ActualValue).HasColumnName("ActualValue");
            this.Property(t => t.Alias).HasColumnName("Alias");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.SourceKey).HasColumnName("SourceKey");
            this.Property(t => t.ContainstSpaceEachSide).HasColumnName("ContainstSpaceEachSide");
            this.Property(t => t.ValueNoSpace).HasColumnName("ValueNoSpace");
            this.Property(t => t.IsAddSAR).HasColumnName("IsAddSAR");

            // Relationships
            this.HasRequired(t => t.TLBParameterGroups)
                .WithMany(t => t.TLB_Parameters)
                .HasForeignKey(d => d.ParameterGroupID);

        }
    }
}
