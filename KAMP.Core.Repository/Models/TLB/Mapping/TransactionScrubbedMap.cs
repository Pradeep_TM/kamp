﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.TLB
{
    public class TransactionScrubbedMap : EntityTypeConfiguration<TransactionScrubbed>
    {
        public TransactionScrubbedMap()  
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ByOrderPartyNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyNameNormScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.BenePartyNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyNameNormScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.CreditBankIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.CreditBankAdviceScrubbed)
                .HasMaxLength(105);

            this.Property(t => t.CreditBankInstructionsScrubbed)
                .HasMaxLength(105);

            this.Property(t => t.IntermediaryBankIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.BeneficiaryBankIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.BeneficiaryBankAdviceScrubbed)
                .HasMaxLength(105);

            this.Property(t => t.DebitBankInstructionsScrubbed)
                .HasMaxLength(105);

            this.Property(t => t.DebitBankIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.OriginatortoBeneInfoScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.BanktoBeneInfoScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.InstructingBankIDNameAddressScrubbed)
                .HasMaxLength(300);

            this.Property(t => t.OriginatingBankNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankNameNormScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankNameNormScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankNameNormScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankNameNormScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankNameNormScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankNameScrubbed)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankNameNormScrubbed)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("TLB_TransactionScrubbed");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ByOrderPartyNameScrubbed).HasColumnName("ByOrderPartyNameScrubbed");
            this.Property(t => t.ByOrderPartyNameNormScrubbed).HasColumnName("ByOrderPartyNameNormScrubbed");
            this.Property(t => t.ByOrderPartyIDNameAddressScrubbed).HasColumnName("ByOrderPartyIDNameAddressScrubbed");
            this.Property(t => t.BenePartyNameScrubbed).HasColumnName("BenePartyNameScrubbed");
            this.Property(t => t.BenePartyNameNormScrubbed).HasColumnName("BenePartyNameNormScrubbed");
            this.Property(t => t.BenePartyIDNameAddressScrubbed).HasColumnName("BenePartyIDNameAddressScrubbed");
            this.Property(t => t.CreditBankIDNameAddressScrubbed).HasColumnName("CreditBankIDNameAddressScrubbed");
            this.Property(t => t.CreditBankAdviceScrubbed).HasColumnName("CreditBankAdviceScrubbed");
            this.Property(t => t.CreditBankInstructionsScrubbed).HasColumnName("CreditBankInstructionsScrubbed");
            this.Property(t => t.IntermediaryBankIDNameAddressScrubbed).HasColumnName("IntermediaryBankIDNameAddressScrubbed");
            this.Property(t => t.BeneficiaryBankIDNameAddressScrubbed).HasColumnName("BeneficiaryBankIDNameAddressScrubbed");
            this.Property(t => t.BeneficiaryBankAdviceScrubbed).HasColumnName("BeneficiaryBankAdviceScrubbed");
            this.Property(t => t.DebitBankInstructionsScrubbed).HasColumnName("DebitBankInstructionsScrubbed");
            this.Property(t => t.DebitBankIDNameAddressScrubbed).HasColumnName("DebitBankIDNameAddressScrubbed");
            this.Property(t => t.OriginatortoBeneInfoScrubbed).HasColumnName("OriginatortoBeneInfoScrubbed");
            this.Property(t => t.OriginatingBankIDNameAddressScrubbed).HasColumnName("OriginatingBankIDNameAddressScrubbed");
            this.Property(t => t.BanktoBeneInfoScrubbed).HasColumnName("BanktoBeneInfoScrubbed");
            this.Property(t => t.InstructingBankIDNameAddressScrubbed).HasColumnName("InstructingBankIDNameAddressScrubbed");
            this.Property(t => t.OriginatingBankNameScrubbed).HasColumnName("OriginatingBankNameScrubbed");
            this.Property(t => t.OriginatingBankNameNormScrubbed).HasColumnName("OriginatingBankNameNormScrubbed");
            this.Property(t => t.InstructingBankNameScrubbed).HasColumnName("InstructingBankNameScrubbed");
            this.Property(t => t.InstructingBankNameNormScrubbed).HasColumnName("InstructingBankNameNormScrubbed");
            this.Property(t => t.DebitBankNameScrubbed).HasColumnName("DebitBankNameScrubbed");
            this.Property(t => t.DebitBankNameNormScrubbed).HasColumnName("DebitBankNameNormScrubbed");
            this.Property(t => t.CreditBankNameScrubbed).HasColumnName("CreditBankNameScrubbed");
            this.Property(t => t.CreditBankNameNormScrubbed).HasColumnName("CreditBankNameNormScrubbed");
            this.Property(t => t.IntermediaryBankNameScrubbed).HasColumnName("IntermediaryBankNameScrubbed");
            this.Property(t => t.IntermediaryBankNameNormScrubbed).HasColumnName("IntermediaryBankNameNormScrubbed");
            this.Property(t => t.BeneficiaryBankNameScrubbed).HasColumnName("BeneficiaryBankNameScrubbed");
            this.Property(t => t.BeneficiaryBankNameNormScrubbed).HasColumnName("BeneficiaryBankNameNormScrubbed");
        }
    }
}
