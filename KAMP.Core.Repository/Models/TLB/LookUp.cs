using KAMP.Core.FrameworkComponents;
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    [KAMPObjectAttribute("TLB", "Use this entity to give permission to LookUp screen.")]
    [LogEntityAttribute("TLB","Configure this entity to track changes done in look ups.")]
    public partial class LookUp
    {
        public int Id { get; set; }
        public int LookGroupId { get; set; }
        public string Value { get; set; }
        public string Reference { get; set; }
        public string OldValue { get; set; }
        public virtual LookUpGroups TLBLookUpGroups { get; set; }
        public virtual ICollection<TLBCaseFile> TLBCaseFile { get; set; }
    }
}
