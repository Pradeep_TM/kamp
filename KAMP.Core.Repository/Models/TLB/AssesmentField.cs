using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class AssesmentField
    {
        public int Id { get; set; }
        public int AssesmentId { get; set; }
        public string FieldName { get; set; }
        public Nullable<int> parameterGroupId { get; set; }
        public string FieldType { get; set; }
    }
}
