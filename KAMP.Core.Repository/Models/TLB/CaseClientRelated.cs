using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class CaseClientRelated
    {
        public int Id { get; set; }
        public Nullable<int> cfId { get; set; }
        public Nullable<int> cfId2 { get; set; }
        public string TransactionPartyName { get; set; }
        public int IsClient { get; set; }
    }
}
