using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class HistoryGroups
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
