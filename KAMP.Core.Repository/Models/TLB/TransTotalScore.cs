using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class TransTotalScore
    {
        public int Id { get; set; }
        public Nullable<int> TotalScore { get; set; }
        public Nullable<int> TransId { get; set; }
        public virtual TLBTransNorm TransNorm { get; set; }
    }
}
