using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class ClientAccounts
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string AccountStatus { get; set; }
        public string Account { get; set; }
        public Nullable<System.DateTime> AccountCLSDate { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerCode { get; set; }
        public string Address { get; set; }
    }
}
