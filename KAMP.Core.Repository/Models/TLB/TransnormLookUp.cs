using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    [KAMPObjectAttribute("TLB")]
    public partial class TransnormLookUp
    {
        public int Id { get; set; }
        public int TransnormLookUpGrpupId { get; set; }
        public string Value { get; set; }
        public string Reference { get; set; }
    }
}
