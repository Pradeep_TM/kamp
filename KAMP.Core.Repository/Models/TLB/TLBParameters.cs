using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    [KAMPObjectAttribute("TLB","Use this entity to set permission on Risk Parameter screen.")]
    public partial class TLBParameters
    {
        public TLBParameters()
        {
            //this.ScoresTransactions = new List<ScoresTransactions>();
        }

        public int Id { get; set; }
        public int ParameterGroupID { get; set; }
        public string ActualValue { get; set; }
        public string Alias { get; set; }
        public string Value { get; set; }
        public string SourceKey { get; set; }
        public Nullable<bool> ContainstSpaceEachSide { get; set; }
        public string ValueNoSpace { get; set; }
        public Nullable<bool> IsAddSAR { get; set; }
        public virtual TLBParameterGroups TLBParameterGroups { get; set; }
        public virtual ICollection<ScoresTransactions> ScoresTransactions { get; set; }
    }
}
