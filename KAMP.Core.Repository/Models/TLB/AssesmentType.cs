using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.TLB
{
    public partial class AssesmentType
    {
        public AssesmentType()
        {
            this.TLBAssesments = new List<Assesments>();
        }

        public int Id { get; set; }
        public string AssesmentTypeDescription { get; set; }
        public virtual ICollection<Assesments> TLBAssesments { get; set; }
    }
}
