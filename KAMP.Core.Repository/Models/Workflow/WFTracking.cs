using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.WF
{
    public partial class WFTracking
    {
        public int Id { get; set; }
        public int ModuleTypeId { get; set; }
        public long ObjectId { get; set; }
        public long AssignmentId { get; set; }
        public Nullable<long> CFId { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual Assignment Assignment { get; set; }
    }
}
