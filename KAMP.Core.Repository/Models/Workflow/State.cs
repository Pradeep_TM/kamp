using System;
using System.Collections.Generic;
namespace KAMP.Core.Repository.WF
{
    using KAMP.Core.Interfaces.Models;
    using KAMP.Core.Repository.UM;
    [KAMPObjectAttribute("Common", "Use this entity to give permission to Workflow Manage State screen.")]
    public partial class State
    {
        public State()
        {
            //this.Activity = new List<Activity>();
            this.Groups = new List<Group>();
        }

        public int Id { get; set; }
        public string StateName { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int ModuleTypeId { get; set; }
        public int StateTypeId { get; set; }
        public virtual ICollection<Activity> Activity { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
        public virtual StateMetadata StateMetadata { get; set; }
        public string GroupName { get; set; }
    }
}
