using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class AssignedToMap : EntityTypeConfiguration<AssignedTo>
    {
        public AssignedToMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.WorkflowStatus)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.State)
                .HasMaxLength(50);

            this.Property(t => t.StepName)
                            .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("WF_AssignedTo");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("ToUserId");
            this.Property(t => t.FromState).HasColumnName("FromState");
            this.Property(t => t.State).HasColumnName("ToState");
            this.Property(t => t.WorkflowStatus).HasColumnName("WorkflowStatus");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.StepName).HasColumnName("StepName");
            this.Property(t => t.ActionId).HasColumnName("ActionId");         
            this.Property(t => t.OpenDate).HasColumnName("OpenDate");
            this.Property(t => t.ParkDate).HasColumnName("ParkDate");
            this.Property(t => t.UnParkDate).HasColumnName("UnParkDate");
            this.Property(t => t.ParkReason).HasColumnName("ParkReason");
            this.Property(t => t.IsPark).HasColumnName("IsPark");
            this.Property(t => t.AssignmentId).HasColumnName("AssignmentId");
            this.Property(t => t.ActionId).HasColumnName("ActionId");
            this.Property(t => t.Sequence).HasColumnName("Sequence");
            this.Property(t => t.StepSequenceNo).HasColumnName("StepSequenceNo");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            // Relationships
            this.HasRequired(t => t.Assignment)
                .WithMany(t => t.AssignedTo)
                .HasForeignKey(d => d.AssignmentId);

        }
    }
}
