﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class StateMetadataMap : EntityTypeConfiguration<StateMetadata>
    {
        public StateMetadataMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.StateType)
                .IsOptional();

            // Table & Column Mappings
            this.ToTable("WF_StateMetadata");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.StateType).HasColumnName("StateType");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

        }
    }
}
