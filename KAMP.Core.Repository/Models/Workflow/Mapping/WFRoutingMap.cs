using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class WFRoutingMap : EntityTypeConfiguration<WFRouting>
    {
        public WFRoutingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("WF_Routing");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FromStateId).HasColumnName("FromStateId");
            this.Property(t => t.ToStateId).HasColumnName("ToStateId");
            this.Property(t => t.ActionId).HasColumnName("ActionId");
            this.Property(t => t.ActivityId).HasColumnName("ActivityId");
            this.Property(t => t.ToActivityId).HasColumnName("ToActivityId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");

            // Relationships
            this.HasRequired(t => t.Activity)
                .WithMany(t => t.WFRouting)
                .HasForeignKey(d => d.ActivityId);
            // Relationships
            this.HasRequired(t => t.Step)
                .WithMany(t => t.WFRouting)
                .HasForeignKey(d => d.StepId).WillCascadeOnDelete(false);

        }
    }
}
