using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class ActivityMap : EntityTypeConfiguration<Activity>
    {
        public ActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.MailTemplate)
                .IsOptional();

            // Table & Column Mappings
            this.ToTable("WF_Activity");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ToStateId).HasColumnName("StateId");
            this.Property(t => t.MailTemplate).HasColumnName("MailTemplate");
            this.Property(t => t.StepId).HasColumnName("StepId");
            this.Property(t => t.ActionId).HasColumnName("ActionId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
  
            // Relationships
            this.HasRequired(t => t.State)
                .WithMany(t => t.Activity)
                .HasForeignKey(d => d.ToStateId);
            this.HasRequired(t => t.Step)
                .WithMany(t => t.Activity)
                .HasForeignKey(d => d.StepId);

        }
    }
}
