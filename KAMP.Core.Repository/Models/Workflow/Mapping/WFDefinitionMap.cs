using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class WFDefinitionMap : EntityTypeConfiguration<WFDefinition>
    {
        public WFDefinitionMap()
        {
            // Primary Key
            this.HasKey(t => t.WFId);

            // Properties
            this.Property(t => t.WFName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WorkFlowDefinition)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("WF_Definition");
            this.Property(t => t.WFId).HasColumnName("WFId");
            this.Property(t => t.WFName).HasColumnName("WFName");
            this.Property(t => t.WorkFlowDefinition).HasColumnName("WorkFlowDefinition");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.ModuleType)
                .WithMany(t => t.WFDefinitions)
                .HasForeignKey(d => d.ModuleTypeId);

        }
    }
}
