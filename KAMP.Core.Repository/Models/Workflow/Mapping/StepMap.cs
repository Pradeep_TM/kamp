using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class StepMap : EntityTypeConfiguration<Step>
    {
        public StepMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.StepName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.State)
                .IsRequired()
                .HasMaxLength(50);


            this.Property(t => t.DisplayName)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("WF_Steps");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.WFId).HasColumnName("WFId");
            this.Property(t => t.StepName).HasColumnName("StepName");
            this.Property(t => t.DisplayName).HasColumnName("DisplayName");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.SequenceNo).HasColumnName("SequenceNo");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.WFDefinition)
                .WithMany(t => t.Steps)
                .HasForeignKey(d => d.WFId);

        }
    }
}
