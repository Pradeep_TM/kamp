using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class WFTrackingMap : EntityTypeConfiguration<WFTracking>
    {
        public WFTrackingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("WF_WorkflowTracking");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.ObjectId).HasColumnName("ObjectId");
            this.Property(t => t.AssignmentId).HasColumnName("AssignmentId");
            this.Property(t => t.CFId).HasColumnName("CFId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.Assignment)
                .WithMany(t => t.WFTracking)
                .HasForeignKey(d => d.AssignmentId);

        }
    }
}
