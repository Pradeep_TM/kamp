using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class StateMap : EntityTypeConfiguration<State>
    {
        public StateMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.StateName)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("WF_States");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.StateName).HasColumnName("StateName");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.StateTypeId).HasColumnName("StateTypeId");

            //ignore properties

            this.Ignore(t => t.GroupName);

            // Relationships
            this.HasMany(t => t.Groups)
                .WithMany(t => t.States)
                .Map(m =>
                    {
                        m.ToTable("GroupStateMapping");
                        m.MapLeftKey("StateId");
                        m.MapRightKey("GroupId");
                    });

            this.HasRequired(t => t.StateMetadata)
               .WithMany(t => t.States)
               .HasForeignKey(d => d.StateTypeId);

        }
    }
}
