using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.WF
{
    public class AssignmentMap : EntityTypeConfiguration<Assignment>
    {
        public AssignmentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CaseNo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Status)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.NextState)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("WF_Assignment");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CaseNo).HasColumnName("CaseNo");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.NextState).HasColumnName("NextState");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}
