using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.WF
{
    
    public partial class Step
    {
        public Step()
        {
            this.Activity = new List<Activity>();
        }

        public int Id { get; set; }
        public int WFId { get; set; }
        public string StepName { get; set; }
        public string DisplayName { get; set; }
        public string State { get; set; }
        public int SequenceNo { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<Activity> Activity { get; set; }
        public virtual WFDefinition WFDefinition { get; set; }
        public virtual ICollection<WFRouting> WFRouting { get; set; }
    }
}
