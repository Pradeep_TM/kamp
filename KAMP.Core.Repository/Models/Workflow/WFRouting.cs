using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.WF
{
    [KAMPObjectAttribute("Common", "Use this entity to give permission to Workflow Routing screen.")]
    public partial class WFRouting
    {
        public int Id { get; set; }
        public int FromStateId { get; set; }
        public int ToStateId { get; set; }
        public int ActivityId { get; set; }
        public int ActionId { get; set; }
        public int ToActivityId { get; set; }
        public int StepId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public virtual Activity Activity { get; set; }
        public virtual Step Step { get; set; }
    }
}
