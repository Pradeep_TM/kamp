using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.WF
{
    public partial class Activity
    {
        public int Id { get; set; }
        public int ToStateId { get; set; }
        public string MailTemplate { get; set; }
        public int StepId { get; set; }
        public int ActionId { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual State State { get; set; }
        public virtual Step Step { get; set; }
        public virtual ICollection<WFRouting> WFRouting { get; set; }
    }
}
