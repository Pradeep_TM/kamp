using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.WF
{
    [KAMPObjectAttribute("Common", "Use this entity to give permission to Workflow screen.")]
    public partial class WFDefinition
    {
        public WFDefinition()
        {
            this.Steps = new List<Step>();
        }

        public int WFId { get; set; }
        public string WFName { get; set; }
        public string WorkFlowDefinition { get; set; }
        public int ModuleTypeId { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual Module ModuleType { get; set; }
        public virtual ICollection<Step> Steps { get; set; }
    }
}
