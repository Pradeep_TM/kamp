using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.WF
{
    [KAMPObjectAttribute("Common", "Use this entity to give permission to Workflow Monitoring screen.")]
    public partial class Assignment
    {
        public Assignment()
        {
            this.AssignedTo = new List<AssignedTo>();
            this.WFTracking = new List<WFTracking>();
        }

        public long Id { get; set; }
        public string CaseNo { get; set; }
        public string Status { get; set; }
        public string NextState { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<AssignedTo> AssignedTo { get; set; }
        public virtual ICollection<WFTracking> WFTracking { get; set; }
    }
}
