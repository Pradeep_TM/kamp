﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.WF
{
    using KAMP.Core.Interfaces.Models;
    public partial class StateMetadata
    {
        public StateMetadata()
        {
            this.States = new List<State>();
        }

        public int Id { get; set; }
        public string StateType { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<State> States { get; set; }
    }

}
