using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.WF
{
    public partial class AssignedTo
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string FromState { get; set; }
        public string State { get; set; }
        public string Comments { get; set; }
        public string WorkflowStatus { get; set; }
        public string StepName { get; set; }
        public int ActionId { get; set; } 
        public Nullable<System.DateTime> OpenDate { get; set; }
        public Nullable<System.DateTime> ParkDate { get; set; }
        public Nullable<System.DateTime> UnParkDate { get; set; }
        public bool IsPark { get; set; }
        public string ParkReason { get; set; }


        #region Used internally in code
        public long AssignmentId { get; set; }
        public int ActivityId { get; set; }
        #endregion

        #region Used by lokesh in WF monitoring
        public int Sequence { get; set; }
        public int StepSequenceNo { get; set; } 
        #endregion       

        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual Assignment Assignment { get; set; }
       

    }
}
