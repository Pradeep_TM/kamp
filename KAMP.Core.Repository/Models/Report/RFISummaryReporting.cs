﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.Report
{
   public class RFISummaryReporting
    {
        public int Id { get; set; }
        public string CaseNumber { get; set; }
        public string SentTo { get; set; }
        public string SentFrom { get; set; }
        public string Subject { get; set; }
        public string SentDate { get; set; }
        public string Body { get; set; }
    }
}
