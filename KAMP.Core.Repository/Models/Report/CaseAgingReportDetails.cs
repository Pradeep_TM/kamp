﻿using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Report
{
    [KAMPObjectAttribute("Common", "Use this entity to give permission to Reporting Screen.")]
   public class CaseAgingReportDetails
    {
       public string CaseNumber { get; set; }
       public string Opened { get; set; }
       public string CaseLevel { get; set; }
       public string CurrentReviewer { get; set; }
       public string CaseStatus { get; set; }
       public string DaysAssigned { get; set; }
       public string DaysOpen { get; set; }
       public string ClassifiedByDays { get; set; }
       public string WorkflowStatus { get; set; }
      
    }

}
