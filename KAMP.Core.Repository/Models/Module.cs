using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository
{
    using UM;
    using WF;
    public partial class Module
    {
        public Module()
        {

        }

        public int ModuleTypeId { get; set; }
        public string ModuleName { get; set; }
        public bool IsActive { get; set; }
        //public virtual ICollection<ModuleValidationTypeMapping> ModuleValidationTypeMapping { get; set; }
        public virtual ICollection<ModuleObjects> ModuleObjects { get; set; }
        public virtual ICollection<RoleInPermission> RoleInPermission { get; set; }
        public virtual ICollection<UserInGroup> UserInGroup { get; set; }
        public virtual ICollection<WFDefinition> WFDefinitions { get; set; }
    }
}
