using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class Clients
    {
        public long Id { get; set; }
        public string ClientName { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Routine> Routine { get; set; }
        public virtual ICollection<Engagement> Engagement { get; set; }
    }
}
