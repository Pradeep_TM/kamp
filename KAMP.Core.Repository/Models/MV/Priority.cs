using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class Priority
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<TransactionScores> TransactionScores { get; set; }
    }
}
