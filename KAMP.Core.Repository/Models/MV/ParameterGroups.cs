using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class ParameterGroups
    {
        public long Id { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> IsIndustryGroup { get; set; }
        public Nullable<bool> IsHRG { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<Parameters> Parameters { get; set; }
    }
}
