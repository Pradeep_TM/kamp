using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class Engagement
    {
        public long Id { get; set; }
        public string EngagementName { get; set; }
        public string EngagementNumber { get; set; }
        public long ClientID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public virtual Clients Clients { get; set; }
        public virtual ICollection<ModuleValidationTypeMapping> ModuleValidationTypeMapping { get; set; }
    }
}
