using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    [KAMPObjectAttribute("MV", "Use this entity to set permission on Create Routine Screen.")]
    public partial class Routine
    {
        public int Id { get; set; }
        public string RoutineName { get; set; }
        public string RoutineDescription { get; set; }
        public Nullable<int> SourceTypeID { get; set; }
        public Nullable<int> RoutineAccountScore { get; set; }
        public Nullable<int> RoutineTransactionScore { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ParentRoutineID { get; set; }
        public Nullable<long> ClientID { get; set; }
        public virtual Clients Clients { get; set; }
        public virtual SourceType SourceType { get; set; }
        public virtual ICollection<CaseRoutine> CaseRoutine { get; set; }
        public virtual ICollection<Resultant> Resultant { get; set; }
        public virtual ICollection<SQLQuery> SQLQuery { get; set; }

        //public virtual ICollection<Routine> Routine1 { get; set; }
        //public virtual Routine Routine2 { get; set; }

        public virtual ICollection<TransactionScores> TransactionScores { get; set; }
    }
}
