using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class TransactionScoresMap : EntityTypeConfiguration<TransactionScores>
    {
        public TransactionScoresMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("MV_TransactionScores");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TranNo).HasColumnName("TranNo");
            this.Property(t => t.RoutineID).HasColumnName("RoutineID");
            this.Property(t => t.ParameterID).HasColumnName("ParameterID");
            this.Property(t => t.PriorityID).HasColumnName("PriorityID");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.Priority)
                .WithMany(t => t.TransactionScores)
                .HasForeignKey(d => d.PriorityID);
            this.HasRequired(t => t.Parameters)
                .WithMany(t => t.TransactionScores)
                .HasForeignKey(d => d.ParameterID);
            this.HasRequired(t => t.Routine)
                .WithMany(t => t.TransactionScores)
                .HasForeignKey(d => d.RoutineID);

        }
    }
}
