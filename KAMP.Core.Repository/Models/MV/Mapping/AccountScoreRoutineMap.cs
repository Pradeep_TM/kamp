﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.MV
{
    public class AccountScoreRoutineMap : EntityTypeConfiguration<AccountScoreRoutine>
    {
        public AccountScoreRoutineMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("MV_AccountScoreRoutine");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AccountScoreID).HasColumnName("AccountScoreID");
            this.Property(t => t.RoutineID).HasColumnName("RoutineID");
            this.Property(t => t.TranNo).HasColumnName("TranNo");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            this.HasRequired(t => t.AccountScores).WithMany(x => x.AccountScoreRoutine).HasForeignKey(y=>y.AccountScoreID);
        }
    }
}
