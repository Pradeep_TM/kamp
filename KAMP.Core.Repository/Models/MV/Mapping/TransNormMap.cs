using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class TransNormMap : EntityTypeConfiguration<TransNorm>
    {
        public TransNormMap()
        {
            // Primary Key
            this.HasKey(t => t.TranNo);

            // Properties
            this.Property(t => t.TranNo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.InternalReferenceNumber)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.MessageType)
                .HasMaxLength(4);

            this.Property(t => t.PaymentType)
                .HasMaxLength(50);

            this.Property(t => t.PartyType)
                .HasMaxLength(15);

            this.Property(t => t.SourceCode)
                .HasMaxLength(3);

            this.Property(t => t.CreditAdviceType)
                .HasMaxLength(3);

            this.Property(t => t.MTReference)
                .HasMaxLength(50);

            this.Property(t => t.FileName)
                .HasMaxLength(50);

            this.Property(t => t.CustomerReference)
                .HasMaxLength(50);

            this.Property(t => t.CustomerAccount1)
                .HasMaxLength(50);

            this.Property(t => t.CustomerAccount2)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyID)
                .HasMaxLength(25);

            this.Property(t => t.ByOrderAcct)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyBIC)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyABA)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyName)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyAddress)
                .HasMaxLength(150);

            this.Property(t => t.ByOrderPartyStateName)
                .HasMaxLength(50);

            this.Property(t => t.ByOrderPartyCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyID)
                .HasMaxLength(25);

            this.Property(t => t.BenePartyAcct)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyBIC)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyABA)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyName)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyAddress)
                .HasMaxLength(150);

            this.Property(t => t.BenePartyStateName)
                .HasMaxLength(50);

            this.Property(t => t.BenePartyCountryName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankID)
                .HasMaxLength(25);

            this.Property(t => t.CreditBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankABA)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankName)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.CreditBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankAdvice)
                .HasMaxLength(105);

            this.Property(t => t.CreditBankAdviceCountryName)
                .HasMaxLength(50);

            this.Property(t => t.CreditBankInstructions)
                .HasMaxLength(105);

            this.Property(t => t.CreditBankInstructionsCountryName)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankID)
                .HasMaxLength(25);

            this.Property(t => t.IntermediaryBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankABA)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankName)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.IntermediaryBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.IntermediaryBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankID)
                .HasMaxLength(25);

            this.Property(t => t.BeneficiaryBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankABA)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankName)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.BeneficiaryBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BeneficiaryBankAdvice)
                .HasMaxLength(105);

            this.Property(t => t.BeneficiaryBankAdviceCountryName)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankID)
                .HasMaxLength(25);

            this.Property(t => t.DebitBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankABA)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankName)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.DebitBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.DebitBankInstructions)
                .HasMaxLength(105);

            this.Property(t => t.DebitBankInstructionsCountryName)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankID)
                .HasMaxLength(25);

            this.Property(t => t.OriginatingBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankABA)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankName)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.OriginatingBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.OriginatingBankCountryName)
                .HasMaxLength(50);

            this.Property(t => t.OriginatortoBeneInfo)
                .HasMaxLength(300);

            this.Property(t => t.OriginatortoBeneInfoCountryName)
                .HasMaxLength(50);

            this.Property(t => t.BanktoBeneInfo)
                .HasMaxLength(300);

            this.Property(t => t.BankToBeneInfoCountryName)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankID)
                .HasMaxLength(25);

            this.Property(t => t.InstructingBankAcct)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankBIC)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankABA)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankName)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankNameNorm)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankAddress)
                .HasMaxLength(150);

            this.Property(t => t.InstructingBankStateName)
                .HasMaxLength(50);

            this.Property(t => t.InstructingBankCountryName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("MV_TransNorm");
            this.Property(t => t.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.TranNo).HasColumnName("TranNo");
            this.Property(t => t.InternalReferenceNumber).HasColumnName("InternalReferenceNumber");
            this.Property(t => t.MessageType).HasColumnName("MessageType");
            this.Property(t => t.PaymentType).HasColumnName("PaymentType");
            this.Property(t => t.PaymentAmount).HasColumnName("PaymentAmount");
            this.Property(t => t.TransDate).HasColumnName("TransDate");
            this.Property(t => t.ValueDate).HasColumnName("ValueDate");
            this.Property(t => t.RecvPay).HasColumnName("RecvPay");
            this.Property(t => t.PartyType).HasColumnName("PartyType");
            this.Property(t => t.SourceCode).HasColumnName("SourceCode");
            this.Property(t => t.CreditAdviceType).HasColumnName("CreditAdviceType");
            this.Property(t => t.MTReference).HasColumnName("MTReference");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.CustomerReference).HasColumnName("CustomerReference");
            this.Property(t => t.CustomerAccount1).HasColumnName("CustomerAccount1");
            this.Property(t => t.CustomerAccount2).HasColumnName("CustomerAccount2");
            this.Property(t => t.ByOrderPartyID).HasColumnName("ByOrderPartyID");
            this.Property(t => t.ByOrderAcct).HasColumnName("ByOrderAcct");
            this.Property(t => t.ByOrderPartyBIC).HasColumnName("ByOrderPartyBIC");
            this.Property(t => t.ByOrderPartyABA).HasColumnName("ByOrderPartyABA");
            this.Property(t => t.ByOrderPartyName).HasColumnName("ByOrderPartyName");
            this.Property(t => t.ByOrderPartyNameNorm).HasColumnName("ByOrderPartyNameNorm");
            this.Property(t => t.ByOrderPartyAddress).HasColumnName("ByOrderPartyAddress");
            this.Property(t => t.ByOrderPartyStateName).HasColumnName("ByOrderPartyStateName");
            this.Property(t => t.ByOrderPartyCountryName).HasColumnName("ByOrderPartyCountryName");
            this.Property(t => t.BenePartyID).HasColumnName("BenePartyID");
            this.Property(t => t.BenePartyAcct).HasColumnName("BenePartyAcct");
            this.Property(t => t.BenePartyBIC).HasColumnName("BenePartyBIC");
            this.Property(t => t.BenePartyABA).HasColumnName("BenePartyABA");
            this.Property(t => t.BenePartyName).HasColumnName("BenePartyName");
            this.Property(t => t.BenePartyNameNorm).HasColumnName("BenePartyNameNorm");
            this.Property(t => t.BenePartyAddress).HasColumnName("BenePartyAddress");
            this.Property(t => t.BenePartyStateName).HasColumnName("BenePartyStateName");
            this.Property(t => t.BenePartyCountryName).HasColumnName("BenePartyCountryName");
            this.Property(t => t.CreditBankID).HasColumnName("CreditBankID");
            this.Property(t => t.CreditBankAcct).HasColumnName("CreditBankAcct");
            this.Property(t => t.CreditBankBIC).HasColumnName("CreditBankBIC");
            this.Property(t => t.CreditBankABA).HasColumnName("CreditBankABA");
            this.Property(t => t.CreditBankName).HasColumnName("CreditBankName");
            this.Property(t => t.CreditBankNameNorm).HasColumnName("CreditBankNameNorm");
            this.Property(t => t.CreditBankAddress).HasColumnName("CreditBankAddress");
            this.Property(t => t.CreditBankStateName).HasColumnName("CreditBankStateName");
            this.Property(t => t.CreditBankCountryName).HasColumnName("CreditBankCountryName");
            this.Property(t => t.CreditBankAdvice).HasColumnName("CreditBankAdvice");
            this.Property(t => t.CreditBankAdviceCountryName).HasColumnName("CreditBankAdviceCountryName");
            this.Property(t => t.CreditBankInstructions).HasColumnName("CreditBankInstructions");
            this.Property(t => t.CreditBankInstructionsCountryName).HasColumnName("CreditBankInstructionsCountryName");
            this.Property(t => t.IntermediaryBankID).HasColumnName("IntermediaryBankID");
            this.Property(t => t.IntermediaryBankAcct).HasColumnName("IntermediaryBankAcct");
            this.Property(t => t.IntermediaryBankBIC).HasColumnName("IntermediaryBankBIC");
            this.Property(t => t.IntermediaryBankABA).HasColumnName("IntermediaryBankABA");
            this.Property(t => t.IntermediaryBankName).HasColumnName("IntermediaryBankName");
            this.Property(t => t.IntermediaryBankNameNorm).HasColumnName("IntermediaryBankNameNorm");
            this.Property(t => t.IntermediaryBankAddress).HasColumnName("IntermediaryBankAddress");
            this.Property(t => t.IntermediaryBankStateName).HasColumnName("IntermediaryBankStateName");
            this.Property(t => t.IntermediaryBankCountryName).HasColumnName("IntermediaryBankCountryName");
            this.Property(t => t.BeneficiaryBankID).HasColumnName("BeneficiaryBankID");
            this.Property(t => t.BeneficiaryBankAcct).HasColumnName("BeneficiaryBankAcct");
            this.Property(t => t.BeneficiaryBankABA).HasColumnName("BeneficiaryBankABA");
            this.Property(t => t.BeneficiaryBankBIC).HasColumnName("BeneficiaryBankBIC");
            this.Property(t => t.BeneficiaryBankName).HasColumnName("BeneficiaryBankName");
            this.Property(t => t.BeneficiaryBankNameNorm).HasColumnName("BeneficiaryBankNameNorm");
            this.Property(t => t.BeneficiaryBankAddress).HasColumnName("BeneficiaryBankAddress");
            this.Property(t => t.BeneficiaryBankStateName).HasColumnName("BeneficiaryBankStateName");
            this.Property(t => t.BeneficiaryBankCountryName).HasColumnName("BeneficiaryBankCountryName");
            this.Property(t => t.BeneficiaryBankAdvice).HasColumnName("BeneficiaryBankAdvice");
            this.Property(t => t.BeneficiaryBankAdviceCountryName).HasColumnName("BeneficiaryBankAdviceCountryName");
            this.Property(t => t.DebitBankID).HasColumnName("DebitBankID");
            this.Property(t => t.DebitBankAcct).HasColumnName("DebitBankAcct");
            this.Property(t => t.DebitBankBIC).HasColumnName("DebitBankBIC");
            this.Property(t => t.DebitBankABA).HasColumnName("DebitBankABA");
            this.Property(t => t.DebitBankName).HasColumnName("DebitBankName");
            this.Property(t => t.DebitBankNameNorm).HasColumnName("DebitBankNameNorm");
            this.Property(t => t.DebitBankAddress).HasColumnName("DebitBankAddress");
            this.Property(t => t.DebitBankStateName).HasColumnName("DebitBankStateName");
            this.Property(t => t.DebitBankCountryName).HasColumnName("DebitBankCountryName");
            this.Property(t => t.DebitBankInstructions).HasColumnName("DebitBankInstructions");
            this.Property(t => t.DebitBankInstructionsCountryName).HasColumnName("DebitBankInstructionsCountryName");
            this.Property(t => t.OriginatingBankID).HasColumnName("OriginatingBankID");
            this.Property(t => t.OriginatingBankAcct).HasColumnName("OriginatingBankAcct");
            this.Property(t => t.OriginatingBankBIC).HasColumnName("OriginatingBankBIC");
            this.Property(t => t.OriginatingBankABA).HasColumnName("OriginatingBankABA");
            this.Property(t => t.OriginatingBankName).HasColumnName("OriginatingBankName");
            this.Property(t => t.OriginatingBankNameNorm).HasColumnName("OriginatingBankNameNorm");
            this.Property(t => t.OriginatingBankAddress).HasColumnName("OriginatingBankAddress");
            this.Property(t => t.OriginatingBankStateName).HasColumnName("OriginatingBankStateName");
            this.Property(t => t.OriginatingBankCountryName).HasColumnName("OriginatingBankCountryName");
            this.Property(t => t.OriginatortoBeneInfo).HasColumnName("OriginatortoBeneInfo");
            this.Property(t => t.OriginatortoBeneInfoCountryName).HasColumnName("OriginatortoBeneInfoCountryName");
            this.Property(t => t.BanktoBeneInfo).HasColumnName("BanktoBeneInfo");
            this.Property(t => t.BankToBeneInfoCountryName).HasColumnName("BankToBeneInfoCountryName");
            this.Property(t => t.InstructingBankID).HasColumnName("InstructingBankID");
            this.Property(t => t.InstructingBankAcct).HasColumnName("InstructingBankAcct");
            this.Property(t => t.InstructingBankBIC).HasColumnName("InstructingBankBIC");
            this.Property(t => t.InstructingBankABA).HasColumnName("InstructingBankABA");
            this.Property(t => t.InstructingBankName).HasColumnName("InstructingBankName");
            this.Property(t => t.InstructingBankNameNorm).HasColumnName("InstructingBankNameNorm");
            this.Property(t => t.InstructingBankAddress).HasColumnName("InstructingBankAddress");
            this.Property(t => t.InstructingBankStateName).HasColumnName("InstructingBankStateName");
            this.Property(t => t.InstructingBankCountryName).HasColumnName("InstructingBankCountryName");
            this.Property(t => t.CaseCount).HasColumnName("CaseCount");
            this.Property(t => t.CaseCheck).HasColumnName("CaseCheck");
        }
    }
}
