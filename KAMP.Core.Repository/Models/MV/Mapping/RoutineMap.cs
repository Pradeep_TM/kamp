using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class RoutineMap : EntityTypeConfiguration<Routine>
    {
        public RoutineMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RoutineName)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("MV_Routine");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoutineName).HasColumnName("RoutineName");
            this.Property(t => t.RoutineDescription).HasColumnName("RoutineDescription");
            this.Property(t => t.SourceTypeID).HasColumnName("SourceTypeID");
            this.Property(t => t.RoutineAccountScore).HasColumnName("RoutineAccountScore");
            this.Property(t => t.RoutineTransactionScore).HasColumnName("RoutineTransactionScore");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ParentRoutineID).HasColumnName("ParentRoutineID");
            this.Property(t => t.ClientID).HasColumnName("ClientID");

            // Relationships
            this.HasOptional(t => t.Clients)
                .WithMany(t => t.Routine)
                .HasForeignKey(d => d.ClientID);
            this.HasOptional(t => t.SourceType)
                .WithMany(t => t.Routine)
                .HasForeignKey(d => d.SourceTypeID);

            //this.HasOptional(t => t.Routine2)
            //    .WithMany(t => t.Routine1)
            //    .HasForeignKey(d => d.ParentRoutineID);

        }
    }
}
