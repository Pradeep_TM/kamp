using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class ParameterGroupsMap : EntityTypeConfiguration<ParameterGroups>
    {
        public ParameterGroupsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("MV_ParameterGroups");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.IsIndustryGroup).HasColumnName("IsIndustryGroup");
            this.Property(t => t.IsHRG).HasColumnName("IsHRG");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}
