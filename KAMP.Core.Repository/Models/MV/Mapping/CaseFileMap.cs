using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class CaseFileMap : EntityTypeConfiguration<MVCaseFile>
    {
        public CaseFileMap()
        {
            // Primary Key
            this.HasKey(t => t.CfId);

            // Properties
            this.Property(t => t.CaseNo)
                .IsOptional()
                .HasMaxLength(50);

            this.Property(t => t.AMLCaseKey)
                .HasMaxLength(15);

            this.Property(t => t.Name)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("MV_CaseFile");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.CaseNo).HasColumnName("CaseNo");
            this.Property(t => t.TransNormID).HasColumnName("TransNormID");
            this.Property(t => t.TransNormNo).HasColumnName("TransNormNo");
            this.Property(t => t.AMLCaseKey).HasColumnName("AMLCaseKey");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.CategoryId).HasColumnName("CategoryId");
            this.Property(t => t.RiskProfile).HasColumnName("RiskProfile");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.StatusId).HasColumnName("StatusId");
            this.Property(t => t.DueDiligence).HasColumnName("DueDiligence");
            this.Property(t => t.MemorandumOfFact).HasColumnName("MemorandumOfFact");
            this.Property(t => t.TransactionCount).HasColumnName("TransactionCount");
            this.Property(t => t.TransactionScore).HasColumnName("TransactionScore");
            this.Property(t => t.AcccountCount).HasColumnName("AcccountCount");
            this.Property(t => t.AcccountScore).HasColumnName("AcccountScore");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifedDate).HasColumnName("ModifedDate");

            // Relationships
            this.HasOptional(t => t.TransNorm)
                .WithMany(t => t.CaseFile)
                .HasForeignKey(d => d.TransNormID);

        }
    }
}
