using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class ModuleValidationTypeMappingMap : EntityTypeConfiguration<ModuleValidationTypeMapping>
    {
        public ModuleValidationTypeMappingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CORE_ModuleValidationTypeMapping");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ModuleTypeID).HasColumnName("ModuleTypeID");
            this.Property(t => t.ValidationID).HasColumnName("ValidationID");
            this.Property(t => t.EngagementID).HasColumnName("EngagementID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.Engagement)
                .WithMany(t => t.ModuleValidationTypeMapping)
                .HasForeignKey(d => d.EngagementID);

        }
    }
}
