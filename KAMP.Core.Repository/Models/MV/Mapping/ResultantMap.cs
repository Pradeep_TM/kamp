using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class ResultantMap : EntityTypeConfiguration<Resultant>
    {
        public ResultantMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("MV_Resultant");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoutineID).HasColumnName("RoutineID");
            this.Property(t => t.ResultantQuery).HasColumnName("ResultantQuery");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.Routine)
                .WithMany(t => t.Resultant)
                .HasForeignKey(d => d.RoutineID);

        }
    }
}
