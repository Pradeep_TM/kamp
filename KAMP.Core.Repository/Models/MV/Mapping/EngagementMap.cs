using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.MV
{
    public class EngagementMap : EntityTypeConfiguration<Engagement>
    {
        public EngagementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.EngagementName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.EngagementNumber)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CORE_Engagement");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EngagementName).HasColumnName("EngagementName");
            this.Property(t => t.EngagementNumber).HasColumnName("EngagementNumber");
            this.Property(t => t.ClientID).HasColumnName("ClientID");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");

            // Relationships
            this.HasRequired(t => t.Clients)
                .WithMany(t => t.Engagement)
                .HasForeignKey(d => d.ClientID);

        }
    }
}
