using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class TransactionScores
    {
        public int Id { get; set; }
        public int TranNo { get; set; }
        public int RoutineID { get; set; }
        public long ParameterID { get; set; }
        public int PriorityID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual Priority Priority { get; set; }
        public virtual Parameters Parameters { get; set; }
        public virtual Routine Routine { get; set; }
    }
}
