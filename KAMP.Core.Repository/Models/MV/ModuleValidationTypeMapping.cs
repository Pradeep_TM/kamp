using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class ModuleValidationTypeMapping
    {
        public int Id { get; set; }
        public int ModuleTypeID { get; set; }
        public int ValidationID { get; set; }
        public long EngagementID { get; set; }
        public bool IsActive { get; set; }
        public virtual Engagement Engagement { get; set; }
    }
}
