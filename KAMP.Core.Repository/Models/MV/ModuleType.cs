using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class ModuleType
    {
        public int ModuleTypeId { get; set; }
        public string ModuleName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        //public virtual ICollection<UM_ModuleObjects> UM_ModuleObjects { get; set; }
        //public virtual ICollection<UM_RoleInPermission> UM_RoleInPermission { get; set; }
        //public virtual ICollection<UM_UserInGroup> UM_UserInGroup { get; set; }
    }
}
