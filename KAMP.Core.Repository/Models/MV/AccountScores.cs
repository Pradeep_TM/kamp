using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
     [KAMPObjectAttribute("MV", "Use this entity to set permission on Analysis Screen.")]
    public partial class AccountScores
    {
        public int Id { get; set; }
        public Nullable<int> TranNo { get; set; }
        public int AccountScore { get; set; }
        public string AccountNumber { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<AccountScoreRoutine> AccountScoreRoutine { get; set; }
    }
}
