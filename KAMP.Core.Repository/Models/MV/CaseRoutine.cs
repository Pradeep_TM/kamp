using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class CaseRoutine
    {
        public long Id { get; set; }
        public long CaseFileID { get; set; }
        public int RoutineID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual MVCaseFile CaseFile { get; set; }
        public virtual Routine Routine { get; set; }
    }
}
