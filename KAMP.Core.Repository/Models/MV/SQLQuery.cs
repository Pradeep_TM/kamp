using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    [KAMPObjectAttribute("MV", "Use this entity to set permission on Query Builder Screen.")]
    public partial class SQLQuery
    {
        public long Id { get; set; }
        public int RoutineID { get; set; }
        public string QuerySyntax { get; set; }
        public string QueryName { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifedOn { get; set; }
        public virtual Routine Routine { get; set; }
    }
}
