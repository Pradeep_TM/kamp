﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.MV
{
    public partial class AccountScoreRoutine
    {
        public int Id { get; set; }
        public int AccountScoreID { get; set; }
        public int RoutineID { get; set; }
        public int TranNo { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual AccountScores AccountScores { get; set; }  
    }
}
