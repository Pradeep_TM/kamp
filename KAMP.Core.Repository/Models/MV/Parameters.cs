using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    public partial class Parameters
    {
        public long Id { get; set; }
        public long ParameterGroupID { get; set; }
        public string ActualValue { get; set; }
        public string Alias { get; set; }
        public string Value { get; set; }
        public string SourceKey { get; set; }
        public Nullable<bool> ContainstSpaceEachSide { get; set; }
        public string ValueNoSpace { get; set; }
        public Nullable<bool> IsAddSAR { get; set; }
        public virtual ParameterGroups ParameterGroups { get; set; }
        public virtual ICollection<TransactionScores> TransactionScores { get; set; }
    }
}
