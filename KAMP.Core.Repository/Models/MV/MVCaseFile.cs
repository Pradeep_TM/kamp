using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.MV
{
    [KAMPObjectAttribute("MV", "Use this entity to set permission on Assign Case, Case Management, Case file card")]
    public partial class MVCaseFile
    {
        public long CfId { get; set; }
        public string CaseNo { get; set; }
        public Nullable<int> TransNormID { get; set; }
        public Nullable<int> TransNormNo { get; set; }
        public string AMLCaseKey { get; set; }
        public string Name { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string RiskProfile { get; set; }
        public int UserId { get; set; }
        public Nullable<int> StatusId { get; set; }
        public string DueDiligence { get; set; }
        public string MemorandumOfFact { get; set; }
        public Nullable<int> TransactionCount { get; set; }
        public Nullable<int> TransactionScore { get; set; }
        public Nullable<int> AcccountCount { get; set; }
        public Nullable<int> AcccountScore { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifedDate { get; set; }
        public virtual TransNorm TransNorm { get; set; }
        public virtual ICollection<CaseRoutine> CaseRoutine { get; set; }
    }
}
