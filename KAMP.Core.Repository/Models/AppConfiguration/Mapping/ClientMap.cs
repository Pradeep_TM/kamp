﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
   public class ClientMap : EntityTypeConfiguration<Client>
    {
       public ClientMap()
       {   // Primary Key
           this.HasKey(t=>t.Id);

           // Properties
           this.Property(t => t.ClientName)
               .IsRequired()
               .HasMaxLength(50);

           this.Property(t => t.ClientCode)
               .IsRequired()
               .HasMaxLength(10);

           this.Property(t => t.Country)
               .IsRequired()
               .HasMaxLength(50);

           this.Property(t => t.ZipCode)
               .HasMaxLength(50);

           // Table & Column Mappings
           this.ToTable("CORE_Clients");
           this.Property(t => t.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
           this.Property(t => t.ClientName).HasColumnName("ClientName");
           this.Property(t => t.ClientCode).HasColumnName("ClientCode");
           this.Property(t => t.Country).HasColumnName("Country");
           this.Property(t => t.ZipCode).HasColumnName("ZipCode");
           this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
           this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
           this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
           this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
           this.Property(t => t.IsActive).HasColumnName("IsActive");
           this.Property(t => t.Logo).HasColumnName("Logo");
       }
    }
}
