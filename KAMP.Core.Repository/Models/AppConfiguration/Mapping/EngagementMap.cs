﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    public class EngagementMap : EntityTypeConfiguration<ClientEngagement>
    {
        public EngagementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.EngagementName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.EngagementNumber)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CORE_Engagement");
            this.Property(t => t.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.EngagementName).HasColumnName("EngagementName");
            this.Property(t => t.EngagementNumber).HasColumnName("EngagementNumber");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            //Relationship
            this.HasRequired(t => t.Client)
               .WithMany(t => t.ClientEngagement)
               .HasForeignKey(d => d.ClientId);
        }
    }
}
