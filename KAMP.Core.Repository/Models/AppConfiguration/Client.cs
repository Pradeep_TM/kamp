﻿using KAMP.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
  public partial class Client
    {
      public Client()
      {
          this.ClientEngagement = new List<ClientEngagement>();
      }
      public long Id { get; set; }
      public string ClientName { get; set; }
      public string Country { get; set; }
      public string ZipCode { get; set; }
      public Nullable<long> CreatedBy { get; set; }
      public Nullable<System.DateTime> CreatedDate { get; set; }
      public Nullable<long> ModifiedBy { get; set; }
      public Nullable<System.DateTime> ModifiedDate { get; set; }
      public Nullable<bool> IsActive { get; set; }
      public byte[] Logo { get; set; }
      public string ClientCode { get; set; }
      public virtual ICollection<ClientEngagement> ClientEngagement { get; set; }
     
    }
}
