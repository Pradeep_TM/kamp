﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models
{
    public class DataManagement
    {
        public int DataManagementID { get; set; }
        public string SourceFileName { get; set; }
        public string SourceFilePath { get; set; }
        public string DBTableName { get; set; }
        public string DBName { get; set; }
        public string DBServerName { get; set; }
        public int UploadTypeID { get; set; }
        public virtual UploadType UploadType { get; set; }
    }
}
