using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace KAMP.Core.Repository.AuditLog
{
    [KAMPObjectAttribute("Common", "Use this Entity to Set permissions to access Audit Configuration screen.")]
    public partial class ConfigurationTable
    {
        public ConfigurationTable()
        {
            EntityModelXmls = new ObservableCollection<EntityModelXml>();
        }

        public int Id { get; set; }
        public string EntityName { get; set; }
        public string ConfigurationXml { get; set; }
        public int ModuleId { get; set; }
        public virtual ICollection<EntityModelXml> EntityModelXmls { get; set; }
    }
}
