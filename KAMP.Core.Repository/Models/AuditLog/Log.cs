using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.AuditLog
{
    public partial class Log
    {
        public int Id { get; set; }
        public int LogDetailsId { get; set; }
        public string AuditXml { get; set; }
        public virtual LogDetail LogDetail { get; set; }
    }
}
