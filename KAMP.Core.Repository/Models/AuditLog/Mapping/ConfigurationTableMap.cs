using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.AuditLog
{
    public class ConfigurationTableMap : EntityTypeConfiguration<ConfigurationTable>
    {
        public ConfigurationTableMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Audit_ConfigurationTables");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EntityName).HasColumnName("EntityName");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
            this.Property(t => t.ConfigurationXml).HasColumnName("ConfigurationXml");
        }
    }
}
