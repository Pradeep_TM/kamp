using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.AuditLog
{
    public class EntityModelXmlMap : EntityTypeConfiguration<EntityModelXml>
    {
        public EntityModelXmlMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Audit_EntityModelXmls");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ConfigurationId).HasColumnName("ConfigurationId");
            this.Property(t => t.Action).HasColumnName("Action");
            this.Property(t => t.EntityXml).HasColumnName("EntityXml");

            // Relationships
            this.HasRequired(t => t.ConfigurationTable)
                .WithMany(t => t.EntityModelXmls)
                .HasForeignKey(d => d.ConfigurationId);

        }
    }
}
