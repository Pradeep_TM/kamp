using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.AuditLog
{
    public class LogMap : EntityTypeConfiguration<Log>
    {
        public LogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Audit_Logs");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LogDetailsId).HasColumnName("LogDetailsId");
            this.Property(t => t.AuditXml).HasColumnName("AuditXml");

            // Relationships
            this.HasRequired(t => t.LogDetail)
                .WithMany(t => t.Logs)
                .HasForeignKey(d => d.LogDetailsId);

        }
    }
}
