using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.AuditLog
{
    public class LogDetailMap : EntityTypeConfiguration<LogDetail>
    {
        public LogDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Audit_LogDetails");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EntityUId).HasColumnName("EntityUId");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
            this.Property(t => t.EntityName).HasColumnName("EntityName");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.DateTime).HasColumnName("DateTime");
            this.Property(t => t.Action).HasColumnName("Action");
            this.Property(t => t.cf_ID).IsOptional().HasColumnName("cf_ID");
        }
    }
}
