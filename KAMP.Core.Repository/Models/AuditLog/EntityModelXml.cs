using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.AuditLog
{
    public partial class EntityModelXml
    {
        public int Id { get; set; }
        public int ConfigurationId { get; set; }
        public string Action { get; set; }
        public string EntityXml { get; set; }
        public virtual ConfigurationTable ConfigurationTable { get; set; }
    }
}
