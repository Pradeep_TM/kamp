using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace KAMP.Core.Repository.AuditLog
{
     [KAMPObjectAttribute("TLB", "Use this entity to set permission on History")]
    public partial class LogDetail
    {
        public LogDetail()
        {
            Logs = new ObservableCollection<Log>();
        }

        public int Id { get; set; }
        public int EntityUId { get; set; }
        public string EntityName { get; set; }
        public int ModuleId { get; set; }
        public string CreatedBy { get; set; }
        public string DateTime { get; set; }
        public string Action { get; set; }
        public virtual ICollection<Log> Logs { get; set; }

        public int? cf_ID { get; set; }
    }
}
