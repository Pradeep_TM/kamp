﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.Mapping
{
    using Repository;
    public class CommonConfigMap : EntityTypeConfiguration<CommonConfiguration>
    {
        public CommonConfigMap()
        {
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ConfigName)
                .IsRequired()
                .HasMaxLength(50);

            // Properties
            this.Property(t => t.ConfigValue)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CommonConfiguration");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ConfigName).HasColumnName("ConfigName");
            this.Property(t => t.ConfigValue).HasColumnName("ConfigValue");
        }
    }
}
