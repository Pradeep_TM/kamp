﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.Mapping
{
    public class DataManagementMap : EntityTypeConfiguration<DataManagement>
    {
        public DataManagementMap()
        {
            // Primary Key
            this.HasKey(t => t.DataManagementID);

            // Properties
            this.Property(t => t.SourceFileName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.SourceFilePath)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.DBTableName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DBName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DBServerName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("DataManagement");
            this.Property(t => t.DataManagementID).HasColumnName("DataManagementId");
            this.Property(t => t.SourceFileName).HasColumnName("SourceFileName");
            this.Property(t => t.SourceFilePath).HasColumnName("SourceFilePath");
            this.Property(t => t.DBTableName).HasColumnName("DBTableName");
            this.Property(t => t.DBName).HasColumnName("DBName");
            this.Property(t => t.DBServerName).HasColumnName("DBServerName");
            this.Property(t => t.UploadTypeID).HasColumnName("UploadTypeId");
         

            // Relationships

            this.HasRequired(t => t.UploadType)
                .WithMany(t => t.DataManagements)
                .HasForeignKey(d => d.UploadTypeID);
        }
    }
}
