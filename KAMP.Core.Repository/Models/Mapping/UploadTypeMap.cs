﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.Mapping
{
    public class UploadTypeMap : EntityTypeConfiguration<UploadType>
    {
        public UploadTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.UploadTypeID);

            // Properties
            this.Property(t => t.UploadTypeName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("UploadType");
            this.Property(t => t.UploadTypeID).HasColumnName("UploadTypeId");
            this.Property(t => t.UploadTypeName).HasColumnName("UploadTypeName");
            this.Property(t => t.IsActive).HasColumnName("Active");
        }
    }
}
