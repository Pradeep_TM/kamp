using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.Models.Mapping
{
    public class ModuleMap : EntityTypeConfiguration<Module>
    {
        public ModuleMap()
        {
            // Primary Key
            this.HasKey(t => t.ModuleTypeId);

            // Properties
            this.Property(t => t.ModuleName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CORE_ModuleType");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.Property(t => t.ModuleName).HasColumnName("ModuleName");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
