﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository
{
    public partial class CommonConfiguration
    {
        public CommonConfiguration()
        {

        }
        public int Id { get; set; }
        public string ConfigName { get; set; }
        public string ConfigValue { get; set; }
    }
}
