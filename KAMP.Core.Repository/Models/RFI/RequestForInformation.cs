using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.RFI
{
     [KAMPObjectAttribute("Common", "Use this Entity to Set permissions to access Request For Information. ")]
    public partial class RequestForInformation
    {
        public RequestForInformation()
        {
            RFIAttachments = new List<RFIAttachments>();
            //RFIResponses = new List<RFIResponses>();
        }

        public int Id { get; set; }
        public int ModuleId { get; set; }
        public string CaseNumber { get; set; }
        public string SentTo { get; set; }
        public string SentFrom { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsAttachmentMail { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public System.DateTime SentDate { get; set; }
        public string CreatedBy { get; set; }
        public virtual ICollection<RFIAttachments> RFIAttachments { get; set; }
        //public virtual ICollection<RFIResponses> RFIResponses { get; set; }
    }
}
