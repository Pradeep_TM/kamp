using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.RFI
{
    public partial class RFIAttachments
    {
        public int Id { get; set; }
        public int RFIId { get; set; }
        public string AttachmentName { get; set; }
        public byte[] Attachment { get; set; }
        public virtual RequestForInformation RequestForInformation { get; set; }
    }
}
