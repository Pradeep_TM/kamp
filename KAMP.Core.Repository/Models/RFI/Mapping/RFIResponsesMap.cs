using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.RFI
{
    public class RFIResponsesMap : EntityTypeConfiguration<RFIResponses>
    {
        public RFIResponsesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.Property(t => t.ResponseUploaded)
                .IsRequired().HasMaxLength(int.MaxValue);

            this.Property(t => t.CaseNumber)
                .IsFixedLength()
                .HasMaxLength(50);

            this.Property(t => t.SentTo)
                .IsRequired()
                .HasMaxLength(255);


            this.Property(t => t.ModuleId)
              .IsRequired();

            this.Property(t => t.SentFrom)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(255);


            // Table & Column Mappings
            this.ToTable("RFIResponses");
            this.Property(t => t.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
            this.Property(t => t.CaseNumber).HasColumnName("CaseNumber");
            this.Property(t => t.SentTo).HasColumnName("SentTo");
            this.Property(t => t.SentFrom).HasColumnName("SentFrom");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.ResponseUploaded).HasColumnName("ResponseUploaded");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CC).HasColumnName("CC");
            this.Property(t => t.BCC).HasColumnName("BCC");
            this.Property(t => t.SentDate).HasColumnName("SentDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
        }
    }
}
