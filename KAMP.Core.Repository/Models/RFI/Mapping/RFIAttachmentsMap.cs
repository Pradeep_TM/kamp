using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.RFI
{
    public class RFIAttachmentsMap : EntityTypeConfiguration<RFIAttachments>
    {
        public RFIAttachmentsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id });

            this.Property(t => t.AttachmentName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Attachment)
                .IsRequired().HasMaxLength(int.MaxValue);

            // Table & Column Mappings
            this.ToTable("RFIAttachments");
            this.Property(t => t.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.RFIId).HasColumnName("RFIId");
            this.Property(t => t.AttachmentName).HasColumnName("AttachmentName");
            this.Property(t => t.Attachment).HasColumnName("Attachment");

            // Relationships
            this.HasRequired(t => t.RequestForInformation)
                .WithMany(t => t.RFIAttachments)
                .HasForeignKey(d => d.RFIId);

        }
    }
}
