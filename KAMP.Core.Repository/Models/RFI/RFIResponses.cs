using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.RFI
{
    public partial class RFIResponses
    {
        public byte[] ResponseUploaded { get; set; }
        public int Id { get; set; }
        public int ModuleId { get; set; }
        public string CaseNumber { get; set; }
        public string SentTo { get; set; }
        public string SentFrom { get; set; }
        public string Subject { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public System.DateTime SentDate { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
