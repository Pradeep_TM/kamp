
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.UM
{
    public partial class RoleInPermission
    {
        public long RoleInPermissionId { get; set; }
        public long RoleId { get; set; }
        public long PermissionSetId { get; set; }
        public virtual PermissionSet PermissionSet { get; set; }
        public int ModuleId { get; set; }
        public virtual Module ModuleType { get; set; }
        public virtual Role Roles { get; set; }
        public bool? IsActive { get; set; }
    }
}
