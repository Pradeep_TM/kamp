﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UM
{
    public class PermissionSet
    {
        public long PermissionSetId { get; set; }
        public string PermissionSetName { get; set; }
        public string ModuleObjectName { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int ModuleId { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<PermissionInSet> PermissionInSet { get; set; }
        public virtual ICollection<RoleInPermission> RoleInPermission { get; set; }
    }
}
