﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UM
{
    public class ModuleObjects
    {
        public long ModuleObjectsId { get; set; }
        public string ModuleObjectName { get; set; }
        public int ModuleTypeId { get; set; }
        public virtual Module Module { get; set; }
    }
}
