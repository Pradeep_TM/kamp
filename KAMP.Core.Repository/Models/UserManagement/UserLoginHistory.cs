﻿using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.UserManagement
{
    [KAMPObjectAttribute("UM")]
    public class UserLoginHistory
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public DateTime LoginTime { get; set; }
        public long UserId { get; set; }
        public int ModuleTypeId { get; set; }
        public string ModuleName { get; set; }
    }
}
