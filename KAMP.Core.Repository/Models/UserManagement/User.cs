
using KAMP.Core.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.UM
{
    [KAMPObjectAttribute("Common", "Use this Entity to Set permissions to access User Management. ")]
    public partial class User
    {
        public User()
        {

        }

        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public bool? IsSuperAdmin { get; set; }
        public virtual ICollection<UserInGroup> UserInGroup { get; set; }
        public virtual ICollection<UserInRole> UserInRole { get; set; }
    }
}
