﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UM
{
    public class PermissionInSet
    {
        public long PermissionInSetId { get; set; }
        public long PermissionId { get; set; }
        public long PermissionSetId { get; set; }
        public virtual Permission Permission { get; set; }
        public virtual PermissionSet PermissionSet { get; set; }
    }
}
