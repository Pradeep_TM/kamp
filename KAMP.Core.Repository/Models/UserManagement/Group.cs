using System;
using System.Collections.Generic;
using KAMP.Core.Repository.WF;
namespace KAMP.Core.Repository.UM
{
    public partial class Group
    {
        public Group()
        {

        }
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual ICollection<UserInGroup> UserInGroup { get; set; }
        public virtual ICollection<State> States { get; set; }
    }
}
