
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.UM
{
    public partial class Permission
    {
        public Permission()
        {

        }

        public long Id { get; set; }
        public string PermissionName { get; set; }
        public virtual ICollection<PermissionInSet> PermissionInSet { get; set; }
    }
}
