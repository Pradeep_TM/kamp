using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.UM
{
    public class RoleInPermissionMap : EntityTypeConfiguration<RoleInPermission>
    {
        public RoleInPermissionMap()
        {
            // Primary Key
            // Primary Key
            this.HasKey(t => t.RoleInPermissionId);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_RoleInPermission");
            this.Property(t => t.RoleInPermissionId).HasColumnName("RoleInPermissionId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.PermissionSetId).HasColumnName("PermissionSetId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            // Relationships
            this.HasRequired(t => t.ModuleType)
               .WithMany(t => t.RoleInPermission)
               .HasForeignKey(d => d.ModuleId);
            this.HasRequired(t => t.PermissionSet)
                .WithMany(t => t.RoleInPermission)
                .HasForeignKey(d => d.PermissionSetId);
            this.HasRequired(t => t.Roles)
                .WithMany(t => t.RoleInPermission)
                .HasForeignKey(d => d.RoleId);

        }
    }
}
