﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UM
{
    public class PermissionInSetMap : EntityTypeConfiguration<PermissionInSet>
    {
        public PermissionInSetMap()
        {
            // Primary Key
            this.HasKey(t => t.PermissionInSetId);

            // Properties
            this.Property(t => t.PermissionInSetId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Table & Column Mappings
            this.ToTable("UM_PermissionInSet");
            this.Property(t => t.PermissionInSetId).HasColumnName("PermissionInSetId");
            this.Property(t => t.PermissionId).HasColumnName("PermissionId");
            this.Property(t => t.PermissionSetId).HasColumnName("PermissionSetId");

            // Relationships
            this.HasRequired(t => t.Permission)
                .WithMany(t => t.PermissionInSet)
                .HasForeignKey(d => d.PermissionId);
            this.HasRequired(t => t.PermissionSet)
                .WithMany(t => t.PermissionInSet)
                .HasForeignKey(d => d.PermissionSetId);

        }
    }
}
