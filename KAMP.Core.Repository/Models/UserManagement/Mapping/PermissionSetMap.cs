﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UM
{
    public class PermissionSetMap : EntityTypeConfiguration<PermissionSet>
    {
        public PermissionSetMap()
        {
            // Primary Key
            this.HasKey(t => t.PermissionSetId);

            // Properties
            this.Property(t => t.PermissionSetName)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("UM_PermissionSet");
            this.Property(t => t.PermissionSetId).HasColumnName("PermissionSetId");
            this.Property(t => t.PermissionSetName).HasColumnName("PermissionSetName");
            this.Property(t => t.ModuleObjectName).HasColumnName("ModuleObjectName");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

        }
    }
}
