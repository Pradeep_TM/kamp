using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.UM
{
    public class UserInGroupMap : EntityTypeConfiguration<UserInGroup>
    {
        public UserInGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.UserInGroupId);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_UserInGroup");
            this.Property(t => t.UserInGroupId).HasColumnName("UserInGroupId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.GroupId).HasColumnName("GroupId");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.Module)
                .WithMany(t => t.UserInGroup)
                .HasForeignKey(d => d.ModuleTypeId);
            this.HasRequired(t => t.Group)
                .WithMany(t => t.UserInGroup)
                .HasForeignKey(d => d.GroupId);
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserInGroup)
                .HasForeignKey(d => d.UserId);

        }
    }
}
