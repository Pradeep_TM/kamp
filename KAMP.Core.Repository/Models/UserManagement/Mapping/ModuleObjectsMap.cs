﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.UM
{
    public class ModuleObjectsMap : EntityTypeConfiguration<ModuleObjects>
    {
        public ModuleObjectsMap()
        {
            // Primary Key
            this.HasKey(t => t.ModuleObjectsId);

            // Properties
            this.Property(t => t.ModuleObjectName)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("UM_ModuleObjects");
            this.Property(t => t.ModuleObjectsId).HasColumnName("ModuleObjectsId");
            this.Property(t => t.ModuleObjectName).HasColumnName("ModuleObjectName");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");

            // Relationships
            this.HasRequired(t => t.Module)
                .WithMany(t => t.ModuleObjects)
                .HasForeignKey(d => d.ModuleTypeId);
        }
    }
}
