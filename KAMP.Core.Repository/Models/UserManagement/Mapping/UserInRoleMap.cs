using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.UM
{
    public class UserInRoleMap : EntityTypeConfiguration<UserInRole>
    {
        public UserInRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.UserInRoleId);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_UserInRole");
            this.Property(t => t.UserInRoleId).HasColumnName("UserInRoleId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.Role)
                .WithMany(t => t.UserInRole)
                .HasForeignKey(d => d.RoleId);
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserInRole)
                .HasForeignKey(d => d.UserId);

        }
    }
}
