﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models.UserManagement.Mapping
{
    public class UserLoginHistoryMap : EntityTypeConfiguration<UserLoginHistory>
    {
        public UserLoginHistoryMap()
        {
            // Table & Column Mappings
            this.ToTable("UM_UserLoginHistory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.LoginTime).HasColumnName("LoginTime");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.ModuleName).HasColumnName("ModuleName");
        }
    }
}
