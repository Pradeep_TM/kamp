using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace KAMP.Core.Repository.UM
{
    public class PermissionMap : EntityTypeConfiguration<Permission>
    {
        public PermissionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_Permissions");
            this.Property(t => t.Id).HasColumnName("PermissionId");
            this.Property(t => t.PermissionName).HasColumnName("PermissionName");
        }
    }
}
