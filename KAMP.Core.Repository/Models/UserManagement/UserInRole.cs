using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.UM
{
    public partial class UserInRole
    {
        public long UserInRoleId { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public bool IsActive { get; set; }
        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
