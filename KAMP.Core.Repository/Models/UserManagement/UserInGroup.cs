using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.UM
{
    public partial class UserInGroup
    {
        public long UserInGroupId { get; set; }
        public long UserId { get; set; }
        public long GroupId { get; set; }
        public int ModuleTypeId { get; set; }
        public bool IsActive { get; set; }
        public virtual Module Module { get; set; }
        public virtual Group Group { get; set; }
        public virtual User User { get; set; }
    }
}
