
using System;
using System.Collections.Generic;

namespace KAMP.Core.Repository.UM
{
    public partial class Role
    {
        public Role()
        {

        }

        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public long ModifiedBy { get; set; }
        public virtual ICollection<RoleInPermission> RoleInPermission { get; set; }
        public virtual ICollection<UserInRole> UserInRole { get; set; }
    }
}
