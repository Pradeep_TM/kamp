﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.Repository.Models
{
    public class UploadType
    {
        public int UploadTypeID { get; set; }
        public string UploadTypeName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public virtual ICollection<DataManagement> DataManagements { get; set; }
    }
}
