using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_TransnormLookUp
    {
        public int Id { get; set; }
        public int TransnormLookUpGrpupId { get; set; }
        public string Value { get; set; }
        public string Reference { get; set; }
    }
}
