using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_Accounts
    {
        public int ID { get; set; }
        public string OP_PartyName { get; set; }
        public string Pr_PartyName { get; set; }
        public string accountNumber { get; set; }
        public string accountSystemRN { get; set; }
        public string accountID { get; set; }
        public string accountOpenDate { get; set; }
        public string BookingCo { get; set; }
        public string accountCategoryRN { get; set; }
        public string accountClassification { get; set; }
        public string codIndicator { get; set; }
        public string lastJournalDate { get; set; }
        public string PositionOrBalance { get; set; }
        public string OP_Derived_TradingName { get; set; }
        public string OP_Site { get; set; }
        public string OP_Site_streetAddress { get; set; }
        public string OP_Site_city { get; set; }
        public string OP_Site_state { get; set; }
        public string OP_SiteCountry { get; set; }
        public string OP_Site_PostalCode { get; set; }
        public string OP_Site_ValidationStatus { get; set; }
        public string OP_Site_ValidationSource { get; set; }
        public string OP_Site_ValidationTS { get; set; }
        public string OP_PartyID { get; set; }
        public string OP_PartyName1 { get; set; }
        public string OP_countryOfJurisdiction { get; set; }
        public string OP_legalForm { get; set; }
        public string OP_capClass { get; set; }
        public string OP_Has_RelianceAgreement__USN_ { get; set; }
        public string OP_Accounts { get; set; }
        public string Pr_Site { get; set; }
        public string PR_Site_streetAddress { get; set; }
        public string PR_Site_city { get; set; }
        public string PR_Site_state { get; set; }
        public string Pr_SiteCountry { get; set; }
        public string PR_Site_PostalCode { get; set; }
        public string PR_Site_ValidationStatus { get; set; }
        public string PR_Site_ValidationSource { get; set; }
        public string PR_Site_ValidationTS { get; set; }
        public string Pr_PartyID { get; set; }
        public string Pr_PartyName1 { get; set; }
        public string Pr_CountryOfJurisdiction { get; set; }
        public string Pr_legalForm { get; set; }
        public string Pr_capClass { get; set; }
        public string rrID { get; set; }
        public string rrName { get; set; }
        public string rrBranchLocation { get; set; }
        public string rrBranch { get; set; }
        public string rrDepartment { get; set; }
        public string rrCostCenter { get; set; }
        public string rrCC { get; set; }
        public string rrBU { get; set; }
        public string rrBG { get; set; }
        public string rrRegion { get; set; }
        public string accountName { get; set; }
        public string accountMailingAddress { get; set; }
        public string MailingAddresscity { get; set; }
        public string MailingAddressstate { get; set; }
        public string MailingAddresscountry { get; set; }
        public string MailingAddresspostalCode { get; set; }
        public string SalesPersonName { get; set; }
        public string SalesPersonNotes { get; set; }
        public string GeneralProduct { get; set; }
        public string SpecificProduct { get; set; }
        public string BusinessDefinitionID { get; set; }
        public string BusinessDefinitionDescription { get; set; }
        public string Supervising_Officer_Name { get; set; }
        public Nullable<int> SalesPersonID { get; set; }
        public string DataFile { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
        public string AccountStatus { get; set; }
        public string AxiomOPTNID { get; set; }
        public string AxiomOPTN { get; set; }
        public string AxiomPMTNID { get; set; }
        public string AxiomPMTN { get; set; }
        public string DerivedOPTNID { get; set; }
        public string DerivedOPTN { get; set; }
        public string DesiTNID { get; set; }
        public string DesiTN { get; set; }
        public string groupDesi { get; set; }
        public string DesiAccount { get; set; }
        public string DesiStatus { get; set; }
        public string DesiName { get; set; }
        public string businessDefDivision { get; set; }
        public Nullable<int> newAccount { get; set; }
        public string newAccountDataFile { get; set; }
    }
}
