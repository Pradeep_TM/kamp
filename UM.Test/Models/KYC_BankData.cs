using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_BankData
    {
        public string Id { get; set; }
        public string CaseNo { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string RelationshipManagerName { get; set; }
        public string LegalFormID { get; set; }
        public string ClientType { get; set; }
        public string ActionSteps { get; set; }
        public string Risk_Rating { get; set; }
        public Nullable<int> EntityTypeID { get; set; }
        public Nullable<int> RegulatoryStatusID { get; set; }
        public Nullable<int> SuitabilityClassificationID { get; set; }
        public int HighRiskAttributeID { get; set; }
        public string RMactionSteps { get; set; }
        public string DoneActionSteps { get; set; }
        public string Registered_Address { get; set; }
        public string Correspondence_Address { get; set; }
        public bool PIC_Indicator { get; set; }
        public bool OFAC_Indicator { get; set; }
        public string Country_of_Incorporation { get; set; }
        public string Country_of_Operation { get; set; }
        public Nullable<bool> ForeignFI { get; set; }
        public Nullable<bool> GAOS { get; set; }
        public Nullable<System.DateTime> CommitedDate { get; set; }
        public string TIN { get; set; }
        public string SICCode { get; set; }
        public string RiskClass { get; set; }
        public string RegisteredJurisdiction { get; set; }
        public string CorespJurisdiction { get; set; }
        public Nullable<bool> NewEntity { get; set; }
        public string RiskRanking { get; set; }
        public string GovtIDSource { get; set; }
        public string RoleOfClient { get; set; }
        public string USAPATROIT { get; set; }
        public Nullable<int> LegalFormsID { get; set; }
        public Nullable<int> CapacityClassificationID { get; set; }
        public Nullable<int> ProductsAndServicesID { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string RegulatorName { get; set; }
        public string RegulatedJurisdiction { get; set; }
        public string HasSDDForm { get; set; }
        public string PEP { get; set; }
        public string Grandfathered { get; set; }
        public string USD { get; set; }
        public string USX { get; set; }
        public string USN { get; set; }
        public string FFI { get; set; }
        public string GamingEntity { get; set; }
        public string EDD { get; set; }
        public string HRJ { get; set; }
        public Nullable<int> ActiveAccounts { get; set; }
        public int C_tempID { get; set; }
        public string CapacityClassDesc { get; set; }
        public string LegalFormDesc { get; set; }
        public string Coded { get; set; }
        public string SeriesinName { get; set; }
        public string OrderPlacerOP { get; set; }
        public string OPAccounts { get; set; }
        public string PrincipalPR { get; set; }
        public string PRAccounts { get; set; }
        public string validationStatus { get; set; }
        public string validationSource { get; set; }
        public string validationTS { get; set; }
        public string countryOfJurisdiction { get; set; }
        public string SDD { get; set; }
        public string CoJG6 { get; set; }
        public string siteIsG6 { get; set; }
        public string governmentIDType { get; set; }
        public string USSocialSecurityNum { get; set; }
        public string passportId { get; set; }
        public string ExchangeListed { get; set; }
        public string ExchangeMktCountry { get; set; }
        public string ExchangeMktName { get; set; }
        public string marketSegment { get; set; }
        public string HQsiteID { get; set; }
        public string HQsiteCountry { get; set; }
        public string HQcity { get; set; }
        public string HQstate { get; set; }
        public string HQpostalCode { get; set; }
        public string HQsiteValidationStatus { get; set; }
        public string HQsiteValidationSource { get; set; }
        public string HQsiteValidationTS { get; set; }
        public string ListedOnExchange { get; set; }
        public string WebSiteAddress { get; set; }
        public string OFACPerformed { get; set; }
        public string TaxStatusCertification { get; set; }
        public Nullable<System.DateTime> OFACSearchDate { get; set; }
        public Nullable<System.DateTime> PATRIOTExpireDate { get; set; }
        public string USSSN { get; set; }
        public string HQCountry { get; set; }
        public string Order_Placer__OP_ { get; set; }
        public string Principal__PR_ { get; set; }
        public string ProductsAndServices { get; set; }
        public string InternalWatchListSearch { get; set; }
        public Nullable<System.DateTime> DateofInternalWatchListSearch { get; set; }
        public string HQStreetAddress { get; set; }
        public Nullable<bool> GDSProofofRegulationorListing { get; set; }
        public Nullable<bool> GDSFormationDocument { get; set; }
        public Nullable<bool> GDSDisclosureDocument { get; set; }
        public Nullable<bool> GDSTaxStatusCertificationYesNo { get; set; }
        public Nullable<bool> GDSListingofShareholders { get; set; }
        public Nullable<bool> GDSListingofPrincipals { get; set; }
        public Nullable<bool> GDSListingofBoardofDirectors { get; set; }
        public Nullable<bool> GDSAnIndividualsIDdocumentation { get; set; }
        public Nullable<bool> GDSNewssearchpertainingtothecustomer { get; set; }
        public Nullable<bool> GDSNewssearchpertainingtorelatedparties { get; set; }
        public Nullable<bool> GDSOFACsearches { get; set; }
        public Nullable<bool> GDSPEPsearches { get; set; }
        public Nullable<bool> GDSInternalWatchListsearches { get; set; }
        public Nullable<bool> GDSUSAPATRIOTActCertification { get; set; }
        public Nullable<bool> GDSOtherCorroborativeEvidence { get; set; }
        public Nullable<bool> GDSSourceofInformationforAMLProgram { get; set; }
        public Nullable<bool> GDSProofofparents { get; set; }
        public Nullable<bool> GDSProofofrelationshipwithparent { get; set; }
        public Nullable<bool> GDSSourceofReferralnewaccounts { get; set; }
        public Nullable<bool> GDSBankingReferenceInformation { get; set; }
        public Nullable<bool> GDSSiteVisitReportsnewaccounts { get; set; }
        public Nullable<bool> GDSRelianceContract { get; set; }
        public Nullable<bool> DDProofofRegulationorListing { get; set; }
        public Nullable<bool> DDFormationDocument { get; set; }
        public Nullable<bool> DDDisclosureDocument { get; set; }
        public Nullable<bool> DDTaxStatusCertificationYesNo { get; set; }
        public Nullable<bool> DDListingofShareholders { get; set; }
        public Nullable<bool> DDListingofPrincipals { get; set; }
        public Nullable<bool> DDListingofBoardofDirectors { get; set; }
        public Nullable<bool> DDAnIndividualsIDdocumentation { get; set; }
        public Nullable<bool> DDNewssearchpertainingtothecustomer { get; set; }
        public Nullable<bool> DDNewssearchpertainingtorelatedparties { get; set; }
        public Nullable<bool> DDOFACsearches { get; set; }
        public Nullable<bool> DDPEPsearches { get; set; }
        public Nullable<bool> DDInternalWatchListsearches { get; set; }
        public Nullable<bool> DDUSAPATRIOTActCertification { get; set; }
        public Nullable<bool> DDOtherCorroborativeEvidence { get; set; }
        public Nullable<bool> DDSourceofInformationforAMLProgram { get; set; }
        public Nullable<bool> DDProofofparents { get; set; }
        public Nullable<bool> DDProofofrelationshipwithparent { get; set; }
        public Nullable<bool> DDSourceofReferralnewaccounts { get; set; }
        public Nullable<bool> DDBankingReferenceInformation { get; set; }
        public Nullable<bool> DDSiteVisitReportsnewaccounts { get; set; }
        public Nullable<bool> DDRelianceContract { get; set; }
        public Nullable<bool> GDSSDDForm { get; set; }
        public Nullable<bool> GDSEDDForm { get; set; }
        public Nullable<bool> GDS312Form { get; set; }
        public Nullable<bool> GDSPEPForm { get; set; }
        public Nullable<bool> DDSDDForm { get; set; }
        public Nullable<bool> DDEDDForm { get; set; }
        public Nullable<bool> DD312Form { get; set; }
        public Nullable<bool> DDPEPForm { get; set; }
        public string PEPSearch { get; set; }
        public Nullable<System.DateTime> DateofPEPSearch { get; set; }
        public string NegetiveNewsSearch { get; set; }
        public Nullable<System.DateTime> NegetiveNewsSearchDate { get; set; }
        public string NegativeNewsIdentified { get; set; }
        public string NumSalesPeople { get; set; }
        public string SingleSalesPersonName { get; set; }
        public Nullable<bool> FirstLevelQA { get; set; }
        public Nullable<int> FirstLevelQAByU_ID { get; set; }
        public Nullable<bool> SecondLevelQA { get; set; }
        public Nullable<int> SecondLevelQAByU_ID { get; set; }
        public Nullable<bool> BankReviewed { get; set; }
        public Nullable<int> BankReviewedByU_ID { get; set; }
        public Nullable<bool> RevisedPerBankComments { get; set; }
        public Nullable<int> RevisedPerBankCommentsByU_ID { get; set; }
        public Nullable<System.DateTime> FirstLevelQADate { get; set; }
        public Nullable<System.DateTime> SecondLevelQADate { get; set; }
        public Nullable<System.DateTime> BankReviewedDate { get; set; }
        public Nullable<System.DateTime> RevisedPerBankCommentsDate { get; set; }
        public Nullable<bool> SUITQuestion { get; set; }
        public Nullable<int> SUITQuestionByU_ID { get; set; }
        public Nullable<System.DateTime> SUITQuestionDate { get; set; }
        public string DataGatherer { get; set; }
        public string Region { get; set; }
        public string CashAccounts { get; set; }
        public string DataGathererLastUpdated { get; set; }
        public Nullable<int> RfiTypeID { get; set; }
        public Nullable<int> NewParty { get; set; }
        public string NewPartyDataFile { get; set; }
        public string Lead_OP_ID { get; set; }
        public Nullable<bool> IsHedgeFund { get; set; }
        public Nullable<int> RoleOfClientID { get; set; }
        public string PrimaryParty { get; set; }
        public string PrimaryPartyName { get; set; }
        public string SubAccount { get; set; }
        public string EntityType { get; set; }
        public string GeographicRisk { get; set; }
        public string ProductType { get; set; }
        public string AdditionalHighRiskFactor { get; set; }
        public string RiskProfile { get; set; }
        public Nullable<System.DateTime> DateOfFormation { get; set; }
        public string C312EDDIdentity { get; set; }
        public string AddressDescription { get; set; }
        public string StreetAddr1 { get; set; }
        public string StreetAddr2 { get; set; }
        public string StreetAddr4 { get; set; }
        public string StreetAddr3 { get; set; }
        public string CountryCode { get; set; }
        public string Correspondence_City { get; set; }
        public string Correspondence_State { get; set; }
        public string Correspondence_Zip { get; set; }
        public string RemediationPopulation { get; set; }
        public string OffshoreLicense { get; set; }
        public string ForeignBank { get; set; }
        public string FBCSignatureDate { get; set; }
        public string USTaxIDMissing { get; set; }
        public string DateSigned { get; set; }
        public string DupeID { get; set; }
        public string AMLApproved { get; set; }
        public string GlobalRiskRating { get; set; }
        public string ReasonForEscalation { get; set; }
        public string Analyst { get; set; }
        public string C2ndReviewer { get; set; }
        public string RID { get; set; }
        public string NucleusID { get; set; }
        public string QA1 { get; set; }
        public string QA2 { get; set; }
        public string QA3 { get; set; }
        public string QA4 { get; set; }
        public string QA5 { get; set; }
        public string QA6 { get; set; }
        public string QA7 { get; set; }
        public string QA8 { get; set; }
        public string QA9 { get; set; }
        public string QA10 { get; set; }
        public string QA11 { get; set; }
        public string QA12 { get; set; }
        public string QA13 { get; set; }
        public string QA14 { get; set; }
        public string QA15 { get; set; }
        public string QA16 { get; set; }
        public string QA17 { get; set; }
        public string QA18 { get; set; }
        public string QA19 { get; set; }
        public string QA20 { get; set; }
        public string QA21 { get; set; }
        public string QA22 { get; set; }
        public string QA23 { get; set; }
        public Nullable<System.DateTime> DateAssigned { get; set; }
        public Nullable<System.DateTime> DateCompleted { get; set; }
        public string DateAssigned2nd { get; set; }
        public string DateCompleted2nd { get; set; }
        public string InvestmentManager { get; set; }
        public string InvestmentManagerCountry { get; set; }
        public string RequestType { get; set; }
        public string PEPsFound { get; set; }
        public string PEPLogged { get; set; }
        public string CustomerID { get; set; }
        public string CustomerIDSource { get; set; }
        public string RequestStatus { get; set; }
        public Nullable<int> cf_RFI_Status_LKUP { get; set; }
        public Nullable<bool> PSG_Client { get; set; }
        public Nullable<System.DateTime> cf_RFI_Status_Dt { get; set; }
        public string Borrower { get; set; }
        public Nullable<bool> BorrowerNonPrimary { get; set; }
        public Nullable<System.DateTime> IA_LastModifiedDate { get; set; }
        public Nullable<System.DateTime> Parent_LastModifiedDate { get; set; }
        public Nullable<System.DateTime> EntityLastModifiedDate { get; set; }
        public Nullable<System.DateTime> EntityCreatedDate { get; set; }
    }
}
