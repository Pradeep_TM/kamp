using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using UM.Test.Models.Mapping;

namespace UM.Test.Models
{
    public partial class KAMPContext : DbContext
    {
        static KAMPContext()
        {
            Database.SetInitializer<KAMPContext>(null);
        }

        public KAMPContext()
            : base("Name=KAMPContext")
        {
        }

        public DbSet<AUDIT_ConfigurationTables> AUDIT_ConfigurationTables { get; set; }
        public DbSet<AUDIT_EntityModelXmls> AUDIT_EntityModelXmls { get; set; }
        public DbSet<AUDIT_LogDetails> AUDIT_LogDetails { get; set; }
        public DbSet<AUDIT_Logs> AUDIT_Logs { get; set; }
        public DbSet<CORE_Clients> CORE_Clients { get; set; }
        public DbSet<CORE_Engagement> CORE_Engagement { get; set; }
        public DbSet<CORE_ModuleType> CORE_ModuleType { get; set; }
        public DbSet<CORE_ModuleValidationTypeMapping> CORE_ModuleValidationTypeMapping { get; set; }
        public DbSet<KYC_Accounts> KYC_Accounts { get; set; }
        public DbSet<KYC_AdditionalHighRiskFactor> KYC_AdditionalHighRiskFactor { get; set; }
        public DbSet<KYC_BankData> KYC_BankData { get; set; }
        public DbSet<KYC_CaseFile> KYC_CaseFile { get; set; }
        public DbSet<KYC_DocumentCode> KYC_DocumentCode { get; set; }
        public DbSet<KYC_DuplicateCase> KYC_DuplicateCase { get; set; }
        public DbSet<KYC_FileLog> KYC_FileLog { get; set; }
        public DbSet<KYC_FormElement> KYC_FormElement { get; set; }
        public DbSet<KYC_FormElementOption> KYC_FormElementOption { get; set; }
        public DbSet<KYC_KPMGEditable> KYC_KPMGEditable { get; set; }
        public DbSet<KYC_PartyType> KYC_PartyType { get; set; }
        public DbSet<KYC_ReqCategory> KYC_ReqCategory { get; set; }
        public DbSet<KYC_ReqClassification> KYC_ReqClassification { get; set; }
        public DbSet<KYC_ReqGridColumn> KYC_ReqGridColumn { get; set; }
        public DbSet<KYC_ReqOption> KYC_ReqOption { get; set; }
        public DbSet<KYC_ReqType> KYC_ReqType { get; set; }
        public DbSet<KYC_RequestForInformation> KYC_RequestForInformation { get; set; }
        public DbSet<KYC_Requirement> KYC_Requirement { get; set; }
        public DbSet<KYC_Response> KYC_Response { get; set; }
        public DbSet<KYC_ResponseGridCell> KYC_ResponseGridCell { get; set; }
        public DbSet<KYC_ResponseOption> KYC_ResponseOption { get; set; }
        public DbSet<KYC_RFIAttachments> KYC_RFIAttachments { get; set; }
        public DbSet<KYC_RFIResponses> KYC_RFIResponses { get; set; }
        public DbSet<lkp_AnalysisType> lkp_AnalysisType { get; set; }
        public DbSet<MV_CaseFile> MV_CaseFile { get; set; }
        public DbSet<MV_Resultant> MV_Resultant { get; set; }
        public DbSet<MV_Routine> MV_Routine { get; set; }
        public DbSet<MV_SQLQuery> MV_SQLQuery { get; set; }
        public DbSet<MV_TransNorm> MV_TransNorm { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<TLB_AssesmentField> TLB_AssesmentField { get; set; }
        public DbSet<TLB_Assesments> TLB_Assesments { get; set; }
        public DbSet<TLB_AssesmentStoredProcedures> TLB_AssesmentStoredProcedures { get; set; }
        public DbSet<TLB_AssesmentType> TLB_AssesmentType { get; set; }
        public DbSet<TLB_Batch> TLB_Batch { get; set; }
        public DbSet<TLB_BatchSteps> TLB_BatchSteps { get; set; }
        public DbSet<TLB_BatchTypes> TLB_BatchTypes { get; set; }
        public DbSet<TLB_BinaryLargeObject> TLB_BinaryLargeObject { get; set; }
        public DbSet<TLB_CaseAssesmentNote> TLB_CaseAssesmentNote { get; set; }
        public DbSet<TLB_CaseClientRelated> TLB_CaseClientRelated { get; set; }
        public DbSet<TLB_CaseFile> TLB_CaseFile { get; set; }
        public DbSet<TLB_CaseRelatedTransaction> TLB_CaseRelatedTransaction { get; set; }
        public DbSet<TLB_CaseTransactions> TLB_CaseTransactions { get; set; }
        public DbSet<TLB_ClientAccounts> TLB_ClientAccounts { get; set; }
        public DbSet<TLB_ClientNameVariations> TLB_ClientNameVariations { get; set; }
        public DbSet<TLB_History> TLB_History { get; set; }
        public DbSet<TLB_HistoryDefaultMessage> TLB_HistoryDefaultMessage { get; set; }
        public DbSet<TLB_HistoryGroups> TLB_HistoryGroups { get; set; }
        public DbSet<TLB_HistoryVariables> TLB_HistoryVariables { get; set; }
        public DbSet<TLB_LookUp> TLB_LookUp { get; set; }
        public DbSet<TLB_LookUpGroups> TLB_LookUpGroups { get; set; }
        public DbSet<TLB_ParameterAlias> TLB_ParameterAlias { get; set; }
        public DbSet<TLB_ParameterGroups> TLB_ParameterGroups { get; set; }
        public DbSet<TLB_Parameters> TLB_Parameters { get; set; }
        public DbSet<TLB_ScoresAccounts> TLB_ScoresAccounts { get; set; }
        public DbSet<TLB_ScoresTransactions> TLB_ScoresTransactions { get; set; }
        public DbSet<TLB_TransNorm> TLB_TransNorm { get; set; }
        public DbSet<TLB_TransnormLookUp> TLB_TransnormLookUp { get; set; }
        public DbSet<TLB_TransnormLookUpGroups> TLB_TransnormLookUpGroups { get; set; }
        public DbSet<TLB_TransTotalScore> TLB_TransTotalScore { get; set; }
        public DbSet<UM_Groups> UM_Groups { get; set; }
        public DbSet<UM_ModuleObjects> UM_ModuleObjects { get; set; }
        public DbSet<UM_PermissionInSet> UM_PermissionInSet { get; set; }
        public DbSet<UM_Permissions> UM_Permissions { get; set; }
        public DbSet<UM_PermissionSet> UM_PermissionSet { get; set; }
        public DbSet<UM_RoleInPermission> UM_RoleInPermission { get; set; }
        public DbSet<UM_Roles> UM_Roles { get; set; }
        public DbSet<UM_UserInGroup> UM_UserInGroup { get; set; }
        public DbSet<UM_UserInRole> UM_UserInRole { get; set; }
        public DbSet<UM_Users> UM_Users { get; set; }
        public DbSet<WF_Activity> WF_Activity { get; set; }
        public DbSet<WF_AssignedTo> WF_AssignedTo { get; set; }
        public DbSet<WF_Assignment> WF_Assignment { get; set; }
        public DbSet<WF_Definition> WF_Definition { get; set; }
        public DbSet<WF_Documents> WF_Documents { get; set; }
        public DbSet<WF_Lookup_Action> WF_Lookup_Action { get; set; }
        public DbSet<WF_States> WF_States { get; set; }
        public DbSet<WF_Steps> WF_Steps { get; set; }
        public DbSet<WF_WorkflowTracking> WF_WorkflowTracking { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AUDIT_ConfigurationTablesMap());
            modelBuilder.Configurations.Add(new AUDIT_EntityModelXmlsMap());
            modelBuilder.Configurations.Add(new AUDIT_LogDetailsMap());
            modelBuilder.Configurations.Add(new AUDIT_LogsMap());
            modelBuilder.Configurations.Add(new CORE_ClientsMap());
            modelBuilder.Configurations.Add(new CORE_EngagementMap());
            modelBuilder.Configurations.Add(new CORE_ModuleTypeMap());
            modelBuilder.Configurations.Add(new CORE_ModuleValidationTypeMappingMap());
            modelBuilder.Configurations.Add(new KYC_AccountsMap());
            modelBuilder.Configurations.Add(new KYC_AdditionalHighRiskFactorMap());
            modelBuilder.Configurations.Add(new KYC_BankDataMap());
            modelBuilder.Configurations.Add(new KYC_CaseFileMap());
            modelBuilder.Configurations.Add(new KYC_DocumentCodeMap());
            modelBuilder.Configurations.Add(new KYC_DuplicateCaseMap());
            modelBuilder.Configurations.Add(new KYC_FileLogMap());
            modelBuilder.Configurations.Add(new KYC_FormElementMap());
            modelBuilder.Configurations.Add(new KYC_FormElementOptionMap());
            modelBuilder.Configurations.Add(new KYC_KPMGEditableMap());
            modelBuilder.Configurations.Add(new KYC_PartyTypeMap());
            modelBuilder.Configurations.Add(new KYC_ReqCategoryMap());
            modelBuilder.Configurations.Add(new KYC_ReqClassificationMap());
            modelBuilder.Configurations.Add(new KYC_ReqGridColumnMap());
            modelBuilder.Configurations.Add(new KYC_ReqOptionMap());
            modelBuilder.Configurations.Add(new KYC_ReqTypeMap());
            modelBuilder.Configurations.Add(new KYC_RequestForInformationMap());
            modelBuilder.Configurations.Add(new KYC_RequirementMap());
            modelBuilder.Configurations.Add(new KYC_ResponseMap());
            modelBuilder.Configurations.Add(new KYC_ResponseGridCellMap());
            modelBuilder.Configurations.Add(new KYC_ResponseOptionMap());
            modelBuilder.Configurations.Add(new KYC_RFIAttachmentsMap());
            modelBuilder.Configurations.Add(new KYC_RFIResponsesMap());
            modelBuilder.Configurations.Add(new lkp_AnalysisTypeMap());
            modelBuilder.Configurations.Add(new MV_CaseFileMap());
            modelBuilder.Configurations.Add(new MV_ResultantMap());
            modelBuilder.Configurations.Add(new MV_RoutineMap());
            modelBuilder.Configurations.Add(new MV_SQLQueryMap());
            modelBuilder.Configurations.Add(new MV_TransNormMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new TLB_AssesmentFieldMap());
            modelBuilder.Configurations.Add(new TLB_AssesmentsMap());
            modelBuilder.Configurations.Add(new TLB_AssesmentStoredProceduresMap());
            modelBuilder.Configurations.Add(new TLB_AssesmentTypeMap());
            modelBuilder.Configurations.Add(new TLB_BatchMap());
            modelBuilder.Configurations.Add(new TLB_BatchStepsMap());
            modelBuilder.Configurations.Add(new TLB_BatchTypesMap());
            modelBuilder.Configurations.Add(new TLB_BinaryLargeObjectMap());
            modelBuilder.Configurations.Add(new TLB_CaseAssesmentNoteMap());
            modelBuilder.Configurations.Add(new TLB_CaseClientRelatedMap());
            modelBuilder.Configurations.Add(new TLB_CaseFileMap());
            modelBuilder.Configurations.Add(new TLB_CaseRelatedTransactionMap());
            modelBuilder.Configurations.Add(new TLB_CaseTransactionsMap());
            modelBuilder.Configurations.Add(new TLB_ClientAccountsMap());
            modelBuilder.Configurations.Add(new TLB_ClientNameVariationsMap());
            modelBuilder.Configurations.Add(new TLB_HistoryMap());
            modelBuilder.Configurations.Add(new TLB_HistoryDefaultMessageMap());
            modelBuilder.Configurations.Add(new TLB_HistoryGroupsMap());
            modelBuilder.Configurations.Add(new TLB_HistoryVariablesMap());
            modelBuilder.Configurations.Add(new TLB_LookUpMap());
            modelBuilder.Configurations.Add(new TLB_LookUpGroupsMap());
            modelBuilder.Configurations.Add(new TLB_ParameterAliasMap());
            modelBuilder.Configurations.Add(new TLB_ParameterGroupsMap());
            modelBuilder.Configurations.Add(new TLB_ParametersMap());
            modelBuilder.Configurations.Add(new TLB_ScoresAccountsMap());
            modelBuilder.Configurations.Add(new TLB_ScoresTransactionsMap());
            modelBuilder.Configurations.Add(new TLB_TransNormMap());
            modelBuilder.Configurations.Add(new TLB_TransnormLookUpMap());
            modelBuilder.Configurations.Add(new TLB_TransnormLookUpGroupsMap());
            modelBuilder.Configurations.Add(new TLB_TransTotalScoreMap());
            modelBuilder.Configurations.Add(new UM_GroupsMap());
            modelBuilder.Configurations.Add(new UM_ModuleObjectsMap());
            modelBuilder.Configurations.Add(new UM_PermissionInSetMap());
            modelBuilder.Configurations.Add(new UM_PermissionsMap());
            modelBuilder.Configurations.Add(new UM_PermissionSetMap());
            modelBuilder.Configurations.Add(new UM_RoleInPermissionMap());
            modelBuilder.Configurations.Add(new UM_RolesMap());
            modelBuilder.Configurations.Add(new UM_UserInGroupMap());
            modelBuilder.Configurations.Add(new UM_UserInRoleMap());
            modelBuilder.Configurations.Add(new UM_UsersMap());
            modelBuilder.Configurations.Add(new WF_ActivityMap());
            modelBuilder.Configurations.Add(new WF_AssignedToMap());
            modelBuilder.Configurations.Add(new WF_AssignmentMap());
            modelBuilder.Configurations.Add(new WF_DefinitionMap());
            modelBuilder.Configurations.Add(new WF_DocumentsMap());
            modelBuilder.Configurations.Add(new WF_Lookup_ActionMap());
            modelBuilder.Configurations.Add(new WF_StatesMap());
            modelBuilder.Configurations.Add(new WF_StepsMap());
            modelBuilder.Configurations.Add(new WF_WorkflowTrackingMap());
        }
    }
}
