using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class CORE_Clients
    {
        public CORE_Clients()
        {
            this.MV_Routine = new List<MV_Routine>();
            this.CORE_Engagement = new List<CORE_Engagement>();
        }

        public long Id { get; set; }
        public string ClientName { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<MV_Routine> MV_Routine { get; set; }
        public virtual ICollection<CORE_Engagement> CORE_Engagement { get; set; }
    }
}
