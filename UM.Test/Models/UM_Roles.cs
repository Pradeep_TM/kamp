using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_Roles
    {
        public UM_Roles()
        {
            this.UM_RoleInPermission = new List<UM_RoleInPermission>();
            this.UM_UserInRole = new List<UM_UserInRole>();
        }

        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public long ModifiedBy { get; set; }
        public virtual ICollection<UM_RoleInPermission> UM_RoleInPermission { get; set; }
        public virtual ICollection<UM_UserInRole> UM_UserInRole { get; set; }
    }
}
