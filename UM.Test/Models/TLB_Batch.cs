using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_Batch
    {
        public int Id { get; set; }
        public int AssesmentId { get; set; }
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string EndTime { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool Approved { get; set; }
        public string ServerJobId { get; set; }
    }
}
