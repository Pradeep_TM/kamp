using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_ScoresAccounts
    {
        public int Id { get; set; }
        public string TransactionAccount { get; set; }
        public Nullable<int> AssesmentId { get; set; }
        public Nullable<int> ParameterId { get; set; }
        public Nullable<int> TransactionId { get; set; }
        public string FIELD { get; set; }
        public string FilterCriteriaId { get; set; }
        public Nullable<bool> IsScored { get; set; }
    }
}
