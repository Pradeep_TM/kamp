using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class CORE_Engagement
    {
        public CORE_Engagement()
        {
            this.CORE_ModuleValidationTypeMapping = new List<CORE_ModuleValidationTypeMapping>();
        }

        public long Id { get; set; }
        public string EngagementName { get; set; }
        public string EngagementNumber { get; set; }
        public long ClientID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public virtual CORE_Clients CORE_Clients { get; set; }
        public virtual ICollection<CORE_ModuleValidationTypeMapping> CORE_ModuleValidationTypeMapping { get; set; }
    }
}
