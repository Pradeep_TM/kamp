using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class MV_CaseFile
    {
        public int CfId { get; set; }
        public string CaseNo { get; set; }
        public int TransNormID { get; set; }
        public int TransNormNo { get; set; }
        public string AMLCaseKey { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string RiskProfile { get; set; }
        public int UserId { get; set; }
        public int StatusId { get; set; }
        public string DueDiligence { get; set; }
        public string MemorandumOfFact { get; set; }
        public Nullable<int> TransactionCount { get; set; }
        public Nullable<int> TransactionScore { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifedOn { get; set; }
        public virtual MV_TransNorm MV_TransNorm { get; set; }
    }
}
