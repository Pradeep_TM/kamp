using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_ReqOption
    {
        public long Id { get; set; }
        public Nullable<long> RequirementId { get; set; }
        public Nullable<long> GridColumnId { get; set; }
        public string OptionName { get; set; }
        public bool IsValid { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual KYC_Requirement KYC_Requirement { get; set; }
    }
}
