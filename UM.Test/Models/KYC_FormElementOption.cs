using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_FormElementOption
    {
        public int Id { get; set; }
        public int FormElementId { get; set; }
        public string OptionText { get; set; }
        public bool Valid { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual KYC_FormElement KYC_FormElement { get; set; }
    }
}
