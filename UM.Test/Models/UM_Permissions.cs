using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_Permissions
    {
        public UM_Permissions()
        {
            this.UM_PermissionInSet = new List<UM_PermissionInSet>();
        }

        public long PermissionId { get; set; }
        public string PermissionName { get; set; }
        public virtual ICollection<UM_PermissionInSet> UM_PermissionInSet { get; set; }
    }
}
