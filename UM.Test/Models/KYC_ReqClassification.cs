using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_ReqClassification
    {
        public KYC_ReqClassification()
        {
            this.KYC_ReqCategory = new List<KYC_ReqCategory>();
        }

        public int Id { get; set; }
        public string ClassificationName { get; set; }
        public bool IsValid { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual ICollection<KYC_ReqCategory> KYC_ReqCategory { get; set; }
    }
}
