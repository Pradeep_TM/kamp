using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_UserInRole
    {
        public long UserInRoleId { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public bool IsActive { get; set; }
        public virtual UM_Roles UM_Roles { get; set; }
        public virtual UM_Users UM_Users { get; set; }
    }
}
