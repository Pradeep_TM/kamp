using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_AssignedTo
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public string WorkflowStatus { get; set; }
        public long AssignmentId { get; set; }
        public string State { get; set; }
        public string Comments { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual WF_Assignment WF_Assignment { get; set; }
    }
}
