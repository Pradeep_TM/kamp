using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_ResponseGridCell
    {
        public KYC_ResponseGridCell()
        {
            this.KYC_ResponseOption = new List<KYC_ResponseOption>();
        }

        public long Id { get; set; }
        public long ResponseId { get; set; }
        public long ColumnId { get; set; }
        public int RowId { get; set; }
        public string Response { get; set; }
        public virtual KYC_ReqGridColumn KYC_ReqGridColumn { get; set; }
        public virtual KYC_Response KYC_Response { get; set; }
        public virtual ICollection<KYC_ResponseOption> KYC_ResponseOption { get; set; }
    }
}
