using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_HistoryDefaultMessage
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int HistoryGroupId { get; set; }
    }
}
