using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_Documents
    {
        public int Id { get; set; }
        public string DocumentName { get; set; }
        public byte[] DocumentContent { get; set; }
        public string DocumentType { get; set; }
        public int StepId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual WF_Steps WF_Steps { get; set; }
    }
}
