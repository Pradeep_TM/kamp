using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_RequestForInformation
    {
        public KYC_RequestForInformation()
        {
            this.KYC_RFIAttachments = new List<KYC_RFIAttachments>();
            this.KYC_RFIResponses = new List<KYC_RFIResponses>();
        }

        public int Id { get; set; }
        public string CfId { get; set; }
        public string SentTo { get; set; }
        public string SentFrom { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsAttachmentMail { get; set; }
        public bool IsResponseUploaded { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public System.DateTime SentDate { get; set; }
        public int CreatedBy { get; set; }
        public virtual ICollection<KYC_RFIAttachments> KYC_RFIAttachments { get; set; }
        public virtual ICollection<KYC_RFIResponses> KYC_RFIResponses { get; set; }
    }
}
