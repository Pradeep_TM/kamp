using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class AUDIT_LogDetails
    {
        public AUDIT_LogDetails()
        {
            this.AUDIT_Logs = new List<AUDIT_Logs>();
        }

        public int Id { get; set; }
        public string CfId { get; set; }
        public string EntityName { get; set; }
        public string CreatedBy { get; set; }
        public string DateTime { get; set; }
        public string Action { get; set; }
        public virtual ICollection<AUDIT_Logs> AUDIT_Logs { get; set; }
    }
}
