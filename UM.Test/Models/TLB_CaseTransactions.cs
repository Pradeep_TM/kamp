using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_CaseTransactions
    {
        public int Id { get; set; }
        public Nullable<int> TransactionId { get; set; }
        public Nullable<int> IsClear { get; set; }
        public Nullable<int> CfId { get; set; }
        public Nullable<bool> SAR { get; set; }
        public virtual TLB_CaseFile TLB_CaseFile { get; set; }
        public virtual TLB_TransNorm TLB_TransNorm { get; set; }
    }
}
