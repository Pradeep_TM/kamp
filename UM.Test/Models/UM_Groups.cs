using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_Groups
    {
        public UM_Groups()
        {
            this.UM_UserInGroup = new List<UM_UserInGroup>();
            this.WF_States = new List<WF_States>();
        }

        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual ICollection<UM_UserInGroup> UM_UserInGroup { get; set; }
        public virtual ICollection<WF_States> WF_States { get; set; }
    }
}
