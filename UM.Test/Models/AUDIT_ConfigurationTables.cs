using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class AUDIT_ConfigurationTables
    {
        public AUDIT_ConfigurationTables()
        {
            this.AUDIT_EntityModelXmls = new List<AUDIT_EntityModelXmls>();
        }

        public int Id { get; set; }
        public string EntityName { get; set; }
        public string ConfigurationXml { get; set; }
        public virtual ICollection<AUDIT_EntityModelXmls> AUDIT_EntityModelXmls { get; set; }
    }
}
