using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_AssesmentType
    {
        public TLB_AssesmentType()
        {
            this.TLB_Assesments = new List<TLB_Assesments>();
        }

        public int Id { get; set; }
        public string AssesmentTypeDescription { get; set; }
        public virtual ICollection<TLB_Assesments> TLB_Assesments { get; set; }
    }
}
