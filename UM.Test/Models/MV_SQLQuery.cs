using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class MV_SQLQuery
    {
        public long Id { get; set; }
        public long RoutineID { get; set; }
        public string QuerySyntax { get; set; }
        public string QueryName { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifedOn { get; set; }
        public virtual MV_Routine MV_Routine { get; set; }
    }
}
