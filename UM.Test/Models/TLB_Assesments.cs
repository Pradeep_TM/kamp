using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_Assesments
    {
        public TLB_Assesments()
        {
            this.TLB_CaseAssesmentNote = new List<TLB_CaseAssesmentNote>();
            this.TLB_ScoresTransactions = new List<TLB_ScoresTransactions>();
        }

        public int Id { get; set; }
        public int AssesmentTypeId { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public Nullable<int> AccountScore { get; set; }
        public Nullable<int> TransactionScore { get; set; }
        public Nullable<bool> Compiled { get; set; }
        public Nullable<bool> Approved { get; set; }
        public Nullable<int> AssessmentStoredProcedure_ID { get; set; }
        public Nullable<System.DateTime> RanOn { get; set; }
        public string BusinessRule { get; set; }
        public Nullable<bool> IsUsedInOneMonth { get; set; }
        public Nullable<bool> IsUsedInLookBack { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public string DescriptionOriginal { get; set; }
        public string StoredProcedureName { get; set; }
        public virtual TLB_AssesmentType TLB_AssesmentType { get; set; }
        public virtual ICollection<TLB_CaseAssesmentNote> TLB_CaseAssesmentNote { get; set; }
        public virtual ICollection<TLB_ScoresTransactions> TLB_ScoresTransactions { get; set; }
    }
}
