using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_FileLog
    {
        public int Id { get; set; }
        public string DocumentType { get; set; }
        public byte[] Content { get; set; }
        public int ContentLength { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public int PartyTypeId { get; set; }
        public int DocumentCodeId { get; set; }
        public int cf_ID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public virtual KYC_CaseFile KYC_CaseFile { get; set; }
        public virtual KYC_DocumentCode KYC_DocumentCode { get; set; }
        public virtual KYC_PartyType KYC_PartyType { get; set; }
    }
}
