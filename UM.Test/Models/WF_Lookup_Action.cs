using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_Lookup_Action
    {
        public WF_Lookup_Action()
        {
            this.WF_Activity = new List<WF_Activity>();
        }

        public int Id { get; set; }
        public string Action { get; set; }
        public virtual ICollection<WF_Activity> WF_Activity { get; set; }
    }
}
