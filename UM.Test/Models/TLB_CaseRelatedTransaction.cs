using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_CaseRelatedTransaction
    {
        public Nullable<int> CfId { get; set; }
        public string CfName { get; set; }
        public int TransactionId { get; set; }
        public string PartyName { get; set; }
        public string PartySource { get; set; }
    }
}
