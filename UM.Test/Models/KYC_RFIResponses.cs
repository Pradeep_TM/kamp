using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_RFIResponses
    {
        public int Id { get; set; }
        public int RFIId { get; set; }
        public byte[] ResponseUploaded { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public virtual KYC_RequestForInformation KYC_RequestForInformation { get; set; }
    }
}
