using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class MV_TransNorm
    {
        public MV_TransNorm()
        {
            this.MV_CaseFile = new List<MV_CaseFile>();
        }

        public int Id { get; set; }
        public int TranNo { get; set; }
        public string InternalReferenceNumber { get; set; }
        public string MessageType { get; set; }
        public string PaymentType { get; set; }
        public Nullable<decimal> PaymentAmount { get; set; }
        public Nullable<System.DateTime> TransDate { get; set; }
        public Nullable<System.DateTime> ValueDate { get; set; }
        public Nullable<int> RecvPay { get; set; }
        public string PartyType { get; set; }
        public string SourceCode { get; set; }
        public string CreditAdviceType { get; set; }
        public string MTReference { get; set; }
        public string FileName { get; set; }
        public string CustomerReference { get; set; }
        public string CustomerAccount1 { get; set; }
        public string CustomerAccount2 { get; set; }
        public string ByOrderPartyID { get; set; }
        public string ByOrderAcct { get; set; }
        public string ByOrderPartyBIC { get; set; }
        public string ByOrderPartyABA { get; set; }
        public string ByOrderPartyName { get; set; }
        public string ByOrderPartyNameNorm { get; set; }
        public string ByOrderPartyAddress { get; set; }
        public string ByOrderPartyStateName { get; set; }
        public string ByOrderPartyCountryName { get; set; }
        public string BenePartyID { get; set; }
        public string BenePartyAcct { get; set; }
        public string BenePartyBIC { get; set; }
        public string BenePartyABA { get; set; }
        public string BenePartyName { get; set; }
        public string BenePartyNameNorm { get; set; }
        public string BenePartyAddress { get; set; }
        public string BenePartyStateName { get; set; }
        public string BenePartyCountryName { get; set; }
        public string CreditBankID { get; set; }
        public string CreditBankAcct { get; set; }
        public string CreditBankBIC { get; set; }
        public string CreditBankABA { get; set; }
        public string CreditBankName { get; set; }
        public string CreditBankNameNorm { get; set; }
        public string CreditBankAddress { get; set; }
        public string CreditBankStateName { get; set; }
        public string CreditBankCountryName { get; set; }
        public string CreditBankAdvice { get; set; }
        public string CreditBankAdviceCountryName { get; set; }
        public string CreditBankInstructions { get; set; }
        public string CreditBankInstructionsCountryName { get; set; }
        public string IntermediaryBankID { get; set; }
        public string IntermediaryBankAcct { get; set; }
        public string IntermediaryBankBIC { get; set; }
        public string IntermediaryBankABA { get; set; }
        public string IntermediaryBankName { get; set; }
        public string IntermediaryBankNameNorm { get; set; }
        public string IntermediaryBankAddress { get; set; }
        public string IntermediaryBankStateName { get; set; }
        public string IntermediaryBankCountryName { get; set; }
        public string BeneficiaryBankID { get; set; }
        public string BeneficiaryBankAcct { get; set; }
        public string BeneficiaryBankABA { get; set; }
        public string BeneficiaryBankBIC { get; set; }
        public string BeneficiaryBankName { get; set; }
        public string BeneficiaryBankNameNorm { get; set; }
        public string BeneficiaryBankAddress { get; set; }
        public string BeneficiaryBankStateName { get; set; }
        public string BeneficiaryBankCountryName { get; set; }
        public string BeneficiaryBankAdvice { get; set; }
        public string BeneficiaryBankAdviceCountryName { get; set; }
        public string DebitBankID { get; set; }
        public string DebitBankAcct { get; set; }
        public string DebitBankBIC { get; set; }
        public string DebitBankABA { get; set; }
        public string DebitBankName { get; set; }
        public string DebitBankNameNorm { get; set; }
        public string DebitBankAddress { get; set; }
        public string DebitBankStateName { get; set; }
        public string DebitBankCountryName { get; set; }
        public string DebitBankInstructions { get; set; }
        public string DebitBankInstructionsCountryName { get; set; }
        public string OriginatingBankID { get; set; }
        public string OriginatingBankAcct { get; set; }
        public string OriginatingBankBIC { get; set; }
        public string OriginatingBankABA { get; set; }
        public string OriginatingBankName { get; set; }
        public string OriginatingBankNameNorm { get; set; }
        public string OriginatingBankAddress { get; set; }
        public string OriginatingBankStateName { get; set; }
        public string OriginatingBankCountryName { get; set; }
        public string OriginatortoBeneInfo { get; set; }
        public string OriginatortoBeneInfoCountryName { get; set; }
        public string BanktoBeneInfo { get; set; }
        public string BankToBeneInfoCountryName { get; set; }
        public string InstructingBankID { get; set; }
        public string InstructingBankAcct { get; set; }
        public string InstructingBankBIC { get; set; }
        public string InstructingBankABA { get; set; }
        public string InstructingBankName { get; set; }
        public string InstructingBankNameNorm { get; set; }
        public string InstructingBankAddress { get; set; }
        public string InstructingBankStateName { get; set; }
        public string InstructingBankCountryName { get; set; }
        public Nullable<int> CaseCount { get; set; }
        public Nullable<bool> CaseCheck { get; set; }
        public virtual ICollection<MV_CaseFile> MV_CaseFile { get; set; }
    }
}
