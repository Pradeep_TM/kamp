using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_AssesmentField
    {
        public int Id { get; set; }
        public int AssesmentId { get; set; }
        public string FieldName { get; set; }
        public Nullable<int> parameterGroupId { get; set; }
        public string FieldType { get; set; }
    }
}
