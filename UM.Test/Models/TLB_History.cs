using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_History
    {
        public int Id { get; set; }
        public Nullable<int> HistoryGroupId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string User { get; set; }
        public string Description { get; set; }
        public int FKey { get; set; }
        public byte[] HistoryBlob { get; set; }
        public string HistoryBlob_FileName { get; set; }
    }
}
