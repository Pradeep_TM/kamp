using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_CaseFile
    {
        public KYC_CaseFile()
        {
            this.KYC_FileLog = new List<KYC_FileLog>();
            this.KYC_KPMGEditable = new List<KYC_KPMGEditable>();
            this.KYC_Response = new List<KYC_Response>();
        }

        public int cf_ID { get; set; }
        public string CustomerNo { get; set; }
        public string CaseNo { get; set; }
        public string CustomerName { get; set; }
        public int CategoryId { get; set; }
        public string RiskProfile { get; set; }
        public Nullable<int> AnalystId { get; set; }
        public string Due_Diligence { get; set; }
        public string MemorandumOfFact { get; set; }
        public Nullable<int> TransactionCount { get; set; }
        public Nullable<int> TransactionScore { get; set; }
        public Nullable<int> AccountCount { get; set; }
        public Nullable<int> AccountScore { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public int DemoteCount { get; set; }
        public string ExecutiveSummary { get; set; }
        public int SARStatus { get; set; }
        public Nullable<int> HighStatusId { get; set; }
        public string PrimAMLRisk { get; set; }
        public string PrimSUITRisk { get; set; }
        public Nullable<bool> HasRelatedCases { get; set; }
        public Nullable<int> RFITypeId { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<int> RiskNumber { get; set; }
        public Nullable<int> HasNoDeriv { get; set; }
        public Nullable<int> PreliminaryRiskNumber { get; set; }
        public string PotentiallyCloseable { get; set; }
        public string WholeOPPotentiallyCloseable { get; set; }
        public string SPVHold { get; set; }
        public string CMTFlag { get; set; }
        public Nullable<bool> Valid { get; set; }
        public Nullable<bool> Duplicate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public virtual ICollection<KYC_FileLog> KYC_FileLog { get; set; }
        public virtual ICollection<KYC_KPMGEditable> KYC_KPMGEditable { get; set; }
        public virtual ICollection<KYC_Response> KYC_Response { get; set; }
    }
}
