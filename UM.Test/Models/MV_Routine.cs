using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class MV_Routine
    {
        public MV_Routine()
        {
            this.MV_Resultant = new List<MV_Resultant>();
            this.MV_SQLQuery = new List<MV_SQLQuery>();
            this.MV_Routine1 = new List<MV_Routine>();
        }

        public long Id { get; set; }
        public string RoutineName { get; set; }
        public string RoutineDescription { get; set; }
        public string Source { get; set; }
        public string Jurisdiction { get; set; }
        public Nullable<int> RoutineAccountScore { get; set; }
        public Nullable<int> RoutineTransactionScore { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<long> ParentRoutineID { get; set; }
        public Nullable<long> ClientID { get; set; }
        public virtual CORE_Clients CORE_Clients { get; set; }
        public virtual ICollection<MV_Resultant> MV_Resultant { get; set; }
        public virtual ICollection<MV_SQLQuery> MV_SQLQuery { get; set; }
        public virtual ICollection<MV_Routine> MV_Routine1 { get; set; }
        public virtual MV_Routine MV_Routine2 { get; set; }
    }
}
