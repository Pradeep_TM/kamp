using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_BatchTypes
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Score { get; set; }
        public Nullable<bool> ASSESS { get; set; }
    }
}
