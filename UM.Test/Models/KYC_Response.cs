using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_Response
    {
        public KYC_Response()
        {
            this.KYC_ResponseGridCell = new List<KYC_ResponseGridCell>();
            this.KYC_ResponseOption = new List<KYC_ResponseOption>();
        }

        public long Id { get; set; }
        public int cf_ID { get; set; }
        public string Response { get; set; }
        public long RequirementId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ResponseJson { get; set; }
        public short ResponseState { get; set; }
        public virtual KYC_CaseFile KYC_CaseFile { get; set; }
        public virtual KYC_Requirement KYC_Requirement { get; set; }
        public virtual ICollection<KYC_ResponseGridCell> KYC_ResponseGridCell { get; set; }
        public virtual ICollection<KYC_ResponseOption> KYC_ResponseOption { get; set; }
    }
}
