using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_PermissionInSet
    {
        public long PermissionInSetId { get; set; }
        public long PermissionId { get; set; }
        public long PermissionSetId { get; set; }
        public virtual UM_Permissions UM_Permissions { get; set; }
        public virtual UM_PermissionSet UM_PermissionSet { get; set; }
    }
}
