using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_States
    {
        public WF_States()
        {
            this.WF_Activity = new List<WF_Activity>();
            this.UM_Groups = new List<UM_Groups>();
        }

        public int Id { get; set; }
        public string StateName { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<WF_Activity> WF_Activity { get; set; }
        public virtual ICollection<UM_Groups> UM_Groups { get; set; }
    }
}
