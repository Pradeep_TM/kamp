using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_WorkflowTracking
    {
        public int Id { get; set; }
        public int ModuleTypeId { get; set; }
        public long ObjectId { get; set; }
        public long AssignmentId { get; set; }
        public Nullable<long> CFId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual WF_Assignment WF_Assignment { get; set; }
    }
}
