using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_UserInGroup
    {
        public long UserInGroupId { get; set; }
        public long UserId { get; set; }
        public long GroupId { get; set; }
        public int ModuleTypeId { get; set; }
        public bool IsActive { get; set; }
        public virtual CORE_ModuleType CORE_ModuleType { get; set; }
        public virtual UM_Groups UM_Groups { get; set; }
        public virtual UM_Users UM_Users { get; set; }
    }
}
