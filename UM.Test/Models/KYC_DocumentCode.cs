using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_DocumentCode
    {
        public KYC_DocumentCode()
        {
            this.KYC_FileLog = new List<KYC_FileLog>();
        }

        public int Id { get; set; }
        public string DocumentCodes { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public virtual ICollection<KYC_FileLog> KYC_FileLog { get; set; }
    }
}
