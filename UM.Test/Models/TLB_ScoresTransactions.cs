using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_ScoresTransactions
    {
        public int Id { get; set; }
        public Nullable<int> TransactionId { get; set; }
        public Nullable<int> AssesmentId { get; set; }
        public Nullable<int> ParameterId { get; set; }
        public string FIELD { get; set; }
        public string FilterCriteriaId { get; set; }
        public Nullable<bool> IsScored { get; set; }
        public virtual TLB_Assesments TLB_Assesments { get; set; }
        public virtual TLB_Parameters TLB_Parameters { get; set; }
        public virtual TLB_TransNorm TLB_TransNorm { get; set; }
    }
}
