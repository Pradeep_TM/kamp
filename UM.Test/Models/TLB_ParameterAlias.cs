using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_ParameterAlias
    {
        public int Id { get; set; }
        public Nullable<int> ParameterId { get; set; }
        public string Alias { get; set; }
    }
}
