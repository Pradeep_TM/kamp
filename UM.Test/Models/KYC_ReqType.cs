using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_ReqType
    {
        public KYC_ReqType()
        {
            this.KYC_FormElement = new List<KYC_FormElement>();
            this.KYC_ReqGridColumn = new List<KYC_ReqGridColumn>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<KYC_FormElement> KYC_FormElement { get; set; }
        public virtual ICollection<KYC_ReqGridColumn> KYC_ReqGridColumn { get; set; }
    }
}
