using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_Steps
    {
        public WF_Steps()
        {
            this.WF_Activity = new List<WF_Activity>();
            this.WF_Documents = new List<WF_Documents>();
        }

        public int Id { get; set; }
        public int WFId { get; set; }
        public string StepName { get; set; }
        public string State { get; set; }
        public int SequenceNo { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<WF_Activity> WF_Activity { get; set; }
        public virtual WF_Definition WF_Definition { get; set; }
        public virtual ICollection<WF_Documents> WF_Documents { get; set; }
    }
}
