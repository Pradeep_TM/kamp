using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class AUDIT_EntityModelXmls
    {
        public int Id { get; set; }
        public int ConfigurationId { get; set; }
        public string Action { get; set; }
        public string EntityXml { get; set; }
        public virtual AUDIT_ConfigurationTables AUDIT_ConfigurationTables { get; set; }
    }
}
