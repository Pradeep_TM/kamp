using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_BatchSteps
    {
        public int ID { get; set; }
        public int BatchTypeId { get; set; }
        public Nullable<int> AssesmentId { get; set; }
        public string BatchStepName { get; set; }
    }
}
