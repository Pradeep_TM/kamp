using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_RoleInPermission
    {
        public long RoleInPermissionId { get; set; }
        public long RoleId { get; set; }
        public long PermissionSetId { get; set; }
        public virtual UM_PermissionSet UM_PermissionSet { get; set; }
        public virtual UM_Roles UM_Roles { get; set; }
    }
}
