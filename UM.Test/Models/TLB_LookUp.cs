using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_LookUp
    {
        public int Id { get; set; }
        public int LookGroupId { get; set; }
        public string Value { get; set; }
        public string Reference { get; set; }
        public string OldValue { get; set; }
        public virtual TLB_LookUpGroups TLB_LookUpGroups { get; set; }
    }
}
