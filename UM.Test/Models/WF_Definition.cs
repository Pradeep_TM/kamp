using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_Definition
    {
        public WF_Definition()
        {
            this.WF_Steps = new List<WF_Steps>();
        }

        public int WFId { get; set; }
        public string WFName { get; set; }
        public string WorkFlowDefinition { get; set; }
        public int ModuleTypeId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<WF_Steps> WF_Steps { get; set; }
    }
}
