using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_CaseAssesmentNote
    {
        public int Id { get; set; }
        public Nullable<int> AssesmentId { get; set; }
        public Nullable<int> cf_Id { get; set; }
        public string Note { get; set; }
        public virtual TLB_Assesments TLB_Assesments { get; set; }
        public virtual TLB_CaseFile TLB_CaseFile { get; set; }
    }
}
