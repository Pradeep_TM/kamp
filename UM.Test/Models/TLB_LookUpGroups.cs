using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_LookUpGroups
    {
        public TLB_LookUpGroups()
        {
            this.TLB_LookUp = new List<TLB_LookUp>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<TLB_LookUp> TLB_LookUp { get; set; }
    }
}
