using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_Requirement
    {
        public KYC_Requirement()
        {
            this.KYC_ReqGridColumn = new List<KYC_ReqGridColumn>();
            this.KYC_ReqOption = new List<KYC_ReqOption>();
            this.KYC_Requirement1 = new List<KYC_Requirement>();
            this.KYC_Response = new List<KYC_Response>();
        }

        public long Id { get; set; }
        public string Requirement { get; set; }
        public int ReqType { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsDefault { get; set; }
        public Nullable<int> ReqCategoryId { get; set; }
        public Nullable<long> ParentReqId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int ModifiedBy { get; set; }
        public virtual KYC_ReqCategory KYC_ReqCategory { get; set; }
        public virtual ICollection<KYC_ReqGridColumn> KYC_ReqGridColumn { get; set; }
        public virtual ICollection<KYC_ReqOption> KYC_ReqOption { get; set; }
        public virtual ICollection<KYC_Requirement> KYC_Requirement1 { get; set; }
        public virtual KYC_Requirement KYC_Requirement2 { get; set; }
        public virtual ICollection<KYC_Response> KYC_Response { get; set; }
    }
}
