using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_HistoryVariables
    {
        public int Id { get; set; }
        public string FKeyTable { get; set; }
        public string FKeyField { get; set; }
        public Nullable<int> FKeyValue { get; set; }
        public string MessField { get; set; }
        public string MessValue { get; set; }
        public Nullable<int> HistoryId { get; set; }
    }
}
