using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class CORE_ModuleValidationTypeMapping
    {
        public int Id { get; set; }
        public int ModuleTypeID { get; set; }
        public int ValidationID { get; set; }
        public long EngagementID { get; set; }
        public bool IsActive { get; set; }
        public virtual CORE_Engagement CORE_Engagement { get; set; }
        public virtual lkp_AnalysisType lkp_AnalysisType { get; set; }
    }
}
