using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_PermissionSet
    {
        public UM_PermissionSet()
        {
            this.UM_PermissionInSet = new List<UM_PermissionInSet>();
            this.UM_RoleInPermission = new List<UM_RoleInPermission>();
        }

        public long PermissionSetId { get; set; }
        public string PermissionSetName { get; set; }
        public long ModuleObjectId { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual UM_ModuleObjects UM_ModuleObjects { get; set; }
        public virtual ICollection<UM_PermissionInSet> UM_PermissionInSet { get; set; }
        public virtual ICollection<UM_RoleInPermission> UM_RoleInPermission { get; set; }
    }
}
