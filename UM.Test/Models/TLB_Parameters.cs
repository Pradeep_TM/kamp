using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_Parameters
    {
        public TLB_Parameters()
        {
            this.TLB_ScoresTransactions = new List<TLB_ScoresTransactions>();
        }

        public int Id { get; set; }
        public int ParameterGroupID { get; set; }
        public string ActualValue { get; set; }
        public string Alias { get; set; }
        public string Value { get; set; }
        public string SourceKey { get; set; }
        public Nullable<bool> ContainstSpaceEachSide { get; set; }
        public string ValueNoSpace { get; set; }
        public Nullable<bool> IsAddSAR { get; set; }
        public virtual TLB_ParameterGroups TLB_ParameterGroups { get; set; }
        public virtual ICollection<TLB_ScoresTransactions> TLB_ScoresTransactions { get; set; }
    }
}
