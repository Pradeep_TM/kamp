using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_Users
    {
        public UM_Users()
        {
            this.UM_UserInGroup = new List<UM_UserInGroup>();
            this.UM_UserInRole = new List<UM_UserInRole>();
        }

        public long UserId { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual ICollection<UM_UserInGroup> UM_UserInGroup { get; set; }
        public virtual ICollection<UM_UserInRole> UM_UserInRole { get; set; }
    }
}
