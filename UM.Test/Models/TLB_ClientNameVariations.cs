using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_ClientNameVariations
    {
        public string FieldScrubbed { get; set; }
        public Nullable<int> IsValid { get; set; }
    }
}
