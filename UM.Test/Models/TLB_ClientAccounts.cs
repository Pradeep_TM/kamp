using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_ClientAccounts
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string AccountStatus { get; set; }
        public string Account { get; set; }
        public Nullable<System.DateTime> AccountCLSDate { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerCode { get; set; }
        public string Address { get; set; }
    }
}
