using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class UM_ModuleObjects
    {
        public UM_ModuleObjects()
        {
            this.UM_PermissionSet = new List<UM_PermissionSet>();
        }

        public long ModuleObjectsId { get; set; }
        public string ModuleObjectName { get; set; }
        public int ModuleTypeId { get; set; }
        public virtual CORE_ModuleType CORE_ModuleType { get; set; }
        public virtual ICollection<UM_PermissionSet> UM_PermissionSet { get; set; }
    }
}
