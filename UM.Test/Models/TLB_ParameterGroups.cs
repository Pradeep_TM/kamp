using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_ParameterGroups
    {
        public TLB_ParameterGroups()
        {
            this.TLB_Parameters = new List<TLB_Parameters>();
        }

        public int Id { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> IsIndustryGroup { get; set; }
        public Nullable<bool> IsHRG { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public virtual ICollection<TLB_Parameters> TLB_Parameters { get; set; }
    }
}
