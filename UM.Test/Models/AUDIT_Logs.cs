using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class AUDIT_Logs
    {
        public int Id { get; set; }
        public int LogDetailsId { get; set; }
        public string AuditXml { get; set; }
        public virtual AUDIT_LogDetails AUDIT_LogDetails { get; set; }
    }
}
