using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_CaseFileMap : EntityTypeConfiguration<KYC_CaseFile>
    {
        public KYC_CaseFileMap()
        {
            // Primary Key
            this.HasKey(t => t.cf_ID);

            // Properties
            this.Property(t => t.CustomerNo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CaseNo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CustomerName)
                .HasMaxLength(150);

            this.Property(t => t.PrimAMLRisk)
                .HasMaxLength(50);

            this.Property(t => t.PrimSUITRisk)
                .HasMaxLength(50);

            this.Property(t => t.PotentiallyCloseable)
                .HasMaxLength(10);

            this.Property(t => t.WholeOPPotentiallyCloseable)
                .HasMaxLength(10);

            this.Property(t => t.SPVHold)
                .HasMaxLength(10);

            this.Property(t => t.CMTFlag)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("KYC_CaseFile");
            this.Property(t => t.cf_ID).HasColumnName("cf_ID");
            this.Property(t => t.CustomerNo).HasColumnName("CustomerNo");
            this.Property(t => t.CaseNo).HasColumnName("CaseNo");
            this.Property(t => t.CustomerName).HasColumnName("CustomerName");
            this.Property(t => t.CategoryId).HasColumnName("CategoryId");
            this.Property(t => t.RiskProfile).HasColumnName("RiskProfile");
            this.Property(t => t.AnalystId).HasColumnName("AnalystId");
            this.Property(t => t.Due_Diligence).HasColumnName("Due_Diligence");
            this.Property(t => t.MemorandumOfFact).HasColumnName("MemorandumOfFact");
            this.Property(t => t.TransactionCount).HasColumnName("TransactionCount");
            this.Property(t => t.TransactionScore).HasColumnName("TransactionScore");
            this.Property(t => t.AccountCount).HasColumnName("AccountCount");
            this.Property(t => t.AccountScore).HasColumnName("AccountScore");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.DemoteCount).HasColumnName("DemoteCount");
            this.Property(t => t.ExecutiveSummary).HasColumnName("ExecutiveSummary");
            this.Property(t => t.SARStatus).HasColumnName("SARStatus");
            this.Property(t => t.HighStatusId).HasColumnName("HighStatusId");
            this.Property(t => t.PrimAMLRisk).HasColumnName("PrimAMLRisk");
            this.Property(t => t.PrimSUITRisk).HasColumnName("PrimSUITRisk");
            this.Property(t => t.HasRelatedCases).HasColumnName("HasRelatedCases");
            this.Property(t => t.RFITypeId).HasColumnName("RFITypeId");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.RiskNumber).HasColumnName("RiskNumber");
            this.Property(t => t.HasNoDeriv).HasColumnName("HasNoDeriv");
            this.Property(t => t.PreliminaryRiskNumber).HasColumnName("PreliminaryRiskNumber");
            this.Property(t => t.PotentiallyCloseable).HasColumnName("PotentiallyCloseable");
            this.Property(t => t.WholeOPPotentiallyCloseable).HasColumnName("WholeOPPotentiallyCloseable");
            this.Property(t => t.SPVHold).HasColumnName("SPVHold");
            this.Property(t => t.CMTFlag).HasColumnName("CMTFlag");
            this.Property(t => t.Valid).HasColumnName("Valid");
            this.Property(t => t.Duplicate).HasColumnName("Duplicate");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.LastModifiedDate).HasColumnName("LastModifiedDate");
        }
    }
}
