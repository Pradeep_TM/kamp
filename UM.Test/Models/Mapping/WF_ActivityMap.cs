using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class WF_ActivityMap : EntityTypeConfiguration<WF_Activity>
    {
        public WF_ActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.MailTemplate)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("WF_Activity");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ToStateId).HasColumnName("ToStateId");
            this.Property(t => t.MailTemplate).HasColumnName("MailTemplate");
            this.Property(t => t.StepId).HasColumnName("StepId");
            this.Property(t => t.ActionId).HasColumnName("ActionId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");

            // Relationships
            this.HasRequired(t => t.WF_Lookup_Action)
                .WithMany(t => t.WF_Activity)
                .HasForeignKey(d => d.ActionId);
            this.HasRequired(t => t.WF_States)
                .WithMany(t => t.WF_Activity)
                .HasForeignKey(d => d.ToStateId);
            this.HasRequired(t => t.WF_Steps)
                .WithMany(t => t.WF_Activity)
                .HasForeignKey(d => d.StepId);

        }
    }
}
