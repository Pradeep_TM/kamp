using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_TransTotalScoreMap : EntityTypeConfiguration<TLB_TransTotalScore>
    {
        public TLB_TransTotalScoreMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TLB_TransTotalScore");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TotalScore).HasColumnName("TotalScore");
        }
    }
}
