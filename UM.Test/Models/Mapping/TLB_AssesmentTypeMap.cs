using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_AssesmentTypeMap : EntityTypeConfiguration<TLB_AssesmentType>
    {
        public TLB_AssesmentTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.AssesmentTypeDescription)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TLB_AssesmentType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AssesmentTypeDescription).HasColumnName("AssesmentTypeDescription");
        }
    }
}
