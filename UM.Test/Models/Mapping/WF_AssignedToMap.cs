using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class WF_AssignedToMap : EntityTypeConfiguration<WF_AssignedTo>
    {
        public WF_AssignedToMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.WorkflowStatus)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.State)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("WF_AssignedTo");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.WorkflowStatus).HasColumnName("WorkflowStatus");
            this.Property(t => t.AssignmentId).HasColumnName("AssignmentId");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.WF_Assignment)
                .WithMany(t => t.WF_AssignedTo)
                .HasForeignKey(d => d.AssignmentId);

        }
    }
}
