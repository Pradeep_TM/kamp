using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class UM_PermissionSetMap : EntityTypeConfiguration<UM_PermissionSet>
    {
        public UM_PermissionSetMap()
        {
            // Primary Key
            this.HasKey(t => t.PermissionSetId);

            // Properties
            this.Property(t => t.PermissionSetName)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("UM_PermissionSet");
            this.Property(t => t.PermissionSetId).HasColumnName("PermissionSetId");
            this.Property(t => t.PermissionSetName).HasColumnName("PermissionSetName");
            this.Property(t => t.ModuleObjectId).HasColumnName("ModuleObjectId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.UM_ModuleObjects)
                .WithMany(t => t.UM_PermissionSet)
                .HasForeignKey(d => d.ModuleObjectId);

        }
    }
}
