using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class UM_PermissionInSetMap : EntityTypeConfiguration<UM_PermissionInSet>
    {
        public UM_PermissionInSetMap()
        {
            // Primary Key
            this.HasKey(t => t.PermissionInSetId);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_PermissionInSet");
            this.Property(t => t.PermissionInSetId).HasColumnName("PermissionInSetId");
            this.Property(t => t.PermissionId).HasColumnName("PermissionId");
            this.Property(t => t.PermissionSetId).HasColumnName("PermissionSetId");

            // Relationships
            this.HasRequired(t => t.UM_Permissions)
                .WithMany(t => t.UM_PermissionInSet)
                .HasForeignKey(d => d.PermissionId);
            this.HasRequired(t => t.UM_PermissionSet)
                .WithMany(t => t.UM_PermissionInSet)
                .HasForeignKey(d => d.PermissionSetId);

        }
    }
}
