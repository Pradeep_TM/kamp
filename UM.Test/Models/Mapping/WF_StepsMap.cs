using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class WF_StepsMap : EntityTypeConfiguration<WF_Steps>
    {
        public WF_StepsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.StepName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.State)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("WF_Steps");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.WFId).HasColumnName("WFId");
            this.Property(t => t.StepName).HasColumnName("StepName");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.SequenceNo).HasColumnName("SequenceNo");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.WF_Definition)
                .WithMany(t => t.WF_Steps)
                .HasForeignKey(d => d.WFId);

        }
    }
}
