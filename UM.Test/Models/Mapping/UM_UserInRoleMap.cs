using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class UM_UserInRoleMap : EntityTypeConfiguration<UM_UserInRole>
    {
        public UM_UserInRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.UserInRoleId);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_UserInRole");
            this.Property(t => t.UserInRoleId).HasColumnName("UserInRoleId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.UM_Roles)
                .WithMany(t => t.UM_UserInRole)
                .HasForeignKey(d => d.RoleId);
            this.HasRequired(t => t.UM_Users)
                .WithMany(t => t.UM_UserInRole)
                .HasForeignKey(d => d.UserId);

        }
    }
}
