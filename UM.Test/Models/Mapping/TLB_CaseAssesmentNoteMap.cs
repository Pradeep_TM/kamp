using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_CaseAssesmentNoteMap : EntityTypeConfiguration<TLB_CaseAssesmentNote>
    {
        public TLB_CaseAssesmentNoteMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TLB_CaseAssesmentNote");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.cf_Id).HasColumnName("cf_Id");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasOptional(t => t.TLB_Assesments)
                .WithMany(t => t.TLB_CaseAssesmentNote)
                .HasForeignKey(d => d.AssesmentId);
            this.HasOptional(t => t.TLB_CaseFile)
                .WithMany(t => t.TLB_CaseAssesmentNote)
                .HasForeignKey(d => d.cf_Id);

        }
    }
}
