using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_ReqOptionMap : EntityTypeConfiguration<KYC_ReqOption>
    {
        public KYC_ReqOptionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.OptionName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("KYC_ReqOption");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RequirementId).HasColumnName("RequirementId");
            this.Property(t => t.GridColumnId).HasColumnName("GridColumnId");
            this.Property(t => t.OptionName).HasColumnName("OptionName");
            this.Property(t => t.IsValid).HasColumnName("IsValid");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasOptional(t => t.KYC_Requirement)
                .WithMany(t => t.KYC_ReqOption)
                .HasForeignKey(d => d.RequirementId);

        }
    }
}
