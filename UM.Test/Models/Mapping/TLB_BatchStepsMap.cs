using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_BatchStepsMap : EntityTypeConfiguration<TLB_BatchSteps>
    {
        public TLB_BatchStepsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.BatchStepName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TLB_BatchSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.BatchTypeId).HasColumnName("BatchTypeId");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.BatchStepName).HasColumnName("BatchStepName");
        }
    }
}
