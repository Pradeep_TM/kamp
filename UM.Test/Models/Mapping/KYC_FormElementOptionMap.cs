using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_FormElementOptionMap : EntityTypeConfiguration<KYC_FormElementOption>
    {
        public KYC_FormElementOptionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.OptionText)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("KYC_FormElementOption");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FormElementId).HasColumnName("FormElementId");
            this.Property(t => t.OptionText).HasColumnName("OptionText");
            this.Property(t => t.Valid).HasColumnName("Valid");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.KYC_FormElement)
                .WithMany(t => t.KYC_FormElementOption)
                .HasForeignKey(d => d.FormElementId);

        }
    }
}
