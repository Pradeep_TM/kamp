using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_ResponseOptionMap : EntityTypeConfiguration<KYC_ResponseOption>
    {
        public KYC_ResponseOptionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_ResponseOption");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ResponseId).HasColumnName("ResponseId");
            this.Property(t => t.ReqOptionId).HasColumnName("ReqOptionId");
            this.Property(t => t.GridCellId).HasColumnName("GridCellId");

            // Relationships
            this.HasOptional(t => t.KYC_Response)
                .WithMany(t => t.KYC_ResponseOption)
                .HasForeignKey(d => d.ResponseId);
            this.HasOptional(t => t.KYC_ResponseGridCell)
                .WithMany(t => t.KYC_ResponseOption)
                .HasForeignKey(d => d.GridCellId);

        }
    }
}
