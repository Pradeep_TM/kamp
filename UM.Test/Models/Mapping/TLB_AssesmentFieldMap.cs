using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_AssesmentFieldMap : EntityTypeConfiguration<TLB_AssesmentField>
    {
        public TLB_AssesmentFieldMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FieldName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.FieldType)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TLB_AssesmentField");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.FieldName).HasColumnName("FieldName");
            this.Property(t => t.parameterGroupId).HasColumnName("parameterGroupId");
            this.Property(t => t.FieldType).HasColumnName("FieldType");
        }
    }
}
