using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class lkp_AnalysisTypeMap : EntityTypeConfiguration<lkp_AnalysisType>
    {
        public lkp_AnalysisTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.AnalysisName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("lkp.AnalysisType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AnalysisName).HasColumnName("AnalysisName");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
