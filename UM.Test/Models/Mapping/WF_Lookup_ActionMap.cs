using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class WF_Lookup_ActionMap : EntityTypeConfiguration<WF_Lookup_Action>
    {
        public WF_Lookup_ActionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Action)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("WF_Lookup_Action");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Action).HasColumnName("Action");
        }
    }
}
