using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class WF_WorkflowTrackingMap : EntityTypeConfiguration<WF_WorkflowTracking>
    {
        public WF_WorkflowTrackingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("WF_WorkflowTracking");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.ObjectId).HasColumnName("ObjectId");
            this.Property(t => t.AssignmentId).HasColumnName("AssignmentId");
            this.Property(t => t.CFId).HasColumnName("CFId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.WF_Assignment)
                .WithMany(t => t.WF_WorkflowTracking)
                .HasForeignKey(d => d.AssignmentId);

        }
    }
}
