using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_BinaryLargeObjectMap : EntityTypeConfiguration<TLB_BinaryLargeObject>
    {
        public TLB_BinaryLargeObjectMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(50);

            this.Property(t => t.FileName)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TLB_BinaryLargeObject");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Object).HasColumnName("Object");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.cf_Id).HasColumnName("cf_Id");
            this.Property(t => t.DateUpdated).HasColumnName("DateUpdated");
        }
    }
}
