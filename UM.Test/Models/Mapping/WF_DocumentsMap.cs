using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class WF_DocumentsMap : EntityTypeConfiguration<WF_Documents>
    {
        public WF_DocumentsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.DocumentName)
                .HasMaxLength(50);

            this.Property(t => t.DocumentType)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("WF_Documents");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DocumentName).HasColumnName("DocumentName");
            this.Property(t => t.DocumentContent).HasColumnName("DocumentContent");
            this.Property(t => t.DocumentType).HasColumnName("DocumentType");
            this.Property(t => t.StepId).HasColumnName("StepId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(t => t.WF_Steps)
                .WithMany(t => t.WF_Documents)
                .HasForeignKey(d => d.StepId);

        }
    }
}
