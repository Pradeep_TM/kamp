using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class AUDIT_LogsMap : EntityTypeConfiguration<AUDIT_Logs>
    {
        public AUDIT_LogsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("AUDIT_Logs");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LogDetailsId).HasColumnName("LogDetailsId");
            this.Property(t => t.AuditXml).HasColumnName("AuditXml");

            // Relationships
            this.HasRequired(t => t.AUDIT_LogDetails)
                .WithMany(t => t.AUDIT_Logs)
                .HasForeignKey(d => d.LogDetailsId);

        }
    }
}
