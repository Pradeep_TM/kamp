using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class MV_RoutineMap : EntityTypeConfiguration<MV_Routine>
    {
        public MV_RoutineMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RoutineName)
                .IsRequired();

            this.Property(t => t.Source)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("MV_Routine");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoutineName).HasColumnName("RoutineName");
            this.Property(t => t.RoutineDescription).HasColumnName("RoutineDescription");
            this.Property(t => t.Source).HasColumnName("Source");
            this.Property(t => t.Jurisdiction).HasColumnName("Jurisdiction");
            this.Property(t => t.RoutineAccountScore).HasColumnName("RoutineAccountScore");
            this.Property(t => t.RoutineTransactionScore).HasColumnName("RoutineTransactionScore");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.ParentRoutineID).HasColumnName("ParentRoutineID");
            this.Property(t => t.ClientID).HasColumnName("ClientID");

            // Relationships
            this.HasOptional(t => t.CORE_Clients)
                .WithMany(t => t.MV_Routine)
                .HasForeignKey(d => d.ClientID);
            this.HasOptional(t => t.MV_Routine2)
                .WithMany(t => t.MV_Routine1)
                .HasForeignKey(d => d.ParentRoutineID);

        }
    }
}
