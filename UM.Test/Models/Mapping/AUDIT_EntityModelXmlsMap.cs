using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class AUDIT_EntityModelXmlsMap : EntityTypeConfiguration<AUDIT_EntityModelXmls>
    {
        public AUDIT_EntityModelXmlsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("AUDIT_EntityModelXmls");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ConfigurationId).HasColumnName("ConfigurationId");
            this.Property(t => t.Action).HasColumnName("Action");
            this.Property(t => t.EntityXml).HasColumnName("EntityXml");

            // Relationships
            this.HasRequired(t => t.AUDIT_ConfigurationTables)
                .WithMany(t => t.AUDIT_EntityModelXmls)
                .HasForeignKey(d => d.ConfigurationId);

        }
    }
}
