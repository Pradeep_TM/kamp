using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_ScoresTransactionsMap : EntityTypeConfiguration<TLB_ScoresTransactions>
    {
        public TLB_ScoresTransactionsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FIELD)
                .HasMaxLength(50);

            this.Property(t => t.FilterCriteriaId)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TLB_ScoresTransactions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.ParameterId).HasColumnName("ParameterId");
            this.Property(t => t.FIELD).HasColumnName("FIELD");
            this.Property(t => t.FilterCriteriaId).HasColumnName("FilterCriteriaId");
            this.Property(t => t.IsScored).HasColumnName("IsScored");

            // Relationships
            this.HasOptional(t => t.TLB_Assesments)
                .WithMany(t => t.TLB_ScoresTransactions)
                .HasForeignKey(d => d.AssesmentId);
            this.HasOptional(t => t.TLB_Parameters)
                .WithMany(t => t.TLB_ScoresTransactions)
                .HasForeignKey(d => d.ParameterId);
            this.HasOptional(t => t.TLB_TransNorm)
                .WithMany(t => t.TLB_ScoresTransactions)
                .HasForeignKey(d => d.TransactionId);

        }
    }
}
