using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class WF_StatesMap : EntityTypeConfiguration<WF_States>
    {
        public WF_StatesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.StateName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("WF_States");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.StateName).HasColumnName("StateName");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasMany(t => t.UM_Groups)
                .WithMany(t => t.WF_States)
                .Map(m =>
                    {
                        m.ToTable("GroupStateMapping");
                        m.MapLeftKey("StateId");
                        m.MapRightKey("GroupId");
                    });


        }
    }
}
