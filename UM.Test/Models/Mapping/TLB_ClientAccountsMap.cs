using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_ClientAccountsMap : EntityTypeConfiguration<TLB_ClientAccounts>
    {
        public TLB_ClientAccountsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.AccountName)
                .HasMaxLength(255);

            this.Property(t => t.AccountStatus)
                .HasMaxLength(50);

            this.Property(t => t.Account)
                .HasMaxLength(50);

            this.Property(t => t.CustomerNumber)
                .HasMaxLength(50);

            this.Property(t => t.CustomerCode)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TLB_ClientAccounts");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.AccountStatus).HasColumnName("AccountStatus");
            this.Property(t => t.Account).HasColumnName("Account");
            this.Property(t => t.AccountCLSDate).HasColumnName("AccountCLSDate");
            this.Property(t => t.CustomerNumber).HasColumnName("CustomerNumber");
            this.Property(t => t.CustomerCode).HasColumnName("CustomerCode");
            this.Property(t => t.Address).HasColumnName("Address");
        }
    }
}
