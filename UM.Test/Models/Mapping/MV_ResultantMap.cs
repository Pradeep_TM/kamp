using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class MV_ResultantMap : EntityTypeConfiguration<MV_Resultant>
    {
        public MV_ResultantMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("MV_Resultant");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoutineID).HasColumnName("RoutineID");
            this.Property(t => t.ResultantQuery).HasColumnName("ResultantQuery");
            this.Property(t => t.AnalysisTypeID).HasColumnName("AnalysisTypeID");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");

            // Relationships
            this.HasRequired(t => t.lkp_AnalysisType)
                .WithMany(t => t.MV_Resultant)
                .HasForeignKey(d => d.AnalysisTypeID);
            this.HasRequired(t => t.MV_Routine)
                .WithMany(t => t.MV_Resultant)
                .HasForeignKey(d => d.RoutineID);

        }
    }
}
