using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_KPMGEditableMap : EntityTypeConfiguration<KYC_KPMGEditable>
    {
        public KYC_KPMGEditableMap()
        {
            // Primary Key
            this.HasKey(t => new { t.cf_ID, t.Value, t.CreatedBy, t.ModifiedBy, t.CreatedDate, t.ModifiedDate, t.GroupId });

            // Properties
            this.Property(t => t.cf_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Value)
                .IsRequired();

            this.Property(t => t.CreatedBy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ModifiedBy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.GroupId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("KYC_KPMGEditable");
            this.Property(t => t.cf_ID).HasColumnName("cf_ID");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.GroupId).HasColumnName("GroupId");
            this.Property(t => t.FormElementId).HasColumnName("FormElementId");

            // Relationships
            this.HasRequired(t => t.KYC_CaseFile)
                .WithMany(t => t.KYC_KPMGEditable)
                .HasForeignKey(d => d.cf_ID);
            this.HasOptional(t => t.KYC_FormElement)
                .WithMany(t => t.KYC_KPMGEditable)
                .HasForeignKey(d => d.FormElementId);

        }
    }
}
