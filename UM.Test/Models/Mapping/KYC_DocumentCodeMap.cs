using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_DocumentCodeMap : EntityTypeConfiguration<KYC_DocumentCode>
    {
        public KYC_DocumentCodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_DocumentCode");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DocumentCodes).HasColumnName("DocumentCodes");
            this.Property(t => t.IsValid).HasColumnName("IsValid");
        }
    }
}
