using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_BatchTypesMap : EntityTypeConfiguration<TLB_BatchTypes>
    {
        public TLB_BatchTypesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TLB_BatchTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Score).HasColumnName("Score");
            this.Property(t => t.ASSESS).HasColumnName("ASSESS");
        }
    }
}
