using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_RequestForInformationMap : EntityTypeConfiguration<KYC_RequestForInformation>
    {
        public KYC_RequestForInformationMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CfId)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.SentTo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SentFrom)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Body)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("KYC_RequestForInformation");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.SentTo).HasColumnName("SentTo");
            this.Property(t => t.SentFrom).HasColumnName("SentFrom");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.IsAttachmentMail).HasColumnName("IsAttachmentMail");
            this.Property(t => t.IsResponseUploaded).HasColumnName("IsResponseUploaded");
            this.Property(t => t.CC).HasColumnName("CC");
            this.Property(t => t.BCC).HasColumnName("BCC");
            this.Property(t => t.SentDate).HasColumnName("SentDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
        }
    }
}
