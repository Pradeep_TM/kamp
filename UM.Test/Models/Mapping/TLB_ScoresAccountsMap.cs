using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_ScoresAccountsMap : EntityTypeConfiguration<TLB_ScoresAccounts>
    {
        public TLB_ScoresAccountsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.TransactionAccount)
                .HasMaxLength(15);

            this.Property(t => t.FIELD)
                .HasMaxLength(50);

            this.Property(t => t.FilterCriteriaId)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TLB_ScoresAccounts");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransactionAccount).HasColumnName("TransactionAccount");
            this.Property(t => t.AssesmentId).HasColumnName("AssesmentId");
            this.Property(t => t.ParameterId).HasColumnName("ParameterId");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.FIELD).HasColumnName("FIELD");
            this.Property(t => t.FilterCriteriaId).HasColumnName("FilterCriteriaId");
            this.Property(t => t.IsScored).HasColumnName("IsScored");
        }
    }
}
