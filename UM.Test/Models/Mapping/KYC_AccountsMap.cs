using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_AccountsMap : EntityTypeConfiguration<KYC_Accounts>
    {
        public KYC_AccountsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.OP_PartyName)
                .HasMaxLength(255);

            this.Property(t => t.Pr_PartyName)
                .HasMaxLength(255);

            this.Property(t => t.accountNumber)
                .HasMaxLength(255);

            this.Property(t => t.accountSystemRN)
                .HasMaxLength(50);

            this.Property(t => t.accountID)
                .HasMaxLength(255);

            this.Property(t => t.accountOpenDate)
                .HasMaxLength(50);

            this.Property(t => t.BookingCo)
                .HasMaxLength(50);

            this.Property(t => t.accountCategoryRN)
                .HasMaxLength(255);

            this.Property(t => t.accountClassification)
                .HasMaxLength(50);

            this.Property(t => t.codIndicator)
                .HasMaxLength(255);

            this.Property(t => t.lastJournalDate)
                .HasMaxLength(50);

            this.Property(t => t.PositionOrBalance)
                .HasMaxLength(255);

            this.Property(t => t.OP_Derived_TradingName)
                .HasMaxLength(255);

            this.Property(t => t.OP_Site)
                .HasMaxLength(50);

            this.Property(t => t.OP_Site_streetAddress)
                .HasMaxLength(255);

            this.Property(t => t.OP_Site_city)
                .HasMaxLength(255);

            this.Property(t => t.OP_Site_state)
                .HasMaxLength(255);

            this.Property(t => t.OP_SiteCountry)
                .HasMaxLength(255);

            this.Property(t => t.OP_Site_PostalCode)
                .HasMaxLength(50);

            this.Property(t => t.OP_Site_ValidationStatus)
                .HasMaxLength(255);

            this.Property(t => t.OP_Site_ValidationSource)
                .HasMaxLength(255);

            this.Property(t => t.OP_Site_ValidationTS)
                .HasMaxLength(50);

            this.Property(t => t.OP_PartyID)
                .HasMaxLength(255);

            this.Property(t => t.OP_PartyName1)
                .HasMaxLength(255);

            this.Property(t => t.OP_countryOfJurisdiction)
                .HasMaxLength(255);

            this.Property(t => t.OP_legalForm)
                .HasMaxLength(255);

            this.Property(t => t.OP_capClass)
                .HasMaxLength(255);

            this.Property(t => t.OP_Has_RelianceAgreement__USN_)
                .HasMaxLength(255);

            this.Property(t => t.OP_Accounts)
                .HasMaxLength(50);

            this.Property(t => t.Pr_Site)
                .HasMaxLength(255);

            this.Property(t => t.PR_Site_streetAddress)
                .HasMaxLength(255);

            this.Property(t => t.PR_Site_city)
                .HasMaxLength(255);

            this.Property(t => t.PR_Site_state)
                .HasMaxLength(255);

            this.Property(t => t.Pr_SiteCountry)
                .HasMaxLength(255);

            this.Property(t => t.PR_Site_PostalCode)
                .HasMaxLength(255);

            this.Property(t => t.PR_Site_ValidationStatus)
                .HasMaxLength(255);

            this.Property(t => t.PR_Site_ValidationSource)
                .HasMaxLength(255);

            this.Property(t => t.PR_Site_ValidationTS)
                .HasMaxLength(255);

            this.Property(t => t.Pr_PartyID)
                .HasMaxLength(255);

            this.Property(t => t.Pr_PartyName1)
                .HasMaxLength(255);

            this.Property(t => t.Pr_CountryOfJurisdiction)
                .HasMaxLength(255);

            this.Property(t => t.Pr_legalForm)
                .HasMaxLength(255);

            this.Property(t => t.Pr_capClass)
                .HasMaxLength(255);

            this.Property(t => t.rrID)
                .HasMaxLength(50);

            this.Property(t => t.rrName)
                .HasMaxLength(255);

            this.Property(t => t.rrBranchLocation)
                .HasMaxLength(50);

            this.Property(t => t.rrBranch)
                .HasMaxLength(255);

            this.Property(t => t.rrDepartment)
                .HasMaxLength(255);

            this.Property(t => t.rrCostCenter)
                .HasMaxLength(50);

            this.Property(t => t.rrCC)
                .HasMaxLength(255);

            this.Property(t => t.rrBU)
                .HasMaxLength(255);

            this.Property(t => t.rrBG)
                .HasMaxLength(255);

            this.Property(t => t.rrRegion)
                .HasMaxLength(255);

            this.Property(t => t.accountName)
                .HasMaxLength(255);

            this.Property(t => t.accountMailingAddress)
                .HasMaxLength(255);

            this.Property(t => t.MailingAddresscity)
                .HasMaxLength(255);

            this.Property(t => t.MailingAddressstate)
                .HasMaxLength(255);

            this.Property(t => t.MailingAddresscountry)
                .HasMaxLength(255);

            this.Property(t => t.MailingAddresspostalCode)
                .HasMaxLength(50);

            this.Property(t => t.SalesPersonName)
                .HasMaxLength(50);

            this.Property(t => t.SalesPersonNotes)
                .HasMaxLength(50);

            this.Property(t => t.GeneralProduct)
                .HasMaxLength(100);

            this.Property(t => t.SpecificProduct)
                .HasMaxLength(100);

            this.Property(t => t.BusinessDefinitionID)
                .HasMaxLength(100);

            this.Property(t => t.BusinessDefinitionDescription)
                .HasMaxLength(100);

            this.Property(t => t.Supervising_Officer_Name)
                .HasMaxLength(4000);

            this.Property(t => t.DataFile)
                .HasMaxLength(4000);

            this.Property(t => t.AccountStatus)
                .HasMaxLength(10);

            this.Property(t => t.AxiomOPTNID)
                .HasMaxLength(20);

            this.Property(t => t.AxiomOPTN)
                .HasMaxLength(150);

            this.Property(t => t.AxiomPMTNID)
                .HasMaxLength(20);

            this.Property(t => t.AxiomPMTN)
                .HasMaxLength(150);

            this.Property(t => t.DerivedOPTNID)
                .HasMaxLength(20);

            this.Property(t => t.DerivedOPTN)
                .HasMaxLength(150);

            this.Property(t => t.DesiTNID)
                .HasMaxLength(20);

            this.Property(t => t.DesiTN)
                .HasMaxLength(150);

            this.Property(t => t.groupDesi)
                .HasMaxLength(50);

            this.Property(t => t.DesiAccount)
                .HasMaxLength(150);

            this.Property(t => t.DesiStatus)
                .HasMaxLength(20);

            this.Property(t => t.DesiName)
                .HasMaxLength(150);

            this.Property(t => t.businessDefDivision)
                .HasMaxLength(50);

            this.Property(t => t.newAccountDataFile)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("KYC_Accounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.OP_PartyName).HasColumnName("OP_PartyName");
            this.Property(t => t.Pr_PartyName).HasColumnName("Pr_PartyName");
            this.Property(t => t.accountNumber).HasColumnName("accountNumber");
            this.Property(t => t.accountSystemRN).HasColumnName("accountSystemRN");
            this.Property(t => t.accountID).HasColumnName("accountID");
            this.Property(t => t.accountOpenDate).HasColumnName("accountOpenDate");
            this.Property(t => t.BookingCo).HasColumnName("BookingCo");
            this.Property(t => t.accountCategoryRN).HasColumnName("accountCategoryRN");
            this.Property(t => t.accountClassification).HasColumnName("accountClassification");
            this.Property(t => t.codIndicator).HasColumnName("codIndicator");
            this.Property(t => t.lastJournalDate).HasColumnName("lastJournalDate");
            this.Property(t => t.PositionOrBalance).HasColumnName("PositionOrBalance");
            this.Property(t => t.OP_Derived_TradingName).HasColumnName("OP_Derived_TradingName");
            this.Property(t => t.OP_Site).HasColumnName("OP_Site");
            this.Property(t => t.OP_Site_streetAddress).HasColumnName("OP_Site_streetAddress");
            this.Property(t => t.OP_Site_city).HasColumnName("OP_Site_city");
            this.Property(t => t.OP_Site_state).HasColumnName("OP_Site_state");
            this.Property(t => t.OP_SiteCountry).HasColumnName("OP_SiteCountry");
            this.Property(t => t.OP_Site_PostalCode).HasColumnName("OP_Site_PostalCode");
            this.Property(t => t.OP_Site_ValidationStatus).HasColumnName("OP_Site_ValidationStatus");
            this.Property(t => t.OP_Site_ValidationSource).HasColumnName("OP_Site_ValidationSource");
            this.Property(t => t.OP_Site_ValidationTS).HasColumnName("OP_Site_ValidationTS");
            this.Property(t => t.OP_PartyID).HasColumnName("OP_PartyID");
            this.Property(t => t.OP_PartyName1).HasColumnName("OP_PartyName1");
            this.Property(t => t.OP_countryOfJurisdiction).HasColumnName("OP_countryOfJurisdiction");
            this.Property(t => t.OP_legalForm).HasColumnName("OP_legalForm");
            this.Property(t => t.OP_capClass).HasColumnName("OP_capClass");
            this.Property(t => t.OP_Has_RelianceAgreement__USN_).HasColumnName("OP_Has_RelianceAgreement (USN)");
            this.Property(t => t.OP_Accounts).HasColumnName("OP Accounts");
            this.Property(t => t.Pr_Site).HasColumnName("Pr_Site");
            this.Property(t => t.PR_Site_streetAddress).HasColumnName("PR_Site_streetAddress");
            this.Property(t => t.PR_Site_city).HasColumnName("PR_Site_city");
            this.Property(t => t.PR_Site_state).HasColumnName("PR_Site_state");
            this.Property(t => t.Pr_SiteCountry).HasColumnName("Pr_SiteCountry");
            this.Property(t => t.PR_Site_PostalCode).HasColumnName("PR_Site_PostalCode");
            this.Property(t => t.PR_Site_ValidationStatus).HasColumnName("PR_Site_ValidationStatus");
            this.Property(t => t.PR_Site_ValidationSource).HasColumnName("PR_Site_ValidationSource");
            this.Property(t => t.PR_Site_ValidationTS).HasColumnName("PR_Site_ValidationTS");
            this.Property(t => t.Pr_PartyID).HasColumnName("Pr_PartyID");
            this.Property(t => t.Pr_PartyName1).HasColumnName("Pr_PartyName1");
            this.Property(t => t.Pr_CountryOfJurisdiction).HasColumnName("Pr_CountryOfJurisdiction");
            this.Property(t => t.Pr_legalForm).HasColumnName("Pr_legalForm");
            this.Property(t => t.Pr_capClass).HasColumnName("Pr_capClass");
            this.Property(t => t.rrID).HasColumnName("rrID");
            this.Property(t => t.rrName).HasColumnName("rrName");
            this.Property(t => t.rrBranchLocation).HasColumnName("rrBranchLocation");
            this.Property(t => t.rrBranch).HasColumnName("rrBranch");
            this.Property(t => t.rrDepartment).HasColumnName("rrDepartment");
            this.Property(t => t.rrCostCenter).HasColumnName("rrCostCenter");
            this.Property(t => t.rrCC).HasColumnName("rrCC");
            this.Property(t => t.rrBU).HasColumnName("rrBU");
            this.Property(t => t.rrBG).HasColumnName("rrBG");
            this.Property(t => t.rrRegion).HasColumnName("rrRegion");
            this.Property(t => t.accountName).HasColumnName("accountName");
            this.Property(t => t.accountMailingAddress).HasColumnName("accountMailingAddress");
            this.Property(t => t.MailingAddresscity).HasColumnName("MailingAddresscity");
            this.Property(t => t.MailingAddressstate).HasColumnName("MailingAddressstate");
            this.Property(t => t.MailingAddresscountry).HasColumnName("MailingAddresscountry");
            this.Property(t => t.MailingAddresspostalCode).HasColumnName("MailingAddresspostalCode");
            this.Property(t => t.SalesPersonName).HasColumnName("SalesPersonName");
            this.Property(t => t.SalesPersonNotes).HasColumnName("SalesPersonNotes");
            this.Property(t => t.GeneralProduct).HasColumnName("GeneralProduct");
            this.Property(t => t.SpecificProduct).HasColumnName("SpecificProduct");
            this.Property(t => t.BusinessDefinitionID).HasColumnName("BusinessDefinitionID");
            this.Property(t => t.BusinessDefinitionDescription).HasColumnName("BusinessDefinitionDescription");
            this.Property(t => t.Supervising_Officer_Name).HasColumnName("Supervising_Officer_Name");
            this.Property(t => t.SalesPersonID).HasColumnName("SalesPersonID");
            this.Property(t => t.DataFile).HasColumnName("DataFile");
            this.Property(t => t.DateAdded).HasColumnName("DateAdded");
            this.Property(t => t.AccountStatus).HasColumnName("AccountStatus");
            this.Property(t => t.AxiomOPTNID).HasColumnName("AxiomOPTNID");
            this.Property(t => t.AxiomOPTN).HasColumnName("AxiomOPTN");
            this.Property(t => t.AxiomPMTNID).HasColumnName("AxiomPMTNID");
            this.Property(t => t.AxiomPMTN).HasColumnName("AxiomPMTN");
            this.Property(t => t.DerivedOPTNID).HasColumnName("DerivedOPTNID");
            this.Property(t => t.DerivedOPTN).HasColumnName("DerivedOPTN");
            this.Property(t => t.DesiTNID).HasColumnName("DesiTNID");
            this.Property(t => t.DesiTN).HasColumnName("DesiTN");
            this.Property(t => t.groupDesi).HasColumnName("groupDesi");
            this.Property(t => t.DesiAccount).HasColumnName("DesiAccount");
            this.Property(t => t.DesiStatus).HasColumnName("DesiStatus");
            this.Property(t => t.DesiName).HasColumnName("DesiName");
            this.Property(t => t.businessDefDivision).HasColumnName("businessDefDivision");
            this.Property(t => t.newAccount).HasColumnName("newAccount");
            this.Property(t => t.newAccountDataFile).HasColumnName("newAccountDataFile");
        }
    }
}
