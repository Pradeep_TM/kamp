using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_PartyTypeMap : EntityTypeConfiguration<KYC_PartyType>
    {
        public KYC_PartyTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_PartyType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.PartyType).HasColumnName("PartyType");
            this.Property(t => t.Valid).HasColumnName("Valid");
        }
    }
}
