using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class UM_UserInGroupMap : EntityTypeConfiguration<UM_UserInGroup>
    {
        public UM_UserInGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.UserInGroupId);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_UserInGroup");
            this.Property(t => t.UserInGroupId).HasColumnName("UserInGroupId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.GroupId).HasColumnName("GroupId");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.CORE_ModuleType)
                .WithMany(t => t.UM_UserInGroup)
                .HasForeignKey(d => d.ModuleTypeId);
            this.HasRequired(t => t.UM_Groups)
                .WithMany(t => t.UM_UserInGroup)
                .HasForeignKey(d => d.GroupId);
            this.HasRequired(t => t.UM_Users)
                .WithMany(t => t.UM_UserInGroup)
                .HasForeignKey(d => d.UserId);

        }
    }
}
