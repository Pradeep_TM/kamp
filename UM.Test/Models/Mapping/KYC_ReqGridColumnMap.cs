using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_ReqGridColumnMap : EntityTypeConfiguration<KYC_ReqGridColumn>
    {
        public KYC_ReqGridColumnMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ColumnName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("KYC_ReqGridColumn");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RequirementId).HasColumnName("RequirementId");
            this.Property(t => t.ColumnName).HasColumnName("ColumnName");
            this.Property(t => t.ReqTypeId).HasColumnName("ReqTypeId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");

            // Relationships
            this.HasRequired(t => t.KYC_ReqType)
                .WithMany(t => t.KYC_ReqGridColumn)
                .HasForeignKey(d => d.ReqTypeId);
            this.HasRequired(t => t.KYC_Requirement)
                .WithMany(t => t.KYC_ReqGridColumn)
                .HasForeignKey(d => d.RequirementId);

        }
    }
}
