using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_ReqClassificationMap : EntityTypeConfiguration<KYC_ReqClassification>
    {
        public KYC_ReqClassificationMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ClassificationName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("KYC_ReqClassification");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ClassificationName).HasColumnName("ClassificationName");
            this.Property(t => t.IsValid).HasColumnName("IsValid");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}
