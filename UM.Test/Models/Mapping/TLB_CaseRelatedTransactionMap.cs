using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_CaseRelatedTransactionMap : EntityTypeConfiguration<TLB_CaseRelatedTransaction>
    {
        public TLB_CaseRelatedTransactionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.TransactionId, t.PartySource });

            // Properties
            this.Property(t => t.CfName)
                .HasMaxLength(150);

            this.Property(t => t.TransactionId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PartyName)
                .HasMaxLength(150);

            this.Property(t => t.PartySource)
                .IsRequired()
                .HasMaxLength(24);

            // Table & Column Mappings
            this.ToTable("TLB_CaseRelatedTransaction");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.CfName).HasColumnName("CfName");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.PartyName).HasColumnName("PartyName");
            this.Property(t => t.PartySource).HasColumnName("PartySource");
        }
    }
}
