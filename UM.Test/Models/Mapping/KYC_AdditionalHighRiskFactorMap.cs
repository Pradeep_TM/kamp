using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_AdditionalHighRiskFactorMap : EntityTypeConfiguration<KYC_AdditionalHighRiskFactor>
    {
        public KYC_AdditionalHighRiskFactorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_AdditionalHighRiskFactor");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.AdditionalHighRiskFactor).HasColumnName("AdditionalHighRiskFactor");
            this.Property(t => t.Valid).HasColumnName("Valid");
        }
    }
}
