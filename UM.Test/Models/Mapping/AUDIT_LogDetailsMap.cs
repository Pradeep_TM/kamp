using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class AUDIT_LogDetailsMap : EntityTypeConfiguration<AUDIT_LogDetails>
    {
        public AUDIT_LogDetailsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("AUDIT_LogDetails");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.EntityName).HasColumnName("EntityName");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.DateTime).HasColumnName("DateTime");
            this.Property(t => t.Action).HasColumnName("Action");
        }
    }
}
