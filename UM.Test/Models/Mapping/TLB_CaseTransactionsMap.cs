using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_CaseTransactionsMap : EntityTypeConfiguration<TLB_CaseTransactions>
    {
        public TLB_CaseTransactionsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TLB_CaseTransactions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.IsClear).HasColumnName("IsClear");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.SAR).HasColumnName("SAR");

            // Relationships
            this.HasOptional(t => t.TLB_CaseFile)
                .WithMany(t => t.TLB_CaseTransactions)
                .HasForeignKey(d => d.CfId);
            this.HasOptional(t => t.TLB_TransNorm)
                .WithMany(t => t.TLB_CaseTransactions)
                .HasForeignKey(d => d.TransactionId);

        }
    }
}
