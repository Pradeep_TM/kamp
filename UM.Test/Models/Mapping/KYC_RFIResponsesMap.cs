using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_RFIResponsesMap : EntityTypeConfiguration<KYC_RFIResponses>
    {
        public KYC_RFIResponsesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.RFIId, t.ResponseUploaded, t.CreatedDate, t.CreatedBy });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RFIId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ResponseUploaded)
                .IsRequired();

            this.Property(t => t.CreatedBy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("KYC_RFIResponses");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RFIId).HasColumnName("RFIId");
            this.Property(t => t.ResponseUploaded).HasColumnName("ResponseUploaded");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");

            // Relationships
            this.HasRequired(t => t.KYC_RequestForInformation)
                .WithMany(t => t.KYC_RFIResponses)
                .HasForeignKey(d => d.RFIId);

        }
    }
}
