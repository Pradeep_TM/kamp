using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class CORE_ModuleTypeMap : EntityTypeConfiguration<CORE_ModuleType>
    {
        public CORE_ModuleTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ModuleTypeId);

            // Properties
            this.Property(t => t.ModuleName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CORE_ModuleType");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");
            this.Property(t => t.ModuleName).HasColumnName("ModuleName");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
