using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class CORE_ModuleValidationTypeMappingMap : EntityTypeConfiguration<CORE_ModuleValidationTypeMapping>
    {
        public CORE_ModuleValidationTypeMappingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CORE_ModuleValidationTypeMapping");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ModuleTypeID).HasColumnName("ModuleTypeID");
            this.Property(t => t.ValidationID).HasColumnName("ValidationID");
            this.Property(t => t.EngagementID).HasColumnName("EngagementID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.CORE_Engagement)
                .WithMany(t => t.CORE_ModuleValidationTypeMapping)
                .HasForeignKey(d => d.EngagementID);
            this.HasRequired(t => t.lkp_AnalysisType)
                .WithMany(t => t.CORE_ModuleValidationTypeMapping)
                .HasForeignKey(d => d.ValidationID);

        }
    }
}
