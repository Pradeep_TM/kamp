using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_RequirementMap : EntityTypeConfiguration<KYC_Requirement>
    {
        public KYC_RequirementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Requirement)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("KYC_Requirement");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Requirement).HasColumnName("Requirement");
            this.Property(t => t.ReqType).HasColumnName("ReqType");
            this.Property(t => t.IsMandatory).HasColumnName("IsMandatory");
            this.Property(t => t.IsDefault).HasColumnName("IsDefault");
            this.Property(t => t.ReqCategoryId).HasColumnName("ReqCategoryId");
            this.Property(t => t.ParentReqId).HasColumnName("ParentReqId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");

            // Relationships
            this.HasOptional(t => t.KYC_ReqCategory)
                .WithMany(t => t.KYC_Requirement)
                .HasForeignKey(d => d.ReqCategoryId);
            this.HasOptional(t => t.KYC_Requirement2)
                .WithMany(t => t.KYC_Requirement1)
                .HasForeignKey(d => d.ParentReqId);

        }
    }
}
