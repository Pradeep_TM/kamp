using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_LookUpMap : EntityTypeConfiguration<TLB_LookUp>
    {
        public TLB_LookUpMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Reference)
                .HasMaxLength(220);

            this.Property(t => t.OldValue)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("TLB_LookUp");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LookGroupId).HasColumnName("LookGroupId");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Reference).HasColumnName("Reference");
            this.Property(t => t.OldValue).HasColumnName("OldValue");

            // Relationships
            this.HasRequired(t => t.TLB_LookUpGroups)
                .WithMany(t => t.TLB_LookUp)
                .HasForeignKey(d => d.LookGroupId);

        }
    }
}
