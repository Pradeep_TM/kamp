using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class CORE_ClientsMap : EntityTypeConfiguration<CORE_Clients>
    {
        public CORE_ClientsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ClientName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Country)
                .HasMaxLength(50);

            this.Property(t => t.ZipCode)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CORE_Clients");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ClientName).HasColumnName("ClientName");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.ZipCode).HasColumnName("ZipCode");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
