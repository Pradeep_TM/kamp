using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class UM_RoleInPermissionMap : EntityTypeConfiguration<UM_RoleInPermission>
    {
        public UM_RoleInPermissionMap()
        {
            // Primary Key
            this.HasKey(t => t.RoleInPermissionId);

            // Properties
            // Table & Column Mappings
            this.ToTable("UM_RoleInPermission");
            this.Property(t => t.RoleInPermissionId).HasColumnName("RoleInPermissionId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.PermissionSetId).HasColumnName("PermissionSetId");

            // Relationships
            this.HasRequired(t => t.UM_PermissionSet)
                .WithMany(t => t.UM_RoleInPermission)
                .HasForeignKey(d => d.PermissionSetId);
            this.HasRequired(t => t.UM_Roles)
                .WithMany(t => t.UM_RoleInPermission)
                .HasForeignKey(d => d.RoleId);

        }
    }
}
