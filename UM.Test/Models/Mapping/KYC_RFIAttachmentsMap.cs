using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_RFIAttachmentsMap : EntityTypeConfiguration<KYC_RFIAttachments>
    {
        public KYC_RFIAttachmentsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.RFIId, t.AttachmentName, t.Attachment });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RFIId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AttachmentName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Attachment)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("KYC_RFIAttachments");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RFIId).HasColumnName("RFIId");
            this.Property(t => t.AttachmentName).HasColumnName("AttachmentName");
            this.Property(t => t.Attachment).HasColumnName("Attachment");

            // Relationships
            this.HasRequired(t => t.KYC_RequestForInformation)
                .WithMany(t => t.KYC_RFIAttachments)
                .HasForeignKey(d => d.RFIId);

        }
    }
}
