using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class MV_CaseFileMap : EntityTypeConfiguration<MV_CaseFile>
    {
        public MV_CaseFileMap()
        {
            // Primary Key
            this.HasKey(t => t.CfId);

            // Properties
            this.Property(t => t.CaseNo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.AMLCaseKey)
                .HasMaxLength(15);

            this.Property(t => t.Name)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("MV_CaseFile");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.CaseNo).HasColumnName("CaseNo");
            this.Property(t => t.TransNormID).HasColumnName("TransNormID");
            this.Property(t => t.TransNormNo).HasColumnName("TransNormNo");
            this.Property(t => t.AMLCaseKey).HasColumnName("AMLCaseKey");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.CategoryId).HasColumnName("CategoryId");
            this.Property(t => t.RiskProfile).HasColumnName("RiskProfile");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.StatusId).HasColumnName("StatusId");
            this.Property(t => t.DueDiligence).HasColumnName("DueDiligence");
            this.Property(t => t.MemorandumOfFact).HasColumnName("MemorandumOfFact");
            this.Property(t => t.TransactionCount).HasColumnName("TransactionCount");
            this.Property(t => t.TransactionScore).HasColumnName("TransactionScore");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifedOn).HasColumnName("ModifedOn");

            // Relationships
            this.HasRequired(t => t.MV_TransNorm)
                .WithMany(t => t.MV_CaseFile)
                .HasForeignKey(d => d.TransNormID);

        }
    }
}
