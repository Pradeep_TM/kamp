using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_FileLogMap : EntityTypeConfiguration<KYC_FileLog>
    {
        public KYC_FileLogMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.Content, t.ContentLength, t.Name, t.Extension, t.PartyTypeId, t.DocumentCodeId, t.cf_ID, t.CreatedDate, t.CreatedBy });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.DocumentType)
                .HasMaxLength(100);

            this.Property(t => t.Content)
                .IsRequired();

            this.Property(t => t.ContentLength)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Extension)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.PartyTypeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DocumentCodeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.cf_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CreatedBy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("KYC_FileLog");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DocumentType).HasColumnName("DocumentType");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.ContentLength).HasColumnName("ContentLength");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Extension).HasColumnName("Extension");
            this.Property(t => t.PartyTypeId).HasColumnName("PartyTypeId");
            this.Property(t => t.DocumentCodeId).HasColumnName("DocumentCodeId");
            this.Property(t => t.cf_ID).HasColumnName("cf_ID");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");

            // Relationships
            this.HasRequired(t => t.KYC_CaseFile)
                .WithMany(t => t.KYC_FileLog)
                .HasForeignKey(d => d.cf_ID);
            this.HasRequired(t => t.KYC_DocumentCode)
                .WithMany(t => t.KYC_FileLog)
                .HasForeignKey(d => d.DocumentCodeId);
            this.HasRequired(t => t.KYC_PartyType)
                .WithMany(t => t.KYC_FileLog)
                .HasForeignKey(d => d.PartyTypeId);

        }
    }
}
