using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_ParameterGroupsMap : EntityTypeConfiguration<TLB_ParameterGroups>
    {
        public TLB_ParameterGroupsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.GroupName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TLB_ParameterGroups");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.IsIndustryGroup).HasColumnName("IsIndustryGroup");
            this.Property(t => t.IsHRG).HasColumnName("IsHRG");
            this.Property(t => t.LastUpdate).HasColumnName("LastUpdate");
        }
    }
}
