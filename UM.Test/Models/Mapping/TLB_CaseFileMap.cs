using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_CaseFileMap : EntityTypeConfiguration<TLB_CaseFile>
    {
        public TLB_CaseFileMap()
        {
            // Primary Key
            this.HasKey(t => t.CfId);

            // Properties
            this.Property(t => t.CaseNo)
                .HasMaxLength(12);

            this.Property(t => t.AMLCaseKey)
                .HasMaxLength(15);

            this.Property(t => t.Name)
                .HasMaxLength(150);

            this.Property(t => t.CreatedBy)
                .HasMaxLength(50);

            this.Property(t => t.StatusBy)
                .HasMaxLength(50);

            this.Property(t => t.ProjectSupervisor)
                .HasMaxLength(50);

            this.Property(t => t.CaseReviewCommittee)
                .HasMaxLength(50);

            this.Property(t => t.LAssesmentTypeAnalyst)
                .HasMaxLength(50);

            this.Property(t => t.LAssesmentTypeCaseTeamLeader)
                .HasMaxLength(50);

            this.Property(t => t.LAssesmentTypeCaseReview)
                .HasMaxLength(50);

            this.Property(t => t.LAssesmentTypeCaseReviewTeamLeader)
                .HasMaxLength(50);

            this.Property(t => t.LAssesmentTypeCompliance)
                .HasMaxLength(50);

            this.Property(t => t.LAssesmentTypeSARPreparer)
                .HasMaxLength(150);

            this.Property(t => t.NAICSCode)
                .HasMaxLength(100);

            this.Property(t => t.NAICSName)
                .HasMaxLength(1000);

            this.Property(t => t.NAICSCategory)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("TLB_CaseFile");
            this.Property(t => t.CfId).HasColumnName("CfId");
            this.Property(t => t.CaseNo).HasColumnName("CaseNo");
            this.Property(t => t.AMLCaseKey).HasColumnName("AMLCaseKey");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.CategoryId).HasColumnName("CategoryId");
            this.Property(t => t.RiskProfile).HasColumnName("RiskProfile");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.StatusId).HasColumnName("StatusId");
            this.Property(t => t.DueDiligence).HasColumnName("DueDiligence");
            this.Property(t => t.MemorandumOfFact).HasColumnName("MemorandumOfFact");
            this.Property(t => t.TransactionCount).HasColumnName("TransactionCount");
            this.Property(t => t.TransactionScore).HasColumnName("TransactionScore");
            this.Property(t => t.AccountCount).HasColumnName("AccountCount");
            this.Property(t => t.AccountScore).HasColumnName("AccountScore");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.OpenDate).HasColumnName("OpenDate");
            this.Property(t => t.CloseDate).HasColumnName("CloseDate");
            this.Property(t => t.StatusDate).HasColumnName("StatusDate");
            this.Property(t => t.StatusBy).HasColumnName("StatusBy");
            this.Property(t => t.DemoteCount).HasColumnName("DemoteCount");
            this.Property(t => t.ExecutiveSummary).HasColumnName("ExecutiveSummary");
            this.Property(t => t.SARStatus).HasColumnName("SARStatus");
            this.Property(t => t.HighStatusId).HasColumnName("HighStatusId");
            this.Property(t => t.ProjectSupervisor).HasColumnName("ProjectSupervisor");
            this.Property(t => t.CaseReviewCommittee).HasColumnName("CaseReviewCommittee");
            this.Property(t => t.LAssesmentTypeAnalyst).HasColumnName("LAssesmentTypeAnalyst");
            this.Property(t => t.LAssesmentTypeCaseTeamLeader).HasColumnName("LAssesmentTypeCaseTeamLeader");
            this.Property(t => t.LAssesmentTypeCaseReview).HasColumnName("LAssesmentTypeCaseReview");
            this.Property(t => t.LAssesmentTypeCaseReviewTeamLeader).HasColumnName("LAssesmentTypeCaseReviewTeamLeader");
            this.Property(t => t.LAssesmentTypeCompliance).HasColumnName("LAssesmentTypeCompliance");
            this.Property(t => t.CaseCreationPart).HasColumnName("CaseCreationPart");
            this.Property(t => t.CaseCreationAssesmentId).HasColumnName("CaseCreationAssesmentId");
            this.Property(t => t.LAssesmentTypeSARPreparer).HasColumnName("LAssesmentTypeSARPreparer");
            this.Property(t => t.NAICSCode).HasColumnName("NAICSCode");
            this.Property(t => t.NAICSName).HasColumnName("NAICSName");
            this.Property(t => t.NAICSCategory).HasColumnName("NAICSCategory");
        }
    }
}
