using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_HistoryMap : EntityTypeConfiguration<TLB_History>
    {
        public TLB_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.User)
                .HasMaxLength(25);

            this.Property(t => t.HistoryBlob_FileName)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TLB_History");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.HistoryGroupId).HasColumnName("HistoryGroupId");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.User).HasColumnName("User");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.FKey).HasColumnName("FKey");
            this.Property(t => t.HistoryBlob).HasColumnName("HistoryBlob");
            this.Property(t => t.HistoryBlob_FileName).HasColumnName("HistoryBlob_FileName");
        }
    }
}
