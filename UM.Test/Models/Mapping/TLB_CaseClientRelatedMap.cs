using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_CaseClientRelatedMap : EntityTypeConfiguration<TLB_CaseClientRelated>
    {
        public TLB_CaseClientRelatedMap()
        {
            // Primary Key
            this.HasKey(t => t.IsClient);

            // Properties
            this.Property(t => t.TransactionPartyName)
                .HasMaxLength(150);

            this.Property(t => t.IsClient)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TLB_CaseClientRelated");
            this.Property(t => t.cfId).HasColumnName("cfId");
            this.Property(t => t.cfId2).HasColumnName("cfId2");
            this.Property(t => t.TransactionPartyName).HasColumnName("TransactionPartyName");
            this.Property(t => t.IsClient).HasColumnName("IsClient");
        }
    }
}
