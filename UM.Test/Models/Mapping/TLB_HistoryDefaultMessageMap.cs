using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_HistoryDefaultMessageMap : EntityTypeConfiguration<TLB_HistoryDefaultMessage>
    {
        public TLB_HistoryDefaultMessageMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Description)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("TLB_HistoryDefaultMessage");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.HistoryGroupId).HasColumnName("HistoryGroupId");
        }
    }
}
