using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_ResponseGridCellMap : EntityTypeConfiguration<KYC_ResponseGridCell>
    {
        public KYC_ResponseGridCellMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KYC_ResponseGridCell");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ResponseId).HasColumnName("ResponseId");
            this.Property(t => t.ColumnId).HasColumnName("ColumnId");
            this.Property(t => t.RowId).HasColumnName("RowId");
            this.Property(t => t.Response).HasColumnName("Response");

            // Relationships
            this.HasRequired(t => t.KYC_ReqGridColumn)
                .WithMany(t => t.KYC_ResponseGridCell)
                .HasForeignKey(d => d.ColumnId);
            this.HasRequired(t => t.KYC_Response)
                .WithMany(t => t.KYC_ResponseGridCell)
                .HasForeignKey(d => d.ResponseId);

        }
    }
}
