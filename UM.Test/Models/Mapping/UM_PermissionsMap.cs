using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class UM_PermissionsMap : EntityTypeConfiguration<UM_Permissions>
    {
        public UM_PermissionsMap()
        {
            // Primary Key
            this.HasKey(t => t.PermissionId);

            // Properties
            this.Property(t => t.PermissionName)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("UM_Permissions");
            this.Property(t => t.PermissionId).HasColumnName("PermissionId");
            this.Property(t => t.PermissionName).HasColumnName("PermissionName");
        }
    }
}
