using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class TLB_ParameterAliasMap : EntityTypeConfiguration<TLB_ParameterAlias>
    {
        public TLB_ParameterAliasMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Alias)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("TLB_ParameterAlias");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ParameterId).HasColumnName("ParameterId");
            this.Property(t => t.Alias).HasColumnName("Alias");
        }
    }
}
