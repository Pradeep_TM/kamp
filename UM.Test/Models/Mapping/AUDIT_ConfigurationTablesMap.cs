using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class AUDIT_ConfigurationTablesMap : EntityTypeConfiguration<AUDIT_ConfigurationTables>
    {
        public AUDIT_ConfigurationTablesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("AUDIT_ConfigurationTables");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EntityName).HasColumnName("EntityName");
            this.Property(t => t.ConfigurationXml).HasColumnName("ConfigurationXml");
        }
    }
}
