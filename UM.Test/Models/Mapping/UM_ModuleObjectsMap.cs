using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class UM_ModuleObjectsMap : EntityTypeConfiguration<UM_ModuleObjects>
    {
        public UM_ModuleObjectsMap()
        {
            // Primary Key
            this.HasKey(t => t.ModuleObjectsId);

            // Properties
            this.Property(t => t.ModuleObjectName)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("UM_ModuleObjects");
            this.Property(t => t.ModuleObjectsId).HasColumnName("ModuleObjectsId");
            this.Property(t => t.ModuleObjectName).HasColumnName("ModuleObjectName");
            this.Property(t => t.ModuleTypeId).HasColumnName("ModuleTypeId");

            // Relationships
            this.HasRequired(t => t.CORE_ModuleType)
                .WithMany(t => t.UM_ModuleObjects)
                .HasForeignKey(d => d.ModuleTypeId);

        }
    }
}
