using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class MV_SQLQueryMap : EntityTypeConfiguration<MV_SQLQuery>
    {
        public MV_SQLQueryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.QuerySyntax)
                .IsRequired();

            this.Property(t => t.QueryName)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("MV_SQLQuery");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoutineID).HasColumnName("RoutineID");
            this.Property(t => t.QuerySyntax).HasColumnName("QuerySyntax");
            this.Property(t => t.QueryName).HasColumnName("QueryName");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifedOn).HasColumnName("ModifedOn");

            // Relationships
            this.HasRequired(t => t.MV_Routine)
                .WithMany(t => t.MV_SQLQuery)
                .HasForeignKey(d => d.RoutineID);

        }
    }
}
