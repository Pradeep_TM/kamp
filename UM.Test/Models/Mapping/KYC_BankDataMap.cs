using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace UM.Test.Models.Mapping
{
    public class KYC_BankDataMap : EntityTypeConfiguration<KYC_BankData>
    {
        public KYC_BankDataMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CustomerNumber, t.HighRiskAttributeID, t.PIC_Indicator, t.OFAC_Indicator, t.C_tempID });

            // Properties
            this.Property(t => t.Id)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.CaseNo)
                .HasMaxLength(50);

            this.Property(t => t.CustomerNumber)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CustomerName)
                .HasMaxLength(255);

            this.Property(t => t.RelationshipManagerName)
                .HasMaxLength(50);

            this.Property(t => t.LegalFormID)
                .HasMaxLength(50);

            this.Property(t => t.ClientType)
                .HasMaxLength(50);

            this.Property(t => t.Risk_Rating)
                .HasMaxLength(50);

            this.Property(t => t.HighRiskAttributeID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Country_of_Incorporation)
                .HasMaxLength(150);

            this.Property(t => t.Country_of_Operation)
                .HasMaxLength(150);

            this.Property(t => t.TIN)
                .HasMaxLength(50);

            this.Property(t => t.USAPATROIT)
                .HasMaxLength(50);

            this.Property(t => t.HasSDDForm)
                .HasMaxLength(5);

            this.Property(t => t.PEP)
                .HasMaxLength(5);

            this.Property(t => t.Grandfathered)
                .HasMaxLength(5);

            this.Property(t => t.USD)
                .HasMaxLength(5);

            this.Property(t => t.USX)
                .HasMaxLength(5);

            this.Property(t => t.USN)
                .HasMaxLength(5);

            this.Property(t => t.FFI)
                .HasMaxLength(5);

            this.Property(t => t.GamingEntity)
                .HasMaxLength(5);

            this.Property(t => t.EDD)
                .HasMaxLength(5);

            this.Property(t => t.HRJ)
                .HasMaxLength(5);

            this.Property(t => t.C_tempID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CapacityClassDesc)
                .HasMaxLength(4000);

            this.Property(t => t.LegalFormDesc)
                .HasMaxLength(4000);

            this.Property(t => t.Coded)
                .HasMaxLength(4000);

            this.Property(t => t.SeriesinName)
                .HasMaxLength(4000);

            this.Property(t => t.OrderPlacerOP)
                .HasMaxLength(4000);

            this.Property(t => t.OPAccounts)
                .HasMaxLength(4000);

            this.Property(t => t.PrincipalPR)
                .HasMaxLength(4000);

            this.Property(t => t.PRAccounts)
                .HasMaxLength(4000);

            this.Property(t => t.validationStatus)
                .HasMaxLength(4000);

            this.Property(t => t.validationSource)
                .HasMaxLength(4000);

            this.Property(t => t.validationTS)
                .HasMaxLength(4000);

            this.Property(t => t.countryOfJurisdiction)
                .HasMaxLength(4000);

            this.Property(t => t.SDD)
                .HasMaxLength(4000);

            this.Property(t => t.CoJG6)
                .HasMaxLength(4000);

            this.Property(t => t.siteIsG6)
                .HasMaxLength(4000);

            this.Property(t => t.governmentIDType)
                .HasMaxLength(4000);

            this.Property(t => t.USSocialSecurityNum)
                .HasMaxLength(4000);

            this.Property(t => t.passportId)
                .HasMaxLength(4000);

            this.Property(t => t.ExchangeListed)
                .HasMaxLength(4000);

            this.Property(t => t.ExchangeMktCountry)
                .HasMaxLength(4000);

            this.Property(t => t.ExchangeMktName)
                .HasMaxLength(4000);

            this.Property(t => t.marketSegment)
                .HasMaxLength(4000);

            this.Property(t => t.HQsiteID)
                .HasMaxLength(4000);

            this.Property(t => t.HQsiteCountry)
                .HasMaxLength(4000);

            this.Property(t => t.HQcity)
                .HasMaxLength(4000);

            this.Property(t => t.HQstate)
                .HasMaxLength(4000);

            this.Property(t => t.HQpostalCode)
                .HasMaxLength(4000);

            this.Property(t => t.HQsiteValidationStatus)
                .HasMaxLength(4000);

            this.Property(t => t.HQsiteValidationSource)
                .HasMaxLength(4000);

            this.Property(t => t.HQsiteValidationTS)
                .HasMaxLength(4000);

            this.Property(t => t.ListedOnExchange)
                .HasMaxLength(5);

            this.Property(t => t.NumSalesPeople)
                .HasMaxLength(50);

            this.Property(t => t.SingleSalesPersonName)
                .HasMaxLength(50);

            this.Property(t => t.DataGatherer)
                .HasMaxLength(10);

            this.Property(t => t.Region)
                .HasMaxLength(10);

            this.Property(t => t.CashAccounts)
                .HasMaxLength(50);

            this.Property(t => t.DataGathererLastUpdated)
                .HasMaxLength(100);

            this.Property(t => t.NewPartyDataFile)
                .HasMaxLength(500);

            this.Property(t => t.Lead_OP_ID)
                .HasMaxLength(50);

            this.Property(t => t.PrimaryParty)
                .HasMaxLength(5);

            this.Property(t => t.SubAccount)
                .HasMaxLength(5);

            this.Property(t => t.EntityType)
                .HasMaxLength(50);

            this.Property(t => t.GeographicRisk)
                .HasMaxLength(50);

            this.Property(t => t.ProductType)
                .HasMaxLength(50);

            this.Property(t => t.C312EDDIdentity)
                .HasMaxLength(50);

            this.Property(t => t.CountryCode)
                .HasMaxLength(5);

            this.Property(t => t.Correspondence_Zip)
                .HasMaxLength(50);

            this.Property(t => t.RemediationPopulation)
                .HasMaxLength(50);

            this.Property(t => t.OffshoreLicense)
                .HasMaxLength(100);

            this.Property(t => t.ForeignBank)
                .HasMaxLength(100);

            this.Property(t => t.FBCSignatureDate)
                .HasMaxLength(100);

            this.Property(t => t.USTaxIDMissing)
                .HasMaxLength(100);

            this.Property(t => t.DateSigned)
                .HasMaxLength(10);

            this.Property(t => t.DupeID)
                .HasMaxLength(100);

            this.Property(t => t.AMLApproved)
                .HasMaxLength(100);

            this.Property(t => t.GlobalRiskRating)
                .HasMaxLength(50);

            this.Property(t => t.Analyst)
                .HasMaxLength(50);

            this.Property(t => t.C2ndReviewer)
                .HasMaxLength(50);

            this.Property(t => t.RID)
                .HasMaxLength(50);

            this.Property(t => t.NucleusID)
                .HasMaxLength(50);

            this.Property(t => t.QA1)
                .HasMaxLength(50);

            this.Property(t => t.QA2)
                .HasMaxLength(50);

            this.Property(t => t.QA3)
                .HasMaxLength(50);

            this.Property(t => t.QA4)
                .HasMaxLength(50);

            this.Property(t => t.QA5)
                .HasMaxLength(50);

            this.Property(t => t.QA6)
                .HasMaxLength(50);

            this.Property(t => t.QA7)
                .HasMaxLength(50);

            this.Property(t => t.QA8)
                .HasMaxLength(50);

            this.Property(t => t.QA9)
                .HasMaxLength(50);

            this.Property(t => t.QA10)
                .HasMaxLength(50);

            this.Property(t => t.QA11)
                .HasMaxLength(50);

            this.Property(t => t.QA12)
                .HasMaxLength(50);

            this.Property(t => t.QA13)
                .HasMaxLength(50);

            this.Property(t => t.QA14)
                .HasMaxLength(50);

            this.Property(t => t.QA15)
                .HasMaxLength(50);

            this.Property(t => t.QA16)
                .HasMaxLength(50);

            this.Property(t => t.QA17)
                .HasMaxLength(50);

            this.Property(t => t.QA18)
                .HasMaxLength(50);

            this.Property(t => t.QA19)
                .HasMaxLength(50);

            this.Property(t => t.QA20)
                .HasMaxLength(50);

            this.Property(t => t.QA21)
                .HasMaxLength(50);

            this.Property(t => t.QA22)
                .HasMaxLength(50);

            this.Property(t => t.QA23)
                .HasMaxLength(50);

            this.Property(t => t.DateAssigned2nd)
                .HasMaxLength(10);

            this.Property(t => t.DateCompleted2nd)
                .HasMaxLength(10);

            this.Property(t => t.InvestmentManager)
                .HasMaxLength(150);

            this.Property(t => t.InvestmentManagerCountry)
                .HasMaxLength(150);

            this.Property(t => t.RequestType)
                .HasMaxLength(150);

            this.Property(t => t.PEPsFound)
                .HasMaxLength(4);

            this.Property(t => t.PEPLogged)
                .HasMaxLength(4);

            this.Property(t => t.RequestStatus)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("KYC_BankData");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CaseNo).HasColumnName("CaseNo");
            this.Property(t => t.CustomerNumber).HasColumnName("CustomerNumber");
            this.Property(t => t.CustomerName).HasColumnName("CustomerName");
            this.Property(t => t.RelationshipManagerName).HasColumnName("RelationshipManagerName");
            this.Property(t => t.LegalFormID).HasColumnName("LegalFormID");
            this.Property(t => t.ClientType).HasColumnName("ClientType");
            this.Property(t => t.ActionSteps).HasColumnName("ActionSteps");
            this.Property(t => t.Risk_Rating).HasColumnName("Risk Rating");
            this.Property(t => t.EntityTypeID).HasColumnName("EntityTypeID");
            this.Property(t => t.RegulatoryStatusID).HasColumnName("RegulatoryStatusID");
            this.Property(t => t.SuitabilityClassificationID).HasColumnName("SuitabilityClassificationID");
            this.Property(t => t.HighRiskAttributeID).HasColumnName("HighRiskAttributeID");
            this.Property(t => t.RMactionSteps).HasColumnName("RMactionSteps");
            this.Property(t => t.DoneActionSteps).HasColumnName("DoneActionSteps");
            this.Property(t => t.Registered_Address).HasColumnName("Registered_Address");
            this.Property(t => t.Correspondence_Address).HasColumnName("Correspondence_Address");
            this.Property(t => t.PIC_Indicator).HasColumnName("PIC_Indicator");
            this.Property(t => t.OFAC_Indicator).HasColumnName("OFAC_Indicator");
            this.Property(t => t.Country_of_Incorporation).HasColumnName("Country_of_Incorporation");
            this.Property(t => t.Country_of_Operation).HasColumnName("Country_of_Operation");
            this.Property(t => t.ForeignFI).HasColumnName("ForeignFI");
            this.Property(t => t.GAOS).HasColumnName("GAOS");
            this.Property(t => t.CommitedDate).HasColumnName("CommitedDate");
            this.Property(t => t.TIN).HasColumnName("TIN");
            this.Property(t => t.SICCode).HasColumnName("SICCode");
            this.Property(t => t.RiskClass).HasColumnName("RiskClass");
            this.Property(t => t.RegisteredJurisdiction).HasColumnName("RegisteredJurisdiction");
            this.Property(t => t.CorespJurisdiction).HasColumnName("CorespJurisdiction");
            this.Property(t => t.NewEntity).HasColumnName("NewEntity");
            this.Property(t => t.RiskRanking).HasColumnName("RiskRanking");
            this.Property(t => t.GovtIDSource).HasColumnName("GovtIDSource");
            this.Property(t => t.RoleOfClient).HasColumnName("RoleOfClient");
            this.Property(t => t.USAPATROIT).HasColumnName("USAPATROIT");
            this.Property(t => t.LegalFormsID).HasColumnName("LegalFormsID");
            this.Property(t => t.CapacityClassificationID).HasColumnName("CapacityClassificationID");
            this.Property(t => t.ProductsAndServicesID).HasColumnName("ProductsAndServicesID");
            this.Property(t => t.DOB).HasColumnName("DOB");
            this.Property(t => t.RegulatorName).HasColumnName("RegulatorName");
            this.Property(t => t.RegulatedJurisdiction).HasColumnName("RegulatedJurisdiction");
            this.Property(t => t.HasSDDForm).HasColumnName("HasSDDForm");
            this.Property(t => t.PEP).HasColumnName("PEP");
            this.Property(t => t.Grandfathered).HasColumnName("Grandfathered");
            this.Property(t => t.USD).HasColumnName("USD");
            this.Property(t => t.USX).HasColumnName("USX");
            this.Property(t => t.USN).HasColumnName("USN");
            this.Property(t => t.FFI).HasColumnName("FFI");
            this.Property(t => t.GamingEntity).HasColumnName("GamingEntity");
            this.Property(t => t.EDD).HasColumnName("EDD");
            this.Property(t => t.HRJ).HasColumnName("HRJ");
            this.Property(t => t.ActiveAccounts).HasColumnName("ActiveAccounts");
            this.Property(t => t.C_tempID).HasColumnName("_tempID");
            this.Property(t => t.CapacityClassDesc).HasColumnName("CapacityClassDesc");
            this.Property(t => t.LegalFormDesc).HasColumnName("LegalFormDesc");
            this.Property(t => t.Coded).HasColumnName("Coded");
            this.Property(t => t.SeriesinName).HasColumnName("SeriesinName");
            this.Property(t => t.OrderPlacerOP).HasColumnName("OrderPlacerOP");
            this.Property(t => t.OPAccounts).HasColumnName("OPAccounts");
            this.Property(t => t.PrincipalPR).HasColumnName("PrincipalPR");
            this.Property(t => t.PRAccounts).HasColumnName("PRAccounts");
            this.Property(t => t.validationStatus).HasColumnName("validationStatus");
            this.Property(t => t.validationSource).HasColumnName("validationSource");
            this.Property(t => t.validationTS).HasColumnName("validationTS");
            this.Property(t => t.countryOfJurisdiction).HasColumnName("countryOfJurisdiction");
            this.Property(t => t.SDD).HasColumnName("SDD");
            this.Property(t => t.CoJG6).HasColumnName("CoJG6");
            this.Property(t => t.siteIsG6).HasColumnName("siteIsG6");
            this.Property(t => t.governmentIDType).HasColumnName("governmentIDType");
            this.Property(t => t.USSocialSecurityNum).HasColumnName("USSocialSecurityNum");
            this.Property(t => t.passportId).HasColumnName("passportId");
            this.Property(t => t.ExchangeListed).HasColumnName("ExchangeListed");
            this.Property(t => t.ExchangeMktCountry).HasColumnName("ExchangeMktCountry");
            this.Property(t => t.ExchangeMktName).HasColumnName("ExchangeMktName");
            this.Property(t => t.marketSegment).HasColumnName("marketSegment");
            this.Property(t => t.HQsiteID).HasColumnName("HQsiteID");
            this.Property(t => t.HQsiteCountry).HasColumnName("HQsiteCountry");
            this.Property(t => t.HQcity).HasColumnName("HQcity");
            this.Property(t => t.HQstate).HasColumnName("HQstate");
            this.Property(t => t.HQpostalCode).HasColumnName("HQpostalCode");
            this.Property(t => t.HQsiteValidationStatus).HasColumnName("HQsiteValidationStatus");
            this.Property(t => t.HQsiteValidationSource).HasColumnName("HQsiteValidationSource");
            this.Property(t => t.HQsiteValidationTS).HasColumnName("HQsiteValidationTS");
            this.Property(t => t.ListedOnExchange).HasColumnName("ListedOnExchange");
            this.Property(t => t.WebSiteAddress).HasColumnName("WebSiteAddress");
            this.Property(t => t.OFACPerformed).HasColumnName("OFACPerformed");
            this.Property(t => t.TaxStatusCertification).HasColumnName("TaxStatusCertification");
            this.Property(t => t.OFACSearchDate).HasColumnName("OFACSearchDate");
            this.Property(t => t.PATRIOTExpireDate).HasColumnName("PATRIOTExpireDate");
            this.Property(t => t.USSSN).HasColumnName("USSSN");
            this.Property(t => t.HQCountry).HasColumnName("HQCountry");
            this.Property(t => t.Order_Placer__OP_).HasColumnName("Order Placer (OP)");
            this.Property(t => t.Principal__PR_).HasColumnName("Principal (PR)");
            this.Property(t => t.ProductsAndServices).HasColumnName("ProductsAndServices");
            this.Property(t => t.InternalWatchListSearch).HasColumnName("InternalWatchListSearch");
            this.Property(t => t.DateofInternalWatchListSearch).HasColumnName("DateofInternalWatchListSearch");
            this.Property(t => t.HQStreetAddress).HasColumnName("HQStreetAddress");
            this.Property(t => t.GDSProofofRegulationorListing).HasColumnName("GDSProofofRegulationorListing");
            this.Property(t => t.GDSFormationDocument).HasColumnName("GDSFormationDocument");
            this.Property(t => t.GDSDisclosureDocument).HasColumnName("GDSDisclosureDocument");
            this.Property(t => t.GDSTaxStatusCertificationYesNo).HasColumnName("GDSTaxStatusCertificationYesNo");
            this.Property(t => t.GDSListingofShareholders).HasColumnName("GDSListingofShareholders");
            this.Property(t => t.GDSListingofPrincipals).HasColumnName("GDSListingofPrincipals");
            this.Property(t => t.GDSListingofBoardofDirectors).HasColumnName("GDSListingofBoardofDirectors");
            this.Property(t => t.GDSAnIndividualsIDdocumentation).HasColumnName("GDSAnIndividualsIDdocumentation");
            this.Property(t => t.GDSNewssearchpertainingtothecustomer).HasColumnName("GDSNewssearchpertainingtothecustomer");
            this.Property(t => t.GDSNewssearchpertainingtorelatedparties).HasColumnName("GDSNewssearchpertainingtorelatedparties");
            this.Property(t => t.GDSOFACsearches).HasColumnName("GDSOFACsearches");
            this.Property(t => t.GDSPEPsearches).HasColumnName("GDSPEPsearches");
            this.Property(t => t.GDSInternalWatchListsearches).HasColumnName("GDSInternalWatchListsearches");
            this.Property(t => t.GDSUSAPATRIOTActCertification).HasColumnName("GDSUSAPATRIOTActCertification");
            this.Property(t => t.GDSOtherCorroborativeEvidence).HasColumnName("GDSOtherCorroborativeEvidence");
            this.Property(t => t.GDSSourceofInformationforAMLProgram).HasColumnName("GDSSourceofInformationforAMLProgram");
            this.Property(t => t.GDSProofofparents).HasColumnName("GDSProofofparents");
            this.Property(t => t.GDSProofofrelationshipwithparent).HasColumnName("GDSProofofrelationshipwithparent");
            this.Property(t => t.GDSSourceofReferralnewaccounts).HasColumnName("GDSSourceofReferralnewaccounts");
            this.Property(t => t.GDSBankingReferenceInformation).HasColumnName("GDSBankingReferenceInformation");
            this.Property(t => t.GDSSiteVisitReportsnewaccounts).HasColumnName("GDSSiteVisitReportsnewaccounts");
            this.Property(t => t.GDSRelianceContract).HasColumnName("GDSRelianceContract");
            this.Property(t => t.DDProofofRegulationorListing).HasColumnName("DDProofofRegulationorListing");
            this.Property(t => t.DDFormationDocument).HasColumnName("DDFormationDocument");
            this.Property(t => t.DDDisclosureDocument).HasColumnName("DDDisclosureDocument");
            this.Property(t => t.DDTaxStatusCertificationYesNo).HasColumnName("DDTaxStatusCertificationYesNo");
            this.Property(t => t.DDListingofShareholders).HasColumnName("DDListingofShareholders");
            this.Property(t => t.DDListingofPrincipals).HasColumnName("DDListingofPrincipals");
            this.Property(t => t.DDListingofBoardofDirectors).HasColumnName("DDListingofBoardofDirectors");
            this.Property(t => t.DDAnIndividualsIDdocumentation).HasColumnName("DDAnIndividualsIDdocumentation");
            this.Property(t => t.DDNewssearchpertainingtothecustomer).HasColumnName("DDNewssearchpertainingtothecustomer");
            this.Property(t => t.DDNewssearchpertainingtorelatedparties).HasColumnName("DDNewssearchpertainingtorelatedparties");
            this.Property(t => t.DDOFACsearches).HasColumnName("DDOFACsearches");
            this.Property(t => t.DDPEPsearches).HasColumnName("DDPEPsearches");
            this.Property(t => t.DDInternalWatchListsearches).HasColumnName("DDInternalWatchListsearches");
            this.Property(t => t.DDUSAPATRIOTActCertification).HasColumnName("DDUSAPATRIOTActCertification");
            this.Property(t => t.DDOtherCorroborativeEvidence).HasColumnName("DDOtherCorroborativeEvidence");
            this.Property(t => t.DDSourceofInformationforAMLProgram).HasColumnName("DDSourceofInformationforAMLProgram");
            this.Property(t => t.DDProofofparents).HasColumnName("DDProofofparents");
            this.Property(t => t.DDProofofrelationshipwithparent).HasColumnName("DDProofofrelationshipwithparent");
            this.Property(t => t.DDSourceofReferralnewaccounts).HasColumnName("DDSourceofReferralnewaccounts");
            this.Property(t => t.DDBankingReferenceInformation).HasColumnName("DDBankingReferenceInformation");
            this.Property(t => t.DDSiteVisitReportsnewaccounts).HasColumnName("DDSiteVisitReportsnewaccounts");
            this.Property(t => t.DDRelianceContract).HasColumnName("DDRelianceContract");
            this.Property(t => t.GDSSDDForm).HasColumnName("GDSSDDForm");
            this.Property(t => t.GDSEDDForm).HasColumnName("GDSEDDForm");
            this.Property(t => t.GDS312Form).HasColumnName("GDS312Form");
            this.Property(t => t.GDSPEPForm).HasColumnName("GDSPEPForm");
            this.Property(t => t.DDSDDForm).HasColumnName("DDSDDForm");
            this.Property(t => t.DDEDDForm).HasColumnName("DDEDDForm");
            this.Property(t => t.DD312Form).HasColumnName("DD312Form");
            this.Property(t => t.DDPEPForm).HasColumnName("DDPEPForm");
            this.Property(t => t.PEPSearch).HasColumnName("PEPSearch");
            this.Property(t => t.DateofPEPSearch).HasColumnName("DateofPEPSearch");
            this.Property(t => t.NegetiveNewsSearch).HasColumnName("NegetiveNewsSearch");
            this.Property(t => t.NegetiveNewsSearchDate).HasColumnName("NegetiveNewsSearchDate");
            this.Property(t => t.NegativeNewsIdentified).HasColumnName("NegativeNewsIdentified");
            this.Property(t => t.NumSalesPeople).HasColumnName("NumSalesPeople");
            this.Property(t => t.SingleSalesPersonName).HasColumnName("SingleSalesPersonName");
            this.Property(t => t.FirstLevelQA).HasColumnName("FirstLevelQA");
            this.Property(t => t.FirstLevelQAByU_ID).HasColumnName("FirstLevelQAByU_ID");
            this.Property(t => t.SecondLevelQA).HasColumnName("SecondLevelQA");
            this.Property(t => t.SecondLevelQAByU_ID).HasColumnName("SecondLevelQAByU_ID");
            this.Property(t => t.BankReviewed).HasColumnName("BankReviewed");
            this.Property(t => t.BankReviewedByU_ID).HasColumnName("BankReviewedByU_ID");
            this.Property(t => t.RevisedPerBankComments).HasColumnName("RevisedPerBankComments");
            this.Property(t => t.RevisedPerBankCommentsByU_ID).HasColumnName("RevisedPerBankCommentsByU_ID");
            this.Property(t => t.FirstLevelQADate).HasColumnName("FirstLevelQADate");
            this.Property(t => t.SecondLevelQADate).HasColumnName("SecondLevelQADate");
            this.Property(t => t.BankReviewedDate).HasColumnName("BankReviewedDate");
            this.Property(t => t.RevisedPerBankCommentsDate).HasColumnName("RevisedPerBankCommentsDate");
            this.Property(t => t.SUITQuestion).HasColumnName("SUITQuestion");
            this.Property(t => t.SUITQuestionByU_ID).HasColumnName("SUITQuestionByU_ID");
            this.Property(t => t.SUITQuestionDate).HasColumnName("SUITQuestionDate");
            this.Property(t => t.DataGatherer).HasColumnName("DataGatherer");
            this.Property(t => t.Region).HasColumnName("Region");
            this.Property(t => t.CashAccounts).HasColumnName("CashAccounts");
            this.Property(t => t.DataGathererLastUpdated).HasColumnName("DataGathererLastUpdated");
            this.Property(t => t.RfiTypeID).HasColumnName("RfiTypeID");
            this.Property(t => t.NewParty).HasColumnName("NewParty");
            this.Property(t => t.NewPartyDataFile).HasColumnName("NewPartyDataFile");
            this.Property(t => t.Lead_OP_ID).HasColumnName("Lead_OP_ID");
            this.Property(t => t.IsHedgeFund).HasColumnName("IsHedgeFund");
            this.Property(t => t.RoleOfClientID).HasColumnName("RoleOfClientID");
            this.Property(t => t.PrimaryParty).HasColumnName("PrimaryParty");
            this.Property(t => t.PrimaryPartyName).HasColumnName("PrimaryPartyName");
            this.Property(t => t.SubAccount).HasColumnName("SubAccount");
            this.Property(t => t.EntityType).HasColumnName("EntityType");
            this.Property(t => t.GeographicRisk).HasColumnName("GeographicRisk");
            this.Property(t => t.ProductType).HasColumnName("ProductType");
            this.Property(t => t.AdditionalHighRiskFactor).HasColumnName("AdditionalHighRiskFactor");
            this.Property(t => t.RiskProfile).HasColumnName("RiskProfile");
            this.Property(t => t.DateOfFormation).HasColumnName("DateOfFormation");
            this.Property(t => t.C312EDDIdentity).HasColumnName("312EDDIdentity");
            this.Property(t => t.AddressDescription).HasColumnName("AddressDescription");
            this.Property(t => t.StreetAddr1).HasColumnName("StreetAddr1");
            this.Property(t => t.StreetAddr2).HasColumnName("StreetAddr2");
            this.Property(t => t.StreetAddr4).HasColumnName("StreetAddr4");
            this.Property(t => t.StreetAddr3).HasColumnName("StreetAddr3");
            this.Property(t => t.CountryCode).HasColumnName("CountryCode");
            this.Property(t => t.Correspondence_City).HasColumnName("Correspondence_City");
            this.Property(t => t.Correspondence_State).HasColumnName("Correspondence_State");
            this.Property(t => t.Correspondence_Zip).HasColumnName("Correspondence_Zip");
            this.Property(t => t.RemediationPopulation).HasColumnName("RemediationPopulation");
            this.Property(t => t.OffshoreLicense).HasColumnName("OffshoreLicense");
            this.Property(t => t.ForeignBank).HasColumnName("ForeignBank");
            this.Property(t => t.FBCSignatureDate).HasColumnName("FBCSignatureDate");
            this.Property(t => t.USTaxIDMissing).HasColumnName("USTaxIDMissing");
            this.Property(t => t.DateSigned).HasColumnName("DateSigned");
            this.Property(t => t.DupeID).HasColumnName("DupeID");
            this.Property(t => t.AMLApproved).HasColumnName("AMLApproved");
            this.Property(t => t.GlobalRiskRating).HasColumnName("GlobalRiskRating");
            this.Property(t => t.ReasonForEscalation).HasColumnName("ReasonForEscalation");
            this.Property(t => t.Analyst).HasColumnName("Analyst");
            this.Property(t => t.C2ndReviewer).HasColumnName("2ndReviewer");
            this.Property(t => t.RID).HasColumnName("RID");
            this.Property(t => t.NucleusID).HasColumnName("NucleusID");
            this.Property(t => t.QA1).HasColumnName("QA1");
            this.Property(t => t.QA2).HasColumnName("QA2");
            this.Property(t => t.QA3).HasColumnName("QA3");
            this.Property(t => t.QA4).HasColumnName("QA4");
            this.Property(t => t.QA5).HasColumnName("QA5");
            this.Property(t => t.QA6).HasColumnName("QA6");
            this.Property(t => t.QA7).HasColumnName("QA7");
            this.Property(t => t.QA8).HasColumnName("QA8");
            this.Property(t => t.QA9).HasColumnName("QA9");
            this.Property(t => t.QA10).HasColumnName("QA10");
            this.Property(t => t.QA11).HasColumnName("QA11");
            this.Property(t => t.QA12).HasColumnName("QA12");
            this.Property(t => t.QA13).HasColumnName("QA13");
            this.Property(t => t.QA14).HasColumnName("QA14");
            this.Property(t => t.QA15).HasColumnName("QA15");
            this.Property(t => t.QA16).HasColumnName("QA16");
            this.Property(t => t.QA17).HasColumnName("QA17");
            this.Property(t => t.QA18).HasColumnName("QA18");
            this.Property(t => t.QA19).HasColumnName("QA19");
            this.Property(t => t.QA20).HasColumnName("QA20");
            this.Property(t => t.QA21).HasColumnName("QA21");
            this.Property(t => t.QA22).HasColumnName("QA22");
            this.Property(t => t.QA23).HasColumnName("QA23");
            this.Property(t => t.DateAssigned).HasColumnName("DateAssigned");
            this.Property(t => t.DateCompleted).HasColumnName("DateCompleted");
            this.Property(t => t.DateAssigned2nd).HasColumnName("DateAssigned2nd");
            this.Property(t => t.DateCompleted2nd).HasColumnName("DateCompleted2nd");
            this.Property(t => t.InvestmentManager).HasColumnName("InvestmentManager");
            this.Property(t => t.InvestmentManagerCountry).HasColumnName("InvestmentManagerCountry");
            this.Property(t => t.RequestType).HasColumnName("RequestType");
            this.Property(t => t.PEPsFound).HasColumnName("PEPsFound");
            this.Property(t => t.PEPLogged).HasColumnName("PEPLogged");
            this.Property(t => t.CustomerID).HasColumnName("CustomerID");
            this.Property(t => t.CustomerIDSource).HasColumnName("CustomerIDSource");
            this.Property(t => t.RequestStatus).HasColumnName("RequestStatus");
            this.Property(t => t.cf_RFI_Status_LKUP).HasColumnName("cf_RFI_Status_LKUP");
            this.Property(t => t.PSG_Client).HasColumnName("PSG_Client");
            this.Property(t => t.cf_RFI_Status_Dt).HasColumnName("cf_RFI_Status_Dt");
            this.Property(t => t.Borrower).HasColumnName("Borrower");
            this.Property(t => t.BorrowerNonPrimary).HasColumnName("BorrowerNonPrimary");
            this.Property(t => t.IA_LastModifiedDate).HasColumnName("IA_LastModifiedDate");
            this.Property(t => t.Parent_LastModifiedDate).HasColumnName("Parent_LastModifiedDate");
            this.Property(t => t.EntityLastModifiedDate).HasColumnName("EntityLastModifiedDate");
            this.Property(t => t.EntityCreatedDate).HasColumnName("EntityCreatedDate");
        }
    }
}
