using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_TransTotalScore
    {
        public int Id { get; set; }
        public Nullable<int> TotalScore { get; set; }
    }
}
