using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class lkp_AnalysisType
    {
        public lkp_AnalysisType()
        {
            this.CORE_ModuleValidationTypeMapping = new List<CORE_ModuleValidationTypeMapping>();
            this.MV_Resultant = new List<MV_Resultant>();
        }

        public int Id { get; set; }
        public string AnalysisName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public virtual ICollection<CORE_ModuleValidationTypeMapping> CORE_ModuleValidationTypeMapping { get; set; }
        public virtual ICollection<MV_Resultant> MV_Resultant { get; set; }
    }
}
