using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_Assignment
    {
        public WF_Assignment()
        {
            this.WF_AssignedTo = new List<WF_AssignedTo>();
            this.WF_WorkflowTracking = new List<WF_WorkflowTracking>();
        }

        public long Id { get; set; }
        public string CaseNo { get; set; }
        public string Status { get; set; }
        public string NextState { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<WF_AssignedTo> WF_AssignedTo { get; set; }
        public virtual ICollection<WF_WorkflowTracking> WF_WorkflowTracking { get; set; }
    }
}
