using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_ResponseOption
    {
        public long Id { get; set; }
        public Nullable<long> ResponseId { get; set; }
        public long ReqOptionId { get; set; }
        public Nullable<long> GridCellId { get; set; }
        public virtual KYC_Response KYC_Response { get; set; }
        public virtual KYC_ResponseGridCell KYC_ResponseGridCell { get; set; }
    }
}
