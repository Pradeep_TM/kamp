using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_DuplicateCase
    {
        public int ID { get; set; }
        public string SourceID { get; set; }
        public string CMTCaseNumber { get; set; }
        public string CustomerName { get; set; }
        public string RemediationPopulation { get; set; }
        public string CISCode { get; set; }
        public string Duplicate { get; set; }
        public string DuplicateOf { get; set; }
        public int Consider { get; set; }
        public Nullable<int> Master { get; set; }
        public string Reason { get; set; }
        public string SpecialComments { get; set; }
    }
}
