using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_CaseClientRelated
    {
        public Nullable<int> cfId { get; set; }
        public Nullable<int> cfId2 { get; set; }
        public string TransactionPartyName { get; set; }
        public int IsClient { get; set; }
    }
}
