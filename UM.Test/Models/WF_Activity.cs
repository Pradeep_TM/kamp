using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class WF_Activity
    {
        public int Id { get; set; }
        public int ToStateId { get; set; }
        public string MailTemplate { get; set; }
        public int StepId { get; set; }
        public int ActionId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public virtual WF_Lookup_Action WF_Lookup_Action { get; set; }
        public virtual WF_States WF_States { get; set; }
        public virtual WF_Steps WF_Steps { get; set; }
    }
}
