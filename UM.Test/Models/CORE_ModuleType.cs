using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class CORE_ModuleType
    {
        public CORE_ModuleType()
        {
            this.UM_ModuleObjects = new List<UM_ModuleObjects>();
            this.UM_UserInGroup = new List<UM_UserInGroup>();
        }

        public int ModuleTypeId { get; set; }
        public string ModuleName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public virtual ICollection<UM_ModuleObjects> UM_ModuleObjects { get; set; }
        public virtual ICollection<UM_UserInGroup> UM_UserInGroup { get; set; }
    }
}
