using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_BinaryLargeObject
    {
        public int Id { get; set; }
        public byte[] Object { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public int cf_Id { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
    }
}
