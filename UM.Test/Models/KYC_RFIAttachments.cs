using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_RFIAttachments
    {
        public int Id { get; set; }
        public int RFIId { get; set; }
        public string AttachmentName { get; set; }
        public byte[] Attachment { get; set; }
        public virtual KYC_RequestForInformation KYC_RequestForInformation { get; set; }
    }
}
