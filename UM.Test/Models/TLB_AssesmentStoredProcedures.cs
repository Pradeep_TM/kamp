using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_AssesmentStoredProcedures
    {
        public int Id { get; set; }
        public string AssesmentRuleName { get; set; }
        public string AssesmentStoredProcedureName { get; set; }
    }
}
