using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_FormElement
    {
        public KYC_FormElement()
        {
            this.KYC_FormElementOption = new List<KYC_FormElementOption>();
            this.KYC_KPMGEditable = new List<KYC_KPMGEditable>();
        }

        public int Id { get; set; }
        public string Key { get; set; }
        public string DisplayName { get; set; }
        public int ValueType { get; set; }
        public bool IsMandatory { get; set; }
        public string OptionsHeading { get; set; }
        public int FormId { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public virtual ICollection<KYC_FormElementOption> KYC_FormElementOption { get; set; }
        public virtual KYC_ReqType KYC_ReqType { get; set; }
        public virtual ICollection<KYC_KPMGEditable> KYC_KPMGEditable { get; set; }
    }
}
