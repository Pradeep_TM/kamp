using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_KPMGEditable
    {
        public int cf_ID { get; set; }
        public string Value { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int GroupId { get; set; }
        public Nullable<int> FormElementId { get; set; }
        public virtual KYC_CaseFile KYC_CaseFile { get; set; }
        public virtual KYC_FormElement KYC_FormElement { get; set; }
    }
}
