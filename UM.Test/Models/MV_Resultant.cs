using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class MV_Resultant
    {
        public long Id { get; set; }
        public long RoutineID { get; set; }
        public string ResultantQuery { get; set; }
        public int AnalysisTypeID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public virtual lkp_AnalysisType lkp_AnalysisType { get; set; }
        public virtual MV_Routine MV_Routine { get; set; }
    }
}
