using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_ReqGridColumn
    {
        public KYC_ReqGridColumn()
        {
            this.KYC_ResponseGridCell = new List<KYC_ResponseGridCell>();
        }

        public long Id { get; set; }
        public long RequirementId { get; set; }
        public string ColumnName { get; set; }
        public int ReqTypeId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int ModifiedBy { get; set; }
        public virtual ICollection<KYC_ResponseGridCell> KYC_ResponseGridCell { get; set; }
        public virtual KYC_ReqType KYC_ReqType { get; set; }
        public virtual KYC_Requirement KYC_Requirement { get; set; }
    }
}
