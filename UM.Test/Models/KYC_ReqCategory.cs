using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_ReqCategory
    {
        public KYC_ReqCategory()
        {
            this.KYC_Requirement = new List<KYC_Requirement>();
        }

        public int Id { get; set; }
        public string CategoryName { get; set; }
        public bool IsValid { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int ReqClassificationId { get; set; }
        public virtual ICollection<KYC_Requirement> KYC_Requirement { get; set; }
        public virtual KYC_ReqClassification KYC_ReqClassification { get; set; }
    }
}
