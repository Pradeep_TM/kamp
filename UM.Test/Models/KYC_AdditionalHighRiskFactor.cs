using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_AdditionalHighRiskFactor
    {
        public int ID { get; set; }
        public string AdditionalHighRiskFactor { get; set; }
        public Nullable<bool> Valid { get; set; }
    }
}
