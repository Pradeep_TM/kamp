using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class TLB_HistoryGroups
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
