using System;
using System.Collections.Generic;

namespace UM.Test.Models
{
    public partial class KYC_PartyType
    {
        public KYC_PartyType()
        {
            this.KYC_FileLog = new List<KYC_FileLog>();
        }

        public int Id { get; set; }
        public string PartyType { get; set; }
        public bool Valid { get; set; }
        public virtual ICollection<KYC_FileLog> KYC_FileLog { get; set; }
    }
}
