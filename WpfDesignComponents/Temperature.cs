﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDesignComponents
{
    public class Temperature
    {
        public Temperature(int sNo,string state, string city, double max, double min, string state1, string city1, double max1, double min1)
        {
            SNo = sNo;
            State = state;
            City = city;
            MaxTemperature = max;
            MinTemperature = min;
            State1 = state1;
            City1 = city1;
            MaxTemperature1 = max1;
            MinTemperature1 = min1;
        }
        public string State { get; set; }
        public int SNo { get; set; }
        public string City { get; set; }
        public double MaxTemperature { get; set; }
        public double MinTemperature { get; set; }

        public string State1 { get; set; }
        public string City1 { get; set; }
        public double MaxTemperature1 { get; set; }
        public double MinTemperature1 { get; set; }
    }
}
