﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDesignComponents
{
    class MainWindowViewModel : INotifyPropertyChanged, IPageControlContract
    {
        private ObservableCollection<Temperature> temperatureCollection;
        public ObservableCollection<Temperature> TemperatureCollection
        {
            get { return temperatureCollection; }
            set
            {
                temperatureCollection = value;
                RaisePropertyChanged("TemperatureCollection");
            }
        }
        public MainWindowViewModel()
        {
            TemperatureCollection = new ObservableCollection<Temperature>();
            BindData();
        }

      


        private void BindData()
        {
            TemperatureCollection.Add(new Temperature(1,"Gujarat", "Ahmedabad", 42.32, 25.36, "Gujarat", "Ahmedabad", 42.32, 25.36));
            TemperatureCollection.Add(new Temperature(2,"Gujarat", "Surat", 39.45, 22.30, "Gujarat", "Surat", 39.45, 22.30));
            TemperatureCollection.Add(new Temperature(3,"Gujarat", "Vadodara", 40.13, 26.75,"Gujarat", "Vadodara", 40.13, 26.75));
            TemperatureCollection.Add(new Temperature(4,"Maharashtra", "Mumbai", 40.40, 23.23,"Maharashtra", "Mumbai", 40.40, 23.23));
            TemperatureCollection.Add(new Temperature(5,"Maharashtra", "Pune", 40.69, 23.10, "Maharashtra", "Pune", 40.69, 23.10));
            TemperatureCollection.Add(new Temperature(6,"Karnataka", "Banglore", 37.15, 20.06,"Karnataka", "Banglore", 37.15, 20.06));
            TemperatureCollection.Add(new Temperature(7,"Gujarat", "Ahmedabad", 42.32, 25.36, "Gujarat", "Ahmedabad", 42.32, 25.36));
            TemperatureCollection.Add(new Temperature(8,"Gujarat", "Surat", 39.45, 22.30, "Gujarat", "Surat", 39.45, 22.30));
            TemperatureCollection.Add(new Temperature(9,"Gujarat", "Vadodara", 40.13, 26.75, "Gujarat", "Vadodara", 40.13, 26.75));
            TemperatureCollection.Add(new Temperature(10,"Maharashtra", "Mumbai", 40.40, 23.23, "Maharashtra", "Mumbai", 40.40, 23.23));
            TemperatureCollection.Add(new Temperature(11, "Maharashtra", "Pune", 40.69, 23.10, "Maharashtra", "Pune", 40.69, 23.10));
            TemperatureCollection.Add(new Temperature(12, "Karnataka", "Banglore", 37.15, 20.06, "Karnataka", "Banglore", 37.15, 20.06));
            TemperatureCollection.Add(new Temperature(13, "Karnataka", "Banglore", 37.15, 20.06, "Karnataka", "Banglore", 37.15, 20.06));
            TemperatureCollection.Add(new Temperature(14, "Gujarat", "Ahmedabad", 42.32, 25.36, "Gujarat", "Ahmedabad", 42.32, 25.36));
            TemperatureCollection.Add(new Temperature(15, "Gujarat", "Surat", 39.45, 22.30, "Gujarat", "Surat", 39.45, 22.30));
            TemperatureCollection.Add(new Temperature(16, "Gujarat", "Vadodara", 40.13, 26.75, "Gujarat", "Vadodara", 40.13, 26.75));
            TemperatureCollection.Add(new Temperature(17, "Maharashtra", "Mumbai", 40.40, 23.23, "Maharashtra", "Mumbai", 40.40, 23.23));
            TemperatureCollection.Add(new Temperature(18, "Maharashtra", "Pune", 40.69, 23.10, "Maharashtra", "Pune", 40.69, 23.10));
            TemperatureCollection.Add(new Temperature(19, "Karnataka", "Banglore", 37.15, 20.06, "Karnataka", "Banglore", 37.15, 20.06));
            TemperatureCollection.Add(new Temperature(20, "Gujarat", "Ahmedabad", 42.32, 25.36, "Gujarat", "Ahmedabad", 42.32, 25.36));
            TemperatureCollection.Add(new Temperature(21, "Gujarat", "Surat", 39.45, 22.30, "Gujarat", "Surat", 39.45, 22.30));
            TemperatureCollection.Add(new Temperature(22, "Gujarat", "Vadodara", 40.13, 26.75, "Gujarat", "Vadodara", 40.13, 26.75));
            TemperatureCollection.Add(new Temperature(23, "Maharashtra", "Mumbai", 40.40, 23.23, "Maharashtra", "Mumbai", 40.40, 23.23));
            TemperatureCollection.Add(new Temperature(24, "Maharashtra", "Pune", 40.69, 23.10, "Maharashtra", "Pune", 40.69, 23.10));
            TemperatureCollection.Add(new Temperature(25, "Karnataka", "Banglore", 37.15, 20.06, "Karnataka", "Banglore", 37.15, 20.06));
            TemperatureCollection.Add(new Temperature(26, "Gujarat", "Ahmedabad", 42.32, 25.36, "Gujarat", "Ahmedabad", 42.32, 25.36));
            TemperatureCollection.Add(new Temperature(27, "Gujarat", "Surat", 39.45, 22.30, "Gujarat", "Surat", 39.45, 22.30));
            TemperatureCollection.Add(new Temperature(28, "Gujarat", "Vadodara", 40.13, 26.75, "Gujarat", "Vadodara", 40.13, 26.75));
            TemperatureCollection.Add(new Temperature(29, "Maharashtra", "Mumbai", 40.40, 23.23, "Maharashtra", "Mumbai", 40.40, 23.23));
            TemperatureCollection.Add(new Temperature(30, "Maharashtra", "Pune", 40.69, 23.10, "Maharashtra", "Pune", 40.69, 23.10));
            TemperatureCollection.Add(new Temperature(31, "Karnataka", "Banglore", 37.15, 20.06, "Karnataka", "Banglore", 37.15, 20.06));
            TemperatureCollection.Add(new Temperature(32, "Karnataka", "Banglore", 37.15, 20.06, "Karnataka", "Banglore", 37.15, 20.06));
            TemperatureCollection.Add(new Temperature(33, "Gujarat", "Ahmedabad", 42.32, 25.36, "Gujarat", "Ahmedabad", 42.32, 25.36));
            TemperatureCollection.Add(new Temperature(34, "Gujarat", "Surat", 39.45, 22.30, "Gujarat", "Surat", 39.45, 22.30));
            TemperatureCollection.Add(new Temperature(35, "Gujarat", "Vadodara", 40.13, 26.75, "Gujarat", "Vadodara", 40.13, 26.75));
            TemperatureCollection.Add(new Temperature(36, "Maharashtra", "Mumbai", 40.40, 23.23, "Maharashtra", "Mumbai", 40.40, 23.23));
            TemperatureCollection.Add(new Temperature(37, "Maharashtra", "Pune", 40.69, 23.10, "Maharashtra", "Pune", 40.69, 23.10));
            TemperatureCollection.Add(new Temperature(38, "Karnataka", "Banglore", 37.15, 20.06, "Karnataka", "Banglore", 37.15, 20.06));
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IPageControlContract Members

        public uint GetTotalCount()
        {
            return (uint)TemperatureCollection.Count;
        }

        public ICollection<object> GetRecordsBy(uint startingIndex, uint numberOfRecords, object filterTag)
        {
            if (startingIndex >= TemperatureCollection.Count)
            {
                return new List<object>();
            }

            List<Temperature> result = new List<Temperature>();

            for (int i = (int)startingIndex; i < TemperatureCollection.Count && i < startingIndex + numberOfRecords; i++)
            {
                result.Add(TemperatureCollection[i]);
            }

            return result.ToList<object>();
        }

        #endregion
    }
}
