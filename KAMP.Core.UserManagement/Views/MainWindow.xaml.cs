﻿using KAMP.Core.UM.UserControls;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.UserControls;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;

namespace KAMP.Core.UserManagement
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public UMViewModel ViewModel;
        Dictionary<UserControlEnum, UIElement> dict = new Dictionary<UserControlEnum, UIElement>();

        public MainWindow()
        {
            InitializeComponent();

            Loaded += UMLoaded;

        }

        private void UMLoaded(object sender, RoutedEventArgs e)
        {
            AssignDataContext();

            ViewModel.NavigationControlViewModel.LoadUserControl += LoadUserControl;
            // InitializeUserControls();
        }

        private void AssignDataContext()
        {
            ViewModel = new UMViewModel() { SelectedControl = UserControlEnum.AddUser };
            UCNavigation.DataContext = ViewModel.NavigationControlViewModel;
            UCHeader.DataContext = ViewModel.HeaderViewModel;
            ViewModel.HeaderViewModel.CurrentContent = "User Management";
            UCHeader.CloseApp += CloseApplication;
        }

        private void CloseApplication()
        {
            var result = MessageBoxControl.Show("Are You Sure You Want To Close The Application?", MessageBoxButton.YesNo, MessageType.Alert);
            if (result == MessageBoxResult.Yes)
                Application.Current.Shutdown();
        }

        private UserControl GetUserControl(UserControlEnum uc)
        {
            switch (uc)
            {
                case UserControlEnum.AddUser:
                    return new AddUser();

                case UserControlEnum.AddRole:
                    return new AddRole();

                case UserControlEnum.AddGroup:
                    return new AddGroup();

                case UserControlEnum.AddPermission:
                    return new AddPermission();
                case UserControlEnum.RoleInPermissionMapping:
                    return new RoleInPermissionMapping();
                case UserControlEnum.UserInGroupMapping:
                    return new UserInGroupMapping();
                case UserControlEnum.UserInRoleMapping:
                    return new UserInRoleMapping();
                default:
                    return null;
            }
        }

        private void LoadUserControl(string ucName)
        {
            var userControlName = ucName.Replace(" ", string.Empty);
            ViewModel.HeaderViewModel.CurrentContent = ucName;
            var userControlKey = (UserControlEnum)Enum.Parse(typeof(UserControlEnum), userControlName);
            var userControl = GetUserControl(userControlKey);

            if (currentLoadedUC.IsNotNull() && userControl.IsNotNull() && currentLoadedUC.ToString().Equals(userControl.ToString()))
                return;
            CastUserControlToPageControl(userControl);
            GridUCContainer.Children.Remove(currentLoadedUC);
            GridUCContainer.Children.Add(userControl);
            currentLoadedUC = userControl;



        }

        private static void CastUserControlToPageControl(UserControl userControl)
        {
            try
            {
                var iPageControl = (KAMP.Core.UserManagement.AppCode.IPageControl)userControl;
                iPageControl.InitializeData();
            }
            catch
            {

            }
        }

        private void RemovePreviousUC(UserControlEnum currentUC)
        {
            if (!ViewModel.SelectedControl.Equals(currentUC))
            {
                var removeChild = dict[ViewModel.SelectedControl];
                GridUCContainer.Children.Remove(removeChild);
                ViewModel.SelectedControl = currentUC;
            }
            else
            {
                var removeChild = dict[currentUC];
                GridUCContainer.Children.Remove(removeChild);
            }
        }

        public UserControl currentLoadedUC { get; set; }
    }
}
