﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    using System.Windows.Input;
    public class UserInRoleViewModel : INotifyPropertyChanged
    {
        private long _userId;
        private long _roleId;
        private List<RoleViewModel> _role;
        private List<UserViewModel> _user;
        private long _userRoleMapId;
        public long UserId { get { return _userId; } set { PropertyChanged.HandleValueChange(this, value, ref _userId, (x) => x.UserId); } }
        public long RoleId { get { return _roleId; } set { PropertyChanged.HandleValueChange(this, value, ref _roleId, (x) => x.RoleId); } }
        public List<RoleViewModel> UserRoles { get; set; }
        public List<UserViewModel> Users { get; set; }
        public long UserRoleMapId { get { return _userRoleMapId; } set { PropertyChanged.HandleValueChange(this, value, ref _userRoleMapId, (x) => x.UserRoleMapId); } }
        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        private string _userName;
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userName, (x) => x.UserName);
            }
        }

        private string _roleName;
        public string RoleName
        {
            get
            {
                return _roleName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _roleName, (x) => x.RoleName);
            }
        }

        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }

        private ICommand _load;

        public ICommand Load
        {
            get
            {

                if (_load == null)
                    _load = new DelegateCommand(() =>
                    {
                        if (OnLoadClick.IsNotNull())
                            OnLoadClick();
                    });
                return _load;
            }

        }

        private ICommand _delete;

        public ICommand Delete
        {
            get
            {

                if (_delete == null)
                    _delete = new DelegateCommand<long>((s) =>
                    {
                        if (OnDeleteClick.IsNotNull())
                            OnDeleteClick(s);
                    });
                return _delete;
            }

        }

        public Action OnSubmitClick;
        public Action<long> OnDeleteClick;
        public Action OnLoadClick;
        private List<UserInRoleViewModel> _userInRoleViewModel;

        public List<UserInRoleViewModel> UserInRoleViewModels
        {
            get { return _userInRoleViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userInRoleViewModel, (x) => x.UserInRoleViewModels); }
        }

        public string BdrBrushColor { get; set; }
        public string LblForegroundColor { get; set; }
        public string WindowBackGroundColor { get; set; }
        public string CheckBoxFillColor { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
