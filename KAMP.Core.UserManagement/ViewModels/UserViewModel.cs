﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using System.Windows.Input;

namespace KAMP.Core.UserManagement.ViewModels
{

    using Repository.UM;
    public class UserViewModel : INotifyPropertyChanged
    {
        private long _userId;

        public long UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userId, (x) => x.UserId);
            }
        }

        private string _userName;
        [Required(ErrorMessage = "User name is mandatory.")]
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value.Trim(), ref _userName, (x) => x.UserName);
            }
        }


        private string _firstName;
        [Required(ErrorMessage = "First name is mandatory.")]
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value.Trim(), ref _firstName, (x) => x.FirstName);
            }
        }

        private string _lastName;
        [Required(ErrorMessage = "Last name is mandatory.")]
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value.Trim(), ref _lastName, (x) => x.LastName);
            }
        }

        private string _email;
        [Required(ErrorMessage = "Email is mandatory.")]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value.Trim(), ref _email, (x) => x.Email);
            }
        }

        private bool _isActive;
        [Required(ErrorMessage = "IsActive is mandatory.")]
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isActive, (x) => x.IsActive);
            }
        }
        private bool _isSuperAdmin;
        public bool IsSuperAdmin
        {
            get
            {
                return _isSuperAdmin;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isSuperAdmin, (x) => x.IsSuperAdmin);
            }
        }

        private DateTime _createdDate;

        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdDate, (x) => x.CreatedDate);
            }
        }

        private DateTime _modifiedDate;

        public DateTime ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedDate, (x) => x.ModifiedDate);
            }
        }

        private long _createdBy;

        public long CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy);
            }
        }

        private long _modifiedBy;

        public long ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedBy, (x) => x.ModifiedBy);
            }
        }

        private ICommand _load;

        public ICommand Load
        {
            get
            {

                if (_load == null)
                    _load = new DelegateCommand(() =>
                    {
                        if (OnLoadClick.IsNotNull())
                            OnLoadClick();
                    });
                return _load;
            }

        }
        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }
        private ICommand _delete;
        public ICommand Delete
        {
            get
            {

                if (_delete == null)
                    _delete = new DelegateCommand<long>((s) =>
                    {
                        if (OnDeleteClick.IsNotNull())
                            OnDeleteClick(s);
                    });
                return _delete;
            }
        }
        private ICommand _edit;
        public ICommand Edit
        {
            get
            {

                if (_edit == null)
                    _edit = new DelegateCommand<long>((s) =>
                    {
                        if (OnEditClick.IsNotNull())
                            OnEditClick(s);
                    });
                return _edit;
            }
        }
        private List<User> _users;

        public List<User> Users
        {
            get { return _users; }
            set { PropertyChanged.HandleValueChange(this, value, ref _users, (x) => x.Users); }
        }


        public string BdrBrushColor { get; set; }
        public string LblForegroundColor { get; set; }
        public string WindowBackGroundColor { get; set; }
        public string CheckBoxFillColor { get; set; }

        public Action OnSubmitClick;
        public Action OnLoadClick;
        public Action<long> OnDeleteClick;
        public Action<long> OnEditClick;
        public event PropertyChangedEventHandler PropertyChanged;



    }
}
