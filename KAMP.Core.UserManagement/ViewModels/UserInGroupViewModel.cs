﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    using System.Windows.Input;
    public class UserInGroupViewModel : INotifyPropertyChanged
    {
        private long _userGrpMapId;
        public long UserGrpMapId
        {
            get
            {
                return _userGrpMapId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userGrpMapId, (x) => x.UserGrpMapId);
            }
        }

        private long _userId;
        private long _userGroupId;
        private ObservableCollection<ModuleTypeViewModel> _module;
        private List<GroupViewModel> _userGroup;
        private List<UserViewModel> _user;
        private int _moduleId;
        private long _userRoleMapId;
        public long UserId { get { return _userId; } set { PropertyChanged.HandleValueChange(this, value, ref _userId, (x) => x.UserId); } }
        public long UserGroupId { get { return _userGroupId; } set { PropertyChanged.HandleValueChange(this, value, ref _userGroupId, (x) => x.UserGroupId); } }
        public ObservableCollection<ModuleTypeViewModel> Modules { get; set; }
        public List<GroupViewModel> UserGroups { get; set; }
        public List<UserViewModel> Users { get; set; }
        public int ModuleId { get { return _moduleId; } set { PropertyChanged.HandleValueChange(this, value, ref _moduleId, (x) => x.ModuleId); } }
        public bool IsActive { get; set; }
        private string _userName;
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userName, (x) => x.UserName);
            }
        }

        private string _userGroupName;
        public string UserGroupName
        {
            get
            {
                return _userGroupName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userGroupName, (x) => x.UserGroupName);
            }
        }

        private string _moduleName;
        public string ModuleName
        {
            get
            {
                return _moduleName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleName, (x) => x.ModuleName);
            }
        }

        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }
        private ICommand _load;

        public ICommand Load
        {
            get
            {

                if (_load == null)
                    _load = new DelegateCommand(() =>
                    {
                        if (OnLoadClick.IsNotNull())
                            OnLoadClick();
                    });
                return _load;
            }

        }
        private ICommand _delete;

        public ICommand Delete
        {
            get
            {

                if (_delete == null)
                    _delete = new DelegateCommand<long>((s) =>
                    {
                        if (OnDeleteClick.IsNotNull())
                            OnDeleteClick(s);
                    });
                return _delete;
            }

        }
        public Action OnSubmitClick;
        public Action<long> OnDeleteClick;
        public Action OnLoadClick;
        private List<UserInGroupViewModel> _userInGroupViewModel;

        public List<UserInGroupViewModel> UserInGroupViewModels
        {
            get { return _userInGroupViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _userInGroupViewModel, (x) => x.UserInGroupViewModels); }
        }


        public string BdrBrushColor { get; set; }
        public string LblForegroundColor { get; set; }
        public string WindowBackGroundColor { get; set; }
        public string CheckBoxFillColor { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
