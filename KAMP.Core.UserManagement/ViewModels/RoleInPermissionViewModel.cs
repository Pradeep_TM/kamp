﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    using System.Windows.Input;
    public class RoleInPermissionViewModel : INotifyPropertyChanged
    {
        private long _rolePermMapId;
        public long RolePermMapId
        {
            get
            {
                return _rolePermMapId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _rolePermMapId, (x) => x.RolePermMapId);
            }
        }

        private long _roleId;
        private long _permissionId;
        private List<ModuleTypeViewModel> _module;
        private List<RoleViewModel> _userRole;
        private ObservableCollection<PermissionSetViewModel> _permission;
        private int _moduleId;
        public long RoleId { get { return _roleId; } set { PropertyChanged.HandleValueChange(this, value, ref _roleId, (x) => x.RoleId); } }
        public long PermissionSetId { get { return _permissionId; } set { PropertyChanged.HandleValueChange(this, value, ref _permissionId, (x) => x.PermissionSetId); } }
        public List<ModuleTypeViewModel> Module { get; set; }
        public List<RoleViewModel> UserRoles { get; set; }
        public List<PermissionSetViewModel> PermissionSets { get; set; }
        public int ModuleId { get { return _moduleId; } set { PropertyChanged.HandleValueChange(this, value, ref _moduleId, (x) => x.ModuleId); } }

        private string _roleName;
        public string RoleName
        {
            get
            {
                return _roleName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _roleName, (x) => x.RoleName);
            }
        }
        private string _moduleObjectName;
        public string ModuleObjectName
        {
            get
            {
                return _moduleObjectName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleObjectName, (x) => x.ModuleObjectName);
            }
        }
        private string _permissionName;
        public string PermissionName
        {
            get
            {
                return _permissionName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _permissionName, (x) => x.PermissionName);
            }
        }

        private string _moduleName;
        public string ModuleName
        {
            get
            {
                return _moduleName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleName, (x) => x.ModuleName);
            }
        }
        public bool IsActive { get; set; }
        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }

        private ICommand _load;

        public ICommand Load
        {
            get
            {

                if (_load == null)
                    _load = new DelegateCommand(() =>
                    {
                        if (OnLoadClick.IsNotNull())
                            OnLoadClick();
                    });
                return _load;
            }

        }
        private ICommand _delete;

        public ICommand Delete
        {
            get
            {

                if (_delete == null)
                    _delete = new DelegateCommand<long>((s) =>
                    {
                        if (OnDeleteClick.IsNotNull())
                            OnDeleteClick(s);
                    });
                return _delete;
            }

        }

        private List<RoleInPermissionViewModel> _roleInPermission;

        public List<RoleInPermissionViewModel> RoleInPermission
        {
            get { return _roleInPermission; }
            set { PropertyChanged.HandleValueChange(this, value, ref _roleInPermission, (x) => x.RoleInPermission); }
        }


        public Action OnSubmitClick;
        public Action OnLoadClick;
        public Action<long> OnDeleteClick;
        public string BdrBrushColor { get; set; }
        public string LblForegroundColor { get; set; }
        public string WindowBackGroundColor { get; set; }
        public string CheckBoxFillColor { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
