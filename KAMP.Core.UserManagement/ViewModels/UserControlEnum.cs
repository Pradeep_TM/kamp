﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    public enum UserControlEnum
    {
        AddUser,
        AddRole,
        AddGroup,
        AddPermission,
        UserInRoleMapping,
        UserInGroupMapping,
        RoleInPermissionMapping
    }
}
