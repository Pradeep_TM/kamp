﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    public class ModuleTypeViewModel : INotifyPropertyChanged
    {
        private int _moduleId;
        public int ModuleId
        {
            get
            {
                return _moduleId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleId, (x) => x.ModuleId);
            }
        }

        private string _moduleName;
        public string ModuleName
        {
            get
            {
                return _moduleName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleName, (x) => x.ModuleName);
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
    }
}
