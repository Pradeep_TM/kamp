﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    public class PermissionInSetViewModel
    {
        public long PermissionInSetId { get; set; }
        public long PermissionId { get; set; }
        public long PermissionSetId { get; set; }
    }
}
