﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using KAMP.Core.FrameworkComponents;
    using System.Windows.Input;
    using Repository.UM;
    public class GroupViewModel : INotifyPropertyChanged
    {
        private long _userGroupId;
        public long UserGroupId
        {
            get
            {
                return _userGroupId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _userGroupId, (x) => x.UserGroupId);
            }
        }

        private string _groupName;
        public string GroupName
        {
            get
            {
                return _groupName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value.Trim(), ref _groupName, (x) => x.GroupName);
            }
        }

        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isActive, (x) => x.IsActive);
            }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdDate, (x) => x.CreatedDate);
            }
        }

        private DateTime _modifiedDate;

        public DateTime ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedDate, (x) => x.ModifiedDate);
            }
        }

        private long _createdBy;

        public long CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _createdBy, (x) => x.CreatedBy);
            }
        }

        private long _modifiedBy;

        public long ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _modifiedBy, (x) => x.ModifiedBy);
            }
        }

        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }
        private ICommand _load;

        public ICommand Load
        {
            get
            {

                if (_load == null)
                    _load = new DelegateCommand(() =>
                    {
                        if (OnLoadClick.IsNotNull())
                            OnLoadClick();
                    });
                return _load;
            }

        }

        private ICommand _delete;

        public ICommand Delete
        {
            get
            {

                if (_delete == null)
                    _delete = new DelegateCommand<long>((s) =>
                    {
                        if (OnDeleteClick.IsNotNull())
                            OnDeleteClick(s);
                    });
                return _delete;
            }

        }

        private List<Group> _groups;

        public List<Group> Groups
        {
            get { return _groups; }
            set { PropertyChanged.HandleValueChange(this, value, ref _groups, (x) => x.Groups); }
        }


        public Action OnSubmitClick;
        public Action OnLoadClick;
        public Action<long> OnDeleteClick;
        public string BdrBrushColor { get; set; }
        public string LblForegroundColor { get; set; }
        public string WindowBackGroundColor { get; set; }
        public string CheckBoxFillColor { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
