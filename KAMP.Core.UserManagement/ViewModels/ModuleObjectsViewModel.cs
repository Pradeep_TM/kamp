﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    public class ModuleObjectsViewModel : INotifyPropertyChanged
    {
        private string _moduleObjectName;

        public string ModuleObjectsName
        {
            get { return _moduleObjectName; }
            set { PropertyChanged.HandleValueChange(this, value, ref _moduleObjectName, (x) => x.ModuleObjectsName); }
        }
        private string _moduleObjectDesc;

        public string ModuleObjectsDesc
        {
            get { return _moduleObjectDesc; }
            set { PropertyChanged.HandleValueChange(this, value, ref _moduleObjectDesc, (x) => x.ModuleObjectsDesc); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
