﻿using KAMP.Core.FrameworkComponents.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    using System.Windows;
    public class UMViewModel : INotifyPropertyChanged
    {
        private NavigationControlViewModel navigationControlViewModel;
        private HeaderViewModel headerViewModel;
        public UMViewModel()
        {
            NavigationControlViewModel = new NavigationControlViewModel();
            PoplulateViewModels();
            headerViewModel = new HeaderViewModel();
        }

        private void PoplulateViewModels()
        {
            navigationControlViewModel = CreateNavigationControlViewModel();
        }

        private NavigationControlViewModel CreateNavigationControlViewModel()
        {
            var items3 = new List<NavigationItemModel>();
            items3.Add(new NavigationItemModel { UserControlName = "Add User", ImageUrl = @"../Assets/Images/create-user1.png" });
            items3.Add(new NavigationItemModel { UserControlName = "Add Role", ImageUrl = @"../Assets/Images/create-role1.png" });
            items3.Add(new NavigationItemModel { UserControlName = "Add Group", ImageUrl = @"../Assets/Images/create-group1.png" });
            items3.Add(new NavigationItemModel { UserControlName = "UserInRole Mapping", ImageUrl = @"../Assets/Images/User-Role Mapping.png" });
            items3.Add(new NavigationItemModel { UserControlName = "UserInGroup Mapping", ImageUrl = @"../Assets/Images/user-group-mapping2.png" });
            items3.Add(new NavigationItemModel { UserControlName = "Add Permission", ImageUrl = @"../Assets/Images/permission1.png" });
            items3.Add(new NavigationItemModel { UserControlName = "RoleInPermission Mapping", ImageUrl = @"../Assets/Images/role-permission-mapping.png" });

            NavigationControlViewModel.PinImage = @"../Assets/Images/pin.png";
            NavigationControlViewModel.UnPinImage = @"../Assets/Images/unpin.png";
            NavigationControlViewModel.ArrowLeftNavigation = @"../Assets/Images/arrowleftnavigation.png";
            NavigationControlViewModel.DarkThemeColor = "#012D84";
            NavigationControlViewModel.ContentBackgroundColor = "#E8EAF6";
            NavigationControlViewModel.LightThemeColor = "#014C9D";
            NavigationControlViewModel.ItemsCollection.Add(new NavigationControlModel { TabName = "User Management", Items = items3 });
            return NavigationControlViewModel;
        }

        public HeaderViewModel HeaderViewModel { get { return headerViewModel; } set { PropertyChanged.HandleValueChange(this, value, ref headerViewModel, x => x.HeaderViewModel);} }
        public NavigationControlViewModel NavigationControlViewModel
        {
            get
            {
                return navigationControlViewModel;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref navigationControlViewModel, x => x.NavigationControlViewModel);
            }
        }

        public UserControlEnum SelectedControl { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
