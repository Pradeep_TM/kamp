﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    public class PermissionViewModel : INotifyPropertyChanged
    {
        public long PermissionId { get; set; }
        public string PermissionName { get; set; }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { PropertyChanged.HandleValueChange(this, value, ref _isSelected, (x) => x.IsSelected); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
