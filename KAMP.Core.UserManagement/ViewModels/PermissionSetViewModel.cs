﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.ViewModels
{
    using FrameworkComponents;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using Repository.UM;
    public class PermissionSetViewModel : INotifyPropertyChanged
    {
        private long _permissionSetId;
        public long PermissionSetId
        {
            get
            {
                return _permissionSetId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _permissionSetId, (x) => x.PermissionSetId);
            }
        }

        private string _permissionSetName;
        public string PermissionSetName
        {
            get
            {
                return _permissionSetName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value.Trim(), ref _permissionSetName, (x) => x.PermissionSetName);
            }
        }

        private int _moduleId;
        public int ModuleId
        {
            get
            {
                return _moduleId;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleId, (x) => x.ModuleId);
            }
        }

        private string _moduleObjectsName;
        public string ModuleObjectsName
        {
            get
            {
                return _moduleObjectsName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleObjectsName, (x) => x.ModuleObjectsName);
            }
        }
        private string _permission;
        public string Permission
        {
            get
            {
                return _permission;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _permission, (x) => x.Permission);
            }
        }

        public List<ModuleTypeViewModel> Modules { get; set; }
        private List<ModuleObjectsViewModel> _moduleObjectsViewModel;
        public List<ModuleObjectsViewModel> ModuleObjectsViewModelsLst
        {
            get { return _moduleObjectsViewModel; }
            set { PropertyChanged.HandleValueChange(this, value, ref _moduleObjectsViewModel, (x) => x.ModuleObjectsViewModelsLst); }
        }

        // public List<ModuleObjectsViewModel> ModuleObjectsViewModelsLst { get; set; }
        public ObservableCollection<PermissionViewModel> Permissions { get; set; }

        public bool IsActive { get; set; }

        private string _moduleName;
        public string ModuleNames
        {
            get
            {
                return _moduleName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _moduleName, (x) => x.ModuleNames);
            }
        }

        private string _permissionName;
        public string PermissionName
        {
            get
            {
                return _permissionName;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _permissionName, (x) => x.PermissionName);
            }
        }

        private bool _isVisible;
        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                PropertyChanged.HandleValueChange(this, value, ref _isVisible, (x) => x.IsVisible);
            }
        }


        //private string _selectedObjectName;
        //public string SelectedObjectName
        //{
        //    get
        //    {
        //        return _selectedObjectName;
        //    }
        //    set
        //    {
        //        PropertyChanged.HandleValueChange(this, value, ref _selectedObjectName, (x) => x.SelectedObjectName);
        //    }
        //}

        private ICommand _submit;

        public ICommand Submit
        {
            get
            {

                if (_submit == null)
                    _submit = new DelegateCommand(() =>
                    {
                        if (OnSubmitClick.IsNotNull())
                            OnSubmitClick();
                    });
                return _submit;
            }

        }
        private ICommand _load;

        public ICommand Load
        {
            get
            {

                if (_load == null)
                    _load = new DelegateCommand(() =>
                    {
                        if (OnLoadClick.IsNotNull())
                            OnLoadClick();
                    });
                return _load;
            }

        }

        private ICommand _edit;
        public ICommand Edit
        {
            get
            {
                if (_edit == null)
                    _edit = new DelegateCommand<long>((s) =>
                    {
                        if (OnEditClick.IsNotNull())
                            OnEditClick(s);
                    });
                return _edit;
            }

        }
        private ICommand _delete;
        public ICommand Delete
        {
            get
            {
                if (_delete == null)
                    _delete = new DelegateCommand<long>((s) =>
                    {
                        if (OnDeleteClick.IsNotNull())
                            OnDeleteClick(s);
                    });
                return _delete;
            }

        }
        //public List<PermissionSetListViewModel> PermissionSetListViewModel { get; set; }

        public Action OnSubmitClick;
        public Action OnLoadClick;
        public Action<long> OnEditClick;
        public Action<long> OnDeleteClick;

        public string BdrBrushColor { get; set; }
        public string LblForegroundColor { get; set; }
        public string WindowBackGroundColor { get; set; }
        public string CheckBoxFillColor { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private List<PermissionSetViewModel> _permissionSet;

        public List<PermissionSetViewModel> PermissionSetLst
        {
            get { return _permissionSet; }
            set { PropertyChanged.HandleValueChange(this, value, ref _permissionSet, (x) => x.PermissionSetLst); }
        }


    }

    //public class PermissionSetListViewModel
    //{

    //}
}
