﻿using KAMP.Core.UserManagement.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.UserManagement.ViewModels;
using KAMP.Core.UserManagement.AppCode;
using System.Threading;
using AutoMapper;
using KAMP.Core.Repository.UM;
using KAMP.Core.UserManagement.AppCode.Automapper;
using KAMP.Core.UM.UserControls;

namespace KAMP.Core.UserManagement.UserControls
{
    /// <summary>
    /// Interaction logic for AddRole.xaml
    /// </summary>
    using Core.FrameworkComponents;
    using KAMP.Core.Common.Models;
    public partial class AddRole : UserControl, KAMP.Core.UserManagement.AppCode.IPageControl
    {
        private RoleViewModel _roleViewModel = null;
        KAMPPrincipal currentPrincipal = ((KAMPPrincipal)Thread.CurrentPrincipal);
        public AddRole()
        {
            InitializeComponent();
            Loaded += Load;
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }
        private async void LoadDataGrid()
        {
            using (var userBl = new UserManagementBL())
            {
                var roles = await Task<List<Role>>.Factory.StartNew(() => userBl.GetAllRole().OrderBy(x => x.RoleName).ToList());

                if (roles.IsCollectionValid())
                {
                    _roleViewModel.Roles = roles;
                }
            }

        }

        private void OnDeleteClick(long param)
        {
            using (var userBl = new UserManagementBL())
            {
                var role = userBl.GetAllRole().FirstOrDefault(x => x.RoleId == param).RoleName;
                var res = MessageBoxControl.Show("Are you sure you want to change the status of the Role '{0}'? ".ToFormat(role), MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    var id = param;

                    userBl.DeleteRole(id);

                    LoadDataGrid();
                }
                else
                {
                    LoadDataGrid();
                }
            }
        }

        private void OnSubmitClick()
        {
            Bootstrapper.Configure();
            _roleViewModel.CreatedDate = _roleViewModel.ModifiedDate = DateTime.Now;
            _roleViewModel.CreatedBy = _roleViewModel.ModifiedBy = currentPrincipal.UserId;
            var mappedRoleModel = Mapper.Map<RoleViewModel, Role>(_roleViewModel);
            var reslt = Validate();
            if (reslt != null)
            {
                MessageBoxControl.Show(reslt, MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                using (var userBl = new UserManagementBL())
                {
                    var roleFromDb = userBl.GetAllRole().FirstOrDefault(x => x.RoleName == _roleViewModel.RoleName);
                    if (roleFromDb == null)
                    {
                        var role = userBl.InsertRole(mappedRoleModel);
                        LoadDataGrid();
                        OpenMessageBox("Successfully created the new role '{0}'".ToFormat(role.RoleName));

                        Clear();

                    }
                    else
                    {
                        MessageBoxControl.Show("Role Name already exists", MessageBoxButton.OK, MessageType.Error);
                    }
                }
            }
            LoadDataGrid();
        }

        private string Validate()
        {
            StringBuilder errorMessage = new StringBuilder();
            if (!_roleViewModel.RoleName.IsNotEmpty())
            {
                errorMessage = errorMessage.AppendLine("Role Name is mandatory.");
            }
            //if (!_roleViewModel.IsActive)
            //    errorMessage = errorMessage.AppendLine("The Role will be Inactive.");
            string errors = errorMessage.ToString();
            if (errors != "")
                return errors;
            return null;
        }


        private void Clear()
        {

            _roleViewModel.RoleName = string.Empty;
            _roleViewModel.IsActive = false;
        }

        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, MessageType.Information);
        }

        public void InitializeData()
        {
            this.DataContext = new RoleViewModel()
            {
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor
            };
            _roleViewModel = this.DataContext as RoleViewModel;

            LoadDataGrid();
            _roleViewModel.OnSubmitClick += OnSubmitClick;
            _roleViewModel.OnDeleteClick += OnDeleteClick;
            _roleViewModel.OnLoadClick += OnLoadClick;
        }

        private void OnLoadClick()
        {
            LoadDataGrid();
        }
    }
}
