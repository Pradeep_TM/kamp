﻿using AutoMapper;
using KAMP.Core.Repository.UM;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.BLL;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.UserManagement.UserControls
{
    using FrameworkComponents;
    using KAMP.Core.Common.Models;
    using KAMP.Core.UM.UserControls;
    using KAMP.Core.UserManagement.AppCode.Automapper;
    /// <summary>
    /// Interaction logic for RoleInPermissionMapping.xaml
    /// </summary>
    public partial class RoleInPermissionMapping : UserControl, KAMP.Core.UserManagement.AppCode.IPageControl
    {
        private RoleInPermissionViewModel _roleInPermissionViewModel = null;
        KAMPPrincipal currentPrincipal = ((KAMPPrincipal)Thread.CurrentPrincipal);
        public RoleInPermissionMapping()
        {
            InitializeComponent();
            Loaded += Load;
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        private void OnDeleteClick(long param)
        {
            using (var userBl = new UserManagementBL())
            {
                var roleInPermission = userBl.GetRoleInPermission().FirstOrDefault(x => x.RoleInPermissionId == param);

                var res = MessageBoxControl.Show("Are you sure you want to change the status of Role: '{0}' & Permission: '{1}' ? ".ToFormat(GetRoleName(roleInPermission.RoleId), GetPermissionSetName(roleInPermission.PermissionSetId)), MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    //var button = sender as Button;
                    var id = param;

                    userBl.DeleteRoleInPermission(id);

                }
                LoadDataGrid();
            }
        }

        private string GetRoleName(long roleId)
        {
            using (var userBl = new UserManagementBL())
            {
                var roleName = userBl.GetAllRole().FirstOrDefault(x => x.RoleId == roleId);
                if (roleName != null)
                    return roleName.RoleName;
                return null;
            }
        }

        private string GetPermissionSetName(long permissionSetId)
        {
            using (var userBl = new UserManagementBL())
            {
                var permname = userBl.GetPermissionSet().FirstOrDefault(x => x.PermissionSetId == permissionSetId);
                if (permname != null)
                    return permname.PermissionSetName;
                return null;
            }
        }
        private void OnSubmitClick()
        {
            Bootstrapper.Configure();
            var viewModel = this.DataContext as RoleInPermissionViewModel;
            var errorMessage = new StringBuilder();

            if (viewModel.PermissionSetId == 0)
                errorMessage.AppendLine("Please select a permission set.");
            if (viewModel.RoleId == 0)
                errorMessage.AppendLine("Please select a role.");
            if (viewModel.ModuleId == 0)
                errorMessage.AppendLine("Please select a module.");
            if (errorMessage.ToString() != string.Empty)
            {
                MessageBoxControl.Show(errorMessage.ToString(), MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                GetName(viewModel);
                using (var userBl = new UserManagementBL())
                {
                    var rolePermMap = userBl.GetRoleInPermission().FirstOrDefault(x => x.RoleId == viewModel.RoleId && x.PermissionSetId == viewModel.PermissionSetId);
                    if (rolePermMap == null)
                    {
                        var role = userBl.GetRoleInPermission().Where(x => x.RoleId == viewModel.RoleId).ToList();
                        if (role.IsCollectionValid())
                        {
                            foreach (var item in role)
                            {
                                if (item.PermissionSet.IsNotNull())
                                {
                                    if (item.PermissionSet.ModuleObjectName == viewModel.ModuleObjectName)
                                    {
                                        MessageBoxControl.Show("There already exists a permission set which has the same module object for this role.", MessageBoxButton.OK, MessageType.Error);
                                        return;
                                    }
                                }
                            }
                        }
                        var permissionSet = userBl.GetPermissionSet().Where(x => x.ModuleObjectName == viewModel.ModuleObjectName
                            && x.ModuleId == viewModel.ModuleId && x.IsActive).ToList();
                        long roleId = 0;

                        foreach (var item in permissionSet)
                        {
                            if (item.RoleInPermission.IsCollectionValid())
                            {
                                foreach (var res in item.RoleInPermission)
                                {
                                    roleId = res.RoleId;
                                    if (roleId == viewModel.RoleId && res.IsActive.Value == true)
                                    {
                                        MessageBoxControl.Show("There already exists a permission set which has the same module object for this role.", MessageBoxButton.OK, MessageType.Error);
                                        return;
                                    }
                                }
                            }
                        }

                        var mappedModel = Mapper.Map<RoleInPermissionViewModel, RoleInPermission>(viewModel);
                        mappedModel.IsActive = true;
                        userBl.InsertRoleInPermission(mappedModel);
                        if (mappedModel.RoleInPermissionId != 0)
                        {
                            OpenMessageBox("Successfully created the new role-permission Mapping '{0}-{1}'".ToFormat(viewModel.PermissionName, viewModel.RoleName));
                            LoadDataGrid();
                            Clear();
                        }
                        else
                        {
                            MessageBoxControl.Show("There was an error while saving the record.Please re-enter the details.", MessageBoxButton.OK, MessageType.Error);
                        }
                    }

                    else
                    {
                        MessageBoxControl.Show(string.Format("The entry for permission set: {0},Role Name: {1}  exists.", viewModel.PermissionName, viewModel.RoleName), MessageBoxButton.OK, MessageType.Error);
                    }
                }

            }
        }

        private void Clear()
        {
            cmbUsrRole.SelectedIndex = 0;
            cmbModule.SelectedIndex = 0;
            lstPerm.SelectedIndex = 0;
        }

        private List<PermissionSetViewModel> AssignPermissionSet(int moduleId)
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var permissionSet = userBl.GetPermissionSet().Where(x => x.ModuleId == moduleId && x.IsActive).ToList();
                var permissionSetInitial = new PermissionSetViewModel()
                {
                    PermissionSetId = 0,
                    PermissionSetName = "-Select-"
                };
                //permissionSet.Add(permissionSetInitial);
                var mappedPermissionSet = Mapper.Map<List<PermissionSet>, List<PermissionSetViewModel>>(permissionSet);
                mappedPermissionSet.Insert(0, permissionSetInitial);
                return mappedPermissionSet;
            }
        }

        private async void LoadDataGrid()
        {
            using (var userBl = new UserManagementBL())
            {
                var roleInPermission = await Task<List<RoleInPermission>>.Factory.StartNew(() => userBl.GetRoleInPermission().OrderBy(x => x.RoleInPermissionId).ToList());
                Bootstrapper.Configure();

                if (roleInPermission.IsCollectionValid())
                {
                    var mappedRoleInPermission = Mapper.Map<List<RoleInPermission>, List<RoleInPermissionViewModel>>(roleInPermission);

                    _roleInPermissionViewModel.RoleInPermission = mappedRoleInPermission.OrderBy(x => x.PermissionName).ToList();
                }
            }
        }


        private List<RoleViewModel> AssignRole()
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var roles = userBl.GetAllRole();
                var role = new RoleViewModel()
                {
                    RoleId = 0,
                    RoleName = "-Select-"
                };

                var rolesLst = Mapper.Map<List<Role>, List<RoleViewModel>>(roles);
                var rols = rolesLst.OrderBy(x => x.RoleName).ToList();
                rols.Insert(0, role);
                return rols;
            }
        }


        private void GetName(RoleInPermissionViewModel context)
        {
            using (var userBl = new UserManagementBL())
            {
                if (context.ModuleId != 0)
                {
                    var module = userBl.GetAllModule().FirstOrDefault(x => x.ModuleTypeId == context.ModuleId);
                    if (module != null)
                        context.ModuleName = module.ModuleName;
                }

                if (context.PermissionSetId != 0)
                {
                    var permission = userBl.GetPermissionSet().FirstOrDefault(x => x.PermissionSetId == context.PermissionSetId);
                    context.PermissionName = permission.PermissionSetName;
                    context.ModuleObjectName = permission.ModuleObjectName;

                }

                if (context.RoleId != 0)
                {
                    var role = userBl.GetAllRole().FirstOrDefault(x => x.RoleId == context.RoleId);
                    context.RoleName = role.RoleName;
                }
            }
        }


        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, MessageType.Information);
        }

        public void InitializeData()
        {
            this.DataContext = new RoleInPermissionViewModel()
            {
                UserRoles = AssignRole(),
                PermissionSets = new List<PermissionSetViewModel>(),
                Module = KAMP.Core.UserManagement.AppCode.Common.AssignModuleDropDown().ToList(),
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor
            };
            _roleInPermissionViewModel = DataContext as RoleInPermissionViewModel;
            _roleInPermissionViewModel.OnSubmitClick += OnSubmitClick;
            _roleInPermissionViewModel.OnDeleteClick += OnDeleteClick;
            _roleInPermissionViewModel.OnLoadClick += OnLoadClick;
            LoadDataGrid();
        }

        private void OnLoadClick()
        {
            LoadDataGrid();
        }

        private void cmbModule_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var moduleId = _roleInPermissionViewModel.ModuleId;

            lstPerm.ItemsSource = AssignPermissionSet(moduleId);
            lstPerm.SelectedIndex = 0;
        }
    }
}
