﻿using AutoMapper;
using KAMP.Core.Repository.UM;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.BLL;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.UserManagement.UserControls
{
    using FrameworkComponents;
    using KAMP.Core.Common.Models;
    using KAMP.Core.UM.UserControls;
    using KAMP.Core.UserManagement.AppCode.Automapper;
    /// <summary>
    /// Interaction logic for UserInGroupMapping.xaml
    /// </summary>
    public partial class UserInGroupMapping : UserControl, KAMP.Core.UserManagement.AppCode.IPageControl
    {
        private UserInGroupViewModel _userInGroupViewModel = null;
        KAMPPrincipal currentPrincipal = ((KAMPPrincipal)Thread.CurrentPrincipal);
        public UserInGroupMapping()
        {
            InitializeComponent();
            Loaded += Load;

        }

        private void Load(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        private void OnDeleteClick(long param)
        {
            using (var userBl = new UserManagementBL())
            {
                var userInGrp = userBl.GetUserInGroup().FirstOrDefault(x => x.UserInGroupId == param);
                var res = MessageBoxControl.Show("Are you sure you want to change the status of User: {0} and Group: {1} ? ".ToFormat(GetUserName(userInGrp.UserId), GetUserGroupName(userInGrp.GroupId)), MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    userBl.DeleteUserinGroup(param);
                }
                LoadDataGrid();
            }
        }

        private string GetUserName(long userId)
        {
            using (var userBl = new UserManagementBL())
            {
                var userName = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == userId);
                if (userName != null)
                    return userName.UserName;
                return null;
            }
        }

        private string GetUserGroupName(long groupId)
        {
            using (var userBl = new UserManagementBL())
            {
                var groupName = userBl.GetAllGroup().FirstOrDefault(x => x.GroupId == groupId).GroupName;
                return groupName;
            }
        }
        private void OnSubmitClick()
        {
            Bootstrapper.Configure();
            var context = this.DataContext as UserInGroupViewModel;
            GetName(context);
            var mappedUser = Mapper.Map<UserInGroupViewModel, UserInGroup>(context);
            var errorMessage = new StringBuilder();
            if (mappedUser.UserId == 0)
                errorMessage.AppendLine("Please select a user.");
            if (mappedUser.GroupId == 0)
                errorMessage.AppendLine("Please select a group.");
            if (mappedUser.ModuleTypeId == 0)
                errorMessage.AppendLine("Please Select a Module.");
            if (errorMessage.ToString() != string.Empty)
            {
                MessageBoxControl.Show(errorMessage.ToString(), MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                var isExist = CheckMappingExist(mappedUser);
                if (!isExist)
                    using (var userBl = new UserManagementBL())
                    {
                        var mappedUserInGrp = Mapper.Map<UserInGroupViewModel, UserInGroup>(context);
                        mappedUserInGrp.IsActive = true;
                        userBl.InsertUserInGroup(mappedUserInGrp);
                        LoadDataGrid();
                        OpenMessageBox("Successfully created the new user-group Mapping '{0}-{1}'".ToFormat(_userInGroupViewModel.UserGroupName, _userInGroupViewModel.UserName));
                        ClearFields();
                    }
                else
                {
                    MessageBoxControl.Show(string.Format("The entry for User Name: {0} , Module: {1} ,User Group: {2}  exists.", _userInGroupViewModel.UserName, _userInGroupViewModel.ModuleName, _userInGroupViewModel.UserGroupName), MessageBoxButton.OK, MessageType.Error);
                }
            }
        }

        private void ClearFields()
        {
            cmbModule.SelectedIndex = 0;
            cmbUsrRole.SelectedIndex = 0;
            cmbUser.SelectedIndex = 0;
        }

        private async void LoadDataGrid()
        {
            //var userInGroups = await GetUserInGroup();
            using (var userBl = new UserManagementBL())
            {
                var userInGroups = await Task<List<UserInGroup>>.Factory.StartNew(() => userBl.GetUserInGroup().OrderBy(x => x.UserInGroupId).ToList());
                Bootstrapper.Configure();

                if (userInGroups.IsCollectionValid())
                {
                    var mappedUser = Mapper.Map<List<UserInGroup>, List<UserInGroupViewModel>>(userInGroups);
                    _userInGroupViewModel.UserInGroupViewModels = mappedUser;
                }
            }


        }

        private List<GroupViewModel> AssignUserGroup()
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var userGrps = userBl.GetAllGroup().ToList();
                var mappedGroup = Mapper.Map<List<Group>, List<GroupViewModel>>(userGrps);
                mappedGroup.Insert(0, new GroupViewModel { GroupName = "-Select-" });
                //var userGrp = new GroupViewModel()
                //{
                //    UserGroupId = 0,
                //    GroupName = "-Select-"
                //};
                //mappedGroup.Add(userGrp);
                return mappedGroup;
            }
        }

        private List<UserViewModel> AssignUsers()
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var users = userBl.GetAllUsers().ToList();
                var mappedGroup = Mapper.Map<List<User>, List<UserViewModel>>(users);
                mappedGroup.Insert(0, new UserViewModel { UserName = "-Select-" });
                //var user = new UserViewModel()
                //{
                //    UserId = 0,
                //    UserName = "-Select-"
                //};
                //mappedGroup.Add(user);
                return mappedGroup;
            }
        }



        private bool CheckMappingExist(UserInGroup mappedUser)
        {
            using (var userBl = new UserManagementBL())
            {
                var userGrpMap = userBl.GetUserInGroup().FirstOrDefault(x => x.UserId == mappedUser.UserId && x.ModuleTypeId == mappedUser.ModuleTypeId && x.GroupId == mappedUser.GroupId);
                if (userGrpMap != null)
                    return true;
                return false;
            }
        }

        private void GetName(UserInGroupViewModel context)
        {
            using (var userBl = new UserManagementBL())
            {
                if (context.UserId != 0)
                {
                    var user = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == context.UserId);
                    context.UserName = user.UserName;
                }


                if (context.UserGroupId != 0)
                {
                    var userGroup = userBl.GetAllGroup().FirstOrDefault(x => x.GroupId == context.UserGroupId);
                    context.UserGroupName = userGroup.GroupName;
                }


                if (context.ModuleId != 0)
                {
                    var module = userBl.GetAllModule().FirstOrDefault(x => x.ModuleTypeId == context.ModuleId);
                    context.ModuleName = module.ModuleName;
                }

            }

        }
        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, MessageType.Information);
        }


        public void InitializeData()
        {
            this.DataContext = new UserInGroupViewModel()
            {
                Modules = KAMP.Core.UserManagement.AppCode.Common.AssignModuleDropDown(),
                Users = AssignUsers(),
                UserGroups = AssignUserGroup(),
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor
            };
            _userInGroupViewModel = DataContext as UserInGroupViewModel;
            _userInGroupViewModel.OnSubmitClick += OnSubmitClick;
            _userInGroupViewModel.OnDeleteClick += OnDeleteClick;
            _userInGroupViewModel.OnLoadClick += OnLoadClick;
            LoadDataGrid();
        }

        private void OnLoadClick()
        {
            LoadDataGrid();
        }
    }
}
