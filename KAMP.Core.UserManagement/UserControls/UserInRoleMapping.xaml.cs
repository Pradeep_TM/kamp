﻿using AutoMapper;
using KAMP.Core.Repository.UM;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.BLL;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.UserManagement.UserControls
{
    using FrameworkComponents;
    using KAMP.Core.Common.Models;
    using KAMP.Core.UM.UserControls;
    using KAMP.Core.UserManagement.AppCode.Automapper;
    /// <summary>
    /// Interaction logic for UserInRoleMapping.xaml
    /// </summary>
    public partial class UserInRoleMapping : UserControl, KAMP.Core.UserManagement.AppCode.IPageControl
    {
        private UserInRoleViewModel _userInRoleViewModel = null;
        KAMPPrincipal currentPrincipal = ((KAMPPrincipal)Thread.CurrentPrincipal);
        public UserInRoleMapping()
        {
            InitializeComponent();
            Loaded += Load;

        }

        private void Load(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        private void OnDeleteClick(long param)
        {
            using (var userBl = new UserManagementBL())
            {
                var userInRole = userBl.GetUserInRole().FirstOrDefault(x => x.UserInRoleId == param);
                var res = MessageBoxControl.Show("Are you sure you want to change the status of User: {0} and Role: {1}? ".ToFormat(GetUserName(userInRole.UserId), GetRoleName(userInRole.RoleId)), MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    userBl.DeleteUserInRole(param);
                }
                LoadDataGrid();
            }
        }

        private string GetUserName(long userId)
        {
            using (var userBl = new UserManagementBL())
            {
                var userName = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == userId);
                if (userName != null)
                    return userName.UserName;
                return null;
            }
        }

        private string GetRoleName(long roleId)
        {
            using (var userBl = new UserManagementBL())
            {
                var roleName = userBl.GetAllRole().FirstOrDefault(x => x.RoleId == roleId);
                if (roleName != null)
                    return roleName.RoleName;
                return null;
            }
        }

        private void OnSubmitClick()
        {
            Bootstrapper.Configure();
            var context = this.DataContext as UserInRoleViewModel;
            GetName(context);
            var mappedUser = Mapper.Map<UserInRoleViewModel, UserInRole>(context);
            mappedUser.IsActive = true;
            var errorMessage = new StringBuilder();
            if (mappedUser.UserId == 0)
                errorMessage.AppendLine("Please select a user.");
            if (mappedUser.RoleId == 0)
                errorMessage.AppendLine("Please select a role.");

            if (errorMessage.ToString() != string.Empty)
            {
                MessageBoxControl.Show(errorMessage.ToString(), MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                var isExist = CheckMappingExist(mappedUser);
                if (!isExist)
                {
                    using (var userBl = new UserManagementBL())
                    {
                        userBl.InsertUserInRole(mappedUser);
                        LoadDataGrid();
                        OpenMessageBox("Successfully created the new user-role Mapping '{0}-{1}'".ToFormat(context.UserName, context.RoleName));
                        Clear();
                    }

                }
                else
                {
                    MessageBoxControl.Show(string.Format("The User Name: {0},Role Name: {1} already exists.", context.UserName, context.RoleName), MessageBoxButton.OK, MessageType.Error);
                }
            }
        }

        private void Clear()
        {
            cmbUser.SelectedIndex = 0;
            cmbUsrRole.SelectedIndex = 0;
        }

        private List<RoleViewModel> AssignRole()
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var roles = userBl.GetAllRole().ToList();
                var mappedRoleModel = Mapper.Map<List<Role>, List<RoleViewModel>>(roles);
                var role = new RoleViewModel()
                {
                    RoleId = 0,
                    RoleName = "-Select-"
                };
                mappedRoleModel.Insert(0, role);
                return mappedRoleModel;
            }
        }

        private List<UserViewModel> AssignUser()
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var users = userBl.GetAllUsers().ToList();
                var mappedUserModel = Mapper.Map<List<User>, List<UserViewModel>>(users);
                var user = new UserViewModel()
                {
                    UserId = 0,
                    UserName = "-Select-"
                };
                mappedUserModel.Insert(0, user);
                return mappedUserModel;
            }
        }
        private async void LoadDataGrid()
        {
            using (var userBl = new UserManagementBL())
            {
                var userInRoles = await Task<List<UserInRole>>.Factory.StartNew(() => userBl.GetUserInRole().OrderBy(x => x.UserInRoleId).ToList());
                Bootstrapper.Configure();
                if (userInRoles.IsCollectionValid())
                {
                    var mappedUser = Mapper.Map<List<UserInRole>, List<UserInRoleViewModel>>(userInRoles);

                    _userInRoleViewModel.UserInRoleViewModels = mappedUser;
                }
            }
        }

        private bool CheckMappingExist(UserInRole mappedUser)
        {
            using (var userBl = new UserManagementBL())
            {
                var userInRoleMap = userBl.GetUserInRole().FirstOrDefault(x => x.UserId == mappedUser.UserId && x.RoleId == mappedUser.RoleId);
                if (userInRoleMap != null)
                    return true;
                return false;
            }
        }

        private void GetName(UserInRoleViewModel context)
        {
            using (var userBl = new UserManagementBL())
            {
                if (context.UserId != 0)
                {
                    var user = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == context.UserId);
                    context.UserName = user.UserName;
                }

                if (context.RoleId != 0)
                {
                    var role = userBl.GetAllRole().FirstOrDefault(x => x.RoleId == context.RoleId);
                    context.RoleName = role.RoleName;
                }
            }
        }

        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, MessageType.Information);
        }


        public void InitializeData()
        {
            this.DataContext = new UserInRoleViewModel()
            {
                Users = AssignUser(),
                UserRoles = AssignRole(),
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor
            };
            _userInRoleViewModel = DataContext as UserInRoleViewModel;
            _userInRoleViewModel.OnSubmitClick += OnSubmitClick;
            _userInRoleViewModel.OnDeleteClick += OnDeleteClick;
            _userInRoleViewModel.OnLoadClick += OnLoadClick;
            LoadDataGrid();
        }

        private void OnLoadClick()
        {
            LoadDataGrid();
        }
    }
}
