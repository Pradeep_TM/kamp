﻿using AutoMapper;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.UnitOfWork;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.BLL;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.AppCode.Automapper;

namespace KAMP.Core.UserManagement.UserControls
{
    using FrameworkComponents;
    using KAMP.Core.UM.UserControls;
    using KAMP.Core.Common.Models;
    using System.Text.RegularExpressions;
    /// <summary>
    /// Interaction logic for AddUser.xaml
    /// </summary>
    public partial class AddUser : UserControl, KAMP.Core.UserManagement.AppCode.IPageControl
    {
        private UserViewModel _userViewModel = null;
        KAMPPrincipal currentPrincipal = ((KAMPPrincipal)Thread.CurrentPrincipal);
        Context context = Application.Current.Properties["Context"] as Context;
        public AddUser()
        {
            InitializeComponent();
            Loaded += Load;
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        public async void LoadDataGrid()
        {
            using (var userBl = new UserManagementBL())
            {
                _userViewModel.Users = await Task<List<User>>.Factory.StartNew(() => userBl.GetAllUsers().OrderBy(x => x.UserName).ToList());
            }

        }


        private void OnDeleteClick(long parm)
        {
            var del = currentPrincipal.ValidatePermissions("User", "CanDelete");

            if (del)
            {
                using (var userBl = new UserManagementBL())
                {
                    if (currentPrincipal.UserId == parm)
                        MessageBoxControl.Show("You are not allowed to delete yourself.", MessageBoxButton.OK, MessageType.Information);

                    var user = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == parm);
                    var userName = user.UserName;
                    var isSuperAdmin = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == currentPrincipal.UserId).IsSuperAdmin;
                    if (isSuperAdmin.HasValue && isSuperAdmin.Value == true)
                    {
                        var res = MessageBoxControl.Show("Are you sure you want to change the status of the User '{0}'? ".ToFormat(userName), MessageBoxButton.YesNo, MessageType.Alert);
                        if (res == MessageBoxResult.Yes)
                        {

                            userBl.DeleteUser(parm);

                            LoadDataGrid();
                        }
                        else
                        {
                            LoadDataGrid();
                        }
                    }
                    else
                    {
                        OpenMessageBox("Only a Super Admin can delete this user.");
                    }

                }
            }


        }

        private void OnSubmitClick()
        {
            Bootstrapper.Configure();

            var rest = Validate();
            if (rest == null)
            {
                if (_userViewModel.UserId != 0)
                {
                    _userViewModel.ModifiedBy = currentPrincipal.UserId;
                    _userViewModel.ModifiedDate = DateTime.Now;
                    var mappedUserModel = Mapper.Map<UserViewModel, User>(_userViewModel);
                    using (var userBl = new UserManagementBL())
                    {
                        userBl.UpdateUser(mappedUserModel);
                        OpenMessageBox("Successfully Updated");
                    }
                }
                else
                {
                    _userViewModel.CreatedDate = _userViewModel.ModifiedDate = DateTime.Now;
                    _userViewModel.CreatedBy = _userViewModel.ModifiedBy = currentPrincipal.UserId;
                    var mappedUserModel = Mapper.Map<UserViewModel, User>(_userViewModel);

                    var user = new User();
                    using (var userBl = new UserManagementBL())
                    {
                        var userFromDb = userBl.GetAllUsers().FirstOrDefault(x => x.UserName == _userViewModel.UserName.Trim() && x.IsActive);
                        if (userFromDb == null)
                        {
                            user = userBl.InsertUser(mappedUserModel);
                            OpenMessageBox("Successfully created the new user '{0}'".ToFormat(user.UserName));
                        }
                        else
                        {
                            MessageBoxControl.Show("User Name already exists.", MessageBoxButton.OK, MessageType.Error);
                        }
                    }
                }

                LoadDataGrid();

                Clear();

            }
            else
            {
                MessageBoxControl.Show(rest, MessageBoxButton.OK, MessageType.Error);
            }




        }

        private string Validate()
        {
            string matchEmailPattern =
           @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
    + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
    + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
    + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

            StringBuilder errorMessage = new StringBuilder();
            if (!_userViewModel.UserName.IsNotEmpty())
            {
                errorMessage = errorMessage.AppendLine("User Name is mandatory.");
            }
            if (!_userViewModel.FirstName.IsNotEmpty())
            {
                errorMessage = errorMessage.AppendLine("First Name is mandatory.");
            }
            if (!_userViewModel.LastName.IsNotEmpty())
            {
                errorMessage = errorMessage.AppendLine("Last Name is mandatory.");
            }
            if (!_userViewModel.Email.IsNotEmpty())
            {
                errorMessage = errorMessage.AppendLine("Email is mandatory.");
            }
            else
            {
                var regexMatch = Regex.IsMatch(_userViewModel.Email, matchEmailPattern);
                if (!regexMatch)
                {
                    errorMessage = errorMessage.AppendLine("Please enter a valid Email.");
                }
            }

            //if (!_userViewModel.IsActive)
            //    errorMessage = errorMessage.AppendLine("The User will be Inactive.");
            string errors = errorMessage.ToString();
            if (errors != "")
                return errors;
            return null;
        }


        private void Clear()
        {
            _userViewModel.UserName = string.Empty;
            _userViewModel.IsActive = false;
            _userViewModel.FirstName = string.Empty;
            _userViewModel.LastName = string.Empty;
            _userViewModel.Email = string.Empty;
            _userViewModel.IsSuperAdmin = false;
            _userViewModel.IsActive = false;
            //txtUsrName.Text = string.Empty;
            //chkActive.IsChecked = false;
        }

        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, MessageType.Information);
        }


        public void InitializeData()
        {
            this.DataContext = new UserViewModel()
            {
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor
            };
            _userViewModel = this.DataContext as UserViewModel;
            LoadDataGrid();
            _userViewModel.OnSubmitClick += OnSubmitClick;
            _userViewModel.OnDeleteClick += OnDeleteClick;
            _userViewModel.OnLoadClick += OnLoadClick;
            _userViewModel.OnEditClick += OnEditClick;
        }

        private void OnEditClick(long userId)
        {
            Bootstrapper.Configure();

            using (var userBl = new UserManagementBL())
            {
                var user = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == userId);
                var mappedUser = Mapper.Map<User, UserViewModel>(user);

                if (mappedUser.IsNotNull())
                {
                    _userViewModel.FirstName = mappedUser.FirstName;
                    _userViewModel.LastName = mappedUser.LastName;
                    _userViewModel.UserName = mappedUser.UserName;
                    _userViewModel.IsSuperAdmin = mappedUser.IsSuperAdmin;
                    _userViewModel.IsActive = mappedUser.IsActive;
                    _userViewModel.Email = mappedUser.Email;
                    _userViewModel.UserId = mappedUser.UserId;
                    LoadDataGrid();
                }
            }
        }

        private void OnLoadClick()
        {
            LoadDataGrid();
        }
    }
}
