﻿using AutoMapper;
using KAMP.Core.Repository;
using KAMP.Core.Repository.UM;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.BLL;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.UserManagement.UserControls
{
    using FrameworkComponents;
    using KAMP.Core.Common.Models;
    using KAMP.Core.Interfaces;
    using KAMP.Core.Interfaces.Models;
    using KAMP.Core.Repository.Enum;
    using KAMP.Core.UM.UserControls;
    using KAMP.Core.UserManagement.AppCode.Automapper;
    using System.Collections;
    /// <summary>
    /// Interaction logic for AddPermission.xaml
    /// </summary>
    public partial class AddPermission : UserControl
    {
        private PermissionSetViewModel _permissionSetViewModel = null;
        KAMPPrincipal currentPrincipal = ((KAMPPrincipal)Thread.CurrentPrincipal);
        public AddPermission()
        {
            InitializeComponent();
            Loaded += Load;
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            this.DataContext = new PermissionSetViewModel()
            {
                Modules = KAMP.Core.UserManagement.AppCode.Common.AssignModuleDropDown().ToList(),
                ModuleObjectsViewModelsLst = AssignDefaultModuleObject(),
                Permissions = AssignPermissions(),
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor,
                ModuleObjectsName = "-Select-"
            };

            _permissionSetViewModel = this.DataContext as PermissionSetViewModel;
            _permissionSetViewModel.OnSubmitClick += OnSubmitClick;
            _permissionSetViewModel.OnEditClick += OnEditClick;
            _permissionSetViewModel.OnDeleteClick += OnDeleteClick;
            _permissionSetViewModel.OnLoadClick += OnLoadClick;
            LoadDataGrid();
        }

        private List<ModuleObjectsViewModel> AssignDefaultModuleObject()
        {
            var moduleObjectViewModel = new List<ModuleObjectsViewModel>();
            var moduleObject = new ModuleObjectsViewModel()
            {
                ModuleObjectsName = "-Select-",
                ModuleObjectsDesc = "-Select-"
            };
            moduleObjectViewModel.Insert(0, moduleObject);
            return moduleObjectViewModel;
        }

        private void OnLoadClick()
        {
            LoadDataGrid();
        }

        private void OnDeleteClick(long param)
        {
            using (var userBl = new UserManagementBL())
            {
                var permissionSet = userBl.GetPermissionById(param);
                var permSetName = string.Empty;
                if (permissionSet.IsNotNull())
                {
                    permSetName = permissionSet.PermissionSetName;
                }
                var res = MessageBoxControl.Show("Are you sure you want to change the status of the Permission '{0}' ".ToFormat(permSetName), MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    userBl.DeletePermissionSet(param);
                    LoadDataGrid();
                }
                else
                {
                    LoadDataGrid();
                }
            }
        }

        private void OnEditClick(long num)
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var permissionSet = userBl.GetPermissionById(num);
                _permissionSetViewModel.ModuleId = permissionSet.ModuleId;
                _permissionSetViewModel.ModuleObjectsName = permissionSet.ModuleObjectName;
                var permissionInSetViewModels = new List<PermissionInSetViewModel>();
                var permissionInSetVM = new PermissionInSetViewModel();
                _permissionSetViewModel.PermissionSetName = permissionSet.PermissionSetName;
                _permissionSetViewModel.PermissionSetId = permissionSet.PermissionSetId;
                var permissionIds = permissionSet.PermissionInSet.Select(x => x.PermissionId).ToList();
                var permissionViewModel = userBl.GetPermission();
                var mappedPermissionViewModel = Mapper.Map<List<Permission>, List<PermissionViewModel>>(permissionViewModel);
                foreach (var permission in mappedPermissionViewModel)
                {
                    var permissionID = permission.PermissionId;
                    if (permissionIds.Contains((permissionID)))
                        permission.IsSelected = true;
                }
                if (_permissionSetViewModel.Permissions.IsCollectionValid())
                {
                    _permissionSetViewModel.Permissions.Clear();
                    _permissionSetViewModel.Permissions.AddRange(mappedPermissionViewModel);
                }
            }

        }

        public void ResetToDefault()
        {
            this.DataContext = new PermissionSetViewModel()
            {
                Modules = KAMP.Core.UserManagement.AppCode.Common.AssignModuleDropDown().ToList(),
                ModuleObjectsViewModelsLst = AssignDefaultModuleObject(),
                Permissions = AssignPermissions(),
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor,
                ModuleObjectsName = "-Select-"
            };

            _permissionSetViewModel = this.DataContext as PermissionSetViewModel;
            _permissionSetViewModel.OnSubmitClick += OnSubmitClick;
            _permissionSetViewModel.OnEditClick += OnEditClick;
            _permissionSetViewModel.OnDeleteClick += OnDeleteClick;
            _permissionSetViewModel.OnLoadClick += OnLoadClick;
            LoadDataGrid();
        }
        private void OnSubmitClick()
        {
            Bootstrapper.Configure();
            var res = Validate();
            if (res == null)
            {
                var selectedModObj = cmbModuleObjects.SelectedValue.ToString();
                _permissionSetViewModel.ModuleObjectsName = selectedModObj;
                var permissionSetModel = new PermissionSet();
                var items = _permissionSetViewModel.Permissions.Where(x => x.IsSelected).ToList();
                List<long> lstPerrmission = new List<long>();

                foreach (var item in items)
                {
                    lstPerrmission.Add(item.PermissionId);
                    //if (item.PermissionId == (int)Enums.PermissionSet.CanAdd)
                    //{
                    //    lstPerrmission.Add((int)Enums.PermissionSet.CanEdit);
                    //    lstPerrmission.Add((int)Enums.PermissionSet.CanDelete);
                    //}
                    //if (item.PermissionId == ((int)Enums.PermissionSet.CanEdit))
                    //{
                    //    lstPerrmission.Add((int)Enums.PermissionSet.CanAdd);
                    //    lstPerrmission.Add((int)Enums.PermissionSet.CanDelete);
                    //}
                    //if (item.PermissionId == (int)Enums.PermissionSet.CanDelete)
                    //{
                    //    lstPerrmission.Add((int)Enums.PermissionSet.CanAdd);
                    //    lstPerrmission.Add((int)Enums.PermissionSet.CanEdit);
                    //}

                }

                _permissionSetViewModel.Permission = string.Join(",", lstPerrmission.ToArray().Distinct());


                using (var userBl = new UserManagementBL())
                {
                    if (_permissionSetViewModel.PermissionSetId != 0)
                    {

                        using (var userbl = new UserManagementBL())
                        {
                            var permissionInSetFromDb = userbl.GetPermissionById(_permissionSetViewModel.PermissionSetId);
                            var permissionInSetId = permissionInSetFromDb.PermissionInSet.Select(x => x.PermissionInSetId).ToList();
                            userbl.DeletePermissionInSet(permissionInSetId);
                            var mappedViewModel = Mapper.Map<PermissionSetViewModel, PermissionSet>(_permissionSetViewModel);
                            mappedViewModel.CreatedDate = permissionInSetFromDb.CreatedDate;
                            mappedViewModel.IsActive = true;
                            string[] ids = _permissionSetViewModel.Permission.Split(',');
                            var permissionInSetViewModels = new Collection<PermissionInSetViewModel>();

                            foreach (var id in ids)
                            {
                                var permissionInSetViewModel = new PermissionInSetViewModel();
                                permissionInSetViewModel.PermissionId = id.As<long>();
                                permissionInSetViewModel.PermissionSetId = _permissionSetViewModel.PermissionSetId;

                                permissionInSetViewModels.Add(permissionInSetViewModel);
                            }
                            var mappedPermissionInSet = Mapper.Map<Collection<PermissionInSetViewModel>, Collection<PermissionInSet>>(permissionInSetViewModels);
                            mappedViewModel.PermissionInSet = mappedPermissionInSet;
                            var permissionSet = userbl.UpdatePermissionSet(mappedViewModel);
                            if (permissionSet.IsNotNull())
                                OpenMessageBox(string.Format("Successfully updated the Permission Set '{0}'".ToFormat(permissionSet.PermissionSetName)));
                        }
                    }
                    else
                    {
                        permissionSetModel.ModuleObjectName = _permissionSetViewModel.ModuleObjectsName;
                        permissionSetModel.PermissionSetName = _permissionSetViewModel.PermissionSetName.Trim();
                        permissionSetModel.ModuleId = _permissionSetViewModel.ModuleId;
                        permissionSetModel.CreatedDate = DateTime.Now;
                        permissionSetModel.ModifiedDate = DateTime.Now;
                        permissionSetModel.CreatedBy = currentPrincipal.UserId;
                        permissionSetModel.ModifiedBy = currentPrincipal.UserId;
                        string[] ids;
                        if (_permissionSetViewModel.Permission == "")
                        {
                            ids = null;
                        }
                        else
                        {
                            ids = _permissionSetViewModel.Permission.Split(',');
                        }
                        var permissionInSetViewModels = new Collection<PermissionInSet>();
                        if (ids.IsCollectionValid())
                        {

                            foreach (var id in ids)
                            {
                                if (id.IsNotNull())
                                {
                                    var permissionInSetModel = new PermissionInSet();
                                    permissionInSetModel.PermissionId = id.As<long>();
                                    permissionInSetModel.PermissionSetId = _permissionSetViewModel.PermissionSetId;
                                    permissionInSetViewModels.Add(permissionInSetModel);
                                }
                            }
                        }
                        permissionSetModel.PermissionInSet = permissionInSetViewModels;

                        var permissionSet = userBl.InsertPermissionSet(permissionSetModel);
                        if (permissionSet != null && permissionSet.PermissionSetId != 0)
                            OpenMessageBox(string.Format("Successfully created the new Permission Set '{0}'".ToFormat(permissionSet.PermissionSetName)));
                    }
                }
                LoadDataGrid();
                ResetToDefault();
            }
            else
            {
                MessageBoxControl.Show(res, MessageBoxButton.OK, MessageType.Error);
            }
        }

        private async void LoadDataGrid()
        {
            using (var userBl = new UserManagementBL())
            {
                var permissionSet = await Task<List<PermissionSet>>.Factory.StartNew(() => userBl.GetPermissionSet().OrderBy(x => x.PermissionSetName).ToList());
                Bootstrapper.Configure();

                var mappedPermissionSet = Mapper.Map<List<PermissionSetViewModel>>(permissionSet);

                _permissionSetViewModel.PermissionSetLst = mappedPermissionSet;
            }
        }


        private ObservableCollection<PermissionViewModel> AssignPermissions()
        {
            Bootstrapper.Configure();
            using (var userBl = new UserManagementBL())
            {
                var newPermLst = new ObservableCollection<PermissionViewModel>();
                var permissions = userBl.GetPermission().OrderBy(x => x.PermissionName).ToList();
                var mappedPermissions = Mapper.Map<List<Permission>, List<PermissionViewModel>>(permissions);
                if (mappedPermissions.IsCollectionValid())
                    newPermLst.AddRange(mappedPermissions);
                return newPermLst;
            }
        }


        private void cmbModule_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var selectedValue = Convert.ToInt32(cmbModule.SelectedValue);
            var selectedValue = _permissionSetViewModel.ModuleId;
            _permissionSetViewModel.ModuleObjectsViewModelsLst = BindModuleObjects(selectedValue);
            //  _permissionSetViewModel.ModuleObjectsName = "-Select-";
            _permissionSetViewModel.ModuleObjectsViewModelsLst.Insert(0, new ModuleObjectsViewModel() { ModuleObjectsDesc = "-Select", ModuleObjectsName = "-Select-" });
            _permissionSetViewModel.ModuleObjectsName = _permissionSetViewModel.ModuleObjectsViewModelsLst.FirstOrDefault().ModuleObjectsName;
        }

        public string GetModule(string moduleObject)
        {
            var repository = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName.Contains("KAMP.Core.Repository"));
            var loadAssembly = AppDomain.CurrentDomain.GetAssemblies()
               .Select(x => Assembly.Load(repository.ToString())).FirstOrDefault().ExportedTypes;
            foreach (var type in loadAssembly)
            {
                var attributes = type.CustomAttributes;
                Dictionary<string, string> modDict = new Dictionary<string, string>();
                foreach (var attribute in attributes)
                {
                    var modClass = attribute.ConstructorArguments.FirstOrDefault().Value.ToString();
                    if (attribute.AttributeType == typeof(KAMPObjectAttribute))
                    {
                        string className = type.Name;
                        if (moduleObject.EqualsIgnoreCase(className))
                            return modClass;
                    }
                }
            }
            return string.Empty;
        }

        private List<ModuleObjectsViewModel> BindModuleObjects(int selectedValue)
        {
            if (selectedValue != 0)
            {
                List<string> classNames = new List<string>();
                List<ModuleObjectsViewModel> moduleObjs = new List<ModuleObjectsViewModel>();
                var moduleName = ((KAMPPrincipal)Thread.CurrentPrincipal).GetModule()[selectedValue];
                var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName.Contains(moduleName));
                var repository = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName.Contains("KAMP.Core.Repository"));
                if (assembly.IsNotNull())
                {
                    var classes = AppDomain.CurrentDomain.GetAssemblies()
                   .Select(x => Assembly.Load(repository.ToString())).FirstOrDefault().ExportedTypes;
                    foreach (var type in classes)
                    {
                        var attributes = type.CustomAttributes;
                        if (attributes.IsCollectionValid())
                        {
                            foreach (var attribute in attributes)
                            {
                                var modClass = attribute.ConstructorArguments.FirstOrDefault().Value.ToString();
                                var entityDesc = string.Empty;
                                if (attribute.ConstructorArguments.LastOrDefault().Value != null)
                                {
                                    entityDesc = attribute.ConstructorArguments.LastOrDefault().Value.ToString();
                                }


                                var modIgnr = moduleName;
                                var tr = modClass.ContainsIgnoreCase(modIgnr);
                                if (attribute.AttributeType == typeof(KAMPObjectAttribute))
                                {
                                    string className = type.Name;
                                    var modObjs = new ModuleObjectsViewModel();
                                    if (tr)
                                    {
                                        if (!moduleObjs.Any(x => x.ModuleObjectsName.Contains(className)))
                                        {
                                            modObjs = new ModuleObjectsViewModel()
                                            {
                                                ModuleObjectsName = className,
                                                ModuleObjectsDesc = entityDesc
                                            };
                                            moduleObjs.Add(modObjs);
                                        }
                                    }
                                    else
                                    {
                                        if (modClass == "Common" && modIgnr != "UM")
                                        {
                                            modObjs = new ModuleObjectsViewModel()
                                            {
                                                ModuleObjectsName = className,
                                                ModuleObjectsDesc = entityDesc
                                            };
                                            moduleObjs.Add(modObjs);
                                        }
                                    }


                                }
                            }
                        }

                    }
                }
                else
                {
                    OpenMessageBox("There are no module objects for the module.");
                }

                return moduleObjs.OrderBy(x => x.ModuleObjectsName).ToList();
            }
            return new List<ModuleObjectsViewModel>();
        }

        private string Validate()
        {
            StringBuilder errorMessage = new StringBuilder();
            if (!_permissionSetViewModel.PermissionSetName.IsNotEmpty())
            {
                errorMessage = errorMessage.AppendLine("Permission Name is mandatory.");
            }
            if (_permissionSetViewModel.ModuleId == 0)
                errorMessage = errorMessage.AppendLine("Please select a module");
            if (!_permissionSetViewModel.ModuleObjectsName.IsNotEmpty())
                errorMessage = errorMessage.AppendLine("Please select a module object.");
            var items = _permissionSetViewModel.Permissions.Where(x => x.IsSelected).ToList();
            if (_permissionSetViewModel.ModuleNames == "-Select-")
            {
                errorMessage = errorMessage.AppendLine("Please select a module");
            }
            if (_permissionSetViewModel.ModuleObjectsName == "-Select-")
            {
                errorMessage = errorMessage.AppendLine("Please select a module object.");
            }
            //if (!items.IsCollectionValid())
            //    errorMessage = errorMessage.AppendLine("Please select the permissions");
            string errors = errorMessage.ToString();
            if (errors != "")
                return errors;
            return null;
        }

        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, MessageType.Information);
        }

        //private void cmbModuleObjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var selectedValue = _permissionSetViewModel.ModuleObjectsName;
        //    _permissionSetViewModel.SelectedObjectName = _permissionSetViewModel.ModuleObjectsName;
        //}
    }
}
