﻿using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.BLL;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMP.Core.UserManagement.UserControls
{
    using AutoMapper;
    using KAMP.Core.Common.Models;
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository.UM;
    using KAMP.Core.UM.UserControls;
    using KAMP.Core.UserManagement.AppCode.Automapper;
    /// <summary>
    /// Interaction logic for AddGroup.xaml
    /// </summary>
    public partial class AddGroup : UserControl, KAMP.Core.UserManagement.AppCode.IPageControl
    {
        private GroupViewModel _groupViewModel = null;
        KAMPPrincipal currentPrincipal = ((KAMPPrincipal)Thread.CurrentPrincipal);
        public AddGroup()
        {
            InitializeComponent();
            Loaded += Load;
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        private void OnDeleteClick(long param)
        {
            using (var userBl = new UserManagementBL())
            {
                var group = userBl.GetAllGroup().FirstOrDefault(x => x.GroupId == param).GroupName;
                if (group.EqualsIgnoreCase("Analyst"))
                {
                    MessageBoxControl.Show("You cannot delete this group. Please read the user manual for further clarification.", MessageBoxButton.OK, MessageType.Error);
                    return;
                }
                var groupStateMap = userBl.CheckGroupStateExist(param);
                if (groupStateMap)
                {
                    MessageBoxControl.Show("You cannot delete this group as it is assigned to a state in the workflow process.", MessageBoxButton.OK, MessageType.Error);
                    return;
                }

                //var button = sender as Button;
                var res = MessageBoxControl.Show("Are you sure you want to change the status of the group '{0}' ?".ToFormat(group), MessageBoxButton.YesNo, MessageType.Alert);
                if (res == MessageBoxResult.Yes)
                {
                    var id = param;

                    userBl.DeleteGroup(id);

                    LoadDataGrid();
                }
                else
                {
                    LoadDataGrid();
                }
            }
        }

        private async void LoadDataGrid()
        {
            using (var userBl = new UserManagementBL())
            {
                var groups = await Task<List<Group>>.Factory.StartNew(() => userBl.GetAllGroup().OrderBy(x => x.GroupName).ToList());

                if (groups.IsCollectionValid())
                {
                    _groupViewModel.Groups = groups;
                }
            }
        }

        private string Validate()
        {
            StringBuilder errorMessage = new StringBuilder();
            if (!_groupViewModel.GroupName.IsNotEmpty())
            {
                errorMessage = errorMessage.AppendLine("Group Name is mandatory.");
            }
            //if (!_groupViewModel.IsActive)
            //    errorMessage = errorMessage.AppendLine("The Group will  be Inactive.");
            string errors = errorMessage.ToString();
            if (errors != "")
                return errors;
            return null;
        }

        private void OnSubmitClick()
        {
            Bootstrapper.Configure();
            var reslt = Validate();
            if (reslt != null)
            {
                MessageBoxControl.Show(reslt, MessageBoxButton.OK, MessageType.Error);
            }
            else
            {
                _groupViewModel.CreatedDate = _groupViewModel.ModifiedDate = DateTime.Now;
                _groupViewModel.CreatedBy = _groupViewModel.ModifiedBy = currentPrincipal.UserId;

                var mappedGroupModel = Mapper.Map<GroupViewModel, Group>(_groupViewModel);
                if (mappedGroupModel.GroupName != null)
                {

                    using (var userBl = new UserManagementBL())
                    {
                        var groupFromDb = userBl.GetAllGroup().FirstOrDefault(x => x.GroupName == (_groupViewModel.GroupName.Trim()));
                        if (groupFromDb == null)
                        {
                            var group = userBl.InsertGroup(mappedGroupModel);
                            if (group != null)
                            {
                                OpenMessageBox("Successfully created the new group '{0}'".ToFormat(group.GroupName));
                                Clear();

                            }
                        }
                        else
                        {
                            MessageBoxControl.Show("Group Name already exists.", MessageBoxButton.OK, MessageType.Error);
                        }
                        LoadDataGrid();

                    }

                }
            }
            LoadDataGrid();
        }

        private void Clear()
        {
            _groupViewModel.GroupName = string.Empty;
            _groupViewModel.IsActive = false;

        }

        public void OpenMessageBox(string msg)
        {
            var messagebox = MessageBoxControl.Show(msg, MessageBoxButton.OK, MessageType.Information);
        }

        public void InitializeData()
        {
            this.DataContext = new GroupViewModel()
            {
                CheckBoxFillColor = Helper.Color.CheckBoxFillColor,
                BdrBrushColor = Helper.Color.BdrBrushColor,
                LblForegroundColor = Helper.Color.LblForegroundColor,
                WindowBackGroundColor = Helper.Color.WindowBackGroundColor
            };
            _groupViewModel = this.DataContext as GroupViewModel;
            _groupViewModel.OnSubmitClick += OnSubmitClick;
            _groupViewModel.OnDeleteClick += OnDeleteClick;
            _groupViewModel.OnLoadClick += OnLoadClick;
            LoadDataGrid();
        }

        private void OnLoadClick()
        {
            LoadDataGrid();

        }


    }
}
