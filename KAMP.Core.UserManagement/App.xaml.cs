﻿using KAMP.Core.Repository;
using KAMP.Core.UserManagement.AppCode;
using KAMP.Core.UserManagement.AppCode.Automapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository.Models;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.UnitOfWork;

namespace KAMP.Core.UserManagement
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Bootstrapper.Configure();
            var currentUser = InitializeAuthorization();
            var userExist = CheckUserExist(currentUser);

            if (userExist != null)
            {
                base.OnStartup(e);
            }
            else
            {
                base.MainWindow.Close();
            }
            //base.OnStartup(e);
        }

        private string InitializeAuthorization()
        {
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            var KampIdentity = WindowsIdentity.GetCurrent();

            var genericPrinciple = new KAMPPrincipal(KampIdentity);

            Thread.CurrentPrincipal = genericPrinciple;
            AppDomain.CurrentDomain.SetThreadPrincipal(genericPrinciple);

            return KampIdentity.Name;
        }

        private User CheckUserExist(string currentUser)
        {
            string[] domainUser = currentUser.Split('\\');
            var domain = domainUser[0];
            var user = domainUser[1];
            using (var dbContext = new UMUnitOfWork())
            {
                var userDtl = dbContext.User.Items.FirstOrDefault(x => x.UserName == user && x.IsActive);
                return userDtl;
            }
        }
    }
}
