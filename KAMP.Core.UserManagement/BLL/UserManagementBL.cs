﻿using KAMP.Core.Repository;
using KAMP.Core.Repository.UM;
using KAMP.Core.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace KAMP.Core.UserManagement.BLL
{
    using FrameworkComponents;
    using KAMP.Core.UserManagement.AppCode;
    using System.Threading;
    public class UserManagementBL : IDisposable
    {

        #region User
        public List<User> GetAllUsers()
        {
            using (var uow = new UMUnitOfWork())
            {
                var users = uow.GetRepository<User>().Items.OrderBy(x => x.UserName).ToList();
                return users;
            }
        }

        public User InsertUser(User user)
        {
            using (var uow = new UMUnitOfWork())
            {
                uow.GetRepository<User>().Insert(user);
                uow.SaveChanges();
            }
            return user;
        }

        public void DeleteUser(long userId)
        {
            using (var uow = new UMUnitOfWork())
            {
                var user = uow.GetRepository<User>().Items.FirstOrDefault(x => x.UserId == userId);
                if (user.IsActive)
                    user.IsActive = false;
                else
                    user.IsActive = true;
                uow.GetRepository<User>().Update(user);
                uow.SaveChanges();
            }
        }

        public User GetUserByid(long id)
        {
            using (var uow = new UMUnitOfWork())
            {
                var user = uow.GetRepository<User>().Items.Include(x => x.UserInRole.Select(y => y.Role.RoleInPermission.Select(z => z.PermissionSet))).FirstOrDefault(x => x.UserId == id);
                return user;
            }
        }
        #endregion

        #region Role

        public List<Role> GetAllRole()
        {
            using (var uow = new UMUnitOfWork())
            {
                var roles = uow.GetRepository<Role>().Items.Include(x => x.RoleInPermission).OrderBy(x => x.RoleName).ToList();
                return roles;
            }
        }

        public Role InsertRole(Role role)
        {
            using (var uow = new UMUnitOfWork())
            {
                uow.GetRepository<Role>().Insert(role);
                uow.SaveChanges();
            }
            return role;
        }

        public void DeleteRole(long roleId)
        {
            using (var uow = new UMUnitOfWork())
            {
                var role = uow.GetRepository<Role>().Items.FirstOrDefault(x => x.RoleId == roleId);
                if (role.IsActive)
                    role.IsActive = false;
                else
                    role.IsActive = true;
                uow.GetRepository<Role>().Update(role);
                uow.SaveChanges();
            }
        }

        #endregion

        #region Group

        public IList<Group> GetAllGroup()
        {
            using (var uow = new UMUnitOfWork())
            {
                var groups = uow.GetRepository<Group>().Items.OrderBy(x => x.GroupName).ToList();
                return groups;
            }
        }

        public Group InsertGroup(Group group)
        {
            using (var uow = new UMUnitOfWork())
            {
                uow.GetRepository<Group>().Insert(group);
                uow.SaveChanges();
            }
            return group;
        }

        public void DeleteGroup(long groupId)
        {
            using (var uow = new UMUnitOfWork())
            {
                var group = uow.GetRepository<Group>().Items.FirstOrDefault(x => x.GroupId == groupId);
                if (group.IsActive)
                    group.IsActive = false;
                else
                    group.IsActive = true;
                uow.GetRepository<Group>().Update(group);
                uow.SaveChanges();
            }
        }

        #endregion

        #region Permission

        public List<Permission> GetPermission()
        {
            using (var uow = new UMUnitOfWork())
            {
                var permission = uow.GetRepository<Permission>().Items.OrderBy(x => x.PermissionName).ToList();
                return permission;
            }
        }

        #endregion

        #region Permission Set

        public PermissionSet InsertPermissionSet(PermissionSet permissionSet)
        {
            using (var uow = new UMUnitOfWork())
            {

                permissionSet.PermissionInSet.Each(x =>
                                                                    {
                                                                        uow.GetRepository<PermissionInSet>().Insert(x);
                                                                    });

                permissionSet.IsActive = true;
                uow.GetRepository<PermissionSet>().Insert(permissionSet);
                uow.SaveChanges();
            }
            return permissionSet;
        }

        public List<PermissionSet> GetPermissionSet()
        {
            using (var uow = new UMUnitOfWork())
            {
                var permissionSet = uow.GetRepository<PermissionSet>().Items.Include(x => x.PermissionInSet).Include(x => x.RoleInPermission).OrderBy(x => x.PermissionSetName).ToList();
                return permissionSet;
            }
        }

        public PermissionSet GetPermissionById(long permissionSetId)
        {
            using (var uow = new UMUnitOfWork())
            {
                var permissionSet = uow.GetRepository<PermissionSet>().Items.Include(x => x.PermissionInSet).FirstOrDefault(x => x.PermissionSetId == permissionSetId);
                return permissionSet;
            }
        }

        public void DeletePermissionInSet(List<long> permInSetId)
        {
            using (var uow = new UMUnitOfWork())
            {
                if (permInSetId.IsCollectionValid())
                {
                    foreach (var id in permInSetId)
                    {
                        var permInSetFrmDb = uow.GetRepository<PermissionInSet>().Items.FirstOrDefault(x => x.PermissionInSetId == id);
                        uow.GetRepository<PermissionInSet>().Delete(permInSetFrmDb);
                    }
                    uow.SaveChanges();
                }
            }
        }

        public PermissionSet UpdatePermissionSet(PermissionSet permissionSet)
        {
            using (var uow = new UMUnitOfWork())
            {
                var permissionSetFromDb = uow.GetRepository<PermissionSet>().Items.Include(x => x.PermissionInSet).FirstOrDefault(x => x.PermissionSetId == permissionSet.PermissionSetId);
                permissionSet.CreatedDate = permissionSetFromDb.CreatedDate;
                permissionSetFromDb.ModuleObjectName = permissionSet.ModuleObjectName;
                permissionSetFromDb.PermissionSetName = permissionSet.PermissionSetName;

                permissionSet.PermissionInSet.Each(x =>
                                                                    {
                                                                        //uow.PermissionInSet.Insert(x);
                                                                        if (x.PermissionId != 0 && x.PermissionSetId != 0)
                                                                            permissionSetFromDb.PermissionInSet.Add(x);
                                                                    });

                permissionSet.ModifiedDate = DateTime.Now;
                uow.GetRepository<PermissionSet>().Update(permissionSetFromDb);
                uow.SaveChanges();
            }
            return permissionSet;
        }

        public void DeletePermissionSet(long permissionSetId)
        {
            using (var uow = new UMUnitOfWork())
            {
                var permissionSetFromDb = uow.GetRepository<PermissionSet>().Items.Include(x => x.PermissionInSet).FirstOrDefault(x => x.PermissionSetId == permissionSetId);
                if (permissionSetFromDb.IsActive)
                    permissionSetFromDb.IsActive = false;
                else
                    permissionSetFromDb.IsActive = true;
                uow.GetRepository<PermissionSet>().Update(permissionSetFromDb);
                uow.SaveChanges();
            }

        }
        #endregion

        #region User In Group

        public List<UserInGroup> GetUserInGroup()
        {
            using (var uow = new UMUnitOfWork())
            {
                var userGrp = uow.GetRepository<UserInGroup>().Items.OrderByDescending(x => x.UserInGroupId).ToList();
                return userGrp;
            }
        }

        public void InsertUserInGroup(UserInGroup userInGroup)
        {
            using (var uow = new UMUnitOfWork())
            {
                uow.GetRepository<UserInGroup>().Insert(userInGroup);
                uow.SaveChanges();
            }
        }

        public UserInGroup GetUserInGroupById(long userInGroupId)
        {
            using (var uow = new UMUnitOfWork())
            {
                var userInGroup = uow.GetRepository<UserInGroup>().Items.FirstOrDefault(x => x.UserInGroupId == userInGroupId);
                return userInGroup;
            }
        }

        public void DeleteUserinGroup(long userInGroupId)
        {
            using (var uow = new UMUnitOfWork())
            {
                var userInGroup = uow.GetRepository<UserInGroup>().Items.FirstOrDefault(x => x.UserInGroupId == userInGroupId);
                if (userInGroup.IsActive)
                    userInGroup.IsActive = false;
                else
                    userInGroup.IsActive = true;
                uow.GetRepository<UserInGroup>().Update(userInGroup);
                uow.SaveChanges();
            }
        }

        #endregion

        #region Module

        public List<Module> GetAllModule()
        {
            using (var uow = new UMUnitOfWork())
            {
                var mod = uow.GetRepository<Module>().Items.OrderBy(x => x.ModuleName).ToList();
                return mod;
            }
        }

        #endregion

        #region User In Role

        public List<UserInRole> GetUserInRole()
        {
            using (var uow = new UMUnitOfWork())
            {
                var userInRole = uow.GetRepository<UserInRole>().Items.OrderByDescending(x => x.UserInRoleId).ToList();
                return userInRole;
            }
        }

        public void InsertUserInRole(UserInRole userInRole)
        {
            using (var uow = new UMUnitOfWork())
            {
                uow.GetRepository<UserInRole>().Insert(userInRole);
                uow.SaveChanges();
            }
        }

        #endregion

        #region Role In Permission

        public List<RoleInPermission> GetRoleInPermission()
        {
            using (var uow = new UMUnitOfWork())
            {
                var roleInPermission = uow.GetRepository<RoleInPermission>().Items.Include(x => x.PermissionSet).OrderByDescending(x => x.RoleInPermissionId).ToList();
                return roleInPermission;
            }
        }

        public void InsertRoleInPermission(RoleInPermission roleInPermission)
        {
            using (var uow = new UMUnitOfWork())
            {
                uow.GetRepository<RoleInPermission>().Insert(roleInPermission);
                uow.SaveChanges();
            }
        }

        #endregion

        #region Dispose
        public void Dispose()
        {
            GC.WaitForFullGCComplete();
        }
        #endregion

        public void DeleteUserInRole(long id)
        {
            using (var uow = new UMUnitOfWork())
            {
                var userInRoleDb = uow.GetRepository<UserInRole>().Items.FirstOrDefault(x => x.UserInRoleId == id);
                if (userInRoleDb.IsActive == false)
                    userInRoleDb.IsActive = true;
                else if (userInRoleDb.IsActive)
                    userInRoleDb.IsActive = false;
                uow.GetRepository<UserInRole>().Update(userInRoleDb);
                uow.SaveChanges();
            }
        }

        public void DeleteRoleInPermission(long id)
        {
            using (var uow = new UMUnitOfWork())
            {
                var roleInPermissionDb = uow.GetRepository<RoleInPermission>().Items.FirstOrDefault(x => x.RoleInPermissionId == id);
                if (roleInPermissionDb != null && roleInPermissionDb.IsActive.HasValue)
                {
                    if (roleInPermissionDb.IsActive.Value == false)
                        roleInPermissionDb.IsActive = true;
                    else if (roleInPermissionDb.IsActive.Value)
                        roleInPermissionDb.IsActive = false;
                    uow.GetRepository<RoleInPermission>().Update(roleInPermissionDb);
                    uow.SaveChanges();
                }

            }
        }

        internal bool CheckGroupStateExist(long param)
        {
            using (var uow = new UMUnitOfWork())
            {
                var groupState = uow.GetRepository<Group>().Items.Include(x => x.States).FirstOrDefault(y => y.GroupId == param);
                if (groupState.States.IsCollectionValid())
                {
                    return true;
                }
                return false;
            }
        }

        internal void UpdateUser(User mappedUserModel)
        {
            using (var uow = new UMUnitOfWork())
            {
                var userFromDb = uow.GetRepository<User>().Items.FirstOrDefault(x => x.UserId == mappedUserModel.UserId);
                if (userFromDb.IsNotNull())
                {
                    userFromDb.UserId = mappedUserModel.UserId;
                    userFromDb.FirstName = mappedUserModel.FirstName;
                    userFromDb.LastName = mappedUserModel.LastName;
                    userFromDb.UserName = mappedUserModel.UserName;
                    userFromDb.Email = mappedUserModel.Email;
                    userFromDb.IsActive = mappedUserModel.IsActive;
                    userFromDb.IsSuperAdmin = mappedUserModel.IsSuperAdmin;
                    userFromDb.ModifiedBy = mappedUserModel.ModifiedBy;
                    userFromDb.ModifiedDate = mappedUserModel.ModifiedDate;
                    uow.GetRepository<User>().Update(userFromDb);
                    uow.SaveChanges();
                }
            }
        }
    }
}
