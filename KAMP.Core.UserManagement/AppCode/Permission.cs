﻿
using KAMP.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace KAMP.Core.UserManagement.AppCode
{
    public class Permissions
    {
        private static Dictionary<string, List<string>> RolePermColl;
        // private static string UserRole = "SuperAdmin";
        private static string Action = "";
        public static void RecalculateControlVisibility(UIElement control, bool hasPermission)
        {
            if (hasPermission)
                control.Visibility = Visibility.Visible;
            else
                control.Visibility = Visibility.Collapsed;
        }
        public static void RecalculateControlIsEnabled(Control control, bool hasPermission)
        {
            control.IsEnabled = hasPermission;
        }

        public static readonly DependencyProperty VisibilityProperty =
            DependencyProperty.RegisterAttached("Visibility", typeof(string), typeof(Permissions), new PropertyMetadata(Visibility_Callback));


        public static readonly DependencyProperty ObjectActionProperty =
          DependencyProperty.RegisterAttached("ObjectAction", typeof(string), typeof(Permissions), new PropertyMetadata(Action_Callback));

        public static void Action_Callback(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            var uiElement = source as UIElement;
            Action = GetObjectAction(uiElement).ToString();
        }

        public static void Visibility_Callback(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            bool hasPermission = false;
            var uiElement = source as UIElement;
            var permissions = GetVisibility(uiElement).ToString();
            var objectAction = GetObjectAction(uiElement);
            //PermissionType permission = new PermissionType();

            //if(permissions.FirstOrDefault(x=> x.Equals(UserRole)) !=null)
            //{
            //    var permssetting = new List<string>();
            //    var permset = RolePermColl.TryGetValue(UserRole, out permssetting);

            //    var permaction = (PermissionType)Enum.Parse(permission.GetType(), Action, false);

            //    if (permssetting.Find(x => x.Contains(permaction.ToString())) != null)
            //        hasPermission = true;
            //}         
            hasPermission = ((KAMPPrincipal)Thread.CurrentPrincipal).ValidatePermissions(permissions, Action);

            RecalculateControlVisibility(uiElement, hasPermission);
        }

        private static object GetVisibility(UIElement uiElement)
        {
            // GenerateRolePermission();
            return uiElement.GetValue(VisibilityProperty) as string;
        }
        public static void SetVisibility(UIElement element, string value)
        {
            element.SetValue(VisibilityProperty, value);

        }

        private static object GetObjectAction(UIElement uiElement)
        {

            return uiElement.GetValue(ObjectActionProperty) as string;
        }
        public static void SetObjectAction(UIElement element, string value)
        {
            element.SetValue(ObjectActionProperty, value);

        }

    }
}
