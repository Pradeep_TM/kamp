﻿using AutoMapper;
using KAMP.Core.Repository.UM;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.AppCode.Automapper
{
    using KAMP.Core.FrameworkComponents;
    using KAMP.Core.Repository;
    using KAMP.Core.UserManagement.BLL;
    public class Bootstrapper
    {

        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<UMMapperProfile>();
            });

            Mapper.AssertConfigurationIsValid();
        }

    }
}
