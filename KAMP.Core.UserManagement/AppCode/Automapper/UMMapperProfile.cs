﻿using AutoMapper;
using KAMP.Core.Repository.UM;
using KAMP.Core.UserManagement.BLL;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KAMP.Core.FrameworkComponents;
using KAMP.Core.Repository;

namespace KAMP.Core.UserManagement.AppCode.Automapper
{
    public class UMMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UserViewModel, User>()
                .ForMember(d => d.IsSuperAdmin, s => s.MapFrom(p => p.IsSuperAdmin))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<User, UserViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<RoleViewModel, Role>().IgnoreAllNonExisting();
            Mapper.CreateMap<Role, RoleViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<GroupViewModel, Group>().IgnoreAllNonExisting();
            Mapper.CreateMap<Group, GroupViewModel>()
                .ForMember(d => d.UserGroupId, s => s.MapFrom(p => p.GroupId))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<ModuleTypeViewModel, Module>().IgnoreAllNonExisting();
            Mapper.CreateMap<Module, ModuleTypeViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<PermissionViewModel, Permission>()
                .ForMember(d => d.Id, s => s.MapFrom(p => p.PermissionId))
                .ForMember(d => d.PermissionName, s => s.MapFrom(p => p.PermissionName))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<Permission, PermissionViewModel>()
                .ForMember(d => d.PermissionId, s => s.MapFrom(p => p.Id))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<UserInGroupViewModel, UserInGroup>()
                .ForMember(d => d.UserInGroupId, s => s.MapFrom(p => p.UserGroupId))
                .ForMember(d => d.GroupId, s => s.MapFrom(p => p.UserGroupId))
                .ForMember(d => d.ModuleTypeId, s => s.MapFrom(p => p.ModuleId))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<UserInGroup, UserInGroupViewModel>()
                .ForMember(d => d.UserGrpMapId, s => s.MapFrom(p => p.UserInGroupId))
                .ForMember(d => d.ModuleName, s => s.MapFrom(p => GetModuleName(p.ModuleTypeId)))
                .ForMember(d => d.UserGroupName, s => s.MapFrom(p => GetUserGroupName(p.GroupId)))
                .ForMember(d => d.UserName, s => s.MapFrom(p => GetUserName(p.UserId)))
                .ForMember(d => d.IsActive, s => s.MapFrom(p => p.IsActive))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<ModuleTypeViewModel, Module>().IgnoreAllNonExisting();
            Mapper.CreateMap<Module, ModuleTypeViewModel>()
                .ForMember(d => d.ModuleId, s => s.MapFrom(p => p.ModuleTypeId))
                .ForMember(d => d.ModuleName, s => s.MapFrom(p => p.ModuleName))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<UserInRoleViewModel, UserInRole>()
                .ForMember(d => d.UserInRoleId, s => s.MapFrom(p => p.UserRoleMapId))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<UserInRole, UserInRoleViewModel>()
                .ForMember(d => d.RoleName, s => s.MapFrom(p => GetRoleName(p.RoleId)))
                .ForMember(d => d.UserName, s => s.MapFrom(p => GetUserName(p.UserId)))
                .ForMember(d => d.UserRoleMapId, s => s.MapFrom(p => p.UserInRoleId))
                .ForMember(d => d.IsActive, s => s.MapFrom(p => p.IsActive))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<RoleInPermission, RoleInPermissionViewModel>()
                .ForMember(d => d.RolePermMapId, s => s.MapFrom(p => p.RoleInPermissionId))
                 .ForMember(d => d.RoleName, s => s.MapFrom(p => GetRoleName(p.RoleId)))
                .ForMember(d => d.PermissionName, s => s.MapFrom(p => GetPermissionName(p.PermissionSetId)))
                .ForMember(d => d.ModuleName, s => s.MapFrom(p => GetModuleName(p.ModuleId)))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<RoleInPermissionViewModel, RoleInPermission>()
                .ForMember(d => d.ModuleType, s => s.Ignore())
                .IgnoreAllNonExisting();
            Mapper.CreateMap<PermissionSet, PermissionSetViewModel>()
                .ForMember(d => d.PermissionSetId, s => s.MapFrom(p => p.PermissionSetId))
                .ForMember(d => d.ModuleObjectsName, s => s.MapFrom(p => p.ModuleObjectName))
                //.ForMember(d => d.ModuleId, s => s.MapFrom(p => p.ModuleTypeId))
                .ForMember(d => d.PermissionSetName, s => s.MapFrom(p => p.PermissionSetName))
                .ForMember(d => d.PermissionSetLst, s => s.Ignore())
                .AfterMap((s, d) =>
                {
                    var permissionInSets = s.PermissionInSet;
                    List<string> lstPermissionName = new List<string>();
                    foreach (var permissionInSet in permissionInSets)
                    {
                        using (var userBl = new UserManagementBL())
                        {

                            var permissionName = userBl.GetPermission().FirstOrDefault(x => x.Id == permissionInSet.PermissionId).PermissionName;
                            lstPermissionName.Add(permissionName);
                        }
                    }
                    d.PermissionName = string.Join(",", lstPermissionName);
                })
                .IgnoreAllNonExisting();
            Mapper.CreateMap<PermissionSetViewModel, PermissionSet>()
                .ForMember(d => d.PermissionSetName, s => s.MapFrom(p => p.PermissionSetName))
                .ForMember(d => d.PermissionSetId, s => s.MapFrom(p => p.PermissionSetId))
                .ForMember(d => d.ModuleObjectName, s => s.MapFrom(p => p.ModuleObjectsName))
                // .ForMember(d => d.ModuleTypeId, s => s.MapFrom(p => p.ModuleId))
                .ForMember(d => d.PermissionInSet, s => s.Ignore())
                .IgnoreAllNonExisting();


            Mapper.CreateMap<PermissionInSet, PermissionInSetViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<PermissionInSetViewModel, PermissionInSet>()
                .ForMember(d => d.Permission, s => s.Ignore())
                .ForMember(d => d.PermissionSet, s => s.Ignore())
                .IgnoreAllNonExisting();
        }

        private string GetUserGroupName(long groupId)
        {
            using (var userBl = new UserManagementBL())
            {
                var groupName = userBl.GetAllGroup().FirstOrDefault(x => x.GroupId == groupId).GroupName;
                return groupName;
            }
        }

        private string GetModuleName(int moduleId)
        {
            using (var userBl = new UserManagementBL())
            {
                var moduleName = userBl.GetAllModule().FirstOrDefault(x => x.ModuleTypeId == moduleId).ModuleName;
                return moduleName;
            }
        }

        private string GetPermissionName(long permissionSetId)
        {
            using (var userBl = new UserManagementBL())
            {
                var permissionName = userBl.GetPermissionSet().FirstOrDefault(x => x.PermissionSetId == permissionSetId);
                if (permissionName != null)
                    return permissionName.PermissionSetName;
                return null;

            }
        }

        private string GetUserName(long userId)
        {
            using (var userBl = new UserManagementBL())
            {
                var userName = userBl.GetAllUsers().FirstOrDefault(x => x.UserId == userId);
                if (userName != null)
                    return userName.UserName;
                return null;
            }
        }

        private string GetRoleName(long roleId)
        {
            using (var userBl = new UserManagementBL())
            {
                var roleName = userBl.GetAllRole().FirstOrDefault(x => x.RoleId == roleId);
                if (roleName != null)
                    return roleName.RoleName;
                return null;
            }
        }
    }
}
