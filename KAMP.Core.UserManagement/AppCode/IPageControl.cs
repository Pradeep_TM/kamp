﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KAMP.Core.UserManagement.AppCode
{
    public interface IPageControl
    {
        void InitializeData();
    }
}
