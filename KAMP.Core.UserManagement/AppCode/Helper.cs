﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.AppCode
{
    public class Helper
    {
        public class Color
        {
            public static readonly string BdrBrushColor = "#FF82BCD6";
            public static readonly string LblForegroundColor = "#0339b1";
            public static readonly string WindowBackGroundColor = "#FFCBE4E6";
            public static readonly string CheckBoxFillColor = "#FF0074B9";
        }
    }
}
