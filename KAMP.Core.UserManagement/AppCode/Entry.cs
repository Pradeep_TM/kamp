﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.AppCode
{
    using Core.Interface;
    using Core.Models;
    using Core.UserManagement.AppCode.Automapper;
    using KAMP.Core.FrameworkComponents.Enums;
    [Export(typeof(IModule))]
    public class Entry : IModule
    {
        ModuleMetadata _metadata = null;
        public void Initialize()
        {
            var metadata = new ModuleMetadata();
            metadata.ModuleName = ModuleNames.UM.ToString();

            Bootstrapper.Configure();
            metadata.HomeWindow = new MainWindow();

            //metadata.HomeWindow = new AssignRoutines();

            Metadata = metadata;

        }

        public ModuleMetadata Metadata
        {
            get
            {
                return _metadata;
            }
            set
            {
                this._metadata = value;
            }
        }
    }
}
