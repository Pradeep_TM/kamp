﻿using KAMP.Core.Common.Models;
using KAMP.Core.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KAMP.Core.UserManagement.AppCode
{
    public class Common
    {
        public static System.Collections.ObjectModel.ObservableCollection<ModuleTypeViewModel> AssignModuleDropDown()
        {
            var principalModule = ((KAMPPrincipal)Thread.CurrentPrincipal).GetModule().OrderBy(x => x.Key).ToList();
            System.Collections.ObjectModel.ObservableCollection<ModuleTypeViewModel> obmodules = new System.Collections.ObjectModel.ObservableCollection<ModuleTypeViewModel>();

            List<ModuleTypeViewModel> moduleLstViewModel = new List<ModuleTypeViewModel>();
            if (principalModule != null)
            {
                foreach (var item in principalModule)
                {
                    var module = new ModuleTypeViewModel()
                    {
                        ModuleId = item.Key,
                        ModuleName = item.Value
                    };

                    moduleLstViewModel.Add(module);
                }
            }

            var modulesLst = moduleLstViewModel.OrderBy(x => x.ModuleName).ToList();
            modulesLst.ForEach(obmodules.Add);
            var moduleInitial = new ModuleTypeViewModel()
            {
                ModuleId = 0,
                ModuleName = "-Select-"
            };
            obmodules.Insert(0, moduleInitial);
            return obmodules;

        }
    }
}
