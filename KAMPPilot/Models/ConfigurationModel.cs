﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMPPilot
{
    public class ConfigurationModel : INotifyPropertyChanged
    {
        public ConfigurationModel()
        {

        }

        private string _clientName;
        public string ClientName
        {
            get { return _clientName; }
            set
            {
                _clientName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientName"));
            }
        }

        private string _clientAbbreviation;
        public string ClientAbbreviation
        {
            get { return _clientAbbreviation; }
            set
            {
                _clientAbbreviation = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientAbbreviation"));
            }
        }
        private string _engagementName;
        public string EngagementName
        {
            get { return _engagementName; }
            set
            {
                _engagementName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EngagementName"));
            }
        }
        private string _engagementNo;
        public string EngagementNo
        {
            get { return _engagementNo; }
            set
            {
                _engagementNo = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EngagementNo"));
            }
        }
        private string _country;
        public string Country
        {
            get { return _country; }
            set
            {
                _country = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Country"));
            }
        }
        private string _serverName;
        public string ServerName
        {
            get { return _serverName; }
            set
            {
                _serverName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ServerName"));
            }
        }

        private string _databaseName;
        public string DatabaseName
        {
            get { return _databaseName; }
            set
            {
                _databaseName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("DatabaseName"));
            }
        }

        private byte[] _clientLogo;
        public byte[] ClientLogo
        {
            get { return _clientLogo; }
            set
            {
                _clientLogo = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientLogo"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
