﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KAMPPilot
{
    /// <summary>
    /// Interaction logic for AutoCompleteBox.xaml
    /// </summary>
    public partial class AutoCompleteBox : UserControl
    {
        public AutoCompleteBox()
        {
            InitializeComponent();
            txtAutoComplete.TextChanged += new TextChangedEventHandler(txtAutoComplete_TextChanged);
        }

        /// <summary>
        /// display item count 
        /// </summary>
        public int DisplayCount
        {
            get { return (int)GetValue(DisplayCountProperty); }
            set { SetValue(DisplayCountProperty, value); }
        }

        public static readonly DependencyProperty DisplayCountProperty =
        DependencyProperty.Register("DisplayCount", typeof(int), typeof(AutoCompleteBox), new UIPropertyMetadata(int.MaxValue));

        /// <summary>
        /// Display Member for autocomplete to display the suggestion list
        /// </summary>
        public string DisplayMemberPath
        {
            get { return (string)GetValue(DisplayMemberPathProperty); }
            set { SetValue(DisplayMemberPathProperty, value); }
        }

        public static readonly DependencyProperty DisplayMemberPathProperty =
        DependencyProperty.Register("DisplayMemberPath", typeof(string), typeof(AutoCompleteBox), new UIPropertyMetadata(""));


        /// <summary>
        /// Selected value  form suggestion list
        /// </summary>
        public string SelectedValuePath
        {
            get { return (string)GetValue(SelectedValuePathProperty); }
            set { SetValue(SelectedValuePathProperty, value); }
        }

        public static readonly DependencyProperty SelectedValuePathProperty =
        DependencyProperty.Register("SelectedValuePath", typeof(string), typeof(AutoCompleteBox), new UIPropertyMetadata(""));


        /// <summary>
        /// Height Of Auto Complete
        /// </summary>
        public double HeightValue
        {
            get { return (double)GetValue(ItemHeightProperty); }
            set { SetValue(ItemHeightProperty, value); }
        }

        public static readonly DependencyProperty ItemHeightProperty =
        DependencyProperty.Register("HeightValue", typeof(double), typeof(AutoCompleteBox), new UIPropertyMetadata(0.0));

        /// <summary>
        /// Width Of Auto Complete
        /// </summary>
        public double WidthValue
        {
            get { return (double)GetValue(ItemWidthProperty); }
            set { SetValue(ItemWidthProperty, value); }
        }

        public static readonly DependencyProperty ItemWidthProperty =
        DependencyProperty.Register("WidthValue", typeof(double), typeof(AutoCompleteBox), new UIPropertyMetadata(0.0));


        public static readonly DependencyProperty CornerRadiusProperty =
           DependencyProperty.Register("CornerRadius",
                                       typeof(CornerRadius),
                                       typeof(AutoCompleteBox),
                                       new UIPropertyMetadata(default(CornerRadius)));

        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        /// <summary>
        /// border thinckness Auto Complete
        /// </summary>
        public Thickness BorderThicknessValue
        {
            get { return (Thickness)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }

        public static readonly DependencyProperty BorderThicknessProperty =
        DependencyProperty.Register("BorderThicknessValue", typeof(Thickness), typeof(AutoCompleteBox), new UIPropertyMetadata(new Thickness(1.0)));

        /// <summary>
        /// border brush Auto Complete
        /// </summary>
        public Brush BorderBrushValue
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushProperty =
        DependencyProperty.Register("BorderBrushValue", typeof(Brush), typeof(AutoCompleteBox), new UIPropertyMetadata(Brushes.Black));

        /// <summary>
        /// Textbox margin  Auto Complete
        /// </summary>
        public Thickness TextBoxMargin
        {
            get { return (Thickness)GetValue(TextBoxMarginProperty); }
            set { SetValue(TextBoxMarginProperty, value); }
        }

        public static readonly DependencyProperty TextBoxMarginProperty =
        DependencyProperty.Register("TextBoxMargin", typeof(Thickness), typeof(AutoCompleteBox), new UIPropertyMetadata(new Thickness(0.0)));


        /// <summary>
        /// Text For Auto Complete
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
        DependencyProperty.Register("Text", typeof(string), typeof(AutoCompleteBox), new FrameworkPropertyMetadata { BindsTwoWayByDefault = true, PropertyChangedCallback = new PropertyChangedCallback(OnTextPropertyChanged) });

        /// <summary>
        /// ItemSource For AutoComplete to display the suggestion list
        /// </summary>
        public IEnumerable<object> ItemsSource
        {
            get { return (IEnumerable<object>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty =
          DependencyProperty.Register("ItemsSource", typeof(IEnumerable<object>), typeof(AutoCompleteBox), new PropertyMetadata(new PropertyChangedCallback(OnItemsSourcePropertyChanged)));




        /// <summary>
        /// SelectedItem For AutoComplete to display
        /// </summary>
        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
          DependencyProperty.Register("SelectedItem", typeof(object), typeof(AutoCompleteBox), new PropertyMetadata(new PropertyChangedCallback(OnSelectedItemPropertyChanged)));

        /// <summary>
        /// SelectedItem For AutoComplete to display
        /// </summary>
        public object SelectedValue
        {
            get { return (object)GetValue(SelectedValueProperty); }
            set { SetValue(SelectedValueProperty, value); }
        }

        public static readonly DependencyProperty SelectedValueProperty =
          DependencyProperty.Register("SelectedValue", typeof(object), typeof(AutoCompleteBox), new PropertyMetadata(new PropertyChangedCallback(OnSelectedValuePropertyChanged)));

        /// <summary>
        /// Text Change event
        /// </summary>
        public event RoutedEventHandler TextBoxContentChanged
        {
            add { AddHandler(TextBoxContentChangedEvent, value); }
            remove { RemoveHandler(TextBoxContentChangedEvent, value); }

        }

        public static readonly RoutedEvent TextBoxContentChangedEvent =
        EventManager.RegisterRoutedEvent("TextBoxContentChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AutoCompleteBox));



        #region PropertyChanged Events
        private static void OnItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as AutoCompleteBox;
            if (control != null)
            {
                control.ItemsSource = (IEnumerable<object>)e.NewValue;
            }
        }

        private static void OnTextPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as AutoCompleteBox;
            if (control != null && e.NewValue != null && (e.OldValue == null || e.OldValue.ToString() != e.NewValue.ToString()))
            {
                control.Text = (string)e.NewValue;
                control.popUp.IsOpen = false;
            }
        }

        private static void OnSelectedItemPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as AutoCompleteBox;
            if (control != null)
            {
                control.SelectedItem = (object)e.NewValue;
            }
        }

        private static void OnSelectedValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {

            var control = sender as AutoCompleteBox;
            if (control != null)
            {
                control.SelectedValue = (object)e.NewValue;
            }
        }

        #endregion

        #region Click Events
        private void txtAutoComplete_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                popUp.IsOpen = false;
                return;
            }
            if (e.Key == Key.Down)
            {
                suggestItems.Focus();
            }

            if (e.Key == Key.Up)
            {
                //var listBoxItem = (ListBoxItem)suggestItems
                //                  .ItemContainerGenerator
                //                  .ContainerFromIndex(suggestItems.Items.Count - 1);

                //suggestItems.SelectedIndex = suggestItems.Items.Count - 1;
                suggestItems.Focus();
                e.Handled = true;
            }

        }

        private void suggestItems_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                popUp.IsOpen = false;
                return;
            }
            if (e.IsDown && e.Key == Key.Enter)
            {
                var listbox = sender as ListBox;
                if (listbox == null)
                    return;
                if (string.IsNullOrEmpty(DisplayMemberPath))
                    Text = (string)listbox.SelectedItem;
                else
                    Text = listbox.SelectedItem.GetType().GetProperty(DisplayMemberPath).GetValue(listbox.SelectedItem, null) as string;

                popUp.IsOpen = false;
            }
        }

        private void suggestItems_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                var listbox = sender as ListBox;
                if (listbox == null)
                    return;
                if (string.IsNullOrEmpty(DisplayMemberPath))
                    Text = (string)listbox.SelectedItem;
                else
                    Text = listbox.SelectedItem.GetType().GetProperty(DisplayMemberPath).GetValue(listbox.SelectedItem, null) as string;

                popUp.IsOpen = false;

            }
        }
        #endregion

        #region Text Change Event
        void txtAutoComplete_TextChanged(object sender, TextChangedEventArgs e)
        {
            e.Handled = true;
            var txt = sender as TextBox;
            //Text = txt.Text;
            if (!string.IsNullOrEmpty(Text) && ItemsSource != null)
            {
                if (string.IsNullOrEmpty(DisplayMemberPath))
                {
                    var items = ItemsSource.Where(x => x.ToString().Trim().ToLower().StartsWith(Text.Trim().ToLower())).Take(DisplayCount).ToList();
                    suggestItems.ItemsSource = items;
                    this.popUp.IsOpen = items.Count > 0;
                }
                else
                {
                    var items = ItemsSource.Where(x => ((string)x.GetType().GetProperty(DisplayMemberPath).GetValue(x, null)).Trim().ToLower().StartsWith(Text.Trim().ToLower())).ToList();
                    suggestItems.ItemsSource = items;
                    this.popUp.IsOpen = items.Count > 0;
                }
            }
            RoutedEventArgs args = new RoutedEventArgs(TextBoxContentChangedEvent);
            RaiseEvent(args);
        }
        #endregion
    }
}
