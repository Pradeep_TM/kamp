﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace KAMPPilot
{
    /// <summary>
    /// Interaction logic for SplashWindow.xaml
    /// </summary>
    public partial class SplashWindow : Window
    {
        public ConfigurationViewModel viewModel;
        private string setUpZipPath;
        private string assemblyName;
        private string configFilePath;
        private string manifestFilePath;
        string extractedZipPath;

        public SplashWindow()
        {
            InitializeComponent();
            viewModel = new ConfigurationViewModel();
            this.DataContext = viewModel;
            assemblyName = System.Configuration.ConfigurationManager.AppSettings["assemblyname"];
        }

        private void SaveConfiguration(object sender, RoutedEventArgs e)
        {

            if (!ValidateEntries())
            {
                LblError.Content = "Please enter all the fields.";
                return;
            }
            else
            {
                LblError.Content = string.Empty;
            }
            //Copy Set up and extract it to a user defined path
            if (!ExtractZippedSetUp()) return;
            ModifyConfigurationFile();
            UpdateDeploymentManifest();
            #region Create hash of the file and updating Application manifest file
            var appManifestPath = ReadFilePath(extractedZipPath, assemblyName + ".application");
            ChangeAppManifest(appManifestPath, manifestFilePath);
            #endregion
            MessageBoxControl.Show("Setup Created Successfully.");
            Application.Current.Shutdown();

        }

        private bool ValidateEntries()
        {
            if (string.IsNullOrEmpty(viewModel.ConfigurationModel.ClientAbbreviation) ||
                string.IsNullOrEmpty(viewModel.ConfigurationModel.ClientName) ||
                string.IsNullOrEmpty(viewModel.ConfigurationModel.Country) ||
                string.IsNullOrEmpty(viewModel.ConfigurationModel.DatabaseName) ||
                string.IsNullOrEmpty(viewModel.ConfigurationModel.EngagementName) ||
                string.IsNullOrEmpty(viewModel.ConfigurationModel.EngagementNo) ||
                string.IsNullOrEmpty(viewModel.ConfigurationModel.ServerName)) return false;
            return true;
        }
        private void ChangeAppManifest(string appManifestFilePath, string manifestFilePath)
        {
            SHA256Managed sha256 = new SHA256Managed();
            var bytes = System.IO.File.ReadAllBytes(manifestFilePath);
            var hashBytes = sha256.ComputeHash(bytes);
            string hash = Convert.ToBase64String(hashBytes);

            //change size and hash
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(appManifestFilePath);
            XmlNode rootNode = xmlDoc.GetElementsByTagName("asmv1:assembly").Item(0);
            foreach (XmlNode item in rootNode.ChildNodes)
            {
                if (item.Name == "dependency")
                {
                    var nodes = item.ChildNodes;
                    foreach (XmlNode itm in nodes)
                    {
                        if (itm.Name == "dependentAssembly" && itm.Attributes.Item(0).Value == "install" && itm.Attributes.Item(1).Value.Contains(assemblyName + ".exe.manifest"))
                        {

                            foreach (XmlAttribute attribute in itm.Attributes)
                            {
                                if (attribute.Name == "size")
                                {
                                    //Change size here
                                    attribute.Value = bytes.Length.ToString();
                                }
                            }

                            foreach (XmlNode node in item.ChildNodes[0].ChildNodes)
                            {
                                if (node.Name == "hash")
                                    foreach (XmlNode hashNode in node.ChildNodes)
                                    {
                                        if (hashNode.Name == "dsig:DigestValue")
                                        {
                                            //Change hash Value here
                                            hashNode.InnerXml = hash;
                                        }
                                    }
                            }
                        }
                    }
                }
            }

            xmlDoc.Save(appManifestFilePath);
        }
        private void UpdateDeploymentManifest()
        {
            //Creating hash
            SHA256Managed sha256 = new SHA256Managed();
            var bytes = System.IO.File.ReadAllBytes(configFilePath);
            var hashBytes = sha256.ComputeHash(bytes);
            string hash = Convert.ToBase64String(hashBytes);

            manifestFilePath = ReadFilePath(extractedZipPath, assemblyName + ".exe.manifest");
            var doc = new XmlDocument();
            doc.Load(manifestFilePath);

            XmlNode rootNode = doc.GetElementsByTagName("asmv1:assembly").Item(0);

            foreach (XmlNode item in rootNode.ChildNodes)
            {
                if (item.Name == "file" && item.Attributes.Item(0).Value == assemblyName + ".exe.config")
                {
                    //Setting Size
                    item.Attributes.Item(1).Value = bytes.Length.ToString();
                    //Setting hash
                    foreach (XmlNode itm in item.ChildNodes[0].ChildNodes)
                    {
                        if (itm.Name == "dsig:DigestValue")
                        {
                            itm.InnerXml = hash;
                        }
                    }
                }
            }
            doc.Save(manifestFilePath);
        }

        private void ModifyConfigurationFile()
        {
            //Read EngagementSetUpFileFrom Package
            configFilePath = ReadFilePath(extractedZipPath, assemblyName + ".exe.config.deploy");
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(configFilePath);
            var oldNode = xdoc.ChildNodes[1];

            //Adding configuration values to file
            StringBuilder sb = AddCofigurationValues(oldNode);
            var node2 = xdoc.CreateNode(XmlNodeType.Element, "configuration", null);
            var decl = xdoc.CreateXmlDeclaration("1.0", "utf-8", null);
            node2.InnerXml = sb.ToString();
            xdoc.RemoveAll();
            xdoc.AppendChild(decl);
            xdoc.AppendChild(node2);
            xdoc.Save(configFilePath);
        }

        private bool ExtractZippedSetUp()
        {
            setUpZipPath = System.Configuration.ConfigurationManager.AppSettings["setupname"];
            var sfd = new SaveFileDialog();
            if (!sfd.ShowDialog().Value)
                return false;
            extractedZipPath = sfd.FileName;
            System.IO.Compression.ZipFile.ExtractToDirectory(setUpZipPath, extractedZipPath);
            return true;
        }

        private string ReadFilePath(string path, string fileName)
        {
            var directory = Directory.EnumerateFiles(path, fileName, SearchOption.AllDirectories);
            return directory.FirstOrDefault();
        }

        private StringBuilder AddCofigurationValues(XmlNode oldNode)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < oldNode.ChildNodes.Count; i++)
            {
                if (oldNode.ChildNodes[i].Name == "appSettings")
                {
                    sb.AppendLine("<" + oldNode.ChildNodes[i].Name + ">");
                    sb.AppendLine(oldNode.ChildNodes[i].InnerXml);
                    sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "DatabaseName", viewModel.ConfigurationModel.DatabaseName.Trim()));
                    sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "ServerName", viewModel.ConfigurationModel.ServerName.Trim()));
                    sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "ClientName", viewModel.ConfigurationModel.ClientName.Trim()));
                    sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "ClientAbbreviation", viewModel.ConfigurationModel.ClientAbbreviation.Trim()));
                    sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "Country", viewModel.ConfigurationModel.Country.Trim()));
                    sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "EngagementName", viewModel.ConfigurationModel.EngagementName.Trim()));
                    sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "EngagementNo", viewModel.ConfigurationModel.EngagementNo.Trim()));
                    if (viewModel.ConfigurationModel.ClientLogo != null)
                        sb.AppendLine(String.Format("<add key=\"{0}\" value=\"{1}\"/>", "ClientLogo", Convert.ToBase64String(viewModel.ConfigurationModel.ClientLogo)));
                    //Base64 of client logo
                    sb.AppendLine("</" + oldNode.ChildNodes[i].Name + ">");
                }
                else if (oldNode.ChildNodes[i].Name == "#comment")
                {

                }
                else
                {
                    sb.AppendLine(oldNode.ChildNodes[i].OuterXml);
                }
            }
            return sb;
        }

        private void UploadImage(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open Image";
            openFileDialog.Filter = "Image Files (*.bmp, *.jpg,*.png,*.jpeg)|*.bmp;*.jpg;*.png;*.jpeg";
            if (!openFileDialog.ShowDialog() == true) return;
            var bitmap = ScaleImage(openFileDialog.FileName);
            viewModel.ConfigurationModel.ClientLogo = ToByteArray(bitmap, ImageFormat.Jpeg);
            ImgClientLogo.Source = ByteToImage(viewModel.ConfigurationModel.ClientLogo);
        }

        public static byte[] ToByteArray(Bitmap image, ImageFormat imgFormat)
        {
            byte[] data = null;
            if (image != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, imgFormat);
                    data = ms.ToArray();
                }
                image.Dispose();
            }
            return data != null ? data : null;
        }

        public static ImageSource ByteToImage(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();
            ImageSource imgSrc = biImg as ImageSource;
            return imgSrc;
        }
        public static Bitmap ScaleImage(string path)
        {
            using (var tmpBitMap = new Bitmap(path))
            {
                int imgHeight = 90;
                int imgWidth = GetWidth(tmpBitMap);
                Bitmap bitmap = new Bitmap(imgWidth, imgHeight, System.Drawing.Imaging.PixelFormat.Format64bppArgb);
                bitmap.SetResolution(tmpBitMap.HorizontalResolution, tmpBitMap.VerticalResolution);
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.DrawImage(tmpBitMap, 0, 0, imgWidth, imgHeight);
                }
                return bitmap;
            }
        }

        private static int GetWidth(Bitmap tmpBitMap)
        {
            float ratio = ((float)tmpBitMap.Width / (float)tmpBitMap.Height);
            var width = ratio * 90;
            return (int)width;
        }

    }
}
