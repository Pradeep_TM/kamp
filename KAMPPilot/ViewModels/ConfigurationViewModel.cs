﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAMPPilot
{
    public class ConfigurationViewModel : INotifyPropertyChanged
    {
        public ConfigurationViewModel()
        {
            ConfigurationModel = new ConfigurationModel();
        }

        private ConfigurationModel _configurationModel;

        public ConfigurationModel ConfigurationModel
        {
            get { return _configurationModel; }
            set
            {
                _configurationModel = value;
                PropertyChanged += PropertyChanged;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
